<?php

/** @var Factory $factory */

use Rockapps\RkLaravel\Models\CampaignHit;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(CampaignHit::class, function (Faker $faker) {
    $user = \Rockapps\RkLaravel\Models\User::inRandomOrder()->first();
    return [
        'ip' => $faker->ipv4,
        'agent' => $faker->userAgent,
        'user_id' => $user ? $user->id : null,
        'method' => $faker->randomElement([CampaignHit::METHOD_CLICK,CampaignHit::METHOD_VIEW]),
    ];
});

//$factory->afterCreatingState(CampaignHit::class, 'state-a', function (CampaignHit $campaign_hit) {
//
//});
