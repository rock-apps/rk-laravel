<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Rockapps\RkLaravel\Models\PushNotification;

$factory->define(\Rockapps\RkLaravel\Models\PushNotification::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'channel' => PushNotification::CHANNEL_PUSH,
        'type' => PushNotification::CHANNEL_PUSH,
        'destination' => \Rockapps\RkLaravel\Services\PushNotificationService::destinations()[0]['key'],
        'scheduled_at' => now(),
        'title' => $faker->slug,
        'body' => $faker->sentence,
    ];
});

$factory->afterCreatingState(PushNotification::class, 'with-devices', function (PushNotification $pn, Faker $faker) {
    \Rockapps\RkLaravel\Models\UserDevice::create([
        'registration_id' => 'ExponentPushToken[eFwqHjLJCq9FbsATYKU6WR]',
        'user_id' => 1
    ]);
    \Rockapps\RkLaravel\Models\UserDevice::create([
        'registration_id' => $faker->lexify('?????????'),
        'user_id' => 1
    ]);
});
