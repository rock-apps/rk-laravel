<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeCreditCard;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeCustomer;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\CreditCard;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\User;

$factory->define(Payment::class, function (Faker $faker) {
    $user = factory(User::class)->create();
    return [
        'credit_card_id' => null,
        'address_id' => factory(Address::class)->create([
            'related_type' => User::class,
            'related_id' => $user->id
        ])->id,
        'gateway' => $faker->randomElement(Payment::GATEWAYS),
        'comments_operator' => $faker->sentence,
        'boleto_instructions' => null,
        'boleto_expiration_date' => null,
        'boleto_barcode' => null,
        'value' => $faker->numberBetween(100000, 999999) + $faker->numberBetween(0, 99) / 100,
        'status' => Payment::STATUS_NOT_STARTED,
        'pgm_transaction_id' => null,
        'payer_id' => $user->id,
        'payer_type' => User::class,
//        'purchaseable_id' => factory(User::class)->create()->id,
//        'purchaseable_type' => User::class,
    ];
});
$factory->state(Payment::class, 'user', function ($faker) {
    $user = factory(User::class)->create();
    return [
        'address_id' => factory(Address::class)->create([
            'related_type' => User::class,
            'related_id' => $user->id
        ])->id,
        'payer_id' => $user->id,
        'payer_type' => User::class,
    ];
});
$factory->state(Payment::class, 'cash', function ($faker) {
    $user = factory(User::class)->create([
        'can_pay_with_cash' => true,
    ]);
    return [
        'payer_id' => $user->id,
        'payer_type' => User::class,
        'gateway' => Payment::GATEWAY_CASH,
    ];
});
$factory->state(Payment::class, 'bank', function ($faker) {
    $user = factory(User::class)->create([
        'can_pay_with_bank' => true,
    ]);
    return [
        'payer_id' => $user->id,
        'payer_type' => User::class,
        'gateway' => Payment::GATEWAY_BANK_TRANSFER,
    ];
});
$factory->state(Payment::class, 'cc-paid', function ($faker) {

    $user = factory(User::class)->states('pgm', 'cc')->create([
        'can_pay_with_cc' => true,
    ]);
    PagarMeCustomer::save($user);

    $number = '4242424242424242';
    $cvv = '123';
    $exp = '1223';

    $card = PagarMeCreditCard::create($user->name, $number, $exp, $cvv, $user);

    return [
        'payer_id' => $user->id,
        'payer_type' => User::class,
        'credit_card_id' => $card->id,
        'address_id' => factory(Address::class)->create([
            'related_type' => User::class,
            'related_id' => $user->id
        ])->id,
        'gateway' => Payment::GATEWAY_PAGARME_CC,
    ];
});
$factory->state(Payment::class, 'boleto', function (Faker $faker) {

    $user = factory(User::class)->states('pgm', 'boleto')->create();

    return [
        'payer_id' => $user->id,
        'payer_type' => User::class,
        'credit_card_id' => factory(CreditCard::class)->state('pgm')->create([
            'owner_id' => $user->id,
            'owner_type' => User::class,
        ])->id,
        'address_id' => factory(Address::class)->create([
            'related_type' => User::class,
            'related_id' => $user->id
        ])->id,
        'boleto_instructions' => $faker->sentence,
        'boleto_expiration_date' => $faker->date(),
        'boleto_barcode' => $faker->uuid,
        'gateway' => Payment::GATEWAY_PAGARME_BOLETO,
    ];
});
