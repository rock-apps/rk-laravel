<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Rockapps\RkLaravel\Helpers\Gerador;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeBankAccount;
use Rockapps\RkLaravel\Models\User;

$factory->define(\Rockapps\RkLaravel\Models\BankAccount::class, function (Faker $faker) {
    return [
        'pgm_bank_id' => $faker->numberBetween(100000, 999999),
        'bank_code' => \Rockapps\RkLaravel\Models\BankAccount::bankList()->random(1)->first()['value'],
        'agencia' => $faker->numberBetween(1000, 9999),
        'agencia_dv' => $faker->randomElement([$faker->numberBetween(0, 9), null]),
        'conta' => $faker->numberBetween(1000, 9999),
        'conta_dv' => $faker->randomElement([$faker->numberBetween(0, 9), null]),
        'document_number' => Gerador::CPF(),
        'legal_name' => $faker->name,
        'type' => $faker->randomElement([
            \Rockapps\RkLaravel\Models\BankAccount::TYPE_CONTA_CORRENTE,
            \Rockapps\RkLaravel\Models\BankAccount::TYPE_CONTA_POUPANCA,
            \Rockapps\RkLaravel\Models\BankAccount::TYPE_CONTA_CORRENTE_CONJUNTA,
            \Rockapps\RkLaravel\Models\BankAccount::TYPE_CONTA_POUPANCA_CONJUNTA
        ]),
        'owner_id' => factory(User::class)->create()->id,
        'owner_type' => User::class,
    ];
});

$factory->state(\Rockapps\RkLaravel\Models\BankAccount::class, 'pgm', function (Faker $faker) {

    $user = factory(\Rockapps\RkLaravel\Models\User::class)->create();

    $faker->addProvider(new \Faker\Provider\pt_BR\Person($faker));
    $account = PagarMeBankAccount::create($user,
        $faker->numberBetween(2, 300),
        $faker->numberBetween(1000, 9999),
        $faker->randomElement([$faker->numberBetween(0, 9), null]),
        $faker->numberBetween(1000, 9999),
        $faker->numberBetween(0, 9),
        $faker->cpf,
        $faker->name,
        $faker->randomElement([
            \Rockapps\RkLaravel\Models\BankAccount::TYPE_CONTA_CORRENTE,
            \Rockapps\RkLaravel\Models\BankAccount::TYPE_CONTA_POUPANCA,
            \Rockapps\RkLaravel\Models\BankAccount::TYPE_CONTA_CORRENTE_CONJUNTA,
            \Rockapps\RkLaravel\Models\BankAccount::TYPE_CONTA_POUPANCA_CONJUNTA
        ])
    );
    $account->syncWithBankRecipient();

    $account->id = null;

    return $account->getAttributes();
});
