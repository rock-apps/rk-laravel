<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Rockapps\RkLaravel\Models\Plan;

$factory->define(Plan::class, function (Faker $faker) {
    return [
        'value' => $faker->randomFloat(2, 10, 100),
        'name' => 'Platinum Plan',
        'code' => 'PPMAN',
        'gateway' => Plan::GATEWAY_MANUAL,
        'days' => 30,
        'trial_days' => 0,
        'payment_method' => 'boleto,credit_card',
        'color' => '#000000',
        'charges' => null,
        'installments' => 1,
        'active' => true,
        'invoice_reminder' => 4,
    ];
});

$factory->state(Plan::class, 'pgm',function (Faker $faker) {
    return [
        'gateway' => Plan::GATEWAY_PAGARME,
        'code' => 'PPPGM',
    ];
});

$factory->state(Plan::class, 'trial',function (Faker $faker) {
    return [
        'trial_days' => 7,
    ];
});

$factory->afterCreatingState(Plan::class,'with-variant', function (Plan $plan) {

    $variant = factory(\Rockapps\RkLaravel\Models\Variant::class)->create();

    $plan->variants()->attach($variant->id, ['object_type' => Plan::class]);

});
