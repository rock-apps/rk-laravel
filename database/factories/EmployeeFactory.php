<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use \Rockapps\RkLaravel\Models\Banner;
use Faker\Generator as Faker;
use \Rockapps\RkLaravel\Helpers\Image;

$factory->define(\Rockapps\RkLaravel\Models\Employee::class, function (Faker $faker) {
    $user = factory(\Rockapps\RkLaravel\Models\User::class)->create();

    return [
        'user_id' => $user->id,
        'position' => $faker->lexify('????'),
        'active' => true,
    ];
});


