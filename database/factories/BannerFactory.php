<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use \Rockapps\RkLaravel\Models\Banner;
use Faker\Generator as Faker;
use \Rockapps\RkLaravel\Helpers\Image;

$factory->define(Banner::class, function (Faker $faker) {
    $user = factory(\Rockapps\RkLaravel\Models\User::class)->create();

    return [
        'title' => $faker->sentence(3),
        'link_url' => $faker->url,
        'object_id' => $user->id,
        'object_type' => get_class($user),
        'position' => $faker->lexify('????'),
        'active' => true,
    ];
});

$factory->afterCreatingState(Banner::class, 'with-media', function (Banner $banner) {

    $media = Image::storeMediable(\Rockapps\RkLaravel\Constants\Constants::LOGO_RK_B64, $banner);
    $banner->image = $media->getUrl();
    $banner->save();
});

