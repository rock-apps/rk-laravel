<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Rockapps\RkLaravel\Models\Address;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['Casa', 'Trabalho']),
        'street' => $faker->streetAddress,
        'number' => $faker->numberBetween(1, 5000),
        'city' => $faker->city,
        'state' => $faker->state,
        'district' => $faker->city,
        'country' => $faker->country,
        'zipcode' => '22621140',
    ];
});
