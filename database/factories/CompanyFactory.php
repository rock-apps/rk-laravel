<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\User;

$factory->define(Company::class, function (Faker $faker) {
    $user = \Rockapps\RkLaravel\Models\User::ofRole(\Rockapps\RkLaravel\Constants\Constants::ROLE_COMPANY)->inRandomOrder()->first();
    if (!$user) {
        $user = factory(\Rockapps\RkLaravel\Models\User::class)->create();
        $user->attachRole(\Rockapps\RkLaravel\Models\Role::findOrFailByName(\Rockapps\RkLaravel\Constants\Constants::ROLE_COMPANY));
    }

    return [
        'name' => $faker->company,
        'headline' => $faker->words(5, true),
//        'about' => $faker->paragraph(1),
        'deliver_value' => $faker->numberBetween(15, 30) / 2,
        'deliver_estimate_time' => 2,
        'deliver_max_range' => 200,//km
        'active' => true,
        'mobile' => $faker->phoneNumber,
        'telephone' => $faker->phoneNumber,
        'long' => -43.319227, // ~av das americas 500
        'lat' => -23.004848,
        'responsible_id' => $user->id
    ];
});

$factory->afterCreatingState(Company::class, 'with-products', function (Company $company) {

    factory(\Rockapps\RkLaravel\Models\Product::class, 10)->create([
        'company_id' => $company->id
    ]);

});

$factory->afterCreatingState(Company::class, 'with-employees', function (Company $company) {

    factory(\Rockapps\RkLaravel\Models\Employee::class, 2)->create([
        'company_id' => $company->id
    ]);

});

$factory->afterCreatingState(Company::class, 'with-bank', function (Company $company) {

    /** @var \Rockapps\RkLaravel\Models\BankAccount $bank */
    $bank = factory(\Rockapps\RkLaravel\Models\BankAccount::class)->state('pgm')->create([
        'owner_id' => $company->responsible_id,
        'owner_type' => get_class($company->responsible),
    ]);
    $bank->syncWithBankRecipient();

});

$factory->afterCreatingState(Company::class, 'with-media', function (Company $company) {

    $media = \Rockapps\RkLaravel\Helpers\Image::storeMediable(\Rockapps\RkLaravel\Constants\Constants::LOGO_RK_B64, $company);
    $company->logo = $media->getUrl();
    $company->save();

});

$factory->afterCreating(Company::class, function (Company $company) {

    $company
        ->addMediaFromUrl('https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_92x30dp.png')
        ->toMediaCollection('default');

    if (!$company->address_id) {
        $address = factory(Address::class)->create([
            'related_id' => $company->responsible_id,
            'related_type' => User::class,
            'street' => 'Av das américas',
            'number' => 3000,
            'city' => 'Rio de Janeiro',
            'state' => 'Rio de Janeiro',
            'district' => 'Barra da Tijuca',
            'country' => 'Brasil',
        ]);
        $company->address_id = $address->id;
        $company->save();
    }

});



$factory->afterCreatingState(Company::class, 'with-shipping-methods', function (Company $company) {
    factory(\Rockapps\RkLaravel\Models\ShippingMethod::class)->create([
        'carrier'=>\Rockapps\RkLaravel\Models\ShippingMethod::CARRIER_MANUAL,
        'company_id' => $company->id
    ]);
    factory(\Rockapps\RkLaravel\Models\ShippingMethod::class)->create([
        'carrier'=>\Rockapps\RkLaravel\Models\ShippingMethod::CARRIER_FIXED,
        'fixed_value' => 10.5,
        'company_id' => $company->id,
    ]);
});

$factory->afterCreatingState(Company::class, 'with-payment-methods', function (Company $company) {
    factory(\Rockapps\RkLaravel\Models\PaymentMethod::class)->create([
        'company_id' => $company->id,
        'gateway' => \Rockapps\RkLaravel\Models\PaymentMethod::GATEWAY_PGM,
        'method' => \Rockapps\RkLaravel\Models\Payment::GATEWAY_PAGARME_CC,
        'gateway_key' => 'ak_test_WESiANQmfyLnqC0zRHur1hDwXjoNB0',
        'gateway_secret' => 'ek_test_KdlvzKbho7kQS3jvVvsuNbPxk8svEY',
        'gateway_sandbox' => true,
        'gateway_webhook' => 'https://webhook.site/4f9a323d-fee3-4704-a66e-90d8f43ab687/',
    ]);
    factory(\Rockapps\RkLaravel\Models\PaymentMethod::class)->create([
        'company_id' => $company->id,
        'gateway' => \Rockapps\RkLaravel\Models\PaymentMethod::GATEWAY_PGM,
        'method' => \Rockapps\RkLaravel\Models\Payment::GATEWAY_PAGARME_BOLETO,
        'gateway_key' => 'ak_test_WESiANQmfyLnqC0zRHur1hDwXjoNB0',
        'gateway_secret' => 'ek_test_KdlvzKbho7kQS3jvVvsuNbPxk8svEY',
        'gateway_sandbox' => true,
        'gateway_webhook' => 'https://webhook.site/4f9a323d-fee3-4704-a66e-90d8f43ab687/',
    ]);
    factory(\Rockapps\RkLaravel\Models\PaymentMethod::class)->create([
        'company_id' => $company->id,
        'gateway' => \Rockapps\RkLaravel\Models\PaymentMethod::GATEWAY_MANUAL,
        'method' => \Rockapps\RkLaravel\Models\Payment::GATEWAY_CASH,
    ]);
});

