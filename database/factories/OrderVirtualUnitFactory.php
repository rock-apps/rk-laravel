<?php

/** @var Factory $factory */

use Illuminate\Database\Eloquent\Factory;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Models\OrderVirtualUnit;
use Rockapps\RkLaravel\Models\Product;
use \Rockapps\RkLaravel\Constants\Constants;

$factory->define(OrderVirtualUnit::class, function (Faker\Generator $faker) {
    $user = User::ofRole(Constants::ROLE_USER)->inRandomOrder()->first();
    if (!$user) {
        $user = factory(User::class)->states(Constants::ROLE_USER)->create();
    }
    /** @var Product $product */
    $product = Product::inRandomOrder()->first();
    return [
        'payment_gateway' => Payment::GATEWAY_METHOD_IAP_APPLE,
        'product_id' => $product->id,
        'user_id' => $user->id,
        'quantity' => 1
    ];

});
