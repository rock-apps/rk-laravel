<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Rockapps\RkLaravel\Models\User;

$factory->define(\Rockapps\RkLaravel\Models\BankTransfer::class, function (Faker $faker) {
    return [
        'pgm_transfer_id' => null,
//        'pgm_recipient_id' => null,
        'pgm_transaction_id' => null,
        'gateway' => \Rockapps\RkLaravel\Models\BankTransfer::GATEWAY_MANUAL,
        'comments_operator' => 'Faker',
        'bank_account_id' => null,
        'transfered_id' => factory(User::class)->create()->id,
        'transfered_type' => User::class,
        'type' => \Rockapps\RkLaravel\Models\BankTransfer::TYPE_MANUAL,
        'status' => \Rockapps\RkLaravel\Models\BankTransfer::STATUS_TRANSFERRED,
        'fee' => 3.67,
        'value' => $faker->numberBetween(5, 300),
    ];
});

$factory->state(\Rockapps\RkLaravel\Models\BankTransfer::class, 'manual', []);
