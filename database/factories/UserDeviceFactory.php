<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Models\UserDevice;

$factory->define(UserDevice::class, function (Faker $faker) {

    return [
        'registration_id' => $faker->uuid,
        'user_id' => \factory(User::class)
    ];
});
