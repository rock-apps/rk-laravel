<?php

/** @var Factory $factory */

use Rockapps\RkLaravel\Models\CampaignElement;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(CampaignElement::class, function (Faker $faker) {
    $banner = \Rockapps\RkLaravel\Models\Banner::factory([]);
    return [
        'element_id' => $banner->id,
        'element_type' => get_class($banner),
        'priority' => 100,
        'active' => true,
    ];
});

$factory->afterCreatingState(CampaignElement::class, 'with-media', function (CampaignElement $campaign_element) {
    $banner = \Rockapps\RkLaravel\Models\Banner::factory([],1,['with-media']);
    return [
        'element_id' => $banner->id,
        'element_type' => get_class($banner),
        'priority' => 100,
        'active' => true,
    ];
});

