<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(\Rockapps\RkLaravel\Models\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->words(2,true).$faker->lexify('??????????'),
        'slug' => null,
        'headline' => $faker->sentence(6),
        'description' => $faker->paragraph(),
        'icon' => null,
        'active' => true,
//        'index' => null,
        'model_type' => \Rockapps\RkLaravel\Models\User::class,
    ];
});
