<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeCustomer;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;

$factory->define(User::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\pt_BR\Person($faker));
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => '12345678',
        'document' => $faker->cpf,
        'birth_date' => '1990-01-01',
        'mobile' => $faker->numerify('###########'),
        'telephone' => $faker->numerify('###########'),
        'gender' => $faker->randomElement([User::GENDER_MALE, User::GENDER_MALE]),
    ];
});

$factory->state(User::class, 'pgm', [
    'birth_date' => '1980-01-01',// Se ativar, o user será integrado no Pagarme (verificar o boot no model)
    'document' => '57566542087',
    'mobile' => '21999988888'
]);
$factory->afterCreatingState(User::class, 'pgm', function (User $user) {

    \Cache::tags(config('entrust.role_user_table'))->flush();
    $role = \Rockapps\RkLaravel\Models\Role::findOrFailByName(Constants::ROLE_USER);
    if(!$user->hasRole(Constants::ROLE_USER)) {
        $user->attachRole($role);
    }
    if (!$user->pgm_customer_id && $user->document && $user->birth_date) {
        PagarMeCustomer::save($user);
    }
});


$factory->state(User::class, 'bank', [
    'can_pay_with_bank' => true,
]);
$factory->state(User::class, 'cash', [
    'can_pay_with_cash' => true,
]);
$factory->state(User::class, 'boleto', [
    'can_pay_with_boleto' => true,
]);
$factory->state(User::class, 'balance', [
    'can_pay_with_balance' => true,
]);
$factory->state(User::class, 'cc', [
    'can_pay_with_cc' => true,
]);
$factory->state(User::class, 'pix', [
    'can_pay_with_pix' => true,
]);


$factory->state(User::class, Constants::ROLE_ADMIN, [
    'password' => Constants::ROLE_ADMIN
]);
$factory->state(User::class, Constants::ROLE_USER, [
    'password' => Constants::ROLE_USER
]);
$factory->state(User::class, Constants::ROLE_USER, [
    'password' => Constants::ROLE_USER
]);
$factory->state(User::class, Constants::ROLE_COMPANY, [
    'password' => Constants::ROLE_COMPANY
]);


$factory->afterCreatingState(User::class, Constants::ROLE_USER, function (User $user) {

    \Cache::tags(config('entrust.role_user_table'))->flush();
    $role = Role::findOrFailByName(Constants::ROLE_USER);
    if(!$user->hasRole($role)) {
        $user->attachRole($role);
    }
});
$factory->afterCreatingState(User::class, Constants::ROLE_ADMIN, function (User $user) {

    \Cache::tags(config('entrust.role_user_table'))->flush();
    $role = Role::findOrFailByName(Constants::ROLE_ADMIN);
    if(!$user->hasRole($role)) {
        $user->attachRole($role);
    }
});
$factory->afterCreatingState(User::class, Constants::ROLE_COMPANY, function (User $user) {
//    $user->attachRole(Role::findOrFailByName(Constants::ROLE_COMPANY));
    factory(Company::class, 1)->create([
        'responsible_id' => $user->id
    ]);
});
$factory->afterCreatingState(User::class, 'with-bank', function (User $user) {
    /** @var \Rockapps\RkLaravel\Models\BankAccount $bank */
    $bank = factory(\Rockapps\RkLaravel\Models\BankAccount::class)->state('pgm')->create([
        'owner_id' => $user->id,
        'owner_type' => get_class($user),
    ]);
    $bank->syncWithBankRecipient();
});

$factory->afterCreatingState(User::class, 'with-address', function (User $user) {

    factory(\Rockapps\RkLaravel\Models\Address::class)->create([
        'related_id' => $user->id,
        'related_type' => get_class($user),
    ]);
});
