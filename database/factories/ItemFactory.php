<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Rockapps\RkLaravel\Models\OrderItem;
use Rockapps\RkLaravel\Models\Order;
use Faker\Generator as Faker;

$factory->define(ORderItem::class, function (Faker $faker) {
//    /** @var Order $order */
//    $order = Order::all()->random(1)->first();
//    $company = $order->company;
//    $product = $company->products()->inRandomOrder()->first();

    return [
        'comments' => $faker->sentence(6),
        'quantity' => $faker->numberBetween(1,3),
//        'unit_value' => $product->uni,
//        'product_id' => $product->id, // Precisa ser enviado durante o create
//        'order_id' => $order->id, // Precisa ser enviado durante o create
    ];
});
