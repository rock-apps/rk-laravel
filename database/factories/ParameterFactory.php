<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Models\User;

$factory->define(Parameter::class, function (Faker $faker) {
    return [
        'key' => $faker->name,
        'group' => $faker->name,
        'type' => Parameter::TYPE_STRING,
        'value' => $faker->sentence,
    ];
});
