<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeCreditCard;
use Rockapps\RkLaravel\Models\CreditCard;

$factory->define(CreditCard::class, function (Faker $faker) {

    return [
        'pgm_card_id' => '12345678',
        'brand' => 'visa',
        'last_digits' => '1234',
        'valid' => 1,
        'holder_name' => 'Usuário Teste',
        'expiration_date' => '1223',
        'default' => $faker->randomElement([0, 1]),
//        'owner_id' => factory(\Rockapps\RkLaravel\Models\User::class)->create()->id,
//        'owner_type' => \Rockapps\RkLaravel\Models\User::class,
    ];
});

$factory->afterCreatingState(CreditCard::class, 'pgm', function (CreditCard $card) {

    $number = '4242424242424242';
    $cvv = '123';
    $exp = '1230';

    $pgm_card = PagarMeCreditCard::create($card->owner->name, $number, $exp, $cvv, $card->owner);
    $card->pgm_card_id = $pgm_card->pgm_card_id;
    $card->save();
});

$factory->afterCreatingState(CreditCard::class, 'pgm-deny', function (CreditCard $card) {

    $number = '4444444444444444';
    $cvv = '666';
    $exp = '1230';

    $pgm_card = PagarMeCreditCard::create($card->owner->name, $number, $exp, $cvv, $card->owner);
    $card->pgm_card_id = $pgm_card->pgm_card_id;
    $card->save();
});
