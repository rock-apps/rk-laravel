<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Rockapps\RkLaravel\Models\Post;

$factory->define(Post::class, function (Faker $faker) {
    $user = \Rockapps\RkLaravel\Models\User::query()->inRandomOrder()->first();
    if(!$user) $user = \factory(\Rockapps\RkLaravel\Models\User::class)->create();

    return [
        'title' => $faker->sentence,
        'heading' => $faker->sentence,
//        'slug',
        'status' => Post::STATUS_APPROVED,
        'owner_id' => $user->id,
        'owner_type'=>\Rockapps\RkLaravel\Models\User::class,
        'visibility' => 'PUBLIC',
        'posted_at' => now(),
        'approved_at' => now(),
    ];
});

$factory->define(\Rockapps\RkLaravel\Models\PostBlock::class, function (Faker $faker) {
    return [
        'content' => $faker->paragraphs(4, true),
        'type' => \Rockapps\RkLaravel\Models\PostBlock::TYPE_TEXT,
        'sequence' => 1,
//        'post_id',
//        'text_size',
//        'bold',
//        'italic',
//        'color',
//        'style',
    ];
});

$factory->afterCreating(Post::class, function (Post $post) {
    \factory(\Rockapps\RkLaravel\Models\PostBlock::class, 3)->create([
        'post_id' => $post->id
    ]);
});
