<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\Rockapps\RkLaravel\Models\ShippingMethod::class, function (Faker $faker) {

    return [
        'carrier' => \Rockapps\RkLaravel\Models\ShippingMethod::CARRIER_MANUAL,
//        'carrier_key' => 'nullable',
//        'carrier_secret' => 'nullable',
//        'carrier_sandbox' => 'nullable|bool',
//        'company_id' => 'required|exists:companies,id',
//        'address_id' => 'nullable|exists:addresses,id',
        'active' => true,
//        'image' => 'nullable',
        'title' => 'Método de Entrega',
        'description' => 'Descrição do método de entrega',
//        'fixed_value' => 'nullable|numeric',
    ];
});

