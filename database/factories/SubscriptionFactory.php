<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Rockapps\RkLaravel\Models\CreditCard;
use Rockapps\RkLaravel\Models\Plan;
use Rockapps\RkLaravel\Models\Subscription;
use Rockapps\RkLaravel\Models\User;

$factory->define(Subscription::class, function (Faker $faker) {
    $user = factory(User::class)->create();
    $user = factory(User::class)->create();
    $plan = factory(Plan::class)->create();
    return [
        'plan_id' => $plan->id,
        'status' => Subscription::STATUS_NOT_STARTED,
//        'mode' => Subscription::MODE_LIFETIME,
//        'gateway' => Subscription::GATEWAY_MANUAL,
//        'payment_method' => Subscription::PAYMENT_METHOD_FREE,
        'comments_operator' => $faker->sentence,
        'payer_type' => User::class,
        'payer_id' => $user->id,
        'subscriber_type' => User::class,
        'subscriber_id' => $user->id,
    ];
});

$factory->state(Subscription::class, 'free-lifetime-active', function (Faker $faker) {
    return [
        'mode' => Subscription::MODE_LIFETIME,
        'gateway' => Subscription::GATEWAY_MANUAL,
        'payment_method' => Subscription::PAYMENT_METHOD_FREE,
    ];
});


$factory->state(Subscription::class, 'pgm-credit-card', function (Faker $faker) {

    $user = factory(User::class)->states('pgm', 'cc', 'boleto', 'with-address')->create();

    $card = factory(CreditCard::class)->state('pgm')->create([
        'owner_id' => $user->id,
        'owner_type' => User::class,
    ]);
    $plan = factory(Plan::class)->state('pgm')->create();
    return [
        'plan_id' => $plan->id,
        'status' => Subscription::STATUS_NOT_STARTED,
        'mode' => Subscription::MODE_RECURRENT,
        'payment_method' => Subscription::PAYMENT_METHOD_CREDIT_CARD,
        'gateway' => Subscription::GATEWAY_PAGARME,
        'comments_operator' => $faker->sentence,
        'payer_type' => User::class,
        'payer_id' => $user->id,
        'credit_card_id' => $card->id,
    ];
});

$factory->state(Subscription::class, 'pgm-trial', function (Faker $faker) {

    $user = factory(User::class)->states('pgm', 'cc', 'boleto', 'with-address')->create();

    $card = factory(CreditCard::class)->state('pgm')->create([
        'owner_id' => $user->id,
        'owner_type' => User::class,
    ]);

    $plan = factory(Plan::class)->states('pgm', 'trial')->create();
    return [
        'plan_id' => $plan->id,
        'status' => Subscription::STATUS_NOT_STARTED,
        'mode' => Subscription::MODE_RECURRENT,
        'payment_method' => Subscription::PAYMENT_METHOD_CREDIT_CARD,
        'gateway' => Subscription::GATEWAY_PAGARME,
        'comments_operator' => $faker->sentence,
        'payer_type' => User::class,
        'payer_id' => $user->id,
        'credit_card_id' => $card->id,
    ];
});

$factory->state(Subscription::class, 'pgm-boleto', function (Faker $faker) {

    $user = factory(User::class)->states('pgm', 'cc', 'boleto', 'with-address')->create();

    $plan = factory(Plan::class)->state('pgm')->create();
    return [
        'plan_id' => $plan->id,
        'status' => Subscription::STATUS_NOT_STARTED,
        'mode' => Subscription::MODE_RECURRENT,
        'payment_method' => Subscription::PAYMENT_METHOD_BOLETO,
        'gateway' => Subscription::GATEWAY_PAGARME,
        'comments_operator' => $faker->sentence,
        'payer_type' => User::class,
        'payer_id' => $user->id,
    ];
});


