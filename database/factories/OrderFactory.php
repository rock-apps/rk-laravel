<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Rockapps\RkLaravel\Models\Order;
use Rockapps\RkLaravel\Models\OrderItem;
use Rockapps\RkLaravel\Models\Product;
use Rockapps\RkLaravel\Models\User;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'status' => Order::STATUS_DRAFT,
        'user_id' => factory(User::class)->state(\Rockapps\RkLaravel\Constants\Constants::ROLE_USER)->create()->id,
        'company_id' => factory(\Rockapps\RkLaravel\Models\Company::class)->state('with-products')->create()->id,
        'description' => $faker->paragraph(1),
    ];
});

$factory->state(Order::class, 'company-with-bank', function (Faker $faker) {

    /** @var \Rockapps\RkLaravel\Models\Company $company */
    $company = factory(\Rockapps\RkLaravel\Models\Company::class)->states(['with-products', 'with-bank'])->create();

    return [
        'status' => Order::STATUS_DRAFT,
        'user_id' => factory(User::class)->state(\Rockapps\RkLaravel\Constants\Constants::ROLE_USER)->create()->id,
        'company_id' => $company->id,
        'description' => $faker->paragraph(1),
    ];
});


$factory->afterCreatingState(Order::Class, 'with-items', function (Order $order, Faker $faker) {

    factory(\Rockapps\RkLaravel\Models\OrderItem::class)->create([
        'order_id' => $order->id,
        'product_id' => factory(\Rockapps\RkLaravel\Models\Product::class)->create(['company_id'=> $order->company_id])->id,
    ]);
});


$factory->state(Order::class, Order::STATUS_DRAFT, [
    'status' => Order::STATUS_DRAFT
]);
$factory->state(Order::class, Order::STATUS_PENDENTE, function (Faker $faker) {
    return [
        'status' => Order::STATUS_PENDENTE,
    ];
});
$factory->state(Order::class, Order::STATUS_APROVADO, function (Faker $faker) {
    return [
        'approved_at' => $faker->dateTimeBetween('-10 months'),
        'status' => Order::STATUS_APROVADO,
    ];
});
//$factory->state(Order::class, Order::STATUS_PAGO, function (Faker $faker) {
//    return [
//        'approved_at' => $faker->dateTimeBetween('-10 months'),
//        'status' => Order::STATUS_PAGO,
//    ];
//});
$factory->state(Order::class, Order::STATUS_ENTREGUE, function (Faker $faker) {
    return [
        'rating' => $faker->numberBetween(1, 5),
        'rating_description' => $faker->paragraph(1),
        'deliver_scheduled_at' => $faker->dateTimeBetween('-10 months'),
        'approved_at' => $faker->dateTimeBetween('-10 months'),
        'delivered_at' => $faker->dateTimeBetween('-10 months'),
        'status' => Order::STATUS_ENTREGUE,
    ];
});
$factory->state(Order::class, Order::STATUS_CANCELADO, [
    'status' => Order::STATUS_CANCELADO
]);
$factory->afterCreatingState(Order::class, 'with-items', function (Order $order) {

    factory(OrderItem::class, 3)->create([
        'product_id' => factory(Product::class)->create(['company_id' => $order->company_id])->id,
        'order_id' => $order->id,
    ]);
});

//$factory->afterCreatingState(Order::class, Order::STATUS_PAGO, function (Order $order, Faker $faker) {
//    $order->createPayment(\App\Models\Payment::GATEWAY_PAGARME_CC, $order->user->creditcards->first()->id);
//});
