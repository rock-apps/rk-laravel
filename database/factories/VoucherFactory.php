<?php

/** @var Factory $factory */

use Rockapps\RkLaravel\Models\Voucher;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Voucher::class, function (Faker $faker) {
    return [
//        'model_id',
//        'model_type',
//        'code',
//        'data',
        'expires_at'=> now()->addDays(30),
        'max_redeem_qty'=> 1,
//        'company_id',
//        'campaign',

    ];
});

$factory->afterCreatingState(Voucher::class, 'with-categories', function (Voucher $voucher) {
    $category = factory(\Rockapps\RkLaravel\Models\Category::class)->create([
        'name' => 'Product Category',
        'model_type' => \Rockapps\RkLaravel\Models\Product::class,
    ]);
    $voucher->categories()->attach($category->id);
});
