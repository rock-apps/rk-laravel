<?php

/** @var Factory $factory */

use Rockapps\RkLaravel\Models\Variant;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Variant::class, function (Faker $faker) {
    return [
        'name' => 'Variant Teste',
        'type' => 'cor',
        'value' => $faker->colorName,
        'value_cast' => \Rockapps\RkLaravel\Models\Parameter::TYPE_STRING ,
        'description' => $faker->sentence,
        'icon' => 'fa fa-home',
        'color' => $faker->hexColor,
        'sequence' => $faker->randomNumber(1),
    ];
});

$factory->afterCreatingState(Variant::class, 'with-categories', function (Variant $variant) {
    $category = factory(\Rockapps\RkLaravel\Models\Category::class)->create([
        'name' => 'Product Category',
        'model_type' => \Rockapps\RkLaravel\Models\Product::class,
    ]);
    $variant->categories()->attach($category->id);
});
