<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Rockapps\RkLaravel\Models\Product;
use Rockapps\RkLaravel\Models\Variant;

$factory->define(Product::class, function (Faker $faker) {


    $n1 = ['Bolo', 'Corte', 'Hambúrguer', 'Prato', 'Churrasco', 'Assado'];
    $n2 = ['Especial', 'de Chocolate', 'de Costela', 'de Frango', 'de Linguiça', 'com Bacon'];

    return [
        'name' => $faker->randomElement($n1) . ' ' . $faker->randomElement($n2),
        'description' => $faker->paragraph(1),
        'active' => true,
        'image' => $faker->imageUrl(),
        'unit_value' => $faker->randomFloat(1, 7, 30),
//        'company_id' => $company ? $company->id : null,
//        'category_id' => $category ? $category->id : null,
    ];
});

$factory->afterCreatingState(Product::class, 'with-media', function (Product $product) {

    $media = \Rockapps\RkLaravel\Helpers\Image::storeMediable(\Rockapps\RkLaravel\Constants\Constants::LOGO_RK_B64, $product);
    $product->image = $media->getUrl();
    $product->save();
});


$factory->state(Product::class,'apple-100min', [
    'name' => "Pacote 100 minutos",
    'apple_product_id' => 'br.com.rk.100min',
    'unit_value' => 22.90,
    'virtual_units_release' => 100*600
]);

$factory->afterCreatingState(Product::class,'with-variant', function (Product $product) {

    $variant = factory(Variant::class)->create();

    $product->variants()->attach($variant->id, ['object_type' => Product::class]);

});
