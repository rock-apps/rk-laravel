<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Rockapps\RkLaravel\Models\Campaign;

$factory->define(Campaign::class, function (Faker $faker) {
    return [
        'name' => 'Campanha',
        'url' => $faker->url,
        'active' => true,
//        'company_id' => 'nullable|exists:companies,id',
        'actual_views' => 0,
        'actual_clicks' => 0,
        'max_views' => 200,
        'max_clicks' => 200,
//        'priority' => 'nullable|numeric',
//        'start_at' => 'nullable|date',
//        'end_at' => 'nullable|date',

    ];
});

$factory->afterCreatingState(Campaign::class, 'with-elements', function (Campaign $campaign) {
    \Rockapps\RkLaravel\Models\CampaignElement::factory(['campaign_id' => $campaign->id,], 5);
});


$factory->afterCreatingState(Campaign::class, 'with-hits', function (Campaign $campaign) {

    $user = \Rockapps\RkLaravel\Models\User::inRandomOrder()->first();
    if(!$user) $user = \Rockapps\RkLaravel\Models\User::factory();

    $campaign->elements()->each(function (\Rockapps\RkLaravel\Models\CampaignElement $campaign_element) use($user) {

        \Rockapps\RkLaravel\Models\CampaignHit::factory([
            'user_id' => $user->id,
            'element_type' => $campaign_element->element_type,
            'element_id' => $campaign_element->element_id,
            'campaign_element_id' => $campaign_element->id,
            'campaign_id' => $campaign_element->campaign_id,
        ], 5);
    });
});



