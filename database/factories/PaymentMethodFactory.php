<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use \Rockapps\RkLaravel\Models\Banner;
use Faker\Generator as Faker;
use \Rockapps\RkLaravel\Helpers\Image;

$factory->define(\Rockapps\RkLaravel\Models\PaymentMethod::class, function (Faker $faker) {

    return [
        'gateway' => \Rockapps\RkLaravel\Models\PaymentMethod::GATEWAY_MANUAL,
        'method' => \Rockapps\RkLaravel\Models\Payment::GATEWAY_MANUAL,
        'gateway_key' => '123456',
        'gateway_secret' => 'ABCDEF',
        'gateway_sandbox' => true,
        'gateway_soft_descriptor' => 'TESTE',
//            'bank_account_id' => 'nullable|exists:bank_accounts,id',
        'active' => true,
        'title' => 'title',
        'description' => 'description',
//        'company_id' => $this->company->id,
    ];
});

