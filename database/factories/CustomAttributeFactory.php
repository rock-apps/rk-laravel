<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\Rockapps\RkLaravel\Models\CustomAttribute::class, function (Faker $faker) {
    return [
        'field' => 'name',
        'cast' => Rockapps\RkLaravel\Models\CustomAttribute::CAST_STRING,
        'value' => 'ABC',
    ];
});
