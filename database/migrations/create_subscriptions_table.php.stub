<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('status');
            $table->string('mode');
            $table->string('gateway');
            $table->string('payment_method');
            $table->string('pgm_subscription_id')->nullable();
            $table->text('iap_receipt_id')->nullable();
            $table->text('apple_transaction_id')->nullable();

            $table->string('comments_operator')->nullable();

            $table->integer('plan_id')->unsigned()->index()->nullable();
            $table->foreign('plan_id')->references('id')->on('plans');

            $table->integer('credit_card_id')->unsigned()->index()->nullable();
            $table->foreign('credit_card_id')->references('id')->on('credit_cards');

            $table->integer('address_id')->unsigned()->index()->nullable();
            $table->foreign('address_id')->references('id')->on('addresses');

            $table->morphs('subscriber');
            $table->morphs('payer');

            $table->dateTime('current_period_start')->nullable();
            $table->dateTime('current_period_end')->nullable();
            $table->dateTime('subscribed_at')->nullable();
            $table->dateTime('expired_at')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
