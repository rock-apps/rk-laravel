<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable();
            $table->string('short_name')->nullable();
            $table->string('fantasy_name')->nullable();
            $table->string('document')->nullable();
            $table->string('description')->nullable();
            $table->string('priority')->default(\Rockapps\RkLaravel\Models\Company::PRIORITY_LOW);

            $table->string('headline')->nullable();
            $table->boolean('ready_to_search')->default(false);
            $table->boolean('active')->default(true);
            $table->boolean('suspended')->default(false);

            $table->string('contact_telephone')->nullable();
            $table->string('contact_telephone_description')->nullable();
            $table->string('contact_mobile')->nullable();
            $table->string('contact_mobile_description')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_email_description')->nullable();
            $table->string('contact_whatsapp')->nullable();
            $table->string('contact_instagram')->nullable();
            $table->string('contact_facebook')->nullable();

            $table->string('primary_color')->nullable();
            $table->string('secondary_color')->nullable();
            $table->string('third_color')->nullable();

            $table->string('logo')->nullable();
            $table->string('subdomain')->nullable();

            $table->decimal('deliver_value')->default(0);
            $table->integer('deliver_estimate_time')->nullable();
            $table->integer('deliver_max_range')->nullable();
            $table->string('mobile')->nullable();
            $table->string('telephone')->nullable();

            $table->decimal('long', 10, 7)->nullable();
            $table->decimal('lat', 10, 7)->nullable();

            $table->string('recipient_id')->nullable();
            $table->string('bank_account_id')->nullable();

            $table->integer('address_id')->nullable();

            $table->integer('responsible_id')->unsigned()->index()->nullable();

            $table->string('email')->nullable();
            $table->string('cnpj')->nullable();
            $table->string('cnaes')->nullable();
            $table->string('cnae_main')->nullable();
            $table->date('date_opening')->nullable();
            $table->string('situation')->nullable();
            $table->string('segment')->nullable();
            $table->string('size')->nullable(); //
            $table->string('business')->nullable();
            $table->string('website')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });

        // Create table for associating objectives to users (Many-to-Many)
        \Schema::create('company_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('company_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id', 'company_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('company_user');
        \Schema::dropIfExists('companies');
    }
}
