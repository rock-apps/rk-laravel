<?php

use Illuminate\Database\Seeder;
use Rockapps\RkLaravel\Models\Product;

class RkProductTableSeeder extends Seeder
{
    public function run()
    {
        if (env('DB_CONNECTION') !== 'sqlite') \DB::statement('SET FOREIGN_KEY_CHECKS = 0');

//        Eloquent::unguard();
        Product::truncate();
        $company = \Rockapps\RkLaravel\Models\Company::first();

        $variant = factory(\Rockapps\RkLaravel\Models\Variant::class)->create();

        factory(Product::class)->create([
            'type' => Product::TYPE_PRODUCT_VIRTUAL,
            'name' => "Bônus para indicação",
            'sku' => "BONUS-INDICACAO",
            'unit_value' => 0,
            'virtual_units_release' => 1 * 60,
            'company_id' => $company->id,
            'active' => false,
        ])
            ->variants()->attach($variant, ['object_type' => Product::class]);

        factory(Product::class)->create([
            'type' => Product::TYPE_PRODUCT_VIRTUAL,
            'name' => "Bônus para novo usuário",
            'sku' => "BONUS-SIGNUP",
            'unit_value' => 0,
            'virtual_units_release' => 10 * 60,
            'company_id' => $company->id,
            'active' => false,
        ])
            ->variants()->attach($variant, ['object_type' => Product::class]);

        factory(Product::class)->create([
            'type' => Product::TYPE_PRODUCT_VIRTUAL,
            'name' => "Pacote 100 minutos",
            'apple_product_id' => 'br.com.voice.guuruu.100min',
            'unit_value' => 22.90,
            'virtual_units_release' => 100 * 60,
            'company_id' => $company->id,
            'active' => true,
            'active_android' => false,
            'active_ios' => true,
        ])
            ->variants()->attach($variant, ['object_type' => Product::class]);
    }
}
