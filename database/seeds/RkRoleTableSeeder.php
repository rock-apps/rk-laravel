<?php

use Illuminate\Database\Seeder;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Models\Permission;
use Rockapps\RkLaravel\Models\Role;

class RkRoleTableSeeder extends Seeder
{
    public function run()
    {
        if (env('DB_CONNECTION') !== 'sqlite') {
//            \DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        }
//        Eloquent::unguard();
        Role::truncate();

        $editCompany = new Permission();
        $editCompany->name = 'edit-company';
        $editCompany->save();


        $admin = Role::firstOrCreate([
            'name' => Constants::ROLE_ADMIN,
            'display_name' => "Administrador",
        ]);

        $createObject = Permission::firstOrCreate([
            'name' => 'create-object',
            'display_name' => 'Criar Objeto',
            'description' => 'Permite Criar Objeto',
        ]);
        $admin->attachPermission($createObject);
        $updateObject = Permission::firstOrCreate([
            'name' => 'update-object',
            'display_name' => 'Atualizar Objeto',
            'description' => 'Permite Atualizar Objeto',
        ]);
        $admin->attachPermission($updateObject);

        Role::firstOrCreate([
            'name' => Constants::ROLE_USER,
            'display_name' => "Cliente",
        ]);

        Role::firstOrCreate([
            'name' => Constants::ROLE_COMPANY,
            'display_name' => "Empresa",
        ]);

    }
}
