<?php

use \Rockapps\RkLaravel\Models\Parameter;
use Illuminate\Database\Seeder;

class RkParameterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env('DB_CONNECTION') !== 'sqlite') {
            \DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        }
//        Eloquent::unguard();
        Parameter::truncate();

    }
}
