<?php

use Illuminate\Database\Seeder;
use Rockapps\RkLaravel\Models\Product;

class RkCompanyTableSeeder extends Seeder
{
    public function run()
    {
        if (env('DB_CONNECTION') !== 'sqlite') \DB::statement('SET FOREIGN_KEY_CHECKS = 0');

//        Eloquent::unguard();
        Product::truncate();

        factory(\Rockapps\RkLaravel\Models\Company::class)->create([
            'name' => 'RockApps Company'
        ]);
    }
}
