@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Confirmação de E-Mail</div>

                    <div class="panel-body">

                        <?php if($user) { ?>
                        <div class="alert alert-success">
                            O email {{ $user->email }} foi verificado com sucesso!
                        </div>
                        <?php } ?>
                        <?php if(!$user) { ?>
                        <div class="alert alert-danger">
                            Ocorreu um erro ao confirmar o usuário.
                        </div>
                        <?php } ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
