rm -rf database\migrations\202*
composer dumpautoload
php artisan vendor:publish --provider="Rockapps\RkLaravel\RkLaravelServiceProvider"
php artisan migrate:fresh
php artisan ide-helper:models -W
rm -rf database\migrations\20*
