# RockApps Package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/rockapps/rk-laravel.svg?style=flat-square)](https://packagist.org/packages/rockapps/rk-laravel)
[![Build Status](https://img.shields.io/travis/rockapps/rk-laravel/master.svg?style=flat-square)](https://travis-ci.org/rockapps/rk-laravel)
[![Quality Score](https://img.shields.io/scrutinizer/g/rockapps/rk-laravel.svg?style=flat-square)](https://scrutinizer-ci.com/g/rockapps/rk-laravel)
[![Total Downloads](https://img.shields.io/packagist/dt/rockapps/rk-laravel.svg?style=flat-square)](https://packagist.org/packages/rockapps/rk-laravel)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.
# RockApps Laravel Package

## Installation

You can install the package via composer:

```bash
composer require rockapps/rk-laravel
```

## Usage

``` php
php artisan vendor:publish --provider="RockApps\RkLaravel\RkLaravelServiceProvider" --tag="migrations"
```


## Nova Migration

Criar a nova migration
```bash
php artisan make:migration create_campaign_table --table=campaigns
```

Renomear o arquivo e adicionar a extensão: `2021_09_02_162303_create_campaign_table.php` para `create_campaign_table.php.stub`.

Adicionar no arquivo `MigrationTestTrait.php` as linhas 

```php
include_once __DIR__ . $prefix . '/database/migrations/create_campaign_table.php.stub';
// e
(new \CreateCampaignTable())->up();
```

Adicionar no arquivo `RkLaravelServiceProvider.php` no método `publishMigration()`, o nome da migration `create_campaign_table`.



### Testing

``` bash
composer test
```

### Security

If you discover any security related issues, please email erick.engelhardt@rockapps.com.br instead of using the issue tracker.

## Credits

- [Erick Engelhardt](https://github.com/rockapps)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
