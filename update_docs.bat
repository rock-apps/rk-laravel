del /f database\migrations\20* && ^
rm -rf database\migrations\20* && ^
composer dumpautoload && ^
php artisan vendor:publish --provider="Rockapps\RkLaravel\RkLaravelServiceProvider" && ^
php artisan migrate:fresh && ^
php artisan ide-helper:models -W && ^
del /f database\migrations\20* && ^
rm -rf database\migrations\20*
