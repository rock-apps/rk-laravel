<?php

use Rockapps\RkLaravel\Constants\Constants;

\Broadcast::channel('App.Admin', function ($user) {
    /** @var \Rockapps\RkLaravel\Models\User $user */
    return $user->hasRole(Constants::ROLE_ADMIN);
});

\Broadcast::channel('Public', function ($user, $id) {
    return true;
});

\Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int)$user->id === (int)$id;
});

