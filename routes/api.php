<?php

use Dingo\Api\Routing\Router;
use Imdhemy\AppStore\Receipts\ReceiptResponse;
use Imdhemy\GooglePlay\Products\ProductPurchase;
use Imdhemy\GooglePlay\Subscriptions\SubscriptionPurchase;
use Imdhemy\Purchases\Facades\Product;
use Imdhemy\Purchases\Facades\Subscription;
use Rockapps\RkLaravel\Routes\Api;


/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {

    $api->group(['middleware' => ['sentry', 'user.suspended', 'user.last.access', 'rk.maintenance', 'log.request.before', 'log.request.after']], function (Router $api) {

        Api::auth($api);
        Api::campaigns($api);
        Api::terms($api);

        $api->get('sample/test', \App\Api\Sample\SampleController::class . '@test');
        $api->post('sample/test', \App\Api\Sample\SampleController::class . '@testPost');
        $api->get('maintenance/off', \App\Api\Sample\SampleController::class . '@maintenanceOff');
        $api->get('maintenance/on', \App\Api\Sample\SampleController::class . '@maintenanceOn');

        $api->get('maintenance/force', function () {
            return ['data' => 'ok'];
        });

        $api->group(['middleware' => ['rk.auth']], function (Router $api) {

            Api::addresses($api);
            Api::bankAccounts($api);
            Api::bankTransfers($api);
            Api::banners($api);
            Api::campaigns($api);
            Api::categories($api);
            Api::chats($api);
            Api::companies($api);
            Api::comments($api);
            Api::configuration($api);
            Api::creditCards($api);
            Api::ordersVirtualUnits($api);
            Api::payments($api);
            Api::paymentMethods($api);
            Api::parameters($api);
            Api::permissions($api);
            Api::plans($api);
            Api::products($api);
            Api::roles($api);
            Api::search($api);
            Api::shippingMethods($api);
            Api::subscriptions($api);
            Api::customAttributes($api);
            Api::userLogged($api);
            Api::users($api);
            Api::vouchers($api);

            $api->get('refresh', ['middleware' => 'rk.refresh', response()->json(['message' => 'Refreshed.'])]);
        });


        $api->group(['middleware' => ['role:admin']], function (Router $api) {
            Api::auditsAdmin($api);
            Api::bannersAdmin($api);
            Api::bankTransfersAdmin($api);
            Api::campaignsAdmin($api);
            Api::campaignElementsAdmin($api);
            Api::categoriesAdmin($api);
            Api::commentsAdmin($api);
            Api::companiesAdmin($api);
            Api::mediasAdmin($api);
            Api::ordersVirtualUnitsAdmin($api);
            Api::parametersAdmin($api);
            Api::paymentsAdmin($api);
            Api::permissionsAdmin($api);
            Api::pushNotificationsAdmin($api);
            Api::plansAdmin($api);
            Api::productsAdmin($api);
            Api::rolesAdmin($api);
            Api::usersAdmin($api);
            Api::subscriptionsAdmin($api);
            Api::variantsAdmin($api);
            Api::vouchersAdmin($api);
        });

    });
    $api->get('lab', function () {


        /**
         *
         * COMPRA GOOGLE
         *
         */
        $itemId = 'product_id';
        $token = 'purchase_token';

        Product::googlePlay()->id($itemId)->token($token)->acknowledge();
// You can optionally submit a developer payload
        Product::googlePlay()->id($itemId)->token($token)->acknowledge("your_developer_payload");

        /** @var ProductPurchase $productReceipt */
        $productReceipt = Product::googlePlay()->id($itemId)->token($token)->get();
        $productReceipt->getProductId();


        /**
         *
         *
         * COMPRA APPLE
         *
         */

        $receiptData = 'the_base64_encoded_receipt_data';
        /** @var ReceiptResponse $receiptResponse */
        $receiptResponse = Product::appStore()->receiptData($receiptData)->verifyReceipt();
        $receiptResponse->getReceipt()->getInApp();


        /**
         *
         *
         * SUBSCRIPTION GOOGLE
         *
         */
        $itemId = 'product_id';
        $token = 'purchase_token';

        Subscription::googlePlay()->id($itemId)->token($token)->acknowledge();
// You can optionally submit a developer payload
        Subscription::googlePlay()->id($itemId)->token($token)->acknowledge("your_developer_payload");

        /** @var SubscriptionPurchase $subscriptionReceipt */
        $subscriptionReceipt = Subscription::googlePlay()->id($itemId)->token($token)->get();
        $subscriptionReceipt->getStartTime();
        $subscriptionReceipt->getExpiryTime()->isFuture();
// You can optionally override the package name
        Subscription::googlePlay()->packageName('com.example.name')->id($itemId)->token($token)->get();


        /**
         *
         *
         * SUBSCRIPTION APPLE
         *
         *
         */
        // To verify a subscription receipt
        $receiptData = 'the_base64_encoded_receipt_data';
        $receiptResponse = Subscription::appStore()->receiptData($receiptData)->verifyReceipt();

// If the subscription is an auto-renewable one,
//call the renewable() method before the trigger method verifyReceipt()
        $receiptResponse = Subscription::appStore()->receiptData($receiptData)->renewable()->verifyReceipt();

// or you can omit the renewable() method and use the verifyRenewable() method instead
        $receiptResponse = Subscription::appStore()->receiptData($receiptData)->verifyRenewable();
        $receiptResponse->getStatus()->isValid();
        dd('lab');
    });

    $api->get('maintenance/force', function () {
        return ['data' => 'ok'];
    });

});
