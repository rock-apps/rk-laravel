<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect('/home');
});
Route::get('/logout', function () {
    Auth::guard('web')->logout();
    return redirect('/');
});
Route::get('/passwordChanged', \Rockapps\RkLaravel\Http\Controllers\Auth\PasswordChangedController::class . '@index')->name('passwordChanged');
Route::get('/activation/email/{token}', \Rockapps\RkLaravel\Http\Controllers\Auth\ActivateEmailController::class . '@index');
Route::get('/home', \Rockapps\RkLaravel\Http\Controllers\HomeBaseController::class . '@index')->name('home');
Auth::routes();

$this->get('password/reset', \Rockapps\RkLaravel\Http\Controllers\Auth\ForgotPasswordController::class.'@showLinkRequestForm')->name('password.request');
$this->post('password/email', \Rockapps\RkLaravel\Http\Controllers\Auth\ForgotPasswordController::class.'@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', \Rockapps\RkLaravel\Http\Controllers\Auth\ResetPasswordController::class.'@showResetForm')->name('password.reset');
$this->post('password/reset', \Rockapps\RkLaravel\Http\Controllers\Auth\ResetPasswordController::class.'@reset')->name('password.update');

$this->get('login', \Rockapps\RkLaravel\Http\Controllers\Auth\LoginController::class.'@showLoginForm')->name('login');
$this->post('login', \Rockapps\RkLaravel\Http\Controllers\Auth\LoginController::class.'@login');
$this->post('logout', \Rockapps\RkLaravel\Http\Controllers\Auth\LoginController::class.'@logout')->name('logout');
