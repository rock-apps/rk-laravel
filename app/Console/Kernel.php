<?php

namespace App\Console;


use App;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Rockapps\RkLaravel\Console\Commands\Dev\CreateStructure;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

        /**
         * Para testes, utilizar
         * $cli = new NomeDoCli();
         * $cli->handle();
         */
//        SendScheduledPushNotification::class,
//        UpdateOngoingSubscription::class,
        CreateStructure::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->command('horizon:snapshot')->everyFiveMinutes();
//        $schedule->command('telescope:prune --hours=24')->daily();

        if (App::environment('production')) {
            $schedule->command('backup:clean')->cron('0 3,9,15,21 * * *');
            $schedule->command('backup:run')->cron('20 3,9,15,21 * * *');
            $schedule->command('backup:monitor')->daily()->at('03:00');
        }
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
