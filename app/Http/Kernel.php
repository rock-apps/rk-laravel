<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Rockapps\RkLaravel\Http\Middleware\LogRequest;
use Rockapps\RkLaravel\Http\Middleware\LogRequestAfter;
use Rockapps\RkLaravel\Http\Middleware\LogRequestBefore;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
//        \Barryvdh\Cors\HandleCors::class, // add this line to enable cors to your routes
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \Illuminate\Foundation\Http\Middleware\TrimStrings::class,
        \Rockapps\RkLaravel\Http\Middleware\SentryContext::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \Rockapps\RkLaravel\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Rockapps\RkLaravel\Http\Middleware\EncryptCookies::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings'
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \Rockapps\RkLaravel\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Dingo\Api\Http\RateLimit\Throttle\Throttle::class,
        'jwt.auth' => \Tymon\JWTAuth\Http\Middleware\Authenticate::class,
        'jwt.refresh' => \Tymon\JWTAuth\Http\Middleware\RefreshToken::class,
        'sentry' => \Rockapps\RkLaravel\Http\Middleware\SentryContext::class,
        'rk.auth' => \Rockapps\RkLaravel\Http\Middleware\Auth\Authenticate::class,
        'rk.refresh' => \Rockapps\RkLaravel\Http\Middleware\Auth\RefreshToken::class,
        'rk.maintenance' => \Rockapps\RkLaravel\Http\Middleware\MaintenanceMode::class,
        'user.suspended' => \Rockapps\RkLaravel\Http\Middleware\UserSuspended::class,
        'sentry.capture.request' => \Rockapps\RkLaravel\Http\Middleware\SentryCaptureRequest::class,
        'user.signup.token' => \Rockapps\RkLaravel\Http\Middleware\UserSuspended::class,
        'user.last.access' => \Rockapps\RkLaravel\Http\Middleware\UserLastAccessUpdate::class,
        'role' => \Rockapps\RkLaravel\Http\Middleware\Auth\Role::class,
        'permission' => \Rockapps\RkLaravel\Http\Middleware\Auth\Permission::class,
        'ability' => \Zizaco\Entrust\Middleware\EntrustAbility::class,
        'log.request.before' => LogRequestBefore::class,
        'log.request.after' => LogRequestAfter::class,
    ];
}
