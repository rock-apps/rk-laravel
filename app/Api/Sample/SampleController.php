<?php

namespace App\Api\Sample;

use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Helpers\Meta\MetaMessage;
use Rockapps\RkLaravel\Models\Banner;

class SampleController extends ControllerBase
{
    public function test()
    {
        MetaMessage::addFlash('Title', 'body', 'danger');
        MetaMessage::addPopup('Title', 'body', 'danger');
        MetaMessage::addToast('Title', 'body', 'danger');

        return $this->showAll(Banner::all());
    }
    public function testPost()
    {
        sleep(1);
        return $this->showData(collect(['teste'=>'beta']));
    }

    public function maintenanceOff()
    {
        return $this->successResponseOk();
    }
    public function maintenanceOn()
    {
        return $this->successResponseOk();
    }

}
