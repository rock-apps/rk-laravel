<?php

$state_machine =[];

foreach (glob(__DIR__ . '/../src/Traits/States/*.php') as $file) {
    $class = basename($file, '.php');
    $file = '\\Rockapps\\RkLaravel\\Traits\\States\\' . $class;

    $state_machine[strtolower(str_replace('StateTrait','',$class))] =  $file::$state_machine;

}
return $state_machine;
