<?php


return [
    'user_model' => config('rk-laravel.chat.user_model', \Rockapps\RkLaravel\Models\User::class),

    /*
     * If not set, the package will use getKeyName() on the user_model specified above
     */
    'user_model_primary_key' => 'id',

    /*
     * This will allow you to broadcast an event when a message is sent
     * Example:
     * Channel: mc-chat-conversation.2,
     * Event: Musonza\Chat\Eventing\MessageWasSent
     */
    'broadcasts' => config('rk-laravel.chat.trigger_events', true),

    /*
     * The event to fire when a message is sent
     * See Musonza\Chat\Eventing\MessageWasSent if you want to customize.
     */
    'sent_message_event' => config('rk-laravel.chat.event_chat_message_sent', \Rockapps\RkLaravel\Events\ChatMessageSent::class),

    /*
     * Automatically convert conversations with more than two users to public
     */
    'make_three_or_more_users_public' => true,

    /*
     * Specify the fields that you want to return each time for the sender.
     * If not set or empty, all the columns for the sender will be returned
     */
    'sender_fields_whitelist' => [],
];
