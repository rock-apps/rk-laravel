<?php

use Illuminate\Support\Str;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => 'local',

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => 'cdn',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'visibility' => 'public',
        ],
        'backup' => [
            'driver' => 's3',
            'key' => env('S3_BACKUP_KEY'),
            'secret' => env('S3_BACKUP_SECRET'),
            'endpoint' => env('S3_BACKUP_ENDPOINT','https://s3.docker.rockapps.com.br'),
            'region' => env('S3_BACKUP_REGION','region'),
            'bucket' => env('S3_BACKUP_BUCKET','bucket'),
            'use_path_style_endpoint' => true,
//            'root' => Str::slug(env('APP_NAME','rk-laravel')),
            'visibility' => 'public',
        ],

        'cdn' => [
            'driver' => 's3',
            'key' => env('S3_CDN_KEY'),
            'secret' => env('S3_CDN_SECRET'),
            'endpoint' => env('S3_CDN_ENDPOINT','https://s3.docker.rockapps.com.br'),
            'region' => env('S3_CDN_REGION','region'),
            'bucket' => env('S3_CDN_BUCKET','bucket'),
            'use_path_style_endpoint' => true,
//            'root' => Str::slug(env('APP_NAME','rk-laravel')),
            'visibility' => 'public',
        ],

    ],

];
