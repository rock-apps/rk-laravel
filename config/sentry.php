<?php

return array(
    'dsn' => env('SENTRY_LARAVEL_DSN'),

    // capture release as git sha
//     'release' => trim(exec('git log --pretty="%h" -n1 HEAD')),
//    'release' => '0.0.1-'.trim(exec('git log --pretty="%h" -n1 HEAD')),
    'release' => file_get_contents(app_path('../sentry.release')),

    // Capture bindings on SQL queries
    'breadcrumbs.sql_bindings' => true,

    // Capture default user context
//    'user_context' => false,
);
