<?php

namespace Rockapps\RkLaravel\Tests;

use Illuminate\Foundation\Testing\WithFaker;
use OwenIt\Auditing\AuditingServiceProvider;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\RkLaravelServiceProvider;

class UnitTestCase extends \Orchestra\Testbench\TestCase
{
    use WithFaker;
    use MigrationTestTrait;

    public function getEnvironmentSetUp($app)
    {
        $this->setupConfig($app);
        $this->setupMigration('/../..');
    }

    public function setupConfig($app)
    {
        $app['config']->set('rk-laravel', include(__DIR__ . '/../config/rk-laravel.php'));

        $state_machine = [];
        foreach (glob(__DIR__ . '/../src/Traits/States/*.php') as $file) {
            $class = basename($file, '.php');
            $file = '\\Rockapps\\RkLaravel\\Traits\\States\\' . $class;
            $state_machine[strtolower(str_replace('StateTrait', '', $class))] = $file::$state_machine;
        }
//        $state_machine['payment'] = Payment::$state_machine;
        $app['config']->set('state-machine', $state_machine);
        $app['config']->set('musonza_chat', include(__DIR__ . '/../config/musonza_chat.php'));
        $app['config']->set('filesystems', include(__DIR__ . '/../config/filesystems.php'));
        $app['config']->set('mediable', include(__DIR__ . '/../config/medialibrary.php'));
        $app['config']->set('audit', include(__DIR__ . '/../config/audit.php'));
    }

    protected function getPackageProviders($app)
    {
        return [
            RkLaravelServiceProvider::class,
            \Intervention\Image\ImageServiceProvider::class,
            \Musonza\Chat\ChatServiceProvider::class,
            \Spatie\MediaLibrary\MediaLibraryServiceProvider::class,
            \QCod\Gamify\GamifyServiceProvider::class,
            \Sebdesign\SM\ServiceProvider::class,
            \Spatie\Tags\TagsServiceProvider::class,
            \geekcom\ValidatorDocs\ValidatorProvider::class,
            \Sentry\Laravel\ServiceProvider::class,
            AuditingServiceProvider::class,
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Chat' => \Musonza\Chat\Facades\ChatFacade::class,
            'Image' => \Intervention\Image\Facades\Image::class,
            'LaravelRating' => \Nagy\LaravelRating\LaravelRatingFacade::class,
            'StateMachine' => \Sebdesign\SM\Facade::class,
            'sentry' => \Sentry\Laravel\Facade::class,
        ];
    }

    /**
     * @throws \Throwable
     */
    public function tearDown(): void
    {
        $this->beforeApplicationDestroyed(function () {
            \DB::disconnect();
        });
        parent::tearDown();
    }

}
