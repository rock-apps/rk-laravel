<?php

namespace Rockapps\RkLaravel\Tests\Api\Search;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Models\Variant;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class SearchControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\User */
    public $user_admin;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
        $this->user_admin = factory(User::class)->state(Constants::ROLE_ADMIN)->create();
    }

    public function testIndexSearch()
    {
        $this->callIndexByUri($this->user_admin, 200, '/v1/search', ['q'=>'teste'], 0);
    }

}
