<?php

namespace Rockapps\RkLaravel\Tests\Api\Chat;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Musonza\Chat\Models\Message;
use Rockapps\RkLaravel\Helpers\Chat;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Conversation;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class ChatControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_a;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_b;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user_a = factory(User::class)->create();
        $this->user_b = factory(User::class)->create();
        $this->user_c = factory(User::class)->create();

    }

    public function testGetChatRoom()
    {
        $room = Chat::createConversation("CANAL1", "Chat Canal 1", null, [$this->user_a]);

        $this->callIndexByUri($this->user_a, 200, 'v1/chats','')
            ->assertJsonStructure(['data' => [['id', 'data' => ['title', 'channel']]]]);

        $this->assertDatabaseHas('mc_conversations', ['id' => $room->id]);

        $this->callIndexByUri($this->user_a, 200, 'v1/chats/' . $room->id)
            ->assertJsonStructure(['data' => ['id', 'data' => ['title', 'channel']]]);
    }

    public function testPostChatMessage()
    {
        $room = Conversation::createConversation("CANAL1", "Chat Canal 1", null, [$this->user_a]);
        $room->addParticipants($this->user_b->id);

        app()['config']->set('rk-laravel.chat.trigger_events', true);
        app()['config']->set('musonza_chat.broadcasts', true);

        // O musonsa executaessa função de replace
        $this->expectsEvents(str_replace('\\', '.', \Rockapps\RkLaravel\Events\ChatMessageSent::class));

        $msg1 = $this->callStoreByUri($this->user_a, 201, "v1/chats/$room->id/messages", [
            'message' => 'Teste Message',
            'chat_id' => $room->id
        ])
            ->assertJsonStructure(['data' => ['id', 'message', 'user' => ['id', 'name']]])
            ->decodeResponseJson()['data'];
        $this->assertDatabaseHas('mc_messages', ['id' => $msg1['id']]);
        $this->assertDatabaseHas('mc_message_notification', ['user_id' => $this->user_b->id, 'message_id' => $msg1['id']]);


        // Teste de marcar como lida
        $this->callIndexByUri($this->user_b, 200, 'v1/chats/' . $room->id,null,null,0)
            ->assertJsonFragment(['unread' => 1])
        ;
        $this->callUpdateByUri($this->user_b, 200, "v1/chats/{$room->id}/read-all",[],0)
            ->assertJsonFragment(['unread' => 0])
        ;
    }

    public function testGetChatMessages()
    {
        $room = Chat::createConversation("CANAL1", "Chat Canal 1", null, [$this->user_a]);
        Chat::addParticipants($room->id, $this->user_b->id);

        Chat::sendMessage('Teste Mensagem 1', $this->user_a->id, $room->id);
        Chat::sendMessage('Teste Mensagem 2', $this->user_a->id, $room->id);
        Chat::sendMessage('Teste Mensagem 2', $this->user_a->id, $room->id);
        Chat::sendMessage('Teste Mensagem 2', $this->user_a->id, $room->id);
        Chat::sendMessage('Teste Mensagem 2', $this->user_a->id, $room->id);
        Chat::sendMessage('Teste Mensagem 2', $this->user_a->id, $room->id);

        $response = $this->callIndexByUri($this->user_b, 200, "v1/chats/$room->id/messages", [
            'page' => 1,
            'per_page' => 2
        ])
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);

        $json = $response->decodeResponseJson();

        $this->assertCount(2, $json['data']);

        $this->assertCount(6, Message::query()->where('conversation_id', $room->id)->get());

    }

    public function testUserNotInChat()
    {
        $room = Chat::createConversation("CANAL1", "Chat Canal 1", null, [$this->user_a]);
        Chat::addParticipants($room->id, $this->user_b->id);

        $this->callIndexByUri($this->user_c, 200, 'v1/chats',null,0)
            ->assertJsonStructure(['data' => []]);

        $this->callIndexByUri($this->user_c, 422, "v1/chats/$room->id/messages", [
            'page' => 1,
            'per_page' => 2
        ])->assertJsonFragment(["message" => "Usuário não percente a esse chat"]);

        $this->callStoreByUri($this->user_c, 422, "v1/chats/$room->id/messages", [
            'message' => 'Teste Message',
            'chat_id' => $room->id
        ])->assertJsonFragment(["message" => "Usuário não percente a esse chat"]);

    }

}
