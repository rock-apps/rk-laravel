<?php

namespace Rockapps\RkLaravel\Tests\Api\LogRequest;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\LogRequest;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class LogRequestControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(\Rockapps\RkLaravel\Models\User::class)->create(['email' => 'user@email.com', 'password' => '123456']);
    }

    public function test_log_request()
    {
        config()->set('rk-laravel.log_request.enabled', true);
        $this->callStoreByUri($this->user, 200, '/v1/sample/test', ['teste' => 1, 'teste2' => 'b'], 0);

        dump(LogRequest::first()->toArray());
        $this->assertCount(1, LogRequest::get());
    }

}
