<?php

namespace Rockapps\RkLaravel\Tests\Api\ShippingMethod;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\ShippingMethod;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class ShippingMethodControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\User */
    public $user_admin;
    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\Company */
    public $company;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_user;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null */
    public $role;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
        $this->user_admin->attachRole($this->role);

        $this->company = factory(\Rockapps\RkLaravel\Models\Company::class)->create();
        $company_role = Role::whereName(Constants::ROLE_COMPANY)->first();
        $this->user_user = factory(\Rockapps\RkLaravel\Models\User::class)->create(['company_id' => $this->company->id]);
        $this->user_user->attachRole($company_role);
    }

    public function testCreateShippingMethod()
    {
        $data = [
            'carrier' => ShippingMethod::CARRIER_FIXED,
            'fixed_value' => 10.6,
            'carrier_key' => '123456',
            'carrier_secret' => 'ABCDEF',
            'carrier_sandbox' => true,
//            'bank_account_id' => $account->id,
            'active' => true,
            'title' => 'title',
            'description' => 'description',
//            'company_id' => 'required|exists:companies,id',
            'image_upload' => Constants::LOGO_RK_B64
        ];

        $shippingMethod = $this->callStoreByUri($this->user_user, 201, '/v1/shipping-methods', $data)->decodeResponseJson()['data'];

        $this->assertDatabaseHas('media', [
            'model_type' => ShippingMethod::class,
            'model_id' => $shippingMethod['id']
        ]);
        $this->assertDatabaseHas('shipping_methods', [
            'id' => $shippingMethod['id'],
            'active' => true,
            'carrier' => ShippingMethod::CARRIER_FIXED,
            'fixed_value' => 10.6,
            'carrier_key' => '123456',
            'carrier_secret' => 'ABCDEF',
            'carrier_sandbox' => true,
        ]);

        /** @var  $shippingMethod */
        $shippingMethod = ShippingMethod::findOrFail($shippingMethod['id']);
        $this->assertEquals($shippingMethod->company_id, $this->user_user->company_id);

    }

    public function testUpdateShippingMethod()
    {
        $address = factory(\Rockapps\RkLaravel\Models\Address::class)->create([
            'related_type' => get_class($this->user_user),
            'related_id' => $this->user_user->id,
        ]);

        $shippingMethod = factory(ShippingMethod::class)->create([
            'carrier' => ShippingMethod::CARRIER_ADDRESS_PICKUP,
            'address_id' => $address->id,
            'active' => true,
            'title' => 'title',
            'description' => 'description',
            'company_id' => $this->company->id,
        ]);

        $this->callUpdateByUri($this->user_admin, 200, '/v1/shipping-methods/' . $shippingMethod->id, [
            'carrier' => ShippingMethod::CARRIER_ADDRESS_PICKUP,
            'title' => 'Novo nome',
            'active' => false
        ])->assertJsonFragment([
            'id' => $shippingMethod->id,
            'title' => 'Novo nome',
            'active' => false
        ]);

    }

    public function testDestroyShippingMethod()
    {
        $shippingMethod = factory(ShippingMethod::class)->create(['company_id' => $this->company->id]);

        $this->callDestroyByUri($this->user_admin, 200, "v1/shipping-methods/" . $shippingMethod->id)
            ->assertJsonFragment(['message' => 'ok']);

        $shippingMethod->refresh();
        $this->assertNotNull($shippingMethod->deleted_at);
    }

    public function testIndexShippingMethods()
    {
        factory(ShippingMethod::class, 2)->create([
            'company_id' => $this->company->id,
            'active' => true,
            'title' => 'Teste'
        ]);
        factory(ShippingMethod::class, 2)->create([
            'company_id' => $this->company->id,
            'active' => false,
            'title' => 'Conta',
            'carrier' => ShippingMethod::CARRIER_FIXED,
            'fixed_value' => 10.5,
        ]);

        $this->callIndexByUri($this->user_admin, 200, '/v1/shipping-methods', null, 4);
        $this->callIndexByUri($this->user_admin, 200, '/v1/shipping-methods', ['active' => true], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/shipping-methods', ['title' => 'Teste'], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/shipping-methods', ['company' => $this->company->id], 4);
        $this->callIndexByUri($this->user_admin, 200, '/v1/shipping-methods', ['carrier' => ShippingMethod::CARRIER_FIXED], 2);

        $this->callIndexByUri($this->user_admin, 200, 'v1/shipping-methods', ['page' => 1, 'per_page' => 2], 2)
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);
    }

    public function testShowShippingMethods()
    {
        $shippingMethod = factory(ShippingMethod::class)->create([
            'company_id' => $this->company->id,
            'active' => true,
            'title' => 'Teste'
        ]);

        $this->callIndexByUri($this->user_admin, 200, '/v1/shipping-methods/' . $shippingMethod->id)
            ->assertJsonStructure(['data' => [
                'id',
                'carrier',
                'address_id',
                'active',
                'image',
                'title',
                'description',
                'fixed_value',
                'company_id',
                'created_at',
                'updated_at',
                'deleted_at',
                'address',
                'company',
                'carrier_key',
                'carrier_secret',
                'carrier_sandbox',
            ]]);

    }

}
