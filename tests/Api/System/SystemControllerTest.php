<?php

namespace Rockapps\RkLaravel\Tests\Api\System;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Models\Variant;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class SystemControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\User */
    public $user_admin;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

    }

    public function testTermTOS()
    {
        $this->get( '/v1/terms/tos')
             ->assertRedirect('https://api.services.rockapps.com.br/termos-de-uso.pdf')
        ;

    }

    public function testTermPrivacy()
    {
        $this->get( '/v1/terms/privacy')
             ->assertRedirect('https://api.services.rockapps.com.br/politica-de-privacidade.pdf');

    }

}
