<?php

namespace Rockapps\RkLaravel\Tests\Api\Order;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\OrderVirtualUnit;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\Product;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class OrderVirtualUnitApiTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;


    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;
    /**
     * @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null
     */
    protected $role;
    /**
     * @var \Illuminate\Database\Eloquent\Model|mixed|User
     */
    protected $user_admin;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @throws \Exception
     */
    public function test_index_order_user()
    {
        $order = factory(OrderVirtualUnit::class)->create([
            'description' => 'teste',
            'payment_gateway' => Payment::GATEWAY_MANUAL,
            'mode' => OrderVirtualUnit::MODE_MANUAL
        ]);
        $user2 = factory(User::class)->state(Constants::ROLE_USER)->create(); /* User2 não pode ver as orders de outros users */

        $this->callIndexByUri($user2, 200, '/v1/orders-virtual-unit', null, 0, 0);
        $this->callIndexByUri($user2, 404, '/v1/orders-virtual-unit/' . $order->id, null, 0);

        $this->callIndexByUri($order->user, 200, '/v1/orders-virtual-unit', null, 1, 0);
        $this->callIndexByUri($order->user, 200, '/v1/orders-virtual-unit/' . $order->id, null, null, 0);

    }

    /**
     * @throws \Exception
     */
    public function test_index_order_admin()
    {
        $order = factory(OrderVirtualUnit::class)->create([
            'description' => 'teste',
            'payment_gateway' => Payment::GATEWAY_MANUAL,
            'mode' => OrderVirtualUnit::MODE_MANUAL
        ]);
        $user2 = factory(User::class)->state(Constants::ROLE_USER)->create(); /* User2 não pode ver as orders de outros users */
        $order = factory(OrderVirtualUnit::class)->create([
            'description' => 'teste',
            'payment_gateway' => Payment::GATEWAY_MANUAL,
            'mode' => OrderVirtualUnit::MODE_MANUAL,
            'user_id' => $user2->id
        ]);
        $admin = $this->user_admin;

        $this->callIndexByUri($admin, 200, '/v1/orders-virtual-unit', null, 2, 0);
        $this->callIndexByUri($admin, 200, '/v1/orders-virtual-unit/' . $order->id, null, null, 0);

        $this->callIndexByUri($admin, 200, '/v1/orders-virtual-unit', ['user_id' => $order->user_id], 1);
        $this->callIndexByUri($admin, 200, '/v1/orders-virtual-unit', ['user_id' => $user2->id], 1);
        $this->callIndexByUri($admin, 200, '/v1/orders-virtual-unit', ['description' => 'teste'], 2);
        $this->callIndexByUri($admin, 200, '/v1/orders-virtual-unit', ['mode' => OrderVirtualUnit::MODE_MANUAL], 2);
        $this->callIndexByUri($admin, 200, '/v1/orders-virtual-unit', ['payment_gateway' => Payment::GATEWAY_MANUAL], 2);
    }

    /**
     * @throws \Exception
     */
    public function test_order_payment_iap_apple()
    {
        /** @var User $user */
        /** @var Product $product */
        $user = $this->user;
        $company = factory(Company::class)->create();
        $product = factory(Product::class)->states('apple-100min')->create(['company_id' => $company->id]);

        $this->assertEquals(0, $user->getBalance());

        $order = $this->callStoreByUri($user, 201, '/v1/orders-virtual-unit', [
            'company' => $company->id,
            'payment_gateway' => Payment::GATEWAY_METHOD_IAP_APPLE,
            'iap_receipt_id' => env('APPLE_RECEIPIT_ID_TEMP'), // phpunit.xml
            'apple_transaction_id' => env('APPLE_TRANSACTION_ID_TEMP'), // phpunit.xml
            "product_id" => $product->id,
            "user_id" => $user->id,
            "quantity" => 1
        ], 0)->decodeResponseJson()['data'];

        $order = OrderVirtualUnit::findOrFail($order['id']);

        $this->assertEquals(OrderVirtualUnit::STATUS_CONFIRMED_PURCHASE, $order->status);
        $this->assertEquals(Payment::STATUS_PAID, $order->payment->status);

        $this->assertDatabaseHas('orders_virtual_unit', ['id' => $order->id]);

        $this->assertEquals(60000, $user->getBalance());
    }

    /**
     * @throws \Exception
     */
    public function test_store_order_manual()
    {
        /** @var User $user */
        /** @var Product $product */
        $company = factory(Company::class)->create();
        $product = factory(Product::class)->states('apple-100min')->create(['company_id' => $company->id]);

        $this->assertEquals(0, $this->user->getBalance());

        $order = $this->callStoreByUri($this->user_admin, 201, '/v1/orders-virtual-unit', [
            'status' => OrderVirtualUnit::STATUS_CONFIRMED_PURCHASE,
            'mode' => OrderVirtualUnit::MODE_MANUAL,
            'payment_gateway' => Payment::GATEWAY_MANUAL,
            "product_id" => $product->id,
            "user_id" => $this->user->id,
            "quantity" => 1
        ], 0)->decodeResponseJson()['data'];

        $order = OrderVirtualUnit::findOrFail($order['id']);

        $this->assertEquals(OrderVirtualUnit::STATUS_CONFIRMED_PURCHASE, $order->status);

        $this->assertDatabaseHas('orders_virtual_unit', ['id' => $order->id]);

        $this->assertEquals(60000, $this->user->getBalance());
    }


    /**
     * @throws \Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
        (new \RkCompanyTableSeeder())->run();
        (new \RkProductTableSeeder())->run();

        $this->role = Role::whereName(Constants::ROLE_USER)->first();
        $this->user = factory(User::class)->create(['email' => 'user@email.com', 'password' => '123456']);
        $this->user->attachRole($this->role);

        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
        $this->user_admin->attachRole($this->role);

    }
}
