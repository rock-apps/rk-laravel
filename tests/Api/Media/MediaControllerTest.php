<?php

namespace Rockapps\RkLaravel\Tests\Api\Media;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Helpers\Image;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Product;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class MediaControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $admin;
    /**
     * @var \Illuminate\Database\Eloquent\Model|mixed|\Rockapps\RkLaravel\Models\Company
     */
    public $company;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->admin = factory(User::class)->state(Constants::ROLE_ADMIN)->create();
    }

    public function testReorder()
    {
        /** @var Product $product */
        $this->company = factory(\Rockapps\RkLaravel\Models\Company::class)->create();
        $product = factory(Product::class)->create([
            'name' => 'Product teste',
            'unit_value' => 5.5,
            'active' => true,
            'type' => Product::TYPE_PRODUCT_VIRTUAL,
            'instance' => Product::INSTANCE_MAIN,
            'company_id' => $this->company->id
        ]);
        $media1 = Image::storeMediable(Constants::LOGO_RK_B64, $product);
        $media2 = Image::storeMediable(Constants::LOGO_RK_B64, $product);

        $this->assertEquals(3, $media1->order_column);
        $this->assertEquals(4, $media2->order_column);

        $this->callUpdateByUri($this->admin, 200, 'v1/medias/reorder', [
            'medias' => [
                $media2->toArray(),
                $media1->toArray(),
            ]
        ], 0);

        $this->assertEquals(2, $media1->refresh()->order_column);
        $this->assertEquals(1, $media2->refresh()->order_column);
    }

    public function testDestroyMedia()
    {
        /** @var Product $product */
        $this->company = factory(\Rockapps\RkLaravel\Models\Company::class)->create();
        $product = factory(Product::class)->create([
            'name' => 'Product teste',
            'unit_value' => 5.5,
            'active' => true,
            'type' => Product::TYPE_PRODUCT_VIRTUAL,
            'instance' => Product::INSTANCE_MAIN,
            'company_id' => $this->company->id
        ]);
        $media1 = Image::storeMediable(Constants::LOGO_RK_B64, $product);

        $this->callDestroyByUri($this->admin, 200, "v1/medias/{$media1->id}")
            ->assertOk();

        $this->assertDatabaseMissing('media',['id'=>$media1->id]);

    }


}
