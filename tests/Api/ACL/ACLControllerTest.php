<?php

namespace Rockapps\RkLaravel\Tests\Api\ACL;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class ACLControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_admin;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_user;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null */
    public $role;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
        $this->user_admin->attachRole($this->role);

        $this->user_user = factory(User::class)->create(['email' => 'user@email.com', 'password' => '123456']);

    }

    public function testAdminAddRole()
    {
        $this->callStoreByUri($this->user_admin, 201, "v1/users/" . $this->user_user->id . "/role/" . $this->role->id)
            ->assertJsonFragment(["admin" => true]);

        $this->assertTrue($this->user_user->hasRole(Constants::ROLE_ADMIN));
    }

    public function testAdminRemoveRole()
    {
        $this->user_user->attachRole($this->role);

        $this->callDestroyByUri($this->user_admin, 200, "v1/users/" . $this->user_user->id . "/role/" . $this->role->id)
            ->assertJsonFragment(["admin" => false])
        ;
        $this->assertFalse($this->user_user->hasRole(Constants::ROLE_ADMIN));

    }

}
