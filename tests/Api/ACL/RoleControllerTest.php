<?php

namespace Rockapps\RkLaravel\Tests\Api\ACL;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Permission;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class RoleControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_admin;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null */
    public $role;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create();

        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create();
        $this->user_admin->attachRole($this->role);

    }

    public function testGetRoles()
    {
        $response = $this->callIndexByUri($this->user, 200, 'v1/roles', [
            'page' => 1,
            'per_page' => 2
        ])
            ->assertJsonStructure([
                'data' => [['id', 'name', 'system', 'permissions' => [['id', 'name', 'system']]]],
                'meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]
            ]);

        $json = $response->decodeResponseJson();


        $this->assertCount(2, $json['data']);
        $this->assertCount(3, Role::all());
    }

    public function testGetSpecificRole()
    {
        $role = Role::first();
        $this->callIndexByUri($this->user, 200, "v1/roles/$role->id")
            ->assertJsonFragment(['name' => $role->name]);
    }

    public function testCreateRole()
    {
        $this->callStoreByUri($this->user_admin, 201, 'v1/roles', [
            'name' => 'Desenvolvedor',
            'description' => 'Descrição Desenvolvedor',
        ])
            ->assertJsonFragment([
                'name' => 'Desenvolvedor',
                'description' => 'Descrição Desenvolvedor',
            ]);

        $this->assertDatabaseHas('roles', [
            'name' => 'Desenvolvedor',
            'description' => 'Descrição Desenvolvedor',
        ]);
    }

    public function testUpdateRole()
    {
        $role = Role::first();
        $role->name = 'Teste';
        $this->callUpdateByUri($this->user_admin, 422, "v1/roles/{$role->id}", $role->toArray())
            ->assertJsonFragment(['message' => 'Esta função é protegida']);

        $role = Role::firstOrCreate([
            'name' => 'TESTE',
            'system' => false,
            'display_name' => "Administrador",
        ]);

        $data = $role->toArray();
        $data['display_name'] = 'ABCDEF';

        $this->callUpdateByUri($this->user_admin, 200, "v1/roles/{$role->id}", $data)
            ->assertJsonFragment(['display_name' => 'ABCDEF']);

        $this->assertDatabaseHas('roles', $data);
    }

    public function testAttachPermissionToRole()
    {
        $p1 = Permission::create(['name' => 'p1']);
        $r1 = Role::create(['name' => 'r1', 'system' => false]);

        $this->assertFalse($r1->refresh()->hasPermission($p1->name));

        $this->callStoreByUri($this->user_admin, 201, "v1/roles/{$r1->id}/permission/{$p1->id}");
        $this->assertTrue($r1->refresh()->hasPermission($p1->name));
    }

    public function testDettachPermissionToRole()
    {
        $p1 = Permission::create(['name' => 'p1']);
        $r1 = Role::create(['name' => 'r1', 'system' => false]);
        $r1->attachPermission($p1);
        $this->assertTrue($r1->refresh()->hasPermission($p1->name));

        $this->callDestroyByUri($this->user_admin, 200, "v1/roles/{$r1->id}/permission/{$p1->id}");
        $this->assertFalse($r1->refresh()->hasPermission($p1->name));
    }

    public function testDestroyRole()
    {
        $role = Role::firstOrCreate([
            'name' => 'TESTE',
            'system' => false,
            'display_name' => "Administrador",
        ]);

        $this->assertDatabaseHas('roles', $role->toArray());

        $this->callDestroyByUri($this->user_admin, 200, "v1/roles/{$role->id}")
            ->assertOk();

        $this->assertDatabaseMissing('roles', $role->toArray());
    }

}
