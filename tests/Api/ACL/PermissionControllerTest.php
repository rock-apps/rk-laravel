<?php

namespace Rockapps\RkLaravel\Tests\Api\ACL;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Permission;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class PermissionControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_admin;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null */
    public $role;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create();

        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create();
        $this->user_admin->attachRole($this->role);

    }

    public function testGetPermissions()
    {
        $response = $this->callIndexByUri($this->user, 200, 'v1/permissions', [
            'page' => 1,
            'per_page' => 2
        ])
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);

        $json = $response->decodeResponseJson();

        $this->assertCount(2, $json['data']);
        $this->assertCount(3, Permission::all());
    }

    public function testGetSpecificPermission()
    {
        $permission = Permission::first();
        $this->callIndexByUri($this->user, 200, "v1/permissions/$permission->id")
            ->assertJsonFragment(['name' => $permission->name]);
    }

    public function testCreatePermission()
    {
        $this->callStoreByUri($this->user_admin, 201, 'v1/permissions', [
            'name' => 'Desenvolvedor',
            'description' => 'Descrição Desenvolvedor',
        ])
            ->assertJsonFragment([
                'name' => 'Desenvolvedor',
                'description' => 'Descrição Desenvolvedor',
            ]);

        $this->assertDatabaseHas('permissions', [
            'name' => 'Desenvolvedor',
            'description' => 'Descrição Desenvolvedor',
        ]);
    }

    public function testUpdatePermission()
    {
        $permission = Permission::first();
        $permission->name = 'Teste';
        $this->callUpdateByUri($this->user_admin, 422, "v1/permissions/{$permission->id}", $permission->toArray())
            ->assertJsonFragment(['message' => 'Esta permissão é protegida']);

        $permission = Permission::create([
            'name' => 'TESTE',
            'system' => false,
            'display_name' => "Administrador",
        ]);

        $data = $permission->toArray();
        $data['display_name'] = 'ABCDEF';

        $this->callUpdateByUri($this->user_admin, 200, "v1/permissions/{$permission->id}", $data)
            ->assertJsonFragment(['display_name' => 'ABCDEF']);

    }

    public function testDestroyPermission()
    {
        $permission = Permission::firstOrCreate([
            'name' => 'TESTE',
            'system' => false,
            'display_name' => "Administrador",
        ]);

        $this->assertDatabaseHas('permissions', $permission->toArray());

        $this->callDestroyByUri($this->user_admin, 200, "v1/permissions/{$permission->id}")
            ->assertOk();

        $this->assertDatabaseMissing('permissions', $permission->toArray());
    }

}
