<?php

namespace Rockapps\RkLaravel\Tests\Api\Variant;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Models\Variant;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class VariantControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\User */
    public $user_admin;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_user;
    /* @var \Illuminate\Database\Eloquent\Model|Category */
    public $category;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null */
    public $role;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
        $this->user_admin = factory(User::class)->state(Constants::ROLE_ADMIN)->create();
    }

    public function testCreateVariant()
    {
        $data = [
            'value' => '#000000',
            'value_cast' => Parameter::TYPE_STRING,
            'type' => 'cor',
            'name' => 'Verde',
            'description' => 'teste'
        ];

        $this->callStoreByUri($this->user_admin, 201, '/v1/variants', $data, 0);

        $this->assertDatabaseHas('variants', [
            'value' => '#000000',
            'value_cast' => Parameter::TYPE_STRING,
            'type' => 'cor',
            'name' => 'Verde',
            'description' => 'teste',
        ]);

    }

    public function testUpdateVariant()
    {
        $variant = factory(Variant::class)->create();

        $this->callUpdateByUri($this->user_admin, 200, '/v1/variants/' . $variant->id, [
            'value' => '#000000',
            'value_cast' => Parameter::TYPE_STRING,
            'type' => 'cor',
            'name' => 'Verde',
            'description' => 'teste',
            'image_upload' => Constants::LOGO_RK_B64
        ], 0)->assertJsonFragment([
            'id' => $variant->id,
            'value' => '#000000',
            'value_cast' => Parameter::TYPE_STRING,
            'type' => 'cor',
            'name' => 'Verde',
            'description' => 'teste',
        ])->assertJsonStructure(['data'=>['image']]);

        $this->assertDatabaseHas('variants', [
            'id' => $variant->id,
            'value' => '#000000',
            'value_cast' => Parameter::TYPE_STRING,
            'type' => 'cor',
            'name' => 'Verde',
            'description' => 'teste',
        ]);
    }

    public function testDestroyVariant()
    {
        $variant = factory(Variant::class)->create();

        $this->callDestroyByUri($this->user_admin, 200, "v1/variants/" . $variant->id)
            ->assertJsonFragment(['message' => 'ok']);

        $variant->refresh();
        $this->assertNotNull($variant->deleted_at);
    }

    public function testIndexVariants()
    {
        factory(Variant::class, 2)->create(['type' => 'a', 'name' => 'Colorido']);
        factory(Variant::class, 2)->create(['type' => 'b','value' => 'AA']);

        $this->callIndexByUri($this->user_admin, 200, '/v1/variants', null, 4);
        $this->callIndexByUri($this->user_admin, 200, '/v1/variants', ['type' => 'a'], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/variants', ['name' => 'Colorido'], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/variants', ['name' => 'Colorido', 'type' => 'b'], 0);
        $this->callIndexByUri($this->user_admin, 200, '/v1/variants', ['value' => 'AA'], 2);

        $this->callIndexByUri($this->user_admin, 200, 'v1/variants', ['page' => 1, 'per_page' => 2], 2)
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);
    }

    public function testShowVariants()
    {
        $variant = factory(Variant::class)->create();

        $this->callIndexByUri($this->user_admin, 200, '/v1/variants/' . $variant->id)
            ->assertJsonStructure(['data' => [
                'id',// => (int)$variant->id,
                'name',// => (string)$variant->name,
                'type',// => (string)$variant->type,
                'value',// => $variant->getCastedValue(),
                'value_cast',// => (string)strtoupper($variant->value_cast),
                'description',// => (string)$variant->description,
                'icon',// => (string)$variant->icon,
                'color',// => (string)$variant->color,
                'image',// => (string)$variant->image,
                'medias',// => $this->transformMedias($variant)
            ]]);
    }

}
