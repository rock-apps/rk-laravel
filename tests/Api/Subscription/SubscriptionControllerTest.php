<?php

namespace Rockapps\RkLaravel\Tests\Api\Subscription;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\CreditCard;
use Rockapps\RkLaravel\Models\Plan;
use Rockapps\RkLaravel\Models\Subscription;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class SubscriptionControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;
    /**
     * @var \Illuminate\Database\Eloquent\Model|Plan
     */
    public $plan;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->state('pgm')->create(['email' => 'test@email.com', 'password' => '123456']);
        $this->plan = factory(Plan::class)->state('pgm')->create();


    }

    public function testSubscribePgmWithCrediCard()
    {
        /** @var CreditCard $card */
        $card = factory(CreditCard::class)->state('pgm')->create([
            'owner_type' => get_class($this->user),
            'owner_id' => $this->user->id,
        ]);

        $this->callStoreByUri($card->owner, 422, 'v1/subscriptions/subscribe', [
            'plan_id' => $this->plan->id,
            'credit_card_id' => $card->id,
        ])->assertJsonFragment(['message' => 'É necessário que o cliente tenha ao menos um endereço cadastrado']);

        factory(Address::class)->create([
            'related_id' => $this->user->id,
            'related_type' => get_class($this->user),
        ]);

        $json = $this->callStoreByUri($card->owner, 201, 'v1/subscriptions/subscribe', [
            'plan_id' => $this->plan->id,
            'credit_card_id' => $card->id,
        ])->decodeResponseJson();

        $subscription = Subscription::findOrFail($json['data']['id']);
        $this->assertEquals(Subscription::STATUS_PAID, $subscription->status);
    }

    public function testSubscribeWithBalance()
    {
        $this->callStoreByUri($this->user, 422, 'v1/subscriptions/subscribe-balance', [
            'plan_id' => $this->plan->id
        ])->assertJsonFragment(['message' => 'Saldo suficiente para assinar este plano']);
    }


    public function testIndexSubscriptions()
    {
        factory(Subscription::class, 2)->states('free-lifetime-active')->create([
            'subscriber_type' => get_class($this->user),
            'subscriber_id' => $this->user->id,
        ]);

        $this->callIndexByUri($this->user, 200, '/v1/subscriptions', null, 2);

        $this->callIndexByUri($this->user, 200, 'v1/subscriptions', ['page' => 1, 'per_page' => 2], 2)
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);
    }
//
//    public function testShowSubscriptions()
//    {
//        $subscription = factory(Subscription::class)->create(['active' => true, 'title' => 'Teste']);
//
//        $this->callIndexByUri($this->user, 200, '/v1/subscriptions/' . $subscription->id)
//            ->assertJsonStructure(['data' => [
//                'id',// => (int)$subscription->id,
//                'title',// => (string)$subscription->title,
//                'object_id',// => (int)$subscription->object_id,
//                'object_type',// => (string)$subscription->object_type,
//                'active',// => (boolean)$subscription->active,
//                'link_url',// => (string)$subscription->link_url,
//                'image',// => (string)$subscription->image,
//                'position',// => (string)$subscription->position,
//                'medias',// => $this->transformMedias($subscription),
//                'created_at',// => (string)$subscription->created_at,
//                'updated_at',// => (string)$subscription->updated_at,
//                'deleted_at',// => isset($subscription->deleted_at) ? (string)$subscription->deleted_at : null,
//            ]]);
//    }

}
