<?php

namespace Rockapps\RkLaravel\Tests\Api\Subscription;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\CreditCard;
use Rockapps\RkLaravel\Models\Plan;
use Rockapps\RkLaravel\Models\Subscription;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class SubscriptionIapAppleControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;
    /**
     * @var \Illuminate\Database\Eloquent\Model|Plan
     */
    public $plan;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);

    }

    public function testSubscribeWithAppleIAP()
    {
        Plan::create([
            'gateway' => Plan::GATEWAY_IAP,
            'name' => 'Plano Platinum',
            'code' => 'PLAT',
            'value' => 22.99,
            'days' => 30,
            'trial_days' => 0,
            'apple_product_id' => 'br.com.rk.laravel',
            'payment_method' => 'boleto,credit_card',
            'color' => '#000000',
            'charges' => null,
            'installments' => 1,
            'active' => true,
            'invoice_reminder' => 4,
        ]);

        $iap_receipt_id_subscription = 'MII...';
        $apple_transaction_id = '1000000700000';

        $this->callStoreByUri($this->user, 422, 'v1/subscriptions/subscribe-iap-ios', [
            'iap_receipt_id' => $iap_receipt_id_subscription,
            'apple_transaction_id' => $apple_transaction_id,
        ])->assertJsonFragment(['message' => 'APPLE_IAP_SECRET não definido']);



//         $json = $this->callStoreByUri($this->user, 201, 'v1/subscriptions/subscribe-iap-ios', [
//            'iap_receipt_id' => $iap_receipt_id_subscription,
//            'apple_transaction_id' => $apple_transaction_id,
//        ])->decodeResponseJson();
//
//        /** @var Subscription $subscription */
//        $subscription = Subscription::findOrFail($json['data']['id']);
//        $this->assertEquals(Subscription::STATUS_ENDED, $subscription->status); // Essa assinatura é antiga e já foi finalizada
//        $this->assertEquals($apple_transaction_id, $subscription->apple_transaction_id);

    }

}
