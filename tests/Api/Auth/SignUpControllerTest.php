<?php

namespace Rockapps\RkLaravel\Tests\Api\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class SignUpControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create(['email' => 'test@emailemail.com', 'password' => '123456']);
    }

    public function testSignUpSuccessfully()
    {
        $role = 'estoque';
        Role::create(['name' => $role]);
        app()['config']->set('rk-laravel.sign_up.allowed_roles', [$role]);

        $this->post("v1/auth/signup/$role", [
            'name' => 'Test User',
            'email' => 'test@email.com',
            'password' => '123456'
        ])->assertJson([
            'status' => 'ok'
        ])->assertStatus(201)
            ->assertJsonFragment(['email' => 'test@email.com']);

        $user = User::whereEmail('test@email.com')->first();
        $this->assertNotNull($user->verification_token);
        $this->assertFalse($user->verified);

        $this->get("activation/email/{$user->verification_token}")
            ->assertSuccessful();
        $user = $user->fresh();
        $this->assertTrue($user->verified);

    }

    public function testSignUpReturnsInvalidRoleError()
    {
        $this->callStoreByUri(null, 422, "v1/auth/signup/xxxxxxx", [
            'name' => 'Teste Prestador',
            'email' => 'teste@teste.com',
            'password' => '12345678'
        ])
            ->assertJsonFragment(['message' => 'Não é permitido se cadastrar com esse perfil (xxxxxxx)']);
    }

    public function testSignUpReturnsValidationError()
    {
        $this->callStoreByUri(null, 422, "v1/auth/signup/user", [
            'name' => 'Teste Prestadora',
            'email' => 'teste@testeb.com',
        ])->assertJsonStructure([
            'error'
        ]);
    }

    public function testSignUpWithInvitationCodeValid()
    {
        /** @var User $user */
        $user = factory(User::class)->state(Constants::ROLE_USER)->create();

        $this->assertNotNull($user->invitation_code);

        $this->post('v1/auth/signup/user', [
            'name' => 'Test User',
            'email' => 'test@email.com',
            'password' => 'password',
            'invitation_code' => $user->invitation_code
        ])->assertJsonFragment([
            'invited_by' => $user->id
        ]);
    }

    public function testSignUpWithInvitationCodeInvalid()
    {
        /** @var User $user */

        $this->post('v1/auth/signup/user', [
            'name' => 'Test User',
            'email' => 'test@email.com',
            'password' => 'password',
            'invitation_code' => 'AAA-1234'
        ])->assertJsonFragment([
            'message' => 'Código de indicação inválido'
        ]);
    }
}
