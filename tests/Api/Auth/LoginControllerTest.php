<?php

namespace Rockapps\RkLaravel\Tests\Api\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class LoginControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
    }

    public function testLoginSuccessfully()
    {
        $this->callStoreByUri($this->user, 200, 'v1/auth/login', [
            'email' => 'test@email.com',
            'password' => '123456'
        ])->assertJson([
            'status' => 'ok'
        ])->assertJsonStructure([
            'status',
            'token',
            'user',
            'expires_in'
        ])->isOk();
    }

    public function testLoginSuspendedError()
    {
        factory(User::class)->create([
            'name' => $this->faker->name,
            'email' => 'a@a.com',
            'password' => '123456',
            'suspended' => 1
        ]);

        $this->post('v1/auth/login', [
            'email' => 'a@a.com',
            'password' => '123456',
        ])
//            ->assertJsonFragment(['message' => 'O usu\u00e1rio est\u00e1 em an\u00e1lise. Entre em contato com nosso suporte.'])
//            ->assertJsonFragment(['message' => 'User is suspended. Please, contact our support.'])
            ->assertStatus(403);
    }

    public function testLoginWithReturnsWrongCredentialsError()
    {
        $this->post('v1/auth/login', [
            'email' => 'unknown@email.com',
            'password' => '123456'
        ])->assertJsonStructure([
            'error'
        ])->assertStatus(403);
    }

    public function testLoginWithReturnsValidationError()
    {
        $this->post('v1/auth/login', [
            'email' => 'test@email.com'
        ])->assertJsonStructure([
            'error'
        ])->assertStatus(422);
    }

    public function testLoginWithEmail()
    {
        $this->post('v1/auth/send-email-token', ['email' => 'test@email.com'])
            ->assertJsonFragment(['status' => 'ok'])->assertStatus(200);
        $this->user->refresh();

        $this->assertNotNull($this->user->signup_token);
        $this->post('v1/auth/login-email-token', [
            'email' => 'test@email.com',
            'signup_token' => $this->user->signup_token
        ])
            ->assertJsonStructure(['user', 'token'])->assertStatus(200);

        $this->user->refresh();
        $this->assertNull($this->user->signup_token);
    }

    public function testLoginImpersonate()
    {
        app()['config']->set('rk-laravel.impersonate.enabled', false);

        $this->callStoreByUri($this->user, 403, 'v1/auth/login', [
            'email' => 'test@email.com',
            'password' => 'abcdefghj'
        ]);

        // Ativa a config, mas não define o password
        app()['config']->set('rk-laravel.impersonate.enabled', true);

        $this->callStoreByUri($this->user, 403, 'v1/auth/login', [
            'email' => 'test@email.com',
            'password' => '1234567890'
        ]);

        // Define o password
        app()['config']->set('rk-laravel.impersonate.password', '1234567890');

        $this->callStoreByUri($this->user, 200, 'v1/auth/login', [
            'email' => 'test@email.com',
            'password' => '1234567890'
        ])->assertJson([
            'status' => 'ok'
        ])->assertJsonStructure([
            'status',
            'token',
            'user',
            'expires_in'
        ])->isOk();

    }
}
