<?php

namespace Rockapps\RkLaravel\Tests\Api\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class RefreshControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /**
     * @var \Illuminate\Database\Eloquent\Model|mixed|User
     */
    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
    }

    public function testRefresh()
    {
        $this->callStoreByUri($this->user, 200, 'v1/auth/refresh')
            ->assertJsonStructure([
                'status',
                'token',
                'expires_in'
            ])->isOk();
    }

    public function testRefreshWithError()
    {
        $response = $this->post('v1/auth/refresh', [], [
            'Authorization' => 'Bearer Wrong'
        ]);

        $response->assertStatus(401);
    }
}
