<?php

namespace Rockapps\RkLaravel\Tests\Api\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Notifications\UserResetPassword;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class ForgotPasswordControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
    }

    public function testForgotPasswordRecoverySuccessfully()
    {
        $this->expectsNotification($this->user,UserResetPassword::class);
        $this->post('v1/auth/recovery', [
            'email' => 'test@email.com'
        ])->assertJson([
            'status' => 'ok'
        ])->isOk();
    }

    public function testForgotPasswordRecoveryReturnsUserNotFoundError()
    {
        $this->post('v1/auth/recovery', [
            'email' => 'unknown@email.com',
        ])->assertJsonStructure([
            'error',
        ])->assertStatus(422);
    }

    public function testForgotPasswordRecoveryReturnsValidationErrors()
    {
        $this->post('v1/auth/recovery', [
            'email' => 'i am not an email',
        ])->assertJsonStructure([
            'error',
        ])->assertStatus(422);
    }
}
