<?php

namespace Rockapps\RkLaravel\Tests\Api\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class LogoutControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
    }

    public function testLogout()
    {
        $user = factory(User::class)->create(['email' => 'teste@email.com']);

        $this->callStoreByUri($user, 200, "v1/auth/logout")
            ->assertJsonFragment(["message" => "Successfully logged out"]);
    }
}
