<?php

namespace Rockapps\RkLaravel\Tests\Api\Maintenance;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class MaintenanceControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');
    }

    public function testMaintenance()
    {
        $this->callIndexByUri(null, 200, 'v1/maintenance/off', null, null, 0);

        Parameter::create(['key' => Constants::MAINTENANCE_MODE, 'value_bool' => true, 'type' => Parameter::TYPE_BOOL]);

        $this->callIndexByUri(null, 422, 'v1/maintenance/on')
            ->assertJsonFragment(['message' => 'Desculpe, estamos em manutenção.']);
    }


}
