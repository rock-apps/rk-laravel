<?php

namespace Rockapps\RkLaravel\Tests\Api\Voucher;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Events\VoucherRedeemed;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Product;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Models\Voucher;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class VoucherControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\User */
    public $user_admin;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_user;
    /* @var \Illuminate\Database\Eloquent\Model|Category */
    public $category;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null */
    public $role;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
        $this->user_admin->attachRole($this->role);

        $this->user_user = factory(\Rockapps\RkLaravel\Models\User::class)->create(['email' => 'user@email.com', 'password' => '123456']);
        $this->category = factory(\Rockapps\RkLaravel\Models\Category::class)->create();

    }

    public function testCreateVoucher()
    {
        $company = factory(Company::class)->create();
        $product = factory(\Rockapps\RkLaravel\Models\Product::class)->create([
            'company_id' => $company->id,
        ]);
        $data = [
            'model_type' => Product::class,
            'model_id' => $product->id,
            'max_redeem_qty' => 2,
            'company_id' => $company->id,
        ];

        $voucher = $this->callStoreByUri($this->user_admin, 201, '/v1/vouchers', $data)->decodeResponseJson()['data'];

        $this->assertDatabaseHas('vouchers', ['id' => $voucher['id']]);

    }

    public function testRedeemVoucher()
    {
        $this->expectsEvents([VoucherRedeemed::class]);

        $company = factory(Company::class)->create();
        $product = factory(\Rockapps\RkLaravel\Models\Product::class)->create([
            'company_id' => $company->id,
        ]);
        $voucher = factory(Voucher::class)->create([
            'model_type' => Product::class,
            'model_id' => $product->id,
            'max_redeem_qty' => 2,
            'company_id' => $company->id,
        ]);

        $this->callStoreByUri($this->user_admin, 200, '/v1/vouchers/redeemed/' . $voucher->code, []);

        $this->assertEquals(1, $voucher->availableQty());
    }

    public function testCreateMultipleVoucher()
    {
        $company = factory(Company::class)->create();
        $product = factory(\Rockapps\RkLaravel\Models\Product::class)->create([
            'company_id' => $company->id,
        ]);
        $data = [
            'model_type' => Product::class,
            'model_id' => $product->id,
            'max_redeem_qty' => 2,
            'company_id' => $company->id,
            'replicate' => 3,
        ];

        $this->callStoreByUri($this->user_admin, 201, '/v1/vouchers', $data)->decodeResponseJson();

        $this->assertEquals(3, Voucher::count());

    }

    public function testUpdateVoucher()
    {
        $company = factory(Company::class)->create();
        $product = factory(\Rockapps\RkLaravel\Models\Product::class)->create([
            'company_id' => $company->id,
        ]);
        $voucher = factory(Voucher::class)->create([
            'model_type' => Product::class,
            'model_id' => $product->id,
            'max_redeem_qty' => 2,
            'company_id' => $company->id,
        ]);

        $this->callUpdateByUri($this->user_admin, 200, '/v1/vouchers/' . $voucher->id, [
            'max_redeem_qty' => 10,
            'code' => $voucher->code,
//            'code' => 'ABC',
            'model_type' => Product::class,
            'model_id' => $product->id,
        ])->assertJsonFragment([
            'max_redeem_qty' => 10,
            'code' => $voucher->code,
            'model_type' => Product::class,
            'model_id' => $product->id,
        ]);

        $this->assertDatabaseHas('vouchers', ['id' => $voucher->id,
            'max_redeem_qty' => 10,
            'code' => $voucher->code,
            'model_type' => Product::class,
            'model_id' => $product->id,
        ]);


        // Testar código único
        $voucher2 = factory(Voucher::class)->create([
            'max_redeem_qty' => 10,
            'model_type' => Product::class,
            'model_id' => $product->id,
        ]);
        $this->callUpdateByUri($this->user_admin, 422, '/v1/vouchers/' . $voucher2->id, [
            'code' => $voucher->code,
            'max_redeem_qty' => 10,
            'model_type' => Product::class,
            'model_id' => $product->id,
        ])->assertJsonFragment([
            'message' => 'Este código já está sendo utilizado'
        ]);

        // Testar alteração de código com sucesso
        $this->callUpdateByUri($this->user_admin, 200, '/v1/vouchers/' . $voucher2->id, [
            'code' => 'AAAA-1234',
            'max_redeem_qty' => 10,
            'model_type' => Product::class,
            'model_id' => $product->id,
        ])->assertJsonFragment([
            'code' => 'AAAA-1234',
            'max_redeem_qty' => 10,
            'model_type' => Product::class,
            'model_id' => $product->id,
        ]);
    }

    public function testIndexVouchers()
    {
        $company = factory(Company::class)->create();
        $product = factory(\Rockapps\RkLaravel\Models\Product::class)->create([
            'company_id' => $company->id,
        ]);
        factory(Voucher::class, 2)->create([
            'model_type' => Product::class,
            'model_id' => $product->id,
            'max_redeem_qty' => 2,
            'company_id' => $company->id,
        ]);
        factory(Voucher::class)->create([
            'model_type' => Product::class,
            'model_id' => $product->id,
            'max_redeem_qty' => 2,
            'company_id' => $company->id,
            'code' => 'Teste',
            'expires_at' => '2020-01-01',
        ]);

        $this->callIndexByUri($this->user_admin, 200, '/v1/vouchers', null, 3);
        $this->callIndexByUri($this->user_admin, 200, '/v1/vouchers', ['company' => $company->id], 3);
        $this->callIndexByUri($this->user_admin, 200, '/v1/vouchers', ['code' => 'Teste'], 1);
        $this->callIndexByUri($this->user_admin, 200, '/v1/vouchers', ['expires_at' => '2020-01-01'], 1);

        $this->callIndexByUri($this->user_admin, 200, 'v1/vouchers', ['page' => 1, 'per_page' => 2], 2)
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);
    }


    public function testShowVouchers()
    {
        $company = factory(Company::class)->create();
        $product = factory(\Rockapps\RkLaravel\Models\Product::class)->create([
            'company_id' => $company->id,
        ]);
        $voucher = factory(Voucher::class)->create([
            'model_type' => Product::class,
            'model_id' => $product->id,
            'max_redeem_qty' => 2,
            'company_id' => $company->id,
        ]);

        $this->callIndexByUri($this->user_admin, 200, '/v1/vouchers/' . $voucher->id)
            ->assertJsonStructure(['data' => [
                'id',// => (int)$voucher->id,
                'code',// => (string)$voucher->code,
                'model_type',// => (string)$voucher->model_type,
                'model_id',// => (int)$voucher->model_id,
                'data',// => (string)$voucher->data,
                'expires_at',// => (string)$voucher->expires_at,
                'created_at',// => (string)$voucher->created_at,
                'updated_at',// => (string)$voucher->updated_at,
                'deleted_at',// => isset($voucher->deleted_at) ? (string)$voucher->deleted_at : null,
                'available_qty',//] = $voucher->availableQty();
                'max_redeem_qty',//] = $voucher->max_redeem_qty;
                'company_id',//] = $voucher->company_id;
                'company',//] = $voucher->company ? $voucher->company->transform(CompanySingleTransformer::class) : null;
                'campaign',//] = $voucher->campaign;

            ]]);
    }

}
