<?php

namespace Rockapps\RkLaravel\Tests\Api\CustomAttribute;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\CustomAttribute;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class CustomAttributeControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\User */
    public $user_admin;
    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\Company */
    public $company;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_user;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null */
    public $role;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
        $this->user_admin->attachRole($this->role);

        $this->company = factory(\Rockapps\RkLaravel\Models\Company::class)->create();
        $company_role = Role::whereName(Constants::ROLE_COMPANY)->first();
        $this->user_user = factory(\Rockapps\RkLaravel\Models\User::class)->create(['company_id' => $this->company->id]);
        $this->user_user->attachRole($company_role);
    }

    public function testCreateCustomAttribute()
    {
        $data = [
            'object_type' => get_class($this->user_user),
            'object_id' => $this->user_user->id,
            'cast' => CustomAttribute::CAST_INT,
            'value' => '123456',
            'field' => 'height',
        ];

        $customAttribute = $this->callStoreByUri($this->user_user, 201, '/v1/custom-attributes', $data)->decodeResponseJson()['data'];

        $this->assertDatabaseHas('custom_attributes', [
            'id' => $customAttribute['id'],
            'object_type' => get_class($this->user_user),
            'object_id' => $this->user_user->id,
            'cast' => CustomAttribute::CAST_INT,
            'value' => '123456',
            'field' => 'height',
        ]);

        /** @var  $customAttribute */
        $customAttribute = CustomAttribute::findOrFail($customAttribute['id']);
        $this->assertEquals($customAttribute->object_id, $this->user_user->id);

    }

    public function testUpdateCustomAttribute()
    {
        $customAttribute = factory(CustomAttribute::class)->create([
            'object_type' => get_class($this->user_user),
            'object_id' => $this->user_user->id,
            'cast' => CustomAttribute::CAST_INT,
            'value' => '123456',
            'field' => 'height',
        ]);

        $this->callUpdateByUri($this->user_admin, 200, '/v1/custom-attributes/' . $customAttribute->id, [
            'object_type' => get_class($this->user_user),
            'object_id' => $this->user_user->id,
            'cast' => CustomAttribute::CAST_INT,
            'value' => '1234561234',
            'field' => 'height',
        ])->assertJsonFragment([
            'id' => $customAttribute->id,
            'value' => '1234561234',
        ]);

    }

    public function testDestroyCustomAttribute()
    {
        $customAttribute = factory(CustomAttribute::class)->create([
            'object_type' => get_class($this->user_user),
            'object_id' => $this->user_user->id,
            'cast' => CustomAttribute::CAST_INT,
            'value' => '1234561234',
            'field' => 'height',
        ]);

        $this->callDestroyByUri($this->user_admin, 200, "v1/custom-attributes/" . $customAttribute->id)
            ->assertJsonFragment(['message' => 'ok']);

        $customAttribute->refresh();
        $this->assertNotNull($customAttribute->deleted_at);
    }

    public function testIndexCustomAttributes()
    {
        factory(CustomAttribute::class, 2)->create([
            'object_type' => get_class($this->user_user),
            'object_id' => $this->user_user->id,
            'field' => 'height',
        ]);
        factory(CustomAttribute::class, 2)->create([
            'object_type' => get_class($this->company),
            'object_id' => $this->company->id,
            'field' => 'width',
        ]);

        $this->callIndexByUri($this->user_admin, 200, '/v1/custom-attributes', null, 4);
        $this->callIndexByUri($this->user_admin, 200, '/v1/custom-attributes', ['field' => 'height'], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/custom-attributes', ['object_id' => $this->company->id], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/custom-attributes', ['object_type' => get_class($this->company)], 2);

        $this->callIndexByUri($this->user_admin, 200, 'v1/custom-attributes', ['page' => 1, 'per_page' => 2], 2)
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);
    }

    public function testShowCustomAttributes()
    {
        $customAttribute = factory(CustomAttribute::class)->create([
            'object_type' => get_class($this->user_user),
            'object_id' => $this->user_user->id,
        ]);

        $this->callIndexByUri($this->user_admin, 200, '/v1/custom-attributes/' . $customAttribute->id)
            ->assertJsonStructure(['data' => [
                'id',
                'object_type',
                'object_id',
                'field',
                'cast',
                'value',
                'created_at',
                'updated_at',
                'deleted_at',
            ]]);

    }

}
