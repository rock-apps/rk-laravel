<?php

namespace Rockapps\RkLaravel\Tests\Api\Campaign;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Campaign;
use Rockapps\RkLaravel\Models\CampaignElement;
use Rockapps\RkLaravel\Models\CampaignHit;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class CampaignApiTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $admin;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;
    /* @var \Illuminate\Database\Eloquent\Model|Category */
    public $category;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration("/../..");

        (new \RkRoleTableSeeder())->run();

        $company = Company::factory();
        $this->admin = factory(User::class)->create();
        $this->admin->company_id = $company->id;
        $this->admin->save();
        $this->admin->attachRole(Role::findOrFailByName(Constants::ROLE_ADMIN));

        $this->user = factory(User::class)->create();
        $this->user->attachRole(Role::findOrFailByName(Constants::ROLE_USER));

        $this->category = factory(Category::class)->create();
    }

    public function testAdminCreateCampaign()
    {
        $data = [
            'active' => true,
            'name' => 'teste',
            'max_views' => 100,
            'max_clicks' => 50,
            'url' => 'https://www.google.com',
            'elements' => CampaignElement::factory([], 2, [], false)
        ];

        $campaign = $this->callStoreByUri($this->admin, 201, "/v1/campaigns", $data, 0)
            ->decodeResponseJson()['data'];

        $this->assertDatabaseHas('campaigns', [
            'id' => $campaign['id'],
            'active' => true,
            'name' => 'teste',
            'max_views' => 100,
            'max_clicks' => 50,
        ]);

        $campaign = Campaign::findOrFail($campaign['id']);

        $this->assertCount(2, $campaign->elements);

    }

    public function testAdminUpdateCampaign()
    {
        /** @var Campaign campaign */
        $campaign = Campaign::factory(['company_id' => $this->admin->company_id], 1, ['with-elements']);

        $this->assertCount(10, $campaign->elements()->get());

        $e = CampaignElement::factory([], 2, [], false)->toArray();
        $e[] = $campaign->elements[0]->toArray();

        // Um elemento será excluído e dois adicionados
        $this->callUpdateByUri($this->admin, 200, '/v1/campaigns/' . $campaign->id, [
            'active' => true,
            'name' => 'teste',
            'max_views' => 100,
            'max_clicks' => 50,
            'elements' => $e
        ], 0)->assertJsonFragment([
            'active' => true,
            'name' => 'teste',
            'max_views' => 100,
            'max_clicks' => 50,
        ]);

        $this->assertDatabaseHas('campaigns', [
            'id' => $campaign->id,
            'active' => true,
            'name' => 'teste',
            'max_views' => 100,
            'max_clicks' => 50,
        ]);

        $this->assertCount(3, $campaign->elements()->get());
    }

    public function testAdminDestroyCampaign()
    {
        /** @var Campaign campaign */
        $campaign = Campaign::factory(['company_id' => $this->admin->company_id]);

        $this->callDestroyByUri($this->admin, 200, '/v1/campaigns/' . $campaign->id)
            ->assertJsonFragment(['message' => 'ok']);

        $campaign->refresh();
        $this->assertNotNull($campaign->deleted_at);
    }

    public function testAdminIndexCampaigns()
    {
        /** @var User $admin2 */
        $admin2 = factory(User::class)->create();
        $admin2->company_id = Company::factory()->id;
        $admin2->save();
        $admin2->attachRole(Role::findOrFailByName(Constants::ROLE_ADMIN));

        Campaign::factory(['active' => true, 'company_id' => $admin2->company_id, 'name' => 'Teste'], 2);
        Campaign::factory(['active' => false, 'company_id' => $admin2->company_id, 'name' => 'Beta'], 2);
        Campaign::factory(['active' => true, 'company_id' => $this->admin->company_id, 'name' => 'Teste'], 2);
        Campaign::factory(['active' => false, 'company_id' => $this->admin->company_id, 'name' => 'Beta'], 2);

        $this->callIndexByUri($this->admin, 200, '/v1/campaigns', null, 4);
        $this->callIndexByUri($admin2, 200, '/v1/campaigns', null, 4);
        $this->callIndexByUri($this->admin, 200, '/v1/campaigns', ['active' => true], 2);
        $this->callIndexByUri($this->admin, 200, '/v1/campaigns', ['name' => 'Teste'], 2);
        $this->callIndexByUri($this->admin, 200, '/v1/campaigns', ['name' => 'Beta', 'active' => false], 0);

        $this->callIndexByUri($this->admin, 200, 'v1/campaigns', ['page' => 1, 'per_page' => 2], 2)
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);
    }

    public function testAdminShowCampaigns()
    {
        $campaign = Campaign::factory(['company_id' => $this->admin->company_id]);

        $this->callIndexByUri($this->admin, 200, '/v1/campaigns/' . $campaign->id)->assertJsonStructure(['data' => [
            'id',// => (int)campaign->id,
            'name',// => (string)campaign->title,
        ]]);
    }

    public function testUserHitCampaigns()
    {
        /** @var Campaign $campaign */
        $campaign = Campaign::factory([
            'company_id' => $this->admin->company_id,
            'url' => 'https://www.google.com'
        ], 1, ['with-elements']);

        $this->assertEquals(0, $campaign->hits()->count());

        $data = $this->callIndexByUri($this->admin, 200, '/v1/campaigns/action/view')->assertJsonStructure(['data' => [
            'url_click',
        ]])->decodeResponseJson('data');

        $this->assertEquals(1, $campaign->hits()->where('method', CampaignHit::METHOD_VIEW)->count());

        $this->get($data['url_click'], $this->createAuthHeader($this->admin))
            ->assertRedirect('https://www.google.com');

        $this->assertEquals(1, $campaign->hits()->where('method', CampaignHit::METHOD_CLICK)->count());
        $this->assertEquals(2, $campaign->hits()->count());

    }

    public function testUserFilterCampaigns()
    {
        /** @var Campaign $campaign */
        $campaign = Campaign::factory([
            'company_id' => $this->admin->company_id,
            'url' => 'https://www.google.com'
        ], 1, ['with-elements']);

        $campaign->elements[0]->element->position = 'teste';
        $campaign->elements[0]->element->save();

        $this->assertEquals(0, $campaign->hits()->count());

        $this->callIndexByUri($this->admin, 422, '/v1/campaigns/action/view', [
            'banner_position' => 'NAOEXISTENTE'
        ], null, 1);

        $data = $this->callIndexByUri($this->admin, 200, '/v1/campaigns/action/view', [
            'banner_position' => 'teste'
        ])->assertJsonStructure(['data' => [
            'url_click',
        ]])->decodeResponseJson('data');

        $this->assertEquals(1, $campaign->hits()->where('method', CampaignHit::METHOD_VIEW)->count());

        $this->get($data['url_click'], $this->createAuthHeader($this->admin))
            ->assertRedirect('https://www.google.com');

        $this->assertEquals(1, $campaign->hits()->where('method', CampaignHit::METHOD_CLICK)->count());
        $this->assertEquals(2, $campaign->hits()->count());

    }

}


