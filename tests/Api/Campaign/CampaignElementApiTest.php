<?php

namespace Rockapps\RkLaravel\Tests\Api\Campaign;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Banner;
use Rockapps\RkLaravel\Models\Campaign;
use Rockapps\RkLaravel\Models\CampaignElement;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class CampaignElementApiTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $admin;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;
    /* @var \Illuminate\Database\Eloquent\Model|Campaign */
    public $campaign;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration("/../..");

        (new \RkRoleTableSeeder())->run();

        $company = Company::factory();
        $this->admin = factory(User::class)->create();
        $this->admin->company_id = $company->id;
        $this->admin->save();
        $this->admin->attachRole(Role::findOrFailByName(Constants::ROLE_ADMIN));

        $this->campaign = Campaign::factory(['company_id' => $company->id]);
    }

    public function testAdminCreateCampaignElement()
    {
        $data = CampaignElement::factory(['campaign_id' => $this->campaign->id], 1, [], 0)->toArray();

        $campaign_element = $this->callStoreByUri($this->admin, 201, "/v1/campaign-elements", $data, 0)
            ->decodeResponseJson()['data'];

        $this->assertDatabaseHas('campaigns', [
            'id' => $campaign_element['id'],
        ]);

        $campaign = Campaign::findOrFail($campaign_element['campaign_id']);

        $this->assertCount(1, $campaign->elements);
    }

    public function testAdminUpdateCampaignElement()
    {
        /** @var CampaignElement $campaign_element */
        $campaign_element = CampaignElement::factory(['campaign_id' => $this->campaign->id, 'active' => false]);
        $banner = Banner::factory();

        $this->callUpdateByUri($this->admin, 200, '/v1/campaign-elements/' . $campaign_element->id, [
            'active' => false,
            'element_type' => Banner::class,
            'element_id' => $banner->id,
            'campaign_id' => $campaign_element->campaign_id,
        ], 0)->assertJsonFragment([
            'active' => false,
        ]);

        $this->assertDatabaseHas('campaign_elements', [
            'id' => $campaign_element->id,
            'active' => false,
        ]);
    }

    public function testAdminDestroyCampaignElement()
    {
        /** @var CampaignElement campaign */
        $campaign_element = CampaignElement::factory(['campaign_id' => $this->campaign->id]);

        $this->callDestroyByUri($this->admin, 200, '/v1/campaign-elements/' . $campaign_element->id)
            ->assertJsonFragment(['message' => 'ok']);

        $campaign_element->refresh();
        $this->assertNotNull($campaign_element->deleted_at);
    }

    public function testAdminShowCampaignElements()
    {
        $campaign_element = CampaignElement::factory(['campaign_id' => $this->campaign->id]);

        $this->callIndexByUri($this->admin, 200, '/v1/campaign-elements/' . $campaign_element->id)->assertJsonStructure(['data' => [
            'id',// => (int)campaign->id,
        ]]);
    }

    public function tearDown(): void
    {
        // do not remove this function
    }

}


