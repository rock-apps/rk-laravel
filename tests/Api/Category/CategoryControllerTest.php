<?php

namespace Rockapps\RkLaravel\Tests\Api\Category;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\BankAccount;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Plan;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class CategoryControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_admin;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null */
    public $role;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create();

        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create();
        $this->user_admin->attachRole($this->role);

        factory(Category::class, 5)->create();
    }

    public function testGetCategories()
    {
        $response = $this->callIndexByUri($this->user, 200, 'v1/categories', [
            'page' => 1,
            'per_page' => 2
        ])
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);

        $json = $response->decodeResponseJson();

        $this->assertCount(2, $json['data']);
        $this->assertCount(5, Category::all());

        $response = $this->callIndexByUri($this->user, 200, 'v1/categories', ['id' => Category::first()->id],1);

        $json = $response->decodeResponseJson();

    }

    public function testGetCategoriesTypes()
    {
        factory(Category::class)->create(['model_type' => User::class]);
        factory(Category::class)->create(['model_type' => BankAccount::class]);
        factory(Category::class)->create(['model_type' => Plan::class]);

        $response = $this->callIndexByUri($this->user, 200, 'v1/categories/types')
        ;

        $json = $response->decodeResponseJson();

        $this->assertCount(3, $json['data']);
    }

    public function testGetSpecificCategory()
    {
        $category = Category::first();
        $this->callIndexByUri($this->user, 200, "v1/categories/$category->id")
            ->assertJsonFragment(['name' => $category->name]);
    }

    public function testCreateCategory()
    {
        $this->callStoreByUri($this->user_admin, 201, 'v1/categories', [
            'name' => 'YEAH',
            'slug' => null,
            'headline' => $this->faker->sentence(6),
            'description' => $this->faker->paragraph(),
            'icon' => null,
            'active' => true,
//        'index' => null,
            'model_type' => \Rockapps\RkLaravel\Models\User::class,
        ])
            ->assertJsonFragment([
                'name' => 'YEAH',
                'active' => true,
            ]);

        $this->assertDatabaseHas('categories', [
            'name' => 'YEAH',
            'active' => true,
        ]);
    }

    public function testUpdateCategory()
    {
        $category = Category::first();
        $category->name = 'Teste';
        $this->callUpdateByUri($this->user_admin, 200, "v1/categories/{$category->id}", $category->toArray())
            ->assertJsonFragment(['name' => 'Teste']);

        $this->assertDatabaseHas('categories', ['id' => $category['id'],'name'=>'Teste']);
    }

    public function testDestroyCategory()
    {
        $category = Category::first();

        $this->callDestroyByUri($this->user_admin, 200, "v1/categories/{$category->id}")
            ->assertOk();

        $category->refresh();
        $this->assertNotNull($category->deleted_at);

    }

}
