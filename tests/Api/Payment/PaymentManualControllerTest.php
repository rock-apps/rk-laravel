<?php

namespace Rockapps\RkLaravel\Tests\Api\Payment;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class PaymentManualControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;
    /**
     * @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null
     */
    protected $role;
    /**
     * @var \Illuminate\Database\Eloquent\Model|mixed|User
     */
    protected $user_admin;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
        $this->user_admin->attachRole($this->role);
    }

    /** @test */
    function paymentBankTransferChangeState()
    {
        /**
         * Esse payment provavelmente será criado no mesmo momento que uma Order
         * @var Payment $payment
         */
        $payment = factory(Payment::class)->state('bank')->create();
        $payment->charge('Teste Bank');
        $this->assertFalse($payment->isPaid());
        $this->assertEquals(Payment::STATUS_NOT_STARTED, $payment->status);

        $this->callUpdateByUri($this->user_admin, 200, 'v1/payments/' . $payment->id . '/status/' . Payment::STATUS_PAID, []);

        $payment->refresh();

        $this->assertTrue($payment->isPaid());

    }

}
