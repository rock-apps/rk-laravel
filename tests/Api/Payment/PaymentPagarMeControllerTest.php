<?php

namespace Rockapps\RkLaravel\Tests\Api\Payment;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeCustomer;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class PaymentPagarMeControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;
    /**
     * @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null
     */
    protected $role;
    /**
     * @var \Illuminate\Database\Eloquent\Model|mixed|User
     */
    protected $user_admin;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new Parameter([
            'key' => Constants::PGM_SOFT_DESCRIPTOR,
            'value' => 'RK',
            'type' => Parameter::TYPE_STRING,
        ]))->save();

        (new \RkRoleTableSeeder())->run();

        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
        $this->user_admin->attachRole($this->role);
    }

    /** @test */
    function paymentBankTransferChangeState()
    {
        $user = factory(User::class)->state('pgm')->create([
            'can_pay_with_boleto' => true
        ]);
        $address = factory(Address::class)->create([
            'related_type' => User::class,
            'related_id' => $user->id
        ]);
        PagarMeCustomer::save($user);

        $payment = new Payment([
            'value' => mt_rand(100, 1000) + mt_rand(1, 99) / 100,
            'gateway' => Payment::GATEWAY_PAGARME_BOLETO,
            'payer_id' => $user->id,
            'payer_type' => User::class,
            'address_id' => $address->id,
            'async' => false,
//            'relateable_id' => $request->get('relateable_id'),
//            'relateable_type' => Order::class,
            'boleto_instructions' => 'Teste Teste',
//            'boleto_expiration_date' => '',
        ]);
        $payment->charge('PEDIDO-BOLETO');

        $this->assertNotNull($payment->id);
        $this->assertNull($payment->paid_at);
        $this->assertNotNull($payment->pgm_transaction_id);
//        $this->assertEquals(Payment::STATUS_PROCESSING, $payment->status); // Após a mudança no ChargeBoleto o status já vai direto
        $this->assertEquals(Payment::STATUS_WAITING_PAYMENT, $payment->status);

        $this->assertFalse($payment->isPaid());

        $this->callIndexByUri($this->user_admin, 200, 'v1/payments-helper/' . $payment->id . '/simulate-pgm-status', ['status' => 'paid']);

        $payment->refresh();

        $this->assertTrue($payment->isPaid());

    }

}
