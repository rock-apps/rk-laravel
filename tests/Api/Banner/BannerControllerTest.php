<?php

namespace Rockapps\RkLaravel\Tests\Api\Banner;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Banner;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class BannerControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\User */
    public $user_admin;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_user;
    /* @var \Illuminate\Database\Eloquent\Model|Category */
    public $category;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null */
    public $role;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
        $this->user_admin->attachRole($this->role);

        $this->user_user = factory(\Rockapps\RkLaravel\Models\User::class)->create(['email' => 'user@email.com', 'password' => '123456']);
        $this->category = factory(\Rockapps\RkLaravel\Models\Category::class)->create();

    }

    public function testCreateBanner()
    {
        $data = [
            'title' => 'Banner teste',
            'object_type' => get_class($this->category),
            'object_id' => $this->category->id,
            'active' => false,
            'image_upload' => Constants::LOGO_RK_B64
        ];

        $banner = $this->callStoreByUri($this->user_admin, 201, '/v1/banners', $data, 0)->decodeResponseJson()['data'];

        $this->assertDatabaseHas('media', [
            'model_type' => Banner::class,
            'model_id' => $banner['id']
        ]);
        $this->assertDatabaseHas('banners', [
            'id' => $banner['id'],
            'object_type' => get_class($this->category),
            'object_id' => $this->category->id,
            'active' => false,
        ]);

    }

    public function testUpdateBanner()
    {
        $banner = factory(Banner::class)->create([
            'object_type' => get_class($this->category),
            'object_id' => $this->category->id,
            'active' => false,
        ]);

        $this->callUpdateByUri($this->user_admin, 200, '/v1/banners/' . $banner->id, [
            'title' => 'Novo nome',
            'active' => true,
            'image_upload' => Constants::LOGO_RK_B64
        ], 0)->assertJsonFragment([
            'id' => $banner->id,
            'object_type' => get_class($this->category),
            'object_id' => $this->category->id,
            'title' => 'Novo nome',
            'active' => true
        ]);

        $this->assertDatabaseHas('banners', ['id' => $banner->id,
            'object_type' => get_class($this->category),
            'object_id' => $this->category->id,
            'title' => 'Novo nome',
            'active' => true
        ]);
    }

    public function testDestroyBanner()
    {
        $banner = factory(Banner::class)->create();

        $this->callDestroyByUri($this->user_admin, 200, "v1/banners/" . $banner->id)
            ->assertJsonFragment(['message' => 'ok']);

        $banner->refresh();
        $this->assertNotNull($banner->deleted_at);
    }

    public function testIndexBanners()
    {
        factory(Banner::class, 2)->create(['active' => true, 'title' => 'Teste']);
        factory(Banner::class, 2)->create(['active' => false, 'object_type' => 'MODEL', 'object_id' => 1234]);

        $this->callIndexByUri($this->user_admin, 200, '/v1/banners', null, 4);
        $this->callIndexByUri($this->user_admin, 200, '/v1/banners', ['active' => true], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/banners', ['title' => 'Teste'], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/banners', ['object_type' => 'MODEL'], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/banners', ['object_id' => 1234], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/banners', ['title' => 'Teste', 'active' => false], 0);

        $this->callIndexByUri($this->user_admin, 200, 'v1/banners', ['page' => 1, 'per_page' => 2], 2)
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);
    }

    public function testShowBanners()
    {
        $banner = factory(Banner::class)->create(['active' => true, 'title' => 'Teste']);

        $this->callIndexByUri($this->user_admin, 200, '/v1/banners/' . $banner->id)
            ->assertJsonStructure(['data' => [
                'id',// => (int)$banner->id,
                'title',// => (string)$banner->title,
                'object_id',// => (int)$banner->object_id,
                'object_type',// => (string)$banner->object_type,
                'active',// => (boolean)$banner->active,
                'link_url',// => (string)$banner->link_url,
                'image',// => (string)$banner->image,
                'position',// => (string)$banner->position,
                'medias',// => $this->transformMedias($banner),
                'created_at',// => (string)$banner->created_at,
                'updated_at',// => (string)$banner->updated_at,
                'deleted_at',// => isset($banner->deleted_at) ? (string)$banner->deleted_at : null,
            ]]);
    }

}
