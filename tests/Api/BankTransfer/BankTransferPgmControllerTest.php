<?php

namespace Rockapps\RkLaravel\Tests\Api\BankTransfer;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\BankAccount;
use Rockapps\RkLaravel\Models\BankTransfer;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class BankTransferPgmControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->state('pgm')->create(['email' => 'test@email.com', 'password' => '123456']);
    }

    public function testGetAndCreatePgmBankTransfers()
    {
        /** @var BankAccount $bank */
        $bank = factory(BankAccount::class)->state('pgm')->create([
            'owner_type' => get_class($this->user),
            'owner_id' => $this->user->id,
        ]);

        $this->callStoreByUri($this->user, 201, 'v1/bank-transfers', [
            'gateway' => BankTransfer::GATEWAY_PAGARME_TRANSFER,
            'bank_account_id' => $bank->id,
            'value' => 200,
        ]);

        $response = $this->callIndexByUri($this->user, 200, 'v1/bank-transfers', [
            'page' => 1,
            'per_page' => 2
        ])
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);

        $json = $response->decodeResponseJson();

        $this->assertCount(1, $json['data']);
    }


}
