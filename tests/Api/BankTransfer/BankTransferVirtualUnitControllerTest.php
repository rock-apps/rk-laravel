<?php

namespace Rockapps\RkLaravel\Tests\Api\BankTransfer;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\BankAccount;
use Rockapps\RkLaravel\Models\BankTransfer;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class BankTransferVirtualUnitControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
    }

    public function testGetAndCreateVirtualUnitBankTransfers()
    {
        Parameter::firstOrCreate([
            'key' => \Rockapps\RkLaravel\Constants\Constants::VIRTUAL_UNIT_BANK_TRANSFER_TAX,
            'value_float' => 0.7,
            'type' => Parameter::TYPE_FLOAT,
            'name' => "Taxa de resgate de Unidades Virtuais",
            'description' => "Exemplo: 1000 Units * 0,7 Tax = R$ 700",
            'readonly' => false,
        ]);

        /** @var BankAccount $bank */
        $bank = factory(BankAccount::class)->create([
            'owner_type' => get_class($this->user),
            'owner_id' => $this->user->id,
        ]);

        $json = $this->callStoreByUri($this->user, 201, 'v1/bank-transfers', [
            'gateway' => BankTransfer::GATEWAY_MANUAL,
            'bank_account_id' => $bank->id,
            'value' => 1000,
        ])
            ->decodeResponseJson()['data'];

        $transfer = BankTransfer::findOrFail($json['id']);

        $this->assertEquals(1000, $transfer->value);
        $this->assertEquals(1428, $transfer->virtual_units);
        $this->assertEquals(0.7, $transfer->virtual_units_tax);
        $this->assertDatabaseHas('bank_transfers', $transfer->toArray());
        $this->assertEquals(BankTransfer::STATUS_REQUESTED, $transfer->status);

    }


}
