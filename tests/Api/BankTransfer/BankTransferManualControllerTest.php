<?php

namespace Rockapps\RkLaravel\Tests\Api\BankTransfer;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\BankAccount;
use Rockapps\RkLaravel\Models\BankTransfer;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class BankTransferManualControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_admin;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create(['email' => 'teest@email.com', 'password' => '123456']);

        $this->user_admin = factory(User::class)->state(Constants::ROLE_ADMIN)->create();
    }

    public function testGetAndCreateManualBankTransfers()
    {
        /** @var BankAccount $bank */
        $bank = factory(BankAccount::class)->create([
            'owner_type' => get_class($this->user),
            'owner_id' => $this->user->id,
        ]);
        $transfer = factory(BankTransfer::class)->state('manual')->create([
            'transfered_id' => $this->user->id,
            'transfered_type' => get_class($this->user),
        ]);

        $this->callStoreByUri($this->user, 201, 'v1/bank-transfers', [
            'gateway' => BankTransfer::GATEWAY_MANUAL,
            'bank_account_id' => $bank->id,
            'value' => 200,
        ]);

        $this->callIndexByUri($this->user, 200, 'v1/bank-transfers/'.$transfer->id)->assertJsonFragment([
            'transfered_id' => $this->user->id,
            'transfered_type' => get_class($this->user),
        ]);

        $response = $this->callIndexByUri($this->user, 200, 'v1/bank-transfers', [
            'page' => 1,
            'per_page' => 2
        ])
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);

        $json = $response->decodeResponseJson();

        $this->assertCount(2, $json['data']);
    }


    public function testChangeBankTransferState()
    {

        /** @var BankTransfer $transfer */
        $transfer = factory(BankTransfer::class)->state('manual')->create([
            'value' => 1000,
            'transfered_id' => $this->user->id,
            'transfered_type' => get_class($this->user),
        ]);

        $this->callUpdateByUri($this->user_admin, 200, "v1/bank-transfers/".$transfer->id, [
            'gateway' => $transfer->gateway,
            'value' => $transfer->value,
            'status' => BankTransfer::STATUS_TRANSFERRED,
        ]);

        $transfer->refresh();

        $this->assertEquals(BankTransfer::STATUS_TRANSFERRED,$transfer->status);
    }

    public function testDestroyBankTransfer()
    {
        $transfer = factory(BankTransfer::class)->state('manual')->create([
            'transfered_id' => $this->user->id,
            'transfered_type' => get_class($this->user),
        ]);

        $this->callDestroyByUri($this->user_admin, 200, "v1/bank-transfers/" . $transfer->id)
            ->assertJsonFragment(['message' => 'ok']);

        $transfer->refresh();
        $this->assertNotNull($transfer->deleted_at);
    }
}
