<?php

namespace Rockapps\RkLaravel\Tests\Api\Plan;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Plan;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Models\Variant;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class PlanControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\User */
    public $user_admin;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_user;
    /* @var \Illuminate\Database\Eloquent\Model|Category */
    public $category;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null */
    public $role;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Company|null */
    public $company;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
        $this->user_admin->attachRole($this->role);

        $this->user_user = factory(\Rockapps\RkLaravel\Models\User::class)->create(['email' => 'user@email.com', 'password' => '123456']);
    }

    public function testCreatePlan()
    {
        $variant = factory(Variant::class)->create();

        $data = [
            'value' => 50.5,
            'name' => 'Platinum Plan',
            'code' => 'PPMAN',
            'gateway' => Plan::GATEWAY_MANUAL,
            'days' => 30,
            'trial_days' => 0,
            'payment_method' => 'boleto,credit_card',
            'color' => '#000000',
            'charges' => null,
            'installments' => 1,
            'active' => true,
            'invoice_reminder' => 4,
            'variants' => [$variant->id]
        ];

        $plan = $this->callStoreByUri($this->user_admin, 201, '/v1/plans', $data, false)->decodeResponseJson()['data'];

        $this->assertDatabaseHas('plans', [
            'id' => $plan['id'],
            'name' => 'Platinum Plan',
            'code' => 'PPMAN',
            'gateway' => Plan::GATEWAY_MANUAL,
        ]);
    }

    public function testUpdatePlan()
    {
        $plan = factory(Plan::class)->create([
            'value' => 50.5,
            'name' => 'Platinum Plan',
            'code' => 'PPMAN',
            'gateway' => Plan::GATEWAY_MANUAL,
            'days' => 30,
            'trial_days' => 0,
            'payment_method' => 'boleto,credit_card',
            'color' => '#000000',
            'charges' => null,
            'installments' => 1,
            'active' => true,
            'invoice_reminder' => 4,
        ]);

        $this->callUpdateByUri($this->user_admin, 200, '/v1/plans/' . $plan->id, [
            'value' => 70.5,
            'name' => 'Platinum Plan 2',
            'code' => 'PPMAN2', 'gateway' => Plan::GATEWAY_MANUAL,
            'days' => 30,
            'trial_days' => 0,
            'invoice_reminder' => 4,
        ], false)->assertJsonFragment([
            'value' => 70.5,
            'name' => 'Platinum Plan 2',
            'code' => 'PPMAN2',
        ]);

        $this->assertDatabaseHas('plans', [
            'value' => 70.5,
            'name' => 'Platinum Plan 2',
            'code' => 'PPMAN2',
        ]);
    }

    public function testUpdatePlanVariants()
    {
        $variant = factory(Variant::class)->create();
        $variant2 = factory(Variant::class)->create();
        $variant3 = factory(Variant::class)->create();
        $plan = factory(Plan::class)->create();
        $plan->variants()->attach($variant->id, ['object_type' => Plan::class]);

        $this->callUpdateByUri($this->user_admin, 200, '/v1/plans/' . $plan->id, [
            'value' => 70.5,
            'name' => 'Platinum Plan 2',
            'code' => 'PPMAN2', 'gateway' => Plan::GATEWAY_MANUAL,
            'days' => 30,
            'trial_days' => 0,
            'invoice_reminder' => 4,
            'variants' => [$variant2->id, $variant3->id],
        ], false)
            ->assertJsonStructure(['data' => ['variants' => [['id'], ['id']]]]);

        $this->assertDatabaseMissing('object_variants', [
            'object_id' => $plan->id,
            'object_type' => Plan::class,
            'variant_id' => $variant->id,
        ]);

        $this->assertDatabaseHas('object_variants', [
            'object_id' => $plan->id,
            'object_type' => Plan::class,
            'variant_id' => $variant2->id,
        ]);

        $this->assertDatabaseHas('object_variants', [
            'object_id' => $plan->id,
            'object_type' => Plan::class,
            'variant_id' => $variant3->id,
        ]);
    }

    public function testIndexPlans()
    {
        factory(Plan::class, 2)->create(['active' => true, 'name' => 'PLANO ESPECIAL', 'active_android' => true]);
        factory(Plan::class, 2)->create(['active' => false, 'active_ios' => false]);

        $this->callIndexByUri($this->user_admin, 200, '/v1/plans', null, 4);
        $this->callIndexByUri($this->user_admin, 200, '/v1/plans', ['active' => true], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/plans', ['name' => 'ESPECIAL'], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/plans', ['active_android' => true, 'active_ios' => false], 2);

        $this->callIndexByUri($this->user_admin, 200, 'v1/plans', ['page' => 1, 'per_page' => 2], 2)
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);
    }

    public function testShowPlans()
    {
        $plan = factory(Plan::class)->create();

        $this->callIndexByUri($this->user_admin, 200, '/v1/plans/' . $plan->id)
            ->assertJsonStructure(['data' => [
                'id',// => (int)$plan->id,
                'active',// => (bool)$plan->active,
                'gateway',// => (string)$plan->gateway,
                'apple_product_id',// => (string)$plan->apple_product_id,
                'google_product_id',// => (string)$plan->google_product_id,
                'name',// => (string)$plan->name,
                'code',// => (string)$plan->code,
                'headline',// => (string)$plan->headline,
                'days',// => (int)$plan->days,
                'trial_days',// => (int)$plan->trial_days,
                'payment_method',// => (string)$plan->payment_method,
                'color',// => (string)$plan->color,
                'value',// => (float)$plan->value,
                'charges',// => (int)$plan->charges,
                'installments',// => (int)$plan->installments,
                'invoice_reminder',// => (int)$plan->invoice_reminder,
                'pgm_plan_id',// => (int)$plan->pgm_plan_id,
                'active_android',// => (bool)$plan->active_android,
                'active_ios',// => (bool)$plan->active_ios,
                'description',// => (string)$plan->description,
                'variants',// => $plan->variants->map(function (Variant $v) {}),
                'created_at',// => (string)$plan->created_at,
                'updated_at',// => (string)$plan->updated_at,
                'deleted_at',// => isset($plan->deleted_at) ? (string)$plan->deleted_at : null,
            ]]);
    }

}
