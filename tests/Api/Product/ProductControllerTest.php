<?php

namespace Rockapps\RkLaravel\Tests\Api\Product;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Product;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Models\Variant;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class ProductControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\User */
    public $user_admin;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_user;
    /* @var \Illuminate\Database\Eloquent\Model|Category */
    public $category;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null */
    public $role;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Company|null */
    public $company;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
        $this->user_admin = factory(User::class)->state(Constants::ROLE_ADMIN)->create();
        $this->user_user = factory(User::class)->state(Constants::ROLE_USER)->create();
        $this->category = factory(\Rockapps\RkLaravel\Models\Category::class)->create();
        $this->company = factory(\Rockapps\RkLaravel\Models\Company::class)->create();
    }

    public function testCreateProduct()
    {
        $variant = factory(Variant::class)->create();

        $data = [
            'name' => 'Product teste',
            'unit_value' => 5.5,
            'active' => true,
            'type' => Product::TYPE_PRODUCT_VIRTUAL,
            'instance' => Product::INSTANCE_MAIN,
            'company_id' => $this->company->id,
            'variants' => [$variant->id]
        ];

        $product = $this->callStoreByUri($this->user_admin, 201, '/v1/products', $data, false)->decodeResponseJson()['data'];

        $this->assertDatabaseHas('products', [
            'id' => $product['id'],
            'name' => 'Product teste',
            'unit_value' => 5.5,
            'active' => true,
            'type' => Product::TYPE_PRODUCT_VIRTUAL,
            'company_id' => $this->company->id
        ]);

    }

    public function testUpdateProduct()
    {
        $product = factory(Product::class)->create([
            'name' => 'Product teste',
            'unit_value' => 5.5,
            'active' => true,
            'type' => Product::TYPE_PRODUCT_VIRTUAL,
            'instance' => Product::INSTANCE_MAIN,
            'company_id' => $this->company->id
        ]);

        $this->callUpdateByUri($this->user_admin, 200, '/v1/products/' . $product->id, [
            'name' => 'Novo nome',
            'unit_value' => 20.33,
            'active' => false,
            'image_upload' => Constants::LOGO_RK_B64
        ], false)->assertJsonFragment([
            'name' => 'Novo nome',
            'unit_value' => 20.33,
            'active' => false,
        ]);

        $this->assertDatabaseHas('products', [
            'name' => 'Novo nome',
            'unit_value' => 20.33,
            'active' => false,
        ]);
    }


    public function testPostProductWithVariants()
    {
        $variant2 = factory(Variant::class)->create();
        $variant3 = factory(Variant::class)->create();

        $json = $this->callStoreByUri($this->user_admin, 201, '/v1/products', [
            'name' => 'Product teste',
            'unit_value' => 5.5,
            'active' => true,
            'type' => Product::TYPE_PRODUCT_VIRTUAL,
            'instance' => Product::INSTANCE_MAIN,
            'company_id' => $this->company->id,
            'variants' => [$variant2->id, $variant3->id],
        ], false)->assertJsonFragment([
            'name' => 'Product teste',
            'active' => true,
        ])->assertJsonStructure(['data' => ['variants' => [['id'], ['id']]]])
            ->decodeResponseJson()['data'];

        $product = Product::findOrFail($json['id']);

        $this->assertDatabaseHas('products', [
            'name' => 'Product teste',
            'id' => $product->id,
            'active' => true,
        ]);

        $this->assertDatabaseHas('object_variants', [
            'object_id' => $product->id,
            'object_type' => Product::class,
            'variant_id' => $variant2->id,
        ]);

        $this->assertDatabaseHas('object_variants', [
            'object_id' => $product->id,
            'object_type' => Product::class,
            'variant_id' => $variant3->id,
        ]);
    }

    public function testUpdateProductVariants()
    {
        $variant = factory(Variant::class)->create();
        $variant2 = factory(Variant::class)->create();
        $variant3 = factory(Variant::class)->create();
        $product = factory(Product::class)->create([
            'name' => 'Product teste',
            'unit_value' => 5.5,
            'active' => true,
            'type' => Product::TYPE_PRODUCT_VIRTUAL,
            'instance' => Product::INSTANCE_MAIN,
            'company_id' => $this->company->id
        ]);
        $product->variants()->attach($variant->id, ['object_type' => Product::class]);

        $this->callUpdateByUri($this->user_admin, 200, '/v1/products/' . $product->id, [
            'name' => 'Novo nome',
            'unit_value' => 20.33,
            'active' => false,
            'variants' => [$variant2->id, $variant3->id],
        ], false)->assertJsonFragment([
            'name' => 'Novo nome',
            'unit_value' => 20.33,
            'active' => false,
        ])->assertJsonStructure(['data' => ['variants' => [['id'], ['id']]]]);

        $this->assertEquals(2, $product->variants()->count());
        $this->assertEquals(2, $product->variants[0]->id);
        $this->assertEquals(3, $product->variants[1]->id);

        $this->assertDatabaseMissing('object_variants', [
            'object_id' => $product->id,
            'object_type' => Product::class,
            'variant_id' => $variant->id,
        ]);

        $this->assertDatabaseHas('object_variants', [
            'object_id' => $product->id,
            'object_type' => Product::class,
            'variant_id' => $variant2->id,
        ]);

        $this->assertDatabaseHas('object_variants', [
            'object_id' => $product->id,
            'object_type' => Product::class,
            'variant_id' => $variant3->id,
        ]);

        // Quando executa o update sequencialmente o sync está removendo as variantes
        // Este teste é para validar isso

        $this->callUpdateByUri($this->user_admin, 200, '/v1/products/' . $product->id, [
            'name' => 'Novo nome 2',
            'unit_value' => 20.33,
            'active' => false,
            'variants' => [$variant2->id, $variant3->id],
        ], false)->assertJsonFragment([
            'name' => 'Novo nome 2',
            'unit_value' => 20.33,
            'active' => false,
        ])->assertJsonStructure(['data' => ['variants' => [['id'], ['id']]]]);

//        $product->variants()->count()

        $this->assertDatabaseMissing('object_variants', [
            'object_id' => $product->id,
            'object_type' => Product::class,
            'variant_id' => $variant->id,
        ]);

        $this->assertDatabaseHas('object_variants', [
            'object_id' => $product->id,
            'object_type' => Product::class,
            'variant_id' => $variant2->id,
        ]);

        $this->assertDatabaseHas('object_variants', [
            'object_id' => $product->id,
            'object_type' => Product::class,
            'variant_id' => $variant3->id,
        ]);
    }

    public function testDestroyProduct()
    {
        $product = factory(Product::class)->create(['company_id' => $this->company->id]);

        $this->callDestroyByUri($this->user_admin, 200, "v1/products/" . $product->id)
            ->assertJsonFragment(['message' => 'ok']);

        $product->refresh();
        $this->assertNotNull($product->deleted_at);
    }

    public function testIndexProducts()
    {
        factory(Product::class, 2)->create(['active' => true, 'company_id' => $this->company->id, 'name' => 'Teste', 'active_ios' => true]);
        $p = factory(Product::class, 2)->create(['active' => false, 'company_id' => $this->company->id, 'active_android' => true,]);

        $this->callIndexByUri($this->user_admin, 200, '/v1/products', null, 4);
        $this->callIndexByUri($this->user_admin, 200, '/v1/products', ['active' => true], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/products', ['name' => 'Teste'], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/products', ['active_android' => true, 'active_ios' => true], 4);
        $this->callIndexByUri($this->user_admin, 200, '/v1/products', ['company_id' => $p[0]->company_id], 4);

        $this->callIndexByUri($this->user_admin, 200, 'v1/products', ['page' => 1, 'per_page' => 2], 2)
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);
    }

    public function testIndexProductsByUser()
    {
        $p1 = factory(Product::class)->create(['active' => true, 'company_id' => $this->company->id,]);
        $p2 = factory(Product::class)->create(['active' => false, 'company_id' => $this->company->id,]);

        $this->callIndexByUri($this->user_user, 200, '/v1/products', null, 1);
    }

    public function testIndexProductsByUserCompany()
    {
        factory(Product::class)->create(['active' => true, 'company_id' => $this->company->id,]);
        factory(Product::class)->create(['active' => false, 'company_id' => $this->company->id,]);

        $this->callIndexByUri($this->company->responsible, 200, '/v1/products', null, 2);
    }

    public function testShowProducts()
    {
        $product = factory(Product::class)->create(['company_id' => $this->company->id]);

        $this->callIndexByUri($this->user_admin, 200, '/v1/products/' . $product->id)
            ->assertJsonStructure(['data' => [
                'id',// => (int)$product->id,
                'name',// => (string)($product->name ? $product->name : $product->category->name),
                'description',// => (string)$product->description,
                'active',// => (bool)$product->active,
                'unit_value',// => (float)$product->unit_value,
                'company_id',// => (integer)$product->company_id,
                'categories',// => $product->categories->map(function (Category $cat) {
                'medias',// => $this->transformMedias($product),
                'created_at',// => (string)$product->created_at,
                'updated_at',// => (string)$product->updated_at,
                'deleted_at',// => isset($product->deleted_at) ? (string)$product->deleted_at : null,
                'type',// => (string)$product->type,
                'instance',// => (string)$product->instance,
                'variation_property',// => (string)$product->variation_property,
                'variation_value',// => (string)$product->variation_value,
                'image',// => $product->image,

                'weight',// => (float)$product->weight,
                'dimension_height',// => (float)$product->dimension_height,
                'dimension_width',// => (float)$product->dimension_width,
                'dimension_depth',// => (float)$product->dimension_depth,

                'virtual_units_release',// => (float)$product->virtual_units_release,
                'apple_product_id',// => (string)$product->apple_product_id,
                'google_product_id',// => (string)$product->google_product_id,
                'active_ios',// => (bool)$product->active_ios,
                'active_android',// => (bool)$product->active_android,

                'main_product_id',// => $product->main_product_id,
                'variants',// => $product->variants,
            ]]);
    }

}
