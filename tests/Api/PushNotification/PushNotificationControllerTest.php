<?php

namespace Rockapps\RkLaravel\Tests\Api\PushNotification;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\PushNotification;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class PushNotificationControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\User */
    public $user_admin;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
        $this->user_admin = factory(User::class)->state(Constants::ROLE_ADMIN)->create(['email' => 'test@email.com', 'password' => '123456']);
    }

    public function testIndexPushNotificationDestinations()
    {
        $this->callIndexByUri($this->user_admin, 200, '/v1/push-notifications/destinations', null, 2)
            ->assertJsonFragment([
                'key' => 'USERS_ALL'
            ]);
    }

    public function testIndexPushNotification()
    {
        factory(PushNotification::class, 2)->create(['type' => 'INFO', 'status' => PushNotification::STATUS_FINISHED]);
        factory(PushNotification::class, 2)->create(['type' => 'WARN', 'status' => PushNotification::STATUS_SCHEDULED]);

        $this->callIndexByUri($this->user_admin, 200, '/v1/push-notifications', ['type' => 'INFO'], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/push-notifications', ['status' => PushNotification::STATUS_FINISHED], 2);

        $this->callIndexByUri($this->user_admin, 200, 'v1/push-notifications', ['page' => 1, 'per_page' => 2], 2)
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);
    }

    public function testCreatePushNotification()
    {
        $data = [
            'name' => 'Push Notification Name',
            'channel' => PushNotification::CHANNEL_PUSH,
            'type' => PushNotification::CHANNEL_PUSH,
            'destination' => \Rockapps\RkLaravel\Services\PushNotificationService::destinations()[0]['key'],
            'scheduled_at' => now()->format('Y-m-d'),
            'title' => 'Título',
            'body' => 'Corpo no Push',
        ];

        $pn = $this->callStoreByUri($this->user_admin, 201, '/v1/push-notifications', $data, false)->decodeResponseJson()['data'];

        $this->assertDatabaseHas('push_notifications', [
            'id' => $pn['id'],
            'name' => 'Push Notification Name',
            'channel' => PushNotification::CHANNEL_PUSH,
            'type' => PushNotification::CHANNEL_PUSH,
            'destination' => \Rockapps\RkLaravel\Services\PushNotificationService::destinations()[0]['key'],
            'title' => 'Título',
            'body' => 'Corpo no Push',
        ]);
    }

    public function testUpdatePushNotification()
    {
        $pn = factory(PushNotification::class)->create(['status' => PushNotification::STATUS_SCHEDULED, 'scheduled_at' => now()->addDays(10)]);

        $date = now()->addDay()->format('Y-m-d');
        $this->callUpdateByUri($this->user_admin, 200, '/v1/push-notifications/' . $pn->id, [
            'name' => 'Teste',
            'type' => $pn->type,
            'destination' => $pn->destination,
            'scheduled_at' => $date,
            'title' => 'title',
            'body' => 'body',
        ], false)
            ->assertJsonFragment([
                'name' => 'Teste',
                'scheduled_at' => $date . ' 00:00:00',
                'title' => 'title',
                'body' => 'body',
            ]);

        $this->assertDatabaseHas('push_notifications', [
            'name' => 'Teste',
            'scheduled_at' => $date . ' 00:00:00',
            'title' => 'title',
            'body' => 'body',
        ]);
    }

    public function testShowPushNotification()
    {
        $pn = factory(PushNotification::class)->create();

        $this->callIndexByUri($this->user_admin, 200, '/v1/push-notifications/' . $pn->id)
            ->assertJsonStructure(['data' => [
                'id',// => (int)$pushNotification->id,
                'status',// => (string)$pushNotification->status,
                'name',// => (string)$pushNotification->name,
                'type',// => (string)$pushNotification->type,
                'destination',// => (string)$pushNotification->destination,
                'scheduled_at',// => (string)$pushNotification->scheduled_at,
                'title',// => (string)$pushNotification->title,
                'body',// => (string)$pushNotification->body,
                'object_id',// => (int)$pushNotification->object_id,
                'object_type',// => (string)$pushNotification->object_type,
                'created_at',// => (string)$pushNotification->created_at,
                'updated_at',// => (string)$pushNotification->updated_at,
                'deleted_at',// => isset($pushNotification->deleted_at) ? (string)$pushNotification->deleted_at : null,
            ]]);
    }

    public function testDestroyPushNotification()
    {
        $pn = factory(PushNotification::class)->create();

        $this->callDestroyByUri($this->user_admin, 200, "v1/push-notifications/" . $pn->id)
            ->assertJsonFragment(['message' => 'ok']);

        $pn->refresh();
        $this->assertNotNull($pn->deleted_at);
    }
}
