<?php

namespace Rockapps\RkLaravel\Tests\Api\Address;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class AddressControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);

        factory(Address::class, 5)->create([
            'related_id' => $this->user->id,
            'related_type' => get_class($this->user),
        ]);
    }

    public function testGetAddresses()
    {
        $this->user_2 = factory(User::class)->create();
        factory(Address::class, 5)->create([
            'related_id' => $this->user_2->id,
            'related_type' => get_class($this->user_2),
        ]);

        $response = $this->callIndexByUri($this->user, 200, 'v1/addresses', [
            'page' => 1,
            'per_page' => 2
        ])
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);

        $json = $response->decodeResponseJson();

        $this->assertCount(2, $json['data']);
        $this->assertCount(5, $this->user->addresses()->get());
    }
    public function testGetAddressesByAdmin()
    {
        $admin = factory(User::class)->state(Constants::ROLE_ADMIN)->create();
        $this->user_2 = factory(User::class)->create();
        factory(Address::class, 2)->create([
            'related_id' => $this->user_2->id,
            'related_type' => get_class($this->user_2),
        ]);

        $response = $this->callIndexByUri($admin, 200, 'v1/addresses', [
            'page' => 1,
            'per_page' => 2,
            'relate_type' => User::class,
            'relate' => $this->user_2->id,
        ])
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);

        $json = $response->decodeResponseJson();

        $this->assertCount(2, $json['data']);
        $this->assertCount(7, Address::all());
    }

    public function testGetSpecificAddress()
    {
        $address = $this->user->addresses[0];
        $this->callIndexByUri($this->user, 200, "v1/addresses/$address->id")
            ->assertJsonFragment(['name' => $address->name]);
    }

    public function testCreateAddress()
    {
        $this->callStoreByUri($this->user, 201, 'v1/addresses', [
            'name' => $this->faker->randomElement(['Casa', 'Trabalho']),
            'street' => $this->faker->streetAddress,
            'number' => 100,
            'city' => $this->faker->city,
            'state' => $this->faker->state,
            'district' => $this->faker->city,
            'country' => $this->faker->country,
            'zipcode' => '22621140',
        ])
            ->assertJsonFragment([
                'zipcode' => '22621140',
                'number' => '100',
            ]);

        $this->assertDatabaseHas('addresses', [
            'zipcode' => '22621140',
            'number' => '100',
        ]);
    }

    public function testUpdateAddress()
    {
        $address = $this->user->addresses[0]->toArray();
        $address['name'] = 'Teste';
        $this->callUpdateByUri($this->user, 200, "v1/addresses/{$address['id']}", $address)
            ->assertJsonFragment(['name' => 'Teste']);

        $this->assertDatabaseHas('addresses', ['id' => $address['id']]);
    }

    public function testDestroyAddress()
    {
        $address = $this->user->addresses[0];

        $this->callDestroyByUri($this->user, 200, "v1/addresses/{$address->id}")
            ->assertOk();

        $address->refresh();
        $this->assertNotNull($address->deleted_at);

    }

    public function testSetDefaultAddress()
    {
        $address = $this->user->addresses[0];
        $address2 = $this->user->addresses[1];

        $this->assertEquals(1, $this->user->addresses->where('default', true)->count());

        $this->callUpdateByUri($this->user, 200, "v1/addresses/{$address->id}/default")
            ->assertJsonFragment(['default' => true]);

        $this->assertEquals($address->id, $this->user->addresses->where('default', true)->first()->id);
        $this->assertEquals(1, $this->user->addresses->where('default', true)->count());


        $this->callUpdateByUri($this->user, 200, "v1/addresses/{$address2->id}/default")
            ->assertJsonFragment(['default' => true]);

        $this->assertEquals($address2->id, $this->user->addresses()->where('default', true)->first()->id);
        $this->assertEquals(1, $this->user->addresses()->where('default', true)->count());

    }

}
