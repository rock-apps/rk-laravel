<?php

namespace Rockapps\RkLaravel\Tests\Api\PaymentMethod;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\PaymentMethod;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class PaymentMethodControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\User */
    public $user_admin;
    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\Company */
    public $company;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_user;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null */
    public $role;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
        $this->user_admin->attachRole($this->role);

        $this->company = factory(\Rockapps\RkLaravel\Models\Company::class)->create();
        $company_role = Role::whereName(Constants::ROLE_COMPANY)->first();
        $this->user_user = factory(\Rockapps\RkLaravel\Models\User::class)->create(['company_id' => $this->company->id]);
        $this->user_user->attachRole($company_role);
    }

    public function testCreatePaymentMethod()
    {
        $data = [
            'gateway' => PaymentMethod::GATEWAY_MANUAL,
            'method' => Payment::GATEWAY_CASH,
            'gateway_key' => '123456',
            'gateway_secret' => 'ABCDEF',
            'gateway_sandbox' => true,
//            'bank_account_id' => $account->id,
            'active' => true,
            'title' => 'title',
            'description' => 'description',
//            'company_id' => 'required|exists:companies,id',
            'image_upload' => Constants::LOGO_RK_B64
        ];

        $paymentMethod = $this->callStoreByUri($this->user_user, 201, '/v1/payment-methods', $data)->decodeResponseJson()['data'];

        $this->assertDatabaseHas('media', [
            'model_type' => PaymentMethod::class,
            'model_id' => $paymentMethod['id']
        ]);
        $this->assertDatabaseHas('payment_methods', [
            'id' => $paymentMethod['id'],
            'active' => true,
            'gateway' => PaymentMethod::GATEWAY_MANUAL,
            'method' => Payment::GATEWAY_CASH,
            'gateway_key' => '123456',
            'gateway_secret' => 'ABCDEF',
        ]);

        /** @var  $paymentMethod */
        $paymentMethod = PaymentMethod::findOrFail($paymentMethod['id']);
        $this->assertEquals($paymentMethod->company_id, $this->user_user->company_id);

    }

    public function testUpdatePaymentMethod()
    {
        $account = factory(\Rockapps\RkLaravel\Models\BankAccount::class)->create();

        $paymentMethod = factory(PaymentMethod::class)->create([
            'gateway' => PaymentMethod::GATEWAY_MANUAL,
            'method' => Payment::GATEWAY_BANK_TRANSFER,
            'bank_account_id' => $account->id,
            'active' => true,
            'title' => 'title',
            'description' => 'description',
            'company_id' => $this->company->id,
        ]);

        $this->callUpdateByUri($this->user_admin, 200, '/v1/payment-methods/' . $paymentMethod->id, [
            'gateway' => PaymentMethod::GATEWAY_MANUAL,
            'method' => Payment::GATEWAY_BANK_TRANSFER,
            'title' => 'Novo nome',
            'active' => true
        ])->assertJsonFragment([
            'id' => $paymentMethod->id,
            'title' => 'Novo nome',
            'active' => true
        ]);

    }

    public function testDestroyPaymentMethod()
    {
        $paymentMethod = factory(PaymentMethod::class)->create(['company_id' => $this->company->id]);

        $this->callDestroyByUri($this->user_admin, 200, "v1/payment-methods/" . $paymentMethod->id, 0)
            ->assertJsonFragment(['message' => 'ok']);

        $paymentMethod->refresh();
        $this->assertNotNull($paymentMethod->deleted_at);
    }

    public function testIndexPaymentMethods()
    {
        factory(PaymentMethod::class, 2)->create([
            'company_id' => $this->company->id,
            'active' => true,
            'title' => 'Teste'
        ]);
        factory(PaymentMethod::class, 2)->create([
            'company_id' => $this->company->id,
            'active' => false,
            'title' => 'Conta'
        ]);

        $this->callIndexByUri($this->user_admin, 200, '/v1/payment-methods', null, 4);
        $this->callIndexByUri($this->user_admin, 200, '/v1/payment-methods', ['active' => true], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/payment-methods', ['title' => 'Teste'], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/payment-methods', ['company' => $this->company->id], 4);

        $this->callIndexByUri($this->user_admin, 200, 'v1/payment-methods', ['page' => 1, 'per_page' => 2], 2)
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);
    }

    public function testShowPaymentMethods()
    {
        $paymentMethod = factory(PaymentMethod::class)->create([
            'company_id' => $this->company->id,
            'active' => true,
            'title' => 'Teste'
        ]);

        $this->callIndexByUri($this->user_admin, 200, '/v1/payment-methods/' . $paymentMethod->id)
            ->assertJsonStructure(['data' => [
                'id',
                'gateway',
                'method',
                'title',
                'description',
                'image',
                'bank_account_id',
                'active',
                'company_id',
                'created_at',
                'updated_at',
                'deleted_at',
                'bank_account',
                'company',
                'gateway_key',
                'gateway_secret',
                'gateway_sandbox',
            ]]);
    }

}
