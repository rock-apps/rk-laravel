<?php

namespace Rockapps\RkLaravel\Tests\Api\MetaMessage;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Banner;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class MetaMessageControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');
    }

    public function testMeta()
    {
        factory(Banner::class, 2)->create();

        $this->callIndexByUri(null, 200, 'v1/sample/test', null, 2)
            ->assertJsonStructure(['meta' => ['messages' => [['title', 'body', 'class', 'type']]]])
            ->decodeResponseJson();
    }


}
