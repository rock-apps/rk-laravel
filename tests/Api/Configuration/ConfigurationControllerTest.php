<?php

namespace Rockapps\RkLaravel\Tests\Api\Configuration;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class ConfigurationControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create();
    }

    public function testGetConfigurations()
    {
        $this->callIndexByUri($this->user, 200, 'v1/configurations')
            ->assertJsonStructure(['data' => ['id', 'configs' => ['key']]]);
    }

    public function testCreateConfiguration()
    {
        $this->callStoreByUri($this->user, 201, 'v1/configurations', [
            'configs' => [
                'chaveA' => 'valor',
                'chaveB' => true,
                'chaveC' => 123,
            ]
        ])
            ->assertJsonFragment([
                'chaveA' => 'valor',
                'chaveB' => true,
                'chaveC' => 123,
            ]);

        $this->assertDatabaseHas('configurations', [
                'configs' => json_encode([
                    'key' => 'valor',
                    'chaveA' => 'valor',
                    'chaveB' => true,
                    'chaveC' => 123,
                ])
            ]
        );
    }

    public function testUpdateConfiguration()
    {
        $this->callUpdateByUri($this->user, 200, 'v1/configurations', [
            'configs' => [
                'chaveA' => 'valor',
                'chaveB' => true,
            ]
        ])
            ->assertJsonFragment([
                'chaveA' => 'valor',
                'chaveB' => true,
            ]);

        $this->callUpdateByUri($this->user, 200, 'v1/configurations', [
            'configs' => [
                'chaveB' => false,
            ]
        ])
            ->assertJsonFragment([
                'chaveA' => 'valor',
                'chaveB' => false,
            ]);


        $this->assertDatabaseHas('configurations', [
                'configs' => json_encode([
                    'key' => 'valor',
                    'chaveA' => 'valor',
                    'chaveB' => false,
                ])
            ]
        );
    }

}
