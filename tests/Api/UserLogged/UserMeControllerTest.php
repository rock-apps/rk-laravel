<?php

namespace Rockapps\RkLaravel\Tests\Api\UserLogged;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Notifications\UserSignUpToken;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class UserMeControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
    }

    public function testGetMe()
    {
        $this->callIndexByUri($this->user, 200, 'v1/user/me')->assertJson([
            'data' => [
                'email' => 'test@email.com',
            ],
        ])->isOk();
    }


    public function testUpdateMe()
    {

        $data = [
            'email' => 'user@emailupdate.com',
            'mobile' => '21988885555',
            'telephone' => '21877776666',
        ];

        $this->callUpdateByUri($this->user, 200, 'v1/user/me', array_merge($data, ['password' => 'secretnovo']))
            ->assertJson(['data' => $data]);

        $this->post('v1/auth/login', [
            'email' => 'user@emailupdate.com',
            'password' => 'secretnovo'
        ]);
        $this->user->refresh();
        $this->assertNotNull($this->user->last_access_at);

    }
    public function testDestroyMe()
    {
        $name = $this->user->name;
        $this->callUpdateByUri($this->user, 200, 'v1/user/me/destroy');

        $this->user->refresh();
        $this->assertNotEquals($name,$this->user->name);
    }

    public function testSuspendedMe()
    {
        $this->user->suspended = true;
        $this->user->save();
        $data = [
            'email' => 'user@emailupdate.com',
            'mobile' => '21988885555',
            'telephone' => '21877776666',
        ];

        $this->callUpdateByUri($this->user, 403, 'v1/user/me', array_merge($data, ['password' => 'secretnovo']))
//            ->assertJsonFragment(["message" => "User is suspended. Please, contact our support."]);
            ->assertJsonFragment(["message" => "O usuário está em análise. Entre em contato com nosso suporte."]);

    }

    public function testOnlineStatusMe()
    {
        $data = [
            'email' => 'user@emailupdate.com',
            'mobile' => '21988885555',
            'telephone' => '21877776666',
            'online_status' => User::ONLINE_STATUS_ONLINE,
        ];

        $this->callUpdateByUri($this->user, 200, 'v1/user/me', $data)
            ->assertJson(['data' => $data])
            ->assertJsonFragment(['online_status' => User::ONLINE_STATUS_ONLINE]);

        $this->callUpdateByUri($this->user, 200, 'v1/user/me', ['show_online_status' => false])
            ->assertJsonFragment(['online_status' => User::ONLINE_STATUS_DISABLED]);

        $this->user->refresh();

    }


    public function testSignUpToken()
    {
        $user = $this->user;

        $this->expectsNotification($user, UserSignUpToken::class);

        $this->callStoreByUri($user, 200, 'v1/user/send-signup-token-email')
            ->assertOk();

        $this->callUpdateByUri($user, 422, 'v1/user/validate-signup-token', [
            'signup_token' => 12345
        ])
            ->assertJsonFragment(['message' => 'Código de verificação incorreto']);

        $user->refresh();

        $this->callUpdateByUri($user, 200, 'v1/user/validate-signup-token', [
            'signup_token' => $user->signup_token
        ])
            ->assertOk();

        $user->refresh();
        $this->assertNull($user->signup_token);
        $this->assertTrue($user->verified);

    }
}
