<?php

namespace Rockapps\RkLaravel\Tests\Api\CreditCard;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\CreditCard;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class CreditCardControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->state('pgm')->create(['email' => 'test@email.com', 'password' => '123456']);
    }

    public function testGetAndCreateCreditCards()
    {
        /** @var CreditCard $card */
        $card = factory(CreditCard::class)->create([
            'owner_type' => get_class($this->user),
            'owner_id' => $this->user->id,
        ]);

        $this->callStoreByUri($card->owner, 201, 'v1/credit-cards', [
            'holder_name' => 'José Yoda',
            'expiration_date' => '1235',
            'number' => 4242424242424242,
            'cvv' => 123,
            'default' => true,
        ]);

        $response = $this->callIndexByUri($card->owner, 200, 'v1/credit-cards', [
            'page' => 1,
            'per_page' => 2
        ])
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);

        $json = $response->decodeResponseJson();

        $this->assertCount(2, $json['data']);
    }

    public function testGetCreditCardsByAdmin()
    {
        $admin = factory(User::class)->state(Constants::ROLE_ADMIN)->create();
        $this->user_2 = factory(User::class)->create();
        factory(CreditCard::class, 2)->create([
            'owner_type' => get_class($this->user),
            'owner_id' => $this->user->id,
            'default' => true,
        ]);
        factory(CreditCard::class, 1)->create([
            'owner_type' => get_class($this->user_2),
            'owner_id' => $this->user_2->id,
        ]);

        $response = $this->callIndexByUri($admin, 200, 'v1/credit-cards', [
            'page' => 1,
            'per_page' => 2,
            'owner_type' => User::class,
            'owner' => $this->user_2->id,
        ])
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);

        $json = $response->decodeResponseJson();

        $this->assertCount(1, $json['data']);
        $this->assertCount(3, CreditCard::all());
    }

    public function testDestroyCreditCard()
    {
        /** @var CreditCard $card */
        $card = factory(CreditCard::class)->create([
            'owner_type' => get_class($this->user),
            'owner_id' => $this->user->id,
            'default' => true,
        ]);

        $this->callDestroyByUri($this->user, 200, "v1/credit-cards/{$card->id}")
            ->assertOk();

        $card->refresh();
        $this->assertNotNull($card->deleted_at);

    }

    public function testSetDefaultCreditCard()
    {
        /** @var CreditCard $card */
        $card = factory(CreditCard::class)->create([
            'owner_type' => get_class($this->user),
            'owner_id' => $this->user->id,
            'default' => true,
        ]);

        /** @var CreditCard $card */
        $card2 = factory(CreditCard::class)->create([
            'owner_type' => get_class($this->user),
            'owner_id' => $this->user->id,
            'default' => false,
        ]);

        $this->assertEquals(1, $this->user->creditCards->where('default', true)->count());

        $this->callUpdateByUri($this->user, 200, "v1/credit-cards/{$card->id}/default")
            ->assertJsonFragment(['default' => true]);

        $this->assertEquals($card->id, $this->user->creditCards->where('default', true)->first()->id);
        $this->assertEquals(1, $this->user->creditCards->where('default', true)->count());


        $this->callUpdateByUri($this->user, 200, "v1/credit-cards/{$card2->id}/default")
            ->assertJsonFragment(['default' => true]);

        $this->assertEquals($card2->id, $this->user->creditCards()->where('default', true)->first()->id);
        $this->assertEquals(1, $this->user->creditCards()->where('default', true)->count());

    }

}
