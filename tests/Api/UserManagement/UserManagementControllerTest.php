<?php

namespace Rockapps\RkLaravel\Tests\Api\UserManagement;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class UserManagementControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;


    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_admin;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_user;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null */
    public $role;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
        $this->user_admin->attachRole($this->role);
    }

    public function testGetAllUsers()
    {
        factory(User::class, 20)->create();

        $response = $this->callIndexByUri($this->user_admin, 200, 'v1/users', [
            'page' => 1,
            'per_page' => 2
        ])
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);

        $json = $response->decodeResponseJson();

        $this->assertCount(21, User::all());
        $this->assertCount(2, $json['data']);

    }

    public function testGetSpecificUser()
    {
        $user = factory(User::class)->create();

        $this->callIndexByUri($this->user_admin, 200, "v1/users/$user->id")
            ->assertJsonFragment(['name' => $user->name]);
    }

    public function testCreateUser()
    {
        $this->callStoreByUri($this->user_admin, 201, 'v1/users', [
            'name' => 'Usuário',
            'email' => 'a@a.com',
            'password' => 'secret',
        ])
            ->assertJsonFragment([
                'name' => 'Usuário',
                'email' => 'a@a.com',
            ]);
    }

    /**
     * @test
     *
     * Test: PUT /api/user
     */
    public function testUpdateUser()
    {
        $user = factory(User::class)->create();
        $data = [
            'name' => 'Foo Updated',
            'suspended' => 1
        ];
        $this->callUpdateByUri($this->user_admin,200,"v1/users/$user->id", $data)
            ->assertJsonFragment($data);
    }


}
