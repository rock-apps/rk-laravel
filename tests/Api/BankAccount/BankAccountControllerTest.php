<?php

namespace Rockapps\RkLaravel\Tests\Api\BankAccount;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Helpers\Gerador;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\BankAccount;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class BankAccountControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);

    }

    public function testGetBankList()
    {
        $this->callIndexByUri($this->user,200, 'v1/bank-accounts/list', null,257,0);

        $json = $this->callIndexByUri($this->user, 200, 'v1/bank-accounts/list', [
            'label' => 'Bradesco',
            'page' => 1,
            'per_page' => 2
        ],4,0)
            ->decodeResponseJson();

        $this->assertCount(4, $json['data']);
    }

    public function testGetBankAccounts()
    {
        /** @var BankAccount $bank */
        $bank = factory(BankAccount::class)->create([
            'owner_type' => get_class($this->user),
            'owner_id' => $this->user->id,
        ]);

        $type = BankAccount::TYPE_CONTA_CORRENTE;
        $gateway = BankAccount::GATEWAY_MANUAL;
        $code = \Rockapps\RkLaravel\Models\BankAccount::bankList()->random()['value'];

        $this->callStoreByUri($bank->owner, 201, 'v1/bank-accounts', [
            'bank_code' => $code,
            'agencia' => 1234,
            'agencia_dv' => 1,
            'conta' => 1234,
            'conta_dv' => 1,
            'document_number' => Gerador::CPF(),
            'legal_name' => $this->faker->name,
            'type' => BankAccount::TYPE_CONTA_CORRENTE,
            'gateway' => $gateway,
        ]);

        $this->callIndexByUri($bank->owner, 200, 'v1/bank-accounts', [
            'type' => $type,
            'gateway' => $gateway,
            'bank_code' => $code,
        ], 1);
        $this->callIndexByUri($bank->owner, 200, 'v1/bank-accounts', [
            'type' => BankAccount::TYPE_CONTA_CORRENTE_CONJUNTA,
        ], 0);
        $this->callIndexByUri($bank->owner, 200, 'v1/bank-accounts', [
            'gateway' => BankAccount::TYPE_CONTA_CORRENTE_CONJUNTA,
        ], 0);
        $this->callIndexByUri($bank->owner, 200, 'v1/bank-accounts', [
            'code' => '9999999'
        ], 0);
        $response = $this->callIndexByUri($bank->owner, 200, 'v1/bank-accounts', [
            'page' => 1,
            'per_page' => 2
        ])
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);

        $json = $response->decodeResponseJson();

        $this->assertCount(2, $json['data']);
    }

    public function testGetBankAccountsByAdmin()
    {
        $admin = factory(User::class)->state(Constants::ROLE_ADMIN)->create();
        $this->user_2 = factory(User::class)->create();

        factory(BankAccount::class)->create([
            'owner_type' => get_class($this->user_2),
            'owner_id' => $this->user_2->id,
        ]);
        factory(BankAccount::class)->create([
            'owner_type' => get_class($this->user),
            'owner_id' => $this->user->id,
        ]);

        $response = $this->callIndexByUri($admin, 200, 'v1/bank-accounts', [
            'page' => 1,
            'per_page' => 2,
            'owner_type' => User::class,
            'owner' => $this->user_2->id,
        ])
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);

        $json = $response->decodeResponseJson();

        $this->assertCount(1, $json['data']);
        $this->assertCount(2, BankAccount::all());
    }

    public function testCreateBankAccount()
    {
        $code = 237; // Bradesco
        $this->callStoreByUri($this->user, 201, 'v1/bank-accounts', [
            'bank_code' => $code,
            'agencia' => 1234,
            'agencia_dv' => 1,
            'conta' => 1234,
            'conta_dv' => 1,
            'document_number' => Gerador::CPF(),
            'legal_name' => $this->faker->name,
            'gateway' => BankAccount::GATEWAY_MANUAL,
            'type' => $this->faker->randomElement([
                BankAccount::TYPE_CONTA_CORRENTE,
                BankAccount::TYPE_CONTA_POUPANCA,
                BankAccount::TYPE_CONTA_CORRENTE_CONJUNTA,
                BankAccount::TYPE_CONTA_POUPANCA_CONJUNTA
            ]),
        ],0)
            ->assertJsonFragment([
                'bank_code' => 237,
                'agencia' => 1234,
                'bank' => [
                    'value' => 237,
                    'label' => 'Banco Bradesco S.A.',
                    'label_short' => 'BCO BRADESCO S.A.'
                ]
            ])->decodeResponseJson();

        $this->assertDatabaseHas('bank_accounts', [
            'bank_code' => $code,
            'agencia' => 1234,
        ]);
    }

    public function testCreateBankAccountPix()
    {
        $this->callStoreByUri($this->user, 201, 'v1/bank-accounts', [
            'pix_key' => '21987654321',
            'type' => \Rockapps\RkLaravel\Models\BankAccount::TYPE_PIX,
            'gateway' => \Rockapps\RkLaravel\Models\BankAccount::GATEWAY_MANUAL,
        ])
            ->assertJsonFragment([
                'pix_key' => '21987654321',
                'type' => \Rockapps\RkLaravel\Models\BankAccount::TYPE_PIX,
                'gateway' => \Rockapps\RkLaravel\Models\BankAccount::GATEWAY_MANUAL,
            ]);

        $this->assertDatabaseHas('bank_accounts', [
            'pix_key' => '21987654321',
            'type' => \Rockapps\RkLaravel\Models\BankAccount::TYPE_PIX,
            'gateway' => \Rockapps\RkLaravel\Models\BankAccount::GATEWAY_MANUAL,
        ]);
    }

    public function testCreateBankAccountPagarMe()
    {
        $code = 237; // Bradesco
        $json = $this->callStoreByUri($this->user, 201, 'v1/bank-accounts', [
            'bank_code' => $code,
            'agencia' => 1234,
            'agencia_dv' => 1,
            'conta' => 1234,
            'conta_dv' => 1,
            'document_number' => Gerador::CPF(),
            'legal_name' => $this->faker->name,
            'type' => BankAccount::TYPE_CONTA_CORRENTE,
            'gateway' => BankAccount::GATEWAY_PGM,
        ])
            ->assertJsonFragment([
                'bank_code' => 237,
                'agencia' => 1234,
                'bank' => [
                    'value' => 237,
                    'label' => 'Banco Bradesco S.A.',
                    'label_short' => 'BCO BRADESCO S.A.'
                ]
            ])->decodeResponseJson()['data'];

        $bank = BankAccount::findOrFail($json['id']);

        $this->assertNotNull($bank->pgm_bank_id);

        $this->assertDatabaseHas('bank_accounts', [
            'bank_code' => $code,
            'agencia' => 1234,
        ]);
    }

}
