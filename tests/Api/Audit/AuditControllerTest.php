<?php

namespace Rockapps\RkLaravel\Tests\Api\Audit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use OwenIt\Auditing\Models\Audit;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class AuditControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;

    public function setUp(): void
    {
        parent::setUp();
        config()->set('audit.force_in_test', true);
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create();
        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create();
        $this->user_admin->attachRole($this->role);
    }

    public function testGetAudits()
    {
        $data = [
            'email' => 'user@emailupdate.com',
            'mobile' => '21988885555',
            'telephone' => '21877776666',
        ];

        $this->callUpdateByUri($this->user, 200, 'v1/user/me', array_merge($data, ['password' => 'secretnovo']))
            ->assertJson(['data' => $data]);

        $this->user->refresh();


        $response = $this->callIndexByUri($this->user_admin, 200, 'v1/audits', [
            'page' => 1,
            'per_page' => 2,
            'auditable_type' => get_class($this->user),
            'auditable_id' => $this->user->id,

        ])
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]])
            ->assertJsonStructure(['data' => [['id','event','url','ip_address','tags','modified']]])
        ;

        $json = $response->decodeResponseJson();

        $this->assertCount(2, $json['data']);
        $this->assertCount(2, $this->user->audits()->get());


        $this->callIndexByUri($this->user_admin, 200, 'v1/audits/'.$json['data'][0]['id'])
            ->assertJsonStructure(['data' => ['id','event','url','ip_address','tags','modified']])
        ;
    }

}
