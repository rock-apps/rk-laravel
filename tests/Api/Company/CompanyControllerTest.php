<?php

namespace Rockapps\RkLaravel\Tests\Api\Company;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class CompanyControllerTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|\Rockapps\RkLaravel\Models\User */
    public $user_admin;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user_user;
    /* @var \Illuminate\Database\Eloquent\Model|Category */
    public $category;
    /** @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|Role|null */
    public $role;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
        $this->role = Role::whereName(Constants::ROLE_ADMIN)->first();
        $this->user_admin = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
        $this->user_admin->attachRole($this->role);

        $this->user_user = factory(\Rockapps\RkLaravel\Models\User::class)->create(['email' => 'user@email.com', 'password' => '123456']);
        $this->category = factory(\Rockapps\RkLaravel\Models\Category::class)->create();

    }

    public function testCreateCompany()
    {
        $data = [
            'name' => 'Company teste',
            'suspended' => false,
            'active' => true,
            'logo_upload' => Constants::LOGO_RK_B64,
            'responsible_id' => $this->user_user->id
        ];

        $company = $this->callStoreByUri($this->user_admin, 201, '/v1/companies', $data, 0)->decodeResponseJson()['data'];

        $this->assertDatabaseHas('media', [
            'model_type' => Company::class,
            'model_id' => $company['id']
        ]);
        $this->assertDatabaseHas('companies', [
            'id' => $company['id'],
            'name' => 'Company teste',
            'suspended' => false,
            'active' => true,
            'responsible_id' => $this->user_user->id
        ]);

    }

    public function testUpdateCompany()
    {
        $company = factory(Company::class)->create([
            'name' => 'Company teste',
            'suspended' => false,
            'active' => false,
            'responsible_id' => $this->user_user->id
        ]);

        $this->callUpdateByUri($this->user_admin, 200, '/v1/companies/' . $company->id, [
            'name' => 'Company Novo Nome',
            'suspended' => true,
        ], 0)->assertJsonFragment([
            'id' => $company->id,'name' => 'Company Novo Nome',
            'suspended' => true,
        ]);

        $this->assertDatabaseHas('companies', [
            'id' => $company->id,
            'name' => 'Company Novo Nome',
            'suspended' => true,
        ]);
    }

    public function testDestroyCompany()
    {
        $company = factory(Company::class)->create();

        $this->callDestroyByUri($this->user_admin, 200, "v1/companies/" . $company->id)
            ->assertJsonFragment(['message' => 'ok']);

        $company->refresh();
        $this->assertNotNull($company->deleted_at);
    }

    public function testIndexCompanies()
    {
        factory(Company::class, 2)->create([
            'name' => 'Empresa',
            'suspended' => false,
            'active' => false,
        ]);
        factory(Company::class, 2)->create([
            'name' => 'Prestador',
            'suspended' => true,
            'active' => true,
        ]);

        Address::factory([
            'city' => 'TesteA',
            'state' => 'RR',
            'related_id' => Company::first()->id,
            'related_type' => Company::class,
        ])->toArray();

        $this->callIndexByUri($this->user_admin, 200, '/v1/companies', ['address_city' => 'Test'], 1); // where like
        $this->callIndexByUri($this->user_admin, 200, '/v1/companies', ['address_state' => 'RR'], 1);

        $this->callIndexByUri($this->user_admin, 200, '/v1/companies', null, 4);
        $this->callIndexByUri($this->user_admin, 200, '/v1/companies', ['active' => true], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/companies', ['suspended' => true], 2);
        $this->callIndexByUri($this->user_admin, 200, '/v1/companies', ['name' => 'Empresa'], 2);


        $this->callIndexByUri($this->user_admin, 200, 'v1/companies', ['page' => 1, 'per_page' => 2], 2)
            ->assertJsonStructure(['meta' => ['pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages']]]);
    }

    public function testShowCompanies()
    {
        $company = factory(Company::class)->create();

        $this->callIndexByUri($this->user_admin, 200, '/v1/companies/' . $company->id)
            ->assertJsonStructure(['data' => [
                'id',// => (int)$company->id,
                'name',// => (string)$company->name,
                'headline',// => (string)$company->headline,
                'suspended',// => (bool)$company->suspended,
                'active',// => (bool)$company->active,
                'rating',// => (float)$company->calculateRating(),
                'rating_count',// => (int)$company->countRating(),
                'is_favorite',// => (bool)$company->isFavoritedBy(\Auth::getUser()),
                'min_value_to_free_deliver',// => (float)$company->min_value_to_free_deliver,
                'deliver_value',// => (float)$company->deliver_value,
                'deliver_estimate_time',// => (int)$company->deliver_estimate_time,
                'deliver_max_range',// => (int)$company->deliver_max_range,
                'categories',// => $company->categories->map(function (Category $cat) {
                'products',// => $company->products()->filter()->get()->map(function (Product $product) {
                'medias',// => $this->transformMedias($company),
                'mobile',// => (string)$company->mobile,
                'telephone',// => (string)$company->telephone
                'address_id',// => $company->address_id,
                'address',// => $company->address ? $company->address->transform(AddressTransformer::class) : null,
                'long',// => (string)$company->long,
                'lat',// => (string)$company->lat,
                'user_distance',// => $company->getAttribute('user_distance'), // Enviado pelo controller index
                'created_at',// => (string)$company->created_at,
                'updated_at',// => (string)$company->updated_at,
                'deleted_at',// => isset($company->deleted_at) ? (string)$company->deleted_at : null,
            ]]);
    }

}
