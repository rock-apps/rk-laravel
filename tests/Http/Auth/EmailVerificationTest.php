<?php

namespace Rockapps\RkLaravel\Tests\Http\Auth;

use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class EmailVerificationTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
    }

    public function test_activate_user_email()
    {
        /** @var User $user */
        $user = factory(User::class)->create(['verified' => false]);
        $this->assertEquals(false, $user->verified);
        $this->assertNotNull($user->verification_token);
        $response = $this->get("activation/email/{$user->verification_token}");
        $response->assertSuccessful();
        $user = $user->fresh();
        $this->assertEquals(true, $user->verified);
    }

}
