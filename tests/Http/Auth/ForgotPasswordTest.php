<?php

namespace Rockapps\RkLaravel\Tests\Http\Auth;

use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Notifications\UserResetPassword;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class ForgotPasswordTest extends RequestTestCase
{

    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();

        $this->user = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
    }

    protected function passwordRequestRoute()
    {
        return route('password.request');
    }

    protected function passwordEmailGetRoute()
    {
        return route('password.email');
    }

    protected function passwordEmailPostRoute()
    {
        return route('password.email');
    }

    protected function guestMiddlewareRoute()
    {
        return route('home');
    }

    public function testUserCanViewAnEmailPasswordForm()
    {

        $response = $this->get($this->passwordRequestRoute());

        $response->assertSuccessful();
        $response->assertViewIs('auth.passwords.email');
    }

    public function testUserCannotViewAnEmailPasswordFormWhenAuthenticated()
    {
        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->get($this->passwordRequestRoute());

        $response->assertRedirect($this->guestMiddlewareRoute());
    }

    public function testUserReceivesAnEmailWithAPasswordResetLink()
    {
        \Notification::fake();
        $user = factory(User::class)->create([
            'email' => 'john@example.com',
        ]);

        $response = $this->post($this->passwordEmailPostRoute(), [
            'email' => 'john@example.com',
        ]);

        $this->assertNotNull($token = \DB::table('password_resets')->first());
        \Notification::assertSentTo($user, UserResetPassword::class, function ($notification, $channels) use ($token) {
            return \Hash::check($notification->token, $token->token) === true;
        });
    }

    public function testUserDoesNotReceiveEmailWhenNotRegistered()
    {
        \Notification::fake();

        $response = $this->from($this->passwordEmailGetRoute())->post($this->passwordEmailPostRoute(), [
            'email' => 'nobody@example.com',
        ]);

        $response->assertRedirect($this->passwordEmailGetRoute());
        $response->assertSessionHasErrors('email');
        \Notification::assertNotSentTo(factory(User::class)->make(['email' => 'nobody@example.com']), UserResetPassword::class);
    }

    public function testEmailIsRequired()
    {
        $response = $this->from($this->passwordEmailGetRoute())->post($this->passwordEmailPostRoute(), []);

        $response->assertRedirect($this->passwordEmailGetRoute());
        $response->assertSessionHasErrors('email');
    }

    public function testEmailIsAValidEmail()
    {
        $response = $this->from($this->passwordEmailGetRoute())->post($this->passwordEmailPostRoute(), [
            'email' => 'invalid-email',
        ]);

        $response->assertRedirect($this->passwordEmailGetRoute());
        $response->assertSessionHasErrors('email');
    }
}
