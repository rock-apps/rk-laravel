<?php

namespace Rockapps\RkLaravel\Tests\Http\Auth;

use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Tests\RequestTestCase;

class LoginTest extends RequestTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration('/../..');

        (new \RkRoleTableSeeder())->run();
        (new \RkParameterSeeder())->run();

        $this->user = factory(User::class)->create(['email' => 'test@email.com', 'password' => '123456']);
    }

    public function testUserCanViewALoginForm()
    {
        $response = $this->get($this->loginGetRoute());

        $response->assertSuccessful();
        $response->assertViewIs('auth.login');
    }

    protected function loginGetRoute()
    {
        return route('login');
    }

    public function testUserCannotViewALoginFormWhenAuthenticated()
    {
        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->get($this->loginGetRoute());

        $response->assertRedirect($this->guestMiddlewareRoute());
    }

    protected function guestMiddlewareRoute()
    {
        return route('home');
    }

    public function testUserCanLoginWithCorrectCredentials()
    {
        $user = factory(User::class)->create([
            'password' => '12345678',
        ]);

        $response = $this->post($this->loginPostRoute(), [
            'email' => $user->email,
            'password' => '12345678',
        ]);

        $response->assertRedirect($this->successfulLoginRoute());
        $this->assertAuthenticatedAs($user,'web');
    }

    protected function loginPostRoute()
    {
        return route('login');
    }

    protected function successfulLoginRoute()
    {
        return route('home');
    }

    public function testRememberMeFunctionality()
    {
        $user = factory(User::class)->create([
            'password' => '12345678',
        ]);

        $response = $this->post($this->loginPostRoute(), [
            'email' => $user->email,
            'password' => '12345678',
            'remember' => 'on',
        ]);

        $user = $user->fresh();

        $response->assertRedirect($this->successfulLoginRoute());
        $response->assertCookie(\Auth::guard('web')->getRecallerName(), vsprintf('%s|%s|%s', [
            $user->id,
            $user->getRememberToken(),
            $user->password,
        ]));
        $this->assertAuthenticatedAs($user,'web');
    }

    public function testUserCannotLoginWithIncorrectPassword()
    {
        $user = factory(User::class)->create([
            'password' => '12345678',
        ]);

        $response = $this->from($this->loginGetRoute())->post($this->loginPostRoute(), [
            'email' => $user->email,
            'password' => 'invalid-password',
        ]);

        $response->assertRedirect($this->loginGetRoute());
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    public function testUserCannotLoginWithEmailThatDoesNotExist()
    {
        $response = $this->from($this->loginGetRoute())->post($this->loginPostRoute(), [
            'email' => 'nobody@example.com',
            'password' => 'invalid-password',
        ]);

        $response->assertRedirect($this->loginGetRoute());
        $response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    public function testUserCanLogout()
    {
        $this->be(factory(User::class)->create());

        $response = $this->get($this->logoutRoute());

        $response->assertRedirect($this->successfulLogoutRoute());
        $this->assertGuest('web');
    }

    protected function logoutRoute()
    {
        return route('logout');
    }

    protected function successfulLogoutRoute()
    {
        return '/';
    }

    public function testUserCannotLogoutWhenNotAuthenticated()
    {
        $response = $this->get($this->logoutRoute());

        $response->assertRedirect($this->successfulLogoutRoute());
        $this->assertGuest('web');
    }

    public function testUserCannotMakeMoreThanFiveAttemptsInOneMinute()
    {
        $user = factory(User::class)->create([
            'password' => '12345678',
        ]);

        foreach (range(0, 5) as $_) {
            $response = $this->from($this->loginGetRoute())->post($this->loginPostRoute(), [
                'email' => $user->email,
                'password' => 'invalid-password',
            ]);
        }

        $response->assertRedirect($this->loginGetRoute());
        $response->assertSessionHasErrors('email');
        $this->assertRegExp(
            $this->getTooManyLoginAttemptsMessage(),
            collect(
                $response
                    ->baseResponse
                    ->getSession()
                    ->get('errors')
                    ->getBag('default')
                    ->get('email')
            )->first()
        );
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

    protected function getTooManyLoginAttemptsMessage()
    {
        return sprintf('/^%s$/', str_replace('\:seconds', '\d+', preg_quote(__('auth.throttle'), '/')));
    }

}
