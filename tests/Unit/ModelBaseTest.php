<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\UnitTestCase;


class ModelBaseTest extends UnitTestCase
{
    use RefreshDatabase;

    public function test_factory_attributes()
    {
        $category = Category::factory([
            'name' => 'T1'
        ]);
        $this->assertInstanceOf(\Rockapps\RkLaravel\Models\Category::class, $category);
        $this->assertEquals('T1', $category->name);
    }

    public function test_factory_state()
    {
        /**
         * O teste é no model user mas o método é igual ao ModelBase
         */
        $user = User::factory([],1,[Constants::ROLE_USER]);
        $this->assertTrue($user->hasRole(Constants::ROLE_USER));
    }

    public function test_factory_make()
    {
        /**
         * O teste é no model user mas o método é igual ao ModelBase
         */
        $user = User::factory([],1,[],false);
        $this->assertNull($user->id);
    }

    public function test_factory_qty()
    {
        $category = Category::factory();
        $this->assertInstanceOf(\Rockapps\RkLaravel\Models\Category::class, $category);

        $categories = Category::factory([],10);
        $this->assertCount(10, $categories);
    }

    protected function setUp(): void
    {
        parent::setUp();
        (new \RkRoleTableSeeder())->run();
        (new \RkParameterSeeder())->run();
    }
}
