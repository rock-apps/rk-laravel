<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Console\Commands\UpdateOngoingSubscription;
use Rockapps\RkLaravel\Events\SubscriptionActivedManual;
use Rockapps\RkLaravel\Events\SubscriptionCreated;
use Rockapps\RkLaravel\Events\SubscriptionEnded;
use Rockapps\RkLaravel\Events\SubscriptionPaid;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMe;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\Subscription;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class SubscriptionUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function subscription_cli_update_manual_expiration()
    {
        /** @var Subscription $subscription */
        $subscription = factory(Subscription::class)->create([
            'status' => Subscription::STATUS_NOT_STARTED,
            'mode' => Subscription::MODE_SINGLE,
            'payment_method' => Subscription::PAYMENT_METHOD_FREE,
            'gateway' => Payment::GATEWAY_MANUAL,
            'current_period_end' => now()->addDays(1),
        ]);

        $subscription->current_period_start = now()->subDays(3);
        $subscription->current_period_end = now()->subDays(2);
        $subscription->save();

        $this->assertEquals(Subscription::STATUS_ACTIVE_MANUAL, $subscription->status);
        $this->assertTrue($subscription->isActive());

        $cli = new UpdateOngoingSubscription();
        $cli->handle();

        $subscription->refresh();
        $this->assertEquals(Subscription::STATUS_ENDED, $subscription->status);
        $this->assertFalse($subscription->isActive());
    }

    /** @test */
    function subscription_events()
    {
        $this->expectsEvents([
            SubscriptionCreated::class,
            SubscriptionActivedManual::class,
            SubscriptionEnded::class,
        ]);

        /** @var Subscription $subscription */
        $subscription = factory(Subscription::class)->create([
            'status' => Subscription::STATUS_NOT_STARTED,
            'mode' => Subscription::MODE_SINGLE,
            'payment_method' => Subscription::PAYMENT_METHOD_FREE,
            'gateway' => Payment::GATEWAY_MANUAL,
            'current_period_end' => now()->addDays(1),
        ]);
        $this->assertEquals(Subscription::STATUS_ACTIVE_MANUAL, $subscription->status);
        $this->assertTrue($subscription->isActive());

        $subscription->apply(Subscription::STATUS_CANCELED);
        $subscription->save();
        $this->assertFalse($subscription->isActive());
    }

    /** @test */
    function subscription_create_manual_expiration()
    {
        /** @var Subscription $subscription */
        $subscription = factory(Subscription::class)->states('free-lifetime-active')->create();
        $this->assertDatabaseHas('subscriptions', $subscription->getAttributes());
        $this->assertDatabaseHas('plans', $subscription->plan->getAttributes());
        $this->assertDatabaseHas('users', $subscription->subscriber->getAttributes());
        $this->assertDatabaseHas('users', $subscription->payer->getAttributes());
//        $this->assertEquals(Subscription::STATUS_NOT_STARTED, $subscription->status);
//        $this->assertFalse($subscription->isActive());
//        $subscription->apply(Subscription::STATUS_ACTIVE_MANUAL);
        $this->assertEquals(Subscription::STATUS_ACTIVE_MANUAL, $subscription->status);
        $this->assertTrue($subscription->isActive());

        $subscription->apply(Subscription::STATUS_CANCELED);
        $this->assertFalse($subscription->isActive());

    }

    /** @test */
    function subscription_create_with_pgm_cc()
    {
        $this->expectsEvents([
            SubscriptionPaid::class,
        ]);

        /** @var Subscription $subscription */
        $subscription = factory(Subscription::class)->states('pgm-credit-card')->create();
        $this->assertDatabaseHas('subscriptions', $subscription->getAttributes());
        $this->assertDatabaseHas('plans', $subscription->plan->getAttributes());
        $this->assertDatabaseHas('users', $subscription->subscriber->getAttributes());
        $this->assertDatabaseHas('users', $subscription->payer->getAttributes());
        $this->assertEquals(Subscription::STATUS_PAID, $subscription->status);
        $this->assertTrue($subscription->isActive());

        $pgm_subscription = PagarMe::initApi()->subscriptions()->get(['id' => $subscription->pgm_subscription_id]);
        $this->assertEquals('paid', $pgm_subscription->status);


        $subscription->apply(Subscription::STATUS_CANCELED);

        $this->assertFalse($subscription->isActive());

        $pgm_subscription = PagarMe::initApi()->subscriptions()->get(['id' => $subscription->pgm_subscription_id]);
        $this->assertEquals('canceled', $pgm_subscription->status);

        $subscription->syncWithGateway();
        $this->assertEquals($subscription->status, Subscription::STATUS_CANCELED);

        PagarMe::webhook([
            'id' => $subscription->pgm_subscription_id,
            'object' => 'subscription'
        ], true);

    }

    /** @test */
    function subscription_create_with_pgm_boleto()
    {
        /** @var Subscription $subscription */
        $subscription = factory(Subscription::class)->states('pgm-boleto')->create();
        $this->assertDatabaseHas('subscriptions', $subscription->getAttributes());
        $this->assertDatabaseHas('plans', $subscription->plan->getAttributes());
        $this->assertDatabaseHas('users', $subscription->subscriber->getAttributes());
        $this->assertDatabaseHas('users', $subscription->payer->getAttributes());
        $this->assertEquals(Subscription::STATUS_UNPAID, $subscription->status);
        $this->assertFalse($subscription->isActive());

        $pgm_subscription = PagarMe::initApi()->subscriptions()->get(['id' => $subscription->pgm_subscription_id]);


        PagarMe::initApi()->transactions()->simulateStatus([
            'id' => $pgm_subscription->current_transaction->id,
            'status' => 'paid'
        ]);
        \sleep(3);

        PagarMe::webhook([
            'id' => $subscription->pgm_subscription_id,
            'object' => 'subscription'
        ], true);

        $pgm_subscription = PagarMe::initApi()->subscriptions()->get(['id' => $subscription->pgm_subscription_id]);
        $this->assertEquals('paid', $pgm_subscription->status);

        PagarMe::webhook([
            'id' => $subscription->pgm_subscription_id,
            'object' => 'subscription'
        ], true);

        $subscription = $subscription->refresh();
        $this->assertEquals(Subscription::STATUS_PAID, $subscription->status);
        $this->assertTrue($subscription->isActive());


    }

    /** @test */
    function subscription_create_with_pgm_trial()
    {
        /** @var Subscription $subscription */
        $subscription = factory(Subscription::class)->states('pgm-trial')->create();
        $this->assertEquals(Subscription::STATUS_TRIALING, $subscription->status);
        $this->assertTrue($subscription->isActive());


    }


    /** @test */
    function subscription_has_renewed()
    {
        # TODO Testar com método checkRenewed e testar com CLI
        # TODO Testar também com uma subscription Manual / ou free para o voiceguru
        # TODO Um CLI diário para adicionar um order virtual unit de 3 min para os usuários que não possuem Compras na data de aniversário de cadastro ou no dia 1 do mês.


        /** @var Subscription $subscription */
        $subscription = factory(Subscription::class)->states('free-lifetime-active')->create();
        $this->assertDatabaseHas('subscriptions', $subscription->getAttributes());
        $this->assertDatabaseHas('plans', $subscription->plan->getAttributes());
        $this->assertDatabaseHas('users', $subscription->subscriber->getAttributes());
        $this->assertDatabaseHas('users', $subscription->payer->getAttributes());
//        $this->assertEquals(Subscription::STATUS_NOT_STARTED, $subscription->status);
//        $this->assertFalse($subscription->isActive());
//        $subscription->apply(Subscription::STATUS_ACTIVE_MANUAL);
        $this->assertEquals(Subscription::STATUS_ACTIVE_MANUAL, $subscription->status);
        $this->assertTrue($subscription->isActive());

        $subscription->apply(Subscription::STATUS_CANCELED);
        $this->assertFalse($subscription->isActive());

    }

    function setUp(): void
    {
        parent::setUp();
        (new \RkRoleTableSeeder())->run();
    }

}
