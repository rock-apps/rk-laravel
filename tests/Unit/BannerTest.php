<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\Banner;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Tests\UnitTestCase;


class BannerTest extends UnitTestCase
{
    use RefreshDatabase;

    public function test_banner()
    {
        /** @var Category $category */
        /** @var Banner $banner */
        $category = factory(Category::class)->create();
        $banner = factory(\Rockapps\RkLaravel\Models\Banner::class)->states(['with-media'])->create([
            'object_type' => get_class($category),
            'object_id' => $category->id,
        ]);
        $this->assertNotNull($banner->image);
        $this->assertCount(1,$banner->media);
        $this->assertInstanceOf(\Rockapps\RkLaravel\Models\Banner::class, $banner);
        $this->assertInstanceOf(Category::class, $banner->object);
    }

    protected function setUp(): void
    {
        parent::setUp();
        (new \RkRoleTableSeeder())->run();
        (new \RkParameterSeeder())->run();
    }
}
