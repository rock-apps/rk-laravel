<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Rockapps\RkLaravel\Helpers\Lalamove\LalamoveService;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class LalamoveUnitTest extends UnitTestCase
{
    public function testQuotationOrder()
    {
        $region = LalamoveService::MKT_BR_RIO;
        $body = [
            "serviceType" => LalamoveService::SERVICE_TYPE_LALAGO,
//            "scheduleAt" => '2020-20-20T00:00:00',
            "specialRequests" => [],
            "requesterContact" => [
                "name" => "test",
                "phone" => "5521994631994"
            ],
            "stops" => [
                [
                    "location" => [
                        "lat" => "-23.009856",
                        "lng" => "-43.309103"
                    ],
                    "addresses" => [
                        "pt_BR" => [
                            "displayString" => "Rua Manuel Brasiliense, 49",
                            "market" => $region
                        ]
                    ]
                ],
                [
                    "location" => [
                        "lat" => "-23.009445",
                        "lng" => "-43.319459"
                    ],
                    "addresses" => [
                        "pt_BR" => [
                            "displayString" => "Rua Jhon Kennedy, 240",
                            "market" => $region
                        ]
                    ]
                ]
            ],
            "deliveries" => [
                [
                    "toStop" => 1,
                    "toContact" => [
                        "name" => "dodo",
                        "phone" => "5521994631995"
                    ],
                    "remarks" => "Do not take this order - SANDBOX CLIENT TEST"
                ]
            ]
        ];

        $quotation = LalamoveService::quotation($body, $region);
        $this->assertGreaterThan(5, (float)$quotation->totalFee);
        $this->assertEquals('BRL', $quotation->totalFeeCurrency);

        $body['quotedTotalFee'] = [
            'amount' => $quotation->totalFee,
            'currency' => $quotation->totalFeeCurrency,
        ];
        $order = LalamoveService::postOrder($body, $region);

        $this->assertNotNull($order->orderRef);
        $this->assertEquals((float)$quotation->totalFee, $order->totalFee);
        $this->assertEquals('BRL', $order->totalFeeCurrency);

        $order_upd = LalamoveService::getOrderStatus($order->orderRef);

        $this->assertEquals("ASSIGNING_DRIVER", $order_upd->status);
        $this->assertNotNull($order_upd->shareLink);

        /**
         * Erro disparado ao cancelar
         *
         * Please verify if you have made the request with the right SANDBOX credentials. Contact Lalamove API Support if errors persist. Reference No.618514591e72c
         */
        $order_cancel = LalamoveService::cancelOrder($order->orderRef);
    }


    protected function setUp(): void
    {
        parent::setUp();
        (new \RkRoleTableSeeder())->run();
        (new \RkParameterSeeder())->run();
    }
}

