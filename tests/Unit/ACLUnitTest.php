<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\Permission;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class ACLUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function category_create_one()
    {
        $p1 = Permission::create(['name' => 'p1']);
        $r1 = Role::create(['name' => 'r1', 'system' => false]);

        $this->assertFalse($r1->hasPermission($p1->name));

        $r1->attachPermission($p1);
        $this->assertTrue($r1->hasPermission($p1->name));

        $r1->detachPermission($p1);
        $this->assertFalse($r1->hasPermission($p1->name));
    }

}
