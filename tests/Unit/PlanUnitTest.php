<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMe;
use Rockapps\RkLaravel\Models\Plan;
use Rockapps\RkLaravel\Models\Variant;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class PlanUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function plan_create_manual()
    {
        /** @var Plan $plan */
        $plan = factory(Plan::class)->create();
        $this->assertDatabaseHas('plans', $plan->getAttributes());
        $this->assertGreaterThan(0, $plan->value);
    }

    /** @test */
    function plan_update_manual()
    {
        /** @var Plan $plan */
        $plan = factory(Plan::class)->create();
        $plan->name = 'Power Plan';
        $plan->save();
        $this->assertDatabaseHas('plans', $plan->getAttributes());
        $this->assertEquals('Power Plan', $plan->name);
    }

    /** @test */
    function plan_create_and_update_pgm()
    {
        /** @var Plan $plan */
        $plan = factory(Plan::class)->state('pgm')->create();
        $plan->refresh(); // Necessário para atualizar os valores não immutables
        $this->assertDatabaseHas('plans', $plan->getAttributes());
        $this->assertGreaterThan(0, $plan->value);

        $plan->name = 'Pro Plan';
        $plan->save();
        $pgm_plan = PagarMe::initApi()->plans()->get(['id'=>$plan->pgm_plan_id]);
        $this->assertEquals($pgm_plan->id,$plan->pgm_plan_id);
        $this->assertEquals($pgm_plan->name,$plan->name);
    }

    /** @test */
    function test_plan_variant()
    {
        /** @var Plan $plan */
        $plan = factory(Plan::class)->states(['with-variant'])->create();

        $plan->refresh();
        $this->assertInstanceOf(Variant::class, $plan->variants[0]);

    }

}
