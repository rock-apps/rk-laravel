<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\Campaign;
use Rockapps\RkLaravel\Models\CampaignElement;
use Rockapps\RkLaravel\Models\CampaignHit;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class CampaignUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    public function testCampaign()
    {
        $company = factory(Company::class)->create();
        /** @var Campaign $campaign */
        $campaign = Campaign::factory(['company_id' => $company->id,]);
        $this->assertInstanceOf(Campaign::class, $campaign);


        /** @var Campaign $campaign */
        $campaign = Campaign::factory([
            'company_id' => $company->id,
        ], 1, ['with-elements', 'with-hits']);

        $hit = $campaign->hits[0];
        $element = $campaign->elements[0];

        $this->assertInstanceOf(Campaign::class, $campaign);

        $this->assertInstanceOf(CampaignElement::class, $element);
        $this->assertInstanceOf(Campaign::class, $element->campaign);

        $this->assertInstanceOf(CampaignHit::class, $hit);
        $this->assertInstanceOf(Campaign::class, $hit->campaign);
        $this->assertInstanceOf(CampaignElement::class, $hit->element);
    }

    public function testCampaignHit()
    {
        /** @var Campaign $campaign */
        $campaign = Campaign::factory([
            'company_id' => factory(Company::class)->create()->id,
        ], 1, ['with-elements', 'with-hits']);

        $user = User::factory();
        /** @var Campaign $campaign */
        $campaign = $campaign->addHit($user, $campaign->elements[0], CampaignHit::METHOD_VIEW);

        $this->assertGreaterThan(0, $campaign->actual_views);
        $this->assertEquals($campaign->actual_views, $campaign->hits()->onlyViews()->count());

        /** @var Campaign $campaign */
        $campaign = $campaign->addHit($user, $campaign->elements[0], CampaignHit::METHOD_CLICK);

        $this->assertGreaterThan(0, $campaign->actual_clicks);
        $this->assertEquals($campaign->actual_clicks, $campaign->hits()->onlyClicks()->count());
    }

    protected function setUp(): void
    {
        parent::setUp();
        (new \RkRoleTableSeeder())->run();
        (new \RkParameterSeeder())->run();
    }
}

