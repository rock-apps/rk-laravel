<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\Favorite;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class FavoriteUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function chat_create()
    {
        /** @var User $user1 */
        /** @var User $user2 */
        /** @var Company $company1 */
        /** @var Company $company2 */
        $user1 = \factory(User::class)->create();
        $user2 = \factory(User::class)->create();
        $company1 = \factory(Company::class)->create();
        $company2 = \factory(Company::class)->create();

        $this->assertTrue($user1->favorite($company1, true));
        $this->assertDatabaseHas('favorites', [
            'favoritable_type' => \get_class($company1),
            'favoritable_id' => $company1->id,
            'favoriter_type' => \get_class($user1),
            'favoriter_id' => $user1->id,
        ]);

        $favorite = Favorite::query()
            ->where('favoritable_type',\get_class($company1))
            ->where('favoritable_id',$company1->id)
            ->where('favoriter_type', \get_class($user1))
            ->where('favoriter_id', $user1->id)
            ->get()->first();

        $this->assertEquals($favorite->favoritable->id, $company1->id);
        $this->assertEquals($favorite->favoriter->id, $user1->id);


        $this->assertTrue($user1->favorite($company2, true));
        $this->assertTrue($user1->favorite($company2, true)); // Validação para ver se não vai contar dobrado
        $this->assertEquals(2, $user1->favorites()->count());
        $this->assertTrue($user1->favorite($company2, false));
        $this->assertEquals(1, $user1->favorites()->count());

        $this->assertTrue($user2->favorite($company1, true));
        $this->assertEquals(2, $company1->favorites()->count());

        $this->assertTrue($user1->favorite($company1, false));
        $this->assertEquals(1, $company1->favorites()->count());
        $this->assertDatabaseMissing('favorites', [
            'favoritable_type' => \get_class($company1),
            'favoritable_id' => $company1->id,
            'favoriter_type' => \get_class($user1),
            'favoriter_id' => $user1->id,
        ]);

        $this->assertTrue($user2->favorite($company2, true));
        $this->assertTrue($user2->hasFavorited($company2));
        $this->assertTrue($company2->isFavoritedBy($user2));

    }

    protected function setUp(): void
    {
        parent::setUp();
        (new \RkRoleTableSeeder())->run();
//        (new \RkParameterSeeder())->run();
    }
}
