<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Helpers\GoogleMapsDistanceMatrix;
use Rockapps\RkLaravel\Helpers\GoogleMapsGeoDecode;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class GeographicalUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    function geo_radius()
    {
        /** @var User $user */
        $user = \factory(User::class)->create();
        $user->latitude = -22.999375;
        $user->longitude = -43.351161;
        $user->save();

        $downtown_lat = -23.005063;
        $downtown_lng = -43.318374;

        /* Não funciona pois o SQLite não suporta as funções internas */
//        $user_in_radius = User::geofence($downtown_lat, $downtown_lng, 3, 5)->get();
//        $this->assertEquals(1, $user_in_radius->count());
    }

    /** @test */
    function geo_google_maps_distancematrix()
    {
        /** @var User $user */
        $user = \factory(User::class)->create();
        $user->latitude = -22.999375;
        $user->longitude = -43.351161;
        $user->save();

        $downtown_lat = -23.005063;
        $downtown_lng = -43.318374;

        /**
         * Precisa ativar a API Google. Pode demorar uns minutos até ativar,
         */
        $geo = new GoogleMapsDistanceMatrix();
        $geo->addOriginByLatLong($user->latitude, $user->longitude);
        $geo->addDestinationByLatLong($downtown_lat, $downtown_lng);
        $geo->calculate();

        $this->assertGreaterThanOrEqual(5000, $geo->getDistance()[0]);
        $this->assertGreaterThanOrEqual(9, $geo->getDurationInMinutes());
        $this->assertLessThanOrEqual(13, $geo->getDurationInMinutes());
//        $this->assertEquals([["hours" => 0, "minutes" => 11]], $geo->getDuration());

    }

    /** @test */
    function geo_google_maps_decode()
    {
        /** @var User $user */
        $user = \factory(User::class)->create();
        $user->latitude = -22.999375;
        $user->longitude = -43.351161;
        $user->save();

        /**
         * Precisa ativar a API Google. Pode demorar uns minutos até ativar,
         */
        $geo = new GoogleMapsGeoDecode();
        $geo->geoDecode('Av das américas', 4999, 'Rio de Janeiro', 'RJ', 'Brasil');
        $this->assertEquals(-23.0004335,$geo->getLat());
        $this->assertEquals(-43,(int)$geo->getLong()); // O long estava variando. Deixei fixo no int

    }

}
