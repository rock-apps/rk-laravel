<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class ConfigurationUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function settings_create()
    {
        /** @var User $user */
        $user = \factory(User::class)->create();

        $data = ['teste' => 1];
        $config = $user->saveConfiguration($data);
        $this->assertDatabaseHas('configurations', [
            'configurable_type' => \get_class($user),
            'configurable_id' => $user->id,
        ]);
        $this->assertEquals($user->id, $config->configurable->id);
        $user->refresh();

        $this->assertEquals(1, $user->getConfigurationKey('teste'));

        $data = ['teste2' => 'valor'];
        $user->saveConfiguration($data, false);
        $this->assertDatabaseHas('configurations', [
            'configurable_type' => \get_class($user),
            'configurable_id' => $user->id,
        ]);

        $this->assertEquals('valor', $user->getConfigurationKey('teste2'));
        $this->assertIsArray($user->configuration->configs);
        $this->assertCount(2, $user->configuration->configs);

    }
}
