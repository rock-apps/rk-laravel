<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class UserUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function user_create()
    {
        $user = factory(User::class)->create(['name' => 'John Doe', 'email' => 'jj@jj.com']);
        $this->assertEquals('John Doe', $user->name);
        $this->assertEquals('jj@jj.com', $user->email);
    }
}
