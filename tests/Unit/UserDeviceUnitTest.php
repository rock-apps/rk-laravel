<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Models\UserDevice;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class UserDeviceUnitTest extends UnitTestCase
{
    use RefreshDatabase;


    /** @test */
    function user_device_create()
    {
        $model = factory(UserDevice::class)->create();
        $this->assertGreaterThan(0, $model->id);
    }
    /** @test */
    function add_device_to_user()
    {
        $user = factory(User::class)->create();
        factory(UserDevice::class)->create(['user_id' => $user->id]);
        factory(UserDevice::class)->create(['user_id' => $user->id]);

        $this->assertEquals(2,$user->devices->count());
    }
}
