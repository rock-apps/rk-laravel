<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeCreditCard;
use Rockapps\RkLaravel\Models\CreditCard;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class CreditCardUnitTest extends UnitTestCase
{
    use RefreshDatabase;


    /** @test */
    function credit_card_create_in_pagar_me()
    {
        /** @var User $user */
        $user = factory(User::class)->state('pgm')->create();

        $number = '4242424242424242';
        $cvv = '123';
        $exp = '1223';

        $card1 = PagarMeCreditCard::create($user->name, $number, $exp, $cvv, $user);
        $this->assertNotNull($card1->pgm_card_id);
        $this->assertDatabaseHas('credit_cards', $card1->getAttributes());
        PagarMeCreditCard::create($user->name, $number, $exp, $cvv, $user);
        PagarMeCreditCard::create($user->name, $number, $exp, $cvv, $user);
        $card2 = PagarMeCreditCard::create($user->name, $number, $exp, $cvv, $user);
        try {
            PagarMeCreditCard::create($user->name, $number, $exp, $cvv, $user);
        } catch (ResourceException $e) {
            $this->assertEquals('Não é permitido ter mais do que 4 cartões cadastrados.', $e->getMessage());
        }
        $this->assertEquals(4, $user->creditCards->count());

    }

    /** @test */
    function credit_card_create_invalid()
    {
        /** @var User $user */
        $user = factory(User::class)->state('pgm')->create();

        $number = '4242424242424242';
        $cvv = '123';
        $exp = '1219';


        try {
        PagarMeCreditCard::create($user->name, $number, $exp, $cvv, $user);
        } catch (ResourceException $e) {
            $this->assertEquals('Este cartão está vencido.', $e->getMessage());
        }
        $this->assertEquals(0, $user->creditCards->count());

    }

    /** @test */
    function credit_card_set_default()
    {
        /** @var User $user */
        $user = factory(User::class)->state('pgm')->create();

        /** @var CreditCard $card1 */
        /** @var CreditCard $card2 */
        /** @var CreditCard $card3 */
        $card1 = factory(CreditCard::class)->create([
            'default' => 1,
            'owner_id' => $user->id,
            'owner_type' => User::class,
        ]);
        $card2 = factory(CreditCard::class)->create([
            'default' => 0,
            'owner_id' => $user->id,
            'owner_type' => User::class,
        ]);
        $card3 = factory(CreditCard::class)->create([
            'default' => 0,
            'owner_id' => $user->id,
            'owner_type' => User::class,
        ]);

        $card3->setDefault();
        $card3->save();
        $this->assertTrue($card3->default);

        $card2->refresh();
        $card1->refresh();
        $this->assertFalse($card2->default);
        $this->assertFalse($card1->default);

        $this->assertEquals(1, $user->creditCards()->whereDefault(1)->count());

    }

    protected function setUp(): void
    {
        parent::setUp();
        (new \RkRoleTableSeeder())->run();
//        (new \RkParameterSeeder())->run();
    }
}
