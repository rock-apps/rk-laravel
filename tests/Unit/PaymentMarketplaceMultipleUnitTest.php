<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Services\PaymentGatewayService;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class PaymentMarketplaceMultipleUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    function setUp(): void
    {
        parent::setUp();

        (new \RkRoleTableSeeder())->run();
    }

    /** @test */
    function payment_create_credit_card_in_a_company_api_pagar_me()
    {
        config([
            'PAGARME_SANDBOX' => null,
            'PAGARME_API_KEY_DEV' => null,
            'PAGARME_API_KEY_PROD' => null,
            'rk-laravel.marketplace.mode' => Constants::MARKETPLACE_MODE_MULTIPLE,
        ]);

        /** @var Company $company */
        $company = factory(Company::class)->state('with-payment-methods')->create();

        # As credenciais do marketplace precisam ser colocadas no controller
        PaymentGatewayService::setCredentials($company, Payment::GATEWAY_PAGARME_CC);

        $user = factory(User::class)->create();
        $address = factory(Address::class)->create([
            'related_type' => User::class,
            'related_id' => $user->id
        ]);

        $payment = new Payment([
            'value' => 150.50,
            'gateway' => Payment::GATEWAY_PAGARME_CC,
            'payer_id' => $user->id,
            'payer_type' => User::class,
            'address_id' => $address->id,
            'async' => false,
            'company_id' => $company->id
        ]);

        $payment->charge('PEDIDO-TESTE', null, [
            'card_holder_name' => $user->name,
            'card_expiration_date' => '1223',
            'card_number' => '4242424242424242',
            'card_cvv' => '123'
        ]);

        $this->assertNotNull($payment->company_id);
        $this->assertNotNull($payment->pgm_transaction_id);
        $this->assertTrue($payment->isPaid());
    }
}
