<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\Permission;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class RoleUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function roles_test_and_validation()
    {
        /** @var User $user */
        $user = factory(User::class)->create();

        $role_owner = new Role();
        $role_owner->name = 'owner';
        $role_owner->save();

        $role_buyer = new Role();
        $role_buyer->name = 'buyer';
        $role_buyer->save();

        $buy_post = new Permission();
        $buy_post->name = 'buy-post';
        $buy_post->save();
        $role_buyer->attachPermission($buy_post);

        $user->attachRole($role_buyer); // parameter can be an Role object, array, or id

        $this->assertTrue($user->hasRole(['buyer']));
        $this->assertFalse($user->hasRole(['owner']));
        $this->assertFalse($user->hasRole(['unknown']));

        $this->assertTrue($user->can(['buy-post']));
        $this->assertFalse($user->can(['unknown-permission']));

    }
}
