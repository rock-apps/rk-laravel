<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Post;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class CategoryUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function category_create_one()
    {
        $category = factory(Category::class)->create(['model_type' => Post::class]);
        /** @var Post $post */
        $post = factory(Post::class)->create();
        $post->syncCategories(collect($category));

        $this->assertEquals(1, $post->categories->count());

        $category2 = factory(Category::class)->create(['model_type' => Post::class]);
        $post->attachCategories($category2);
        $post->refresh();
        $this->assertEquals(2, $post->categories->count());


        $category3 = factory(Category::class)->create(['model_type' => Post::class]);
        $post->syncCategories($category3);
        $post->refresh();
        $this->assertEquals(1, $post->categories->count());

        $category3 = factory(Category::class)->create(['model_type' => Post::class]);
        $post->syncCategories([$category2->id, $category3->id]);
        $post->refresh();
        $this->assertEquals(2, $post->categories->count());

        $post->attachCategories([$category3->id]);
        $post->refresh();
        $this->assertEquals(2, $post->categories->count());
    }

    /** @test */
    function category_fail_to_add_from_another_model()
    {
        $category = factory(Category::class)->create(['model_type' => User::class]);
        /** @var Post $post */
        $post = factory(Post::class)->create();

        try {
            $post->attachCategories(collect($category));
        } catch (\Exception $exception) {
            $this->assertInstanceOf(ResourceException::class, $exception);
        }
        try {
            $post->attachCategories($category->id);
        } catch (\Exception $exception) {
            $this->assertInstanceOf(ResourceException::class, $exception);
        }
        $post->refresh();
        $this->assertEquals(0, $post->categories->count());

        $category = factory(Category::class)->create(['model_type' => Post::class]);
        $category2 = factory(Category::class)->create(['model_type' => Post::class]);
        $category3 = factory(Category::class)->create(['model_type' => Post::class]);
        $this->assertEquals(0, $post->categories->count());
        $post->attachCategories($category->id);
        $post->refresh();
        $this->assertEquals(1, $post->categories->count());
        $post->attachCategories($category2->id);
        $post->refresh();
        $this->assertEquals(2, $post->categories->count());
        $post->attachCategories(collect([$category3->id]));
        $post->refresh();
        $this->assertEquals(3, $post->categories->count());
    }


    /** @test */
    function category_has_subcategories()
    {
        /** @var Category $parent_category */
        /** @var Category $child_category */
        $parent_category = factory(Category::class)->create(['model_type' => Post::class]);
        factory(Category::class)->create(['model_type' => Post::class, 'parent_category_id' => $parent_category->id]);
        $child_category = factory(Category::class)->create(['model_type' => Post::class, 'parent_category_id' => $parent_category->id]);
        $child_child_category = factory(Category::class)->create(['model_type' => Post::class, 'parent_category_id' => $child_category->id]);

        $parent_category->refresh();
        $child_category->refresh();

        $this->assertEquals(2, $parent_category->childCategories->count());
        $this->assertInstanceOf(Category::class, $parent_category->childCategories[0]);
        $this->assertInstanceOf(Category::class, $parent_category->childCategories[1]);

        $this->assertInstanceOf(Category::class, $child_category->parentCategory);

        $this->assertInstanceOf(Category::class, $child_category->childCategories[0]);
    }

    /** @test */
    function category_subcategory_cant_add_nested()
    {
        /** @var Category $category_lvl_0 */
        /** @var Category $category_lvl_1 */
        /** @var Category $category_lvl_2 */
        $category_lvl_0 = factory(Category::class)->create(['model_type' => Post::class]);
        $category_lvl_1 = factory(Category::class)->create(['model_type' => Post::class, 'parent_category_id' => $category_lvl_0->id]);
        $category_lvl_2 = factory(Category::class)->create(['model_type' => Post::class, 'parent_category_id' => $category_lvl_1->id]);

        $category_lvl_0->refresh();

        try {
//            $this->expectException(ResourceException::class);
//            $this->expectExceptionMessage('A categoria pai não pode ser uma subcategoria');
            $category_lvl_0->parent_category_id = $category_lvl_1->id;
            $category_lvl_0->save();
        }catch (\Exception $e) {
            $this->assertEquals('A categoria pai não pode ser uma subcategoria',$e->getMessage());
        }

        try {
//            $this->expectException(ResourceException::class);
//            $this->expectExceptionMessage('A categoria pai não pode ser uma subcategoria');
            $category_lvl_0->parent_category_id = $category_lvl_2->id;
            $category_lvl_0->save();
        }catch (\Exception $e) {
            $this->assertEquals('A categoria pai não pode ser uma subcategoria',$e->getMessage());
        }
    }

    function category_subcategory_nested()
    {
        $category = factory(Category::class)->create(['model_type' => Post::class]);
        /** @var Post $post */
        $post = factory(Post::class)->create();
        $post->syncCategories(collect($category));

        $this->assertEquals(1, $post->categories->count());

        $category2 = factory(Category::class)->create(['model_type' => Post::class]);
        $post->attachCategories($category2);
        $post->refresh();
        $this->assertEquals(2, $post->categories->count());


        $category3 = factory(Category::class)->create(['model_type' => Post::class]);
        $post->syncCategories($category3);
        $post->refresh();
        $this->assertEquals(1, $post->categories->count());

        $category3 = factory(Category::class)->create(['model_type' => Post::class]);
        $post->syncCategories([$category2->id, $category3->id]);
        $post->refresh();
        $this->assertEquals(2, $post->categories->count());

        $post->attachCategories([$category3->id]);
        $post->refresh();
        $this->assertEquals(2, $post->categories->count());
    }

}
