<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Events\VoucherRedeemed;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Models\Product;
use Rockapps\RkLaravel\Models\ShippingMethod;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Models\Voucher;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class ShippingMethodUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    function setUp(): void
    {
        parent::setUp();

        (new \RkRoleTableSeeder())->run();
    }

    /** @test */
    function test_shipping_method_create()
    {
        /** @var ShippingMethod $method */

        $company = factory(Company::class)->create();
        $address = factory(\Rockapps\RkLaravel\Models\Address::class)->create([
            'related_type' => get_class($company),
            'related_id' => $company->id,
        ]);
        $method = factory(\Rockapps\RkLaravel\Models\ShippingMethod::class)->create([
            'carrier' => ShippingMethod::CARRIER_ADDRESS_PICKUP,
            'company_id' => $company->id,
            'address_id' => $address->id,
        ]);

        $this->assertInstanceOf(Company::class, $method->company);
        $this->assertInstanceOf(Address::class, $method->address);

    }

}
