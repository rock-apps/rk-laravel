<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Events\ChatMessageSent;
use Rockapps\RkLaravel\Helpers\Chat;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class ChatUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function chat_create()
    {
        $user1 = \factory(User::class)->create();
        $user2 = \factory(User::class)->create();

        app()['config']->set('rk-laravel.chat.trigger_events', true);
        app()['config']->set('musonza_chat.broadcasts', true);
        // O musonsa executaessa função de replace
        $this->expectsEvents(str_replace('\\', '.', \Rockapps\RkLaravel\Events\ChatMessageSent::class));

        $room = Chat::createConversation("CANAL1", "Chat Canal 1", null, [$user1]);
        $this->assertDatabaseHas('mc_conversations', ['id' => $room->id]);
        $this->assertDatabaseHas('mc_conversation_user', ['user_id' => $user1->id]);

        $msg1 = Chat::sendMessage('Teste Mensagem 1', $user1->id, $room->id);
        $this->assertDatabaseHas('mc_messages', ['id' => $msg1->id]);


        Chat::addParticipants($room->id, $user2->id);
        $this->assertDatabaseHas('mc_conversation_user', ['user_id' => $user2->id]);
        $msg2 = Chat::sendMessage('Teste Mensagem 2', $user2->id, $room->id);
        $this->assertDatabaseHas('mc_messages', ['id' => $msg2->id]);

        $this->assertDatabaseHas('mc_message_notification', ['user_id' => $user1->id,'message_id'=>$msg1->id]);
        $this->assertDatabaseHas('mc_message_notification', ['user_id' => $user1->id,'message_id'=>$msg2->id]);
        $this->assertDatabaseHas('mc_message_notification', ['user_id' => $user2->id,'message_id'=>$msg2->id]);
    }
}
