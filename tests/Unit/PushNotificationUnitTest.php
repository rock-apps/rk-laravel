<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Console\Commands\SendScheduledPushNotification;
use Rockapps\RkLaravel\Models\Banner;
use Rockapps\RkLaravel\Models\PushMessage;
use Rockapps\RkLaravel\Models\PushNotification;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class PushNotificationUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    function setUp(): void
    {
        parent::setUp();

        (new \RkRoleTableSeeder())->run();
    }

    function test_push_notification_cli_send_scheduled()
    {
        /** @var PushNotification $pn */
        $banner = factory(Banner::class)->create();
        $pn = factory(PushNotification::class)
            ->state('with-devices')
            ->create([
                'status' => PushNotification::STATUS_SCHEDULED,
                'object_type' => Banner::class,
                'object_id' => $banner->id,
                'scheduled_at' => now()->addMinutes(10)
            ]);

        $pn->refresh();
        $this->assertEquals(PushNotification::STATUS_SCHEDULED, $pn->status);
        $pn->scheduled_at = now()->subMinutes(5);
        $pn->save();

        $cli = new SendScheduledPushNotification();
        $cli->handle();
        $pn->refresh();
        $this->assertEquals(PushNotification::STATUS_SENDING, $pn->status);

        $this->assertInstanceOf(PushMessage::class, $pn->pushMessages()->first());
        $this->assertEquals(PushMessage::STATUS_SUCCESS, $pn->pushMessages()->get()[0]->status);

        $pn->pushMessages()->each(function(PushMessage $pm){
            $pm->status = PushMessage::STATUS_SUCCESS;
            $pm->save();
        });

        /**
         * Por algum motivo o states não está atualizando aqui Apesar o status realmente ter trocado
         * E foi verificado no PushMessage Boot
         */
//        $pn->fresh();
//        $this->assertEquals(PushNotification::STATUS_FINISHED, $pn->status);
    }


}
