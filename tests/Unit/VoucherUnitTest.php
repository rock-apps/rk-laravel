<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Events\VoucherRedeemed;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Models\Product;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Models\Voucher;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class VoucherUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    function setUp(): void
    {
        parent::setUp();

        (new Parameter([
            'key' => Constants::PGM_SOFT_DESCRIPTOR,
            'value' => 'RK',
            'type' => Parameter::TYPE_STRING,
        ]))->save();

        (new \RkRoleTableSeeder())->run();
    }

    /** @test */
    function test_voucher_create()
    {
        $this->expectsEvents([VoucherRedeemed::class]);
        /** @var Voucher $voucher */
        /** @var Product $product */

        $company = factory(Company::class)->create();
        $product = factory(\Rockapps\RkLaravel\Models\Product::class)->create([
            'company_id' => $company->id,
        ]);
        $voucher = factory(Voucher::class)->create([
            'model_type' => Product::class,
            'model_id' => $product->id,
            'max_redeem_qty' => 2,
            'company_id' => $company->id,
        ]);

        $user = factory(User::class)->states([Constants::ROLE_USER])->create();
        $user->redeemCode($voucher->code);

        $this->assertInstanceOf(Company::class, $voucher->company);
        $this->assertInstanceOf(Product::class, $voucher->model);
        $this->assertInstanceOf(User::class, $voucher->users()->first());

    }

    /** @test */
    function test_voucher_create_redeem_qty()
    {
        $this->expectsEvents([VoucherRedeemed::class]);
        /** @var Voucher $voucher */
        /** @var Product $product */

        $company = factory(Company::class)->create();
        $product = factory(\Rockapps\RkLaravel\Models\Product::class)->create([
            'company_id' => $company->id,
        ]);
        $voucher = factory(Voucher::class)->create([
            'model_type' => Product::class,
            'model_id' => $product->id,
            'max_redeem_qty' => 2,
        ]);

        $voucher->refresh();

        $this->assertDatabaseHas('vouchers', $voucher->getAttributes());
        $this->assertEquals(2, $voucher->max_redeem_qty);
        $this->assertEquals(2, $voucher->availableQty());
        factory(User::class)->states([Constants::ROLE_USER])->create()->redeemCode($voucher->code);

        $this->assertEquals(2, $voucher->max_redeem_qty);
        $this->assertEquals(1, $voucher->availableQty());

        factory(User::class)->states([Constants::ROLE_USER])->create()->redeemCode($voucher->code);
        $this->assertEquals(0, $voucher->availableQty());

        try {
            factory(User::class)->states([Constants::ROLE_USER])->create()->redeemCode($voucher->code);
        } catch (\Exception $e) {
            $this->assertEquals('Este código já atingiu o número máximo de utilizações.', $e->getMessage());
        }

    }

    /** @test */
    function test_voucher_re_use()
    {
        $this->expectsEvents([VoucherRedeemed::class]);
        /** @var Voucher $voucher */
        /** @var Product $product */

        $company = factory(Company::class)->create();
        $product = factory(\Rockapps\RkLaravel\Models\Product::class)->create([
            'company_id' => $company->id,
        ]);
        $voucher = factory(Voucher::class)->create([
            'model_type' => Product::class,
            'model_id' => $product->id,
            'max_redeem_qty' => 2,
        ]);

        $voucher->refresh();

        $user = factory(User::class)->states([Constants::ROLE_USER])->create();
        $user->redeemCode($voucher->code);

        try {
            $user->redeemCode($voucher->code);
        } catch (\Exception $e) {
            $this->assertEquals('Este código já foi utilizado.', $e->getMessage());
        }

    }

    /** @test */
    function test_voucher_create_redeem_expired()
    {
        /** @var Voucher $voucher */
        /** @var Product $product */

        $company = factory(Company::class)->create();
        $product = factory(\Rockapps\RkLaravel\Models\Product::class)->create([
            'company_id' => $company->id,
        ]);
        $voucher = factory(Voucher::class)->create([
            'model_type' => Product::class,
            'model_id' => $product->id,
            'expires_at' => now()->subDays(1),
        ]);

        $voucher->refresh();

        $this->assertNotNull($voucher->expires_at);

        try {
            factory(User::class)->states([Constants::ROLE_USER])->create()->redeemCode($voucher->code);
        } catch (\Exception $e) {
            $this->assertEquals('Este código já expirou.', $e->getMessage());
        }

    }

}
