<?php


namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\OrderVirtualUnit;
use Rockapps\RkLaravel\Models\Product;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class OrderVirtualUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    public function test_order()
    {
        $company = factory(Company::class)->create();
        $order = factory(OrderVirtualUnit::class)->create([
            'mode' => OrderVirtualUnit::MODE_MANUAL,
            'status' => OrderVirtualUnit::STATUS_CONFIRMED_PURCHASE,
            'product_id' => factory(Product::class)->state('apple-100min')->create(['company_id'=>$company->id])->id,
        ]);
        $this->assertInstanceOf(OrderVirtualUnit::class, $order);
        $this->assertInstanceOf(User::class, $order->user);
        $this->assertInstanceOf(Product::class, $order->product);
        $this->assertGreaterThan(0, $order->total_value);
        $this->assertEquals($order->total_value, $order->product->unit_value * $order->quantity);
        $this->assertEquals($order->virtual_units, $order->product->virtual_units_release);
        $this->assertEquals(60000, $order->virtual_units);
        $user = $order->user;
        $user->refresh();
        $this->assertEquals(60000, $user->virtual_unit_balance);

    }

    protected function setUp(): void
    {
        parent::setUp();
        (new \RkRoleTableSeeder())->run();
        (new \RkCompanyTableSeeder())->run();
        (new \RkProductTableSeeder())->run();
        (new \RkParameterSeeder())->run();
    }

}
