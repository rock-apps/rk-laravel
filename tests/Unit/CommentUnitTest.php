<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Models\Post;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class CommentUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function comment_create()
    {
        /** @var User $user */
        /** @var User $user2 */
        /** @var Post $post */
        $user = \factory(User::class)->create();
        $user2 = \factory(User::class)->create();
        $post = \factory(Post::class)->create();

        $comment = $user->comment($post, 'Bom post');
        $this->assertDatabaseHas('comments', [
            'commentable_type' => \get_class($post),
            'commentable_id' => $post->id,
            'commenter_type' => \get_class($user),
            'commenter_id' => $user->id,
        ]);
        $user->comment($post, 'Bom post 2');
        $this->assertEquals(2, $user->comments()->count());


        $this->assertEquals($comment->commentable->id, $post->id);
        $this->assertEquals($comment->commenter->id, $user->id);

        $this->assertTrue($post->isCommentedBy($user));
        $this->assertFalse($post->isCommentedBy($user2));
    }
    /** @test */
    function comment_create_on_comments()
    {
        /** @var User $user */
        /** @var User $user2 */
        /** @var Post $post */
        $user = \factory(User::class)->create();
        $user2 = \factory(User::class)->create();
        $post = \factory(Post::class)->create();

        $comment = $user->comment($post, 'Bom post');

        $comment2 = $user2->comment($comment, 'Réplica!');
        $comment3 = $user->comment($comment2, 'Tréplica!');

        $this->assertEquals(2,$user->comments()->count());
        $this->assertEquals(1,$user2->comments()->count());


    }
    /** @test */
    function comment_like()
    {
        /** @var Post $post */
        $post = \factory(Post::class)->create();
        /** @var User $user */
        $user = \factory(User::class)->create();

        $comment = $user->comment($post, 'Bom post');

        $this->assertEquals(0, $comment->likesCount());
        $user->like($comment);
        $this->assertEquals(1, $comment->likesCount());

    }
}
