<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Tests\UnitTestCase;


class CompanyTest extends UnitTestCase
{
    use RefreshDatabase;

    public function test_company()
    {
        /** @var Category $category */
        /** @var Company $company */
        $category = factory(Category::class)->create();
        $company = factory(\Rockapps\RkLaravel\Models\Company::class)->states(['with-media'])->create();
        $this->assertNotNull($company->logo);
        $this->assertCount(2,$company->media);
        $this->assertInstanceOf(\Rockapps\RkLaravel\Models\Company::class, $company);
    }

    protected function setUp(): void
    {
        parent::setUp();
        (new \RkRoleTableSeeder())->run();
        (new \RkParameterSeeder())->run();
    }
}
