<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class AddressUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function address_create_one()
    {
        /** @var Company $company */
        /** @var Address $address */
        $company = factory(Company::class)->create();
        $address = factory(Address::class)->create([
            'related_type' => Company::class,
            'related_id' => $company->id
        ]);

        $this->assertGreaterThan(0, $address->id);
        $this->assertEquals($address->id, $company->addresses()->first()->id);
        $this->assertInstanceOf(Company::class, $address->related);
    }

    /** @test */
    function addresses_create_many()
    {
        /** @var Company $company */
        $company = factory(Company::class)->create();

        $company->addresses()->create(
            factory(Address::class)->raw([
                'related_type' => Company::class,
                'related_id' => $company->id
            ])
        );
        $company->addresses()->create(
            factory(Address::class)->raw([
                'related_type' => Company::class,
                'related_id' => $company->id
            ])
        );

        $this->assertEquals(2, $company->addresses->count());
    }

    /** @test */
    function credit_address_set_default()
    {
        /** @var Company $company */
        $company = factory(Company::class)->create();

        /** @var Address $address1 */
        /** @var Address $address2 */
        /** @var Address $address3 */
        $address1 = factory(Address::class)->create([
            'default' => 1,
            'related_type' => Company::class,
            'related_id' => $company->id
        ]);
        $address2 = factory(Address::class)->create([
            'default' => 0,
            'related_type' => Company::class,
            'related_id' => $company->id
        ]);
        $address3 = factory(Address::class)->create([
            'default' => 0,
            'related_type' => Company::class,
            'related_id' => $company->id
        ]);

        $address3->setDefault();
        $address3->save();

        $address2->refresh();
        $address1->refresh();
        $this->assertFalse($address2->default);
        $this->assertFalse($address1->default);
    }

    protected function setUp(): void
    {
        parent::setUp();
        (new \RkRoleTableSeeder())->run();
//        (new \RkParameterSeeder())->run();
    }
}
