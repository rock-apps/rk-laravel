<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class ParameterUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function parameter_create()
    {
        $parameter = factory(Parameter::class)->create();
        $this->assertGreaterThan(0, $parameter->id);
    }
    /** @test */
    function parameter_create_get_key()
    {
        $parameter = factory(Parameter::class)->create(['key'=>'TESTE']);
        $this->assertEquals(Parameter::getByKey('TESTE')->id, $parameter->id);
    }
}
