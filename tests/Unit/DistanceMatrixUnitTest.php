<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\DistanceMatrix;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class DistanceMatrixUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function category_create_one()
    {
        $dm = DistanceMatrix::findByLatLongOrCreate(
            -22.999290, -43.360212,
            -23.004909, -43.319255,
            DistanceMatrix::CREATE_MODE_HAVERSINE
        );
        $this->assertEquals(6781, $dm->distance);

        $dm = DistanceMatrix::findByLatLongOrCreate(
            -22.999290, -43.360212,
            -23.004909, -43.319255,
            DistanceMatrix::CREATE_MODE_VINCENTY
        );
        $this->assertCount(2, DistanceMatrix::all());

        $this->assertEquals(6781, $dm->distance);

    }

}
