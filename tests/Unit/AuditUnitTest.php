<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use OwenIt\Auditing\Models\Audit;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Services\AuditService;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class AuditUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    function setUp(): void
    {
        parent::setUp();

        (new \RkRoleTableSeeder())->run();
    }

    /** @test */
    function test_track_attributes_create()
    {
        config()->set('audit.force_in_test', true);
        config()->set('audit.console', true);

        $company = factory(Company::class)->create(['name' => 'NOME1']);
        $this->assertEquals(2, $company->audits()->count());

        $company->name = 'NOME2';
        $company->save();
        $this->assertEquals(3, $company->audits()->count());

        $objects = AuditService::filterByAttribute($company, 'name', false);
        $this->assertInstanceOf(Audit::class, $objects[0]);

        $values = AuditService::filterByAttribute($company, 'name', true);

        $this->assertEquals(2, $values->count());

        $this->assertEquals(['NOME1', 'NOME2'], $values->toArray());

    }

}
