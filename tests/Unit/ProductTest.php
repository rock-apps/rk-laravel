<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Product;
use Rockapps\RkLaravel\Models\Variant;
use Rockapps\RkLaravel\Tests\UnitTestCase;


class ProductTest extends UnitTestCase
{
    use RefreshDatabase;

    public function test_product()
    {
        /** @var Category $category */
        /** @var Product $product */
        $category = factory(Category::class)->create();
        $company = factory(Company::class)->create();
        $product = factory(\Rockapps\RkLaravel\Models\Product::class)->states(['with-media'])->create([
            'company_id' => $company->id,
        ]);
        $this->assertNotNull($product->image);
        $this->assertCount(1, $product->media);
        $this->assertInstanceOf(\Rockapps\RkLaravel\Models\Product::class, $product);
        $this->assertInstanceOf(\Rockapps\RkLaravel\Models\Company::class, $product->company);
    }

    public function test_product_variant()
    {
        /** @var Product $product */
        $category = factory(Category::class)->create();
        $company = factory(Company::class)->create();
        $product = factory(\Rockapps\RkLaravel\Models\Product::class)->states(['with-variant'])->create([
            'company_id' => $company->id,
        ]);

        $product->refresh();
        $this->assertInstanceOf(Variant::class, $product->variants[0]);

    }

    protected function setUp(): void
    {
        parent::setUp();
        (new \RkRoleTableSeeder())->run();
        (new \RkParameterSeeder())->run();
    }
}
