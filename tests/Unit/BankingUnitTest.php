<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Helpers\Gerador;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMe;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeBankAccount;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeBankTransfer;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeCustomer;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeRecipient;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\BankAccount;
use Rockapps\RkLaravel\Models\BankTransfer;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class BankingUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function bank_account_create()
    {
        /** @var Company $company */
        /** @var BankAccount $bank */
        $company = factory(Company::class)->create();
        $bank = factory(BankAccount::class)->create([
            'owner_id' => $company->id,
            'owner_type' => Company::class,
        ]);
        $this->assertDatabaseHas('bank_accounts', $bank->toArray());
        $this->assertEquals($company->bankAccounts()->first()->id, $bank->id);
        $this->assertEquals($bank->owner->id, $company->id);
    }

    /** @test */
    function bank_account_create_in_pagar_me()
    {
        $company = factory(Company::class)->create();

        $bank = PagarMeBankAccount::create($company, 1, 123, null, 12345, 1, Gerador::CPF(), 'John Doe', BankAccount::TYPES[1]);
        $this->assertNotEmpty($bank->pgm_bank_id);
        $this->assertEquals($company->bankAccounts()->first()->id, $bank->id);

        /** @var BankAccount $bank2 */
        $bank2 = factory(BankAccount::class)->state('pgm')->create();
        $this->assertNotEmpty($bank2->pgm_bank_id);

    }

    /** @test */
    function bank_recipient_create_in_pagar_me()
    {
        /** @var Company $company */
        $company = factory(Company::class)->create();
        $cpf = Gerador::CPF();
        $bank = PagarMeBankAccount::create($company, 1, 123, null, 12345, 1, $cpf, 'John Doe', BankAccount::TYPES[1]);
        $this->assertNotEmpty($bank->pgm_bank_id);
        $this->assertEquals($company->bankAccounts()->first()->id, $bank->id);

        $recipient = PagarMeRecipient::save($bank, $company, false, 'monthly', 1);
        $this->assertNotEmpty($recipient->pgm_recipient_id);
        $this->assertEquals(Company::class, $recipient->related_type);
        $this->assertEquals($recipient->related_id, $company->id);
        $this->assertEquals($recipient->bank_account_id, $bank->id);
        $this->assertEquals(false, $recipient->transfer_enabled);
        $this->assertEquals('monthly', $recipient->transfer_interval);
        $this->assertEquals(1, $recipient->transfer_day);

        $company->refresh();

        // Update bank account and transfer info

        $bank2 = PagarMeBankAccount::create($company, 2, 1234, null, 12345, 1, $cpf, 'John Doe', BankAccount::TYPES[1]);
        $this->assertNotEmpty($bank2->pgm_bank_id);
        $this->assertEquals(2, $company->bankAccounts()->count());

        $recipient2 = PagarMeRecipient::save($bank2, $company, true, 'weekly', 2);

        $this->assertEquals($recipient->id, $recipient2->id);
        $this->assertEquals($recipient->pgm_recipient_id, $recipient2->pgm_recipient_id);

        $this->assertEquals(true, $recipient2->transfer_enabled);
        $this->assertEquals('weekly', $recipient2->transfer_interval);
        $this->assertEquals(2, $recipient2->transfer_day);

    }

    /** @test */
    function bank_transfer_create_manual()
    {
        /** @var Company $company */
        /** @var BankTransfer $transfer */
        $company = factory(Company::class)->create();
        $transfer = factory(BankTransfer::class)->state('manual')->create([
            'transfered_id' => $company->id,
            'transfered_type' => Company::class,
        ]);
        $this->assertDatabaseHas('bank_transfers', $transfer->toArray());
        $this->assertEquals($company->bankTransfers()->first()->id, $transfer->id);
        $this->assertEquals($transfer->transfered->id, $company->id);
        $this->assertEquals(BankTransfer::STATUS_TRANSFERRED, $transfer->status);
    }


    /** @test */
    function bank_transfer_create_manual_virtual_unita()
    {
        $user = factory(User::class)->state(Constants::ROLE_USER)->create();

        Parameter::firstOrCreate([
            'key' => \Rockapps\RkLaravel\Constants\Constants::VIRTUAL_UNIT_BANK_TRANSFER_TAX,
            'value_float' => 0.7,
            'type' => Parameter::TYPE_FLOAT,
            'name' => "Taxa de resgate de Unidades Virtuais",
            'description' => "Exemplo: 1000 Units * 0,7 Tax = R$ 700",
            'readonly' => false,
        ]);

        /** @var BankTransfer $transfer */
        $transfer = factory(BankTransfer::class)->state('manual')->create([
            'value' => 1000,
            'transfered_id' => $user->id,
            'transfered_type' => get_class($user),
        ]);

        $this->assertEquals(1000, $transfer->value);
        $this->assertEquals(1428, $transfer->virtual_units);
        $this->assertEquals(0.7, $transfer->virtual_units_tax);
        $this->assertDatabaseHas('bank_transfers', $transfer->toArray());
        $this->assertEquals(BankTransfer::STATUS_TRANSFERRED, $transfer->status);
    }

    /** @test */
    function bank_transfer_provision_in_pagar_me()
    {
        (new Parameter(['key' => Constants::PGM_SOFT_DESCRIPTOR, 'value' => 'RK', 'type' => Parameter::TYPE_STRING,]))->save();
        (new Parameter(['key' => Constants::PGM_BOLETO_FEE, 'value_float' => 3.80, 'type' => Parameter::TYPE_FLOAT]))->save();
        (new Parameter(['key' => Constants::PGM_DEFAULT_RECIPIENT_ID, 'value' => 're_ck0jyujt603rh5y6ejz593wv2', 'type' => Parameter::TYPE_STRING]))->save();

        /** @var User $user_main */
        /** @var Address $address_main */
        $user_main = factory(User::class)->state('pgm')->create(['can_pay_with_boleto' => true]);
        $address_main = factory(Address::class)->create(['related_type' => User::class, 'related_id' => $user_main->id]);
        PagarMeCustomer::save($user_main);

        /*
         * Boletão de provisionamento
         */
        $payment = new Payment([
            'value' => 1,
            'gateway' => Payment::GATEWAY_PAGARME_BOLETO,
            'direction' => Payment::DIRECTION_OUTCOME,
            'payer_id' => $user_main->id,
            'payer_type' => User::class,
            'address_id' => $address_main->id,
            'async' => false,
            'boleto_instructions' => 'Teste Teste',
        ]);
        $payment->save();
        $this->assertNotNull($payment->id);

        /*
         * Transferências Bancárias
         */

        $companies = collect();
        $transfers = collect();

        foreach ([1, 2, 3, 4] as $item) {

            /** @var Company $company */
            /** @var BankTransfer $transfer */
            $company = factory(Company::class)->create();
            $name = 'John Doe ' . now()->format('Ymd-His');
            $bank = PagarMeBankAccount::create($company, 237, 1234, null, 12345, 1, Gerador::CPF(), $name, BankAccount::TYPES[1]);
            PagarMeRecipient::save($bank, $company, true, 'weekly', 2);

            $transfer = factory(BankTransfer::class)->create([
                'transfered_id' => $company->id,
                'transfered_type' => Company::class,
                'pgm_transfer_id' => null,
                'pgm_transaction_id' => null,
                'gateway' => \Rockapps\RkLaravel\Models\BankTransfer::GATEWAY_PAGARME_PAYMENT_SPLIT,
                'comments_operator' => 'Faker',
                'bank_account_id' => $bank->id,
                'type' => \Rockapps\RkLaravel\Models\BankTransfer::TYPE_NEW,
                'status' => \Rockapps\RkLaravel\Models\BankTransfer::STATUS_REQUESTED,
                'fee' => 0,
                'value' => $item * 10,
                'payment_id' => $payment->id,
            ]);
            $companies->add($company);
            $transfers->add($transfer);
        }

        /*
         * Pagamento do Boleto de Provisionamento com SPLIT de pagamento para vários recebedores
         */

        $payment->refresh();
        $this->assertEquals(4, $payment->bankTransfers()->count());
        $this->assertEquals(10 + 20 + 30 + 40, $payment->bankTransfers()->sum('value'));

        $payment->value = $payment->bankTransfers()->sum('value');
        $payment->save();
        $payment->charge('Boleto Provisionamento');

        sleep(2);
        PagarMe::initApi()->transactions()->simulateStatus(['id' => $payment->pgm_transaction_id, 'status' => 'paid']);
        PagarMe::webhook(['id' => $payment->pgm_transaction_id, 'object' => 'transaction'], true);
        /** @var Payment $new_payment */
        $new_payment = Payment::where('pgm_transaction_id', $payment->pgm_transaction_id)->first();
        $this->assertEquals(Payment::STATUS_PAID, $new_payment->status);

        foreach ($new_payment->bankTransfers as $k => $bankTransfer) {
            $this->assertEquals(10 * ($k + 1), $bankTransfer->transfered->bankRecipient->getPgmBalance());
        }

        /*
         * Transferências Efetivadas
         */
        foreach ($payment->bankTransfers as $bankTransfer) {
            $this->assertEquals(BankTransfer::STATUS_REQUESTED, $bankTransfer->status);
            $bankTransfer = PagarMeBankTransfer::create($bankTransfer);
            $this->assertEquals(BankTransfer::STATUS_PENDING_TRANSFER, $bankTransfer->status);
        }

        /*
         * Validando o saldo zerado
         */
        foreach ($new_payment->bankTransfers as $bankTransfer) {
            $this->assertEquals(0, $bankTransfer->transfered->bankRecipient->getPgmBalance());
        }

    }

    protected function setUp(): void
    {
        parent::setUp();
        (new \RkRoleTableSeeder())->run();
//        (new \RkParameterSeeder())->run();
    }
}
