<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMe;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeCreditCard;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeCustomer;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\UnitTestCase;
use function mt_rand;

class PaymentUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    function setUp(): void
    {
        parent::setUp();

        (new Parameter([
            'key' => Constants::PGM_SOFT_DESCRIPTOR,
            'value' => 'RK',
            'type' => Parameter::TYPE_STRING,
        ]))->save();

        (new \RkRoleTableSeeder())->run();
    }

    /** @test */
    function payment_create()
    {
        /** @var Payment $payment */
        $payment = factory(Payment::class)->states('cash')->create();
        $this->assertDatabaseHas('payments', $payment->getAttributes());
        $this->assertGreaterThan(0, $payment->value);
        $this->assertFalse($payment->isPaid());


    }

    /** @test */
    function payment_create_new_then_charge_approve_then_cancel()
    {
        /** @var Payment $payment */

        $payment = factory(Payment::class)->state('cash')->create();
        $this->assertNull($payment->paid_at);
        $payment->charge('TEST_CASH');
        $this->assertFalse($payment->isPaid());
        $this->assertEquals(Payment::STATUS_NOT_STARTED, $payment->status);

        $payment->apply(Payment::STATUS_PAID);
        $this->assertTrue($payment->isPaid());

        $payment->apply(Payment::STATUS_CANCELED);
        $this->assertFalse($payment->isPaid());
        $this->assertEquals(Payment::STATUS_CANCELED, $payment->status);
    }

    /** @test */
    function payment_method_bank_not_allowed()
    {
        $user = factory(User::class)->create([
            'can_pay_with_bank' => false,
        ]);

        try {
            factory(Payment::class)->state('bank')->create([
                'payer_id' => $user->id,
                'payer_type' => User::class,
            ]);
        } catch (ResourceException $exception) {
            $this->assertEquals('O cliente não pode realizar pagamentos com transferência.', $exception->getMessage());
        }
    }

    /** @test */
    function payment_create_credit_card_in_pagar_me_success_sync()
    {
        $user = factory(User::class)->state('pgm')->create();
        $address = factory(Address::class)->create([
            'related_type' => User::class,
            'related_id' => $user->id
        ]);
        PagarMeCustomer::save($user);

        $card = PagarMeCreditCard::create($user->name, '4242424242424242', '1223', '123', $user);

        $payment = new Payment([
            'value' => 150.50,
            'gateway' => Payment::GATEWAY_PAGARME_CC,
            'payer_id' => $user->id,
            'payer_type' => User::class,
            'credit_card_id' => $card->id,
            'address_id' => $address->id,
            'async' => false,
        ]);
        $payment->charge("PEDIDO-TESTE");

        $this->assertEquals($user->payments()->first()->id, $payment->id);
        $this->assertNotNull($payment->id);
        $this->assertNotNull($payment->pgm_transaction_id);
        $this->assertTrue($payment->isPaid());

    }

    /** @test */
    function payment_create_credit_card_in_pagar_me_success_async()
    {
        $user = factory(User::class)->state('pgm')->create();
        $address = factory(Address::class)->create([
            'related_type' => User::class,
            'related_id' => $user->id
        ]);
        PagarMeCustomer::save($user);

        $card = PagarMeCreditCard::create($user->name, '4242424242424242', '1223', '123', $user);

        $payment = new Payment([
            'value' => 150.50,
            'gateway' => Payment::GATEWAY_PAGARME_CC,
            'payer_id' => $user->id,
            'payer_type' => User::class,
            'credit_card_id' => $card->id,
            'address_id' => $address->id,
            'async' => true,
        ]);
        $payment->charge("PEDIDO-TESTE");


        $this->assertEquals($user->payments()->first()->id, $payment->id);
        $this->assertNotNull($payment->id);
        $this->assertNotNull($payment->pgm_transaction_id);
        $this->assertFalse($payment->isPaid());
        $this->assertEquals(Payment::STATUS_PROCESSING, $payment->status);
        sleep(3);
        PagarMe::webhook([
            'id' => $payment->pgm_transaction_id,
            'object' => 'transaction'
        ], true);

        $new_p = Payment::where('pgm_transaction_id', $payment->pgm_transaction_id)->first();
        $this->assertEquals(Payment::STATUS_PAID, $new_p->status);
        $this->assertTrue($new_p->isPaid());

    }

    /** @test */
    function payment_create_pix_in_pagar_me_success_async()
    {
        $user = factory(User::class)->states(['pgm', 'pix'])->create();
        $address = factory(Address::class)->create([
            'related_type' => User::class,
            'related_id' => $user->id
        ]);
        PagarMeCustomer::save($user);

        $payment = new Payment([
            'value' => 150.50,
            'gateway' => Payment::GATEWAY_PAGARME_PIX,
            'payer_id' => $user->id,
            'payer_type' => User::class,
//            'credit_card_id' => $card->id,
            'address_id' => $address->id,
            'pix_expiration_date' => now()->addDays(1),
            'async' => true,
        ]);
        $payment->charge("PEDIDO-TESTE");

        $this->assertEquals($user->payments()->first()->id, $payment->id);
        $this->assertNotNull($payment->id);
        $this->assertNotNull($payment->pgm_transaction_id);
        $this->assertNotNull($payment->pix_qr_code);
        $this->assertFalse($payment->isPaid());
        $this->assertEquals(Payment::STATUS_WAITING_PAYMENT, $payment->status);
        sleep(3);
        PagarMe::initApi()->transactions()->simulateStatus([
            'id' => $payment->pgm_transaction_id,
            'status' => 'paid'
        ]);

        PagarMe::webhook([
            'id' => $payment->pgm_transaction_id,
            'object' => 'transaction'
        ], true);

        $new_p = Payment::where('pgm_transaction_id', $payment->pgm_transaction_id)->first();
        $this->assertEquals(Payment::STATUS_PAID, $new_p->status);
        $this->assertTrue($new_p->isPaid());

    }

    /** @test */
    function payment_create_credit_card_in_pagar_me_reject()
    {
        $user = factory(User::class)->state('pgm')->create();
        $address = factory(Address::class)->create([
            'related_type' => User::class,
            'related_id' => $user->id
        ]);
        PagarMeCustomer::save($user);

        $card = PagarMeCreditCard::create($user->name, '4444444444444444', '1225', '666', $user);

        $payment = new Payment([
            'value' => mt_rand(1, 100) + mt_rand(1, 99) / 100,
            'gateway' => Payment::GATEWAY_PAGARME_CC,
            'payer_id' => $user->id,
            'payer_type' => User::class,
            'credit_card_id' => $card->id,
            'address_id' => $address->id,
            'async' => false,
        ]);
        try {
            $payment->charge("PEDIDO-CARTAO");
        } catch (\Exception $e) {
            $this->assertEquals('O pagamento não foi aceito.', $e->getMessage());
        }

        $this->assertNotNull($payment->id);
        $this->assertNull($payment->paid_at);
        $this->assertNotNull($payment->pgm_transaction_id);
        $this->assertEquals(Payment::STATUS_REFUSED, $payment->status);
    }

    /** @test */
    function payment_create_boleto_in_pagar_me_success()
    {
        $user = factory(User::class)->state('pgm')->create([
            'can_pay_with_boleto' => true
        ]);
        $address = factory(Address::class)->create([
            'related_type' => User::class,
            'related_id' => $user->id
        ]);
        PagarMeCustomer::save($user);

        $payment = new Payment([
            'value' => mt_rand(100, 1000) + mt_rand(1, 99) / 100,
            'gateway' => Payment::GATEWAY_PAGARME_BOLETO,
            'payer_id' => $user->id,
            'payer_type' => User::class,
            'address_id' => $address->id,
            'async' => false,
//            'relateable_id' => $request->get('relateable_id'),
//            'relateable_type' => Order::class,
            'boleto_instructions' => 'Teste Teste',
//            'boleto_expiration_date' => '',
        ]);
        $payment->charge('PEDIDO-BOLETO');

        $this->assertNotNull($payment->id);
        $this->assertNull($payment->paid_at);
        $this->assertNotNull($payment->pgm_transaction_id);
//        $this->assertEquals(Payment::STATUS_PROCESSING, $payment->status); // Após a mudança no ChargeBoleto o status já vai direto
        $this->assertEquals(Payment::STATUS_WAITING_PAYMENT, $payment->status);

        sleep(2);
        PagarMe::initApi()->transactions()->simulateStatus([
            'id' => $payment->pgm_transaction_id,
            'status' => 'paid'
        ]);

        PagarMe::webhook([
            'id' => $payment->pgm_transaction_id,
            'object' => 'transaction'
        ], true);

        $new_payment = Payment::where('pgm_transaction_id', $payment->pgm_transaction_id)->first();
        $this->assertEquals(Payment::STATUS_PAID, $new_payment->status);
    }

    /** @test */
    function payment_refund_webhook()
    {
        /** @var Payment $payment */
        $payment = factory(Payment::class)->state('cc-paid')->create();
        $payment->charge('Teste Refund');
        $this->assertTrue($payment->isPaid());

        $transaction = PagarMe::initApi()->transactions()->refund([
            'id' => $payment->pgm_transaction_id,
        ]);
        PagarMe::webhook([
            'id' => $payment->pgm_transaction_id,
            'object' => 'transaction'
        ], true);

        $new_payment = Payment::where('pgm_transaction_id', $payment->pgm_transaction_id)->first();
        $this->assertEquals(Payment::STATUS_REFUNDED, $new_payment->status);
        $this->assertFalse($new_payment->isPaid());
    }

    /** @test */
    function payment_refund_manual()
    {
        /** @var Payment $payment */
        $payment = factory(Payment::class)->state('cc-paid')->create();
        $payment->charge('Teste Refund');
        $this->assertTrue($payment->isPaid());

        $payment->apply(Payment::STATUS_REFUNDED);

        $transaction = PagarMe::initApi()->transactions()->get([
            'id' => $payment->pgm_transaction_id,
        ]);

        $this->assertEquals('refunded', $transaction->status);

        $this->assertEquals(Payment::STATUS_REFUNDED, $payment->status);
        $this->assertFalse($payment->isPaid());
    }

    /** @test */
    function payment_bank_transfer_manual()
    {
        /** @var Payment $payment */
        $payment = factory(Payment::class)->state('bank')->create();
        $payment->charge('Teste Bank');
        $this->assertFalse($payment->isPaid());
        $this->assertEquals(Payment::STATUS_NOT_STARTED, $payment->status);

        $payment->apply(Payment::STATUS_PAID);
        $payment->save();

        $this->assertTrue($payment->isPaid());
    }

}
