<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\BankAccount;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\PaymentMethod;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class PaymentMethodUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    function setUp(): void
    {
        parent::setUp();

        (new \RkRoleTableSeeder())->run();
    }

    /** @test */
    function test_payment_method_create()
    {
        /** @var PaymentMethod $method */

        $company = factory(Company::class)->create();
        $account = factory(\Rockapps\RkLaravel\Models\BankAccount::class)->create([
            'owner_type' => get_class($company),
            'owner_id' => $company->id,
        ]);
        $method = factory(\Rockapps\RkLaravel\Models\PaymentMethod::class)->create([
            'gateway' => PaymentMethod::GATEWAY_MANUAL,
            'method' => Payment::GATEWAY_BANK_TRANSFER,
            'bank_account_id' => $account->id,
            'company_id' => $company->id,
        ]);

        $this->assertInstanceOf(Company::class, $method->company);
        $this->assertInstanceOf(BankAccount::class, $method->bankAccount);

    }

}
