<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Helpers\Meta\MetaBag;
use Rockapps\RkLaravel\Helpers\Meta\MetaMessage;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class MetaMessageUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    function test_geo_radius()
    {
        /** @var MetaBag $bag */
        $bag = app('meta-bag');
        $this->assertInstanceOf(MetaBag::class, $bag);

        $this->assertEmpty($bag->toArray());

        MetaMessage::addFlash('title', 'body');
        MetaMessage::addFlash('title');

        $this->assertCount(2, $bag->toArray());

        app('meta-bag')->clear();

        $this->assertEmpty($bag->toArray());
    }

}
