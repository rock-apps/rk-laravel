<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Models\Order;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class PurchaseableUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    function setUp(): void
    {
        parent::setUp();

        (new Parameter([
            'key' => Constants::PGM_SOFT_DESCRIPTOR,
            'value' => 'RK',
            'type' => Parameter::TYPE_STRING,
        ]))->save();

        (new \RkRoleTableSeeder())->run();
    }

    /** @test */
    function purchaseable_create()
    {
        /** @var Order $order */
        $order = factory(Order::class)->states(['company-with-bank', 'with-items'])->create();
        $order->refresh();
        $this->assertDatabaseHas('orders', $order->getAttributes());
        $this->assertGreaterThan(0, $order->total_value);
        $this->assertFalse($order->isConfirmed());
        $this->assertEquals(Order::STATUS_DRAFT, $order->status);
        $order->apply(Order::STATUS_PENDING_PURCHASE);
        $order->save();
        $this->assertEquals(Order::STATUS_PENDING_PURCHASE, $order->status);
        $order->apply(Order::STATUS_CONFIRMED_PURCHASE);
        $order->save();
        $this->assertEquals(Order::STATUS_CONFIRMED_PURCHASE, $order->status);
    }

    /** @test */
    function purchaseable_create_with_payment_and_balance()
    {
        /** @var User $user */
        $user = factory(User::class)->state('cash')->create();

        /** @var Order $order */
        $order = factory(Order::class)->states(['company-with-bank', 'with-items'])->create([
            'user_id'=>$user->id
        ]);
        $order->refresh();
        $payment = new Payment([
            'value' => 150.50,
            'gateway' => Payment::GATEWAY_CASH,
            'payer_id' => $user->id,
            'payer_type' => User::class,
            'purchaseable_type' => Order::class,
            'purchaseable_id' => $order->id,
            'change_purchaseable_state' => true,
        ]);
        $payment->charge('TEST_CASH');
        $this->assertDatabaseHas('payments', $payment->getAttributes());
        $this->assertFalse($payment->isPaid());
        $this->assertDatabaseHas('orders', $order->getAttributes());
        $this->assertFalse($order->isConfirmed());
        $this->assertEquals(Order::STATUS_DRAFT, $order->status);

        $this->assertEquals(0, $user->getBalance());

        $payment->apply(Payment::STATUS_PAID);
        $payment->save();
        $this->assertTrue($payment->isPaid());
        $order = $order->refresh();
        $this->assertTrue($order->isConfirmed());
        $this->assertEquals(Order::STATUS_CONFIRMED_PURCHASE, $order->status);

        $this->assertEquals(0, $user->getBalance());

        $payment->apply(Payment::STATUS_CANCELED);
        $payment->save();
        $this->assertFalse($payment->isPaid());
        $order = $order->refresh();
        $this->assertFalse($order->isConfirmed());
        $this->assertEquals(Order::STATUS_BLOCKED_PURCHASE, $order->status);

        $this->assertEquals(0, $user->getBalance());


        $payment = new Payment([
            'value' => 250,
            'gateway' => Payment::GATEWAY_CASH,
            'payer_id' => $user->id,
            'payer_type' => User::class,
        ]);
        $payment->charge('TEST_CASH');
        $payment->apply(Payment::STATUS_PAID);
        $payment->save();

        // A função getBalance retorna sempre Zero
        $this->assertEquals(0, $user->getBalance());

    }

}
