<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Helpers\Image;
use Rockapps\RkLaravel\Models\Post;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class MediableUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function meadiable_test()
    {
        /** @var Post $post */
        $post = \factory(Post::class)->create();


        $this->assertEquals(0, $post->getMedia()->count());

        $url = Image::storeMediable(Constants::LOGO_RK_B64, $post)->getFullUrl();
        $this->assertStringContainsString('https://', $url);

        $post->refresh();
        $this->assertEquals(1, $post->getMedia()->count());

        $this->assertStringContainsString('https://', $post->media()->first()->getFullUrl());
    }
}
