<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Models\Post;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class RatingUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function rating()
    {
        /** @var Post $post1 */
        /** @var Post $post2 */
        $post1 = \factory(Post::class)->create();
        $post2 = \factory(Post::class)->create();

        /** @var User $user1 */
        /** @var User $user2 */
        $user1 = \factory(User::class)->create();
        $user2 = \factory(User::class)->create();

        $user1->rate($post1, 4);
        $user2->rate($post1, 1);
        $this->assertEquals(2.5, $post1->ratingsAvg());
        $this->assertEquals(2, $post1->ratingsCount());

        $user1->rate($post2, 5);
        $this->assertEquals(2, $user1->rated()->count());

        $this->assertTrue($user1->isRated($post1));
        $this->assertFalse($user2->isRated($post2));
    }

    /** @test */
    function voting()
    {
        /** @var Post $post1 */
        /** @var Post $post2 */
        $post1 = \factory(Post::class)->create();
        $post2 = \factory(Post::class)->create();

        /** @var User $user1 */
        /** @var User $user2 */
        $user1 = \factory(User::class)->create();
        $user2 = \factory(User::class)->create();

        $user1->upVote($post1);
        $user2->upVote($post1);
        $user2->downVote($post1);
        $this->assertEquals(2, $post1->totalVotesCount());
        $this->assertEquals(1, $post1->upVotesCount());
        $this->assertEquals(1, $post1->downVotesCount());

        $user1->upVote($post2);
        $this->assertEquals(2, $user1->upVoted()->count());
    }

    /** @test */
    function likes()
    {
        /** @var Post $post1 */
        /** @var Post $post2 */
        $post1 = \factory(Post::class)->create();
        $post2 = \factory(Post::class)->create();

        /** @var User $user1 */
        /** @var User $user2 */
        $user1 = \factory(User::class)->create();
        $user2 = \factory(User::class)->create();

        $user1->like($post1);
        $user2->like($post1);
        $this->assertEquals(2, $post1->likesCount());
        $this->assertEquals(0, $post1->dislikesCount());
        $user2->dislike($post1);
        $this->assertEquals(2, $post1->likesDislikesCount());
        $this->assertEquals(1, $post1->dislikesCount());
        $this->assertEquals(1, $post1->likesCount());

        $user1->upVote($post2);
        $this->assertEquals(2, $user1->liked()->count());
        $this->assertEquals(1, $user2->disliked()->count());
    }
}
