<?php

namespace Rockapps\RkLaravel\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Models\PostBlock;
use Rockapps\RkLaravel\Models\Post;
use Rockapps\RkLaravel\Tests\UnitTestCase;

class PostUnitTest extends UnitTestCase
{
    use RefreshDatabase;

    /** @test */
    function test_block_relation()
    {
        /** @var Post $post1 */
        $post1 = \factory(Post::class)->create();
        $post_block = \factory(PostBlock::class)->create([
            'post_id' => $post1->id
        ]);
        $post1->refresh();
        $this->assertInstanceOf(PostBlock::class,$post1->blocks[0]);
        $this->assertInstanceOf(Post::class,$post_block->post);

    }

}
