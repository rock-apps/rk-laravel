<?php

namespace Rockapps\RkLaravel\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Rockapps\RkLaravel\Models\User;

class InvitedUserSignedUp implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $user;
    private $invited_user;

    public function __construct(User $user, User $invited_user)
    {
        $this->user = $user;
        $this->invited_user = $invited_user;
//        $this->user->notify(new \Rockapps\RkLaravel\Notifications\OrderApproved($order));
//        $this->invited_user->notify(new \Rockapps\RkLaravel\Notifications\OrderApproved($order));
    }

    public function broadcastOn()
    {
        return [
            new PrivateChannel('App.User.' . $this->user->id),
            new PrivateChannel('App.User.' . $this->invited_user->id)
        ];
    }
}
