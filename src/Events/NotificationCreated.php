<?php

namespace Rockapps\RkLaravel\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Transformers\DatabaseNotificationTransformer;


class NotificationCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var User */
    private $user;

    public function __construct(User $user)
    {
        /**
         * Envia um broadcast no channel do usuário com as notificações não lidas
         */
        $this->user = $user;
    }

    public function broadcastWith()
    {
        return \fractal($this->user->unreadNotifications, DatabaseNotificationTransformer::class)->toArray()['data'];
    }

    public function broadcastAs()
    {
        return 'NotificationCreated';
    }

    public function broadcastOn()
    {
        return [
            new PrivateChannel('App.User.' . $this->user->id),
        ];
    }
}
