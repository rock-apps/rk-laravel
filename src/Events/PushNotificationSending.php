<?php

namespace Rockapps\RkLaravel\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Rockapps\RkLaravel\Services\PushNotificationService;
use Rockapps\RkLaravel\Models\PushMessage;
use Rockapps\RkLaravel\Models\PushNotification;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Traits\States\PushNotificationStateTrait;
use Rockapps\RkLaravel\Transformers\PushNotificationTransformer;

class PushNotificationSending implements ShouldBroadcast, ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels, Queueable;

    /**
     * @var PushNotification
     */
    private $pushNotification;

    /**
     * PushNotificationSending constructor.
     * @param PushNotification|PushNotificationStateTrait $pushNotification
     */
    public function __construct(PushNotification $pushNotification)
    {
        $this->pushNotification = $pushNotification;

        /** @var PushNotificationService $service */
        $service_class = config('rk-laravel.push_notification.service', PushNotificationService::class);
        $service = new $service_class();

        /** @var User[] $users */
        $users = $service->findUsersByKey($pushNotification->destination);

        foreach ($users as $user) {
            foreach ($user->devices as $device) {
                $pushMessage = new PushMessage();
                $pushMessage->user_id = $user->id;
                $pushMessage->user_device_id = $device->id;
                $pushMessage->push_notification_id = $pushNotification->id;
                $pushMessage->save();
                $pushMessage->send();
            }
        }
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return $this->pushNotification->transform(PushNotificationTransformer::class);
    }

    public function broadcastOn()
    {
        return new PrivateChannel('App.Admin');
    }

    public function broadcastAs()
    {
        return 'PushNotificationSending';
    }

}
