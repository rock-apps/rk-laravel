<?php

namespace Rockapps\RkLaravel\Events;

use Rockapps\RkLaravel\Models\Order;
use Rockapps\RkLaravel\Models\OrderVirtualUnit;
use Rockapps\RkLaravel\Transformers\OrderTransformer;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Rockapps\RkLaravel\Transformers\OrderVirtualUnitTransformer;

class OrderVirtualUnitPending implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Order
     */
    private $order;

    public function __construct(OrderVirtualUnit $order)
    {
        $this->order = $order;
//        $order->company->responsible->notify(new \Rockapps\RkLaravel\Notifications\OrderVirtualUnitPending($order));
        $order->notify(new \Rockapps\RkLaravel\Notifications\OrderVirtualUnitPending($order));
    }

    public function broadcastWith()
    {
        return $this->order->transform(OrderVirtualUnitTransformer::class);
    }

    public function broadcastAs()
    {
        return 'OrderVirtualUnitPending';
    }

    public function broadcastOn()
    {
        return [
            new PrivateChannel('App.Admin'),
            new PrivateChannel('App.OrderVirtualUnit.' . $this->order->id)
        ];
    }
}
