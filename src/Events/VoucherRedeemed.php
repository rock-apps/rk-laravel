<?php

namespace Rockapps\RkLaravel\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Rockapps\RkLaravel\Models\OrderVirtualUnit;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\Subscription;
use Rockapps\RkLaravel\Models\Voucher;
use Rockapps\RkLaravel\Transformers\SubscriptionTransformer;
use Rockapps\RkLaravel\Transformers\VoucherTransformer;

class VoucherRedeemed implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;

    /** @var Voucher */
    public $voucher;

    public function __construct($user, Voucher $voucher)
    {
        $this->user = $user;
        $this->voucher = $voucher;

//        if (method_exists($user, 'notify')) {
//            $user->notify(new \Rockapps\RkLaravel\Notifications\SubscriptionRenewed($subscription));
//        }
    }

    public function broadcastWith()
    {
        return $this->voucher->transform(VoucherTransformer::class);
    }

    public function broadcastAs()
    {
        return 'VoucherRedeemed';
    }

    public function broadcastOn()
    {
        return [
            new PrivateChannel('App.Admin'),
            new PrivateChannel('App.Voucher.' . $this->voucher->id)
        ];
    }
}
