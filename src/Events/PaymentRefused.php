<?php

namespace Rockapps\RkLaravel\Events;

use Rockapps\RkLaravel\Interfaces\PayerInterface;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Musonza\Chat\Models\MessageNotification;

class PaymentRefused implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Payment $payment
     */
    protected $payment;
    private $message;
    /**
     * @var User|PayerInterface
     */
    private $payer;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Creates an entry in the message_notification table for each participant
     * This will be used to determine if a message is read or deleted.
     */
    public function createNotifications()
    {
        MessageNotification::make($this->message, $this->message->conversation);
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        $transformer = config('rk-laravel.payment.event_payment_refused_transformer');
        return fractal($this->payment, new $transformer())->toArray()['data'];
    }

    public function broadcastOn()
    {
        return new PrivateChannel('App.User.' . $this->payment->payer_id);
    }

    public function broadcastAs()
    {
        return 'PaymentPaid';
    }

}
