<?php

namespace Rockapps\RkLaravel\Events;

use Rockapps\RkLaravel\Models\Order;
use Rockapps\RkLaravel\Transformers\OrderTransformer;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderPaid implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Order
     */
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
        $order->user->notify(new \Rockapps\RkLaravel\Notifications\OrderPaid($order));
        $order->company->responsible->notify(new \Rockapps\RkLaravel\Notifications\OrderPaid($order));
        $order->notify(new \Rockapps\RkLaravel\Notifications\OrderPaid($order));
    }

    public function broadcastWith()
    {
        return $this->order->transform(OrderTransformer::class);
    }

    public function broadcastAs()
    {
        return 'OrderPaid';
    }

    public function broadcastOn()
    {
        return [
            new PrivateChannel('App.Admin'),
            new PrivateChannel('App.Order.' . $this->order->id)
        ];
    }
}
