<?php

namespace Rockapps\RkLaravel\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Musonza\Chat\Models\Message;
use Musonza\Chat\Models\MessageNotification;
use Rockapps\RkLaravel\Transformers\ChatMessageTransformer;

class ChatMessageSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;

    public function __construct(Message $message)
    {
        $this->message = $message;
        $this->createNotifications();
    }

    /**
     * Creates an entry in the message_notification table for each participant
     * This will be used to determine if a message is read or deleted.
     */
    public function createNotifications()
    {
        MessageNotification::make($this->message, $this->message->conversation);
    }

    /**
     * Get the data to broadcast.
     *
     * @return array|void
     */
    public function broadcastWith()
    {
        $transformer = config('rk-laravel.chat.chat_message_transformer', ChatMessageTransformer::class);
        return fractal($this->message, new $transformer())->toArray()['data'];
    }

    public function broadcastAs()
    {
        return 'ChatMessageSent';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array|PrivateChannel
     */
    public function broadcastOn()
    {
        $channel = $this->message->conversation->data['channel'];
        return new PrivateChannel($channel);
    }
}
