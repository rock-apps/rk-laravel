<?php

namespace Rockapps\RkLaravel\Events;

use App\Models\Admin;
use App\Models\Checkpoint;
use Rockapps\RkLaravel\Models\Order;
use Rockapps\RkLaravel\Models\OrderVirtualUnit;
use Rockapps\RkLaravel\Transformers\OrderTransformer;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Rockapps\RkLaravel\Transformers\OrderVirtualUnitTransformer;

class OrderVirtualUnitCanceled implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Order
     */
    private $order;

    public function __construct(OrderVirtualUnit $order)
    {
        $this->order = $order;

        $order->user->notify(new \Rockapps\RkLaravel\Notifications\OrderVirtualUnitCancelled($order));
//        $order->company->responsible->notify(new \Rockapps\RkLaravel\Notifications\OrderVirtualUnitCancelled($order));
        $order->notify(new \Rockapps\RkLaravel\Notifications\OrderVirtualUnitCancelled($order));
    }

    public function broadcastWith()
    {
        return $this->order->transform(OrderVirtualUnitTransformer::class);
    }

    public function broadcastAs()
    {
        return 'OrderVirtualUnitCanceled';
    }

    public function broadcastOn()
    {
        return [
            new PrivateChannel('App.Admin'),
            new PrivateChannel('App.OrderVirtualUnit.' . $this->order->id)
        ];
    }
}
