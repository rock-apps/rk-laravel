<?php

namespace Rockapps\RkLaravel\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Rockapps\RkLaravel\Models\BankTransfer;
use Rockapps\RkLaravel\Transformers\BankTransferTransformer;

class BankTransferFailed implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var BankTransfer $bankTransfer
     */
    protected $bankTransfer;

    public function __construct(BankTransfer $bankTransfer)
    {
        $this->bankTransfer = $bankTransfer;
    }

    public function broadcastWith()
    {
        return fractal($this->bankTransfer, new BankTransferTransformer())->toArray()['data'];
    }

    public function broadcastOn()
    {
        return new PrivateChannel('App.User.' . $this->bankTransfer->transfered_id);
    }

    public function broadcastAs()
    {
        return 'BankTransferFailed';
    }

}
