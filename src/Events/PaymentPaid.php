<?php

namespace Rockapps\RkLaravel\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Rockapps\RkLaravel\Models\Payment;

class PaymentPaid implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Payment $payment
     */
    protected $payment;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        $transformer = config('rk-laravel.payment.event_payment_paid_transformer');
        return fractal($this->payment, new $transformer())->toArray()['data'];
    }

    public function broadcastOn()
    {
        return new PrivateChannel('App.User.' . $this->payment->payer_id);
    }

    public function broadcastAs()
    {
        return 'PaymentPaid';
    }

}
