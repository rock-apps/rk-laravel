<?php

namespace Rockapps\RkLaravel\Events;

use App\Models\Admin;
use App\Models\Checkpoint;
use Rockapps\RkLaravel\Models\Order;
use Rockapps\RkLaravel\Transformers\OrderTransformer;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderCanceled implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Order
     */
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;

        $order->user->notify(new \Rockapps\RkLaravel\Notifications\OrderCancelled($order));
        $order->company->responsible->notify(new \Rockapps\RkLaravel\Notifications\OrderCancelled($order));
        $order->notify(new \Rockapps\RkLaravel\Notifications\OrderCancelled($order));
    }

    public function broadcastWith()
    {
        return $this->order->transform(OrderTransformer::class);
    }

    public function broadcastAs()
    {
        return 'OrderCanceled';
    }

    public function broadcastOn()
    {
        return [
            new PrivateChannel('App.Admin'),
            new PrivateChannel('App.Order.' . $this->order->id)
        ];
    }
}
