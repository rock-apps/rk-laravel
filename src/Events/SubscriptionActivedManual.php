<?php

namespace Rockapps\RkLaravel\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Rockapps\RkLaravel\Models\Subscription;
use Rockapps\RkLaravel\Transformers\SubscriptionTransformer;

class SubscriptionActivedManual implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Subscription
     */
    private $subscription;

    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;

        if (method_exists($subscription->subscriber, 'notify')) {
            $subscription->subscriber->notify(new \Rockapps\RkLaravel\Notifications\SubscriptionActivedManual($subscription));
        }
        return true;
    }

    public function broadcastWith()
    {
        return $this->subscription->transform(SubscriptionTransformer::class);
    }

    public function broadcastAs()
    {
        return 'SubscriptionActivedManual';
    }

    public function broadcastOn()
    {
        return [
            new PrivateChannel('App.Admin'),
            new PrivateChannel('App.Subscription.' . $this->subscription->id)
        ];
    }
}
