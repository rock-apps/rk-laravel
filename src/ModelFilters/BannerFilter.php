<?php

namespace Rockapps\RkLaravel\ModelFilters;

use Illuminate\Database\Eloquent\Builder;
use Rockapps\RkLaravel\Models\User;

class BannerFilter extends BaseModelFilter
{
    public $relations = [];
    protected $blacklist = ['active'];

    public function setup()
    {
        /** @var User $user */
        $user = \Auth::getUser();
        if ($user->hasRole('admin')) {
            $this->whitelistMethod('active');
        }else{
            $this->where('active', true);
        }
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function object($relate_id)
    {
        return $this->where('object_id', $relate_id);
    }

    public function objectType($relate_type)
    {
        return $this->where('object_type', $relate_type);
    }

    public function title($value)
    {
        return $this->where(function (Builder $q) use ($value) {
            return $q->where('title', 'LIKE', "%$value%");
        });
    }

    public function active($value)
    {
        return $this->where('active', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }

    public function position($value)
    {
        return $this->where('position', '=', $value);
    }
}
