<?php

namespace Rockapps\RkLaravel\ModelFilters;

use Carbon\Carbon;
use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Rockapps\RkLaravel\Exceptions\RequestValidationException;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Models\Variant;

class BaseModelFilter extends ModelFilter
{
    public function customSort($custom_sort)
    {
        $explode = explode(',', $custom_sort);

        $field = $explode[0];
        $direction = $explode[1];

        if (!$field) {
            return $this;
        }

        if (!in_array($direction, ['asc', 'desc'])) {
            throw new RequestValidationException('Sentido da ordenação inválido. Utilize asc ou desc.');
        }

        if (\Schema::hasColumn($this->getModel()->getTable(), $field)) {
            return $this->orderBy($field, $direction);
        }
        return $this;

    }


    public function status($value)
    {
        return $this->whereIn('status',explode(',', $value));
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function category($value)
    {
        if (!method_exists($this->getModel(), 'categories')) throw new RequestValidationException('Este recurso não possui categorias');
# TODO: Testar multiplas categorias no filtro com WHEREIN
        return $this->whereHas('categories', function (Builder $query) use ($value) {
            $query->where('category_id', $value);
        });
    }

    public function variant($values)
    {
        $values = explode(',',$values);
        foreach ($values as $value) {
            $variants = explode("|", $value);
            $variant = Variant::whereType($variants[0])->whereValue($variants[1])->first();
            $variantId = $variant ? $variant->id : 0;
            $this->whereHas('variants', function ($query) use ($variantId) {
                return $query->where('variant_id', $variantId);
            });
        }
        return $this;
    }
    public function variants($values){
        return $this->variant($values);
    }

    public function favorites($value)
    {
        if (!method_exists($this->getModel(), 'favorites')) throw new RequestValidationException('Este recurso não é favoritável');

        $class = get_class($this->getModel());
        $parts = explode(Str::lower($class), '\\');

        if (filter_var($value, FILTER_VALIDATE_BOOLEAN)) {
            $this->whereHas('favorites', function (Builder $query) use ($class, $parts) {
                return $query->where('favoritable_type', config("rk-laravel.$parts[0].model", $class))
                    ->where('favoriter_type', config('rk-laravel.user.model', User::class))
                    ->where('favoriter_id', \Auth::getUser()->id);
            });
        }
        return $this;
    }

    public function createdAtStart($value)
    {
        $date = Carbon::parse($value);
        if (!$date->isValid()) throw new RequestValidationException('Data Inválida (created_at)');

        return $this->where('created_at', '>=', $value);
    }

    public function createdAtEnd($value)
    {
        $date = Carbon::parse($value);
        if (!$date->isValid()) throw new RequestValidationException('Data Inválida (created_at)');

        return $this->where('created_at', '<=', $value);
    }

    public function updatedAtStart($value)
    {
        $date = Carbon::parse($value);
        if (!$date->isValid()) throw new RequestValidationException('Data Inválida (updated_at)');

        return $this->where('updated_at', '>=', $value);
    }

    public function updatedAtEnd($value)
    {
        $date = Carbon::parse($value);
        if (!$date->isValid()) throw new RequestValidationException('Data Inválida (updated_at)');

        return $this->where('updated_at', '<=', $value);
    }

    public function includeTrashed($value)
    {
        if (filter_var($value, FILTER_VALIDATE_BOOLEAN) && method_exists($this, 'withTrashed')) {
            return $this->withTrashed();
        }
        return $this;
    }

    public function onlyTrashed($value)
    {
        if (filter_var($value, FILTER_VALIDATE_BOOLEAN) && method_exists($this, 'withTrashed')) {
            return $this->whereNotNull('deleted_at');
        }
        return $this;
    }

    public function random($value)
    {
        return $this->inRandomOrder()->limit($value);
    }

    public function limit($value)
    {
        $value = filter_var($value, FILTER_VALIDATE_INT);
        if ($value > 0) {
            return parent::limit($value);
        }
        return $this;
    }

    public function createdAt($value)
    {
        return $this->whereDate('created_at', $value);
    }

    public function updatedAt($value)
    {
        return $this->whereDate('updated_at', $value);
    }

    private function prepareFieldValue($input): array
    {

        $explode = explode(',', $input);
        $field = preg_replace("/[^a-z_]/", "", $explode[0]); // Somente texto e _
        $value = $explode[1];

        if (!\Schema::hasColumn($this->getModel()->getTable(),$field))
        {
            throw new RequestValidationException("Este campo não existe: {$field}");
        }

        return ['field' => $field, 'value' => $value];
    }

    public function fieldStartAt($input)
    {
        $input = $this->prepareFieldValue($input);
        $date = Carbon::parse($input['value']);
        if (!$date->isValid()) throw new RequestValidationException("Data Inválida ({$input['field']})");

        return $this->whereDate($input['field'], '>=', $input['value']);
    }

    public function fieldEndAt($input)
    {
        $input = $this->prepareFieldValue($input);
        $date = Carbon::parse($input['value']);
        if (!$date->isValid()) throw new RequestValidationException("Data Inválida ({$input['field']})");

        return $this->whereDate($input['field'], '<=', $input['value']);
    }

    public function fieldMin($input)
    {
        $input = $this->prepareFieldValue($input);
        return $this->whereDate($input['field'], '>=', $input['value']);
    }

    public function fieldMax($input)
    {
        $input = $this->prepareFieldValue($input);
        return $this->whereDate($input['field'], '<=', $input['value']);
    }

    public function fieldLike($input)
    {
        $input = $this->prepareFieldValue($input);
        return $this->where($input['field'], 'like', "%".$input['value']."%");
    }

    public function whereLike($column, $value)
    {
        return $this->where($column, 'like', '%'.$value.'%');
    }

    public function orWhereLike($column, $value)
    {
        return $this->orWhere($column, 'like', '%'.$value.'%');
    }
}
