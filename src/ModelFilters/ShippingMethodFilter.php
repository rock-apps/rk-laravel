<?php

namespace Rockapps\RkLaravel\ModelFilters;

use Illuminate\Database\Eloquent\Builder;

class ShippingMethodFilter extends BaseModelFilter
{
    public $relations = [];

    public function setup()
    {
    }

    public function company($id)
    {
        return $this->where('company_id', $id);
    }

    public function title($value)
    {
        return $this->where(function (Builder $q) use ($value) {
            return $q->where('title', 'LIKE', "%$value%");
        });
    }

    public function active($value)
    {
        return $this->where('active', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }

    public function carrier($value)
    {
        return $this->where('carrier', '=', $value);
    }
}
