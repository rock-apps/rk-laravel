<?php

namespace Rockapps\RkLaravel\ModelFilters;


use Rockapps\RkLaravel\Models\User;

class BankAccountFilter extends BaseModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    public $blacklist = ['owner'];

    public function setup()
    {
        /** @var User $user */
        $user = \Auth::getUser();

        if (!$user) {
            return $this->where('owner_id', 0);
        }

        if ($user->hasRole('admin')) {
            return $this->whitelistMethod('owner')
                ->withTrashed();
        } else {
            return $this->where('owner_type',  config('rk-laravel.user.model',User::class))
                ->where('owner_id', $user->id);
        }
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function type($value)
    {
        return $this->where('type', $value);
    }

    public function gateway($value)
    {
        return $this->where('gateway', $value);
    }

    public function bankCode($value)
    {
        return $this->where('bank_code', $value);
    }

    public function owner($owner_id)
    {
        return $this->where('owner_id', $owner_id);
    }

    public function owner_type($owner_type)
    {
        return $this->where('owner_type', $owner_type);
    }

}
