<?php

namespace Rockapps\RkLaravel\ModelFilters;

use Rockapps\RkLaravel\Constants\Constants;

class OrderVirtualUnitFilter extends \Rockapps\RkLaravel\ModelFilters\BaseModelFilter
{
    public $relations = [];
    public $blacklist = ['user'];

    public function setup()
    {
        $user = \Auth::getUser();
        if ($user) {
            if ($user->hasRole(Constants::ROLE_ADMIN)) {
                return $this->whitelistMethod('user');
            } else {
                return $this->where('user_id', $user->id);
            }
        }

        return $this->where('user_id', 0);
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function description($value)
    {
        return $this->where('description', 'LIKE', "%$value%");
    }

    public function status($value)
    {
        return $this->where('status', '=', $value);
    }

    public function paymentGateway($value)
    {
        return $this->where('payment_gateway', '=', $value);
    }

    public function mode($value)
    {
        return $this->where('mode', '=', $value);
    }

    public function user($value)
    {
        return $this->where('user_id', '=', $value);
    }
}
