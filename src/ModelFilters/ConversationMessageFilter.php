<?php

namespace Rockapps\RkLaravel\ModelFilters;


class ConversationMessageFilter extends BaseModelFilter
{

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function body($value)
    {
        return $this->where('body', 'like', "%{$value}%");
    }

    public function conversation($value)
    {
        return $this->where('conversation_id', $value);
    }

    public function type($value)
    {
        return $this->where('type', $value);
    }

    public function user($value)
    {
        return $this->where('user_id', $value);
    }

}
