<?php

namespace Rockapps\RkLaravel\ModelFilters;



class UserFilter extends BaseModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function name($name)
    {
        return $this->where('name', 'LIKE', "%$name%");
    }
    public function email($email)
    {
        return $this->where('email', 'LIKE', "%$email%");
    }
    public function document($document)
    {
        return $this->where('document', 'LIKE', "%$document%");
    }
    public function gender($gender)
    {
        return $this->where('gender', 'LIKE', "%$gender%");
    }
    public function telephone($value)
    {
        return $this->where(function($q) use ($value)
        {
            return $q->where('telephone', 'LIKE', "%$value%")
                ->orWhere('mobile', 'LIKE', "%$value%");
        });
    }
    public function roles($roles)
    {
        $roles = explode(',', $roles);

        return $this->whereHas('roles', function ($query) use ($roles) {
            $query->whereIn('name', $roles);
        });
    }
    public function id($value)
    {
        return $this->where('id', $value);
    }

}
