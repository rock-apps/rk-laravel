<?php

namespace Rockapps\RkLaravel\ModelFilters;


use Rockapps\RkLaravel\Models\User;

class SubscriptionFilter extends BaseModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    protected $blacklist = ['subscriber','subscriberType'];
    public function setup()
    {
        /** @var User $user */
        $user = \Auth::getUser();
        if ($user && $user->hasRole('admin')) {
            $this->whitelistMethod('subscriber');
            $this->whitelistMethod('subscriberType');
        }else{
            $this->where('subscriber_id', $user->id);
            $this->where('subscriber_type',  config('rk-laravel.user.model',User::class));
            $this->scopes(['onlyActive']);
        }
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function onlyActive($value)
    {
        return $this->scopes(['onlyActive']);
    }

    public function status($value)
    {
        return $this->whereIn('status', explode(',', $value));
    }

    public function gateway($value)
    {
        return $this->whereIn('gateway', explode(',', $value));
    }

    public function paymentMethod($value)
    {
        return $this->whereIn('payment_method', explode(',', $value));
    }

    public function mode($value)
    {
        return $this->whereIn('mode', explode(',', $value));
    }

    public function currentPeriodStart($value)
    {
        return $this->whereDate('current_period_start', '>=', $value);
    }

    public function currentPeriodEnd($value)
    {
        return $this->whereDate('current_period_end', '<=', $value);
    }

    public function subscriber($value)
    {
        return $this->where('subscriber_id', $value);
    }

    public function subscriberType($value)
    {
        return $this->where('subscriber_type', $value);
    }

    public function plan($value)
    {
        return $this->where('plan_id', $value);
    }

}
