<?php

namespace Rockapps\RkLaravel\ModelFilters;



class RoleFilter extends BaseModelFilter
{
    public $relations = [];
    public $blacklist = [];

    public function name($value)
    {
        return $this->where(function ($q) use ($value) {
            return $q->where('name', 'LIKE', "%$value%")
                ->orWhere('display_name', 'LIKE', "%$value%")
                ->orWhere('description', 'LIKE', "%$value%");
        });
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function description($value)
    {
        return $this->where('description', 'LIKE', "%$value%");
    }

    public function system($value)
    {
        return $this->where('system', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }


}
