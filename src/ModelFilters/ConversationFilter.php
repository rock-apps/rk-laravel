<?php

namespace Rockapps\RkLaravel\ModelFilters;


class ConversationFilter extends BaseModelFilter
{
    public function setup()
    {

    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function q($value)
    {
        return $this->where('data', 'like', "%{$value}%");
    }

}
