<?php

namespace Rockapps\RkLaravel\ModelFilters;


class PostFilter extends BaseModelFilter
{
    public $relations = [];

    public function setup()
    {

    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function status($value)
    {
        return $this->where('status', $value);
    }

    public function type($value)
    {
        return $this->where('type', $value);
    }

}
