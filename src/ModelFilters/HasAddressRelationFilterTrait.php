<?php

namespace Rockapps\RkLaravel\ModelFilters;

use Illuminate\Database\Eloquent\Builder;

trait HasAddressRelationFilterTrait
{
    public function addressCity($value)
    {
        $this->whereHas('address', function (Builder $query) use ($value) {
            return $query->where('city', 'like', '%'.$value.'%');
        });
    }

    public function addressState($value)
    {
        $this->whereHas('address', function (Builder $query) use ($value) {
            return $query->where('state', $value);
        });
    }

    public function addressCountry($value)
    {
        $this->whereHas('address', function (Builder $query) use ($value) {
            return $query->where('country', 'like', '%'.$value.'%');
        });
    }
}
