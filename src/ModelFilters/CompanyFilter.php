<?php

namespace Rockapps\RkLaravel\ModelFilters;

use Illuminate\Database\Eloquent\Builder;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\User;

class CompanyFilter extends BaseModelFilter
{
    use HasAddressesRelationFilterTrait;

    public $relations = [];

    public function setup()
    {
//        $user = \Auth::getUser();
//        if ($user && $user->hasRole(Constants::ROLE_COMPANY)) {
//            return $this->where('responsible_id', $user->id);
//        }
//        if (!$user || ($user && $user->hasRole(Constants::ROLE_USER))) {
//            return $this
//                ->whereNotNull('recipient_id') // Verificar se está tudo ok
//                ->whereNotNull('bank_account_id') // Verificar se está tudo ok
//                ->where('active', 1)
//                ->where('suspended', 0)
//                ->whereNotNull('deliver_estimate_time')
//                ->whereNotNull('lat')
//                ->whereNotNull('long')
//                ->whereHas('products', function (Builder $query) {
//                    return $query->where('active', true);
//                })
//                ->whereHas('media')
//                ->whereHas('address')
//                $company->media()->count()
//                $company->responsible->bankRecipient->bankAccount()->whereNull('deleted_at')->count()
//                ;
//        }

    }

    public function id($value)
    {
        return $this->where('id', $value);
    }


    public function recent($value)
    {
        return $this->orderByDesc('created_at')->limit($value);
    }

    public function random($value)
    {
        return $this->inRandomOrder()->limit($value);
    }

    public function name($value)
    {
        return $this->where(function (Builder $q) use ($value) {
            return $q->where('name', 'LIKE', "%$value%")
                ->orWhere('headline', 'LIKE', "%$value%")
                ->orWhereHas('products', function (Builder $q) use ($value) {
                    return $q->where('name', 'LIKE', "%$value%")
                        ->where('active', true);
                });
        });
    }

    public function active($value)
    {
        return $this->where('active', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }

    public function suspended($value)
    {
        return $this->where('suspended', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }

    public function freeDeliver($value)
    {
        if (filter_var($value, FILTER_VALIDATE_BOOLEAN)) {
            return $this->where('deliver_value', '=', 0);
        }
        return $this;
    }

    public function favorites($value)
    {
        if (filter_var($value, FILTER_VALIDATE_BOOLEAN)) {
            $this->whereHas('favorites', function (Builder $query) {
                return $query->where('favoritable_type', config('rk-laravel.company.model', Company::class))
                    ->where('favoriter_type', config('rk-laravel.user.model', User::class))
                    ->where('favoriter_id', \Auth::getUser()->id);
            });
        }
        return $this;
    }

}
