<?php

namespace Rockapps\RkLaravel\ModelFilters;

use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;
use Rockapps\RkLaravel\Constants\Constants;

class CampaignFilter extends ModelFilter
{
    protected $blacklist = [];

    public function setup()
    {
        $user = \Auth::getUser();

        if ($user && $user->hasRole(Constants::ROLE_ADMIN)) {
            return $this;
        }else if ($user && $user->company_id) {
            return $this->where('company_id', $user->company_id);
        }
        return $this->where('company_id', 0);
    }

    public function name($value)
    {
        return $this->where(function (Builder $q) use ($value) {
            return $q->where('name', 'LIKE', "%$value%");
        });
    }

    public function active($value)
    {
        return $this->where('active', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }
}

