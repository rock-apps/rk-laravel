<?php

namespace Rockapps\RkLaravel\ModelFilters;


use Rockapps\RkLaravel\Models\User;

class BankTransferFilter extends BaseModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    public $blacklist = ['transfered'];

    public function setup()
    {
        /** @var User $user */
        $user = \Auth::getUser();

        if (!$user) {
            $this->where('transfered_type',  config('rk-laravel.user.model',User::class));
            $this->where('transfered_id', 0);
        }

        if ($user->hasRole('admin')) {
            $this->whitelistMethod('transfered');
//            $this->withTrashed();
        } else {
            $this->where('transfered_type',  config('rk-laravel.user.model',User::class));
            $this->where('transfered_id', $user->id);
        }
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function transfered($owner_id)
    {
        return $this->where('transfered_id', $owner_id);
    }

    public function transfered_type($owner_type)
    {
        return $this->where('transfered_type', $owner_type);
    }

    public function status($value)
    {
        return $this->whereIn('status', explode(',', $value));
    }

    public function startAt($value)
    {
        return $this->whereDate('created_at', '>=', $value);
    }

    public function endAt($value)
    {
        return $this->whereDate('created_at', '<=', $value);
    }
}
