<?php

namespace Rockapps\RkLaravel\ModelFilters;



class VoucherFilter extends BaseModelFilter
{
    public $relations = [];
    public $blacklist = [];

    public function setup()
    {

    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function code($value)
    {
        return $this->where('code', 'LIKE', "%$value%");
    }
    public function model_type($value)
    {
        return $this->where('model_type', '=', "%$value%");
    }

    public function model_id($value)
    {
        return $this->where('model_id', '=', $value);
    }

    public function company($value)
    {
        return $this->where('company_id', '=', $value);
    }

    public function data($value)
    {
        return $this->where('data', '=', $value);
    }
    public function expiresAt($value)
    {
        return $this->whereDate('expires_at', '=', $value);
    }
}
