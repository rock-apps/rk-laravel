<?php

namespace Rockapps\RkLaravel\ModelFilters;


use Rockapps\RkLaravel\Models\User;

class AddressFilter extends BaseModelFilter
{
    public $relations = [];

    /**
     * O método não é RELATED pois já era um método existente no ModelFilter
     */
    public $blacklist = ['relate'];

    public function setup()
    {
        /** @var User $user */
        $user = \Auth::getUser();

        if (!$user) {
            return $this->where('owner_id', 0);
        }

        if ($user->hasRole('admin')) {
            return $this->whitelistMethod('relate')
                ->withTrashed();
        } else {
            return $this->where('related_type', config('rk-laravel.user.model', User::class))
                ->where('related_id', $user->id);
        }
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function relate($relate_id)
    {
        return $this->where('related_id', $relate_id);
    }

    public function relate_type($relate_type)
    {
        return $this->where('related_type', $relate_type);
    }

}
