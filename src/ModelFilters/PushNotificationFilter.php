<?php

namespace Rockapps\RkLaravel\ModelFilters;


use Rockapps\RkLaravel\Models\User;

class PushNotificationFilter extends BaseModelFilter
{
    public $relations = [];

    protected $blacklist = ['isActive'];

    public function setup()
    {
        /** @var User $user */
        $user = \Auth::getUser();
//        if ($user->hasRole('admin')) {
//            $this->withTrashed();
//        }
    }

    public function title($value)
    {
        return $this->where('title', 'LIKE', "%$value%");
    }
    public function objectType($value)
    {
        return $this->where('object_type', $value);
    }

    public function objectId($value)
    {
        return $this->where('object_id', $value);
    }

    public function destination($value)
    {
        return $this->where('destination', $value);
    }

    public function type($value)
    {
        return $this->where('type', $value);
    }

    public function status($value)
    {
        return $this->where('status', $value);
    }

    public function scheduledAtStart($value)
    {
        return $this->whereDate('scheduled_at', '>=', $value);
    }

    public function scheduledAtEnd($value)
    {
        return $this->whereDate('scheduled_at', '<=', $value);
    }

}
