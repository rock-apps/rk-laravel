<?php

namespace Rockapps\RkLaravel\ModelFilters;


use Rockapps\RkLaravel\Models\User;

class ConfigurationFilter extends BaseModelFilter
{
    public $relations = [];

    public function setup()
    {
        /** @var User $user */
        $user = \Auth::getUser();

        if (!$user) {
            $this->where('configurable_type', config('rk-laravel.user.model', User::class));
            $this->where('configurable_id', 0);
        } else {
            $this->where('configurable_type', config('rk-laravel.user.model', User::class));
            $this->where('configurable_id', $user->id);
        }
    }

}
