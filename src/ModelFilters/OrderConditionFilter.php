<?php

namespace Rockapps\RkLaravel\ModelFilters;

use App\Constants\Constant;
use App\Models\Company;
use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;
use Rockapps\RkLaravel\Constants\Constants;

class OrderConditionFilter extends BaseModelFilter
{
    public $relations = [];

    public function setup()
    {
        $user = \Auth::getUser();

        if (!$user) return null;

        if ($user->hasRole(Constants::ROLE_USER)) {
            $this->whereHas('order', function (Builder $query) use ($user) {
                $query->where('user_id', $user->id);
            });
        }

        if ($user->hasRole(Constants::ROLE_COMPANY)) {
            $this->whereHas('order', function (Builder $query) use ($user) {
                $query->whereHas('company', function (Builder $query) use ($user) {
                    $query->where('responsible_id', $user->id);
                });
            });
        }
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function company($value)
    {
        return $this->where('company_id', $value);
    }

    public function category($value)
    {
        return $this->where('category_id', $value);
    }

    public function name($value)
    {
        return $this->where(function (Builder $q) use ($value) {
            return $q->where('name', 'LIKE', "%$value%")
                ->orWhere('description', 'LIKE', "%$value%");
        });
    }

    public function active($value)
    {
        return $this->where('active', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }
}
