<?php

namespace Rockapps\RkLaravel\ModelFilters;

use Illuminate\Database\Eloquent\Builder;
use Rockapps\RkLaravel\Constants\Constants;

class ProductFilter extends BaseModelFilter
{
    public $relations = [];
    public $blacklist = ['active'];

    public function setup()
    {
        $user = \Auth::getUser();

        if (!$user){

            return $this->where('active',true)
                ->whereNull('deleted_at');

        } elseif ($user->hasRole(Constants::ROLE_USER)) {

            return $this->where('active',true)
                ->whereNull('deleted_at');

        }else if ($user->hasRole(Constants::ROLE_ADMIN)) {

            return $this->whitelistMethod('active');

        } else if ($user->hasRole(Constants::ROLE_COMPANY)) {

            $this->where('company_id', $user->company->id);

        }

        return $this;
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function active($value)
    {
        return $this->where('active', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }

    public function activeIos($value)
    {
        return $this->where('active_ios', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }
    public function activeAndroid($value)
    {
        return $this->where('active_android', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }

    public function company($value)
    {
        return $this->where('company_id', $value);
    }

    public function name($value)
    {
        return $this->where(function (Builder $q) use ($value) {
            return $q->where('name', 'LIKE', "%$value%")
                ->orWhere('description', 'LIKE', "%$value%");
        });
    }
}
