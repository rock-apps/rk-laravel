<?php

namespace Rockapps\RkLaravel\ModelFilters;


use Rockapps\RkLaravel\Models\Plan;
use Rockapps\RkLaravel\Models\User;

class PlanFilter extends BaseModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    protected $blacklist = ['active'];

    public function setup()
    {
        /** @var User $user */
        $user = \Auth::getUser();
        if ($user->hasRole('admin')) {
            $this->whitelistMethod('active');
        }else{
            $this->where('active', true);
        }
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function name($value)
    {
        return $this->where(function ($q) use ($value) {
            return $q->where('name', 'LIKE', "%$value%")
                ->orWhere('description', 'LIKE', "%$value%");
        });
    }

    public function active($value)
    {
        return $this->where('active', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }

    public function gateway($value)
    {
        return $this->where('gateway', '=', $value);
    }

    public function activeIos($value)
    {
        return $this->where('active_ios', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }
    public function activeAndroid($value)
    {
        return $this->where('active_android', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }

}
