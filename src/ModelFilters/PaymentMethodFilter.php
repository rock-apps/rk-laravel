<?php

namespace Rockapps\RkLaravel\ModelFilters;

use Illuminate\Database\Eloquent\Builder;

class PaymentMethodFilter extends BaseModelFilter
{
    public $relations = [];

    public function setup()
    {
    }

    public function company($id)
    {
        return $this->where('company_id', $id);
    }

    public function title($value)
    {
        return $this->where(function (Builder $q) use ($value) {
            return $q->where('title', 'LIKE', "%$value%");
        });
    }

    public function active($value)
    {
        return $this->where('active', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }

    public function gateway($value)
    {
        return $this->where('gateway', '=', $value);
    }

    public function method($value)
    {
        return $this->where('method', '=', $value);
    }
}
