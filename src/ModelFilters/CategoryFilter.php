<?php

namespace Rockapps\RkLaravel\ModelFilters;



class CategoryFilter extends BaseModelFilter
{
    public $relations = [];
    public $blacklist = [];

    public function setup()
    {
    }

    public function name($value)
    {
        return $this->where(function ($q) use ($value) {
            return $q->where('name', 'LIKE', "%$value%")
                ->orWhere('description', 'LIKE', "%$value%")
                ->orWhere('headline', 'LIKE', "%$value%")
                ->orWhere('slug', 'LIKE', "%$value%");
        });
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function description($value)
    {
        return $this->where('description', 'LIKE', "%$value%");
    }

    public function headline($value)
    {
        return $this->where('headline', 'LIKE', "%$value%");
    }

    public function slug($value)
    {
        return $this->where('slug', 'LIKE', "%$value%");
    }

    public function active($value)
    {
        return $this->where('active', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }

    public function modelType($value)
    {
        return $this->where('model_type', '=', $value);
    }


}
