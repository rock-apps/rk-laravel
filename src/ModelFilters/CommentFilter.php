<?php

namespace Rockapps\RkLaravel\ModelFilters;



class CommentFilter extends BaseModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    protected $blacklist = ['includeTrashed'];

    public function setup()
    {
        $user = \Auth::getUser();
        if($user && $user->hasRole('admin')) {
            $this->whitelistMethod('includeTrashed');
        }
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function includeTrashed($value)
    {
        return $this->withTrashed();
    }

    public function is_approved($value)
    {
        return $this->whereIn('is_approved', $value);
    }

    public function startAt($value)
    {
        return $this->whereDate('created_at', '>=', $value);
    }

    public function endAt($value)
    {
        return $this->whereDate('created_at', '<=', $value);
    }

    public function commenter($value)
    {
        return $this->where('commenter_id', $value);
    }
    public function commenter_type($value)
    {
        return $this->where('commenter_type',$value);
    }

    public function commentable($value)
    {
        return $this->where('commentable_id', $value);
    }

    public function commentable_type($value)
    {
        return $this->where('commentable_type', $value);
    }

}
