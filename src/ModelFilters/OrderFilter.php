<?php

namespace Rockapps\RkLaravel\ModelFilters;

use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;
use Rockapps\RkLaravel\Constants\Constants;

class OrderFilter extends BaseModelFilter
{
    public $relations = [];

    public function setup()
    {
        $user = \Auth::getUser();

        if (!$user) return null;

        if ($user->hasRole(Constants::ROLE_ADMIN)) {
            $this->whitelistMethod('user');
        } else if ($user->hasRole(Constants::ROLE_COMPANY)) {
            $this->where('company_id', $user->company->id);
        } else if ($user->hasRole(Constants::ROLE_USER)) {
            $this->where('user_id', $user->id);
        }
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function company($value)
    {
        return $this->where('company_id', $value);
    }

    public function status($value)
    {
        return $this->whereIn('status', explode(',', $value));
    }

    public function active($value)
    {
        return $this->where('active', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }

    public function dateStart($value)
    {
        return $this->where('created_at', '>=', $value);
    }

    public function dateEnd($value)
    {
        return $this->where('created_at', '<=', $value);
    }
}
