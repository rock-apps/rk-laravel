<?php

namespace Rockapps\RkLaravel\ModelFilters;

use Illuminate\Database\Eloquent\Builder;

class CustomAttributesFilter extends BaseModelFilter
{
    public $relations = [];

    public function setup()
    {
    }

    public function field($value)
    {
        return $this->where('field', '=', $value);
    }

    public function object($relate_id)
    {
        return $this->where('object_id', $relate_id);
    }

    public function objectType($relate_type)
    {
        return $this->where('object_type', $relate_type);
    }
}
