<?php

namespace Rockapps\RkLaravel\ModelFilters;


class VariantFilter extends \Rockapps\RkLaravel\ModelFilters\BaseModelFilter
{
    public $relations = [];

    public function setup()
    {
        $user = \Auth::getUser();

        if(!$user) return null;

        return $this;
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function name($value)
    {
        return $this->where('name', 'like', "%{$value}%");
    }

    public function type($value)
    {
        return $this->where('type', 'like', "%{$value}%");
    }

    public function value($value)
    {
        return $this->where('value', 'like', "%{$value}%");

    }
}
