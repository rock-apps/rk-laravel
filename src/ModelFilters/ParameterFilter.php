<?php

namespace Rockapps\RkLaravel\ModelFilters;


class ParameterFilter extends BaseModelFilter
{
    public function description($value)
    {
        return $this->where('key', 'LIKE', "%$value%")
            ->orWhere('name', 'LIKE', "%$value%")
            ->orWhere('description', 'LIKE', "%$value%");
    }

    public function name($value)
    {
        return $this->where('name', 'LIKE', "%$value%");
    }

    public function group($value)
    {
        return $this->where('group', $value);
    }

    public function key($value)
    {
        return $this->where('key', 'LIKE', "%$value%");
    }

    public function type($value)
    {
        return $this->where('type', $value);
    }

}
