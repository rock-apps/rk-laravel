<?php

namespace Rockapps\RkLaravel\ModelFilters;


class PostBlockFilter extends BaseModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];


    public function setup()
    {

    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function isActive($value)
    {
        return $this->where('active', '=', filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }

    public function apple($value)
    {
        return $this->whereNotNull('apple_product_id');
    }

    public function google($value)
    {
        return $this->whereNotNull('google_product_id');
    }

}
