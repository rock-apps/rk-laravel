<?php

namespace Rockapps\RkLaravel\ModelFilters;


use Rockapps\RkLaravel\Models\User;

class PaymentFilter extends BaseModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    protected $blacklist = ['payer','payerType'];
    public function setup()
    {
        /** @var User $user */
        $user = \Auth::getUser();
        if ($user->hasRole('admin')) {
            $this->whitelistMethod('payer');
            $this->whitelistMethod('payerType');
        }else{
            $this->where('payer_id', $user->id);
            $this->where('payer_type',  config('rk-laravel.user.model',User::class));
        }
    }

    public function id($value)
    {
        return $this->where('id', $value);
    }

    public function status($value)
    {
        return $this->whereIn('status', explode(',', $value));
    }

    public function startAt($value)
    {
        return $this->whereDate('created_at', '>=', $value);
    }

    public function endAt($value)
    {
        return $this->whereDate('created_at', '<=', $value);
    }

    public function payer($value)
    {
        return $this->where('payer_id', $value);
    }

    public function payerType($value)
    {
        return $this->where('payer_type', $value);
    }

    public function purchaseable($value)
    {
        return $this->where('purchaseable_id', $value);
    }

    public function purchaseableType($value)
    {
        return $this->where('purchaseable_type', $value);
    }

    public function company($value)
    {
        return $this->where('company_id', $value);
    }
}
