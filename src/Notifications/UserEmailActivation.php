<?php

namespace Rockapps\RkLaravel\Notifications;

use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;

class UserEmailActivation extends ResetPasswordNotification
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $code = mt_rand(10, 999) . '-' . ($notifiable->id + 100) . '-' . mt_rand(1, 9999) . '-' . mt_rand(10, 99999);
        return (new MailMessage)
            ->greeting('Olá ' . $notifiable->getFirstName() . ',')
            ->subject('Ativação de E-mail')
            ->line('Você está recebendo este e-mail pois se cadastrou! Clique no botão abaixo para confirmar seu e-mail')
            ->action('Confirmar E-mail', url('activation/email', $code))
            ->line('Caso você não tenha solicitado uma nova senha, entre o contato com o suporte.')
            ->salutation(config('rk-laravel.email.salutation'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
