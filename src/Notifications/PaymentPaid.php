<?php

namespace Rockapps\RkLaravel\Notifications;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Rockapps\RkLaravel\Channels\ExpoChannel;
use Rockapps\RkLaravel\Helpers\ExpoPushMessage;
use Rockapps\RkLaravel\Models\Payment;

class PaymentPaid extends Notification
{

    /**
     * @var Payment $payment
     */
    protected $payment;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    public function via()
    {
        return ['database', 'broadcast', ExpoChannel::class];
    }

    public function toBroadcast()
    {
        return new BroadcastMessage($this->toArray());
    }

    public function broadcastOn()
    {
        return new PrivateChannel('App.User.' . $this->payable->id);
    }

    public function toExpo()
    {
        $value = number_format($this->payment->value, 2);
        $payment = $this->payment;

        (new ExpoPushMessage())
            ->setTitle("Seu pagamento foi confirmado!")
            ->setBody("O valor de {$value} foi tarifado do seu meio de pagamento.")
            ->setData(
                ['purchaseable' => ['id' => $payment->purchaseable_id, 'type' => $payment->purchaseable_type]],
                "Seu pagamento foi confirmado!")
            ->sendToUser($payment->payer);
    }

    public function toArray()
    {
        $payment = $this->payment;
        $value = number_format($this->payment->value, 2);

        return [
            'message' => "O pagamento #{$payment->id} de {$value} foi confirmado com sucesso",
            'purchaseable' => ['id' => $payment->purchaseable_id, 'type' => $payment->purchaseable_type],
            'payer' => ['id' => $payment->payer_id, 'type' => $payment->payer_type],
            'payment' => [
                'id' => $payment->id,
                'gateway' => $payment->gateway,
                'gateway_token' => $payment->pgm_transaction_id,
                'paid_at' => (string)$payment->paid_at,
                'value' => $payment->value,
            ],
            'created_at' => (string)\Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ];
    }
}
