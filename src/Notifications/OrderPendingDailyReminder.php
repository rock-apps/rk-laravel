<?php

namespace Rockapps\RkLaravel\Notifications;


use Rockapps\RkLaravel\Models\Order;
use Rockapps\RkLaravel\Models\User;
use Exception;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Rockapps\RkLaravel\Channels\ExpoChannel;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderPendingDailyReminder extends Notification
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /** @var User $notifiable */
        return (new MailMessage)
            ->subject('Lembrete de Pedido Pendente')
            ->greeting('Olá ' . $notifiable->getFirstName() . ',')
            ->line('Identificamos que você recebeu um pedido e ainda não aceitou.')
//            ->line('Para fechar a venda é necessário abrir o aplicativo Vizinhança, em "Pedidos" click na aba "Pendente", click no pedido e aceite.')
            ->line('Não perca venda.')
            ->salutation(config('rk-laravel.email.salutation'));
    }
}
