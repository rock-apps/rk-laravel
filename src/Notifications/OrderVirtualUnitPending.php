<?php

namespace Rockapps\RkLaravel\Notifications;

use Rockapps\RkLaravel\Channels\ExpoChannel;
use Rockapps\RkLaravel\Models\Order;
use Rockapps\RkLaravel\Models\OrderVirtualUnit;
use Rockapps\RkLaravel\Models\User;
use Exception;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Rockapps\RkLaravel\Helpers\ExpoPushMessage;

class OrderVirtualUnitPending extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var OrderVirtualUnit
     */
    private $order;

    public function __construct(OrderVirtualUnit $order)
    {
        $this->order = $order;
    }

    public function via($notifiable)
    {
        if ($notifiable instanceof User) {
            return ['database', 'broadcast', 'mail', ExpoChannel::class];
        } else if ($notifiable instanceof Order) {
            return ['database'];
        } else {
            return ['database'];
        }
    }

    public function toBroadcast()
    {
        return new BroadcastMessage($this->toArray());
    }

    public function toArray()
    {
        return [
            'order' => [
                'id' => $this->order->id,
                'total_value' => $this->order->total_value,
            ],
            'created_at' => (string)\Carbon\Carbon::now()
        ];
    }

    public function broadcastOn()
    {
        return new PrivateChannel('App.OrderVirtualUnit.' . $this->order->id);
    }

    /**
     * @param $notifiable
     * @return array|bool
     * @throws Exception
     */
    public function toExpo($notifiable)
    {
        $title = 'Chegou um pedido para aprovação';
        $body = "Verifique o pedido #{$this->order->id}";
        return (new ExpoPushMessage())
            ->setTitle($title)
            ->setBody($body)
            ->setData(['order' => ['id' => $this->order->id]], $title, 'OrderPending')
            ->sendToUser($notifiable);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /** @var User $notifiable */
        return (new MailMessage)
            ->subject('Novo Pedido #'.$this->order->id)
            ->greeting('Olá ' . $notifiable->getFirstName() . ',')
            ->line('Olá! Você recebeu um novo pedido no Aplicativo!')
            ->salutation(config('rk-laravel.email.salutation'));
    }
}
