<?php

namespace Rockapps\RkLaravel\Notifications;

use Exception;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Rockapps\RkLaravel\Channels\ExpoChannel;
use Rockapps\RkLaravel\Helpers\ExpoPushMessage;
use Rockapps\RkLaravel\Models\Order;
use Rockapps\RkLaravel\Models\OrderVirtualUnit;
use Rockapps\RkLaravel\Models\User;

class OrderVirtualUnitApproved extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var OrderVirtualUnit
     */
    private $order;

    public function __construct(OrderVirtualUnit $order)
    {
        $this->order = $order;
    }

    public function via($notifiable)
    {
        if ($notifiable instanceof User) {
            return ['database', 'broadcast', ExpoChannel::class];
        } else if ($notifiable instanceof Order) {
            return ['database'];
        } else {
            return ['database'];
        }
    }

    public function toBroadcast()
    {
        return new BroadcastMessage($this->toArray());
    }

    public function toArray()
    {
        return [
            'order' => [
                'id' => $this->order->id,
                'total_value' => $this->order->total_value,
            ],
            'created_at' => (string)\Carbon\Carbon::now()
        ];
    }

    public function broadcastOn()
    {
        return new PrivateChannel('App.OrderVirtualUnit.' . $this->order->id);
    }

    /**
     * @param $notifiable
     * @return array|bool
     * @throws Exception
     */
    public function toExpo($notifiable)
    {
        $title = "O pedido {$this->order->id} foi confirmado!";
        return (new ExpoPushMessage())
            ->setTitle($title)
            ->setBody("O valor será tarifado em breve.")
            ->setData(['order' => ['id' => $this->order->id]], $title, 'OrderApproved')
            ->sendToUser($notifiable);
    }
}
