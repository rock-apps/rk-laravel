<?php

namespace Rockapps\RkLaravel\Notifications;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Rockapps\RkLaravel\Channels\ExpoChannel;
use Rockapps\RkLaravel\Helpers\ExpoPushMessage;
use Rockapps\RkLaravel\Models\PushMessage;
use Rockapps\RkLaravel\Models\PushNotification;

class PushMessageSent extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var PushMessage
     */
    protected $pushMessage;
    /**
     * @var PushNotification
     */
    protected $pushNotification;

    public function __construct(PushMessage $pushMessage, PushNotification $pushNotification = null)
    {
        $this->queue = config('rk-laravel.push_notification.queue', 'default');
        $this->pushMessage = $pushMessage;
        if (!$pushNotification) $pushNotification = $pushMessage->pushNotification;
        $this->pushNotification = $pushNotification;
    }

    public function via()
    {
        return ['database', 'broadcast', ExpoChannel::class];
    }

    public function toBroadcast()
    {
        return new BroadcastMessage($this->toArray());
    }

    public function toArray()
    {
        $pushNotification = $this->pushNotification;
        $pushMessage = $this->pushMessage;

        return
            [
                'id' => $pushMessage->id,
                'name' => (string)$pushNotification->name,
                'type' => (string)$pushNotification->type,
                'destination' => (string)$pushNotification->destination,
                'scheduled_at' => (string)$pushNotification->scheduled_at,
                'title' => (string)$pushNotification->title,
                'message' => (string)$pushNotification->title,
                'body' => (string)$pushNotification->body,
                'object_id' => (int)$pushNotification->object_id,
                'object_type' => (string)$pushNotification->object_type,
                'created_at' => (string)\Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ];
    }

    public function broadcastOn()
    {
        return new PrivateChannel('App.User.' . $this->pushMessage->user_id);
    }

    public function toExpo()
    {
        $pushNotification = $this->pushNotification;
        $pushMessage = $this->pushMessage;

        $expo = new ExpoPushMessage();
        $return = $expo->setTitle($this->pushNotification->title)
            ->setBody($this->pushNotification->body)
            ->setData(
                [
                    'id' => $pushMessage->id,
                    'name' => (string)$pushNotification->name,
                    'type' => (string)$pushNotification->type,
                    'destination' => (string)$pushNotification->destination,
                    'scheduled_at' => (string)$pushNotification->scheduled_at,
                    'title' => (string)$pushNotification->title,
                    'body' => (string)$pushNotification->body,
                    'object_id' => (int)$pushNotification->object_id,
                    'object_type' => (string)$pushNotification->object_type,
                ],
                $pushNotification->title,
                $pushNotification->type
            )
            ->sendToDevice($this->pushMessage->device->registration_id);

        if (count($return) === 1 && array_key_exists('status', $return[0]) && $return[0]['status'] === 'ok') {
            $pushMessage->status = PushMessage::STATUS_SUCCESS;
            $pushMessage->sent_at = now();
            $pushMessage->response = json_encode($return[0]);
        } else {
            $pushMessage->status = PushMessage::STATUS_ERROR;
            $pushMessage->response = json_encode($return);
            if (config('rk-laravel.push_notification.remove_device_on_error')) {
                $pushMessage->device->delete();
            }
        }
        $pushMessage->save();

    }
}
