<?php

namespace Rockapps\RkLaravel\Notifications;


use Rockapps\RkLaravel\Channels\ExpoChannel;
use Rockapps\RkLaravel\Models\Order;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Rockapps\RkLaravel\Helpers\ExpoPushMessage;

class OrderApprovedDeliverToday extends Notification
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', ExpoChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        /** @var Order $notifiable */
        return [
//            'order' => [
//                'id' => $notifiable->id,
//                'total_value' => $notifiable->total_value,
//            ],
            'created_at' => (string)\Carbon\Carbon::now()
        ];
    }

    /**
     * @param $notifiable
     * @return array|bool
     * @throws Exception
     */
    public function toExpo($notifiable)
    {
        $title = "Olá! Você tem um pedido no Aplicativo Vizinhança para entrega HOJE!";
        $body = "Para ver, acesse o Aplicativo, Pedidos, e na aba de Aprovados.";
        return (new ExpoPushMessage())
            ->setTitle($title)
            ->setBody($body)
            ->setData([], $title, 'OrderApprovedDeliverToday')
            ->sendToUser($notifiable);
    }
}
