<?php

namespace Rockapps\RkLaravel\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Rockapps\RkLaravel\Exceptions\IntegrationException;
use Rockapps\RkLaravel\Helpers\FacilitaMovel\FacilitaMovel;
use Rockapps\RkLaravel\Models\User;

class TwoFactorToken extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['sms', 'database'];
    }

    /**
     * @param mixed $notifiable
     */
    public function toSms($notifiable)
    {
//        /** @var User $notifiable */
//        throw new IntegrationException('Implementar o SMS Token');
//        return FacilitaMovel::sendSms($notifiable->mobile, 'Utilize o código ' . $notifiable->two_factor_token . ' para acessar.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'two_factor_token' => $notifiable->two_factor_token,
            'user_id' => $notifiable->id,
        ];
    }
}
