<?php

namespace Rockapps\RkLaravel\Notifications;


use Rockapps\RkLaravel\Models\Order;
use Exception;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Rockapps\RkLaravel\Channels\ExpoChannel;
use Illuminate\Notifications\Notification;
use Rockapps\RkLaravel\Helpers\ExpoPushMessage;

class OrderPendingReminder extends Notification
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', ExpoChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        /** @var Order $notifiable */
        return [
//            'order' => [
//                'id' => $notifiable->id,
//                'total_value' => $notifiable->total_value,
//            ],
            'created_at' => (string)\Carbon\Carbon::now()
        ];
    }

    /**
     * @param $notifiable
     * @return array|bool
     * @throws Exception
     */
    public function toExpo($notifiable)
    {
        $title = "Olá! Você tem um pedido no Aplicativo Vizinhança aguardando para ser aprovado.";
        $body = "Para ver, acesse o Aplicativo, Pedidos, e na aba de Pendente, abrir o pedido e aprovar ou recusar.";
        return (new ExpoPushMessage())
            ->setTitle($title)
            ->setBody($body)
            ->setData([], $title, 'OrderPendingReminder')
            ->sendToUser($notifiable);
    }
}
