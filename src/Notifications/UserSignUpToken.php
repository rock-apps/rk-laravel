<?php

namespace Rockapps\RkLaravel\Notifications;

use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class UserSignUpToken extends ResetPasswordNotification
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Olá ' . $notifiable->getFirstName() . ',')
            ->subject('Ativação Cadastro')
            ->line('Utilize o código abaixo para validar o cadastro.')
            ->line(new HtmlString("<strong>$notifiable->signup_token</strong>"))
//            ->line('Caso você não tenha solicitado uma nova senha, entre o contato com o suporte.')
            ->line('Obrigado!')
            ->salutation(config('rk-laravel.email.salutation'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
