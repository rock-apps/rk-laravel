<?php

namespace Rockapps\RkLaravel\Notifications;

use Rockapps\RkLaravel\Channels\ExpoChannel;
use Rockapps\RkLaravel\Models\Subscription;
use Rockapps\RkLaravel\Models\User;
use Exception;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Rockapps\RkLaravel\Helpers\ExpoPushMessage;

class SubscriptionPaid extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Subscription
     */
    private $subscription;

    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toBroadcast()
    {
        return new BroadcastMessage($this->toArray());
    }

    public function toArray()
    {
        return [
            'subscription' => $this->subscription->transform(),
            'created_at' => (string)\Carbon\Carbon::now()
        ];
    }

    public function broadcastOn()
    {
        return new PrivateChannel('App.Subscription.' . $this->subscription->id);
    }

}
