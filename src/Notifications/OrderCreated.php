<?php

namespace Rockapps\RkLaravel\Notifications;

use Rockapps\RkLaravel\Channels\ExpoChannel;
use Rockapps\RkLaravel\Models\Order;
use Rockapps\RkLaravel\Models\User;
use Exception;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Rockapps\RkLaravel\Helpers\ExpoPushMessage;

class OrderCreated extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Order
     */
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toBroadcast()
    {
        return new BroadcastMessage($this->toArray());
    }

    public function toArray()
    {
        return [
            'order' => [
                'id' => $this->order->id,
                'total_value' => $this->order->total_value,
            ],
            'created_at' => (string)\Carbon\Carbon::now()
        ];
    }

    public function broadcastOn()
    {
        return new PrivateChannel('App.Order.' . $this->order->id);
    }

}
