<?php

namespace Rockapps\RkLaravel\Notifications;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Rockapps\RkLaravel\Channels\ExpoChannel;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Helpers\ExpoPushMessage;
use Rockapps\RkLaravel\Models\Comment;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Transformers\CommentTransformer;

class CommentDeleted extends Notification
//    implements ShouldQueue
{
//    use Queueable;
#TODO: Não funcionou utilizar o queued. A notification travou na fila

    /**
     * @var Comment $comment
     */
    protected $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    public function via()
    {
        return ['database', 'broadcast', ExpoChannel::class];
    }

    public function toBroadcast()
    {
        return new BroadcastMessage($this->toArray());
    }

    public function toArray()
    {
        return [
            'message' => "Seu comentário foi excluído!",
            'comment' => $this->comment->transform(CommentTransformer::class),
            'created_at' => (string)\Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ];
    }

    public function broadcastOn()
    {
        return new PrivateChannel('App.User.' . $this->comment->commenter_id);
    }

    public function toExpo()
    {
        $title = "Seu comentário foi excluído!";
        $body = "Seu comentário foi analisado e não está de acordo com as políticas de uso. Evite a suspensão de sua conta.";
        $param_title = Parameter::where('key',Constants::COMMENTS_DELETE_PUSH_TITLE)->get()->first();
        if ($param_title) $title = $param_title->value;
        $param_body = Parameter::where('key',Constants::COMMENTS_DELETE_PUSH_BODY)->get()->first();
        if ($param_body) $body = $param_body->value;

        (new ExpoPushMessage())
            ->setTitle($title)
            ->setBody($body)
            ->setData(['comment' => $this->comment->transform(CommentTransformer::class)],
                $title)
            ->sendToUser($this->comment->commenter);
    }
}
