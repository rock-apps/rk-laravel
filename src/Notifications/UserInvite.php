<?php

namespace Rockapps\RkLaravel\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserInvite extends Notification
{
    use Queueable;

    private $name;
    private $email;

    public function __construct($email, $name)
    {
        $this->email = $email;
        $this->name = $name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->cc($this->email, $this->name)
            ->subject('Convite')
            ->greeting('Olá ' . $this->name . ',')
            ->line("Você está recebendo este e-mail pois foi convidado pelo {$notifiable->name}. Utilize o código abaixo ao se cadastrar.")
            ->line("## $notifiable->invitation_code")
            ->salutation(config('rk-laravel.email.salutation'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
