<?php

namespace Rockapps\RkLaravel\Notifications;

use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;

class UserResetPassword extends ResetPasswordNotification
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $url ='password/reset/'.$this->token.'?email='.$notifiable->email;
        return (new MailMessage)
            ->greeting('Olá ' . $notifiable->getFirstName() . ',')
            ->subject('Solicitação de Reset de Senha')
            ->line('Você está recebendo este e-mail pois solicitou um reset de senha na sua conta. Clique no botão abaixo para criar uma nova senha')
            ->action('Reiniciar Senha',  url($url))
            ->line('Caso você não tenha solicitado uma nova senha, entre o contato com o suporte.')
            ->salutation(config('rk-laravel.email.salutation'));
    }
}
