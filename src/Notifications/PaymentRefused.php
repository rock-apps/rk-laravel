<?php

namespace Rockapps\RkLaravel\Notifications;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Rockapps\RkLaravel\Channels\ExpoChannel;
use Rockapps\RkLaravel\Helpers\ExpoPushMessage;
use Rockapps\RkLaravel\Interfaces\PayerInterface;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\User;

class PaymentRefused extends Notification
{

    /**
     * @var Payment $payment
     */
    protected $payment;
    /**
     * @var User|PayerInterface
     */
    private $payable;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    public function via()
    {
        return ['database', 'broadcast', ExpoChannel::class];
    }

    public function toBroadcast()
    {
        return new BroadcastMessage($this->toArray());
    }

    public function broadcastOn()
    {
        return new PrivateChannel('App.User.' . $this->payable->id);
    }

    public function toExpo()
    {
        $payment = $this->payment;

        (new ExpoPushMessage())
            ->setTitle("Seu pagamento foi rejeitado!")
            ->setBody("Selecione um nova forma de pagamento.")
            ->setData(
                ['purchaseable' => ['id' => $payment->purchaseable_id, 'type' => $payment->purchaseable_type]],
                "Seu pagamento foi rejeitado!")
            ->sendToUser($payment->payer);
    }

    public function toArray()
    {
        $payment = $this->payment;
        $value = number_format($this->payment->value, 2);

        return [
            'message' => "O pagamento de {$value} foi rejeitado",
            'purchaseable' => ['id' => $payment->purchaseable_id, 'type' => $payment->purchaseable_type],
            'payer' => ['id' => $payment->payer_id, 'type' => $payment->payer_type],
            'created_at' => (string)\Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ];
    }
}
