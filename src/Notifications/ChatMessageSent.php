<?php

namespace Rockapps\RkLaravel\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Musonza\Chat\Models\Message;
use Rockapps\RkLaravel\Channels\ExpoChannel;
use Rockapps\RkLaravel\Helpers\ExpoPushMessage;
use Rockapps\RkLaravel\Models\User;

class ChatMessageSent extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Message
     */
    private $message;

    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    public function via($notifiable)
    {
        return [
            'database',
            'broadcast',
            ExpoChannel::class
        ];
    }

    public function toExpo(User $notifiable)
    {
        return (new ExpoPushMessage())
            ->setTitle($this->message->body)
//            ->setBody($body)
            ->setData([
                'order' => ['id' => $this->order->id],
                'chat' => ['id' => $this->message->conversation_id]
            ],
                $this->message->body)
            ->sendToUser($notifiable);
    }

    public function toArray($notifiable)
    {
        return [
            'message' => $this->message->body,
            'chat' => $this->message->conversation_id,
            'created_by' => \Auth::getUser() ? \Auth::getUser()->id : null,
            'created_at' => (string)\Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ];
    }
}
