<?php

namespace Rockapps\RkLaravel\Exceptions;

use Exception;
use Illuminate\Support\MessageBag;
use Rockapps\RkLaravel\Helpers\Sentry;

class RequestValidationException extends \Dingo\Api\Exception\ResourceException
{
    /**
     * MessageBag errors.
     *
     * @var MessageBag
     */
    protected $errors;

    /**
     * Create a new resource exception instance.
     *
     * @param string $message
     * @param MessageBag|array $errors
     * @param Exception $previous
     * @param array $headers
     * @param int $code
     *
     */
    public function __construct($message = null, $errors = null, Exception $previous = null, $headers = [], $code = 0)
    {
        Sentry::addContent($errors);

        parent::__construct($message, $errors, $previous, $headers, $code);
    }
}
