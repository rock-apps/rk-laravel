<?php

namespace Rockapps\RkLaravel\Exceptions;

use Exception;
use Illuminate\Support\MessageBag;
use Rockapps\RkLaravel\Helpers\Sentry;

class AccessDeniedException extends \Dingo\Api\Exception\ResourceException
{
    /**
     * MessageBag errors.
     *
     * @var MessageBag
     */
    protected $errors;

    /**
     * Create a new resource exception instance.
     *
     * @param string $message
     * @param $allowed_permission
     * @param $user_permission
     * @param Exception $previous
     * @param array $headers
     * @param int $code
     */
    public function __construct($message, $allowed_permission, $user_permission = null, Exception $previous = null, $headers = [], $code = 0)
    {
        Sentry::addContent([
            'allowed' => $allowed_permission,
            'users' => $user_permission,
        ]);

        parent::__construct($message, null, $previous, $headers, $code);
    }
}
