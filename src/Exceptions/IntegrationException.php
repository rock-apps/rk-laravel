<?php

namespace Rockapps\RkLaravel\Exceptions;

use Rockapps\RkLaravel\Helpers\Sentry;
use Exception;
use Illuminate\Support\MessageBag;
use Dingo\Api\Contract\Debug\MessageBagErrors;
use Symfony\Component\HttpKernel\Exception\HttpException;

class IntegrationException extends HttpException implements MessageBagErrors
{

    /**
     * MessageBag errors.
     *
     * @var \Illuminate\Support\MessageBag
     */
    protected $errors;

    /**
     * Create a new resource exception instance.
     *
     * @param string $message
     * @param \Illuminate\Support\MessageBag|array $errors
     * @param null $exception_id
     * @param \Exception $previous
     * @param array $headers
     * @param int $code
     *
     */
    public function __construct($message = null, $errors = null, $exception_id = null, Exception $previous = null, $headers = [], $code = 0)
    {
        if ($exception_id) {
            if (is_array($errors)) {
                $errors['exception_id'] = $exception_id;
            } else {
                $errors = [];
                $errors['exception_id'] = $exception_id;
            }

            $message .= ' [' . $exception_id . ']';
        }
        Sentry::addContent($errors);

        $this->errors = new MessageBag;

        parent::__construct(500, $message, $previous, $headers, $code);
    }

    /**
     * Get the errors message bag.
     *
     * @return \Illuminate\Support\MessageBag
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Determine if message bag has any errors.
     *
     * @return bool
     */
    public function hasErrors()
    {
        return !$this->errors->isEmpty();
    }
}
