<?php

namespace Rockapps\RkLaravel\Exceptions;

use Dingo\Api\Exception\Handler as DingoHandler;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Rockapps\RkLaravel\Models\ModelBase;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use function Sentry\captureException;

class ApiHandler extends DingoHandler
{
    public function handle(Exception $exception)
    {
        if ($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
//dd(1);
            /** @var ModelBase $model */
            $model = ($exception->getModel());
            $model = new $model;
            $validationMessages = $model->getValidationMessages();

            $message = "Registro não encontrado";
            if (key_exists('error.not_found', $validationMessages)) {
                $message = $validationMessages['error.not_found'];
            }

            return $this->renderError($message, 404);

        } else if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            $this->processException($exception);
            return $this->renderError('Rota não encontrada.', 404);

        } else if ($exception instanceof DingoAccess) {
            $this->processException($exception);
            return $this->renderError('Rota não encontrada.', 404);

        } else if ($exception instanceof \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
            || $exception instanceof \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException
            || $exception instanceof \Rockapps\RkLaravel\Exceptions\AccessDeniedException
            || $exception instanceof \Rockapps\RkLaravel\Exceptions\AccessModelDeniedException
        ) {
            return $this->renderError($exception->getMessage(), 403);

        } else if ($exception instanceof AuthenticationException) {
            return $this->renderError('Não Autenticado.', 401);

        } else if ($exception instanceof TokenExpiredException) {
            return $this->renderError('Token Expirado. Realize o login novamente.', 401);

        } else if ($exception instanceof TokenInvalidException) {
            return $this->renderError('Token Inválido. Realize o login novamente.', 401);

        } else if ($exception instanceof JWTException) {
            return $this->renderError('Token Ausente. Realize o login novamente.', 401);

        } else if ($exception instanceof \Carbon\Exceptions\InvalidFormatException) {
            $this->processException($exception);
            return $this->renderError('Formato de data inválido.', 422);

        } else if ($exception instanceof \Rockapps\RkLaravel\Exceptions\RequestValidationException
            || $exception instanceof \Symfony\Component\HttpKernel\Exception\HttpException
            || $exception instanceof \Rockapps\RkLaravel\Exceptions\ResourceException
            || $exception instanceof \Rockapps\RkLaravel\Exceptions\IntegrationException
            || $exception instanceof \Rockapps\RkLaravel\Exceptions\AccessDeniedException) {
            return parent::handle($exception);

        } else if ($exception instanceof \Dingo\Api\Exception\RateLimitExceededException) {
            $this->processException($exception);
            return $this->renderError('Você ultrapassou o limite de requisições. Aguarde alguns instantes e tente novamente.', 422);
        }

        if (config('rk-laravel.configuration.errors.hide_dev_errors', true)) {
            $this->processException($exception);
            $msg_500 = config('rk-laravel.configuration.errors.500', 'Ocorreu um erro no servidor');
            return $this->renderError($msg_500, 500);
        }

        return parent::handle($exception);
    }

    public function renderError($message = 'Error', $status_code = 400)
    {
        return response()->json([
            'error' => [
                'message' => $message
            ]
        ], $status_code);
    }

    private function processException($exception)
    {
        captureException($exception);
        parent::handle($exception);
    }

}
