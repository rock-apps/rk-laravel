<?php

namespace Rockapps\RkLaravel\Exceptions;

use Exception;
use Illuminate\Support\MessageBag;
use Rockapps\RkLaravel\Helpers\Sentry;

class AccessModelDeniedException extends \Dingo\Api\Exception\ResourceException
{
    /**
     * MessageBag errors.
     *
     * @var MessageBag
     */
    protected $errors;

    /**
     * Create a new resource exception instance.
     *
     * @param string $message
     * @param Exception|null $previous
     * @param array $headers
     * @param int $code
     */
    public function __construct($message, Exception $previous = null, $headers = [], $code = 0)
    {
//        Sentry::addContent([
//            'allowed' => $allowed_permission,
//            'users' => $user_permission,
//        ]);

        parent::__construct($message, null, $previous, $headers, $code);
    }
}
