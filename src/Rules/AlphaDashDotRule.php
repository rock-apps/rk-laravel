<?php

namespace Rockapps\RkLaravel\Rules;

use Illuminate\Contracts\Validation\Rule;

class AlphaDashDotRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!is_string($value) && !is_numeric($value)) {
            return false;
        }

        return preg_match('/^[\pL\pM\pN_-].+$/u', $value) > 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'O campo :attribute só pode conter letras, números, traços, pontos e underline.';
    }
}
