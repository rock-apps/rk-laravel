<?php


namespace Rockapps\RkLaravel\Routes;

use Dingo\Api\Routing\Router;
use Rockapps\RkLaravel\Api\Address\AddressController;
use Rockapps\RkLaravel\Api\Audit\AuditController;
use Rockapps\RkLaravel\Api\Auth\AuthController;
use Rockapps\RkLaravel\Api\BankAccount\BankAccountController;
use Rockapps\RkLaravel\Api\BankTransfer\BankTransferController;
use Rockapps\RkLaravel\Api\Banner\BannerController;
use Rockapps\RkLaravel\Api\Campaign\CampaignController;
use Rockapps\RkLaravel\Api\Campaign\CampaignElementController;
use Rockapps\RkLaravel\Api\Category\CategoryController;
use Rockapps\RkLaravel\Api\Chat\ChatController;
use Rockapps\RkLaravel\Api\Comment\CommentController;
use Rockapps\RkLaravel\Api\Company\CompanyController;
use Rockapps\RkLaravel\Api\Configuration\ConfigurationController;
use Rockapps\RkLaravel\Api\Contact\ContactController;
use Rockapps\RkLaravel\Api\CreditCard\CreditCardController;
use Rockapps\RkLaravel\Api\Media\MediaController;
use Rockapps\RkLaravel\Api\Notification\NotificationController;
use Rockapps\RkLaravel\Api\Order\OrderVirtualUnitController;
use Rockapps\RkLaravel\Api\Parameter\ParameterController;
use Rockapps\RkLaravel\Api\Payment\PaymentController;
use Rockapps\RkLaravel\Api\PaymentMethod\PaymentMethodController;
use Rockapps\RkLaravel\Api\Plan\PlanController;
use Rockapps\RkLaravel\Api\Post\PostBlockController;
use Rockapps\RkLaravel\Api\Post\PostController;
use Rockapps\RkLaravel\Api\Product\ProductController;
use Rockapps\RkLaravel\Api\PushNotification\PushNotificationController;
use Rockapps\RkLaravel\Api\Search\SearchController;
use Rockapps\RkLaravel\Api\ShippingMethod\ShippingMethodController;
use Rockapps\RkLaravel\Api\Subscription\SubscriptionController;
use Rockapps\RkLaravel\Api\System\PermissionController;
use Rockapps\RkLaravel\Api\System\RoleController;
use Rockapps\RkLaravel\Api\CustomAttributes\CustomAttributesController;
use Rockapps\RkLaravel\Api\System\TermsController;
use Rockapps\RkLaravel\Api\User\UserController;
use Rockapps\RkLaravel\Api\User\UserLoggedController;
use Rockapps\RkLaravel\Api\Variant\VariantController;
use Rockapps\RkLaravel\Api\Voucher\VoucherController;
use Rockapps\RkLaravel\Models\PaymentMethod;

class Api
{
    public static function auth(Router $api, $class = AuthController::class)
    {
        $api->group(['prefix' => 'auth'], function (Router $api) use ($class) {
            $api->post('login', $class . '@login');
            $api->post('login-two-factor', $class . '@loginTwoFactor');
            $api->post('login-email-token', $class . '@loginWithEmailToken');
            $api->post('send-two-factor-token', $class . '@sendTwoFactorToken');
            $api->post('send-email-token', $class . '@sendEmailToken');
            $api->post('oauth/{provider}', $class . '@oauth');
            $api->post('signup/{role}', $class . '@signUp');
            $api->post('recovery', $class . '@sendResetEmail');
            $api->post('reset', $class . '@resetPassword');
            $api->post('logout', $class . '@logout');
            $api->post('refresh', $class . '@refresh');
        });
    }

    public static function userLogged(Router $api, $class = UserLoggedController::class)
    {
        $api->get('user/me', $class . '@me');
        $api->put('user/me/destroy', $class . '@destroyMe');
        $api->put('user/me', $class . '@updateMe');
        $api->post('user/invite', $class . '@invite');
        $api->post('user/device/{registration_id}', $class . '@addDevice');
        $api->delete('user/device/{registration_id}', $class . '@removeDevice');
        $api->put('user/validate-signup-token', $class . '@validateSignUpToken');
        $api->post('user/send-signup-token-email', $class . '@sendSignUpTokenEmail');
        $api->post('user/send-activation-email', $class . '@sendActivationEmail');
    }

    public static function roles(Router $api, $class = RoleController::class)
    {
        $api->get('roles', $class . '@index');
        $api->get('roles/{id}', $class . '@show');
    }

    public static function search(Router $api, $class = SearchController::class)
    {
        $api->get('search', $class . '@search');
    }

    public static function rolesAdmin(Router $api, $class = RoleController::class)
    {
        $api->post('roles', $class . '@store');
        $api->post('roles/{role_id}/permission/{permission_id}', $class . '@attachPermission');
        $api->put('roles/{id}', $class . '@update');
        $api->delete('roles/{id}', $class . '@destroy');
        $api->delete('roles/{role_id}/permission/{permission_id}', $class . '@dettachPermission');
    }


    public static function permissions(Router $api, $class = PermissionController::class)
    {
        $api->get('permissions', $class . '@index');
        $api->get('permissions/{id}', $class . '@show');
    }

    public static function permissionsAdmin(Router $api, $class = PermissionController::class)
    {
        $api->post('permissions', $class . '@store');
        $api->put('permissions/{id}', $class . '@update');
        $api->delete('permissions/{id}', $class . '@destroy');
    }

    public static function auditsAdmin(Router $api, $class = AuditController::class)
    {
        $api->get('audits', $class . '@index');
        $api->get('audits/{id}', $class . '@show');
    }

    public static function notifications(Router $api, $class = NotificationController::class)
    {
        $api->get('notifications', $class . '@index');
        $api->get('notifications/unread', $class . '@indexUnread');
        $api->put('notifications/read/all', $class . '@markAllAsRead');
        $api->put('notifications/read/{id}', $class . '@markAsRead');
    }

    public static function parameters(Router $api, $class = ParameterController::class)
    {
        $api->get('parameters', $class . '@index');
    }

    public static function parametersAdmin(Router $api, $class = ParameterController::class)
    {
        $api->get('parameters/{id}', $class . '@show');
        $api->put('parameters/{id}', $class . '@update');
    }

    public static function bankAccounts(Router $api, $class = BankAccountController::class)
    {
        $api->get('bank-accounts', $class . '@index');
        $api->get('bank-accounts/list', $class . '@indexBankList');
//        $api->get('bank-accounts/{id}',$class. '@show');
        $api->post('bank-accounts', $class . '@store');
        $api->delete('bank-accounts/{id}', $class . '@destroy');
    }

    public static function posts(Router $api, $class = PostController::class)
    {
        $api->get('posts', $class . '@index');
        $api->get('posts/{id}', $class . '@show');
    }

    public static function postsAdmin(Router $api, $class = PostController::class)
    {
        $api->post('posts', $class . '@store');
        $api->put('posts/{id}', $class . '@update');
        $api->delete('posts/{id}', $class . '@destroy');
    }

    public static function products(Router $api, $class = ProductController::class)
    {
        $api->get('products', $class . '@index');
        $api->get('products/{id}', $class . '@show');
    }

    public static function productsAdmin(Router $api, $class = ProductController::class)
    {
        $api->post('products', $class . '@store');
        $api->put('products/{id}', $class . '@update');
        $api->delete('products/{id}', $class . '@destroy');
    }

    public static function banners(Router $api, $class = BannerController::class)
    {
        $api->get('banners', $class . '@index');
        $api->get('banners/{id}', $class . '@show');
    }

    public static function bannersAdmin(Router $api, $class = BannerController::class)
    {
        $api->post('banners', $class . '@store');
        $api->put('banners/{id}', $class . '@update');
        $api->delete('banners/{id}', $class . '@destroy');
    }

    public static function campaigns(Router $api, $class = CampaignController::class)
    {
        $api->get('campaigns/action/view', $class . '@actionView');
        $api->get('campaigns/action/click/{campaign_id}/{campaign_element_id}', $class . '@actionClick');
    }

    public static function campaignsAdmin(Router $api, $class = CampaignController::class)
    {
        $api->get('campaigns', $class . '@index');
        $api->get('campaigns/{id}', $class . '@show');
        $api->post('campaigns', $class . '@store');
        $api->put('campaigns/{id}', $class . '@update');
        $api->delete('campaigns/{id}', $class . '@destroy');
    }
    public static function campaignElementsAdmin(Router $api, $class = CampaignElementController::class)
    {
        $api->get('campaign-elements/{id}', $class . '@show');
        $api->post('campaign-elements', $class . '@store');
        $api->put('campaign-elements/{id}', $class . '@update');
        $api->delete('campaign-elements/{id}', $class . '@destroy');
    }

    public static function companies(Router $api, $class = CompanyController::class)
    {
        $api->get('companies', $class . '@index');
        $api->get('companies/{id}', $class . '@show');
    }

    public static function companiesAdmin(Router $api, $class = CompanyController::class)
    {
        $api->post('companies', $class . '@store');
        $api->put('companies/{id}', $class . '@update');
        $api->delete('companies/{id}', $class . '@destroy');
    }

    public static function postBlocksAdmin(Router $api, $class = PostBlockController::class)
    {
        $api->get('post-blocks', $class . '@index');
        $api->get('post-blocks/{id}', $class . '@show');
        $api->put('post-blocks/{id}', $class . '@update');
        $api->post('post-blocks', $class . '@store');
        $api->delete('post-blocks/{id}', $class . '@destroy');
    }

    public static function configuration(Router $api, $class = ConfigurationController::class)
    {
        $api->get('configurations', $class . '@index');
        $api->post('configurations', $class . '@store');
        $api->put('configurations', $class . '@update');
    }

    public static function bankTransfers(Router $api, $class = BankTransferController::class)
    {
        $api->get('bank-transfers', $class . '@index');
        $api->get('bank-transfers/{id}', $class . '@show');
        $api->post('bank-transfers', $class . '@store');
    }

    public static function variantsAdmin(Router $api, $class = VariantController::class)
    {
        $api->put('variants/{id}', $class . '@update');
        $api->delete('variants/{id}', $class . '@destroy');
        $api->get('variants', $class . '@index');
        $api->get('variants/{id}', $class . '@show');
        $api->post('variants', $class . '@store');
    }

    public static function bankTransfersAdmin(Router $api, $class = BankTransferController::class)
    {
        $api->put('bank-transfers/{id}', $class . '@update');
        $api->delete('bank-transfers/{id}', $class . '@destroy');
    }


    public static function categories(Router $api, $class = CategoryController::class)
    {
        $api->get('categories', $class . '@index');
        $api->get('categories/types', $class . '@indexTypes');
        $api->get('categories/{id}', $class . '@show');
    }

    public static function categoriesAdmin(Router $api, $class = CategoryController::class)
    {
        $api->post('categories', $class . '@store');
        $api->put('categories/{id}', $class . '@update');
        $api->delete('categories/{id}', $class . '@destroy');
    }

    public static function mediasAdmin(Router $api, $class = MediaController::class)
    {
        $api->put('medias/reorder', $class . '@reorder');
        $api->delete('medias/{id}', $class . '@destroy');
    }

    public static function creditCards(Router $api, $class = CreditCardController::class)
    {
        $api->get('credit-cards', $class . '@index');
//        $api->get('credit-cards/{id}',$class. '@show');
        $api->post('credit-cards', $class . '@store');
        $api->delete('credit-cards/{id}', $class . '@destroy');
        $api->put('credit-cards/{id}/default', $class . '@setDefault');
    }

    public static function comments(Router $api, $class = CommentController::class)
    {
        $api->post('comments', $class . '@store');
    }

    public static function commentsAdmin(Router $api, $class = CommentController::class)
    {
        $api->put('comments/{id}', $class . '@update');
        $api->get('comments', $class . '@index');
        $api->get('comments/{id}', $class . '@show');
        $api->delete('comments/{id}', $class . '@destroy');
    }

    public static function ordersVirtualUnits(Router $api, $class = OrderVirtualUnitController::class)
    {
        $api->post('orders-virtual-unit', $class . '@store');
        $api->get('orders-virtual-unit', $class . '@index');
        $api->get('orders-virtual-unit/{id}', $class . '@show');
    }

    public static function ordersVirtualUnitsAdmin(Router $api, $class = OrderVirtualUnitController::class)
    {
        $api->put('orders-virtual-unit/{id}', $class . '@update');
//        $api->put('orders-virtual/{id}', $class . '@update');
//        $api->get('orders-virtual', $class . '@index');
//        $api->get('orders-virtual/{id}', $class . '@show');
//        $api->delete('orders-virtual/{id}', $class . '@destroy');
    }

    public static function addresses(Router $api, $class = AddressController::class)
    {
        $api->get('addresses', $class . '@index');
        $api->get('addresses/{id}', $class . '@show');
        $api->post('addresses', $class . '@store');
        $api->delete('addresses/{id}', $class . '@destroy');
        $api->put('addresses/{id}', $class . '@update');
        $api->put('addresses/{id}/default', $class . '@setDefault');
    }

    public static function webhooks(Router $api, $class = PaymentController::class)
    {
        $api->get('webhooks-pagarme', $class . '@webhooksPagarMe');
        $api->post('webhooks-pagarme', $class . '@webhooksPagarMe');
    }

    public static function payments(Router $api, $class = PaymentController::class)
    {
        $api->get('payments', $class . '@index');
        $api->get('payments/{id}', $class . '@show');
        $api->post('payments/payment-iap-ios', $class . '@paymentIapIos');
        $api->get('payments-helper/{id}/simulate-pgm-status', $class . '@simulatePagarmeStatus');
    }

    public static function paymentsAdmin(Router $api, $class = PaymentController::class)
    {
        $api->post('payments', $class . '@store');
        $api->put('payments/{id}', $class . '@update');
        $api->post('payments/provision', $class . '@storeProvisionPayment');
        $api->put('payments/provision/{id}', $class . '@updateProvisionPayment');
        $api->put('payments/{id}/status/{status}', $class . '@changeStatus');
    }

    public static function paymentMethods(Router $api, $class = PaymentMethodController::class)
    {
        $api->get('payment-methods', $class . '@index');
        $api->get('payment-methods/{id}', $class . '@show');
        $api->post('payment-methods', $class . '@store');
        $api->put('payment-methods/{id}', $class . '@update');
        $api->delete('payment-methods/{id}', $class . '@destroy');
    }

    public static function shippingMethods(Router $api, $class = ShippingMethodController::class)
    {
        $api->get('shipping-methods', $class . '@index');
        $api->get('shipping-methods/{id}', $class . '@show');
        $api->post('shipping-methods', $class . '@store');
        $api->put('shipping-methods/{id}', $class . '@update');
        $api->delete('shipping-methods/{id}', $class . '@destroy');
    }

    public static function customAttributes(Router $api, $class = CustomAttributesController::class)
    {
        $api->get('custom-attributes', $class . '@index');
        $api->get('custom-attributes/{id}', $class . '@show');
        $api->post('custom-attributes', $class . '@store');
        $api->put('custom-attributes/{id}', $class . '@update');
        $api->delete('custom-attributes/{id}', $class . '@destroy');
    }

    public static function chats(Router $api, $class = ChatController::class)
    {
        $api->get('chats', $class . '@index');
        $api->get('chats/{chat_id}', $class . '@show');
        $api->post('chats/{chat_id}/messages', $class . '@storeMessage');
        $api->put('chats/{chat_id}/read-all', $class . '@markAsRead');
        $api->get('chats/{chat_id}/messages', $class . '@showMessages');
    }

    public static function vouchers(Router $api, $class = VoucherController::class)
    {
        $api->post('vouchers/redeemed/{code}', $class . '@redeemed');
    }

    public static function vouchersAdmin(Router $api, $class = VoucherController::class)
    {
        $api->get('vouchers', $class . '@index');
        $api->get('vouchers/{id}', $class . '@show');
        $api->post('vouchers', $class . '@store');
        $api->put('vouchers/{id}', $class . '@update');
    }

    public static function plans(Router $api, $class = PlanController::class)
    {
        $api->get('plans', $class . '@index');
        $api->get('plans/{id}', $class . '@show');
    }

    public static function plansAdmin(Router $api, $class = PlanController::class)
    {
        $api->put('plans/{id}', $class . '@update');
        $api->post('plans', $class . '@store');
    }

    public static function pushNotificationsAdmin(Router $api, $class = PushNotificationController::class)
    {
        $api->get('push-notifications/destinations', $class.'@destinations');
        $api->resource('push-notifications', $class);
        $api->put('push-notifications/{id}/status/{status}', $class . '@changeStatus');
    }

    public static function subscriptions(Router $api, $class = SubscriptionController::class)
    {
        $api->get('subscriptions', $class . '@index');
        $api->get('subscriptions/{id}', $class . '@show');
        $api->post('subscriptions/subscribe', $class . '@subscribe');
        $api->post('subscriptions/subscribe-balance', $class . '@subscribeBalance');
        $api->post('subscriptions/subscribe-iap-ios', $class . '@subscribeIapIos');
        $api->post('subscriptions/subscribe-iap-google', $class . '@subscribeIapGoogle');
        $api->put('subscriptions/{id}/cancel', $class . '@cancel');
    }

    public static function subscriptionsAdmin(Router $api, $class = SubscriptionController::class)
    {
        $api->post('subscriptions', $class . '@store');
        $api->put('subscriptions/{id}', $class . '@update');
    }

    public static function contact(Router $api, $class = ContactController::class)
    {
        $api->post('contact/send-mail', $class . '@sendMail');
    }

    public static function users(Router $api, $class = UserController::class)
    {
        $api->get('users', $class . '@index');
        $api->get('users/{id}', $class . '@show');
    }

    public static function terms(Router $api, $class = TermsController::class)
    {
        $api->get('terms/tos', $class . '@tos');
        $api->get('terms/privacy', $class . '@privacy');
    }

    public static function usersAdmin(Router $api, $class = UserController::class)
    {
        $api->put('users/{id}', $class . '@update');
        $api->post('users', $class . '@store');
        $api->post('users/{user_id}/role/{role_id}', $class . '@roleAdd');
        $api->delete('users/{user_id}/role/{role_id}', $class . '@roleDestroy');
    }
}


