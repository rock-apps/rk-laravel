<?php

namespace Rockapps\RkLaravel\Services;

use Rockapps\RkLaravel\Models\Payment;

class MarketplaceSplitService
{
    /**
     * @param Payment $payment
     * @param $data
     * @return array
     */
    public static function calculate($payment, $data)
    {
        if (in_array($payment->gateway, [
            Payment::GATEWAY_PAGARME_CC,
            Payment::GATEWAY_PAGARME_BOLETO,
            Payment::GATEWAY_PAGARME_PIX
        ])) {
            return self::pagarMe($data);
        }
        return null;

    }

    /**
     * @param $data
     * @return array
     */
    public static function pagarMe($data)
    {
        return [];
//        $fixed_tax = Parameter::getByKey(Constant::PGM_FIXED_TAX)->value_float; // ~R$1,20
//
//        $bankRecipient = $this->company->responsible->bankRecipient;
//
//        $value_mktplace = round($this->getTotalValue() - ($this->company->tax / 100) * $this->getTotalValue() - $fixed_tax, 2);
//        $value_main = $this->getTotalValue() - $value_mktplace;
//        return [
//            [
//                'recipient_id' => Parameter::getByKey(Constant::PGM_RECEIVABLE_MAIN_ID)->value,
//                //                'percentage' => $tax,
//                'amount' => number_format($value_main, 2, '', ''),
//                'liable' => Parameter::getByKey(Constant::PGM_LIABLE_MAIN)->value_bool,
//                'charge_processing_fee' => Parameter::getByKey(Constant::PGM_CHARGE_PROCESSING_FEE_MAIN)->value_bool,
//            ],
//            [
//                'recipient_id' => $bankRecipient->pgm_recipient_id,
//                //                'percentage' => 100 - $tax,
//                'amount' => number_format($value_mktplace, 2, '', ''),
//                'liable' => Parameter::getByKey(Constant::PGM_LIABLE_MARKETPLACE)->value_bool,
//                'charge_processing_fee' => Parameter::getByKey(Constant::PGM_CHARGE_PROCESSING_FEE_MARKETPLACE)->value_bool,
//            ]
//        ];
    }
}
