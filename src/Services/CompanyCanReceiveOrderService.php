<?php

namespace Rockapps\RkLaravel\Services;

use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\Company;

class CompanyCanReceiveOrderService
{
    /**
     * @param Company $company
     * @return bool
     */
    public static function canReceive($company)
    {
        $msg = [];
        foreach (self::checkReady($company) as $item) {
            if (!$item['value']) $msg[] = $item['key'];
        }
        if (count($msg)) {
            throw new ResourceException('Esta loja não está habilitada para receber pedidos. Erro: ' . implode(', ', $msg));
        }
        return true;
    }

    /**
     * @param Company $company
     * @return array
     */
    public static function checkReady($company)
    {
        $return = [];

        if ($company->address) {
            if (!$company->lat || !$company->long) {
                $company->lat = $company->address->lat;
                $company->long = $company->address->lng;
                $company->save();
            }
            $return[] = ['key' => 'Endereço selecionado', 'value' => true];
        } else {
            $return[] = ['key' => 'Não há endereço cadastrado para a empresa', 'value' => false];
        }

        if (!$company->responsible->bankRecipient || !$company->responsible->bankRecipient->bankAccount()->whereNull('deleted_at')->count()) {
            $return[] = ['key' => 'Não há conta bancária configurada', 'value' => false];
        } else {
            $return[] = ['key' => 'Conta bancária configurada', 'value' => true];
        }
        if ($company->suspended) {
            $return[] = ['key' => 'Empresa suspensa', 'value' => false];
        }
        if (!$company->active) {
            $return[] = ['key' => 'Empresa desativada', 'value' => false];
        } else {
            $return[] = ['key' => 'Empresa ativada', 'value' => true];
        }
        if (!$company->media()->count()) {
            $return[] = ['key' => 'Foto da loja', 'value' => false];
        } else {
            $return[] = ['key' => 'Foto da loja', 'value' => true];
        }
        if ($company->deliver_estimate_time === null) {
            $return[] = ['key' => 'Tempo de entrega', 'value' => false];
        } else {
            $return[] = ['key' => 'Tempo de entrega', 'value' => true];
        }
        if (!$company->products()->where('active', true)->count()) {
            $return[] = ['key' => 'Nenhum produto disponível', 'value' => false];
        } else {
            $return[] = ['key' => 'Produto disponível', 'value' => true];
        }
        if (!$company->name) {
            $return[] = ['key' => 'É necessário definir o nome da empresa', 'value' => false];
        } else {
            $return[] = ['key' => 'Nome da empresa confirmado', 'value' => true];
        }

        return $return;
    }

}
