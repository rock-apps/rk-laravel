<?php

namespace Rockapps\RkLaravel\Services;

use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\PaymentMethod;

class PaymentGatewayService
{
    /**
     * @param Company $company
     * @param $gateway
     */
    public static function setCredentials($company, $gateway)
    {
        if (!$company) return;

        if (!in_array($gateway, Payment::GATEWAYS)) {
            throw new ResourceException("Gateway inválido");
        }

        switch ($gateway) {
            case Payment::GATEWAY_PAGARME_CC:
            case Payment::GATEWAY_PAGARME_BOLETO:
            case Payment::GATEWAY_PAGARME_PIX:

                if (config('rk-laravel.marketplace.mode') === Constants::MARKETPLACE_MODE_MULTIPLE) {

                    /** @var PaymentMethod $pm */
                    $pm = $company->paymentMethods()
                        ->where('method', $gateway)
                        ->where('gateway', PaymentMethod::GATEWAY_PGM)
                        ->first();

                    if (!$pm) throw new ResourceException("Método de pagamento Pagar.Me não configurado ({$gateway})");
                    if (!$pm->gateway_webhook) throw new ResourceException("Gateway Webhook inválido");
                    if (!$pm->gateway_soft_descriptor) throw new ResourceException("Gateway Soft Descriptor inválido");
                    if (!$pm->gateway_key) throw new ResourceException("Gateway Key inválido");

                    config([
                        'rk-laravel.payment.pagarme.sandbox' => $pm->gateway_sandbox,
                        'rk-laravel.payment.pagarme.api_key_dev' => $pm->gateway_key,
                        'rk-laravel.payment.pagarme.api_key_prod' => $pm->gateway_key,
                        'rk-laravel.payment.pagarme.webhook' => $pm->gateway_webhook,
                        'rk-laravel.payment.pagarme.soft_descriptor' => $pm->gateway_soft_descriptor,
                    ]);
                }

                break;

            case Payment::GATEWAY_FATURA:
            case Payment::GATEWAY_CASH:
            case Payment::GATEWAY_BANK_TRANSFER:
            case Payment::GATEWAY_BALANCE:
            case Payment::GATEWAY_METHOD_IAP_APPLE:
            case Payment::GATEWAY_METHOD_IAP_GOOGLE:
            case Payment::GATEWAY_MANUAL:
            case Payment::GATEWAY_VOUCHER:
            default:
        }
    }
}
