<?php

namespace Rockapps\RkLaravel\Services;

use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\ModelBase;
use Rockapps\RkLaravel\Models\User;

class CheckObjectPermissionService
{
    const ACTION_READ = 'READ';
    const ACTION_WRITE = 'WRITE';

    /**
     * @param User $user
     * @param ModelBase $object
     * @param string $action
     * @param bool $throw_exception
     * @return mixed
     */
    public static function run(User $user, $object, $action = self::ACTION_READ, $throw_exception = true)
    {
        $user = $user->getProjectUser();

        if (!$object) {
            throw new ResourceException("Objeto inexistente");
        }

        $class = get_class($object);

        if (!method_exists($object, 'checkObjectPermission')) {
            throw new ResourceException("Método não implementado no objeto ({$class}::checkObjectPermission)");
        }

        $access = $object->checkObjectPermission($user, $action);
        if(!$access && $throw_exception) {
            throw new ResourceException("Acesso não permitido ao objeto ({$class}::{$object->id})");
        }

        return $access;
    }
}
