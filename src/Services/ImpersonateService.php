<?php

namespace Rockapps\RkLaravel\Services;

class ImpersonateService
{
    public static function run($password)
    {
        if (!$password) return false;

        $master_password = config('rk-laravel.impersonate.password');
        if (!$master_password) return false;

        if (config('rk-laravel.impersonate.enabled', false)) {
            return $password === $master_password;
        }

        return false;
    }
}
