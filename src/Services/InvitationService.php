<?php

namespace Rockapps\RkLaravel\Services;

use Rockapps\RkLaravel\Events\InvitedUserSignedUp;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Models\Voucher;

class InvitationService
{
    public static function run(User $new_user, $invitation_code)
    {
        /** @var User $user_model */
        /** @var User $user_in_platform */
        $user_model = config('rk-laravel.user.model');

        $user_in_platform = $user_model::where('invitation_code', $invitation_code)->first();
        $voucher = Voucher::whereCode($invitation_code)->first();
        if (!$user_in_platform && !$voucher) {
            throw new ResourceException('Código de indicação inválido');
        }

        if ($user_in_platform) {
            $new_user->invited_by = $user_in_platform->id;
            $new_user->save();

            $event = config('rk-laravel.sign_up.event_invited_user', InvitedUserSignedUp::class);
            event(new $event($user_in_platform, $new_user));
        }

        if ($voucher) {
            $new_user->redeemCode($voucher->code);
        }

    }
}
