<?php

namespace Rockapps\RkLaravel\Services;

use Illuminate\Support\Str;
use Rockapps\RkLaravel\Models\User;

class SentrySendUserToken
{
    public static function run(User $user)
    {

        if (
            in_array(env('APP_ENV'), ['homolog', 'local']) ||
            Str::contains($user->email, '@rockapps.com.br')) {

            return true;

        }
        return false;
    }
}
