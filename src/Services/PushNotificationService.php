<?php

namespace Rockapps\RkLaravel\Services;

use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\User;

class PushNotificationService
{
    public static function destinations()
    {
        return collect([
            ['id' => 1, 'key' => 'USERS_ALL', 'label' => 'Todos os Usuários'],
            ['id' => 2, 'key' => 'USERS_ADMIN', 'label' => 'Todos os Administradores'],
        ]);
    }

    public static function findUsersByKey($key)
    {
        switch ($key) {
            case 'USERS_ALL':
                return User::all();
            case 'USERS_ADMIN':
                return User::withRole('admin')->all();
            default:

        }

        throw new ResourceException('O grupo de destinatários não foi configurado.');
    }
}
