<?php

namespace Rockapps\RkLaravel\Services;

use Illuminate\Support\Str;
use Rockapps\RkLaravel\Helpers\PhpToCurl;

class LogRequestService
{
    public static function allow()
    {
        return true;
    }

    public static function level()
    {
        if (Str::contains(request()->getUri(), 'webhook')) {
            return 'WEBHOOK';
        }
        if (Str::contains(request()->getUri(), 'iap')) {
            return 'IAP';
        }
        if (Str::contains(request()->getUri(), 'pagarme')) {
            return 'PAGARME';
        }
        if (Str::contains(request()->getUri(), 'sample')) {
            return 'SAMPLE';
        }
        return 'DEBUG';
    }

    public static function curl()
    {
        return (new PhpToCurl())->doAll();
    }
}
