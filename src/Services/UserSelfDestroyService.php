<?php

namespace Rockapps\RkLaravel\Services;

use Rockapps\RkLaravel\Models\User;

class UserSelfDestroyService
{
    public static function run(User $user)
    {
        /** @var User $user_model */
        /** @var User $user_to_destroy */
        $user_model = config('rk-laravel.user.model');
        $user_to_destroy = $user_model::where('id', $user->id)->first();

        $faker = \Faker\Factory::create();

        \DB::table('users')
            ->where('id', $user_to_destroy->id)
            ->update([
                'name' => $faker->lexify('??????????????'),
                'email' => $faker->lexify('???????????????') . '@anonimize.rk.com',
                'document' => $faker->numerify('###########'),
                'birth_date' => '1900-01-01',
                'description' => $faker->lexify('??????????????'),
                'photo' => null,
                'gender' => null,
                'mobile' => $faker->numerify('#########'),
                'telephone' => $faker->numerify('#########'),
                'password' => 'anonimizado',
//                'admin_comments',
//                'can_pay_with_bank',
//                'can_pay_with_boleto',
//                'can_pay_with_cash',
//                'can_pay_with_cc',
//                'education' => $faker->lexify('??????????????'),
//                'handcap',
//                'suspended',
                'username' => $faker->lexify('??????????????'),
//                'online_status',
//                'show_online_status',
//                'description_internal',

            ]);

    }
}
