<?php

namespace Rockapps\RkLaravel\Services;

use Illuminate\Support\Str;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Models\Parameter;

class MaintenanceModeCheckerService
{
    public static function check()
    {
        /**
         * Utilizar livremente User, Parameter, Url e Env
         */

        $params = Parameter::where('key',Constants::MAINTENANCE_MODE)->get()->first();

        $param_on = $params ? $params->value_bool : false;

        $in_maintenance = $param_on || env('MAINTENANCE_MODE', false);

        if (!$in_maintenance) return false;

        $url = request()->url();
        if (Str::contains($url, 'maintenance/on')) {
            return true;
        } else {
            return false;
        }

        $user = \Auth::getUser();
        if (!$user) return true;

        if ($user->hasRole('cliente')) {
            return true;
        }

        if (
            Str::contains($user->email, '@rockapps.com.br') ||
            $user->email === 'admin@rockapps.com.br' ||
            Str::contains($user->id, 27)
        ) {
            return false;
        }

        return true;
    }

}
