<?php

namespace Rockapps\RkLaravel\Services;

use Rockapps\RkLaravel\Events\InvitedUserSignedUp;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\OrderVirtualUnit;
use Rockapps\RkLaravel\Models\User;

class UserGetBalanceService
{
    public static function run(User $user)
    {
//        $user = $user->getProjectUser();
        $add = $user->ordersVirtualUnit()
            ->where('status', OrderVirtualUnit::STATUS_CONFIRMED_PURCHASE)
            ->sum('virtual_units');

        // Excluir saldo
        // $transcriptions_sum = $this->transcriptions()->sum('seconds');
        $minus = 0;

        return $add - $minus;

    }
}
