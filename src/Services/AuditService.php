<?php

namespace Rockapps\RkLaravel\Services;

use Illuminate\Support\Arr;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Models\Audit;

class AuditService
{
    public static function filterByAttribute(Auditable $object, $field, $only_values = false)
    {
        $audits = $object->audits()
            ->get()
            ->filter(function (Audit $audit) use ($field) {
                return Arr::has($audit->new_values, $field);
            })
            ->values(); // Removes os índices inválidos que foram filtrados

        if ($only_values) {
            return $audits->map(function (Audit $audit) use ($field) {
                return $audit->new_values[$field];
            });
        }

        return $audits;

    }
}
