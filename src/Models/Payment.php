<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Iben\Statable\Statable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Validation\Rule;
use OwenIt\Auditing\Auditable;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\Iap\GooglePayment;
use Rockapps\RkLaravel\Helpers\Iap\IosPayment;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeTransaction;
use Rockapps\RkLaravel\Interfaces\PayerInterface;
use Rockapps\RkLaravel\Interfaces\PurchaseableInterface;
use Rockapps\RkLaravel\ModelFilters\PaymentFilter;
use Rockapps\RkLaravel\Traits\Attributes\PaymentAttributesTrait;
use Rockapps\RkLaravel\Traits\States\PaymentStateTrait;

/**
 * Rockapps\RkLaravel\Models\Payment
 *
 * @property int $id
 * @property float $value
 * @property bool $async
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $paid_at
 * @property string $gateway
 * @property string|null $pgm_transaction_id
 * @property string|null $comments_operator
 * @property string|null $boleto_url
 * @property \Illuminate\Support\Carbon|null $boleto_expiration_date
 * @property string|null $boleto_barcode
 * @property string|null $boleto_instructions
 * @property int|null $credit_card_id
 * @property int|null $address_id
 * @property string $payer_type
 * @property int $payer_id
 * @property string|null $purchaseable_type
 * @property int|null $purchaseable_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Address $address
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read CreditCard|null $creditCard
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|Payment filter($input = [], $filter = null)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment newQuery()
 * @method static \Illuminate\Database\Query\Builder|Payment onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereAsync($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereBoletoBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereBoletoExpirationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereBoletoInstructions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereBoletoUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereCommentsOperator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereCreditCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereGateway($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Payment wherePaidAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment wherePayerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment wherePayerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment wherePgmTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment wherePurchaseableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment wherePurchaseableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|Payment withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Payment withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $payer
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $purchaseable
 * @property-read \Illuminate\Database\Eloquent\Collection|\Iben\Statable\Models\StateHistory[] $stateHistory
 * @property-read int|null $state_history_count
 * @property int|null $subscription_id
 * @property-read \Rockapps\RkLaravel\Models\Subscription $subscription
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Payment whereSubscriptionId($value)
 * @property string $direction
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Payment whereDirection($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\BankTransfer[] $bankTransfers
 * @property-read int|null $bank_transfers_count
 * @property int $change_purchaseable_state
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Payment whereChangePurchaseableState($value)
 * @property string|null $iap_receipt_id
 * @property string|null $apple_transaction_id
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereAppleTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereIapReceiptId($value)
 * @property string|null $iap_product_id
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereIapProductId($value)
 * @property \Illuminate\Support\Carbon|null $pix_expiration_date
 * @property string|null $pix_qr_code
 * @method static \Illuminate\Database\Eloquent\Builder|Payment wherePixExpirationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment wherePixQrCode($value)
 * @property string|null $google_order_id
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereGoogleOrderId($value)
 * @property int|null $product_id
 * @property-read \Rockapps\RkLaravel\Models\Product|null $product
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereProductId($value)
 * @property int|null $company_id
 * @property-read \Rockapps\RkLaravel\Models\Company|null $company
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereCompanyId($value)
 */
class Payment extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Auditable, Notifiable, Filterable, Statable;
    use PaymentStateTrait;
    use PaymentAttributesTrait;

    const STATUS_NOT_STARTED = 'NOT_STARTED';
    const STATUS_PROCESSING = 'PROCESSING';
    const STATUS_AUTHORIZED = 'AUTHORIZED';
    const STATUS_PAID = 'PAID';
    const STATUS_REFUNDED = 'REFUNDED';
    const STATUS_WAITING_PAYMENT = 'WAITING_PAYMENT';
    const STATUS_PENDING_REFUND = 'PENDING_REFUND';
    const STATUS_REFUSED = 'REFUSED';
    const STATUS_CANCELED = 'CANCELED';
    const STATUSES = [
        self::STATUS_WAITING_PAYMENT,
        self::STATUS_PROCESSING,
        self::STATUS_AUTHORIZED,
        self::STATUS_PAID,
        self::STATUS_PENDING_REFUND,
        self::STATUS_REFUNDED,
        self::STATUS_REFUSED,
        self::STATUS_CANCELED
    ];
    const GATEWAY_CASH = 'CASH';
    const GATEWAY_PAGARME_CC = 'PAGARME_CC';
    const GATEWAY_PAGARME_BOLETO = 'PAGARME_BOLETO';
    const GATEWAY_PAGARME_PIX = 'PAGARME_PIX';
    const GATEWAY_FATURA = 'FATURA';
    const GATEWAY_BANK_TRANSFER = 'BANK_TRANSFER'; // Não será cadastrado!
    const GATEWAY_BALANCE = 'BALANCE'; // Não será cadastrado!
    const GATEWAY_MANUAL = 'MANUAL'; // Não será cadastrado!
    const GATEWAY_METHOD_IAP_APPLE = 'IAP_APPLE';
    const GATEWAY_METHOD_IAP_GOOGLE = 'IAP_GOOGLE';
    const GATEWAY_VOUCHER = 'VOUCHER';
    const GATEWAYS = [
        self::GATEWAY_PAGARME_CC,
        self::GATEWAY_PAGARME_BOLETO,
        self::GATEWAY_PAGARME_PIX,
        self::GATEWAY_FATURA,
        self::GATEWAY_CASH,
        self::GATEWAY_BANK_TRANSFER,
        self::GATEWAY_BALANCE,
        self::GATEWAY_METHOD_IAP_APPLE,
        self::GATEWAY_METHOD_IAP_GOOGLE,
        self::GATEWAY_MANUAL,
        self::GATEWAY_VOUCHER,
    ];

    const DIRECTION_INCOME = 'INCOME';
    const DIRECTION_OUTCOME = 'OUTCOME';
    const DIRECTIONS = [
        self::DIRECTION_INCOME,
        self::DIRECTION_OUTCOME,
    ];

    protected $casts = [
        'value' => 'float',
        'purchaseable_id' => 'int',
        'payer_id' => 'int',
        'credit_card_id' => 'int',
        'company_id' => 'int',
        'async' => 'bool',
        'change_purchaseable_state' => 'bool',
    ];
    protected $dates = [
        'paid_at',
        'boleto_expiration_date',
        'pix_expiration_date',
    ];
    protected $attributes = [
        'status' => self::STATUS_NOT_STARTED,
        'async' => false,
        'change_purchaseable_state' => true,
        'direction' => self::DIRECTION_INCOME,
    ];
    protected $fillable = [
        'credit_card_id',
        'gateway',
        'comments_operator',
        'boleto_instructions',
        'boleto_expiration_date',
        'boleto_barcode',
        'pix_expiration_date',
        'pix_qr_code',
        'value',
        'direction',
        'status',
        'async',
        'pgm_transaction_id',
        'payer_id',
        'address_id',
        'product_id',
        'company_id',
        'payer_type',
        'purchaseable_id',
        'purchaseable_type',
    ];
    protected $rules = [
//        'address_id' => 'required|numeric|exists:addresses,id,address_id,{$address_id}', # Vai verificar se já existe na tabela. Now testes, ainda não salvou na base.
        'payer_id' => 'required|numeric',
        'payer_type' => 'required|string',
        'purchaseable_id' => 'nullable|numeric',
        'purchaseable_type' => 'nullable|string',
        'value' => 'required|numeric|gt:0',
        'gateway' => 'required|string',
        'comments_operator' => 'nullable|string',
        'credit_card_id' => 'nullable|numeric',
        'address_id' => 'nullable|numeric',
        'iap_receipt_id' => 'nullable|string',
        'apple_transaction_id' => 'nullable|string',
        'google_order_id' => 'nullable|string',
        'product_id' => 'nullable|numeric|exists:products,id',
        'company_id' => 'nullable|numeric|exists:companies,id',
    ];
    protected $validationMessages = [
        'error.save' => 'Erro ao salvar o pagamento.',
        'error.not_found' => 'Pagamento não encontrado.',
        'address_id.required' => 'Endereço inválido.',
        'payer_type.required' => 'O pagador é obrigatório.',
        'payer_id.required' => 'O pagador é obrigatório.',
    ];

    public function modelFilter()
    {
        return $this->provideFilter(config('rk-laravel.payment.filter', PaymentFilter::class));
    }

    public function validate($rules = [])
    {
        # TODO Verificar se o Parameters permite que o sistema aceite pagamento nos meios.

        $rules['gateway'] = ['required', 'string', Rule::in(self::GATEWAYS)];

        if ($this->creditCard && $this->creditCard->owner->id !== $this->payer->id) {
            $this->validationErrors->add('not_owner', 'Este cartão não pertence a este pagador.');
        }

        /** @var PayerInterface $payer */
        $payer = $this->payer;
        if (!$payer) {
            throw new ResourceException('Payer não definido.');
        }
        if ($this->gateway === Payment::GATEWAY_BALANCE && !$payer->can_pay_with_balance) {
            throw new ResourceException('O cliente não pode realizar pagamentos com saldo.');
        }
        if ($this->gateway === Payment::GATEWAY_FATURA && !$payer->can_pay_with_boleto) {
            throw new ResourceException('O cliente não pode realizar pagamentos faturados.');
        }
        if ($this->gateway === Payment::GATEWAY_BANK_TRANSFER && !$payer->can_pay_with_bank) {
            throw new ResourceException('O cliente não pode realizar pagamentos com transferência.');
        }
        if ($this->gateway === Payment::GATEWAY_CASH && !$payer->can_pay_with_cash) {
            throw new ResourceException('O cliente não pode realizar pagamentos com dinheiro.');
        }
        if ($this->gateway === Payment::GATEWAY_PAGARME_CC && !$payer->can_pay_with_cc) {
            throw new ResourceException('O cliente não pode realizar pagamentos com cartão de crédito.');
        }
        if ($this->gateway === Payment::GATEWAY_PAGARME_BOLETO && !$payer->can_pay_with_boleto) {
            throw new ResourceException('O cliente não pode realizar pagamentos com boleto.');
        }
        if ($this->gateway === Payment::GATEWAY_PAGARME_PIX) {
            if (!$payer->can_pay_with_pix) throw new ResourceException('O cliente não pode realizar pagamentos com PIX.');
            $add_minutes = config('rk-laravel.payment.pagarme.pix_expiration_add_minutes', 5 * 60);
            if (!$this->pix_expiration_date) $this->pix_expiration_date = now()->addMinutes($add_minutes);
            $this->async = true;
        }
        if ($this->gateway === Payment::GATEWAY_METHOD_IAP_APPLE) {

            if (!$this->iap_receipt_id) {
                throw new ResourceException('O código IAP Receipt ID é obrigatório.');
            }
            if (!$this->apple_transaction_id) { // || goole_transaction_id quando tive!!!
                throw new ResourceException('O código IAP Apple Transaction ID é obrigatório.');
            }
        }
        if ($this->gateway === Payment::GATEWAY_METHOD_IAP_GOOGLE) {
            if (!$this->iap_receipt_id) {
                throw new ResourceException('O código IAP Receipt ID (Google purchase_token) é obrigatório.');
            }
        }
        if (!\in_array($this->gateway, self::GATEWAYS)) {
            throw new ResourceException('Tipo de pagamento inválido.');
        }

        /**
         * Só verifica o cartão caso seja um marketplace único
         * Se for múltiplo, o CC não é criado no gateway e não há como validar
         */
        if (Constants::isMarketplaceDisabled() || Constants::isMarketplaceCentralized()) {
            if ($this->gateway === Payment::GATEWAY_PAGARME_CC && !$this->creditCard) {
                throw new ResourceException('É necessário selecionar um cartão de crédito.');
            }
        }

        if ($this->isExternalGateway() && !$this->address) {
            $address = Address::whereRelatedType($this->payer_type)
                ->whereRelatedId($this->payer_id)
                ->first();
            if ($address) {
                $this->address_id = $address->id;
            } else {
                throw new ResourceException('É necessário que o cliente tenha ao menos um endereço cadastrado');
            }
        }

        return parent::validate($rules);
    }

    public function isExternalGateway()
    {
        return in_array($this->gateway, [
            self::GATEWAY_PAGARME_CC,
            self::GATEWAY_PAGARME_BOLETO,
            self::GATEWAY_PAGARME_PIX,
        ]);
    }

    public function purchaseable()
    {
        return $this->morphTo('purchaseable');
    }

    /*
     *
     * RELATION
     *
     */

    public function payer()
    {
        return $this->morphTo('payer');
    }

    public function creditCard()
    {
        return $this->belongsTo(CreditCard::class, 'credit_card_id', 'id')->withTrashed();
    }

    public function address()
    {
        return $this->hasOne(Address::class, 'id', 'address_id')->withTrashed();
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id')->withTrashed();
    }

    public function subscription()
    {
        return $this->hasOne(Subscription::class, 'id', 'subscription_id')->withTrashed();
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id')->withTrashed();
    }

    public function bankTransfers()
    {
        return $this->hasMany(BankTransfer::class, 'payment_id', 'id')->withTrashed();
    }

    /**
     * @param $item_title
     * @param null $split_rules
     * @param array $data Dados adicionais necessário para alguns gateways
     * @return $this|Payment
     * @throws \Google\Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Container\EntryNotFoundException
     * @throws \SM\SMException
     */
    public function charge($item_title, $split_rules = null, $data = [])
    {
        if ($this->isPaid()) {
            throw new ResourceException('Este pagamento já encontra-se pago');
        }

        switch ($this->gateway) {
            case Payment::GATEWAY_BANK_TRANSFER:
            case Payment::GATEWAY_CASH:
                $this->save();
                return $this;

            case Payment::GATEWAY_FATURA:
                // No modo fatura, não é criado um pagamento
                $this->changePurchaseableStatus(true);
                return $this;

            case Payment::GATEWAY_METHOD_IAP_APPLE:
                # TODO: Criar evento
                return IosPayment::create($this);

            case Payment::GATEWAY_METHOD_IAP_GOOGLE:
                # TODO: Criar evento
                return GooglePayment::create($this);

            case Payment::GATEWAY_PAGARME_CC:
                $transaction = new PagarMeTransaction($this, $split_rules);
                $transaction->addItem("#" . $this->purchaseable_id ?: $this->purchaseable_id, $item_title);
                return $transaction->chargeCC($data);

            case Payment::GATEWAY_PAGARME_BOLETO:
                $this->async = true;
                $transaction = new PagarMeTransaction($this, $split_rules);
                $transaction->addItem("#" . $this->purchaseable_id ?: $this->purchaseable_id, $item_title);
                return $transaction->chargeBoleto($this->boleto_expiration_date, $this->boleto_instructions);

            case Payment::GATEWAY_PAGARME_PIX:
                $this->async = true;
                $transaction = new PagarMeTransaction($this, $split_rules);
                $transaction->addItem("#" . $this->purchaseable_id ?: $this->purchaseable_id, $item_title);
                return $transaction->chargePix($this->pix_expiration_date);

            case Payment::GATEWAY_BALANCE:
                if ($this->payer->getBalance() >= $this->value) {
                    $this->apply(Payment::STATUS_PAID);
                    $this->save();
                    return $this;
                } else {
                    throw new ResourceException('Saldo insuficiente');
                }
            default:
                throw new ResourceException('Gateway inválido');
        }
        return $this;
    }

    public function isPaid()
    {
        return $this->status === self::STATUS_PAID && $this->paid_at;
    }

    public function changePurchaseableStatus($approved = true)
    {
        /*
         * Não verificar com $this->purchaseable pois ele vai chamar o relacionamento e disparar um erro caso o relacionamento não exista
         */
        if ($this->change_purchaseable_state && $this->purchaseable_type && $this->purchaseable_id) {
            $this->purchaseable->apply($approved ? PurchaseableInterface::STATUS_CONFIRMED_PURCHASE : PurchaseableInterface::STATUS_BLOCKED_PURCHASE);
            $this->purchaseable->save();
        }
        return true;
    }

    public function syncWithGateway()
    {
        return PagarMeTransaction::sync($this);
    }

    protected function getGraph()
    {
        return 'payment'; // the SM config to use
    }
}
