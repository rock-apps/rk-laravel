<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeBankTransfer;
use Rockapps\RkLaravel\ModelFilters\BankTransferFilter;
use Rockapps\RkLaravel\Traits\Attributes\BankTransferAttributesTrait;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Traits\States\BankTransferStateTrait;
use Rockapps\RkLaravel\Constants\Constants;
/**
 * Rockapps\RkLaravel\Models\BankTransfer
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Rockapps\RkLaravel\Models\BankAccount $creditCard
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $owner
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer filter($input = [], $filter = null)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\BankTransfer onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\BankTransfer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\BankTransfer withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $pgm_transfer_id
 * @property string|null $pgm_recipient_id
 * @property string|null $pgm_transaction_id
 * @property float $value
 * @property float $fee
 * @property string $status
 * @property string $type
 * @property string $transfered_type
 * @property int $transfered_id
 * @property int|null $bank_account_id
 * @property string|null $comments_operator
 * @property string $gateway
 * @property \Illuminate\Support\Carbon|null $funding_at
 * @property \Illuminate\Support\Carbon|null $funding_estimated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereBankAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereCommentsOperator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereFundingAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereFundingEstimatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereGateway($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer wherePgmRecipientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer wherePgmTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer wherePgmTransferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereTransferedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereTransferedType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer whereValue($value)
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $transfered
 * @property-read \Rockapps\RkLaravel\Models\BankAccount|null $bankAccount
 * @property int|null $payment_id
 * @property-read \Rockapps\RkLaravel\Models\Payment|null $payment
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankTransfer wherePaymentId($value)
 * @property string|null $currency
 * @property float|null $virtual_units
 * @method static \Illuminate\Database\Eloquent\Builder|BankTransfer whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BankTransfer whereVirtualUnits($value)
 * @property string|null $virtual_units_tax
 * @method static \Illuminate\Database\Eloquent\Builder|BankTransfer whereVirtualUnitsTax($value)
 */
class BankTransfer extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Auditable, Filterable;
    use BankTransferStateTrait;
    use BankTransferAttributesTrait;

    const TYPE_DOC = 'DOC';
    const TYPE_TED = 'TED';
    const TYPE_CREDITO_CONTA = 'CREDITO_EM_CONTA';
    const TYPE_MANUAL = 'MANUAL';
    const TYPE_NEW = 'NEW';
    const TYPE_PIX = 'PIX';
    const TYPES = [self::TYPE_NEW, self::TYPE_DOC, self::TYPE_TED, self::TYPE_CREDITO_CONTA, self::TYPE_MANUAL, self::TYPE_PIX];

    const GATEWAY_PAGARME_TRANSFER = 'PAGARME_TRANSFER';
    const GATEWAY_PAGARME_PAYMENT_SPLIT = 'PAGARME_PAYMENT_SPLIT';
    const GATEWAY_MANUAL = 'MANUAL';
    const GATEWAYS = [self::GATEWAY_MANUAL, self::GATEWAY_PAGARME_TRANSFER, self::GATEWAY_PAGARME_PAYMENT_SPLIT];

    const STATUS_REQUESTED = 'REQUESTED'; // Não faz parte do PAGARME
    const STATUS_PENDING_TRANSFER = 'PENDING_TRANSFER';
    const STATUS_TRANSFERRED = 'TRANSFERRED';
    const STATUS_FAILED = 'FAILED';
    const STATUS_PROCESSING = 'PROCESSING';
    const STATUS_CANCELED = 'CANCELED';
    const STATUSES = [self::STATUS_REQUESTED, self::STATUS_PENDING_TRANSFER, self::STATUS_TRANSFERRED, self::STATUS_PROCESSING, self::STATUS_FAILED, self::STATUS_CANCELED];

    protected $attributes = [
        'fee' => 0,
        'currency' => 'BRL'
//        'status' => self::STATUS_PROCESSING
    ];
    protected $dates = [
        'funding_at',
        'funding_estimated_at'
    ];
    protected $casts = [
        'value' => 'float',
        'fee' => 'float',
        'transfered_id' => 'int',
        'payment_id' => 'int',
        'virtual_units' => 'int'
    ];
    protected $fillable = [
        'gateway',
        'comments_operator',
        'bank_account_id',
        'transfered_id',
        'transfered_type',
        'type',
        'status',
        'fee',
        'value',
        'payment_id',
    ];


    protected $rules = [
        'gateway' => 'required',
        'bank_account_id' => 'nullable|exists:bank_accounts,id',
        'transfered_id' => 'required',
        'transfered_type' => 'required',
        'type' => 'required',
        'status' => 'required',
        'fee' => 'required|numeric|min:0',
        'value' => 'required|numeric|min:0',
        'payment_id' => 'nullable|numeric|exists:payments,id',
        'virtual_units' => 'nullable|numeric',
        'virtual_units_tax' => 'nullable|numeric',
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar a transferência.',
        'error.not_found' => 'Transferência não encontrada.',
    ];

    public function payment()
    {
        return $this->belongsTo(Payment::class)->withTrashed();
    }

    public function validate(array $rules = array())
    {
        $rules['status'] = ['required'
//            , Rule::in(self::TYPES) // Não está funcionando. Os testes estão falhando
        ];
//        if ($this->gateway === self::GATEWAY_PAGARME) {
//            $rules['bank_account_id'] = 'required';
//        }
//        if ($this->gateway === self::GATEWAY_MANUAL) {
//            if($this->status !== self::STATUS_TRANSFERRED) {
//                throw new ResourceException('O gateway manual só permite o status transferido');
//            }
//            if($this->type !== self::TYPE_MANUAL) {
//                throw new ResourceException('O gateway manual só permite o tipo de transferência manual');
//            }
//        }

        /** @var Parameter $virtual_unit_tax */
        $virtual_unit_tax = Parameter::where('key', Constants::VIRTUAL_UNIT_BANK_TRANSFER_TAX)->first();
        if ($virtual_unit_tax) {
            $this->virtual_units_tax = $virtual_unit_tax->value_float;
            $this->virtual_units = (int)($this->value / $this->virtual_units_tax);
        }

        return parent::validate($rules);
    }

    public function delete()
    {
        if ($this->gateway === self::GATEWAY_PAGARME_PAYMENT_SPLIT || $this->gateway === self::GATEWAY_PAGARME_TRANSFER && $this->status !== self::STATUS_REQUESTED) {
            throw new ResourceException('Não é possível excluir está transferência foi já está vinculada ao gateway');
        }
        return parent::delete();
    }

    public function modelFilter()
    {
        return $this->provideFilter(BankTransferFilter::class);
    }

    public function transfered()
    {
        return $this->morphTo('transfered');
    }

    public function bankAccount()
    {
        return $this->belongsTo(BankAccount::class, 'bank_account_id', 'id')->withTrashed();
    }

    public function syncWithGateway()
    {
        return PagarMeBankTransfer::sync($this);
    }

}
