<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;
use OwenIt\Auditing\Auditable;
use Rockapps\RkLaravel\ModelFilters\CampaignHitFilter;
use Rockapps\RkLaravel\Transformers\CampaignHitTransformer;


/**
 * Rockapps\RkLaravel\Models\CampaignHit
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Rockapps\RkLaravel\Models\Campaign $campaign
 * @property-read \Rockapps\RkLaravel\Models\CampaignElement $element
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignHit filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignHit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignHit newQuery()
 * @method static \Illuminate\Database\Query\Builder|CampaignHit onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignHit paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignHit query()
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignHit simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignHit whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignHit whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignHit whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Query\Builder|CampaignHit withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CampaignHit withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $method
 * @property int $campaign_id
 * @property string $element_type
 * @property int $element_id
 * @property int $campaign_element_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|CampaignHit onlyClicks()
 * @method static Builder|CampaignHit onlyViews()
 * @method static Builder|CampaignHit whereCampaignElementId($value)
 * @method static Builder|CampaignHit whereCampaignId($value)
 * @method static Builder|CampaignHit whereCreatedAt($value)
 * @method static Builder|CampaignHit whereElementId($value)
 * @method static Builder|CampaignHit whereElementType($value)
 * @method static Builder|CampaignHit whereId($value)
 * @method static Builder|CampaignHit whereMethod($value)
 * @method static Builder|CampaignHit whereUpdatedAt($value)
 * @method static Builder|CampaignHit whereUserId($value)
 * @property string $ip
 * @property string $agent
 * @method static Builder|CampaignHit whereAgent($value)
 * @method static Builder|CampaignHit whereIp($value)
 */
class CampaignHit extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use Auditable, Filterable;

    const METHOD_VIEW = 'VIEW';
    const METHOD_CLICK = 'CLICK';

    public $transformer = CampaignHitTransformer::class;

    protected $table = 'campaign_hits';

    public static function boot()
    {
        parent::boot();

        self::created(function (CampaignHit $hit) {

            $campaign = $hit->campaign;

            if ($hit->method === CampaignHit::METHOD_VIEW) {
                $campaign->actual_views++;
            } else if ($hit->method === CampaignHit::METHOD_CLICK) {
                $campaign->actual_clicks++;
            }
            $campaign->save();
        });
    }

    protected $casts = [
        'user_id' => 'int',
        'campaign_id' => 'int',
        'element_id' => 'int',
        'campaign_element_id' => 'int'
    ];

    protected $dates = [];

    protected $fillable = [
        'user_id',
        'method',
        'campaign_id',
        'element_type',
        'element_id',
        'campaign_element_id'
    ];

    protected $rules = [
        'user_id' => 'nullable|exists:users,id',
        'method' => 'required',
        'ip' => 'required|ip',
        'agent' => 'required',
        'campaign_id' => 'nullable|exists:campaigns,id',
        'element_id' => 'required|numeric',
        'element_type' => 'required|string',
        'campaign_element_id' => 'required|exists:campaign_elements,id',
    ];

    protected $attributes = [
    ];

    public function validate(array $rules = array())
    {
        $this->rules['method'] = ['required', Rule::in([self::METHOD_CLICK, self::METHOD_VIEW])];

        return parent::validate($rules);
    }

    public function campaign()
    {
        return $this->belongsTo(config('rk-laravel.campaign.model', \Rockapps\RkLaravel\Models\Campaign::class), 'campaign_id');
    }

    public function element()
    {
        return $this->belongsTo(config('rk-laravel.campaign_element.model', \Rockapps\RkLaravel\Models\CampaignElement::class), 'campaign_element_id');
    }

    public function modelFilter()
    {
        return $this->provideFilter(config('rk-laravel.campaign_hit.filter', CampaignHitFilter::class));
    }


    /**
     * @param Builder $query
     * @return Builder
     * @noinspection PhpUnused
     */
    public function scopeOnlyViews($query)
    {
        return $query->where('method',self::METHOD_VIEW);
    }

    /**
     * @param Builder $query
     * @return Builder
     * @noinspection PhpUnused
     */
    public function scopeOnlyClicks($query)
    {
        return $query->where('method',self::METHOD_CLICK);
    }
}
