<?php

namespace Rockapps\RkLaravel\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\ModelBase;

/**
 * Rockapps\RkLaravel\Models\Item
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Rockapps\RkLaravel\Models\Order $order
 * @property-read \Rockapps\RkLaravel\Models\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\OrderItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\OrderItem newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\OrderItem onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\OrderItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\OrderItem withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\OrderItem withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $comments
 * @property int $quantity
 * @property float $unit_value
 * @property float $total_value
 * @property int|null $product_id
 * @property int|null $order_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\OrderItem whereComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\OrderItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\OrderItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\OrderItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\OrderItem whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\OrderItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\OrderItem whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\OrderItem whereTotalValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\OrderItem whereUnitValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\OrderItem whereUpdatedAt($value)
 */
class OrderItem extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Auditable;

    protected $casts = [
        'quantity' => 'int',
        'unit_value' => 'float',
        'total_value' => 'float',
        'product_id' => 'int',
        'order_id' => 'int'
    ];

    protected $fillable = [
        'comments',
        'quantity',
        'unit_value',
        'total_value',
        'product_id',
    ];

    protected $rules = [
        'comments' => 'nullable|string',
        'quantity' => 'required|numeric',
        'product_id' => 'required|numeric|exists:products,id',
        'order_id' => 'required|numeric|exists:orders,id',
    ];
    protected $attributes = [
        'quantity' => 0,
        'unit_value' => 0,
        'total_value' => 0
    ];

    public static function boot()
    {
        parent::boot();
        self::saved(function (OrderItem $item) {
            $order = $item->order;
            $order->total_value = $order->getTotalValue();
            $order->save();
        });
        self::deleted(function (OrderItem $item) {
            $order = $item->order;
            $order->total_value = $order->getTotalValue();
            $order->save();
        });

    }

    public function validate(array $rules = array())
    {
        $product = Product::withTrashed()->findOrFail($this->product_id);
        $this->unit_value = $product->unit_value;
        $this->total_value = $this->quantity * $this->unit_value;

        $order = Order::findOrFail($this->order_id);

        if ($product->company_id !== $order->company_id) {
            throw new ResourceException('Este produto não pertence a essa empresa.');
        }

        if ($order->status !== Order::STATUS_DRAFT) {
            throw new ResourceException('Não é possível adicionar itens a este pedido.');
        }

        if (!$this->id) {
            if (!$product->active || $product->deleted_at) {
                throw new ResourceException("O produto $product->name não está mais disponível.");
            }
        }

        return parent::validate($rules);
    }

    public function order()
    {
        return $this->belongsTo(\Rockapps\RkLaravel\Models\Order::class, 'order_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(\Rockapps\RkLaravel\Models\Product::class, 'product_id', 'id')->withTrashed();
    }

    public function delete()
    {
        if($this->order->status !== Order::STATUS_DRAFT){
            throw new ResourceException('Não é mais possível excluir um item deste pedido');
        }
        return parent::delete();
    }
}
