<?php

namespace Rockapps\RkLaravel\Models;


use Illuminate\Database\Eloquent\SoftDeletes;
use Rockapps\RkLaravel\Traits\Like\Likeable;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Interfaces\CommentableInterface;
use Rockapps\RkLaravel\Traits\Commentable;

/**
 * Rockapps\RkLaravel\Models\Comment
 *
 * @property int $id
 * @property string $commenter_type
 * @property int $commenter_id
 * @property string $commentable_type
 * @property int $commentable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $commentable
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $commenter
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Comment whereCommentableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Comment whereCommentableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Comment whereCommenterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Comment whereCommenterType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Comment whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $comment
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Rating[] $likes
 * @property-read int|null $likes_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Comment whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Comment whereDeletedAt($value)
 * @property bool $is_approved
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Comment whereIsApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereLike($column, $value, $boolean = 'and')
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Comment onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Comment withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Comment withoutTrashed()
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 */
class Comment extends ModelBase implements CommentableInterface, \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Likeable, Commentable, Auditable;
    // Disable Laravel's mass assignment protection
    protected $guarded = [];

    protected $casts = [
        'is_approved' => 'bool'
    ];

    protected $attributes = [
        'is_approved' => true
    ];

    public function commenter()
    {
        return $this->morphTo('commenter');
    }

    public function commentable()
    {
        return $this->morphTo('commentable');
    }

}
