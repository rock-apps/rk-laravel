<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Iben\Statable\Statable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Validation\Rule;
use OwenIt\Auditing\Auditable;
use Rockapps\RkLaravel\ModelFilters\VariantFilter;
use Rockapps\RkLaravel\Traits\Favoritable;
use Rockapps\RkLaravel\Traits\HasAddresses;
use Rockapps\RkLaravel\Traits\HasCategories;
use Rockapps\RkLaravel\Traits\Rate\Rateable;
use Rockapps\RkLaravel\Transformers\VariantTransformer;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;


/**
 * Rockapps\RkLaravel\Models\Variant
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Address[] $addresses
 * @property-read int|null $addresses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Category[] $categorias
 * @property-read int|null $categorias_count
 * @property \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Favorite[] $favorites
 * @property-read int|null $favorites_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Rating[] $ratings
 * @property-read int|null $ratings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Iben\Statable\Models\StateHistory[] $stateHistory
 * @property-read int|null $state_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|Variant filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Variant newQuery()
 * @method static \Illuminate\Database\Query\Builder|Variant onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Variant paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant query()
 * @method static \Illuminate\Database\Eloquent\Builder|Variant simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant withAllCategories($categories)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant withAnyCategories($categories)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant withCategories($categories)
 * @method static \Illuminate\Database\Query\Builder|Variant withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Variant withoutAnyCategories()
 * @method static \Illuminate\Database\Eloquent\Builder|Variant withoutCategories($categories)
 * @method static \Illuminate\Database\Query\Builder|Variant withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $description
 * @property string|null $icon
 * @property string|null $color
 * @property int|null $sequence
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereSequence($value)
 * @property string $value_cast
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereValueCast($value)
 * @property string|null $image
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereImage($value)
 * @property string|null $url
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereUrl($value)
 * @property string|null $style
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereStyle($value)
 * @property string|null $icon_family
 * @method static \Illuminate\Database\Eloquent\Builder|Variant whereIconFamily($value)
 */
class Variant extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable, HasMedia
{
    use SoftDeletes, Auditable, Notifiable, Statable, Filterable, HasAddresses, HasMediaTrait, Rateable, HasCategories, Favoritable;

    public $transformer = VariantTransformer::class;

    protected $attributes = [
        'value_cast' => Parameter::TYPE_STRING
    ];

    protected $casts = [
        'id' => 'int',
        'sequence' => 'int',
    ];

    protected $dates = [
    ];

    protected $fillable = [
        'value',
        'value_cast',
        'type',
        'name',
        'description',
        'icon',
        'icon_family',
        'color',
        'sequence',
        'url',
        'style',
    ];
    protected $validationMessages = [
        'error.save' => 'Erro ao salvar a característica.',
        'error.not_found' => 'Característica não encontrada.',
    ];
    protected $rules = [
        'value' => 'required',
        'value_cast' => 'required',
        'type' => 'required',
        'name' => 'required',
        'description' => 'nullable',
        'icon' => 'nullable',
        'icon_family' => 'nullable',
        'color' => 'nullable',
        'sequence' => 'nullable|numeric',
        'image' => 'nullable',
    ];

    public function validate(array $rules = array())
    {
        $rules['value_cast'] = ['required', Rule::in(Parameter::TYPES)];

        return parent::validate($rules);
    }

    public function getCastedValue()
    {
        switch ($this->value_cast) {
            case Parameter::TYPE_FLOAT:
            case Parameter::TYPE_CURRENCY:
                return (float)$this->value;
            case Parameter::TYPE_INT:
                return (int)$this->value;
            case Parameter::TYPE_BOOL:
                return (bool)$this->value;
            case Parameter::TYPE_STRING:
            default:
                return (string)$this->value;
        }
    }

    /* Categorias em português pra não sobrescrever o metodo da Trait */
    public function categorias()
    {
        return $this->belongsToMany(Category::class, 'category_variants', 'variant_id', 'category_id');
    }

    public function modelFilter()
    {
        return $this->provideFilter(VariantFilter::class);
    }
}
