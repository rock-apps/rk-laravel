<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Iben\Statable\Statable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rockapps\RkLaravel\Events\SubscriptionCreated;
use Rockapps\RkLaravel\Events\SubscriptionRenewed;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\Iap\GoogleSubscription;
use Rockapps\RkLaravel\Helpers\Iap\IosSubscription;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeSubscription;
use Rockapps\RkLaravel\ModelFilters\SubscriptionFilter;
use Rockapps\RkLaravel\Services\SubscriptionService;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Traits\HasPayer;
use Rockapps\RkLaravel\Traits\Services\SubscriptionServiceTrait;
use Rockapps\RkLaravel\Traits\States\SubscriptionStateTrait;
use Rockapps\RkLaravel\Transformers\SubscriptionTransformer;

/**
 * Rockapps\RkLaravel\Models\Subscription
 *
 * @property int $id
 * @property string $status
 * @property string|null $comments_operator
 * @property int|null $plan_id
 * @property string $subscriber_type
 * @property int $subscriber_id
 * @property string $payer_type
 * @property int $payer_id
 * @property \Illuminate\Support\Carbon|null $subscribed_at
 * @property \Illuminate\Support\Carbon|null $expired_at
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $payer
 * @property-read \Illuminate\Database\Eloquent\Collection|Payment[] $payments
 * @property-read int|null $payments_count
 * @property-read Plan $plan
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $subscriber
 * @method static Builder|Subscription newModelQuery()
 * @method static Builder|Subscription newQuery()
 * @method static Builder|Subscription query()
 * @method static Builder|Subscription whereCommentsOperator($value)
 * @method static Builder|Subscription whereCreatedAt($value)
 * @method static Builder|Subscription whereDeletedAt($value)
 * @method static Builder|Subscription whereExpiredAt($value)
 * @method static Builder|Subscription whereId($value)
 * @method static Builder|Subscription wherePayerId($value)
 * @method static Builder|Subscription wherePayerType($value)
 * @method static Builder|Subscription wherePlanId($value)
 * @method static Builder|Subscription whereStatus($value)
 * @method static Builder|Subscription whereSubscribedAt($value)
 * @method static Builder|Subscription whereSubscriberId($value)
 * @method static Builder|Subscription whereSubscriberType($value)
 * @method static Builder|Subscription whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $mode
 * @property string $gateway
 * @property string|null $pgm_subscription_id
 * @property \Illuminate\Support\Carbon|null $current_period_start
 * @property \Illuminate\Support\Carbon|null $current_period_end
 * @method static Builder|Subscription whereCurrentPeriodEnd($value)
 * @method static Builder|Subscription whereCurrentPeriodStart($value)
 * @method static Builder|Subscription whereGateway($value)
 * @method static Builder|Subscription whereMode($value)
 * @method static Builder|Subscription wherePgmSubscriptionId($value)
 * @property string $payment_method
 * @property int|null $credit_card_id
 * @property-read CreditCard $creditCard
 * @method static Builder|Subscription whereCreditCardId($value)
 * @method static Builder|Subscription wherePaymentMethod($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Iben\Statable\Models\StateHistory[] $stateHistory
 * @property-read int|null $state_history_count
 * @method static Builder|Subscription filter($input = [], $filter = null)
 * @method static Builder|Subscription paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static Builder|Subscription simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static Builder|Subscription whereBeginsWith($column, $value, $boolean = 'and')
 * @method static Builder|Subscription whereEndsWith($column, $value, $boolean = 'and')
 * @method static Builder|Subscription whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Subscription onlyActive()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Subscription onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Subscription withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Subscription withoutTrashed()
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property string|null $iap_receipt_id
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Subscription whereIapReceiptId($value)
 * @property string|null $apple_transaction_id
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Subscription whereAppleTransactionId($value)
 * @property int|null $address_id
 * @method static Builder|Subscription whereAddressId($value)
 * @property-read \Rockapps\RkLaravel\Models\Address|null $address
 * @property \Illuminate\Support\Carbon|null $renewed_at
 * @method static Builder|Subscription whereRenewedAt($value)
 * @property int|null $product_id
 * @property-read \Rockapps\RkLaravel\Models\Product|null $product
 * @method static Builder|Subscription whereProductId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\OrderVirtualUnit[] $orderVirtualUnit
 * @property-read int|null $order_virtual_unit_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\OrderVirtualUnit[] $orderVirtualUnits
 * @property-read int|null $order_virtual_units_count
 * @property string|null $android_transaction_id
 * @method static Builder|Subscription whereAndroidTransactionId($value)
 */
class Subscription extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use HasPayer, Statable, Filterable, SoftDeletes;
    use Auditable;
    use SubscriptionStateTrait;
    use SubscriptionServiceTrait;

    const GATEWAY_PAGARME = 'PAGARME';
    const GATEWAY_APPLE = 'APPLE';
    const GATEWAY_GOOGLE = 'GOOGLE';
    const GATEWAY_MANUAL = 'MANUAL';
    const GATEWAY_BALANCE = 'BALANCE';

    const PAYMENT_METHOD_CREDIT_CARD = 'CREDIT_CARD';
    const PAYMENT_METHOD_PIX = 'PIX';
    const PAYMENT_METHOD_BOLETO = 'BOLETO';
    const PAYMENT_METHOD_FREE = 'FREE';
    const PAYMENT_METHOD_BALANCE = 'BALANCE'; // reutilização de pontos ou saldo acumulado
    const PAYMENT_METHOD_IAP = 'IAP';
    const PAYMENT_METHOD_BANK_TRANSFER = 'BANK_TRANSFER';

    const STATUS_NOT_STARTED = 'NOT_STARTED';
    const STATUS_ACTIVE_MANUAL = 'ACTIVE_MANUAL';
    const STATUS_TRIALING = 'TRIALING';
    const STATUS_PAID = 'PAID';
    const STATUS_PENDING_PAYMENT = 'PENDING_PAYMENT';
    const STATUS_UNPAID = 'UNPAID';
    const STATUS_CANCELED = 'CANCELED';
    const STATUS_ENDED = 'ENDED';

    const MODE_RECURRENT = 'RECURRENT';
    const MODE_SINGLE = 'SINGLE';
    const MODE_LIFETIME = 'LIFETIME';

    protected $transformer = SubscriptionTransformer::class;

    protected $guarded = [];
    protected $dates = [
        'subscribed_at',
        'expired_at',
        'current_period_start',
        'current_period_end',
        'renewed_at',
    ];
    protected $attributes = [
        'status' => Subscription::STATUS_NOT_STARTED,
//        'payment_method',
//        'plan_id',
//        'subscriber',
//        'subscribed_at',
//        'expired_at',
    ];
    protected $fillable = [
        'status',
        'gateway',
        'mode',
        'payment_method',
        'comments_operator',
        'plan_id',
        'address_id',
        'product_id',
        'subscriber_type',
        'subscriber_id',
        'subscribed_at',
        'expired_at',
        'renewed_at',
        'current_period_start',
        'current_period_end',
    ];
    protected $rules = [
        'status' => 'required',
        'payment_method' => 'required|in:CREDIT_CARD,BOLETO,FREE,BALANCE,IAP,PIX',
        'mode' => 'required',
        'gateway' => 'required|in:PAGARME,MANUAL,BALANCE,APPLE,GOOGLE',
        'comments_operator' => 'nullable|string',
        'plan_id' => 'required|numeric|exists:plans,id',
        'address_id' => 'nullable|numeric|exists:addresses,id',
        'subscriber_type' => 'required|string',
        'subscriber_id' => 'required|numeric',
        'subscribed_at' => 'nullable|date',
        'expired_at' => 'nullable|date',
        'current_period_start' => 'required|date',
        'current_period_end' => 'nullable|date',
        'renewed_at' => 'nullable|date',
        'product_id' => 'nullable|numeric|exists:products,id',
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar a assinatura.',
        'error.not_found' => 'Assinatura não encontrada.',
    ];

    public function __construct(array $attributes = array())
    {
        $this->setRawAttributes(array_merge($this->attributes, array(
            'current_period_start' => now()
        )), true);
        parent::__construct($attributes);
    }

    public static function boot()
    {
        parent::boot();
        self::saved(function (Subscription $subscription) {
            if (!$subscription->pgm_subscription_id && $subscription->gateway === self::GATEWAY_PAGARME) {
                PagarMeSubscription::save($subscription);
            }
        });
        self::created(function (Subscription $subscription) {
            if ($subscription->gateway === self::GATEWAY_MANUAL) {
                $subscription->apply(self::STATUS_ACTIVE_MANUAL);
                $subscription->save();
            }

            /** @var SubscriptionCreated $event */
            $event = config('rk-laravel.subscription.event_created', SubscriptionCreated::class);
            if ($event) event(new $event($subscription));
        });
    }

    public function validate($rules = [])
    {
        if ($this->gateway === self::GATEWAY_PAGARME) {
            if (!\in_array($this->payment_method, [self::PAYMENT_METHOD_CREDIT_CARD, self::PAYMENT_METHOD_BOLETO])) {
                throw new ResourceException('Este método de pagamento não é permitido para este gateway.');
            }
            if ($this->mode !== self::MODE_RECURRENT) {
                throw new ResourceException('Este modo não é permitido para este gateway.');
            }

            $address = Address::whereRelatedType($this->payer_type)
                ->whereRelatedId($this->payer_id)
                ->first();
            if ($address) {
                $this->address_id = $address->id;
            } else {
                throw new ResourceException('É necessário que o cliente tenha ao menos um endereço cadastrado');
            }

        } else if ($this->gateway === self::GATEWAY_MANUAL) {
            if ($this->payment_method !== self::PAYMENT_METHOD_FREE) {
                throw new ResourceException('Para assinaturas manuais, só é permitido o método pagamento gratuito.');
            }
        } else if ($this->gateway === self::GATEWAY_BALANCE) {
            if ($this->payment_method !== self::PAYMENT_METHOD_BALANCE) {
                throw new ResourceException('Para assinaturas internas, só é permitido o método pagamento interno (ex: saldo).');
            }
        } else if ($this->gateway === self::GATEWAY_APPLE || $this->gateway === self::GATEWAY_GOOGLE) {
            if ($this->payment_method !== self::PAYMENT_METHOD_IAP) {
                throw new ResourceException('Para assinaturas IAP, só é permitido o método pagamento IAP.');
            }
            if (!$this->iap_receipt_id) {
                throw new ResourceException('O código IAP Receipt ID é obrigatório.');
            }
        }

        if ($this->payment_method === self::PAYMENT_METHOD_CREDIT_CARD) {
            if (!$this->credit_card_id) {
                throw new ResourceException('É necessário selecionar um cartão de crédito para este método de pagamento.');
            }
        }

        if ($this->mode === self::MODE_LIFETIME) {
            $this->current_period_end = null;
        } elseif ($this->mode === self::MODE_SINGLE) {
            $rules['current_period_start'] = 'required|date';
            $rules['current_period_end'] = 'required|date';
        } elseif ($this->mode === self::MODE_RECURRENT) {
            if (!in_array($this->payment_method, [self::PAYMENT_METHOD_CREDIT_CARD, self::PAYMENT_METHOD_BOLETO, self::PAYMENT_METHOD_IAP])) {
                throw new ResourceException('Para o modo recorrente, é necessário um tipo de pagamento não gratuito.');
            }
        }

        if (!$this->payer_type) $this->payer_type = $this->subscriber_type;
        if (!$this->payer_id) $this->payer_id = $this->subscriber_id;

        return parent::validate($rules);
    }

    public function convertGateway()
    {
        if ($this->gateway === self::GATEWAY_APPLE && $this->payment_method === self::PAYMENT_METHOD_IAP) {
            return Payment::GATEWAY_METHOD_IAP_APPLE;
        }
        if ($this->gateway === self::GATEWAY_GOOGLE && $this->payment_method === self::PAYMENT_METHOD_IAP) {
            return Payment::GATEWAY_METHOD_IAP_GOOGLE;
        }

        if ($this->gateway === self::GATEWAY_PAGARME && $this->payment_method === self::PAYMENT_METHOD_CREDIT_CARD) {
            return Payment::GATEWAY_PAGARME_CC;
        }
        if ($this->gateway === self::GATEWAY_PAGARME && $this->payment_method === self::PAYMENT_METHOD_BOLETO) {
            return Payment::GATEWAY_PAGARME_BOLETO;
        }
        if ($this->gateway === self::GATEWAY_PAGARME && $this->payment_method === self::PAYMENT_METHOD_PIX) {
            return Payment::GATEWAY_PAGARME_PIX;
        }
        if ($this->gateway === self::GATEWAY_BALANCE && $this->payment_method === self::PAYMENT_METHOD_BALANCE) {
            return Payment::GATEWAY_BALANCE;
        }
        if ($this->gateway === self::GATEWAY_MANUAL && $this->payment_method === self::PAYMENT_METHOD_BALANCE) {
            return Payment::GATEWAY_BALANCE;
        }
        if ($this->gateway === self::GATEWAY_MANUAL && $this->payment_method === self::PAYMENT_METHOD_BANK_TRANSFER) {
            return Payment::GATEWAY_BANK_TRANSFER;
        }
        if ($this->gateway === self::GATEWAY_MANUAL) {
            return Payment::GATEWAY_MANUAL;
        }
        return $this->gateway;
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'subscription_id', 'id')->withTrashed();
    }

    public function orderVirtualUnits()
    {
        return $this->hasMany(OrderVirtualUnit::class, 'subscription_id', 'id')->withTrashed();
    }

    public function plan()
    {
//        return $this->hasOne(Plan::class,'id','plan_id');
        return $this->belongsTo(Plan::class)->withTrashed();
    }

    public function creditCard()
    {
        return $this->belongsTo(CreditCard::class)->withTrashed();
    }

    public function subscriber()
    {
        return $this->morphTo('subscriber');
    }

    public function address()
    {
        return $this->hasOne(Address::class, 'id', 'address_id')->withTrashed();
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id')->withTrashed();
    }

    public function isActive()
    {
        return in_array($this->status, [self::STATUS_ACTIVE_MANUAL, self::STATUS_TRIALING, self::STATUS_PAID]);
    }

    public function syncWithGateway()
    {
        if ($this->gateway === self::GATEWAY_PAGARME) {
            $subs = PagarMeSubscription::sync($this);
            $subs->checkRenewed();
            return $subs;
        } else if ($this->gateway === self::GATEWAY_APPLE) {
            $subs = IosSubscription::sync($this);
            $subs->checkRenewed();
            return $subs;
        } else if ($this->gateway === self::GATEWAY_GOOGLE) {
            $subs = GoogleSubscription::sync($this);
            $subs->checkRenewed();
            return $subs;
        }
        return $this;
    }

    public function checkRenewed()
    {
        if ($this->current_period_end->isFuture() && $this->renewed_at->addDays($this->plan->days)->isPast()) {

            $this->renewed_at = now();
            $this->save();

            /** @var SubscriptionRenewed $event */
            $event = config('rk-laravel.subscription.event_renewed', SubscriptionRenewed::class);
            if ($event) event(new $event($this));

            return true;
        }
        return false;
    }

    public function isExpired()
    {
        return $this->current_period_end && $this->current_period_end->isPast();
    }

    /**
     * Scope a query to only include active subscriptions.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOnlyActive(Builder $query)
    {
        return $query->whereIn('status', [self::STATUS_ACTIVE_MANUAL, self::STATUS_TRIALING, self::STATUS_PAID]);
    }

    public function modelFilter()
    {
        return $this->provideFilter(SubscriptionFilter::class);
    }

    protected function getGraph()
    {
        return 'subscription'; // the SM config to use
    }
}
