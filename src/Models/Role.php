<?php namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\ModelFilters\RoleFilter;
use Rockapps\RkLaravel\Traits\ModelBaseSave;
use Rockapps\RkLaravel\Transformers\RoleTransformer;
use Symfony\Component\Console\Exception\LogicException;
use Zizaco\Entrust\EntrustRole;

/**
 * Rockapps\RkLaravel\Models\Role
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Permission[] $perms
 * @property-read int|null $perms_count
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $system
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereSystem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Role paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Role simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereLike($column, $value, $boolean = 'and')
 * @property int|null $company_id
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereCompanyId($value)
 */
class Role extends EntrustRole
{
//    use ModelBaseSave;
    use Filterable;
    use ModelBaseSave;

    public $transformer = RoleTransformer::class;
    protected $table = 'roles';
    protected $casts = [
        'system' => 'boolean'
    ];
    protected $rules = [
        'name' => 'required',
    ];
    protected $fillable = [
        'name',
        'system',
        'display_name',
        'description',
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar a função.',
        'error.not_found' => 'Função não encontrada.',
    ];


    /**
     * Boot the role model
     * Attach event listener to remove the many-to-many records when trying to delete
     * Will NOT delete any records if the role model uses soft deletes.
     *
     * @return void|bool
     */
    public static function boot()
    {
        parent::boot();

        static::updating(function (Role $role) {
            if ($role->system) {
                throw new ResourceException('Esta função é protegida');
            }
        });
        static::deleting(function (Role $role) {
            if ($role->system) {
                throw new ResourceException('Esta função é protegida');
            }
            if (!method_exists(\Config::get('entrust.role'), 'bootSoftDeletes')) {
                $role->users()->sync([]);
                $role->perms()->sync([]);
            }

            return true;
        });
    }

    /**
     * @param $name
     * @return User[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public static function getUsersWithRoleName($name)
    {
        return self::whereName($name)->first()->users;
    }

    public static function permissioners($names = [])
    {
        $data = [];

        foreach ($names as $name) {

            /** @var Role $role */
            $role = Role::query()
                ->where('name', '=', $name)
                ->get()->first();

            if (!$role) {
                throw new LogicException('Permissão não encontrada: ' . $name);
            }

            foreach ($role->users as $user) {
                $item = [
                    'role_id' => $role->id,
                    'role_name' => $role->name,
                    'role_description' => $role->description,
                    'user_id' => $user->id,
                    'user_name' => $user->name,
                ];
                $data[] = $item;
            }
        }
        $data_group = collect($data);
        $data = $data_group->groupBy('user_id');

        return $data->toArray();
    }

    /**
     * @param $name
     * @return Role
     */
    public static function findOrFailByName($name)
    {
        $role = Role::whereName($name)->first();
        if (!$role) {
            throw (new \Illuminate\Database\Eloquent\ModelNotFoundException)->setModel(
                get_class(new Role()), $name
            );
        }
        return $role;
    }

    /**
     * Attach permission to current role.
     *
     * @param object|array $permission
     *
     * @return void
     */
    public function attachPermission($permission)
    {
        if (is_object($permission)) {
            $permission = $permission->getKey();
        }

        \Cache::tags(config('entrust.permission_role_table'))->flush();
        if (is_array($permission)) {
            return $this->attachPermissions($permission);
        }

        $this->perms()->attach($permission);

        \Cache::tags(config('entrust.permission_role_table'))->flush();
    }

    /**
     * Detach permission from current role.
     *
     * @param object|array $permission
     *
     * @return void
     */
    public function detachPermission($permission)
    {
        if (is_object($permission)) {
            $permission = $permission->getKey();
        }

        \Cache::tags(config('entrust.permission_role_table'))->flush();
        if (is_array($permission)) {
            return $this->detachPermissions($permission);
        }

        $this->perms()->detach($permission);
        \Cache::tags(config('entrust.permission_role_table'))->flush();
    }

    /**
     * Get the custom validation messages being used by the model.
     *
     * Não conseguimos utilizar o ValidatingTrait aqui
     *
     * @return array
     */
    public function getValidationMessages()
    {
        return isset($this->validationMessages) ? $this->validationMessages : [];
    }

    public function modelFilter()
    {
        return $this->provideFilter(RoleFilter::class);
    }
}
