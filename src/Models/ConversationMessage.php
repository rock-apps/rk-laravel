<?php

namespace Rockapps\RkLaravel\Models;


use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Musonza\Chat\Models\Message;
use Rockapps\RkLaravel\ModelFilters\ConversationMessageFilter;
use Rockapps\RkLaravel\Traits\Like\Likeable;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Interfaces\CommentableInterface;
use Rockapps\RkLaravel\Traits\Commentable;
use Rockapps\RkLaravel\Traits\ModelTransform;

/**
 * Rockapps\RkLaravel\Models\ConversationMessage
 *
 * @property int $id
 * @property string $body
 * @property int $conversation_id
 * @property int $user_id
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Musonza\Chat\Models\Conversation $conversation
 * @property-read \Rockapps\RkLaravel\Models\User $sender
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage whereConversationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConversationMessage whereUserId($value)
 * @mixin \Eloquent
 */
class ConversationMessage extends Message implements \OwenIt\Auditing\Contracts\Auditable
{
    use Filterable,Auditable, ModelTransform;

    public function modelFilter()
    {
        return $this->provideFilter(ConversationMessageFilter::class);
    }
}
