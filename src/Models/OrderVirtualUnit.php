<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Iben\Statable\Statable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Validation\Rule;
use OwenIt\Auditing\Auditable;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Interfaces\PurchaseableInterface;
use Rockapps\RkLaravel\Services\UserGetBalanceService;
use Rockapps\RkLaravel\Traits\Attributes\PurchaseableAttributesTrait;
use Rockapps\RkLaravel\Traits\HasPayment;
use Rockapps\RkLaravel\Traits\Rate\Rateable;
use Rockapps\RkLaravel\Traits\States\OrderVirtualUnitStateTrait;
use Rockapps\RkLaravel\Transformers\OrderVirtualUnitTransformer;

/**
 * Rockapps\RkLaravel\Models\OrderVirtualUnit
 *
 * @property int $id
 * @property string $status
 * @property string $mode
 * @property string $payment_gateway
 * @property string|null $description
 * @property int $product_id
 * @property int $virtual_units
 * @property int $quantity
 * @property float|null $total_value
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $owner
 * @property-read \Rockapps\RkLaravel\Models\Payment|null $payment
 * @property-read \Rockapps\RkLaravel\Models\Product $product
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Rating[] $ratings
 * @property-read int|null $ratings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Iben\Statable\Models\StateHistory[] $stateHistory
 * @property-read int|null $state_history_count
 * @property-read \Rockapps\RkLaravel\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit newQuery()
 * @method static \Illuminate\Database\Query\Builder|OrderVirtualUnit onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit wherePaymentGateway($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereTotalValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereVirtualUnits($value)
 * @method static \Illuminate\Database\Query\Builder|OrderVirtualUnit withTrashed()
 * @method static \Illuminate\Database\Query\Builder|OrderVirtualUnit withoutTrashed()
 * @mixin \Eloquent
 * @property int $company_id
 * @property-read \Rockapps\RkLaravel\Models\Company $company
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereCompanyId($value)
 * @property int|null $subscription_id
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereSubscriptionId($value)
 * @property-read \Rockapps\RkLaravel\Models\Subscription|null $subscription
 * @property int|null $voucher_id
 * @property-read \Rockapps\RkLaravel\Models\Voucher|null $voucher
 * @method static \Illuminate\Database\Eloquent\Builder|OrderVirtualUnit whereVoucherId($value)
 */
class OrderVirtualUnit extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable, PurchaseableInterface
{
    use SoftDeletes,
        Auditable,
        Notifiable,
        Statable,
        Filterable,
        OrderVirtualUnitStateTrait,
        Rateable,
        HasPayment,
        PurchaseableAttributesTrait;

    const MODE_MANUAL = 'MANUAL';
    const MODE_INVITE = 'INVITE';
    const MODE_SIGNUP = 'SIGNUP';
    const MODE_PURCHASE = 'PURCHASE';
    const MODE_SUBSCRIPTION_RENEW = 'SUBSCRIPTION_RENEW';
    const MODE_VOUCHER = 'VOUCHER';

    public $transformer = OrderVirtualUnitTransformer::class;
    public $table = 'orders_virtual_unit';

    protected $casts = [
        'user_id' => 'int',
        'total_value' => 'float',
        'quantity' => 'int',
        'product_id' => 'int',
        'company_id' => 'int',
        'subscription_id' => 'int',
        'voucher_id' => 'int',
        'virtual_units' => 'int'
    ];

    protected $fillable = [
        'status',
        'user_id',
        'description',
        'quantity',
        'product_id',
        'company_id',
        'subscription_id',
        'voucher_id',
        'total_value',
        'payment_gateway',
        'mode',
    ];

    protected $rules = [
        'payment_gateway' => 'required',
        'product_id' => 'required|exists:products,id',
        'company_id' => 'required|exists:companies,id',
        'user_id' => 'required|exists:users,id',
        'quantity' => 'required',
        'status' => 'required|in:DRAFT,PENDING_PURCHASE,CONFIRMED_PURCHASE,CANCELED,BLOCKED_PURCHASE',
        'virtual_units' => 'required|numeric',
        'subscription_id' => 'nullable|exists:subscriptions,id',
        'voucher_id' => 'nullable|exists:vouchers,id',
    ];

    protected $attributes = [
        'status' => self::STATUS_PENDING_PURCHASE,
        'mode' => self::MODE_PURCHASE,
    ];


    protected $validationMessages = [
        'error.save' => 'Erro ao salvar o pedido.',
        'error.not_found' => 'Pedido não encontrado.',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function (OrderVirtualUnit $order) {
            $order->total_value = $order->product->unit_value * $order->quantity;
            $order->virtual_units = $order->product->virtual_units_release;
        });
        self::saved(function (OrderVirtualUnit $order) {
            $service = config('rk-laravel.user.get_balance_service', UserGetBalanceService::class);
            $user = $order->user;
            $user->virtual_unit_balance = $service::run($user);
            $user->save();
        });
    }

    public function validate(array $rules = array())
    {
        if (!$this->id) {
            if ($this->mode === OrderVirtualUnit::MODE_PURCHASE) {
                if (!(\Rockapps\RkLaravel\Models\Product::findOrFail($this->product_id))->active) {
                    throw new ResourceException('O produto selecionado não está disponível.');
                }
            }
        }

        $rules['mode'] = ['required', Rule::in([
            self::MODE_MANUAL,
            self::MODE_INVITE,
            self::MODE_SIGNUP,
            self::MODE_PURCHASE,
            self::MODE_SUBSCRIPTION_RENEW,
            self::MODE_VOUCHER,
        ])];

        $this->total_value = $this->product->unit_value * $this->quantity;
        $this->virtual_units = $this->product->virtual_units_release;
        $this->company_id = $this->product->company_id;

        return parent::validate($rules);
    }

    public function user()
    {
        $model = config('rk-laravel.user.model', \Rockapps\RkLaravel\Models\User::class);
        return $this->belongsTo($model, 'user_id', 'id');
    }

    public function product()
    {
        $model = config('rk-laravel.product.model', \Rockapps\RkLaravel\Models\Product::class);
        return $this->belongsTo($model, 'product_id');
    }

    public function company()
    {
        $model = config('rk-laravel.company.model', \Rockapps\RkLaravel\Models\Company::class);
        return $this->belongsTo($model, 'company_id');
    }

    public function subscription()
    {
        $model = config('rk-laravel.subscription.model.model', \Rockapps\RkLaravel\Models\Subscription::class);
        return $this->belongsTo($model, 'subscription_id');
    }

    public function voucher()
    {
        return $this->belongsTo(Voucher::class, 'voucher_id');
    }

    public function modelFilter()
    {
        $filter = config('rk-laravel.order_virtual_unit.filter', \Rockapps\RkLaravel\ModelFilters\OrderVirtualUnitFilter::class);
        return $this->provideFilter($filter);
    }

    protected function getGraph()
    {
        return 'order'; // the SM config to use
    }
}
