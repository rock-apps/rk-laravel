<?php

namespace Rockapps\RkLaravel\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\GoogleMapsGeoDecode;
use Rockapps\RkLaravel\ModelFilters\AddressFilter;
use Rockapps\RkLaravel\Traits\Auditable;

/**
 * Rockapps\RkLaravel\Models\Address
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address query()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $lat
 * @property string|null $lng
 * @property string|null $name
 * @property boolean|null $default
 * @property string $street
 * @property string $number
 * @property string|null $complement
 * @property string $district
 * @property string $zipcode
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $related_type
 * @property int $related_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereCName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereComplement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereRelatedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereRelatedType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereZipcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Address whereName($value)
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $related
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Address onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Address withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Address withoutTrashed()
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 */
class Address extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Auditable;

    // Disable Laravel's mass assignment protection
    protected $guarded = [];
    protected $casts = [
        'default' => 'bool',
    ];
    protected $fillable = [
        'name',
        'street',
        'number',
        'complement',
        'district',
        'zipcode',
        'city',
        'state',
        'country',
        'lat',
        'lng',
    ];
    protected $rules = [
        'name' => 'sometimes',
        'street' => 'required',
        'number' => 'required',
        'complement' => 'sometimes',
        'district' => 'required',
        'zipcode' => 'required|string|max:8',
        'city' => 'required',
        'state' => 'required',
        'country' => 'required',
        'related_id' => 'required',
        'related_type' => 'required',
//        'lat' => 'required',
//        'lng' => 'required',
    ];

    public static function boot()
    {
        parent::boot();
        self::saving(function (Address $address) {
            if (config('rk-laravel.address.decode_lat_lng')) {
                $geo = new GoogleMapsGeoDecode();
                $geo->geoDecode($address->street, $address->number, $address->city, $address->state, $address->country);
                $zip = $geo->getZipcode();
                $state = $geo->getState('short_name');
                $street = $geo->getStreet();
                $city = $geo->getCity();
                $district = $geo->getDistrict();
                $country = $geo->getCountry();

                if ($zip && strlen($zip) === 5) {
                    $address->zipcode = $zip . '-000';
                } elseif ($zip && strlen($zip) > 5) {
                    $address->zipcode = $zip;
                }
                if (!$street || !$state || !$city || !$district || !$country) {
                    // Nem sempre o $zip é encontrado por isso ele foi removido da verificação
                    throw new ResourceException('Não foi possível encontrar o endereço');
                }
                if ($state) $address->state = $state;
                if ($street) $address->street = $street;
                if ($city) $address->city = $city;
                if ($district) $address->district = $district;
                if ($country) $address->country = $country;
                $address->lat = $geo->getLat();
                $address->lng = $geo->getLong();
            }
        });

        self::creating(function (Address $address) {

            $addresses_count = Address::whereRelatedType($address->related_type)
                ->whereRelatedId($address->related_id)
                ->count();

            if ($addresses_count === 0) {
                $address->default = true;
            }
            return true;
        });
    }

    public function getDisplayString()
    {
        $add = $this->street . ', ' . $this->number;
        if ($this->complement) $add .= ', ' . $this->complement;
        $add .= ', ' . $this->district . '. ' . $this->city . ' - ' . $this->state;
        return $add;
    }

    public function setDefault()
    {
        Address::whereRelatedId($this->related_id)
            ->whereRelatedType($this->related_type)
            ->update([
                'default' => false
            ]);

        $this->default = true;
        return $this->save();
    }

    public function modelFilter()
    {
        return $this->provideFilter(AddressFilter::class);
    }

    public function related()
    {
        return $this->morphTo('related');
    }

    public function setZipcodeAttribute($value)
    {
        $this->attributes['zipcode'] = preg_replace('/[^0-9]/', '', $value);
    }
}
