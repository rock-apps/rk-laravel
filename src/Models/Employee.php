<?php

namespace Rockapps\RkLaravel\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Config;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Transformers\EmployeeTransformer;
use Watson\Validating\ValidatingTrait;


/**
 * Rockapps\RkLaravel\Models\Employee
 *
 * @property int $id
 * @property string|null $position
 * @property bool $active
 * @property int|null $company_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Rockapps\RkLaravel\Models\Company|null $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Rockapps\RkLaravel\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Employee newQuery()
 * @method static \Illuminate\Database\Query\Builder|Employee onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee query()
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Employee wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employee whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Employee withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Employee withoutTrashed()
 * @mixin \Eloquent
 */
class Employee extends ModelBase
{
    use SoftDeletes;
    use Auditable;
    use ValidatingTrait;

    public $transformer = EmployeeTransformer::class;

    protected $guarded = [];

    protected $attributes = [
        'active' => true,
    ];

    protected $casts = [
        'company_id' => 'int',
        'user_id' => 'int',
        'active' => 'bool',
    ];

    protected $dates = [];

    protected $fillable = [
        'company_id',
        'user_id',
    ];

    protected $rules = [

        'company_id' => 'required|numeric|exists:companies,id',
        'user_id' => 'required|numeric|exists:users,id',

        'position' => 'nullable|string',
        'active' => 'required|boolean',
    ];

    public function company()
    {
        $model = config('rk-laravel.company.model', \Rockapps\RkLaravel\Models\Company::class);
        return $this->belongsTo($model, 'company_id', 'id');
    }

    public function user()
    {
        $model = config('rk-laravel.user.model', \Rockapps\RkLaravel\Models\User::class);
        return $this->belongsTo($model, 'user_id', 'id');
    }

    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Config::get('entrust.role'), Config::get('entrust.role_user_table'), Config::get('entrust.user_foreign_key'), Config::get('entrust.role_foreign_key'))
            ->where('company_id', $this->company_id);
    }

}
