<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use Rockapps\RkLaravel\ModelFilters\BannerFilter;
use Rockapps\RkLaravel\Transformers\BannerTransformer;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;


/**
 * Rockapps\RkLaravel\Models\Banner
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $object
 * @method static \Illuminate\Database\Eloquent\Builder|Banner filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Banner newQuery()
 * @method static \Illuminate\Database\Query\Builder|Banner onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Banner paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner query()
 * @method static \Illuminate\Database\Eloquent\Builder|Banner simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Query\Builder|Banner withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Banner withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $title
 * @property bool $active
 * @property string|null $link_url
 * @property string|null $image
 * @property string|null $object_type
 * @property int|null $object_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereLinkUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereObjectType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereUpdatedAt($value)
 * @property string|null $position
 * @method static \Illuminate\Database\Eloquent\Builder|Banner wherePosition($value)
 */
class Banner extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable, HasMedia
{
    use SoftDeletes, Auditable, Filterable, HasMediaTrait;

    public $transformer = BannerTransformer::class;

    protected $table = 'banners';
    protected $casts = [
        'object_id' => 'int',
        'active' => 'boolean',
    ];

    protected $fillable = [
        'object_type',
        'object_id',
        'title',
        'active',
        'link_url',
        'position',
    ];

    protected $rules = [
        'title' => 'nullable|string',
    ];

    protected $attributes = [
        'active' => true,
    ];

    public static function boot()
    {
        parent::boot();
    }

    public function validate(array $rules = array())
    {
        return parent::validate($rules);
    }

    public function object()
    {
        return $this->morphTo('object');
    }


    public function modelFilter()
    {
        return $this->provideFilter(config('rk-laravel.banner.filter', BannerFilter::class));
    }

}
