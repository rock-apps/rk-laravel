<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeRecipient;
use Rockapps\RkLaravel\Interfaces\BankRecipientInterface;
use Rockapps\RkLaravel\ModelFilters\BankAccountFilter;
use Rockapps\RkLaravel\Traits\Auditable;

/**
 * Rockapps\RkLaravel\Models\BankAccount
 *
 * @property int $id
 * @property string $pgm_bank_id
 * @property string $bank_code
 * @property string $agencia
 * @property string|null $agencia_dv
 * @property string $conta
 * @property string|null $conta_dv
 * @property string $document_number
 * @property string $legal_name
 * @property string $type
 * @property bool $charge_transfer_fees
 * @property string|null $transfer_interval
 * @property string|null $transfer_day
 * @property int|null $transfer_enabled
 * @property string|null $anticipatable_volume_percentage
 * @property string|null $automatic_anticipation_enabled
 * @property string|null $automatic_anticipation_type
 * @property string|null $automatic_anticipation_days
 * @property string|null $automatic_anticipation_1025_delay
 * @property string $owner_type
 * @property int $owner_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $owner
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount filter($input = [], $filter = null)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\BankAccount onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereAgencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereAgenciaDv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereAnticipatableVolumePercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereAutomaticAnticipation1025Delay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereAutomaticAnticipationDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereAutomaticAnticipationEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereAutomaticAnticipationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereBankCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereChargeTransferFees($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereConta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereContaDv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereLegalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount wherePgmBankId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount wherePgmRecipientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereTransferDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereTransferEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereTransferInterval($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankAccount whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\BankAccount withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\BankAccount withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $pix_key
 * @property string|null $gateway
 * @method static \Illuminate\Database\Eloquent\Builder|BankAccount whereGateway($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BankAccount wherePixKey($value)
 */
class BankAccount extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Auditable, Filterable;

    const GATEWAY_PGM = 'PGM';
    const GATEWAY_MANUAL = 'MANUAL';

    const TYPE_PIX = 'pix';
    const TYPE_CONTA_CORRENTE = 'conta_corrente';
    const TYPE_CONTA_POUPANCA = 'conta_poupanca';
    const TYPE_CONTA_CORRENTE_CONJUNTA = 'conta_corrente_conjunta';
    const TYPE_CONTA_POUPANCA_CONJUNTA = 'conta_poupanca_conjunta';

    const TYPES = [
        self::TYPE_PIX,
        self::TYPE_CONTA_CORRENTE,
        self::TYPE_CONTA_POUPANCA,
        self::TYPE_CONTA_CORRENTE_CONJUNTA,
        self::TYPE_CONTA_POUPANCA_CONJUNTA
    ];

    protected $attributes = [
        'gateway' => self::GATEWAY_PGM
    ];

    protected $casts = [
        'owner_id' => 'int',
        'valid' => 'bool',
        'default' => 'bool',
        'charge_transfer_fees' => 'bool',
        'bank_code' => 'int',
        'agencia' => 'int',
//        'agencia_dv' => 'int',
        'conta' => 'int',
//        'conta_dv' => 'int',
    ];

    protected $fillable = [
        'gateway',
        'pgm_bank_id',
        'bank_code',
        'agencia',
        'agencia_dv',
        'conta',
        'conta_dv',
        'document_number',
        'legal_name',
        'type',
        'owner_type',
        'owner_id',
        'pix_key',
    ];

    protected $rules = [
//        'pgm_bank_id' => 'required',
//        'bank_code' => 'required',
//        'agencia' => 'required',
//        'conta' => 'required',
//        'document_number' => 'required',
//        'legal_name' => 'required',
        'type' => 'required',
        'owner_type' => 'required',
        'owner_id' => 'required',
        'gateway' => 'required',
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar conta bancária.',
        'error.not_found' => 'Conta Bancária não encontrada.',
        'type.required' => 'O tipo de conta é obrigatório.',
    ];

    public static function bankFind($bank_code)
    {
        if (!$bank_code) return [];

        foreach (self::bankList() as $bank_item) {
            if($bank_item['value'] === (int)$bank_code) return $bank_item;
        }

        return [];
    }

    public static function bankList()
    {
        return collect([
            ['value' => 1, 'label' => 'Banco do Brasil S.A.', 'label_short' => 'BCO DO BRASIL S.A.'],
            ['value' => 3, 'label' => 'BANCO DA AMAZONIA S.A.', 'label_short' => 'BCO DA AMAZONIA S.A.'],
            ['value' => 4, 'label' => 'Banco do Nordeste do Brasil S.A.', 'label_short' => 'BCO DO NORDESTE DO BRASIL S.A.'],
            ['value' => 7, 'label' => 'BANCO NACIONAL DE DESENVOLVIMENTO ECONOMICO E SOCIAL', 'label_short' => 'BNDES'],
            ['value' => 10, 'label' => 'CREDICOAMO CREDITO RURAL COOPERATIVA', 'label_short' => 'CREDICOAMO'],
            ['value' => 11, 'label' => 'CREDIT SUISSE HEDGING-GRIFFO CORRETORA DE VALORES S.A', 'label_short' => 'C.SUISSE HEDGING-GRIFFO CV S/A'],
            ['value' => 12, 'label' => 'Banco Inbursa S.A.', 'label_short' => 'BANCO INBURSA'],
            ['value' => 14, 'label' => 'STATE STREET BRASIL S.A. ? BANCO COMERCIAL', 'label_short' => 'STATE STREET BR S.A. BCO COMERCIAL'],
            ['value' => 15, 'label' => 'UBS Brasil Corretora de Câmbio, Títulos e Valores Mobiliários S.A.', 'label_short' => 'UBS BRASIL CCTVM S.A.'],
            ['value' => 16, 'label' => 'COOPERATIVA DE CRÉDITO MÚTUO DOS DESPACHANTES DE TRÂNSITO DE SANTA CATARINA E RI', 'label_short' => 'CCM DESP TRÂNS SC E RS'],
            ['value' => 17, 'label' => 'BNY Mellon Banco S.A.', 'label_short' => 'BNY MELLON BCO S.A.'],
            ['value' => 18, 'label' => 'Banco Tricury S.A.', 'label_short' => 'BCO TRICURY S.A.'],
            ['value' => 21, 'label' => 'BANESTES S.A. BANCO DO ESTADO DO ESPIRITO SANTO', 'label_short' => 'BCO BANESTES S.A.'],
            ['value' => 24, 'label' => 'Banco Bandepe S.A.', 'label_short' => 'BCO BANDEPE S.A.'],
            ['value' => 25, 'label' => 'Banco Alfa S.A.', 'label_short' => 'BCO ALFA S.A.'],
            ['value' => 29, 'label' => 'Banco Itaú Consignado S.A.', 'label_short' => 'BANCO ITAÚ CONSIGNADO S.A.'],
            ['value' => 33, 'label' => 'BANCO SANTANDER (BRASIL) S.A.', 'label_short' => 'BCO SANTANDER (BRASIL) S.A.'],
            ['value' => 36, 'label' => 'Banco Bradesco BBI S.A.', 'label_short' => 'BCO BBI S.A.'],
            ['value' => 37, 'label' => 'Banco do Estado do Pará S.A.', 'label_short' => 'BCO DO EST. DO PA S.A.'],
            ['value' => 40, 'label' => 'Banco Cargill S.A.', 'label_short' => 'BCO CARGILL S.A.'],
            ['value' => 41, 'label' => 'Banco do Estado do Rio Grande do Sul S.A.', 'label_short' => 'BCO DO ESTADO DO RS S.A.'],
            ['value' => 47, 'label' => 'Banco do Estado de Sergipe S.A.', 'label_short' => 'BCO DO EST. DE SE S.A.'],
            ['value' => 60, 'label' => 'Confidence Corretora de Câmbio S.A.', 'label_short' => 'CONFIDENCE CC S.A.'],
            ['value' => 62, 'label' => 'Hipercard Banco Múltiplo S.A.', 'label_short' => 'HIPERCARD BM S.A.'],
            ['value' => 63, 'label' => 'Banco Bradescard S.A.', 'label_short' => 'BANCO BRADESCARD'],
            ['value' => 64, 'label' => 'GOLDMAN SACHS DO BRASIL BANCO MULTIPLO S.A.', 'label_short' => 'GOLDMAN SACHS DO BRASIL BM S.A'],
            ['value' => 65, 'label' => 'Banco AndBank (Brasil) S.A.', 'label_short' => 'BCO ANDBANK S.A.'],
            ['value' => 66, 'label' => 'BANCO MORGAN STANLEY S.A.', 'label_short' => 'BCO MORGAN STANLEY S.A.'],
            ['value' => 69, 'label' => 'Banco Crefisa S.A.', 'label_short' => 'BCO CREFISA S.A.'],
            ['value' => 70, 'label' => 'BRB - BANCO DE BRASILIA S.A.', 'label_short' => 'BRB - BCO DE BRASILIA S.A.'],
            ['value' => 74, 'label' => 'Banco J. Safra S.A.', 'label_short' => 'BCO. J.SAFRA S.A.'],
            ['value' => 75, 'label' => 'Banco ABN Amro S.A.', 'label_short' => 'BCO ABN AMRO S.A.'],
            ['value' => 76, 'label' => 'Banco KDB do Brasil S.A.', 'label_short' => 'BCO KDB BRASIL S.A.'],
            ['value' => 77, 'label' => 'Banco Inter S.A.', 'label_short' => 'BANCO INTER'],
            ['value' => 78, 'label' => 'Haitong Banco de Investimento do Brasil S.A.', 'label_short' => 'HAITONG BI DO BRASIL S.A.'],
            ['value' => 79, 'label' => 'Banco Original do Agronegócio S.A.', 'label_short' => 'BCO ORIGINAL DO AGRO S/A'],
            ['value' => 80, 'label' => 'B&T CORRETORA DE CAMBIO LTDA.', 'label_short' => 'B&T CC LTDA.'],
            ['value' => 81, 'label' => 'BancoSeguro S.A.', 'label_short' => 'BANCOSEGURO S.A.'],
            ['value' => 82, 'label' => 'BANCO TOPÁZIO S.A.', 'label_short' => 'BANCO TOPÁZIO S.A.'],
            ['value' => 83, 'label' => 'Banco da China Brasil S.A.', 'label_short' => 'BCO DA CHINA BRASIL S.A.'],
            ['value' => 84, 'label' => 'UNIPRIME NORTE DO PARANÁ - COOPERATIVA DE CRÉDITO LTDA', 'label_short' => 'UNIPRIME NORTE DO PARANÁ - CC'],
            ['value' => 85, 'label' => 'Cooperativa Central de Crédito - Ailos', 'label_short' => 'COOP CENTRAL AILOS'],
            ['value' => 88, 'label' => 'BANCO RANDON S.A.', 'label_short' => 'BANCO RANDON S.A.'],
            ['value' => 89, 'label' => 'CREDISAN COOPERATIVA DE CRÉDITO', 'label_short' => 'CREDISAN CC'],
            ['value' => 91, 'label' => 'CENTRAL DE COOPERATIVAS DE ECONOMIA E CRÉDITO MÚTUO DO ESTADO DO RIO GRANDE DO S', 'label_short' => 'CCCM UNICRED CENTRAL RS'],
            ['value' => 92, 'label' => 'BRK S.A. Crédito, Financiamento e Investimento', 'label_short' => 'BRK S.A. CFI'],
            ['value' => 93, 'label' => 'PÓLOCRED SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E À EMPRESA DE PEQUENO PORT', 'label_short' => 'POLOCRED SCMEPP LTDA.'],
            ['value' => 94, 'label' => 'Banco Finaxis S.A.', 'label_short' => 'BANCO FINAXIS'],
            ['value' => 95, 'label' => 'Travelex Banco de Câmbio S.A.', 'label_short' => 'TRAVELEX BANCO DE CÂMBIO S.A.'],
            ['value' => 96, 'label' => 'Banco B3 S.A.', 'label_short' => 'BCO B3 S.A.'],
            ['value' => 97, 'label' => 'Credisis - Central de Cooperativas de Crédito Ltda.', 'label_short' => 'CREDISIS CENTRAL DE COOPERATIVAS DE CRÉDITO LTDA.'],
            ['value' => 98, 'label' => 'Credialiança Cooperativa de Crédito Rural', 'label_short' => 'CREDIALIANÇA CCR'],
            ['value' => 99, 'label' => 'UNIPRIME CENTRAL - CENTRAL INTERESTADUAL DE COOPERATIVAS DE CREDITO LTDA.', 'label_short' => 'UNIPRIME CENTRAL CCC LTDA.'],
            ['value' => 100, 'label' => 'Planner Corretora de Valores S.A.', 'label_short' => 'PLANNER CV S.A.'],
            ['value' => 101, 'label' => 'RENASCENCA DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIÁRIOS LTDA', 'label_short' => 'RENASCENCA DTVM LTDA'],
            ['value' => 102, 'label' => 'XP INVESTIMENTOS CORRETORA DE CÂMBIO,TÍTULOS E VALORES MOBILIÁRIOS S/A', 'label_short' => 'XP INVESTIMENTOS CCTVM S/A'],
            ['value' => 104, 'label' => 'CAIXA ECONOMICA FEDERAL', 'label_short' => 'CAIXA ECONOMICA FEDERAL'],
            ['value' => 105, 'label' => 'Lecca Crédito, Financiamento e Investimento S/A', 'label_short' => 'LECCA CFI S.A.'],
            ['value' => 107, 'label' => 'Banco Bocom BBM S.A.', 'label_short' => 'BCO BOCOM BBM S.A.'],
            ['value' => 108, 'label' => 'PORTOCRED S.A. - CREDITO, FINANCIAMENTO E INVESTIMENTO', 'label_short' => 'PORTOCRED S.A. - CFI'],
            ['value' => 111, 'label' => 'OLIVEIRA TRUST DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIARIOS S.A.', 'label_short' => 'OLIVEIRA TRUST DTVM S.A.'],
            ['value' => 113, 'label' => 'Magliano S.A. Corretora de Cambio e Valores Mobiliarios', 'label_short' => 'MAGLIANO S.A. CCVM'],
            ['value' => 114, 'label' => 'Central Cooperativa de Crédito no Estado do Espírito Santo - CECOOP', 'label_short' => 'CENTRAL COOPERATIVA DE CRÉDITO NO ESTADO DO ESPÍRITO SANTO'],
            ['value' => 117, 'label' => 'ADVANCED CORRETORA DE CÂMBIO LTDA', 'label_short' => 'ADVANCED CC LTDA'],
            ['value' => 119, 'label' => 'Banco Western Union do Brasil S.A.', 'label_short' => 'BCO WESTERN UNION'],
            ['value' => 120, 'label' => 'BANCO RODOBENS S.A.', 'label_short' => 'BCO RODOBENS S.A.'],
            ['value' => 121, 'label' => 'Banco Agibank S.A.', 'label_short' => 'BCO AGIBANK S.A.'],
            ['value' => 122, 'label' => 'Banco Bradesco BERJ S.A.', 'label_short' => 'BCO BRADESCO BERJ S.A.'],
            ['value' => 124, 'label' => 'Banco Woori Bank do Brasil S.A.', 'label_short' => 'BCO WOORI BANK DO BRASIL S.A.'],
            ['value' => 125, 'label' => 'Plural S.A. Banco Múltiplo', 'label_short' => 'PLURAL BCO BM'],
            ['value' => 126, 'label' => 'BR Partners Banco de Investimento S.A.', 'label_short' => 'BR PARTNERS BI'],
            ['value' => 127, 'label' => 'Codepe Corretora de Valores e Câmbio S.A.', 'label_short' => 'CODEPE CVC S.A.'],
            ['value' => 128, 'label' => 'MS Bank S.A. Banco de Câmbio', 'label_short' => 'MS BANK S.A. BCO DE CÂMBIO'],
            ['value' => 129, 'label' => 'UBS Brasil Banco de Investimento S.A.', 'label_short' => 'UBS BRASIL BI S.A.'],
            ['value' => 130, 'label' => 'CARUANA S.A. - SOCIEDADE DE CRÉDITO, FINANCIAMENTO E INVESTIMENTO', 'label_short' => 'CARUANA SCFI'],
            ['value' => 131, 'label' => 'TULLETT PREBON BRASIL CORRETORA DE VALORES E CÂMBIO LTDA', 'label_short' => 'TULLETT PREBON BRASIL CVC LTDA'],
            ['value' => 132, 'label' => 'ICBC do Brasil Banco Múltiplo S.A.', 'label_short' => 'ICBC DO BRASIL BM S.A.'],
            ['value' => 133, 'label' => 'CONFEDERAÇÃO NACIONAL DAS COOPERATIVAS CENTRAIS DE CRÉDITO E ECONOMIA FAMILIAR E', 'label_short' => 'CRESOL CONFEDERAÇÃO'],
            ['value' => 134, 'label' => 'BGC LIQUIDEZ DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIÁRIOS LTDA', 'label_short' => 'BGC LIQUIDEZ DTVM LTDA'],
            ['value' => 136, 'label' => 'CONFEDERAÇÃO NACIONAL DAS COOPERATIVAS CENTRAIS UNICRED LTDA. - UNICRED DO BRASI', 'label_short' => 'CONF NAC COOP CENTRAIS UNICRED'],
            ['value' => 138, 'label' => 'Get Money Corretora de Câmbio S.A.', 'label_short' => 'GET MONEY CC LTDA'],
            ['value' => 139, 'label' => 'Intesa Sanpaolo Brasil S.A. - Banco Múltiplo', 'label_short' => 'INTESA SANPAOLO BRASIL S.A. BM'],
            ['value' => 140, 'label' => 'Easynvest - Título Corretora de Valores SA', 'label_short' => 'EASYNVEST - TÍTULO CV SA'],
            ['value' => 142, 'label' => 'Broker Brasil Corretora de Câmbio Ltda.', 'label_short' => 'BROKER BRASIL CC LTDA.'],
            ['value' => 143, 'label' => 'Treviso Corretora de Câmbio S.A.', 'label_short' => 'TREVISO CC S.A.'],
            ['value' => 144, 'label' => 'BEXS BANCO DE CÂMBIO S/A', 'label_short' => 'BEXS BCO DE CAMBIO S.A.'],
            ['value' => 145, 'label' => 'LEVYCAM - CORRETORA DE CAMBIO E VALORES LTDA.', 'label_short' => 'LEVYCAM CCV LTDA'],
            ['value' => 146, 'label' => 'GUITTA CORRETORA DE CAMBIO LTDA.', 'label_short' => 'GUITTA CC LTDA'],
            ['value' => 149, 'label' => 'Facta Financeira S.A. - Crédito Financiamento e Investimento', 'label_short' => 'FACTA S.A. CFI'],
            ['value' => 157, 'label' => 'ICAP do Brasil Corretora de Títulos e Valores Mobiliários Ltda.', 'label_short' => 'ICAP DO BRASIL CTVM LTDA.'],
            ['value' => 159, 'label' => 'Casa do Crédito S.A. Sociedade de Crédito ao Microempreendedor', 'label_short' => 'CASA CREDITO S.A. SCM'],
            ['value' => 163, 'label' => 'Commerzbank Brasil S.A. - Banco Múltiplo', 'label_short' => 'COMMERZBANK BRASIL S.A. - BCO MÚLTIPLO'],
            ['value' => 169, 'label' => 'BANCO OLÉ CONSIGNADO S.A.', 'label_short' => 'BCO OLÉ CONSIGNADO S.A.'],
            ['value' => 173, 'label' => 'BRL Trust Distribuidora de Títulos e Valores Mobiliários S.A.', 'label_short' => 'BRL TRUST DTVM SA'],
            ['value' => 174, 'label' => 'PEFISA S.A. - CRÉDITO, FINANCIAMENTO E INVESTIMENTO', 'label_short' => 'PEFISA S.A. - CFI'],
            ['value' => 177, 'label' => 'Guide Investimentos S.A. Corretora de Valores', 'label_short' => 'GUIDE'],
            ['value' => 180, 'label' => 'CM CAPITAL MARKETS CORRETORA DE CÂMBIO, TÍTULOS E VALORES MOBILIÁRIOS LTDA', 'label_short' => 'CM CAPITAL MARKETS CCTVM LTDA'],
            ['value' => 183, 'label' => 'SOCRED S.A. - SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E À EMPRESA DE PEQUENO P', 'label_short' => 'SOCRED SA - SCMEPP'],
            ['value' => 184, 'label' => 'Banco Itaú BBA S.A.', 'label_short' => 'BCO ITAÚ BBA S.A.'],
            ['value' => 188, 'label' => 'ATIVA INVESTIMENTOS S.A. CORRETORA DE TÍTULOS, CÂMBIO E VALORES', 'label_short' => 'ATIVA S.A. INVESTIMENTOS CCTVM'],
            ['value' => 189, 'label' => 'HS FINANCEIRA S/A CREDITO, FINANCIAMENTO E INVESTIMENTOS', 'label_short' => 'HS FINANCEIRA'],
            ['value' => 190, 'label' => 'SERVICOOP - COOPERATIVA DE CRÉDITO DOS SERVIDORES PÚBLICOS ESTADUAIS DO RIO GRAN', 'label_short' => 'SERVICOOP'],
            ['value' => 191, 'label' => 'Nova Futura Corretora de Títulos e Valores Mobiliários Ltda.', 'label_short' => 'NOVA FUTURA CTVM LTDA.'],
            ['value' => 194, 'label' => 'PARMETAL DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIÁRIOS LTDA', 'label_short' => 'PARMETAL DTVM LTDA'],
            ['value' => 196, 'label' => 'FAIR CORRETORA DE CAMBIO S.A.', 'label_short' => 'FAIR CC S.A.'],
            ['value' => 197, 'label' => 'Stone Pagamentos S.A.', 'label_short' => 'STONE PAGAMENTOS S.A.'],
            ['value' => 208, 'label' => 'Banco BTG Pactual S.A.', 'label_short' => 'BANCO BTG PACTUAL S.A.'],
            ['value' => 212, 'label' => 'Banco Original S.A.', 'label_short' => 'BANCO ORIGINAL'],
            ['value' => 213, 'label' => 'Banco Arbi S.A.', 'label_short' => 'BCO ARBI S.A.'],
            ['value' => 217, 'label' => 'Banco John Deere S.A.', 'label_short' => 'BANCO JOHN DEERE S.A.'],
            ['value' => 218, 'label' => 'Banco BS2 S.A.', 'label_short' => 'BCO BS2 S.A.'],
            ['value' => 222, 'label' => 'BANCO CRÉDIT AGRICOLE BRASIL S.A.', 'label_short' => 'BCO CRÉDIT AGRICOLE BR S.A.'],
            ['value' => 224, 'label' => 'Banco Fibra S.A.', 'label_short' => 'BCO FIBRA S.A.'],
            ['value' => 233, 'label' => 'Banco Cifra S.A.', 'label_short' => 'BANCO CIFRA'],
            ['value' => 237, 'label' => 'Banco Bradesco S.A.', 'label_short' => 'BCO BRADESCO S.A.'],
            ['value' => 241, 'label' => 'BANCO CLASSICO S.A.', 'label_short' => 'BCO CLASSICO S.A.'],
            ['value' => 243, 'label' => 'Banco Máxima S.A.', 'label_short' => 'BCO MÁXIMA S.A.'],
            ['value' => 246, 'label' => 'Banco ABC Brasil S.A.', 'label_short' => 'BCO ABC BRASIL S.A.'],
            ['value' => 249, 'label' => 'Banco Investcred Unibanco S.A.', 'label_short' => 'BANCO INVESTCRED UNIBANCO S.A.'],
            ['value' => 250, 'label' => 'BCV - BANCO DE CRÉDITO E VAREJO S.A.', 'label_short' => 'BCV'],
            ['value' => 253, 'label' => 'Bexs Corretora de Câmbio S/A', 'label_short' => 'BEXS CC S.A.'],
            ['value' => 254, 'label' => 'PARANÁ BANCO S.A.', 'label_short' => 'PARANA BCO S.A.'],
            ['value' => 259, 'label' => 'MONEYCORP BANCO DE CÂMBIO S.A.', 'label_short' => 'MONEYCORP BCO DE CÂMBIO S.A.'],
            ['value' => 260, 'label' => 'Nu Pagamentos S.A.', 'label_short' => 'NU PAGAMENTOS S.A.'],
            ['value' => 265, 'label' => 'Banco Fator S.A.', 'label_short' => 'BCO FATOR S.A.'],
            ['value' => 266, 'label' => 'BANCO CEDULA S.A.', 'label_short' => 'BCO CEDULA S.A.'],
            ['value' => 268, 'label' => 'BARI COMPANHIA HIPOTECÁRIA', 'label_short' => 'BARI CIA HIPOTECÁRIA'],
            ['value' => 269, 'label' => 'BANCO HSBC S.A.', 'label_short' => 'BCO HSBC S.A.'],
            ['value' => 270, 'label' => 'Sagitur Corretora de Câmbio Ltda.', 'label_short' => 'SAGITUR CC LTDA'],
            ['value' => 271, 'label' => 'IB Corretora de Câmbio, Títulos e Valores Mobiliários S.A.', 'label_short' => 'IB CCTVM S.A.'],
            ['value' => 272, 'label' => 'AGK CORRETORA DE CAMBIO S.A.', 'label_short' => 'AGK CC S.A.'],
            ['value' => 273, 'label' => 'Cooperativa de Crédito Rural de São Miguel do Oeste - Sulcredi/São Miguel', 'label_short' => 'CCR DE SÃO MIGUEL DO OESTE'],
            ['value' => 274, 'label' => 'MONEY PLUS SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E A EMPRESA DE PEQUENO PORT', 'label_short' => 'MONEY PLUS SCMEPP LTDA'],
            ['value' => 276, 'label' => 'Senff S.A. - Crédito, Financiamento e Investimento', 'label_short' => 'SENFF S.A. - CFI'],
            ['value' => 278, 'label' => 'Genial Investimentos Corretora de Valores Mobiliários S.A.', 'label_short' => 'GENIAL INVESTIMENTOS CVM S.A.'],
            ['value' => 279, 'label' => 'COOPERATIVA DE CREDITO RURAL DE PRIMAVERA DO LESTE', 'label_short' => 'CCR DE PRIMAVERA DO LESTE'],
            ['value' => 280, 'label' => 'Avista S.A. Crédito, Financiamento e Investimento', 'label_short' => 'AVISTA S.A. CFI'],
            ['value' => 281, 'label' => 'Cooperativa de Crédito Rural Coopavel', 'label_short' => 'CCR COOPAVEL'],
            ['value' => 283, 'label' => 'RB CAPITAL INVESTIMENTOS DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIÁRIOS LIMITADA', 'label_short' => 'RB CAPITAL INVESTIMENTOS DTVM LTDA.'],
            ['value' => 285, 'label' => 'Frente Corretora de Câmbio Ltda.', 'label_short' => 'FRENTE CC LTDA.'],
            ['value' => 286, 'label' => 'COOPERATIVA DE CRÉDITO RURAL DE OURO SULCREDI/OURO', 'label_short' => 'CCR DE OURO'],
            ['value' => 288, 'label' => 'CAROL DISTRIBUIDORA DE TITULOS E VALORES MOBILIARIOS LTDA.', 'label_short' => 'CAROL DTVM LTDA.'],
            ['value' => 289, 'label' => 'DECYSEO CORRETORA DE CAMBIO LTDA.', 'label_short' => 'DECYSEO CC LTDA.'],
            ['value' => 290, 'label' => 'Pagseguro Internet S.A.', 'label_short' => 'PAGSEGURO'],
            ['value' => 292, 'label' => 'BS2 Distribuidora de Títulos e Valores Mobiliários S.A.', 'label_short' => 'BS2 DTVM S.A.'],
            ['value' => 293, 'label' => 'Lastro RDV Distribuidora de Títulos e Valores Mobiliários Ltda.', 'label_short' => 'LASTRO RDV DTVM LTDA'],
            ['value' => 296, 'label' => 'VISION S.A. CORRETORA DE CAMBIO', 'label_short' => 'VISION S.A. CC'],
            ['value' => 298, 'label' => 'Vip\'s Corretora de Câmbio Ltda.', 'label_short' => 'VIPS CC LTDA.'],
            ['value' => 299, 'label' => 'SOROCRED CRÉDITO, FINANCIAMENTO E INVESTIMENTO S.A.', 'label_short' => 'SOROCRED CFI S.A.'],
            ['value' => 300, 'label' => 'Banco de la Nacion Argentina', 'label_short' => 'BCO LA NACION ARGENTINA'],
            ['value' => 301, 'label' => 'BPP Instituição de Pagamento S.A.', 'label_short' => 'BPP IP S.A.'],
            ['value' => 306, 'label' => 'PORTOPAR DISTRIBUIDORA DE TITULOS E VALORES MOBILIARIOS LTDA.', 'label_short' => 'PORTOPAR DTVM LTDA'],
            ['value' => 307, 'label' => 'Terra Investimentos Distribuidora de Títulos e Valores Mobiliários Ltda.', 'label_short' => 'TERRA INVESTIMENTOS DTVM'],
            ['value' => 309, 'label' => 'CAMBIONET CORRETORA DE CÂMBIO LTDA.', 'label_short' => 'CAMBIONET CC LTDA'],
            ['value' => 310, 'label' => 'VORTX DISTRIBUIDORA DE TITULOS E VALORES MOBILIARIOS LTDA.', 'label_short' => 'VORTX DTVM LTDA.'],
            ['value' => 313, 'label' => 'AMAZÔNIA CORRETORA DE CÂMBIO LTDA.', 'label_short' => 'AMAZÔNIA CC LTDA.'],
            ['value' => 315, 'label' => 'PI Distribuidora de Títulos e Valores Mobiliários S.A.', 'label_short' => 'PI DTVM S.A.'],
            ['value' => 318, 'label' => 'Banco BMG S.A.', 'label_short' => 'BCO BMG S.A.'],
            ['value' => 319, 'label' => 'OM DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIÁRIOS LTDA', 'label_short' => 'OM DTVM LTDA'],
            ['value' => 320, 'label' => 'China Construction Bank (Brasil) Banco Múltiplo S/A', 'label_short' => 'BCO CCB BRASIL S.A.'],
            ['value' => 321, 'label' => 'CREFAZ SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E A EMPRESA DE PEQUENO PORTE LT', 'label_short' => 'CREFAZ SCMEPP LTDA'],
            ['value' => 322, 'label' => 'Cooperativa de Crédito Rural de Abelardo Luz - Sulcredi/Crediluz', 'label_short' => 'CCR DE ABELARDO LUZ'],
            ['value' => 323, 'label' => 'MERCADOPAGO.COM REPRESENTACOES LTDA.', 'label_short' => 'MERCADO PAGO'],
            ['value' => 324, 'label' => 'CARTOS SOCIEDADE DE CRÉDITO DIRETO S.A.', 'label_short' => 'CARTOS SCD S.A.'],
            ['value' => 325, 'label' => 'Órama Distribuidora de Títulos e Valores Mobiliários S.A.', 'label_short' => 'ÓRAMA DTVM S.A.'],
            ['value' => 326, 'label' => 'PARATI - CREDITO, FINANCIAMENTO E INVESTIMENTO S.A.', 'label_short' => 'PARATI - CFI S.A.'],
            ['value' => 329, 'label' => 'QI Sociedade de Crédito Direto S.A.', 'label_short' => 'QI SCD S.A.'],
            ['value' => 330, 'label' => 'BANCO BARI DE INVESTIMENTOS E FINANCIAMENTOS S.A.', 'label_short' => 'BANCO BARI S.A.'],
            ['value' => 331, 'label' => 'Fram Capital Distribuidora de Títulos e Valores Mobiliários S.A.', 'label_short' => 'FRAM CAPITAL DTVM S.A.'],
            ['value' => 332, 'label' => 'Acesso Soluções de Pagamento S.A.', 'label_short' => 'ACESSO SOLUCOES PAGAMENTO SA'],
            ['value' => 335, 'label' => 'Banco Digio S.A.', 'label_short' => 'BANCO DIGIO'],
            ['value' => 336, 'label' => 'Banco C6 S.A.', 'label_short' => 'BCO C6 S.A.'],
            ['value' => 340, 'label' => 'Super Pagamentos e Administração de Meios Eletrônicos S.A.', 'label_short' => 'SUPER PAGAMENTOS E ADMINISTRACAO DE MEIOS ELETRONICOS S.A.'],
            ['value' => 341, 'label' => 'ITAÚ UNIBANCO S.A.', 'label_short' => 'ITAÚ UNIBANCO S.A.'],
            ['value' => 342, 'label' => 'Creditas Sociedade de Crédito Direto S.A.', 'label_short' => 'CREDITAS SCD'],
            ['value' => 343, 'label' => 'FFA SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E À EMPRESA DE PEQUENO PORTE LTDA.', 'label_short' => 'FFA SCMEPP LTDA.'],
            ['value' => 348, 'label' => 'Banco XP S.A.', 'label_short' => 'BCO XP S.A.'],
            ['value' => 349, 'label' => 'AL5 S.A. CRÉDITO, FINANCIAMENTO E INVESTIMENTO', 'label_short' => 'AL5 S.A. CFI'],
            ['value' => 350, 'label' => 'COOPERATIVA DE CRÉDITO RURAL DE PEQUENOS AGRICULTORES E DA REFORMA AGRÁRIA DO CE', 'label_short' => 'CREHNOR LARANJEIRAS'],
            ['value' => 352, 'label' => 'TORO CORRETORA DE TÍTULOS E VALORES MOBILIÁRIOS LTDA', 'label_short' => 'TORO CTVM LTDA'],
            ['value' => 354, 'label' => 'NECTON INVESTIMENTOS S.A. CORRETORA DE VALORES MOBILIÁRIOS E COMMODITIES', 'label_short' => 'NECTON INVESTIMENTOS S.A CVM'],
            ['value' => 355, 'label' => 'ÓTIMO SOCIEDADE DE CRÉDITO DIRETO S.A.', 'label_short' => 'ÓTIMO SCD S.A.'],
            ['value' => 359, 'label' => 'ZEMA CRÉDITO, FINANCIAMENTO E INVESTIMENTO S/A', 'label_short' => 'ZEMA CFI S/A'],
            ['value' => 360, 'label' => 'TRINUS CAPITAL DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIÁRIOS S.A.', 'label_short' => 'TRINUS CAPITAL DTVM'],
            ['value' => 362, 'label' => 'CIELO S.A.', 'label_short' => 'CIELO S.A.'],
            ['value' => 363, 'label' => 'SOCOPA SOCIEDADE CORRETORA PAULISTA S.A.', 'label_short' => 'SOCOPA SC PAULISTA S.A.'],
            ['value' => 364, 'label' => 'GERENCIANET PAGAMENTOS DO BRASIL LTDA', 'label_short' => 'GERENCIANET PAGTOS BRASIL LTDA'],
            ['value' => 365, 'label' => 'SOLIDUS S.A. CORRETORA DE CAMBIO E VALORES MOBILIARIOS', 'label_short' => 'SOLIDUS S.A. CCVM'],
            ['value' => 366, 'label' => 'BANCO SOCIETE GENERALE BRASIL S.A.', 'label_short' => 'BCO SOCIETE GENERALE BRASIL'],
            ['value' => 367, 'label' => 'VITREO DISTRIBUIDORA DE TÍTULOS E VALORES MOBILIÁRIOS S.A.', 'label_short' => 'VITREO DTVM S.A.'],
            ['value' => 368, 'label' => 'Banco CSF S.A.', 'label_short' => 'BCO CSF S.A.'],
            ['value' => 370, 'label' => 'Banco Mizuho do Brasil S.A.', 'label_short' => 'BCO MIZUHO S.A.'],
            ['value' => 371, 'label' => 'WARREN CORRETORA DE VALORES MOBILIÁRIOS E CÂMBIO LTDA.', 'label_short' => 'WARREN CVMC LTDA'],
            ['value' => 373, 'label' => 'UP.P SOCIEDADE DE EMPRÉSTIMO ENTRE PESSOAS S.A.', 'label_short' => 'UP.P SEP S.A.'],
            ['value' => 374, 'label' => 'REALIZE CRÉDITO, FINANCIAMENTO E INVESTIMENTO S.A.', 'label_short' => 'REALIZE CFI S.A.'],
            ['value' => 376, 'label' => 'BANCO J.P. MORGAN S.A.', 'label_short' => 'BCO J.P. MORGAN S.A.'],
            ['value' => 377, 'label' => 'MS SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E À EMPRESA DE PEQUENO PORTE LTDA', 'label_short' => 'MS SCMEPP LTDA'],
            ['value' => 378, 'label' => 'BBC LEASING S.A. - ARRENDAMENTO MERCANTIL', 'label_short' => 'BBC LEASING'],
            ['value' => 379, 'label' => 'COOPERFORTE - COOPERATIVA DE ECONOMIA E CRÉDITO MÚTUO DOS FUNCIONÁRIOS DE INSTIT', 'label_short' => 'CECM COOPERFORTE'],
            ['value' => 381, 'label' => 'BANCO MERCEDES-BENZ DO BRASIL S.A.', 'label_short' => 'BCO MERCEDES-BENZ S.A.'],
            ['value' => 382, 'label' => 'FIDÚCIA SOCIEDADE DE CRÉDITO AO MICROEMPREENDEDOR E À EMPRESA DE PEQUENO PORTE L', 'label_short' => 'FIDUCIA SCMEPP LTDA'],
            ['value' => 383, 'label' => 'BOLETOBANCÁRIO.COM TECNOLOGIA DE PAGAMENTOS LTDA.', 'label_short' => 'JUNO'],
            ['value' => 387, 'label' => 'Banco Toyota do Brasil S.A.', 'label_short' => 'BCO TOYOTA DO BRASIL S.A.'],
            ['value' => 389, 'label' => 'Banco Mercantil do Brasil S.A.', 'label_short' => 'BCO MERCANTIL DO BRASIL S.A.'],
            ['value' => 390, 'label' => 'BANCO GM S.A.', 'label_short' => 'BCO GM S.A.'],
            ['value' => 391, 'label' => 'COOPERATIVA DE CREDITO RURAL DE IBIAM - SULCREDI/IBIAM', 'label_short' => 'CCR DE IBIAM'],
            ['value' => 393, 'label' => 'Banco Volkswagen S.A.', 'label_short' => 'BCO VOLKSWAGEN S.A'],
            ['value' => 394, 'label' => 'Banco Bradesco Financiamentos S.A.', 'label_short' => 'BCO BRADESCO FINANC. S.A.'],
            ['value' => 396, 'label' => 'HUB PAGAMENTOS S.A', 'label_short' => 'HUB PAGAMENTOS'],
            ['value' => 399, 'label' => 'Kirton Bank S.A. - Banco Múltiplo', 'label_short' => 'KIRTON BANK'],
            ['value' => 403, 'label' => 'CORA SOCIEDADE DE CRÉDITO DIRETO S.A.', 'label_short' => 'CORA SCD S.A.'],
            ['value' => 412, 'label' => 'BANCO CAPITAL S.A.', 'label_short' => 'BCO CAPITAL S.A.'],
            ['value' => 422, 'label' => 'Banco Safra S.A.', 'label_short' => 'BCO SAFRA S.A.'],
            ['value' => 456, 'label' => 'Banco MUFG Brasil S.A.', 'label_short' => 'BCO MUFG BRASIL S.A.'],
            ['value' => 464, 'label' => 'Banco Sumitomo Mitsui Brasileiro S.A.', 'label_short' => 'BCO SUMITOMO MITSUI BRASIL S.A.'],
            ['value' => 473, 'label' => 'Banco Caixa Geral - Brasil S.A.', 'label_short' => 'BCO CAIXA GERAL BRASIL S.A.'],
            ['value' => 477, 'label' => 'Citibank N.A.', 'label_short' => 'CITIBANK N.A.'],
            ['value' => 479, 'label' => 'Banco ItauBank S.A.', 'label_short' => 'BCO ITAUBANK S.A.'],
            ['value' => 487, 'label' => 'DEUTSCHE BANK S.A. - BANCO ALEMAO', 'label_short' => 'DEUTSCHE BANK S.A.BCO ALEMAO'],
            ['value' => 488, 'label' => 'JPMorgan Chase Bank, National Association', 'label_short' => 'JPMORGAN CHASE BANK'],
            ['value' => 492, 'label' => 'ING Bank N.V.', 'label_short' => 'ING BANK N.V.'],
            ['value' => 495, 'label' => 'Banco de La Provincia de Buenos Aires', 'label_short' => 'BCO LA PROVINCIA B AIRES BCE'],
            ['value' => 505, 'label' => 'Banco Credit Suisse (Brasil) S.A.', 'label_short' => 'BCO CREDIT SUISSE S.A.'],
            ['value' => 545, 'label' => 'SENSO CORRETORA DE CAMBIO E VALORES MOBILIARIOS S.A', 'label_short' => 'SENSO CCVM S.A.'],
            ['value' => 600, 'label' => 'Banco Luso Brasileiro S.A.', 'label_short' => 'BCO LUSO BRASILEIRO S.A.'],
            ['value' => 604, 'label' => 'Banco Industrial do Brasil S.A.', 'label_short' => 'BCO INDUSTRIAL DO BRASIL S.A.'],
            ['value' => 610, 'label' => 'Banco VR S.A.', 'label_short' => 'BCO VR S.A.'],
            ['value' => 611, 'label' => 'Banco Paulista S.A.', 'label_short' => 'BCO PAULISTA S.A.'],
            ['value' => 612, 'label' => 'Banco Guanabara S.A.', 'label_short' => 'BCO GUANABARA S.A.'],
            ['value' => 613, 'label' => 'Omni Banco S.A.', 'label_short' => 'OMNI BANCO S.A.'],
            ['value' => 623, 'label' => 'Banco Pan S.A.', 'label_short' => 'BANCO PAN'],
            ['value' => 626, 'label' => 'BANCO C6 CONSIGNADO S.A.', 'label_short' => 'BCO C6 CONSIG'],
            ['value' => 630, 'label' => 'Banco Smartbank S.A.', 'label_short' => 'SMARTBANK'],
            ['value' => 633, 'label' => 'Banco Rendimento S.A.', 'label_short' => 'BCO RENDIMENTO S.A.'],
            ['value' => 634, 'label' => 'BANCO TRIANGULO S.A.', 'label_short' => 'BCO TRIANGULO S.A.'],
            ['value' => 637, 'label' => 'BANCO SOFISA S.A.', 'label_short' => 'BCO SOFISA S.A.'],
            ['value' => 643, 'label' => 'Banco Pine S.A.', 'label_short' => 'BCO PINE S.A.'],
            ['value' => 652, 'label' => 'Itaú Unibanco Holding S.A.', 'label_short' => 'ITAÚ UNIBANCO HOLDING S.A.'],
            ['value' => 653, 'label' => 'BANCO INDUSVAL S.A.', 'label_short' => 'BCO INDUSVAL S.A.'],
            ['value' => 654, 'label' => 'BANCO DIGIMAIS S.A.', 'label_short' => 'BCO DIGIMAIS S.A.'],
            ['value' => 655, 'label' => 'Banco Votorantim S.A.', 'label_short' => 'BCO VOTORANTIM S.A.'],
            ['value' => 707, 'label' => 'Banco Daycoval S.A.', 'label_short' => 'BCO DAYCOVAL S.A'],
            ['value' => 712, 'label' => 'Banco Ourinvest S.A.', 'label_short' => 'BCO OURINVEST S.A.'],
            ['value' => 739, 'label' => 'Banco Cetelem S.A.', 'label_short' => 'BCO CETELEM S.A.'],
            ['value' => 741, 'label' => 'BANCO RIBEIRAO PRETO S.A.', 'label_short' => 'BCO RIBEIRAO PRETO S.A.'],
            ['value' => 743, 'label' => 'Banco Semear S.A.', 'label_short' => 'BANCO SEMEAR'],
            ['value' => 745, 'label' => 'Banco Citibank S.A.', 'label_short' => 'BCO CITIBANK S.A.'],
            ['value' => 746, 'label' => 'Banco Modal S.A.', 'label_short' => 'BCO MODAL S.A.'],
            ['value' => 747, 'label' => 'Banco Rabobank International Brasil S.A.', 'label_short' => 'BCO RABOBANK INTL BRASIL S.A.'],
            ['value' => 748, 'label' => 'BANCO COOPERATIVO SICREDI S.A.', 'label_short' => 'BCO COOPERATIVO SICREDI S.A.'],
            ['value' => 751, 'label' => 'Scotiabank Brasil S.A. Banco Múltiplo', 'label_short' => 'SCOTIABANK BRASIL'],
            ['value' => 752, 'label' => 'Banco BNP Paribas Brasil S.A.', 'label_short' => 'BCO BNP PARIBAS BRASIL S A'],
            ['value' => 753, 'label' => 'Novo Banco Continental S.A. - Banco Múltiplo', 'label_short' => 'NOVO BCO CONTINENTAL S.A. - BM'],
            ['value' => 754, 'label' => 'Banco Sistema S.A.', 'label_short' => 'BANCO SISTEMA'],
            ['value' => 755, 'label' => 'Bank of America Merrill Lynch Banco Múltiplo S.A.', 'label_short' => 'BOFA MERRILL LYNCH BM S.A.'],
            ['value' => 756, 'label' => 'BANCO COOPERATIVO DO BRASIL S.A. - BANCOOB', 'label_short' => 'BANCOOB'],
            ['value' => 757, 'label' => 'BANCO KEB HANA DO BRASIL S.A.', 'label_short' => 'BCO KEB HANA DO BRASIL S.A.'],
        ]);
    }

    public function syncWithBankRecipient()
    {
        if (!$this->id) throw new ResourceException('A conta precisa ser criada antes de vincular no recipient');
        return PagarMeRecipient::save($this,
            $this->owner,
            config('rk-laravel.bank_transfer.pgm_receiver_transfer_enabled'),
            config('rk-laravel.bank_transfer.pgm_receiver_transfer_interval'),
            config('rk-laravel.bank_transfer.pgm_receiver_transfer_day')
        );
    }

    public function validate(array $rules = array())
    {
        $rules['type'] = ['required', Rule::in(self::TYPES)];
        $rules['gateway'] = ['required'];
        if (!$this->gateway) {
            $this->gateway = config('rk-laravel.bank_transfer.bank_account_default_gateway', self::GATEWAY_PGM);
        }
        if ($this->gateway === self::GATEWAY_PGM) {
            $rules['pgm_bank_id'] = ['required'];
        }

        if (in_array($this->type, [
            self::TYPE_CONTA_CORRENTE,
            self::TYPE_CONTA_POUPANCA,
            self::TYPE_CONTA_CORRENTE_CONJUNTA,
            self::TYPE_CONTA_POUPANCA_CONJUNTA
        ])) {
            $rules['bank_code'] = ['required'];
            $rules['agencia'] = ['required'];
            $rules['conta'] = ['required'];
            $rules['document_number'] = ['required'];
            $rules['legal_name'] = ['required'];
        } else if ($this->type === self::TYPE_PIX) {
            $rules['pix_key'] = ['required'];
        }

        return parent::validate($rules);
    }

    public function modelFilter()
    {
        return $this->provideFilter(BankAccountFilter::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo|BankRecipientInterface
     */
    public function owner()
    {
        return $this->morphTo('owner');
    }
}
