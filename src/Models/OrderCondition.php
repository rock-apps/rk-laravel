<?php

namespace Rockapps\RkLaravel\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use Rockapps\RkLaravel\Exceptions\ResourceException;

/**
 * Rockapps\RkLaravel\Models\OrderCondition
 *
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $target
 * @property int $sequence
 * @property string $value
 * @property string $percentage
 * @property int|null $order_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Rockapps\RkLaravel\Models\Order|null $order
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition newQuery()
 * @method static \Illuminate\Database\Query\Builder|OrderCondition onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition query()
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition wherePercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition whereSequence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition whereTarget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|OrderCondition withTrashed()
 * @method static \Illuminate\Database\Query\Builder|OrderCondition withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $comments
 * @method static \Illuminate\Database\Eloquent\Builder|OrderCondition whereComments($value)
 */
class OrderCondition extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Auditable;

    const TYPE_DISCOUNT = 'DISCOUNT';
    const TYPE_VOUCHER = 'VOUCHER';
    const TYPE_SHIPPING = 'SHIPPING';
    const TYPE_TAX = 'TAX';

    protected $casts = [
        'quantity' => 'int',
        'unit_value' => 'float',
        'total_value' => 'float',
        'order_id' => 'int'
    ];

    protected $fillable = [
        'comments',
        'quantity',
        'unit_value',
        'total_value',
        'product_id',
    ];

    protected $rules = [
        'comments' => 'nullable|string',
        'quantity' => 'required|numeric',
        'product_id' => 'required|numeric|exists:products,id',
        'order_id' => 'required|numeric|exists:orders,id',
    ];
    protected $attributes = [
        'quantity' => 0,
        'unit_value' => 0,
        'total_value' => 0
    ];

    public static function boot()
    {
        parent::boot();
        self::saved(function (OrderItem $item) {
            $order = $item->order;
            $order->total_value = $order->getTotalValue();
            $order->save();
        });
        self::deleted(function (OrderItem $item) {
            $order = $item->order;
            $order->total_value = $order->getTotalValue();
            $order->save();
        });

    }

    public function validate(array $rules = array())
    {
        $product = Product::withTrashed()->findOrFail($this->product_id);
        $this->unit_value = $product->unit_value;
        $this->total_value = $this->quantity * $this->unit_value;

        $order = Order::findOrFail($this->order_id);

        if ($product->company_id !== $order->company_id) {
            throw new ResourceException('Este produto não pertence a essa empresa.');
        }

        if ($order->status !== Order::STATUS_DRAFT) {
            throw new ResourceException('Não é possível adicionar itens a este pedido.');
        }

        if (!$this->id) {
            if (!$product->active || $product->deleted_at) {
                throw new ResourceException("O produto $product->name não está mais disponível.");
            }
        }

        return parent::validate($rules);
    }

    public function order()
    {
        return $this->belongsTo(\Rockapps\RkLaravel\Models\Order::class, 'order_id', 'id');
    }

    public function delete()
    {
        if($this->order->status !== Order::STATUS_DRAFT){
            throw new ResourceException('Não é mais possível excluir uma condição deste pedido');
        }
        return parent::delete();
    }
}
