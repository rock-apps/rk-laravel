<?php namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\ModelFilters\PermissionFilter;
use Rockapps\RkLaravel\Traits\ModelBaseSave;
use Rockapps\RkLaravel\Transformers\PermissionTransformer;
use Zizaco\Entrust\EntrustPermission;

/**
 * Rockapps\RkLaravel\Models\Permission
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $system
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereSystem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereLike($column, $value, $boolean = 'and')
 * @property int|null $company_id
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereCompanyId($value)
 */
class Permission extends EntrustPermission
{
    use Filterable;
    use ModelBaseSave;

    public $transformer = PermissionTransformer::class;

    protected $casts = [
        'system' => 'boolean'
    ];
    protected $rules = [
        'name' => 'required',
    ];
    protected $validationMessages = [
        'error.save' => 'Erro ao salvar a permissão.',
        'error.not_found' => 'Permissão não encontrada.'
    ];
    protected $fillable = [
        'name',
        'system',
        'display_name',
        'description',
    ];

    /**
     * Boot the permission model
     * Attach event listener to remove the many-to-many records when trying to delete
     * Will NOT delete any records if the permission model uses soft deletes.
     *
     * @return void|bool
     */
    public static function boot()
    {
        parent::boot();

        static::updating(function (Permission $permission) {
            if ($permission->system) {
                throw new ResourceException('Esta permissão é protegida');
            }
        });
        static::deleting(function (Permission $permission) {
            if ($permission->system) {
                throw new ResourceException('Esta permissão é protegida');
            }
            if (!method_exists(\Config::get('entrust.permission'), 'bootSoftDeletes')) {
                $permission->roles()->sync([]);
            }

            return true;
        });
    }

    public function modelFilter()
    {
        return $this->provideFilter(PermissionFilter::class);
    }
}
