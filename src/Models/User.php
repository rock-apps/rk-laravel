<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;
use QCod\Gamify\Gamify;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\Sentry;
use Rockapps\RkLaravel\Interfaces\BankRecipientInterface;
use Rockapps\RkLaravel\Interfaces\CommenterInterface;
use Rockapps\RkLaravel\Interfaces\FavoriterInterface;
use Rockapps\RkLaravel\Interfaces\PayerInterface;
use Rockapps\RkLaravel\ModelFilters\UserFilter;
use Rockapps\RkLaravel\Notifications\UserResetPassword;
use Rockapps\RkLaravel\Services\UserGetBalanceService;
use Rockapps\RkLaravel\Traits\Attributes\UserAttributesTrait;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Traits\CanRedeemVouchers;
use Rockapps\RkLaravel\Traits\Commenter;
use Rockapps\RkLaravel\Traits\Favoriter;
use Rockapps\RkLaravel\Traits\Geographical;
use Rockapps\RkLaravel\Traits\HasAddress;
use Rockapps\RkLaravel\Traits\HasAddresses;
use Rockapps\RkLaravel\Traits\HasBankRecipient;
use Rockapps\RkLaravel\Traits\HasConfiguration;
use Rockapps\RkLaravel\Traits\Like\CanLike;
use Rockapps\RkLaravel\Traits\Payer;
use Rockapps\RkLaravel\Traits\Rate\CanRate;
use Rockapps\RkLaravel\Traits\Vote\CanVote;
use Rockapps\RkLaravel\Transformers\UserTransformer;
use Sentry\Severity;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Watson\Validating\ValidatingTrait;
use Zizaco\Entrust\Traits\EntrustUserTrait;


/**
 * Rockapps\RkLaravel\Models\User
 *
 * @property-read Address $address
 * @property-read \Illuminate\Database\Eloquent\Collection|Address[] $addresses
 * @property-read int|null $addresses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|UserDevice[] $devices
 * @property-read int|null $devices_count
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property string $suspended
 * @property string $verified
 * @property string|null $verification_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $pgm_customer_id
 * @property int $can_pay_with_balance
 * @property int $can_pay_with_boleto
 * @property int $can_pay_with_bank
 * @property int $can_pay_with_cash
 * @property int $can_pay_with_cc
 * @method static Builder|User whereBirthDate($value)
 * @method static Builder|User whereCanPayWithBank($value)
 * @method static Builder|User whereCanPayWithBoleto($value)
 * @method static Builder|User whereCanPayWithCash($value)
 * @method static Builder|User whereCanPayWithCc($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDocument($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereGender($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereMobile($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User wherePgmCustomerId($value)
 * @method static Builder|User wherePhoto($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereSuspended($value)
 * @method static Builder|User whereTelephone($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static Builder|User whereVerificationToken($value)
 * @method static Builder|User whereVerified($value)
 * @property int|null $company_id
 * @method static Builder|User whereCompanyId($value)
 * @property int $reputation
 * @property-read \Illuminate\Database\Eloquent\Collection|\QCod\Gamify\Badge[] $badges
 * @property-read int|null $badges_count
 * @property-read Configuration $configuration
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\QCod\Gamify\Reputation[] $reputations
 * @property-read int|null $reputations_count
 * @method static Builder|User whereReputation($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|Role[] $roles
 * @property-read int|null $roles_count
 * @method static Builder|User withRole($role)
 * @property string|null $admin_comments
 * @property string|null $description
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\User ofRole($role_name)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\User whereAdminComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\User whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\User withRoles($roles)
 * @property string|null $photo
 * @property string|null $birth_date
 * @property string|null $document
 * @property string|null $gender
 * @property string|null $mobile
 * @property string|null $invitation_code
 * @property string|null $telephone
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\User filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\User paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\User simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\User whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\User whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\User whereInvitationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\User whereLike($column, $value, $boolean = 'and')
 * @property float|null $longitude
 * @property float|null $latitude
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\User whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\User whereLongitude($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\BankAccount[] $bankAccounts
 * @property-read int|null $bank_accounts_count
 * @property-read \Rockapps\RkLaravel\Models\BankRecipient|null $bankRecipient
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\BankTransfer[] $bankTransfers
 * @property-read int|null $bank_transfers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Voucher[] $vouchers
 * @property-read int|null $vouchers_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\User whereCanPayWithBalance($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\CreditCard[] $creditCards
 * @property-read int|null $credit_cards_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Payment[] $payments
 * @property-read int|null $payments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\UserSocialAccount[] $socialAccounts
 * @property-read int|null $social_accounts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\UserSocialAccount[] $linkedSocialAccounts
 * @property-read int|null $linked_social_accounts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @property string|null $two_factor_token
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Favorite[] $favorites
 * @property-read int|null $favorites_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Rating[] $likes
 * @property-read int|null $likes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Rating[] $ratings
 * @property-read int|null $ratings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Rating[] $votes
 * @property-read int|null $votes_count
 * @method static Builder|User distance($latitude, $longitude)
 * @method static Builder|User geofence($latitude, $longitude, $inner_radius, $outer_radius)
 * @method static Builder|User whereTwoFactorToken($value)
 * @property-read \Rockapps\RkLaravel\Models\Company|null $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Order[] $orders
 * @property-read int|null $orders_count
 * @property string|null $handcap
 * @property string|null $education
 * @method static Builder|User whereEducation($value)
 * @method static Builder|User whereHandcap($value)
 * @property int $is_support
 * @method static Builder|User whereIsSupport($value)
 * @property string|null $signup_token
 * @method static Builder|User whereSignupToken($value)
 * @property int $can_pay_with_pix
 * @method static Builder|User whereCanPayWithPix($value)
 * @property int|null $invited_by
 * @method static Builder|User whereInvitedBy($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\OrderVirtualUnit[] $ordersVirtualUnit
 * @property-read int|null $orders_virtual_unit_count
 * @property string|null $username
 * @property string|null $online_status
 * @property bool $show_online_status
 * @property string|null $description_internal
 * @method static Builder|User whereDescriptionInternal($value)
 * @method static Builder|User whereOnlineStatus($value)
 * @method static Builder|User whereShowOnlineStatus($value)
 * @method static Builder|User whereUsername($value)
 * @property int $virtual_unit_balance
 * @method static Builder|User whereVirtualUnitBalance($value)
 * @property \Illuminate\Support\Carbon|null $last_access_at
 * @method static Builder|User whereLastAccessAt($value)
 */
class User extends \Illuminate\Foundation\Auth\User implements \OwenIt\Auditing\Contracts\Auditable, JWTSubject, HasMedia, BankRecipientInterface, PayerInterface, CommenterInterface, FavoriterInterface
{
    use Auditable;
    use ValidatingTrait;
    use HasConfiguration;
    use EntrustUserTrait;
    use HasAddress, HasAddresses, UserAttributesTrait;
    use HasConfiguration;
    use Notifiable, Gamify;
    use Filterable;
    use HasMediaTrait;
    use HasBankRecipient;
    use CanRedeemVouchers;
    use HasApiTokens;
    use Payer;
    use Favoriter;
    use CanRate, CanVote, CanLike;
    use Commenter;
    use Geographical;

    const GENDER_MALE = 'MALE';
    const GENDER_FEMALE = 'FEMALE';

    const DEFICIENCIA_VISUAL = 'VISUAL';
    const DEFICIENCIA_AUDITIVA = 'AUDITIVA';
    const DEFICIENCIA_FISICA = 'FISICA';
    const DEFICIENCIA_INTELECTUAL = 'INTELECTUAL';

    const ESCOLARIDADE_NAO_INFORMADO_PELO_CLIENTE = 'NAO_INFORMADO_PELO_CLIENTE';
    const ESCOLARIDADE_ANALFABETO = 'ANALFABETO';
    const ESCOLARIDADE_ATE_O_5_ANO_INCOMPLETO_DO_ENSINO_FUNDAMENTAL = 'ATE_O_5_ANO_INCOMPLETO_DO_ENSINO_FUNDAMENTAL';
    const ESCOLARIDADE_ATE_5_ANO_COMPLETO_DO_ENSINO_FUNDAMENTAL = 'ATE_5_ANO_COMPLETO_DO_ENSINO_FUNDAMENTAL';
    const ESCOLARIDADE_DO_6_AO_9_ANO_DO_ENSINO_FUNDAMENTAL = 'DO_6_AO_9_ANO_DO_ENSINO_FUNDAMENTAL';
    const ESCOLARIDADE_ENSINO_FUNDAMENTAL_COMPLETO = 'ENSINO_FUNDAMENTAL_COMPLETO';
    const ESCOLARIDADE_ENSINO_MEDIO_INCOMPLETO = 'ENSINO_MEDIO_INCOMPLETO';
    const ESCOLARIDADE_ENSINO_MEDIO_COMPLETO = 'ENSINO_MEDIO_COMPLETO';
    const ESCOLARIDADE_EDUCACAO_SUPERIOR_INCOMPLETO = 'EDUCACAO_SUPERIOR_INCOMPLETO';
    const ESCOLARIDADE_EDUCACAO_SUPERIOR_COMPLETO = 'EDUCACAO_SUPERIOR_COMPLETO';
    const ESCOLARIDADE_POS_GRADUACAO_INCOMPLETO = 'POS_GRADUACAO_INCOMPLETO';
    const ESCOLARIDADE_POS_GRADUACAO_COMPLETO = 'POS_GRADUACAO_COMPLETO';
    const ESCOLARIDADE_MESTRADO_INCOMPLETO = 'MESTRADO_INCOMPLETO';
    const ESCOLARIDADE_MESTRADO_COMPLETO = 'MESTRADO_COMPLETO';
    const ESCOLARIDADE_DOUTORADO_INCOMPLETO = 'DOUTORADO_INCOMPLETO';
    const ESCOLARIDADE_DOUTORADO_COMPLETO = 'DOUTORADO_COMPLETO';
    const ESCOLARIDADE_POS_DOUTORADO_INCOMPLETO = 'POS_DOUTORADO_INCOMPLETO';
    const ESCOLARIDADE_POS_DOUTORADO_COMPLETO = 'POS_DOUTORADO_COMPLETO';

    const ONLINE_STATUS_ONLINE = 'ONLINE';
    const ONLINE_STATUS_OFFLINE = 'OFFLINE';
    const ONLINE_STATUS_INVISIBLE = 'INVISIBLE';
    const ONLINE_STATUS_AWAY = 'AWAY';
    const ONLINE_STATUS_DISABLED = 'DISABLED';

    public $fillable_admin = [
        'admin_comments',
        'can_pay_with_bank',
        'can_pay_with_balance',
        'can_pay_with_boleto',
        'can_pay_with_cash',
        'can_pay_with_cc',
        'suspended',
    ];
    public $transformer = UserTransformer::class;

    protected $casts = [
        'is_support' => 'bool',
        'verified' => 'bool',
        'suspended' => 'bool',
        'invited_by' => 'int',
        'show_online_status' => 'bool',
        'virtual_unit_balance' => 'int',
    ];

    protected $dates = [
        'last_access_at'
    ];

    protected $attributes = [
        'show_online_status' => true
    ];

    protected $fillable = [
        'birth_date',
        'document',
        'email',
        'description',
        'photo',
        'gender',
        'mobile',
        'name',
        'password',
        'telephone',
        'admin_comments',
        'can_pay_with_bank',
        'can_pay_with_boleto',
        'can_pay_with_cash',
        'can_pay_with_cc',
        'education',
        'handcap',
        'suspended',
        'description',
        'username',
        'online_status',
        'show_online_status',
        'description_internal',
    ];
    protected $guarded = [];

    protected $rules = [
        'password' => 'required',
        'name' => 'required|string|min:5',
        'email' => 'required|email',
        'document' => 'nullable|cpf_cnpj',
        'invited_by' => 'nullable|exists:users,id',
    ];
    protected $validationMessages = [
        'error.save' => 'Erro ao salvar.',
        'error.not_found' => 'Registro não encontrado.',
        'email.unique' => 'Este e-mail já foi registrado anteriormente.',
    ];

    /**
     * @return int
     * @noinspection PhpUnused
     */
    public static function generateTwoFactorToken()
    {
        return mt_rand(1000, 9999);
    }

    /**
     * Alias to eloquent many-to-many relation's attach() method.
     *
     * @param mixed $role
     */
    public function attachRole($role)
    {
        \Cache::tags(config('entrust.role_user_table'))->flush();
        if (is_object($role)) {
            $role = $role->getKey();
        }

        if (is_array($role)) {
            $role = $role['id'];
        }

        if (is_string($role)) {
            $role = Role::findOrFailByName($role)->id;
        }

        if (!$this->hasRole($role)) {
            $this->roles()->attach($role);
            \Cache::tags(config('entrust.role_user_table'))->flush();
        }
    }

    /**
     * @param array $attributes
     * @param array $states
     * @param bool $create
     * @param int $qty
     * @return self[]|self
     */
    public static function factory(array $attributes = [], int $qty = 1,array $states = [], bool $create = true)
    {
        $class = get_called_class();

        if ($create) {
            $results = factory($class, $qty)->states($states)->create($attributes);
        } else {
            $results = factory($class, $qty)->states($states)->make($attributes);
        }

        return \count($results) === 1 ? $results[0] : $results;
    }


    /**
     * Checks if the user has a role by its name.
     *
     * @param string|array $name Role name or array of role names.
     * @param bool $requireAll All roles in the array are required.
     *
     * @return bool
     */
    public function hasRole($name, $requireAll = false)
    {
        // Condifionar adicionada caso seja o objeto role
        if (is_object($name) && get_class($name) === Role::class) {
            /** @noinspection PhpUndefinedFieldInspection */
            $name = $name->name;
        }

        if (is_array($name)) {
            foreach ($name as $roleName) {
                $hasRole = $this->hasRole($roleName);

                if ($hasRole && !$requireAll) {
                    return true;
                } elseif (!$hasRole && $requireAll) {
                    return false;
                }
            }

            // If we've made it this far and $requireAll is FALSE, then NONE of the roles were found
            // If we've made it this far and $requireAll is TRUE, then ALL of the roles were found.
            // Return the value of $requireAll;
            return $requireAll;
        } else {
            foreach ($this->cachedRoles() as $role) {
                if ($role->name == $name) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Alias to eloquent many-to-many relation's detach() method.
     *
     * @param mixed $role
     */
    public function detachRole($role)
    {
        \Cache::tags(config('entrust.role_user_table'))->flush();
        if (is_object($role)) {
            $role = $role->getKey();
        }

        if (is_array($role)) {
            $role = $role['id'];
        }

        if (is_string($role)) {
            $role = Role::findOrFailByName($role)->id;
        }

        $this->roles()->detach($role);
        \Cache::tags(config('entrust.role_user_table'))->flush();
    }

    /**
     * @return float|int
     */
    public function getBalance()
    {
        $service = config('rk-laravel.user.get_balance_service', UserGetBalanceService::class);
        return $service::run($this);
    }

    public function modelFilter()
    {
        return $this->provideFilter(config('rk-laravel.user.filter', UserFilter::class));
    }

    public function devices()
    {
        return $this->hasMany(UserDevice::class, 'user_id', 'id');
    }

    public function ordersVirtualUnit()
    {
        $class = config('rk-laravel.order_virtual_unit.model', OrderVirtualUnit::class);
        return $this->hasMany($class, 'user_id', 'id');
    }

    public function linkedSocialAccounts()
    {
        return $this->hasMany(UserSocialAccount::class, 'user_id', 'id');
    }

    public function sendPasswordResetNotification($token)
    {
        $notification_class = config('rk-laravel.sign_up.user_reset_password_notification', UserResetPassword::class);
        $this->notify(new $notification_class($token));
    }

    /**
     * Automatically creates hash for the user password.
     *
     * @param string $value
     * @return void
     * @noinspection PhpUnused
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    public function save(array $options = [])
    {
        $validated = $this->validate();
        $errors = $this->getErrors();
        $parent_save = false;
        if ($validated) {
            $parent_save = parent::save($options);
        }
        if (!$validated || $errors->isNotEmpty() || $this->isInvalid() || !$parent_save) {
            if (\DB::transactionLevel()) {
                \DB::rollBack();
            }
            if (config('rk-laravel.sentry.log_validation_messages')) {
                Sentry::capture($this->validationMessages['error.save'], $this->getErrors(), Severity::DEBUG());
            }
            if (\App::environment('testing')) {
                dump($this->validationMessages['error.save'], $this->getErrors(), $options, $this->toArray());
            }
            if (\App::runningInConsole()) {
                dump($this->validationMessages['error.save'], $this->getErrors(), $options, $this->toArray());
            }
            throw new ResourceException($this->validationMessages['error.save'], $this->getErrors());
        }
        return true;
    }

    /**
     * Validate the model instance
     *
     * @param array $rules Validation rules
     * @return bool
     */
    public function validate(array $rules = array())
    {
        if (!$this->id) {
            $this->verification_token = self::generateVerificationToken();
            $this->signup_token = self::generateSignUpToken();
            $this->invitation_code = self::generateInvitationCode();
        }
        // check for overrides, then remove any empty rules
        $rules = (empty($rules)) ? $this->rules : $rules;

//        $rules['document'] = "nullable|cpf|unique:users,document,$this->id,id";
        $rules['email'] = "required|email|unique:users,email,$this->id,id";

        if (config('rk-laravel.sign_up.two_factor_enabled')) {
            $rules['mobile'] = "required|unique:users,mobile,$this->id,id";
        }

        foreach ($rules as $field => $rls) {
            if ($rls == '') {
                unset($rules[$field]);
            }
        }

        if (empty($rules)) {
            return true;
        } else {

            // perform validation
            $validator = \Validator::make($this->getAttributes(), $rules, $this->validationMessages);

            if ($validator->passes()) {
                // if the model is valid, unset old errors
//                if ($this->validationErrors === null || $this->validationErrors->count() > 0) {
//                    $this->validationErrors = new MessageBag;
//                }
                return true;
            } else {
                // otherwise set the new ones
                $this->validationErrors = $validator->messages();
                return false;
            }
        }
    }

    public static function generateVerificationToken()
    {
        return Str::random(32);
    }

    public static function generateSignUpToken()
    {
        return mt_rand(1000, 9999);
    }

    public static function generateInvitationCode()
    {
        while (true) {
            $faker = \Faker\Factory::create();
            $lex = Str::upper($faker->lexify('????'));
            $number = $faker->numerify('####');
            $code = $lex . '-' . $number;
            if (!self::whereInvitationCode($code)->first()) return $code;
        }
        return '';
    }

    /**
     * @return string
     * @noinspection PhpUnused
     */
    public function isVerified()
    {
        return $this->verified;
    }

    public function isSuspended()
    {
        return $this->suspended;
    }

    public function transform($transformer = null, $keep_data_index = false, $extra_data = [])
    {
        if (!$transformer) {
            if ($this->transformer) {
                $transformer = $this->transformer;
            } else {
                $elements = explode('\\', get_class($this));
                $transformer = '\\App\\Transformers\\' . $elements[2] . 'Transformer';
            }
        }


        if (!$transformer) {
            $transformer = $this->transformer;
        }

        $transformation = fractal($this, new $transformer($extra_data));
        $data = $transformation->toArray();
        if ($keep_data_index) {
            return $data;
        } else {
            return $data['data'];
        }
    }

    public function splitName()
    {
        $name = trim($this->name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim(preg_replace('#' . preg_quote($last_name, '#') . '#', '', $name));
        return array($first_name, $last_name);
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'responsible_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id', 'id');
    }

    /**
     * Scope a query to only include users of a given role.
     *
     * @param Builder $query
     * @param mixed $role_name
     * @return Builder
     * @noinspection PhpUnused
     */
    public function scopeOfRole($query, $role_name)
    {
        $role = Role::findOrFailByName($role_name);
        return $query->whereHas('roles', function (Builder $query) use ($role) {
            $query->where('name', '=', $role->name);
        });
    }

    /**
     *Filtering users according to their role
     *
     * @param Builder $query
     * @param $roles
     * @return Builder
     * @noinspection PhpUnused
     */
    public function scopeWithRoles($query, $roles)
    {
        return $query->whereHas('roles', function (Builder $query) use ($roles) {
            $query->whereIn('name', $roles);
        });
    }

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     * @noinspection PhpUnused
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'App.User.' . $this->id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|User|User[]
     * @noinspection PhpUnused
     */
    public function getRkUser()
    {
        return \Rockapps\RkLaravel\Models\User::findOrFail($this->id);
    }

    /**
     * @return mixed
     */
    public function getProjectUser()
    {
        $user_model = config('rk-laravel.user.model');
        if (!$user_model) throw new ResourceException('Parâmetro não configurado rk-laravel.user.model');
        return $user_model::findOrFail($this->id);
    }

    /**
     * @param User $user
     * @param $action
     * @return bool
     */
    public function checkObjectPermission($user, $action)
    {
        return $user === $this->id || $user->hasRole('admin');
    }
}
