<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Rockapps\RkLaravel\Interfaces\CommentableInterface;
use Rockapps\RkLaravel\ModelFilters\PostFilter;
use Rockapps\RkLaravel\Traits\Commentable;
use Rockapps\RkLaravel\Traits\HasCategories;
use Rockapps\RkLaravel\Traits\Like\Likeable;
use Rockapps\RkLaravel\Traits\Rate\Rateable;
use Rockapps\RkLaravel\Traits\Vote\Votable;
use Rockapps\RkLaravel\Transformers\PostTransformer;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * Rockapps\RkLaravel\Models\Post
 *
 * @property \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Rating[] $likes
 * @property-read int|null $likes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Rating[] $ratings
 * @property-read int|null $ratings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Rating[] $votes
 * @property-read int|null $votes_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Post onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post withAllCategories($categories)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post withAnyCategories($categories)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post withCategories($categories)
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Post withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post withoutAnyCategories()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post withoutCategories($categories)
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Post withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string $title
 * @property string|null $heading
 * @property string $slug
 * @property string $status
 * @property string $owner_type
 * @property int $owner_id
 * @property string $visibility
 * @property \Illuminate\Support\Carbon $posted_at
 * @property \Illuminate\Support\Carbon $approved_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereApprovedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereHeading($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post wherePostedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereVisibility($value)
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\PostBlock[] $blocks
 * @property-read int|null $blocks_count
 * @property string|null $type
 * @property string|null $content
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Post whereType($value)
 */
class Post extends ModelBase implements CommentableInterface, HasMedia
{
    use SoftDeletes, Filterable;
    use Rateable, Votable, Likeable;
    use Commentable;
    use HasMediaTrait;
    use HasCategories;

    const STATUS_DRAFT = 'DRAFT';
    const STATUS_PENNDING = 'PENDING';
    const STATUS_APPROVED = 'APPROVED';

    public $transformer = PostTransformer::class;

    protected $guarded = [];

    protected $casts = [
        'owner_id' => 'int',
    ];

    protected $dates = [
        'posted_at',
        'approved_at'
    ];

    protected $fillable = [
        'title',
        'heading',
        'slug',
        'type',
        'content',
        'status',
        'owner_id',
        'owner_type',
        'visibility',
        'posted_at',
        'approved_at',
    ];

    protected $rules = [
        'title' => 'required|string',
        'heading' => 'nullable|string',
        'slug' => 'required|string',
        'content' => 'nullable|string',
        'type' => 'nullable|string',
        'status' => 'required|string|in:DRAFT,PENDING,APPROVED',
        'owner_id' => 'required|numeric',
        'owner_type' => 'required|string',
        'visibility' => 'nullable|string',
        'approved_at' => 'nullable|date',
        'posted_at' => 'nullable|date',
    ];

    public function validate(array $rules = array())
    {
        if(!$this->slug) {
            $this->slug = Str::slug($this->title);
        }
        if(!$this->posted_at) {
            $this->posted_at = now();
        }
        if(!$this->approved_at) {
            $this->approved_at = now();
        }
        return parent::validate($rules);
    }

    public function blocks()
    {
        return $this->hasMany(PostBlock::class, 'post_id', 'id');
    }

    public function owner()
    {
        return $this->morphTo('owner');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(600)
//            ->height(232)
            ->sharpen(10);
    }
    public function modelFilter()
    {
        return $this->provideFilter(PostFilter::class);
    }
}
