<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use Rockapps\RkLaravel\ModelFilters\CampaignFilter;
use Rockapps\RkLaravel\Traits\HasCategories;
use Rockapps\RkLaravel\Transformers\CampaignTransformer;


/**
 * Rockapps\RkLaravel\Models\Campaign
 *
 * @property int $id
 * @property bool $active
 * @property string|null $comments_operator
 * @property int|null $company_id
 * @property int $priority
 * @property \Illuminate\Support\Carbon|null $start_at
 * @property \Illuminate\Support\Carbon|null $end_at
 * @property int|null $max_views
 * @property int|null $max_clicks
 * @property int|null $actual_views
 * @property int|null $actual_clicks
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\CampaignHit[] $campaignHits
 * @property-read int|null $campaign_hits_count
 * @property-read \Rockapps\RkLaravel\Models\Company|null $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\CampaignElement[] $elements
 * @property-read int|null $elements_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\CampaignHit[] $hits
 * @property-read int|null $hits_count
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign newQuery()
 * @method static \Illuminate\Database\Query\Builder|Campaign onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign query()
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereActualClicks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereActualViews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereCommentsOperator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereMaxClicks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereMaxViews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Campaign withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Campaign withoutTrashed()
 * @mixin \Eloquent
 * @property string $name
 * @property string|null $url
 * @property \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign withAllCategories($categories)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign withAnyCategories($categories)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign withCategories($categories)
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign withoutAnyCategories()
 * @method static \Illuminate\Database\Eloquent\Builder|Campaign withoutCategories($categories)
 */
class Campaign extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Auditable, Filterable, HasCategories;

    public $transformer = CampaignTransformer::class;

    protected $table = 'campaigns';

    protected $casts = [
        'active' => 'bool',
        'company_id' => 'int',
        'max_views' => 'int',
        'max_clicks' => 'int',
        'priority' => 'int',
        'actual_views' => 'int',
        'actual_clicks' => 'int',
    ];

    protected $dates = [
        'start_at',
        'end_at',
    ];

    protected $fillable = [
        'active',
        'comments_operator',
        'company_id',
        'priority',
        'start_at',
        'end_at',
        'max_views',
        'max_clicks',
        'name',
        'url',
    ];

    protected $rules = [
        'name' => 'string|required',
        'url' => 'string|url|required',
        'active' => 'bool|required',
        'company_id' => 'nullable|exists:companies,id',
        'actual_views' => 'nullable|numeric',
        'actual_clicks' => 'nullable|numeric',
        'max_views' => 'nullable|numeric',
        'max_clicks' => 'nullable|numeric',
        'priority' => 'nullable|numeric',
        'start_at' => 'nullable|date',
        'end_at' => 'nullable|date',
    ];

    protected $attributes = [
        'active' => true,
        'max_clicks' => 0,
        'max_views' => 0,
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar a campanha.',
        'error.not_found' => 'Campanha não encontrada.',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null|Campaign
     */
    public static function getRandomActive()
    {
        $campaigns = Campaign::query()
            ->where('active', true)
            ->whereHas('elements')
            ->get();

        $campaigns->filter(function(Campaign $campaign){

            return $campaign->actual_views <= $campaign->max_views || $campaign->max_views === 0;

        })->filter(function(Campaign $campaign){

            return $campaign->actual_clicks <= $campaign->max_clicks || $campaign->max_clicks === 0;

        })->filter(function(Campaign $campaign){

            if(!$campaign->start_at) return true;
            return $campaign->start_at->isPast();

        })->filter(function(Campaign $campaign){

            if(!$campaign->end_at) return true;
            return $campaign->end_at->isFuture();

        });

        return $campaigns->random(1)->first();

    }

    public function company()
    {
        return $this->belongsTo(config('rk-laravel.company.model', \Rockapps\RkLaravel\Models\Company::class), 'company_id');
    }

    public function hits()
    {
        return $this->hasMany(config('rk-laravel.campaign_hits.model', CampaignHit::class), 'campaign_id', 'id');
    }

    public function elements()
    {
        return $this->hasMany(config('rk-laravel.campaign_element.model', CampaignElement::class), 'campaign_id', 'id');
    }

    public function modelFilter()
    {
        return $this->provideFilter(config('rk-laravel.campaign.filter', CampaignFilter::class));
    }

    /**
     * @param User $user
     * @param $campaign_element
     * @param $method
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function addHit($user, $campaign_element, $method)
    {
        $this->campaignHits()->create([
            'method' => $method,
            'user_id' => $user->id,
            'element_id' => $campaign_element->id,
            'element_type' => get_class($campaign_element),
            'campaign_element_id' => $campaign_element->id,
        ]);

        # É executado o refresh para obter a última atualização de contagem
        return $this->refresh();
    }

    public function campaignHits()
    {
        return $this->hasMany(config('rk-laravel.campaign_hit.model', CampaignHit::class), 'campaign_id', 'id');
    }
}
