<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Iben\Statable\Statable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\ModelFilters\PushNotificationFilter;
use Rockapps\RkLaravel\Services\PushNotificationService;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Traits\States\PushNotificationStateTrait;

/**
 * Rockapps\RkLaravel\Models\PushNotification
 *
 * @property int $id
 * @property string $name
 * @property string $status
 * @property string $type
 * @property string $channel
 * @property string $destination
 * @property \Illuminate\Support\Carbon $scheduled_at
 * @property string $title
 * @property string|null $body
 * @property \Illuminate\Support\Carbon|null $started_at
 * @property \Illuminate\Support\Carbon|null $finished_at
 * @property string|null $object_type
 * @property int|null $object_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\PushMessage[] $pushMessages
 * @property-read int|null $push_messages_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification filter($input = [], $filter = null)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\PushNotification onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereChannel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereDestination($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereFinishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereObjectType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereScheduledAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushNotification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\PushNotification withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\PushNotification withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Iben\Statable\Models\StateHistory[] $stateHistory
 * @property-read int|null $state_history_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static Builder|PushNotification isPast()
 */
class PushNotification extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Filterable, Statable, Auditable;
    use PushNotificationStateTrait;

    const STATUS_SENDING = 'SENDING';
    const STATUS_SCHEDULED = 'SCHEDULED';
    const STATUS_FINISHED = 'FINISHED';

    const CHANNEL_PUSH = 'PUSH';

    protected $attributes = [
        'channel' => self::CHANNEL_PUSH,
        'status' => self::STATUS_SCHEDULED,
    ];

    protected $dates = [
        'scheduled_at',
        'started_at',
        'finished_at',
    ];

    protected $rules = [
        'name' => 'required|string',
        'channel' => 'required|string',
        'type' => 'required|string',
        'destination' => 'required|string',
        'scheduled_at' => 'required|date',
        'title' => 'required|string',
        'body' => 'nullable|string',
    ];

    protected $fillable = [
        'name',
        'type',
        'destination',
        'scheduled_at',
        'title',
        'body',
        'object_id',
        'object_type'
    ];

    public function validate(array $rules = array())
    {
        /** @var PushNotificationService $service */
        $service = config('rk-laravel.push_notification.service', PushNotificationService::class);

        $valid = $service::destinations()->filter(function($destination){
            return $this->destination === $destination['key'];
        });
        if(!$valid->count()) {
            throw new ResourceException('Destinatário de Push Notification inválido');
        }

        return parent::validate($rules); // TODO: Change the autogenerated stub
    }

    public function modelFilter()
    {
        return $this->provideFilter(PushNotificationFilter::class);
    }

    public function pushMessagesTotalCount()
    {
        return $this->hasMany(PushMessage::class, 'push_notification_id', 'id')->count();
    }
    public function pushMessagesTotalSuccess()
    {
        return $this->pushMessages()->where('status',PushMessage::STATUS_SUCCESS)->count();
    }
    public function pushMessagesTotalError()
    {
        return $this->pushMessages()->where('status',PushMessage::STATUS_ERROR)->count();
    }
    public function pushMessagesTotalWaiting()
    {
        return $this->pushMessages()->where('status',PushMessage::STATUS_WAITING)->count();
    }

    public function pushMessages()
    {
        return $this->hasMany(PushMessage::class, 'push_notification_id', 'id');
    }


    /**
     * Filtering past pushes
     *
     * @param Builder $query
     * @return Builder
     * @noinspection PhpUnused
     */
    public function scopeIsPast($query)
    {
        $now = now()->format('Y-m-d H:i:s');
        return $query->where('scheduled_at', '<=', $now);
    }

    protected function getGraph()
    {
        return 'push_notification'; // the SM config to use
    }
}
