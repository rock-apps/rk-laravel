<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rockapps\RkLaravel\ModelFilters\CampaignElementFilter;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Transformers\CampaignElementTransformer;


/**
 * Rockapps\RkLaravel\Models\CampaignElement
 *
 * @property int $id
 * @property int $campaign_id
 * @property string $element_type
 * @property int $element_id
 * @property int $priority
 * @property bool $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Rockapps\RkLaravel\Models\Campaign $campaign
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $element
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement newQuery()
 * @method static \Illuminate\Database\Query\Builder|CampaignElement onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement query()
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement whereCampaignId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement whereElementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement whereElementType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|CampaignElement withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CampaignElement withoutTrashed()
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|CampaignElement whereDeletedAt($value)
 */
class CampaignElement extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Auditable, Filterable;

    public $transformer = CampaignElementTransformer::class;

    protected $table = 'campaign_elements';

    protected $casts = [
        'element_id' => 'int',
        'active' => 'bool',
        'priority' => 'int',
    ];

    protected $dates = [];

    protected $fillable = [
        'campaign_id',
        'element_id',
        'element_type',
        'priority',
        'active',
    ];

    protected $rules = [
        'active' => 'bool|required',
        'campaign_id' => 'required|exists:campaigns,id',
        'element_id' => 'required|numeric',
        'element_type' => 'required|string',
        'priority' => 'nullable|numeric',
    ];

    protected $attributes = [
        'active' => true,
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar o elemento da campanha.',
        'error.not_found' => 'Elemento da campanha não encontrado.',
    ];


    public function element()
    {
        return $this->morphTo('element');
    }

    public function campaign()
    {
        return $this->belongsTo(config('rk-laravel.campaign.model.model', \Rockapps\RkLaravel\Models\Campaign::class), 'campaign_id');
    }

    public function modelFilter()
    {
        return $this->provideFilter(config('rk-laravel.campaign_element.filter', CampaignElementFilter::class));
    }
}
