<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Rockapps\RkLaravel\Exceptions\AccessModelDeniedException;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Exceptions\StoreResourceException;
use Rockapps\RkLaravel\Helpers\Sentry;
use Rockapps\RkLaravel\Traits\ModelBaseSave;
use Sentry\Severity;

/**
 * Rockapps\RkLaravel\Models\ModelBase
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereLike($column, $value, $boolean = 'and')
 */
class ModelBase extends \Illuminate\Database\Eloquent\Model
{
    use Filterable;
    use ModelBaseSave;

    const PERMISSION_READ = 'READ';
    const PERMISSION_WRITE = 'WRITE';

    protected $rules = [];
    protected $validationMessages = [
        'error.save' => 'Erro ao salvar.',
        'error.not_found' => 'Registro não encontrado.',
        'error.access_denied' => 'Você não possui permissão para acessar esse registro.',
    ];

    public static function getRulesRequest()
    {
        $self = new self();
        return \property_exists($self, 'rules') ? $self->rules : [];
    }

    /**
     * @param array $attributes
     * @param array $states
     * @param bool $create
     * @param int $qty
     * @return self[]|self
     */
    public static function factory(array $attributes = [], int $qty = 1, array $states = [], bool $create = true)
    {

        $class = get_called_class();

        if ($create) {
            $results = factory($class, $qty)->states($states)->create($attributes);
        } else {
            $results = factory($class, $qty)->states($states)->make($attributes);
        }

        return \count($results) === 1 ? $results[0] : $results;
    }

    public function save(array $options = [])
    {
        $validated = $this->validate();
        $errors = $this->getErrors();
        $parent_save = false;
        if ($validated) {
            $parent_save = parent::save($options);
        }

        if (!$validated || $errors->isNotEmpty() || $this->isInvalid() || !$parent_save) {
            if (\DB::transactionLevel()) {
                \DB::rollBack();
            }

            $msg = array_key_exists('error.save', $this->validationMessages) ? $this->validationMessages['error.save'] : 'Erro ao salvar objeto ' . get_class($this);

            if (config('rk-laravel.sentry.log_validation_messages')) {
                Sentry::capture($msg, $this->getErrors(), Severity::DEBUG());
            }
            if (\App::environment('testing')) {
                dump($msg, $this->getErrors(), $options, $this->toArray());
            }
            if (\App::runningInConsole()) {
                dump($msg, $this->getErrors(), $options, $this->toArray());
            }
            throw new StoreResourceException($msg, $this->getErrors());
        }
        return true;
    }

    /**
     * Validate the model instance
     *
     * @param array $rules Validation rules
     * @return bool
     */
    public function validate(array $rules = array())
    {
        // check for overrides, then remove any empty rules
        $rules = (empty($rules)) ? $this->rules : $rules;

        foreach ($rules as $field => $rls) {
            if ($rls == '') {
                unset($rules[$field]);
            }
        }

        if (empty($rules)) {
            return true;
        } else {

            // perform validation
            $validator = \Validator::make($this->getAttributes(), $rules, $this->validationMessages);

            if ($validator->passes()) {
                // if the model is valid, unset old errors
//                if ($this->validationErrors === null || $this->validationErrors->count() > 0) {
//                    $this->validationErrors = new MessageBag;
//                }
                return true;
            } else {
                // otherwise set the new ones
                $this->validationErrors = $validator->messages();
                return false;
            }
        }

    }

    public function canRead($user = null, $throw_exception = true)
    {
        if (!$user) $user = \Auth::getUser();
        return $this->checkPermission($user, self::PERMISSION_READ, $throw_exception);
    }

    /**
     * Utilizar essa função no método autorize das requests
     * Estender essa função para o projeto atual
     *
     * @param User|null $user
     * @param string $action
     * @param bool $throw_exception
     * @return bool
     */
    public function checkPermission($user = null, $action = self::PERMISSION_WRITE, $throw_exception = true)
    {
        // Esta função precisa ser sobrescrita no projeto.
        throw new ResourceException('A função checkPermission precisa ser rescrita conforme o projeto.');
        if (1) return true;

        $validationMessages = $this->getValidationMessages();

        $message = "Você não possui permissão para acessar esse registro";
        if (key_exists('error.access_denied', $validationMessages)) {
            $message = $validationMessages['error.not_found'];
        }
        if ($throw_exception) throw new AccessModelDeniedException($message);

        return false;
    }

    public function canWrite($user = null, $throw_exception = true)
    {
        if (!$user) $user = \Auth::getUser();
        return $this->checkPermission($user, self::PERMISSION_WRITE, $throw_exception);
    }


    /**
     * Sincroniza o relacionamento HasMany
     * Utilizar principalmente no controller.
     *
     * $campaign->syncRelationHasMany($request->get('elements'), 'elements', true);
     *
     * @param $attributes
     * @param $relation
     * @param bool $delete
     * @return mixed
     */
    public function syncRelationHasMany($attributes, $relation, $delete = true)
    {
        $c = collect($attributes);

        if ($delete) $this->$relation()->whereNotIn('id', $c->pluck('id'), 'or')->delete();

        $model = $this->$relation()->getModel();
        $this->$relation()->saveMany($c->map(function ($data) use ($model) {
            $filtered_data = [];
            foreach ($model->fillable as $key) {
                $filtered_data[$key] = $data[$key];
            }
            return $model::firstOrNew($filtered_data);
        }));
        return $this->$relation();

    }
}
