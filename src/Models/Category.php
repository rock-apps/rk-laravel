<?php

namespace Rockapps\RkLaravel\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\ModelFilters\CategoryFilter;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Transformers\CategoryTransformer;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * Rockapps\RkLaravel\Models\Category
 *
 * @property int $id
 * @property string $name
 * @property string|null $slug
 * @property string|null $description
 * @property string|null $headline
 * @property string $color_front
 * @property string $color_back
 * @property string|null $icon
 * @property int $active
 * @property int $index
 * @property string $model
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $related
 * @property-write mixed $zipcode
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Category onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereColorBack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereColorFront($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereHeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Category withoutTrashed()
 * @mixin \Eloquent
 * @property int|null $sequence
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereSequence($value)
 * @property string $model_type
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Category whereModelType($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Category[] $childCategories
 * @property-read int|null $child_categories_count
 * @property-read Category|null $parentCategory
 * @property int|null $parent_category_id
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereParentCategoryId($value)
 */
class Category extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable, HasMedia
{
    use SoftDeletes;
    use Auditable;
    use HasMediaTrait;

    public $transformer = CategoryTransformer::class;

    protected $guarded = [];
    protected $casts = [
        'active' => 'bool',
        'index' => 'integer',
        'parent_category_id' => 'integer',
    ];
    protected $fillable = [
        'name',
        'slug',
        'description',
        'headline',
        'color_front',
        'color_back',
        'icon',
        'icon_family',
        'active',
        'index',
        'model_type',
        'parent_category_id',
    ];

    protected $attributes = [
        'active' => true,
        'color_front' => '#000000',
        'color_back' => '#ffffff',
    ];

    protected $rules = [
        'name' => 'required|string',
//        'slug' => 'required|string|unique:categories',
        'description' => 'nullable|string',
        'headline' => 'nullable|string',
        'color_front' => 'nullable|string',
        'color_back' => 'nullable|string',
        'icon' => 'nullable|string',
        'icon_family' => 'nullable|string',
        'active' => 'required|boolean',
        'index' => 'nullable|numeric',
        'model_type' => 'required|string',
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar a categoria.',
        'error.not_found' => 'Categoria não encontrada.',
    ];

    public function validate(array $rules = array())
    {
        if (!$this->slug) $this->slug = Str::slug($this->name);
        $rules['slug'] = "required|unique:categories,slug,$this->id,id";

        if ($this->parent_category_id) {
            $parent_category = Category::findOrFail($this->parent_category_id);
            if ($this->model_type !== $parent_category->model_type) {
                throw new ResourceException('O tipo de modelo da categoria pai é diferente do tipo de modelo solecionado');
            }
        }

        // Verifica se a caregotia atual é filha em uma das sua categorias filha.
        // Garante o erro de recursividade
        $category = $this;
        $func = function ($childCategories) use (&$func, $category) {

            foreach ($childCategories as $childCategory) {
                if ($category->parent_category_id === $childCategory->id) throw new ResourceException('A categoria pai não pode ser uma subcategoria');
                $func($childCategory->childCategories);
            }

            return true;
        };

        $func($this->childCategories);

        return parent::validate($rules);
    }

    public function modelFilter()
    {
        return $this->provideFilter(CategoryFilter::class);
    }

    public function parentCategory()
    {
        return $this->hasOne(Category::class, 'id', 'parent_category_id');
    }

    public function childCategories()
    {
        return $this->hasMany(Category::class, 'parent_category_id', 'id');
    }

    /**
     * @param Media|null $media
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->keepOriginalImageFormat()
            ->width(600)
//            ->height(232)
            ->sharpen(10);
    }
}
