<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\Cartographic;
use Rockapps\RkLaravel\Helpers\GoogleMapsDistanceMatrix;
use Rockapps\RkLaravel\Models\ModelBase;

/**
 * Rockapps\RkLaravel\Models\DistanceMatrix
 *
 * @property int $id
 * @property string $origin_lat
 * @property string $origin_long
 * @property string $destination_lat
 * @property string $destination_long
 * @property int $distance
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix newQuery()
 * @method static \Illuminate\Database\Query\Builder|DistanceMatrix onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix query()
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix whereDestinationLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix whereDestinationLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix whereDistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix whereOriginLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix whereOriginLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|DistanceMatrix withTrashed()
 * @method static \Illuminate\Database\Query\Builder|DistanceMatrix withoutTrashed()
 * @mixin \Eloquent
 * @property string $mode
 * @method static \Illuminate\Database\Eloquent\Builder|DistanceMatrix whereMode($value)
 */
class DistanceMatrix extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Auditable;

    const CREATE_MODE_GOOGLE = 'GOOGLE';
    const CREATE_MODE_VINCENTY = 'VINCENTY';
    const CREATE_MODE_HAVERSINE = 'HAVERSINE';

    protected $table = 'distance_matrices';

    protected $casts = [
        'id' => 'int',
        'distance' => 'int',
    ];

    protected $fillable = [
        'origin_lat',
        'origin_long',
        'destination_lat',
        'destination_long',
        'distance',
        'mode',
    ];

    protected $rules = [
        'origin_lat' => 'required',
        'origin_long' => 'required',
        'destination_lat' => 'required',
        'destination_long' => 'required',
        'distance' => 'required',
        'mode' => 'required',
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar o distancia.',
        'error.not_found' => 'Distância não encontrada.',
    ];

    public static function findByLatLongOrCreate($origin_lat, $origin_long, $dest_lat, $dest_long, $mode = self::CREATE_MODE_VINCENTY)
    {
        $dm = self::findByLatLong($origin_lat, $origin_long, $dest_lat, $dest_long,$mode);

        if (!$dm) {
            $dm = new self();
            $dm->origin_lat = $origin_lat;
            $dm->origin_long = $origin_long;
            $dm->destination_lat = $dest_lat;
            $dm->destination_long = $dest_long;
            $dm->mode = $mode;
            if ($mode === self::CREATE_MODE_VINCENTY) {
                $dm->distance = Cartographic::vincentyGreatCircleDistance($origin_lat, $origin_long, $dest_lat, $dest_long) * config('rk-laravel.address.distance_matrix_street_factor', 1.6);
            } else if ($mode === self::CREATE_MODE_HAVERSINE) {
                $dm->distance = Cartographic::circleDistance($origin_lat, $origin_long, $dest_lat, $dest_long) * config('rk-laravel.address.distance_matrix_street_factor', 1.6);
            } else if ($mode === self::CREATE_MODE_GOOGLE) {
                $geo = new GoogleMapsDistanceMatrix();
                $geo->addOriginByLatLong($origin_lat, $origin_long);
                $geo->addDestinationByLatLong($dest_lat, $dest_long);
                $geo->calculate();
                $dm->distance = $geo->getDistance()[0];
            } else {
                throw new ResourceException('Método de cálculo para matriz de distância não definido');
            }
            $dm->save();
        }

        return $dm;
    }

    public static function findByLatLong($origin_lat, $origin_long, $dest_lat, $dest_long,$mode)
    {
        return self::where('origin_lat', $origin_lat)
            ->where('origin_long', $origin_long)
            ->where('destination_lat',  $dest_lat)
            ->where('destination_long', $dest_long)
            ->where('mode', $mode)
            ->first();
    }

}
