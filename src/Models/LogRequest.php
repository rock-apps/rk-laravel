<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Rockapps\RkLaravel\Services\LogRequestService;

class LogRequest extends ModelBase
{
    use Filterable;

//    public $transformer = CampaignTransformer::class;

//    protected $table = 'log_requests';

    protected $casts = [
        'user_id' => 'int',
        'port' => 'int',
        'headers' => 'array',
        'body' => 'array',
        'response_code' => 'int',
        'response_body' => 'array',
        'duration' => 'float',
        'extra' => 'array',
    ];

    protected $attributes = [
        'level' => 'DEBUG'
    ];

    protected $fillable = [
        'user_id',
        'level',
        'port',
        'url',
        'method',
        'headers',
        'querystring',
        'body',
        'response_code',
        'response_body',
        'duration',
        'extra',
        'curl',
    ];

    protected $rules = [
        'level' => 'required',
        'port' => 'required|numeric',
        'method' => 'required|string',
        'headers' => 'nullable',
        'querystring' => 'nullable|string',
        'body' => 'nullable',
        'response_code' => 'nullable|numeric',
        'response_body' => 'nullable',
        'duration' => 'nullable|numeric',
        'url' => 'string|url|required',
        'user_id' => 'nullable|exists:users,id',
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar log request.',
        'error.not_found' => 'Log Request não encontrado.',
    ];

    public static function boot()
    {
        parent::boot();
        self::saving(function (LogRequest $logRequest) {
            /** @var $service LogRequestService */
            $service = config('rk-laravel.log_request.service', LogRequestService::class);
            $logRequest->curl = $service::curl();
        });
    }

    public function user()
    {
        return $this->belongsTo(config('rk-laravel.user.model', \Rockapps\RkLaravel\Models\User::class), 'user_id');
    }
}
