<?php

namespace Rockapps\RkLaravel\Models;

use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMe;

/**
 * Rockapps\RkLaravel\Models\BankRecipient
 *
 * @property int $id
 * @property string $related_type
 * @property int $related_id
 * @property string|null $pgm_recipient_id
 * @property string|null $transfer_interval
 * @property string|null $transfer_day
 * @property int|null $transfer_enabled
 * @property string|null $anticipatable_volume_percentage
 * @property string|null $automatic_anticipation_enabled
 * @property string|null $automatic_anticipation_type
 * @property string|null $automatic_anticipation_days
 * @property string|null $automatic_anticipation_1025_delay
 * @property string|null $status
 * @property string|null $status_reason
 * @property int $bank_account_id
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Rockapps\RkLaravel\Models\BankAccount $bankAccount
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $related
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereAnticipatableVolumePercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereAutomaticAnticipation1025Delay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereAutomaticAnticipationDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereAutomaticAnticipationEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereAutomaticAnticipationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereBankAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient wherePgmRecipientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereRelatedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereRelatedType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereStatusReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereTransferDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereTransferEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereTransferInterval($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\BankRecipient whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 */
class BankRecipient extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use Auditable;

    protected $guarded = [];
    protected $casts = [
        'transfer_day' => 'int',
        'transfer_interval' => 'string',
        'transfer_enabled' => 'bool',
    ];
    protected $fillable = [
    ];
    protected $rules = [
        'bank_account_id' => 'required|exists:bank_accounts,id',
        'transfer_day' => 'nullable|numeric|max:31',
        'transfer_enabled' => 'required|boolean',
        'transfer_interval' => 'nullable|string|in:monthly,weekly,daily',
    ];

//    public static function boot()
//    {
//        parent::boot();
//        self::creating(function (Address $address) {
//
//            $addresses_count = Address::whereRelatedType($address->related_type)
//                ->whereRelatedId($address->related_id)
//                ->count();
//
//            if ($addresses_count === 0) {
//                $address->default = true;
//            }
//            return true;
//        });
//    }


    public function related()
    {
        return $this->morphTo('related');
    }

    public function bankAccount()
    {
        return $this->belongsTo(BankAccount::class, 'bank_account_id', 'id')->withTrashed();
    }

    public function getPgmBalance($attribute = 'available')
    {
        $balance = PagarMe::initApi()->recipients()->getBalance([
            'recipient_id' => $this->pgm_recipient_id,
        ]);

        if ($attribute) {
            return $balance->$attribute->amount / 100;
        }
        return $balance;
    }
}
