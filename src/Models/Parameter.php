<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\ModelFilters\ParameterFilter;
use Rockapps\RkLaravel\Traits\Auditable;

/**
 * Rockapps\RkLaravel\Models\Parameter
 *
 * @property int $id
 * @property string $key
 * @property string|null $group
 * @property string|null $value
 * @property string $type
 * @property string|null $name
 * @property string|null $description
 * @property mixed|null $value_currency
 * @property int|null $value_int
 * @property float|null $value_float
 * @property bool|null $value_bool
 * @property bool $readonly
 * @property bool $hidden
 * @property bool $archived
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Parameter onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereArchived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereHidden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereReadonly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereValueBool($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereValueCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereValueFloat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Parameter whereValueInt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Parameter withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Parameter withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereLike($column, $value, $boolean = 'and')
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 */
class Parameter extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Filterable, Auditable;

    const TYPE_FLOAT = 'FLOAT';
    const TYPE_BOOL = 'BOOL';
    const TYPE_STRING = 'STRING';
    const TYPE_INT = 'INT';
    const TYPE_CURRENCY = 'CURRENCY';
    const TYPES = [
        self::TYPE_FLOAT,
        self::TYPE_BOOL,
        self::TYPE_STRING,
        self::TYPE_INT,
        self::TYPE_CURRENCY,
    ];

    protected $casts = [
        'value_int' => 'int',
        'value_currency' => 'decimal:2',
        'value_float' => 'float',
        'value_bool' => 'bool',
        'readonly' => 'bool',
        'hidden' => 'bool',
        'archived' => 'bool'
    ];

    protected $rules = [
        'key' => 'required|string',
        'group' => 'nullable|string',
        'type' => 'required|string',
        'description' => 'nullable|string',
        'value' => 'nullable|string',
        'value_float' => 'nullable|numeric',
        'value_bool' => 'nullable|bool',
        'value_int' => 'nullable|integer',
        'value_currency' => 'nullable|numeric',
    ];

    protected $fillable = [
        'key',
        'group',
        'type',
        'description',
        'value',
        'value_float',
        'value_bool',
        'value_int',
        'value_currency',
    ];

    public static function boot()
    {
        parent::boot();
        self::saving(function (Parameter $parameter) {
            cache()->forget('parameters.key.' . $parameter->key);
            cache()->forget('parameters.all');
        });
    }

    /**
     * @param $key
     * @return Builder|Model|object|null|Parameter
     * @throws Exception
     */
    public static function getByKey($key)
    {
        $parameter = cache()->remember('parameters.key.' . $key, config('rk-laravel.cache.parameters.ttl', 60), function () use ($key) {
            return Parameter::query()
                ->where('key', $key)
                ->first();
        });

        if (!$parameter) {
            throw new ResourceException('Parâmetro não encontrado: ' . $key);
        }
        return $parameter;
    }

    public function modelFilter()
    {
        return $this->provideFilter(ParameterFilter::class);
    }
}
