<?php

namespace Rockapps\RkLaravel\Models;

use Illuminate\Database\Eloquent\Model;
use Rockapps\RkLaravel\Transformers\RatingTransformer;

/**
 * Rockapps\RkLaravel\Models\Rating
 *
 * @property int $id
 * @property string $description
 * @property string $model_type
 * @property int $model_id
 * @property string $rateable_type
 * @property int $rateable_id
 * @property float $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $model
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $rateable
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Rating newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Rating newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Rating query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Rating whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Rating whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Rating whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Rating whereModelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Rating whereModelType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Rating whereRateableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Rating whereRateableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Rating whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Rating whereValue($value)
 * @mixin \Eloquent
 */
class Rating extends Model
{
    protected $guarded = [];

    public $transformer = RatingTransformer::class;

    protected $table = 'ratings';

    public function model()
    {
        return $this->morphTo();
    }

    public function rateable()
    {
        return $this->morphTo();
    }
}
