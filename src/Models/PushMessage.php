<?php

namespace Rockapps\RkLaravel\Models;

use App\Models\IntegrationSebrae;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rockapps\RkLaravel\Notifications\PushMessageSent;

/**
 * Rockapps\RkLaravel\Models\PushMessageSent
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $sent_at
 * @property int $push_notification_id
 * @property int $user_device_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Rockapps\RkLaravel\Models\UserDevice $device
 * @property-read \Rockapps\RkLaravel\Models\PushNotification $pushNotification
 * @property-read \Rockapps\RkLaravel\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage filter($input = [], $filter = null)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\PushMessage onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage wherePushNotificationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage whereSentAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage whereUserDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\PushMessage withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\PushMessage withoutTrashed()
 * @mixin \Eloquent
 * @property string $status
 * @property string|null $data
 * @property string|null $return
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage whereReturn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage whereStatus($value)
 * @property string|null $response
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PushMessage whereResponse($value)
 */
class PushMessage extends ModelBase
{
    use SoftDeletes, Filterable;

    const STATUS_WAITING = 'WAITING';
    const STATUS_ERROR = 'ERROR';
    const STATUS_SUCCESS = 'SUCCESS';

    protected $attributes = [
        'status' => self::STATUS_WAITING
    ];

    protected $dates = [
        'sent_at',
    ];

    protected $rules = [
        'push_notification_id' => 'required|int',
        'user_device_id' => 'required|int',
        'user_id' => 'required|int',
        'data' => 'nullable|string',
    ];

    protected $fillable = [
        'push_notification_id',
        'user_device_id',
        'user_id',
        'data',
        'status',
        'sent_at',
    ];

    public static function boot()
    {
        parent::saved(function (PushMessage $pushMessage) {
            $remainingMessages = PushMessage::wherePushNotificationId($pushMessage->push_notification_id)
                ->whereStatus(PushMessage::STATUS_WAITING)
                ->count();

            if ($remainingMessages === 0) {

                $pushNotification = $pushMessage->pushNotification;
                if ($pushNotification->canApply(PushNotification::STATUS_FINISHED)) {
                    $pushNotification->apply(PushNotification::STATUS_FINISHED);
                    $pushNotification->finished_at = now();
                    $pushNotification->save();
                }
            }
        });

        parent::boot();
    }


    public function send()
    {
        if ($this->isSent()) return true;
        $this->user->notify(new PushMessageSent($this));
        return true;
    }

    public function isSent()
    {
        return (bool)$this->sent_at;
    }

    public function pushNotification()
    {
        return $this->hasOne(PushNotification::class, 'id', 'push_notification_id')->withTrashed();
    }

    public function user()
    {
        return $this->hasOne(config('rk-laravel.user.model', User::class), 'id', 'user_id');
    }

    public function device()
    {
        return $this->hasOne(UserDevice::class, 'id', 'user_device_id')->withTrashed();
    }
}
