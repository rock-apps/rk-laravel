<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;
use Rockapps\RkLaravel\ModelFilters\CustomAttributesFilter;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Transformers\CustomAttributesTransformer;


/**
 * Rockapps\RkLaravel\Models\CustomAttribute
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $value
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $object
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute newQuery()
 * @method static \Illuminate\Database\Query\Builder|CustomAttribute onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Query\Builder|CustomAttribute withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CustomAttribute withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $object_type
 * @property int|null $object_id
 * @property string $field
 * @property string $cast
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute whereCast($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute whereField($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute whereObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute whereObjectType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomAttribute whereValue($value)
 */
class CustomAttribute extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Auditable, Filterable;

    const CAST_INT = 'INT';
    const CAST_FLOAT = 'FLOAT';
    const CAST_STRING = 'STRING';
    const CAST_BOOL = 'BOOL';
    const CASTS = [
        self::CAST_BOOL,
        self::CAST_FLOAT,
        self::CAST_INT,
        self::CAST_STRING,
    ];
    public $transformer = CustomAttributesTransformer::class;

    protected $attributes = [
//        'active' => false,
    ];

    protected $casts = [
        'object_type' => 'string',
        'object_id' => 'int',
        'field' => 'string',
        'cast' => 'string',
        'value' => 'string',
    ];

    protected $fillable = [
        'object_type',
        'object_id',
        'field',
        'cast',
        'value',
    ];

    protected $rules = [
        'object_type' => 'required',
        'object_id' => 'required',
        'field' => 'required',
        'cast' => 'required',
        'value' => 'required',
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar atributo.',
        'error.not_found' => 'Atributo não encontrado.',
        'field.required' => 'O campo (coluna) é obrigatório.',
        'cast.required' => 'A conversão (cast) é obrigatório.',
        'value.required' => 'O valor é obrigatório.',
    ];

    public function getValueAttribute($value)
    {
        switch ($this->cast) {
            case self::CAST_STRING:
                return (string)$value;
            case self::CAST_FLOAT:
                return (float)$value;
            case self::CAST_BOOL:
                return (bool)$value;
            case self::CAST_INT:
                return (int)$value;
        }
        return $value;
    }

    public function validate(array $rules = array())
    {
        $rules['cast'] = ['required', Rule::in(self::CASTS)];

        return parent::validate($rules);
    }

    public function modelFilter()
    {
        return $this->provideFilter(CustomAttributesFilter::class);
    }

    public function object()
    {
        return $this->morphTo('object');
    }

}
