<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMePlan;
use Rockapps\RkLaravel\ModelFilters\PlanFilter;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Transformers\PlanTransformer;

/**
 * Rockapps\RkLaravel\Models\Plan
 *
 * @property-read Address $address
 * @property-read \Illuminate\Database\Eloquent\Collection|Address[] $addresses
 * @property-read int|null $addresses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|UserDevice[] $devices
 * @property-read int|null $devices_count
 * @property-write mixed $birth_date
 * @property-write mixed $document
 * @property-write mixed $gender
 * @property-write mixed $mobile
 * @property-write mixed $photo
 * @property-write mixed $telephone
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $headline
 * @property string|null $remember_token
 * @property string $suspended
 * @property string $verified
 * @property string|null $verification_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $pgm_customer_id
 * @property int $can_pay_with_boleto
 * @property int $can_pay_with_bank
 * @property int $can_pay_with_cash
 * @property int $can_pay_with_cc
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCanPayWithBank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCanPayWithBoleto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCanPayWithCash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCanPayWithCc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePgmCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSuspended($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereVerificationToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereVerified($value)
 * @property float $value
 * @property string $gateway
 * @property int $days
 * @property int $trial_days
 * @property string|null $payment_method
 * @property string $color
 * @property string|null $charges
 * @property string $installments
 * @property string|null $pgm_plan_id
 * @property int $active
 * @property int $invoice_reminder
 * @property string|null $comments_operator
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Subscription[] $subscriptions
 * @property-read int|null $subscriptions_count
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereAtive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereCharges($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereCommentsOperator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereGateway($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereInstallments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereInvoiceReminder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan wherePaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan wherePgmPlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereTrialDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereActive($value)
 * @property string $code
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Plan filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Plan paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Plan simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Plan whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Plan whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Plan whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Plan whereHeadline($value)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Plan onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Plan withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Plan withoutTrashed()
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property string|null $apple_product_id
 * @property string|null $google_product_id
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Plan whereAppleProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Plan whereGoogleProductId($value)
 * @property bool $active_ios
 * @property bool $active_android
 * @property string|null $description
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Variant[] $variants
 * @property-read int|null $variants_count
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereActiveAndroid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereActiveIos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereDescription($value)
 * @property int|null $product_id
 * @property-read \Rockapps\RkLaravel\Models\Product|null $product
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereProductId($value)
 */
class Plan extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use Filterable, SoftDeletes, Auditable;

    const GATEWAY_MANUAL = 'MANUAL';

    // Disable Laravel's mass assignment protection
    const GATEWAY_PAGARME = 'PAGARME';
    const GATEWAY_IAP = 'IAP';
    public $transformer = PlanTransformer::class;
    protected $guarded = [];
    protected $casts = [
        'value' => 'float',
        'active' => 'boolean',
        'active_android' => 'boolean',
        'active_ios' => 'boolean',
    ];
    protected $dates = [
        'paid_at',
        'boleto_expiration_date',
    ];
    protected $attributes = [
        'gateway' => self::GATEWAY_MANUAL,
        'days' => 30,
        'trial_days' => 0,
        'payment_method' => 'boleto,credit_card,iap',
        'color' => '#000000',
        'charges' => null,
        'installments' => 1,
        'active' => true,
        'active_android' => true,
        'active_ios' => true,
        'invoice_reminder' => 4,
    ];
    protected $fillable = [
        'value',
        'apple_product_id',
        'google_product_id',
        'active_ios',
        'active_android',
        'code',
        'name',
        'gateway',
        'description',
        'days',
        'trial_days',
        'payment_method',
        'color',
        'charges',
        'installments',
        'pgm_plan_id',
        'product_id',
        'active',
        'invoice_reminder',
        'comments_operator',
    ];

    protected $rules = [
        'value' => 'required|numeric',
        'name' => 'required|string',
        'gateway' => 'required|string|in:MANUAL,PAGARME,IAP',
        'days' => 'required|numeric',
        'trial_days' => 'required|numeric',
        'payment_method' => 'nullable|string',
        'color' => 'nullable|string',
        'charges' => 'nullable|numeric',
        'installments' => 'required|numeric',
//        'pgm_plan_id',
        'active' => 'required|boolean',
        'invoice_reminder' => 'required|numeric',
        'comments_operator' => 'nullable|string',
        'apple_product_id' => 'nullable|string',
        'google_product_id' => 'nullable|string',
        'product_id' => 'nullable|numeric|exists:products,id',
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar o plano.',
        'error.not_found' => 'Plano não encontrado.',
    ];

    public static function boot()
    {
        parent::boot();
        self::saved(function (Plan $plan) {
            if ($plan->gateway === self::GATEWAY_PAGARME) {
                return PagarMePlan::save($plan);
            }
            return true;
        });
        self::updating(function (Plan $plan) {

            if ($plan->gateway === self::GATEWAY_PAGARME && $plan->pgm_plan_id) {
                $immutable_values = [
                    'gateway',
                    'days',
                    'payment_method',
                    'charges',
                    'installments',
                ];

                foreach ($immutable_values as $immutable_value) {
                    $plan->$immutable_value = $plan->getOriginal($immutable_value);
                }
            }
        });
    }

    public function validate(array $rules = array())
    {
        if ($this->active_android) $rules['google_product_id'] = ['required', 'string', 'min:3'];
        if ($this->active_ios) $rules['apple_product_id'] = ['required', 'string', 'min:3'];

        return parent::validate($rules);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id')->withTrashed();
    }

    public function variants()
    {
        return $this->belongsToMany(Variant::class, 'object_variants', 'object_id', 'variant_id')
            ->wherePivot('object_type', '=', Plan::class);
    }

    public function modelFilter()
    {
        return $this->provideFilter(PlanFilter::class);
    }
}
