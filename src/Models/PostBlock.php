<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Rockapps\RkLaravel\ModelFilters\PostBlockFilter;
use Rockapps\RkLaravel\Transformers\PostBlockTransformer;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * Rockapps\RkLaravel\Models\PostBlock
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereLike($column, $value, $boolean = 'and')
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $content
 * @property string|null $type
 * @property int $sequence
 * @property int $post_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock whereSequence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock whereUpdatedAt($value)
 * @property-read \Rockapps\RkLaravel\Models\Post|null $post
 * @property int|null $text_size
 * @property bool $bold
 * @property bool $italic
 * @property string|null $color
 * @property array|null $style
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock whereBold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock whereItalic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock whereStyle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\PostBlock whereTextSize($value)
 */
class PostBlock extends ModelBase implements HasMedia
{
    use HasMediaTrait;
    use Filterable;

    const TYPE_TEXT = 'TEXT';
    const TYPE_IMAGE = 'IMAGE';
    const TYPE_LINK = 'LINK';
    const TYPE_YOUTUBE = 'YOUTUBE';
    const TYPE_UL = 'UL';
    const TYPE_OL = 'OL';
    public $transformer = PostBlockTransformer::class;
    protected $table = 'post_blocks';
    protected $guarded = [];
    protected $dates = [];
    protected $casts = [
        'post_id' => 'int',
        'sequence' => 'int',
        'text_size' => 'int',
        'bold' => 'boolean',
        'italic' => 'boolean',
        'style' => 'array',
    ];

    protected $fillable = [
        'content',
        'type',
        'sequence',
        'post_id',
        'text_size',
        'bold',
        'italic',
        'color',
        'style',
    ];

    protected $rules = [
        'content' => 'nullable|string',
        'type' => 'required|string|in:TEXT,IMAGE,LINK,YOUTUBE,UL,OL',
        'sequence' => 'required|numeric',
        'post_id' => 'required|numeric|exists:posts,id',
    ];

    public function post()
    {
        return $this->hasOne(Post::class, 'id', 'post_id');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(600)
//            ->height(232)
            ->sharpen(10);
    }

    public function modelFilter()
    {
        return $this->provideFilter(PostBlockFilter::class);
    }
}
