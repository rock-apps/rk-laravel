<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use Rockapps\RkLaravel\ModelFilters\ProductFilter;
use Rockapps\RkLaravel\Traits\HasCategories;
use Rockapps\RkLaravel\Traits\Rate\Rateable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * Rockapps\RkLaravel\Models\Product
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Rockapps\RkLaravel\Models\Category $category
 * @property-read \Rockapps\RkLaravel\Models\Company $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Order[] $orders
 * @property-read int|null $orders_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Product onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Product withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Product withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string|null $image
 * @property string|null $description
 * @property bool $active
 * @property float $unit_value
 * @property int|null $company_id
 * @property int|null $category_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product whereUnitValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product whereUpdatedAt($value)
 * @property \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product withAllCategories($categories)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product withAnyCategories($categories)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product withCategories($categories)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product withoutAnyCategories()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Product withoutCategories($categories)
 * @property string $type
 * @property string $instance
 * @property string|null $variation_property
 * @property string|null $variation_value
 * @property string|null $weight
 * @property string|null $dimension_height
 * @property string|null $dimension_width
 * @property string|null $dimension_depth
 * @property int|null $main_product_id
 * @property-read Product|null $mainProduct
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $productsVariations
 * @property-read int|null $products_variations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Rating[] $ratings
 * @property-read int|null $ratings_count
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDimensionDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDimensionHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDimensionWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereInstance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereMainProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereVariationProperty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereVariationValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereWeight($value)
 * @property string|null $apple_product_id
 * @property string|null $google_product_id
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereAppleProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereGoogleProductId($value)
 * @property float $virtual_units_release
 * @property bool $active_ios
 * @property bool $active_android
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereActiveAndroid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereActiveIos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereVirtualUnitsRelease($value)
 * @property string|null $sku
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereSku($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\OrderVirtualUnit[] $ordersVirtualUnit
 * @property-read int|null $orders_virtual_unit_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Variant[] $variants
 * @property-read int|null $variants_count
 */
class Product extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable, HasMedia
{
    use Filterable;
    use SoftDeletes, Auditable;
    use HasMediaTrait;
    use HasCategories;
    use Rateable;

    const TYPE_SERVICE_PRESENTIAL = 'SERVICE_PRESENTIAL';
    const TYPE_SERVICE_REMOTE = 'SERVICE_REMOTE';
    const TYPE_PRODUCT_VIRTUAL = 'PRODUCT_VIRTUAL';
    const TYPE_PRODUCT_PHYSICAL = 'PRODUCT_PHYSICAL';
    const TYPES = [
        self::TYPE_SERVICE_PRESENTIAL,
        self::TYPE_SERVICE_REMOTE,
        self::TYPE_PRODUCT_VIRTUAL,
        self::TYPE_PRODUCT_PHYSICAL,
    ];

    const INSTANCE_MAIN = 'MAIN';
    const INSTANCE_VARIATION = 'VARIATION';

    protected $casts = [
        'unit_value' => 'float',
        'active' => 'bool',
        'active_ios' => 'bool',
        'active_android' => 'bool',
        'virtual_units_release' => 'float',
        'weight' => 'float',
        'dimension_height' => 'float',
        'dimension_width' => 'float',
        'dimension_depth' => 'float',
        'company_id' => 'int',
        'main_product_id' => 'int',
    ];

    protected $fillable = [
        'name',
        'type',
        'sku',
        'instance',
        'variation_property',
        'variation_value',
        'image',
        'description',
        'active',
        'active_ios',
        'active_android',
        'weight',
        'dimension_height',
        'dimension_width',
        'dimension_depth',
        'unit_value',
        'virtual_units_release',
        'apple_product_id',
        'google_product_id',
        'active_ios',
        'active_android',
        'company_id',
        'main_product_id',
    ];

    protected $attributes = [
        'type' => Product::TYPE_PRODUCT_PHYSICAL,
        'instance' => Product::INSTANCE_MAIN,
    ];

    protected $rules = [
        'name' => 'required|string',//|min:5',
        'description' => 'nullable',
        'unit_value' => 'required|numeric',
        'active' => 'required|boolean',
        'company_id' => 'required|exists:companies,id',
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar o produto.',
        'error.not_found' => 'Produto não encontrado.',
    ];


    public function company()
    {
        $class = config('rk-laravel.company.model', \Rockapps\RkLaravel\Models\Company::class);
        return $this->belongsTo($class, 'company_id', 'id');
    }

    public function mainProduct()
    {
        $class = config('rk-laravel.product.model', \Rockapps\RkLaravel\Models\Product::class);
        return $this->belongsTo($class, 'main_product_id', 'id');
    }

    public function productsVariations()
    {
        $class = config('rk-laravel.product.model', \Rockapps\RkLaravel\Models\Product::class);
        return $this->hasMany($class, 'main_product_id', 'id');
    }

    public function variants()
    {
        $class = config('rk-laravel.product.model', \Rockapps\RkLaravel\Models\Product::class);
        return $this->belongsToMany(Variant::class, 'object_variants', 'object_id', 'variant_id')
            ->wherePivot('object_type', '=', $class);
    }

    public function orders()
    {
        $class = config('rk-laravel.order.model', \Rockapps\RkLaravel\Models\Order::class);
        return $this->hasMany($class, 'id', 'product_id');
    }

    public function ordersVirtualUnit()
    {
        $class = config('rk-laravel.order_virtual_unit.model', \Rockapps\RkLaravel\Models\OrderVirtualUnit::class);
        return $this->hasMany($class, 'id', 'product_id');
    }

    /**
     * @param Media|null $media
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->keepOriginalImageFormat()
            ->width(640)
//            ->height(232)
            ->sharpen(10);

        $this->addMediaConversion('cover')
            ->keepOriginalImageFormat()
            ->width(1280)
            ->sharpen(10);
    }


    public function modelFilter()
    {
        return $this->provideFilter(config('rk-laravel.product.filter', ProductFilter::class));
    }

}
