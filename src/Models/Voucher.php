<?php

namespace Rockapps\RkLaravel\Models;

use Carbon\Carbon;
use EloquentFilter\Filterable;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\ModelFilters\VoucherFilter;
use Rockapps\RkLaravel\Services\VoucherCodeGenerator;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Transformers\VoucherTransformer;


/**
 * Rockapps\RkLaravel\Models\Voucher
 *
 * @property int $id
 * @property string $code
 * @property string $model_type
 * @property int $model_id
 * @property \Illuminate\Support\Collection|null $data
 * @property \Illuminate\Support\Carbon|null $expires_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $model
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher whereExpiresAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher whereModelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher whereModelType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Voucher whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $max_redeem_qty
 * @property int|null $company_id
 * @property string|null $campaign
 * @method static \Illuminate\Database\Eloquent\Builder|Voucher whereCampaign($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Voucher whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Voucher whereMaxRedeemQty($value)
 * @property-read \Rockapps\RkLaravel\Models\Company|null $company
 */
class Voucher extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use Filterable;
    use Auditable;

    public $transformer = VoucherTransformer::class;

    protected $fillable = [
        'model_id',
        'model_type',
        'code',
        'data',
        'expires_at',
        'max_redeem_qty',
        'company_id',
        'campaign',
    ];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'expires_at'
    ];
    protected $casts = [
        'data' => 'collection',
        'max_redeem_qty' => 'int'
    ];
    protected $attributes = [
        'max_redeem_qty' => 1,
    ];

    protected $rules = [
        'max_redeem_qty' => 'required|numeric|min:1',
        'company_id' => 'nullable|exists:companies,id',
        'campaign' => 'nullable',
        'code' => 'required',
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao realizar ao salvar o voucher.',
        'error.not_found' => 'Voucher não encontrado.',
    ];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->table = config('vouchers.table', 'vouchers');
    }

    /**
     * Check if code is expired.
     *
     * @return bool
     */
    public function isExpired()
    {
        return $this->expires_at ? Carbon::now()->gte($this->expires_at) : false;
    }

    public function availableQty()
    {
        $used_qty = \DB::table('user_voucher')
            ->where('voucher_id', $this->id)
            ->count();

        return $this->max_redeem_qty - $used_qty;
    }

    public function modelFilter()
    {
        return $this->provideFilter(VoucherFilter::class);
    }

    public function validate(array $rules = array())
    {
        if (!$this->code) {
            /** @var VoucherCodeGenerator $service */
            $service = config('rk-laravel.voucher.service_code', VoucherCodeGenerator::class);
            $this->code = (new $service())->generate();
        } else {
            if (self::whereCode($this->code)->where('id', '<>', $this->id)->count()) {
                throw new ResourceException('Este código já está sendo utilizado');
            }
        }
        return parent::validate($rules);
    }

    /**
     * Get the users who redeemed this voucher.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(config('rk-laravel.user.model', User::class), 'user_voucher')->withPivot('redeemed_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function model()
    {
        return $this->morphTo();
    }

    public function company()
    {
        return $this->belongsTo(config('rk-laravel.company.model', \Rockapps\RkLaravel\Models\Company::class), 'company_id');
    }

}
