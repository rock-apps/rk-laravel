<?php

namespace Rockapps\RkLaravel\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Rockapps\RkLaravel\Models\UserDevice
 *
 * @property int $id
 * @property string $registration_id
 * @property int|null $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\UserDevice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\UserDevice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\UserDevice query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\UserDevice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\UserDevice whereRegistrationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\UserDevice whereUserId($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\UserDevice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\UserDevice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\ModelBase whereLike($column, $value, $boolean = 'and')
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\UserDevice onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\UserDevice whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\UserDevice withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\UserDevice withoutTrashed()
 */
class UserDevice extends ModelBase
{
    use SoftDeletes;
    // Disable Laravel's mass assignment protection
    protected $guarded = [];
}
