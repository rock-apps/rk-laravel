<?php

namespace Rockapps\RkLaravel\Models;

use Carbon\Carbon;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\ModelFilters\CreditCardFilter;

/**
 * Rockapps\RkLaravel\Models\CreditCard
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $owner
 * @method static \Illuminate\Database\Eloquent\Builder|CreditCard filter($input = [], $filter = null)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditCard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditCard newQuery()
 * @method static \Illuminate\Database\Query\Builder|CreditCard onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditCard paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditCard query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|CreditCard simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CreditCard whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|CreditCard whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|CreditCard whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Query\Builder|CreditCard withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CreditCard withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string $pgm_card_id
 * @property string $holder_name
 * @property string $expiration_date
 * @property string $brand
 * @property string $last_digits
 * @property bool $valid
 * @property bool $default
 * @property string $owner_type
 * @property int $owner_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\CreditCard whereBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\CreditCard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\CreditCard whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\CreditCard whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\CreditCard whereExpirationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\CreditCard whereHolderName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\CreditCard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\CreditCard whereLastDigits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\CreditCard whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\CreditCard whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\CreditCard wherePgmCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\CreditCard whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\CreditCard whereValid($value)
 */
class CreditCard extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Auditable, Filterable;

    protected $casts = [
        'owner_id' => 'int',
        'valid' => 'bool',
        'default' => 'bool',
    ];

    protected $fillable = [
        'pgm_card_id',
        'holder_name',
        'expiration_date',
        'brand',
        'last_digits',
        'valid',
        'default',
        'owner_type',
        'owner_id',
    ];

    protected $rules = [
        'pgm_card_id' => 'required',
        'holder_name' => 'required',
        'expiration_date' => 'required|max:4',
        'brand' => 'required',
        'last_digits' => 'required',
        'default' => 'required',
        'owner_type' => 'required',
        'owner_id' => 'required',
    ];


    protected $validationMessages = [
        'error.save' => 'Erro ao salvar o cartão.',
        'error.not_found' => 'Cartão não encontrado.',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function (CreditCard $card) {
            $count = CreditCard::whereOwnerId($card->owner_id)
                ->whereOwnerType($card->owner_type)
                ->count();
            if ($count === 0) {
                $card->default = true;
            } else if ($count === 4) {
                throw new ResourceException('Não é permitido ter mais do que 4 cartões cadastrados.');
            }

            $split = str_split($card->expiration_date, 2);
            $month = $split[0];
            $year = $split[1];

            $date = Carbon::create('20' . $year, $month, 1, 23, 59, 59)->endOfMonth();
            if (now() > $date) {
                throw new ResourceException('Este cartão está vencido.');
            }
        });

        /*
         * Não dá para utilizar a função abaixo pois não possuímos o número do cartão cadastrado
         */
//        self::created(function (CreditCard $card) {
//            if (!$card->pgm_card_id) {
//                PagarMeCreditCard::save($card);
//            }
//        });
    }

    public function setDefault()
    {
        foreach ($this->owner->creditCards as $creditCard) {
            /** @var CreditCard $creditCard */
            $creditCard->default = false;
            $creditCard->save();
        }
        $this->default = true;
        return $this->save();
    }

    public function getRules()
    {
        return [
            'holder_name' => 'sometimes',
            'expiration_date' => 'sometimes',
            'brand' => 'sometimes',
            'last_digits' => 'sometimes',
        ];
    }

    public function modelFilter()
    {
        return $this->provideFilter(CreditCardFilter::class);
    }

    public function owner()
    {
        return $this->morphTo('owner');
    }

}
