<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use Rockapps\RkLaravel\Interfaces\BankRecipientInterface;
use Rockapps\RkLaravel\Interfaces\FavoritableInterface;
use Rockapps\RkLaravel\Interfaces\HasBankAccountInterface;
use Rockapps\RkLaravel\ModelFilters\CompanyFilter;
use Rockapps\RkLaravel\Services\CompanyCanReceiveOrderService;
use Rockapps\RkLaravel\Traits\Favoritable;
use Rockapps\RkLaravel\Traits\HasAddresses;
use Rockapps\RkLaravel\Traits\HasBankAccounts;
use Rockapps\RkLaravel\Traits\HasBankRecipient;
use Rockapps\RkLaravel\Traits\HasCategories;
use Rockapps\RkLaravel\Traits\Subscriber;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * Rockapps\RkLaravel\Models\Company
 *
 * @property int $id
 * @property string $name
 * @property string|null $headline
 * @property string|null $about
 * @property bool $active
 * @property bool $suspended
 * @property mixed $deliver_value
 * @property int|null $deliver_estimate_time
 * @property int|null $deliver_max_range
 * @property string|null $mobile
 * @property string|null $telephone
 * @property float|null $long
 * @property float|null $lat
 * @property string|null $recipient_id
 * @property string|null $bank_account_id
 * @property int|null $address_id
 * @property int|null $responsible_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Rockapps\RkLaravel\Models\Address|null $address
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Favorite[] $favorites
 * @property-read int|null $favorites_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Product[] $products
 * @property-read int|null $products_count
 * @property-read \Rockapps\RkLaravel\Models\User|null $responsible
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Company onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereBankAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereDeliverEstimateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereDeliverMaxRange($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereDeliverValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereHeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereRecipientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereResponsibleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereSuspended($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company withAllCategories($categories)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company withAnyCategories($categories)
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company withCategories($categories)
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Company withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company withoutAnyCategories()
 * @method static \Illuminate\Database\Eloquent\Builder|\Rockapps\RkLaravel\Models\Company withoutCategories($categories)
 * @method static \Illuminate\Database\Query\Builder|\Rockapps\RkLaravel\Models\Company withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $min_value_to_free_deliver
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereMinValueToFreeDeliver($value)
 * @property string|null $email
 * @property string|null $cnpj
 * @property string|null $cnaes
 * @property string|null $cnae_main
 * @property string|null $date_opening
 * @property string|null $situation
 * @property string|null $segment
 * @property string|null $size
 * @property string|null $business
 * @property string|null $website
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereBusiness($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereCnaeMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereCnaes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereCnpj($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereDateOpening($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereSegment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereSituation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereWebsite($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Address[] $addresses
 * @property-read int|null $addresses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\BankAccount[] $bankAccounts
 * @property-read int|null $bank_accounts_count
 * @property-read \Rockapps\RkLaravel\Models\BankRecipient|null $bankRecipient
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\BankTransfer[] $bankTransfers
 * @property-read int|null $bank_transfers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Subscription[] $subscriptions
 * @property-read int|null $subscriptions_count
 * @property string|null $short_name
 * @property string|null $fantasy_name
 * @property string|null $document
 * @property string|null $description
 * @property string $priority
 * @property int $ready_to_search
 * @property string|null $contact_telephone
 * @property string|null $contact_telephone_description
 * @property string|null $contact_mobile
 * @property string|null $contact_mobile_description
 * @property string|null $contact_email
 * @property string|null $contact_email_description
 * @property string|null $contact_whatsapp
 * @property string|null $contact_instagram
 * @property string|null $contact_facebook
 * @property string|null $primary_color
 * @property string|null $secondary_color
 * @property string|null $third_color
 * @property string|null $logo
 * @property string|null $subdomain
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereContactEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereContactEmailDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereContactFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereContactInstagram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereContactMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereContactMobileDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereContactTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereContactTelephoneDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereContactWhatsapp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereFantasyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company wherePrimaryColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereReadyToSearch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereSecondaryColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereSubdomain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereThirdColor($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\PaymentMethod[] $paymentMethods
 * @property-read int|null $payment_methods_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\ShippingMethod[] $shippingMethods
 * @property-read int|null $shipping_methods_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Employee[] $employees
 * @property-read int|null $employees_count
 */
class Company extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable, HasMedia, FavoritableInterface, HasBankAccountInterface, BankRecipientInterface
{
    use HasAddresses, HasBankAccounts, Subscriber;
    use HasBankRecipient;
    use Filterable;
    use SoftDeletes;
    use Auditable;
    use HasMediaTrait;
    use HasCategories;
    use Favoritable;

    const SITUCACAO_ATIVO = 'ativa';
    const SITUCACAO_ENCERRADA = 'encerrada';
    const SITUCACAO_OUTROS = 'outros';
    const TIPO_PJ_EMPRESA = 'empresa';
    const TIPO_PJ_ARTESAO = 'artesao';
    const TIPO_PJ_PRODUTOR_RURAL = 'produtor_rural';
    const PRIORITY_HIGH = 'HIGH';
    const PRIORITY_NORMAL = 'NORMAL';
    const PRIORITY_LOW = 'LOW';
    const PORTE_ME = 1;
    const PORTE_EPP = 2;
    const PORTE_MEDIA = 3;
    const PORTE_GRANDE = 4;
    const PORTE_MEI = 5;
    const SETORES_INDUSTRIA = 1;
    const SETORES_COMERCIO = 2;
    const SETORES_SERVICOS = 3;
    const SETORES_AGROPECUARIA = 4;
    protected $casts = [
        'active' => 'bool',
        'suspended' => 'bool',
        'responsible_id' => 'int',
        'deliver_value' => 'decimal:2',
        'deliver_estimate_time' => 'int',
        'deliver_max_range' => 'int',
        'min_value_to_free_deliver' => 'float',
        'address_id' => 'int',
    ];
    protected $fillable = [
        'name',
        'short_name',
        'fantasy_name',
        'document',
        'description',
        'priority',
        'headline',
        'ready_to_search',
        'active',
        'suspended',
        'contact_telephone',
        'contact_telephone_description',
        'contact_mobile',
        'contact_mobile_description',
        'contact_email',
        'contact_email_description',
        'contact_whatsapp',
        'contact_instagram',
        'contact_facebook',
        'primary_color',
        'secondary_color',
        'third_color',
        'subdomain',
        'deliver_value',
        'deliver_estimate_time',
        'deliver_max_range',
        'mobile',
        'telephone',
        'long',
        'lat',
        'recipient_id',
        'bank_account_id',
        'address_id',
        'responsible_id',
        'email',
        'cnpj',
        'cnaes',
        'cnae_main',
        'date_opening',
        'situation',
        'segment',
        'size',
        'business',
        'website',
    ];

    protected $attributes = [
        'active' => true,
        'suspended' => false,
        'deliver_value' => 0,
        'deliver_max_range' => 3,
//        'min_value_to_free_deliver' => 0 // Exite um mutator que transforma esse valor em null caso seja zero
//        'deliver_estimate_time' => '2'
    ];

    protected $rules = [
        'suspended' => 'required|boolean',
        'name' => 'nullable|string',
        'headline' => 'nullable',
        'about' => 'nullable',
        'active' => 'required|boolean',
        'mobile' => 'nullable',
        'telephone' => 'nullable',
        'responsible_id' => 'required|exists:users,id',
        'deliver_max_range' => 'nullable|numeric|min:1',
        'deliver_estimate_time' => 'nullable|numeric',
        'address_id' => 'nullable|numeric|exists:addresses,id',
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar a empresa.',
        'error.not_found' => 'Empresa não encontrada.',
    ];


    public static function boot()
    {
        parent::boot();
        self::saving(function (Company $company) {
            $address = Address::find($company->address_id);
            if ($address) {
                $company->lat = $address->lat;
                $company->long = $address->lng;
            }
        });
    }

    public function canReceiverOrders()
    {
        /** @var CompanyCanReceiveOrderService $service */
        $service = config('rk-laravel.company.can_receive_order_service', CompanyCanReceiveOrderService::class);
        return $service::canReceive($this);
    }

    public function checkReady()
    {
        /** @var CompanyCanReceiveOrderService $service */
        $service = config('rk-laravel.company.can_receive_order_service', CompanyCanReceiveOrderService::class);
        return $service::checkReady($this);
    }

    public function products()
    {
        return $this->hasMany(\Rockapps\RkLaravel\Models\Product::class, 'company_id', 'id');
    }

    public function paymentMethods()
    {
        return $this->hasMany(\Rockapps\RkLaravel\Models\PaymentMethod::class, 'company_id', 'id');
    }

    public function shippingMethods()
    {
        return $this->hasMany(\Rockapps\RkLaravel\Models\ShippingMethod::class, 'company_id', 'id');
    }

    public function calculateRating()
    {
        return (float)$this->orders()->whereNotNull('rating')->average('rating');
    }

    public function orders()
    {
        return $this->hasMany(\Rockapps\RkLaravel\Models\Order::class, 'company_id', 'id');
    }

    public function employees()
    {
        /** @var Employee $model */
        $model = config('rk-laravel.employee.model', Employee::class);
        return $this->hasMany($model, 'company_id', 'id');
    }

    public function countRating()
    {
        return (float)$this->orders()->where('rating', '>=', 1)->count('rating');
    }

    public function responsible()
    {
        return $this->belongsTo(\Rockapps\RkLaravel\Models\User::class, 'responsible_id', 'id');
    }

    public function address()
    {
        return $this->hasOne(Address::class, 'id', 'address_id')->withTrashed();
    }

    public function setMinValueToFreeDeliver($value)
    {
        $this->attributes['min_value_to_free_deliver'] = $value ? $value : null;
    }

    public function setMobileAttribute($value)
    {
        $this->attributes['mobile'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function setTelephoneAttribute($value)
    {
        $this->attributes['telephone'] = preg_replace('/[^0-9]/', '', $value);
    }

    /**
     * @param Media|null $media
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->keepOriginalImageFormat()
            ->width(600)
//            ->height(232)
            ->sharpen(10);
    }


    public function modelFilter()
    {
        return $this->provideFilter(CompanyFilter::class);
    }

}
