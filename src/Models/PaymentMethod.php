<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;
use Rockapps\RkLaravel\ModelFilters\PaymentMethodFilter;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Transformers\PaymentMethodTransformer;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Rockapps\RkLaravel\Models\PaymentMethod
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $owner
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod newQuery()
 * @method static \Illuminate\Database\Query\Builder|PaymentMethod onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod query()
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Query\Builder|PaymentMethod withTrashed()
 * @method static \Illuminate\Database\Query\Builder|PaymentMethod withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string $owner_type
 * @property int $owner_id
 * @property string $gateway
 * @property string $method
 * @property string|null $gateway_key
 * @property string|null $gateway_secret
 * @property int|null $bank_account_id
 * @property bool $active
 * @property string|null $image
 * @property string|null $title
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereBankAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereGateway($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereGatewayKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereGatewaySecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereUpdatedAt($value)
 * @property bool $gateway_sandbox
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereGatewaySandbox($value)
 * @property-read \Rockapps\RkLaravel\Models\BankAccount|null $bankAccount
 * @property int $company_id
 * @property-read \Rockapps\RkLaravel\Models\Company $company
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereCompanyId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property string|null $gateway_webhook
 * @property string|null $gateway_soft_descriptor
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereGatewaySoftDescriptor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaymentMethod whereGatewayWebhook($value)
 */
class PaymentMethod extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable, HasMedia
{
    use SoftDeletes, Auditable, Filterable;
    use HasMediaTrait;

    const GATEWAY_PGM = 'PGM';
    const GATEWAY_MANUAL = 'MANUAL';

    const GATEWAYS = [
        self::GATEWAY_PGM,
        self::GATEWAY_MANUAL,
    ];

    public $transformer = PaymentMethodTransformer::class;

    protected $attributes = [
        'active' => false,
        'gateway_sandbox' => false,
    ];

    protected $casts = [
        'gateway' => 'string',
        'method' => 'string',
        'gateway_key' => 'string',
        'gateway_secret' => 'string',
        'gateway_sandbox' => 'bool',
        'bank_account_id' => 'int',
        'company_id' => 'int',
        'active' => 'bool',
        'image' => 'string',
        'title' => 'string',
        'description' => 'string',
        'gateway_soft_descriptor' => 'string',
    ];

    protected $fillable = [
        'gateway',
        'method',
        'gateway_key',
        'gateway_secret',
        'gateway_webhook',
        'bank_account_id',
        'active',
        'image',
        'title',
        'description',
        'gateway_soft_descriptor',
    ];

    protected $rules = [
        'gateway' => 'required',
        'method' => 'required',
        'gateway_key' => 'nullable',
        'gateway_secret' => 'nullable',
        'gateway_sandbox' => 'nullable|bool',
        'gateway_webhook' => 'nullable',
        'bank_account_id' => 'nullable|exists:bank_accounts,id',
        'active' => 'required|bool',
        'image' => 'nullable',
        'title' => 'required',
        'description' => 'nullable',
        'gateway_soft_descriptor' => 'nullable',
        'company_id' => 'required|exists:companies,id',
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar método de pagamento.',
        'error.not_found' => 'Método de Pagamento não encontrado.',
        'method.required' => 'O tipo de conta é obrigatório.',
        'gateway.required' => 'O gateway é obrigatório.',
        'company_id.required' => 'A empresa é obrigatória',
    ];

    public function validate(array $rules = array())
    {
        $rules['gateway'] = ['required', Rule::in(self::GATEWAYS)];

        if ($this->gateway === self::GATEWAY_PGM) {
            $rules['gateway_key'] = ['required'];
            $rules['gateway_secret'] = ['required'];
            $rules['gateway_sandbox'] = ['required', 'boolean'];
            $rules['gateway_soft_descriptor'] = ['required'];

            $rules['method'] = ['required', Rule::in([
                Payment::GATEWAY_PAGARME_PIX,
                Payment::GATEWAY_PAGARME_BOLETO,
                Payment::GATEWAY_PAGARME_CC,
            ])];

        } else if ($this->gateway === self::GATEWAY_MANUAL) {
            $rules['method'] = ['required', Rule::in([
                Payment::GATEWAY_FATURA,
                Payment::GATEWAY_CASH,
                Payment::GATEWAY_BANK_TRANSFER,
                Payment::GATEWAY_BALANCE,
                Payment::GATEWAY_METHOD_IAP_APPLE,
                Payment::GATEWAY_METHOD_IAP_GOOGLE,
                Payment::GATEWAY_MANUAL,
                Payment::GATEWAY_VOUCHER,
            ])];

            if ($this->method === Payment::GATEWAY_BANK_TRANSFER) {
                $rules['bank_account_id'] = ['required', 'exists:bank_accounts,id'];
            }
        }

        return parent::validate($rules);
    }

    public function modelFilter()
    {
        return $this->provideFilter(PaymentMethodFilter::class);
    }

    public function bankAccount()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $this->belongsTo(BankAccount::class, 'bank_account_id', 'id')->withTrashed();
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
}
