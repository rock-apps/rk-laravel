<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;
use Rockapps\RkLaravel\ModelFilters\ShippingMethodFilter;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Transformers\ShippingMethodTransformer;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Rockapps\RkLaravel\Models\ShippingMethod
 *
 * @property int $id
 * @property string $carrier
 * @property string|null $carrier_key
 * @property string|null $carrier_secret
 * @property bool $carrier_sandbox
 * @property int $company_id
 * @property string|null $address_id
 * @property float|null $fixed_value
 * @property bool $active
 * @property string|null $image
 * @property string|null $title
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Rockapps\RkLaravel\Models\Address|null $address
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Rockapps\RkLaravel\Models\Company $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod newQuery()
 * @method static \Illuminate\Database\Query\Builder|ShippingMethod onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod query()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereCarrier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereCarrierKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereCarrierSandbox($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereCarrierSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereFixedValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingMethod whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|ShippingMethod withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ShippingMethod withoutTrashed()
 * @mixin \Eloquent
 */
class ShippingMethod extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable, HasMedia
{
    use SoftDeletes, Auditable, Filterable;
    use HasMediaTrait;

    const CARRIER_SYSTEM_CALCULATION = 'SYSTEM_CALCULATION'; // Cálculo específico do sistema através de Service
    const CARRIER_FIXED = 'FIXED';
    const CARRIER_MANUAL = 'MANUAL';
    const CARRIER_CORREIOS_PAC = 'CORREIOS_PAC';
    const CARRIER_CORREIOS_SEDEX = 'CORREIOS_SEDEX';
    const CARRIER_LALAMOVE = 'LALAMOVE';
    const CARRIER_ADDRESS_PICKUP = 'ADDRESS_PICKUP';

    const CARRIERS = [
        self::CARRIER_FIXED,
        self::CARRIER_MANUAL,
        self::CARRIER_SYSTEM_CALCULATION,
        self::CARRIER_CORREIOS_PAC,
        self::CARRIER_CORREIOS_SEDEX,
        self::CARRIER_LALAMOVE,
        self::CARRIER_ADDRESS_PICKUP,
    ];

    public $transformer = ShippingMethodTransformer::class;

    protected $attributes = [
        'active' => false,
        'carrier_sandbox' => false,
    ];

    protected $casts = [
        'carrier' => 'string',
        'carrier_key' => 'string',
        'carrier_secret' => 'string',
        'carrier_sandbox' => 'bool',
        'company_id' => 'int',
        'address_id' => 'string',
        'active' => 'bool',
        'image' => 'string',
        'title' => 'string',
        'description' => 'string',
        'fixed_value' => 'float',
    ];

    protected $fillable = [
        'carrier',
        'carrier_key',
        'carrier_secret',
        'carrier_sandbox',
        'company_id',
        'address_id',
        'active',
        'image',
        'title',
        'description',
        'fixed_value',
    ];

    protected $rules = [
        'carrier' => 'required',
        'carrier_key' => 'nullable',
        'carrier_secret' => 'nullable',
        'carrier_sandbox' => 'nullable|bool',
        'company_id' => 'required|exists:companies,id',
        'address_id' => 'nullable|exists:addresses,id',
        'active' => 'required|bool',
        'image' => 'nullable',
        'title' => 'required',
        'description' => 'nullable',
        'fixed_value' => 'nullable|numeric',
    ];

    protected $validationMessages = [
        'error.save' => 'Erro ao salvar método de entrega.',
        'error.not_found' => 'Método de Entrega não encontrado.',
        'carrier.required' => 'O entregador é obrigatório.',
        'company_id.required' => 'A empresa é obrigatória',
    ];

    public function validate(array $rules = array())
    {
        $rules['carrier'] = ['required', Rule::in(self::CARRIERS)];

        if ($this->carrier === self::CARRIER_LALAMOVE) {
            $rules['carrier_key'] = ['required'];
            $rules['carrier_secret'] = ['required'];
            $rules['carrier_sandbox'] = ['required', 'boolean'];
        }
        if ($this->carrier === self::CARRIER_ADDRESS_PICKUP) {
            $rules['address_id'] = ['required', 'exists:addresses,id'];
        }
        if (in_array($this->carrier,[self::CARRIER_CORREIOS_SEDEX,self::CARRIER_CORREIOS_PAC])) {
            $rules['address_id'] = ['required', 'exists:addresses,id'];
        }
        if ($this->carrier === self::CARRIER_FIXED) {
            $rules['fixed_value'] = ['required', 'numeric'];
        }

        return parent::validate($rules);
    }

    public function modelFilter()
    {
        return $this->provideFilter(ShippingMethodFilter::class);
    }

    public function address()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return $this->belongsTo(\Rockapps\RkLaravel\Models\Address::class, 'address_id')
            ->withTrashed();
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
}
