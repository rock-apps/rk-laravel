<?php

namespace Rockapps\RkLaravel\Models;

use Carbon\Carbon;
use EloquentFilter\Filterable;
use Iben\Statable\Statable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Musonza\Chat\Models\Conversation;
use OwenIt\Auditing\Auditable;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Events\OrderApproved;
use Rockapps\RkLaravel\Events\OrderCanceled;
use Rockapps\RkLaravel\Events\OrderCreated;
use Rockapps\RkLaravel\Events\OrderDelivered;
use Rockapps\RkLaravel\Events\OrderPending;
use Rockapps\RkLaravel\Events\OrderRejected;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\Chat;
use Rockapps\RkLaravel\Helpers\GoogleMapsDistanceMatrix;
use Rockapps\RkLaravel\Interfaces\PurchaseableInterface;
use Rockapps\RkLaravel\Traits\Attributes\PurchaseableAttributesTrait;
use Rockapps\RkLaravel\Traits\HasAddress;
use Rockapps\RkLaravel\Traits\HasPayment;
use Rockapps\RkLaravel\Traits\HasPayments;
use Rockapps\RkLaravel\Traits\States\OrderStateTrait;

/**
 * Rockapps\RkLaravel\Models\Order
 *
 * @property int $id
 * @property string $status
 * @property string|null $description
 * @property int|null $address_id
 * @property string|null $address_street
 * @property string|null $address_number
 * @property string|null $address_complement
 * @property string|null $address_district
 * @property string|null $address_zipcode
 * @property string|null $address_city
 * @property string|null $address_state
 * @property int|null $rating
 * @property string|null $rating_description
 * @property string|null $reject_reason
 * @property string|null $payment_gateway
 * @property \Illuminate\Support\Carbon|null $asked_at
 * @property string|null $rejected_at
 * @property \Illuminate\Support\Carbon|null $approved_at
 * @property \Illuminate\Support\Carbon|null $delivered_at
 * @property \Illuminate\Support\Carbon|null $deliver_scheduled_at
 * @property mixed $deliver_value
 * @property mixed $total_value
 * @property bool $is_paid
 * @property int|null $user_id
 * @property int|null $credit_card_id
 * @property int|null $company_id
 * @property int|null $conversation_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $voucher_id
 * @property string|null $voucher_code
 * @property string $voucher_discount_value
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Rockapps\RkLaravel\Models\Company|null $company
 * @property-read Conversation|null $conversation
 * @property-read CreditCard|null $credit_card
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\OrderItem[] $items
 * @property-read int|null $items_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|Payment[] $payments
 * @property-read int|null $payments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Iben\Statable\Models\StateHistory[] $stateHistory
 * @property-read int|null $state_history_count
 * @property-read \Rockapps\RkLaravel\Models\User|null $user
 * @property-read Voucher|null $voucher
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Query\Builder|Order onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddressCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddressComplement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddressDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddressNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddressState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddressStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddressZipcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereApprovedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAskedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereConversationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreditCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeliverScheduledAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeliverValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeliveredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereIsPaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelBase whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePaymentGateway($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereRatingDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereRejectReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereRejectedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereTotalValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereVoucherCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereVoucherDiscountValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereVoucherId($value)
 * @method static \Illuminate\Database\Query\Builder|Order withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Order withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $min_value_to_free_deliver
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereMinValueToFreeDeliver($value)
 * @property-read \Rockapps\RkLaravel\Models\Address|null $address
 * @property-read \Rockapps\RkLaravel\Models\Payment|null $payment
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\OrderCondition[] $conditions
 * @property-read int|null $conditions_count
 */
class Order extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable, PurchaseableInterface
{
    use Statable;
    use HasPayment;
    use Filterable;
    use HasAddress;
    use SoftDeletes;
    use Auditable;
    use Notifiable;
    use Statable;
    use HasPayments;
//     use   PurchaseableStateTrait;
    use PurchaseableAttributesTrait;
    use OrderStateTrait;

    const STATUS_PENDENTE = 'PENDENTE';
    const STATUS_APROVADO = 'APROVADO';
    const STATUS_ENTREGUE = 'ENTREGUE';
    const STATUS_CANCELADO = 'CANCELADO';


    public $fillable_cliente = [
        'description',
        'payment_gateway',
        'credit_card_id',
        'company_id',
        'address_id',
        'address_street',
        'address_number',
        'address_complement',
        'address_district',
        'address_zipcode',
        'address_city',
        'address_state',
        'rating',
        'rating_description',
        'voucher_code',
        'deliver_scheduled_at',
    ];
    public $fillable_prestador = [
        'reject_reason',
        'deliver_scheduled_at',
        'deliver_value'
    ];
    protected $casts = [
        'total_value' => 'decimal:2',
        'user_id' => 'int',
        'company_id' => 'int',
        'rating' => 'int',
        'is_paid' => 'bool',
        'deliver_value' => 'decimal:2',
        'value_discount_voucher' => 'decimal:2',
    ];
    protected $dates = [
        'asked',
        'asked_at',
        'deliver_scheduled_at',
        'delivered_at',
        'approved_at',
    ];
    protected $attributes = [
        'status' => self::STATUS_DRAFT,
        'is_paid' => false,
        'payment_gateway' => Payment::GATEWAY_PAGARME_CC,
    ];
    protected $fillable = [
        'description',
        'payment_gateway',
        'status',
        'delivered_at',
        'deliver_scheduled_at',
        'approved_at',
        'credit_card_id',
        'user_id',
        'company_id',
        'address_id',
        'address_street',
        'address_number',
        'address_complement',
        'address_district',
        'address_zipcode',
        'address_city',
        'address_state',
        'voucher_code',
        'rating',
        'rating_description',
        'deliver_value',
        'reject_reason',
    ];
    protected $validationMessages = [
        'error.save' => 'Erro ao salvar o pedido.',
        'error.not_found' => 'Pedido não encontrado.',
        'reject_reason.required' => 'O motivo da recusa é obrigatório.',
        'reject_reason.string' => 'O motivo da recusa é obrigatório.',
    ];

    protected $rules = [
        'voucher_id' => 'nullable|exists:vouchers,id',
        'conversation_id' => 'nullable|exists:mc_conversations,id',
        'rating' => 'nullable|integer|min:1|max:5',
        'rating_description' => 'nullable',
        'status' => 'required|in:DRAFT,PENDENTE,APROVADO,PAGO,ENTREGUE,CANCELADO,PENDING_PURCHASE,BLOCKED_PURCHASE,CONFIRMED_PURCHASE',
        'user_id' => 'required|exists:users,id',
        'company_id' => 'required|exists:companies,id',
        'credit_card_id' => 'nullable|exists:credit_cards,id',
        'address_id' => 'nullable|exists:addresses,id',
        'is_paid' => 'required|boolean',
    ];

    public static function boot()
    {
        parent::boot();
        self::updating(function (Order $order) {

            if (in_array($order->status, [Order::STATUS_DRAFT, Order::STATUS_PENDENTE])) {
                $order->total_value = $order->getTotalValue();
                $address = Address::find($order->address_id);
                if ($address) {
                    $order->address_city = $address->city;
                    $order->address_complement = $address->complement;
                    $order->address_district = $address->district;
                    $order->address_number = $address->number;
                    $order->address_state = $address->state;
                    $order->address_street = $address->street;
                    $order->address_zipcode = $address->zipcode;
                }
            }
        });

        self::saving(function (Order $order) {

            $order->updateVoucherValue();

            if ($order->getOriginal('address_id') !== $order->address_id && $order->address_id) {
                $address = Address::find($order->address_id);
                $company = Company::find($order->company_id);
                $geo = new GoogleMapsDistanceMatrix();
                $geo->addOriginByLatLong($address->lat, $address->lng);
                $geo->addDestinationByLatLong($company->lat, $company->long);
                $geo->calculate();
                $distance = $geo->getDistance(true);
                if ($distance > $company->deliver_max_range * 1000) {
                    throw new ResourceException('O endereço selecionado não é atendido por esta loja.');
                }
            }

        });

        self::creating(function (Order $order) {
            $company = Company::findOrFail($order->company_id);
            $order->deliver_value = $company->deliver_value;
        });
//
        self::created(function (Order $order) {
            $conversation = Chat::createConversation("App.Order.$order->id", "Chat Pedido #$order->id", [
                'order_id' => $order->id
            ]);
            Chat::addParticipants($conversation->id, [
                $order->user_id,
                $order->company->responsible_id
            ]);

            $order->conversation_id = $conversation->id;
            $order->save();

            event(new OrderCreated($order));
        });

    }

    public function getTotalValue()
    {
        $this->updateVoucherValue();

        $items_value = $this->getItemsValue();

        if (!is_null($this->company->min_value_to_free_deliver) && $this->company->min_value_to_free_deliver <= $items_value) {
            $this->deliver_value = 0;
        } else {
            $this->deliver_value = $this->company->deliver_value;
        }
        $return = $items_value + $this->deliver_value - $this->voucher_discount_value;
        if ($return < 0) return 0;
        return round($return, 2);
    }

    public function updateVoucherValue()
    {
        if (!$this->voucher_id) {
            $this->voucher_discount_value = 0;
            return false;
        }
        /** @var Voucher $voucher */
        $voucher = Voucher::findOrFail($this->voucher_id);

        if ($voucher->data->offsetExists('voucher_discount_percentage')) {
            $this->voucher_discount_value = $this->getItemsValue() * (float)$voucher->data['voucher_discount_percentage'];
        } elseif ($voucher->data->offsetExists('voucher_discount_value')) {
            $this->voucher_discount_value = $voucher->data['voucher_discount_value'];
        }
        return $voucher;
    }

    public function getItemsValue()
    {
        $total = 0;
        foreach ($this->items as $item) {
            /** @var OrderItem $item */
            $total += $item->total_value;
        }
        return $total;
    }

    /**
     * @throws \Exception
     */
    public function getSplitRules()
    {
        $tax = Parameter::getByKey(Constants::PGM_RECEIVABLE_DEFAULT_TAX)->value_float;
        $fixed_tax = Parameter::getByKey(Constants::PGM_FIXED_TAX)->value_float; // ~R$1,20

        $bankRecipient = $this->company->responsible->bankRecipient;

        $value_mktplace = round(($this->getItemsValue() + $this->deliver_value) * (1 - $tax) - $fixed_tax, 2);
        $value_main = $this->getTotalValue() - $value_mktplace;
//
//        if (round($value_mktplace + $value_main, 2) !== $this->getTotalValue()) {
//            $diff = $this->getTotalValue() - $value_mktplace + $value_main;
//            $value_main += $diff;
//        }

        return [
            [
                'recipient_id' => Parameter::getByKey(Constants::PGM_RECEIVABLE_MAIN_ID)->value,
//                'percentage' => $tax,
                'amount' => number_format($value_main, 2, '', ''),
                'liable' => Parameter::getByKey(Constants::PGM_LIABLE_MAIN)->value_bool,
                'charge_processing_fee' => Parameter::getByKey(Constants::PGM_CHARGE_PROCESSING_FEE_MAIN)->value_bool,
            ],
            [
                'recipient_id' => $bankRecipient->pgm_recipient_id,
//                'percentage' => 100 - $tax,
                'amount' => number_format($value_mktplace, 2, '', ''),
                'liable' => Parameter::getByKey(Constants::PGM_LIABLE_MARKETPLACE)->value_bool,
                'charge_processing_fee' => Parameter::getByKey(Constants::PGM_CHARGE_PROCESSING_FEE_MARKETPLACE)->value_bool,
            ]
        ];

    }

    public function validate(array $rules = array())
    {
        $company = Company::findOrFail($this->company_id);

        if (!$company->canReceiverOrders()) {
            throw new ResourceException('Esta loja não está habilitada para receber pedidos');
        }

        return parent::validate($rules);
    }

    public function delete()
    {
        if ($this->status === self::STATUS_DRAFT) {
            return parent::delete();
        }
        throw new ResourceException('Não é possível excluir este pedido.');
    }

    public function updateRating($rating, $rating_description = null)
    {
        if ($this->status !== self::STATUS_ENTREGUE) {
            throw new ResourceException('Este pedido ainda não pode ser avaliado.');
        }
        $this->rating = $rating;
        $this->rating_description = $rating_description;
    }

    public function user()
    {
        return $this->belongsTo(config('rk-laravel.user.model', \Rockapps\RkLaravel\Models\User::class), 'user_id');
    }

    public function company()
    {
        return $this->belongsTo(config('rk-laravel.company.model', \Rockapps\RkLaravel\Models\Company::class), 'company_id');
    }

    public function items()
    {
        return $this->hasMany(config('rk-laravel.order_item.model', \Rockapps\RkLaravel\Models\OrderItem::class), 'order_id', 'id');
    }

    public function conditions()
    {
        return $this->hasMany(config('rk-laravel.order_condition.model', \Rockapps\RkLaravel\Models\OrderCondition::class), 'order_id', 'id');
    }

    public function conversation()
    {
        return $this->hasOne(Conversation::class, 'id', 'conversation_id');
    }

    public function voucher()
    {
        return $this->hasOne(Voucher::class, 'id', 'voucher_id');
    }

    public function credit_card()
    {
        return $this->hasOne(CreditCard::class, 'id', 'credit_card_id');
    }

    public function setDeliveredAtAttribute($value)
    {
        $this->attributes['delivered_at'] = $value ? Carbon::parse($value) : null;
    }

    public function setDeliverScheduledAtAttribute($value)
    {
        $this->attributes['deliver_scheduled_at'] = $value ? Carbon::parse($value) : null;
    }

    public function setApprovedAtAttribute($value)
    {
        $this->attributes['approved_at'] = $value ? Carbon::parse($value) : null;
    }

    public function setAskedAtAttribute($value)
    {
        $this->attributes['asked_at'] = $value ? Carbon::parse($value) : null;
    }

    public function setAddressZipcodeAttribute($value)
    {
        $this->attributes['address_zipcode'] = preg_replace('/[^0-9]/', '', $value);
    }

    protected function getGraph()
    {
        return 'order'; // the SM config to use
    }
}
