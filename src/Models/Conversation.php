<?php

namespace Rockapps\RkLaravel\Models;


use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Musonza\Chat\Models\Message;
use Musonza\Chat\Models\MessageNotification;
use Rockapps\RkLaravel\Helpers\Chat;
use Rockapps\RkLaravel\ModelFilters\ConversationFilter;
use Rockapps\RkLaravel\Traits\Like\Likeable;
use Rockapps\RkLaravel\Traits\Auditable;
use Rockapps\RkLaravel\Interfaces\CommentableInterface;
use Rockapps\RkLaravel\Traits\Commentable;
use Rockapps\RkLaravel\Traits\ModelTransform;

/**
 * Rockapps\RkLaravel\Models\Conversation
 *
 * @property int $id
 * @property int $private
 * @property string|null $data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Musonza\Chat\Models\Message|null $last_message
 * @property-read \Illuminate\Database\Eloquent\Collection|\Musonza\Chat\Models\Message[] $messages
 * @property-read int|null $messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Conversation filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Conversation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Conversation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Conversation paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Conversation query()
 * @method static \Illuminate\Database\Eloquent\Builder|Conversation simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Conversation whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Conversation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Conversation whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Conversation whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Conversation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Conversation whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|Conversation wherePrivate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Conversation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Conversation extends \Musonza\Chat\Models\Conversation implements \OwenIt\Auditing\Contracts\Auditable
{
    use Filterable, Auditable, ModelTransform;

    /**
     * @param null $channel
     * @param null $title
     * @param null $data
     * @param array $participants
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|Conversation|Conversation[]
     */
    public static function createConversation($channel = null, $title = null, $data = null, $participants = [])
    {
        $conversation = \Chat::createConversation($participants);

        $data['title'] = $title;
        $data['channel'] = $channel;
        $conversation->update(['data' => $data]);

        return self::findOrFail($conversation->id);
    }

    public function updateConversation($data)
    {
        /** @var \Musonza\Chat\Models\Conversation $conversation */
        $conversation = \Chat::conversations()->getById($this->id);
        $conversation->update(['data' => $data]);
    }

    public function sendMessage($message, $user_id)
    {
        return \Chat::message($message)
            ->from($user_id)
            ->to(\Chat::conversations()->getById($this->id))
            ->send();
    }

    public function addParticipants($participants)
    {
        $chat = \Chat::conversations()->getById($this->id);
        $chat->addParticipants($participants);
    }

    public function hasParticipant($user_id)
    {
        foreach (Chat::getParticipants($this->id) as $participant) {
            if($participant->id === $user_id) return true;
        }
        return false;
    }

    /**
     * Return the recent message in a Conversation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function last_message()
    {
        return $this->hasOne(ConversationMessage::class)->orderBy('mc_messages.id', 'desc')->with('sender');
    }

    /**
     * Messages in conversation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(ConversationMessage::class, 'conversation_id')->with('sender');
    }

    /**
     * @param $user
     * @return Collection|Conversation[]
     */
    public static function whereUserIsParticipant($user)
    {
        $ids = collect(\Chat::conversations()->setUser($user)->get()->items())->pluck('id');

        return Conversation::whereIn('id',$ids)->get();
    }

    /**
     * Get unread notifications.
     *
     * @param User $user
     *
     * @return Collection|MessageNotification[]
     */
    public function unReadNotifications($user)
    {
        return MessageNotification::query()
            ->where([
                ['user_id', '=', $user->getKey()],
                ['conversation_id', '=', $this->id],
                ['is_seen', '=', 0]
            ])->get();
    }

    /**
     * Get unread notifications count.
     *
     * @param User $user
     *
     * @return int
     */
    public function unReadNotificationsCount($user)
    {
        return MessageNotification::query()
            ->where([
                ['user_id', '=', $user->getKey()],
                ['conversation_id', '=', $this->id],
                ['is_seen', '=', 0]
            ])->count();
    }

    public function getParticipants()
    {
        return $this->users;
    }

    public function modelFilter()
    {
        return $this->provideFilter(ConversationFilter::class);
    }
}
