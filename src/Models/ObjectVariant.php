<?php

namespace Rockapps\RkLaravel\Models;

use EloquentFilter\Filterable;
use Iben\Statable\Statable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Auditable;
use Rockapps\RkLaravel\Traits\Favoritable;
use Rockapps\RkLaravel\Traits\HasAddresses;
use Rockapps\RkLaravel\Traits\HasCategories;
use Rockapps\RkLaravel\Traits\Rate\Rateable;
use Rockapps\RkLaravel\Transformers\ObjectVariantTransformer;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;


/**
 * Rockapps\RkLaravel\Models\ObjectVariant
 *
 * @property int $id
 * @property int $variant_id
 * @property string $object_type
 * @property int $object_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Address[] $addresses
 * @property-read int|null $addresses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Favorite[] $favorites
 * @property-read int|null $favorites_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Rating[] $ratings
 * @property-read int|null $ratings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Iben\Statable\Models\StateHistory[] $stateHistory
 * @property-read int|null $state_history_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Rockapps\RkLaravel\Models\Variant[] $variants
 * @property-read int|null $variants_count
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant filter($input = [], $filter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant newQuery()
 * @method static \Illuminate\Database\Query\Builder|ObjectVariant onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant paginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant query()
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant simplePaginateFilter($perPage = null, $columns = [], $pageName = 'page', $page = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant whereBeginsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant whereEndsWith($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant whereLike($column, $value, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant whereObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant whereObjectType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant whereVariantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant withAllCategories($categories)
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant withAnyCategories($categories)
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant withCategories($categories)
 * @method static \Illuminate\Database\Query\Builder|ObjectVariant withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant withoutAnyCategories()
 * @method static \Illuminate\Database\Eloquent\Builder|ObjectVariant withoutCategories($categories)
 * @method static \Illuminate\Database\Query\Builder|ObjectVariant withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $object
 */
class ObjectVariant extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable, HasMedia
{
    use SoftDeletes, Auditable, Notifiable, Statable, Filterable, HasAddresses, HasMediaTrait, Rateable, HasCategories, Favoritable;

    public $transformer = ObjectVariantTransformer::class;

    protected $table = 'object_variants';
    protected $attributes = [
    ];

    protected $casts = [
        'id' => 'int',
        'variant_id' => 'int',
    ];

    protected $dates = [
    ];

    protected $fillable = [
        'object_id',
        'object_type',
        'variant_id',
    ];
    protected $validationMessages = [
        'error.save' => 'Erro ao salvar a característica.',
        'error.not_found' => 'Característica não encontrada.',
    ];
    protected $rules = [
        'object_id' => 'required',
        'object_type' => 'required',
        'variant_id' => 'required',
    ];

    public static function boot()
    {
        parent::boot();
    }

    public function validate(array $rules = array())
    {
        return parent::validate($rules);
    }

    public function variants()
    {
        return $this->hasOne(Variant::class, 'id', 'variant_id');
    }

    public function object()
    {
        return $this->morphTo('object');
    }

}
