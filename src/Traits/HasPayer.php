<?php

namespace Rockapps\RkLaravel\Traits;

use Illuminate\Database\Eloquent\Collection;
use Rockapps\RkLaravel\Interfaces\PayerInterface;
use Rockapps\RkLaravel\Models\Address;

/**
 * Trait HasPayer
 * @package Rockapps\RkLaravel\Traits
 *
 * @property Collection|PayerInterface[] $payer
 */
trait HasPayer
{
    public function payer()
    {
        return $this->morphTo('payer');
    }
}
