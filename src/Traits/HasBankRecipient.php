<?php

namespace Rockapps\RkLaravel\Traits;

use Illuminate\Database\Eloquent\Relations\MorphOne;
use Rockapps\RkLaravel\Models\BankRecipient;
use Rockapps\RkLaravel\Models\BankTransfer;

/**
 * Trait HasBankRecipient
 * @package Rockapps\RkLaravel\Traits
 *
 * @property BankRecipient|MorphOne $bankRecipient
 *
 */
trait HasBankRecipient
{
    /**
     * @return mixed|BankRecipient|MorphOne
     */
    public function bankRecipient()
    {
        return $this->morphOne(BankRecipient::class, 'related');
    }

    public function bankTransfers()
    {
        return $this->morphMany(BankTransfer::class, 'transfered');
    }

}
