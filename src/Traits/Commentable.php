<?php

namespace Rockapps\RkLaravel\Traits;

use Rockapps\RkLaravel\Interfaces\CommenterInterface;
use Rockapps\RkLaravel\Models\Comment;

/**
 * Trait Commentable
 * @package Rockapps\RkLaravel\Traits
 *
 */
trait Commentable
{
    public function isCommentedBy(CommenterInterface $commenter)
    {
        return (bool)Comment::query()
            ->where('commentable_type', \get_class($this))
            ->where('commentable_id', $this->id)
            ->where('commenter_type', \get_class($commenter))
            ->where('commenter_id', $commenter->id)->get()->count();
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
