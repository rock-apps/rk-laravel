<?php

namespace Rockapps\RkLaravel\Traits;


use Illuminate\Support\Str;

trait ConsoleLogTrait
{
    public function logWarn($message)
    {
        $this->log('<fg=yellow>WARN</>', '<fg=yellow>' . $message . '</>');
    }

    public function log($type, $message)
    {
        $now = now();
        $message = $now->format('Y-m-d H:i:s') . ' [' . $type . '] ' . $message;
        echo $message . "\n";
        \Storage::disk('local')->append('/job_' . Str::slug(get_class($this)) . '.txt', $message);
    }

    public function logSuccess($message)
    {
        $this->log('<fg=green>SUCCESS</>', '<fg=green>✔️ ' . $message . '</>');
    }

    public function logInfo($message)
    {
        $this->log('INFO', $message);
    }

    public function logLine($message)
    {
        $this->log('>', $message);
    }

    public function logException(\Exception $exception)
    {
        $this->error($exception->getMessage());
        $this->log('<fg=red>EXCEPTION</>', '<fg=red>' . $exception->getFile() . ' -> linha: ' . $exception->getLine() . '</>');
        if ($this->dump_error) {
            dump($exception->getTraceAsString());
        }
    }

    public function logError($message)
    {
        $this->log('<fg=red>ERROR</>', '<fg=red>❌ ' . $message . '</>');
    }

}
