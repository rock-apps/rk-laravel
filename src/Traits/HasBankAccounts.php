<?php

namespace Rockapps\RkLaravel\Traits;

use Illuminate\Database\Eloquent\Collection;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\BankAccount;

/**
 * Trait HasBankAccounts
 * @package Rockapps\RkLaravel\Traits
 *
 * @property Collection|Address[] $address
 */
trait HasBankAccounts
{
    public function bankAccounts()
    {
        return $this->morphMany(BankAccount::class, 'owner');
    }

}
