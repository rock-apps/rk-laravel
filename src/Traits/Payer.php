<?php

namespace Rockapps\RkLaravel\Traits;

use Illuminate\Database\Eloquent\Collection;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\CreditCard;
use Rockapps\RkLaravel\Models\Payment;

/**
 * Trait Payer
 * @package Rockapps\RkLaravel\Traits
 *
 * @property Collection|CreditCard[] $creditCards
 * @property Collection|Payment[] $payments
 */
trait Payer
{
    public function canPay()
    {
        return (bool)$this->pgm_customer_id;
    }

    public function creditCards()
    {
        return $this->morphMany(CreditCard::class,'owner');
    }
    public function payments()
    {
        return $this->morphMany(Payment::class,'payer');
    }

}
