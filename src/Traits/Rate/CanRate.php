<?php

namespace Rockapps\RkLaravel\Traits\Rate;

use Rockapps\RkLaravel\Helpers\Rating as LaravelRating;
use Rockapps\RkLaravel\Models\Rating;

trait CanRate
{
    public function ratings()
    {
        return $this->morphMany(Rating::class, 'model');
    }

    public function rate($model, $value, $description = null)
    {
        return (new \Rockapps\RkLaravel\Helpers\Rating)->rate($this, $model, $value, $description);
    }

    public function getRatingValue($model)
    {
        return (new \Rockapps\RkLaravel\Helpers\Rating)->getRatingValue($this, $model);
    }

    public function isRated($model)
    {
        return (new \Rockapps\RkLaravel\Helpers\Rating)->isRated($this, $model);
    }

    public function rated()
    {
        return (new \Rockapps\RkLaravel\Helpers\Rating)->resolveRatedItems($this->ratings);
    }
}
