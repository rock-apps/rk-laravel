<?php

namespace Rockapps\RkLaravel\Traits;

use Rockapps\RkLaravel\Models\Configuration;

/**
 * Trait Settingable
 * @package Rockapps\RkLaravel\Traits
 *
 */
trait HasConfiguration
{
    public function saveConfiguration($data = [], $overwrite = false)
    {
        $configuration = $this->configuration()->first();
        if ($configuration) {
            if ($overwrite) {
                $configuration->configs = $data;
            } else {
                $configuration->configs = \array_merge($configuration->configs, $data);
            }
            $configuration->save();
        } else {
            $configuration = new Configuration();
            $configuration->configurable_type = \get_class($this);
            $configuration->configurable_id = $this->id;
            $configuration->configs = $data;
            $configuration->save();
        }

        return $configuration;
    }

    public function configuration()
    {
        return $this->morphOne(Configuration::class, 'configurable');
    }

    public function getConfigurationKey($key)
    {
        $configuration = $this->configuration()->first();
        if (!$configuration) {
            return null;
        }
        $configs = $configuration->configs;

        if (array_key_exists($key, $configs)) {
            return $configs[$key];
        }

        return null;
    }
}
