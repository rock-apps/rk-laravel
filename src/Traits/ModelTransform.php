<?php

namespace Rockapps\RkLaravel\Traits;

/**
 * Trait ModelTransform
 * @package Rockapps\RkLaravel\Traits
 *
 */
trait ModelTransform
{
    public function transform($transformer = null, $keep_data_index = false, $extra_data = [])
    {
        if (!$transformer) {
            if ($this->transformer) {
                $transformer = $this->transformer;
            } else {
                $elements = explode('\\', get_class($this));
                $transformer = '\\App\\Transformers\\' . $elements[2] . 'Transformer';
            }
        }


        if (!$transformer) {
            $transformer = $this->transformer;
        }

        $transformation = fractal($this, new $transformer($extra_data));
        $data = $transformation->toArray();
        if ($keep_data_index) {
            return $data;
        } else {
            return $data['data'];
        }
    }
}
