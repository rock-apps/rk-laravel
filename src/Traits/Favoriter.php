<?php

namespace Rockapps\RkLaravel\Traits;

use Illuminate\Database\Eloquent\Collection;
use Rockapps\RkLaravel\Interfaces\FavoritableInterface;
use Rockapps\RkLaravel\Models\CreditCard;
use Rockapps\RkLaravel\Models\Favorite;
use Rockapps\RkLaravel\Models\Payment;

/**
 * Trait Payer
 * @package Rockapps\RkLaravel\Traits
 *
 * @property Collection|CreditCard[] $creditCards
 * @property Collection|Payment[] $payments
 */
trait Favoriter
{
    public function favorite(FavoritableInterface $favoritable, bool $value = true)
    {
        $has_favorited = $this->hasFavorited($favoritable);

        $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);

        // Delete favorite
        if ($has_favorited && !$value) {
            return (bool) Favorite::query()
                ->where('favoritable_type', \get_class($favoritable))
                ->where('favoritable_id', $favoritable->id)
                ->where('favoriter_type', \get_class($this))
                ->where('favoriter_id', $this->id)
                ->delete();
        }

        // Do Favorite
        if (!$has_favorited && $value) {

            $favorited = new Favorite([
                'favoritable_type' => \get_class($favoritable),
                'favoritable_id' => $favoritable->id,
                'favoriter_type' => \get_class($this),
                'favoriter_id' => $this->id
            ]);
            return $favorited->save();
        }

        return true;
    }

    public function hasFavorited(FavoritableInterface $favoritable)
    {
        $favorited = Favorite::query()
            ->where('favoritable_type', \get_class($favoritable))
            ->where('favoritable_id', $favoritable->id)
            ->where('favoriter_type', \get_class($this))
            ->where('favoriter_id', $this->id)
            ->get()->first();

        return (bool)$favorited;
    }

    public function favorites()
    {
        return $this->morphMany(Favorite::class, 'favoriter');
    }

}
