<?php

namespace Rockapps\RkLaravel\Traits;

use Illuminate\Database\Eloquent\Collection;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\Payment;

/**
 * Trait HasPayment
 * @package Rockapps\RkLaravel\Traits
 *
 * @property Collection|Payment[] $payment
 */
trait HasPayment
{
    public function payment()
    {
        return $this->morphOne(Payment::class, 'purchaseable');
    }
}
