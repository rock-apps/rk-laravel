<?php


namespace Rockapps\RkLaravel\Traits\States;

use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeTransaction;
use Rockapps\RkLaravel\Models\Payment;

trait PaymentStateTrait
{
    public static $state_machine = [
        // class of your domain object
        'class' => Payment::class,

        // name of the graph (default is "default")
        'graph' => 'payment',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'status',

        // list of all possible states
        'states' => [
            Payment::STATUS_NOT_STARTED,
            Payment::STATUS_WAITING_PAYMENT,
            Payment::STATUS_PROCESSING,
            Payment::STATUS_AUTHORIZED,
            Payment::STATUS_PAID,
            Payment::STATUS_PENDING_REFUND,
            Payment::STATUS_REFUNDED,
            Payment::STATUS_REFUSED,
            Payment::STATUS_CANCELED
        ],

        // list of all possible transitions
        'transitions' => [
            Payment::STATUS_WAITING_PAYMENT => [
                'from' => [Payment::STATUS_PROCESSING, Payment::STATUS_NOT_STARTED],
                'to' => Payment::STATUS_WAITING_PAYMENT,
            ],
            Payment::STATUS_REFUNDED => [
                'from' => [Payment::STATUS_PAID, Payment::STATUS_PENDING_REFUND],
                'to' => Payment::STATUS_REFUNDED,
            ],
            Payment::STATUS_PROCESSING => [
                'from' => [Payment::STATUS_NOT_STARTED, Payment::STATUS_WAITING_PAYMENT],
                'to' => Payment::STATUS_PROCESSING,
            ],
            Payment::STATUS_PAID => [
                'from' => [Payment::STATUS_NOT_STARTED, Payment::STATUS_WAITING_PAYMENT, Payment::STATUS_PROCESSING],
                'to' => Payment::STATUS_PAID,
            ],
            Payment::STATUS_REFUSED => [
                'from' => [Payment::STATUS_NOT_STARTED, Payment::STATUS_WAITING_PAYMENT, Payment::STATUS_PROCESSING],
                'to' => Payment::STATUS_REFUSED,
            ],
            Payment::STATUS_CANCELED => [
                'from' => [
                    Payment::STATUS_WAITING_PAYMENT,
                    Payment::STATUS_PROCESSING,
                    Payment::STATUS_NOT_STARTED,
                    Payment::STATUS_PAID,
                ],
                'to' => Payment::STATUS_CANCELED,
            ],
        ],
        'callbacks' => [
            'after' => [
                'history' => [
                    'do' => '\Rockapps\RkLaravel\Helpers\StateHistoryManager@storeHistory',
                ],
            ],

            'before' => [
                'before_history' => [
                    'do' => '\Rockapps\RkLaravel\Helpers\StateHistoryManager@actionTransition',
                ],
            ],
        ],
    ];

    public function getNextStates()
    {
        $transitions = collect(self::$state_machine['transitions']);

        $next_states = [];
        foreach ($transitions as $transition => $values) {

            if (in_array($this->status, $values['from'])) {
                $label = null;
                switch ($values['to']) {
//                    case Payment::STATUS_NOT_STARTED:
//                    case Payment::STATUS_WAITING_PAYMENT:
//                    case Payment::STATUS_PROCESSING:
//                    case Payment::STATUS_AUTHORIZED:
//                    case Payment::STATUS_PENDING_REFUND:
//                    case Payment::STATUS_REFUSED:
                    case Payment::STATUS_PAID:
                        $label = 'Confirmar pagamento';
                        break;
                    case Payment::STATUS_REFUNDED:
                        $label = 'Estornar pagamento';
                        break;
                    case Payment::STATUS_CANCELED:
                        $label = 'Cancelar pagamento';
                        break;
                }
                if ($label) {
                    $next_states[] = [
                        'state' => $values['to'],
                        'label' => $label,
                    ];
                }
            }
        }

        return $next_states;
    }

    public function WAITING_PAYMENT()
    {
    }

    public function CANCELED()
    {
        if (!in_array($this->gateway, [self::GATEWAY_BANK_TRANSFER, self::GATEWAY_CASH, self::GATEWAY_FATURA])) {
            throw new ResourceException('Não é possível cancelar um pagamento de gateway externo. Utilize a opção de reembolso.');
        }
        $this->paid_at = null;
        $this->changePurchaseableStatus(false);

        if ($this->pgm_transaction_id) {
            if ($this->gateway === self::GATEWAY_PAGARME_CC && in_array($this->status, [self::STATUS_PROCESSING, self::STATUS_PAID])) {
                PagarMeTransaction::refundTransaction($this);
            }
        }
    }

    public function REFUNDED()
    {
        if (!in_array($this->gateway, [self::GATEWAY_PAGARME_CC])) {
            throw new ResourceException('Não é possível reembolsar um pagamento sem ser por cartão de crédito. Utilize a opção de cancelamento.');
        }
        $this->paid_at = null;
        $this->changePurchaseableStatus(false);

        if ($this->pgm_transaction_id) {

            if ($this->gateway === self::GATEWAY_PAGARME_CC && in_array($this->status, [self::STATUS_PROCESSING, self::STATUS_PAID])) {
                PagarMeTransaction::refundTransaction($this);
            }
        }
    }

    public function REFUSED()
    {
        $this->paid_at = null;

        if (config('rk-laravel.payment.trigger_events')) {
            $class = config('rk-laravel.payment.event_payment_refused');
            event(new $class($this));
        }

        $this->changePurchaseableStatus(false);
    }

    public function PAID()
    {
        $this->paid_at = now();

        if (config('rk-laravel.payment.trigger_events')) {
            $class = config('rk-laravel.payment.event_payment_paid');
            event(new $class($this));
        }

        $this->changePurchaseableStatus(true);
    }

    public function PROCESSING()
    {

    }

}
