<?php


namespace Rockapps\RkLaravel\Traits\States;

use Rockapps\RkLaravel\Interfaces\PurchaseableInterface;

trait PurchaseableStateTrait
{

    public static $state_machine = [
        // class of your domain object
        'class' => PurchaseableInterface::class,

        // name of the graph (default is "default")
        'graph' => 'purchaseable',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'status',

        // list of all possible states
        'states' => [
            PurchaseableInterface::STATUS_BLOCKED_PURCHASE,
            PurchaseableInterface::STATUS_CONFIRMED_PURCHASE,
            PurchaseableInterface::STATUS_PENDING_PURCHASE,
        ],

        // list of all possible transitions
        'transitions' => [
            PurchaseableInterface::STATUS_CONFIRMED_PURCHASE => [
                'from' => [PurchaseableInterface::STATUS_PENDING_PURCHASE, PurchaseableInterface::STATUS_BLOCKED_PURCHASE],
                'to' => PurchaseableInterface::STATUS_CONFIRMED_PURCHASE,
            ],
            PurchaseableInterface::STATUS_BLOCKED_PURCHASE => [
                'from' => [PurchaseableInterface::STATUS_PENDING_PURCHASE, PurchaseableInterface::STATUS_CONFIRMED_PURCHASE],
                'to' => PurchaseableInterface::STATUS_BLOCKED_PURCHASE,
            ],
        ],
        'callbacks' => [
            'after' => [
                'history' => [
                    'do' => '\Rockapps\RkLaravel\Helpers\StateHistoryManager@storeHistory',
                ],
            ],

            'before' => [
                'before_history' => [
                    'do' => '\Rockapps\RkLaravel\Helpers\StateHistoryManager@actionTransition',
                ],
            ],
        ],
    ];

}
