<?php


namespace Rockapps\RkLaravel\Traits\States;

use Rockapps\RkLaravel\Models\BankTransfer;

trait BankTransferStateTrait
{
    public static $state_machine = [
        // class of your domain object
        'class' => BankTransfer::class,

        // name of the graph (default is "default")
        'graph' => 'bank_transfer',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'status',

        // list of all possible states
        'states' => [
            BankTransfer::STATUS_REQUESTED,
            BankTransfer::STATUS_TRANSFERRED,
            BankTransfer::STATUS_PROCESSING,
            BankTransfer::STATUS_CANCELED,
            BankTransfer::STATUS_FAILED,
            BankTransfer::STATUS_PENDING_TRANSFER,
        ],

        // list of all possible transitions
        'transitions' => [
            BankTransfer::STATUS_TRANSFERRED => [
                'from' => [BankTransfer::STATUS_REQUESTED, BankTransfer::STATUS_PROCESSING, BankTransfer::STATUS_PENDING_TRANSFER],
                'to' => BankTransfer::STATUS_TRANSFERRED,
            ],
            BankTransfer::STATUS_PROCESSING => [
                'from' => [BankTransfer::STATUS_REQUESTED, BankTransfer::STATUS_PENDING_TRANSFER],
                'to' => BankTransfer::STATUS_PROCESSING,
            ],
            BankTransfer::STATUS_CANCELED => [
                'from' => [BankTransfer::STATUS_REQUESTED, BankTransfer::STATUS_PROCESSING, BankTransfer::STATUS_PENDING_TRANSFER],
                'to' => BankTransfer::STATUS_CANCELED,
            ],
            BankTransfer::STATUS_FAILED => [
                'from' => [BankTransfer::STATUS_REQUESTED, BankTransfer::STATUS_PROCESSING, BankTransfer::STATUS_PENDING_TRANSFER],
                'to' => BankTransfer::STATUS_FAILED,
            ],
        ],
        'callbacks' => [
            'after' => [
                'history' => [
                    'do' => '\Rockapps\RkLaravel\Helpers\StateHistoryManager@storeHistory',
                ],
            ],

            'before' => [
                'before_history' => [
                    'do' => '\Rockapps\RkLaravel\Helpers\StateHistoryManager@actionTransition',
                ],
            ],
        ],
    ];

    public function getNextStates()
    {
        $transitions = collect(self::$state_machine['transitions']);

        $next_states = [];
        foreach ($transitions as $transition => $values) {

            if (in_array($this->status, $values['from'])) {
                $label = null;
                switch ($values['to']) {

//                BankTransfer::STATUS_REQUESTED
//                BankTransfer::STATUS_PENDING_TRANSFER
//                BankTransfer::STATUS_TRANSFERRED
//                BankTransfer::STATUS_FAILED
//                BankTransfer::STATUS_PROCESSING
//                BankTransfer::STATUS_CANCELED

                    case BankTransfer::STATUS_PROCESSING:
                        $label = 'Em Processamento';
                        break;
                    case BankTransfer::STATUS_TRANSFERRED:
                        $label = 'Transferência Concluída';
                        break;
                    case BankTransfer::STATUS_CANCELED:
                        $label = 'Cancelar Transferência';
                        break;
                    case BankTransfer::STATUS_FAILED:
                        $label = 'Falha na Transferência';
                        break;
                }
                if ($label) {
                    $next_states[] = [
                        'state' => $values['to'],
                        'label' => $label,
                    ];
                }
            }
        }

        return $next_states;
    }

    public function CANCELED()
    {
        if (config('rk-laravel.bank_transfer.trigger_events')) {
            $class = config('rk-laravel.bank_transfer.event_bank_transfer_canceled');
            event(new $class($this));
        }
    }

    public function FAILED()
    {
        if (config('rk-laravel.bank_transfer.trigger_events')) {
            $class = config('rk-laravel.bank_transfer.event_bank_transfer_failed');
            event(new $class($this));
        }
    }

    public function PROCESSING()
    {
        if (config('rk-laravel.bank_transfer.trigger_events')) {
            $class = config('rk-laravel.bank_transfer.event_bank_transfer_processing');
            event(new $class($this));
        }
    }

    public function TRANSFERRED()
    {
        if (config('rk-laravel.bank_transfer.trigger_events')) {
            $class = config('rk-laravel.bank_transfer.event_bank_transfer_transferred');
            event(new $class($this));
        }

    }


}
