<?php

namespace Rockapps\RkLaravel\Traits\States;

use Rockapps\RkLaravel\Models\Order;
use Rockapps\RkLaravel\Models\OrderVirtualUnit;
use Rockapps\RkLaravel\Models\Payment;
use SM\Event\TransitionEvent;

trait OrderVirtualUnitStateTrait
{
    public static $state_machine = [
        'class' => OrderVirtualUnit::class,
        'graph' => 'order',
        'property_path' => 'status',
        'states' => [
            OrderVirtualUnit::STATUS_DRAFT,
            OrderVirtualUnit::STATUS_BLOCKED_PURCHASE,
            OrderVirtualUnit::STATUS_PENDING_PURCHASE,
            OrderVirtualUnit::STATUS_CONFIRMED_PURCHASE,
        ],

        // list of all possible transitions
        'transitions' => [
            OrderVirtualUnit::STATUS_PENDING_PURCHASE => [
                'from' => [OrderVirtualUnit::STATUS_DRAFT, OrderVirtualUnit::STATUS_PENDING_PURCHASE],
                'to' => OrderVirtualUnit::STATUS_PENDING_PURCHASE,
            ],
            OrderVirtualUnit::STATUS_CONFIRMED_PURCHASE => [ // Confirmou o pagamento - a transição vem do objeto payment
                'from' => [OrderVirtualUnit::STATUS_CONFIRMED_PURCHASE, OrderVirtualUnit::STATUS_PENDING_PURCHASE],
                'to' => OrderVirtualUnit::STATUS_CONFIRMED_PURCHASE,
            ],
            OrderVirtualUnit::STATUS_BLOCKED_PURCHASE => [
                'from' => [OrderVirtualUnit::STATUS_PENDING_PURCHASE],
                'to' => OrderVirtualUnit::STATUS_BLOCKED_PURCHASE
            ]
        ],
        'callbacks' => [
            'after' => [
//                'after_ready' => [
//                    'on' => 'READY',
//                    'do' => '\App\Models\Event@storeHistory',
//                ],
                'history' => [
                    'do' => self::class . '@storeHistory',
                ],
            ],
            'before' => [
                'before_history' => [
                    'do' => '\Rockapps\RkLaravel\Helpers\StateHistoryManager@actionTransition',
                ],
            ],
        ],
    ];

    public static function storeHistory(TransitionEvent $transitionEvent)
    {
        \Rockapps\RkLaravel\Helpers\StateHistoryManager::storeHistory($transitionEvent);
        $sm = $transitionEvent->getStateMachine();
        /** @var Order $order */
        $order = $sm->getObject();

        switch ($transitionEvent->getTransition()) {

            case OrderVirtualUnit::STATUS_PENDING_PURCHASE:
                $event = config('rk-laravel.order_virtual_unit.event_pending');
                $event ? event(new $event($order)) : null;
                break;
            case OrderVirtualUnit::STATUS_CONFIRMED_PURCHASE:
                $event = config('rk-laravel.order_virtual_unit.event_approved');
                $event ? event(new $event($order)) : null;
                break;
            case OrderVirtualUnit::STATUS_BLOCKED_PURCHASE:
                $event = config('rk-laravel.order_virtual_unit.event_canceled');
                $event ? event(new $event($order)) : null;
                break;
            default:
                break;

        }

    }

    public function getNextStates()
    {
        $transitions = collect(self::$state_machine['transitions']);

        $next_states = [];
        foreach ($transitions as $transition => $values) {

            if (in_array($this->status, $values['from'])) {
                $label = null;
                switch ($values['to']) {

                    case OrderVirtualUnit::STATUS_PENDING_PURCHASE:
                        $label = 'Pagamento Pendente';
                        break;
                    case OrderVirtualUnit::STATUS_CONFIRMED_PURCHASE:
                        $label = 'Pagamento Confirmado';
                        break;
                    case OrderVirtualUnit::STATUS_BLOCKED_PURCHASE:
                        $label = 'Compra Bloqueada (cancelada)';
                        break;
                }
                if ($label) {
                    $next_states[] = [
                        'state' => $values['to'],
                        'label' => $label,
                    ];
                }
            }
        }

        return $next_states;
    }

    public function isConfirmed()
    {
        return $this->status === OrderVirtualUnit::STATUS_CONFIRMED_PURCHASE;
    }

    public function DRAFT()
    {

    }

    public function BLOCKED_PURCHASE()
    {
    }

    public function PENDING_PURCHASE()
    {
    }

    public function CONFIRMED_PURCHASE()
    {
    }

    public function CANCELED()
    {
        /** @var Payment $payment */
        $payment = $this->payment;
        if ($payment) {
            if ($payment->isExternalGateway()) {
                $payment->REFUNDED();
            } else {
                $payment->CANCELED();
            }
        }
    }

}
