<?php


namespace Rockapps\RkLaravel\Traits\States;

use Rockapps\RkLaravel\Events\SubscriptionActivedManual;
use Rockapps\RkLaravel\Events\SubscriptionEnded;
use Rockapps\RkLaravel\Events\SubscriptionPaid;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeSubscription;
use Rockapps\RkLaravel\Models\Subscription;
use Rockapps\RkLaravel\Services\SubscriptionService;
use SM\Event\TransitionEvent;

trait SubscriptionStateTrait
{
    public static $state_machine = [
        // class of your domain object
        'class' => Subscription::class,

        // name of the graph (default is "default")
        'graph' => 'subscription',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'status',

        // list of all possible states
        'states' => [
            subscription::STATUS_NOT_STARTED,
            subscription::STATUS_ACTIVE_MANUAL,
            subscription::STATUS_TRIALING,
            subscription::STATUS_PAID,
            subscription::STATUS_PENDING_PAYMENT,
            subscription::STATUS_UNPAID,
            subscription::STATUS_CANCELED,
            subscription::STATUS_ENDED,
        ],

        // list of all possible transitions
        'transitions' => [
//            Subscription::STATUS_NOT_STARTED => [
//                'from' => [STATUS_NOT_STARTED, Subscription::STATUS_PENDING_REFUND],
//                'to' => Subscription::STATUS_REFUNDED,
//            ],
//            Subscription::STATUS_PROCESSING => [
//                'from' => [Subscription::STATUS_NOT_STARTED, Subscription::STATUS_WAITING_SUBSCRIPTION],
//                'to' => Subscription::STATUS_PROCESSING,
//            ],
            Subscription::STATUS_UNPAID => [
                'from' => [Subscription::STATUS_NOT_STARTED],
                'to' => Subscription::STATUS_UNPAID,
            ],
            Subscription::STATUS_TRIALING => [
                'from' => [Subscription::STATUS_NOT_STARTED],
                'to' => Subscription::STATUS_TRIALING,
            ],
            Subscription::STATUS_CANCELED => [
                'from' => [Subscription::STATUS_PAID, Subscription::STATUS_ACTIVE_MANUAL],
                'to' => Subscription::STATUS_CANCELED,
            ],
            Subscription::STATUS_PAID => [
                'from' => [Subscription::STATUS_NOT_STARTED, Subscription::STATUS_TRIALING, Subscription::STATUS_PENDING_PAYMENT, Subscription::STATUS_UNPAID],
                'to' => Subscription::STATUS_PAID,
            ],
            Subscription::STATUS_ACTIVE_MANUAL => [
                'from' => [
                    subscription::STATUS_NOT_STARTED,
                    Subscription::STATUS_ACTIVE_MANUAL,
                ],
                'to' => Subscription::STATUS_ACTIVE_MANUAL,
            ],
            Subscription::STATUS_ENDED => [
                'from' => [
                    subscription::STATUS_TRIALING,
                    subscription::STATUS_PAID,
                    Subscription::STATUS_ACTIVE_MANUAL,
                ],
                'to' => Subscription::STATUS_ENDED,
            ],
        ],
        'callbacks' => [
            'after' => [
                'history' => [
                    'do' => '\Rockapps\RkLaravel\Helpers\StateHistoryManager@storeHistory',
                ],

                Subscription::STATUS_CANCELED => [
                    'on' => Subscription::STATUS_CANCELED,
                    'do' => 'Rockapps\RkLaravel\Traits\States\SubscriptionStateTrait@afterCANCELED',
                    'args' => ['object', 'event'],
                ],
                Subscription::STATUS_ENDED => [
                    'on' => Subscription::STATUS_ENDED,
                    'do' => 'Rockapps\RkLaravel\Traits\States\SubscriptionStateTrait@afterENDED',
                    'args' => ['object', 'event'],
                ],
                Subscription::STATUS_PAID => [
                    'on' => Subscription::STATUS_PAID,
                    'do' => 'Rockapps\RkLaravel\Traits\States\SubscriptionStateTrait@afterPAID',
                    'args' => ['object', 'event'],
                ],
                Subscription::STATUS_ACTIVE_MANUAL => [
                    'on' => Subscription::STATUS_ACTIVE_MANUAL,
                    'do' => 'Rockapps\RkLaravel\Traits\States\SubscriptionStateTrait@afterACTIVE_MANUAL',
                    'args' => ['object', 'event'],
                ],
            ],
            'before' => [
                'before_history' => [
                    'do' => '\Rockapps\RkLaravel\Helpers\StateHistoryManager@actionTransition',
                ],
            ],
        ],
    ];

    public function getNextStates()
    {
        $transitions = collect(self::$state_machine['transitions']);

        $next_states = [];
        foreach ($transitions as $transition => $values) {

            if (in_array($this->status, $values['from'])) {
                $label = null;
                $helper = null;
                switch ($values['to']) {

                    case Subscription::STATUS_ACTIVE_MANUAL:
                        $label = 'Ativar Manualmente';
                        $helper = 'Ativa a assinatura manualmente, independente da situação do pagamento.';
                        break;
                    case Subscription::STATUS_PAID:
                        $label = 'Confirmar Pagamento';
                        $helper = 'Marca a assinatura como paga, independente da situação do pagamento. Utilize quando houve a confirmação do valor manualmente.';
                        break;
                    case Subscription::STATUS_ENDED:
                        $label = 'Encerrar';
                        $helper = 'Encerra o prazo da assinatura imediatamente.';
                        break;
                    case Subscription::STATUS_CANCELED:
                        $label = 'Cancelar com Estorno';
                        $helper = 'Cancela a assinatura, realizando o estorno do pagamento.';
                        break;
                }
                if ($label) {
                    $next_states[] = [
                        'state' => $values['to'],
                        'helper' => $helper,
                        'label' => $label,
                    ];
                }
            }
        }

        return $next_states;
    }
    /**
     * @param Subscription $subscription
     * @param TransitionEvent $event
     * @return void
     */
    public static function afterACTIVE_MANUAL(Subscription $subscription, TransitionEvent $event)
    {
        /** @var SubscriptionEnded $event */
        $event = config('rk-laravel.subscription.event_actived_manual', SubscriptionActivedManual::class);
        if ($event) event(new $event($subscription));
    }

    /**
     * @param Subscription $subscription
     * @param TransitionEvent $event
     * @return void
     */
    public static function afterCANCELED(Subscription $subscription, TransitionEvent $event)
    {
        /** @var SubscriptionEnded $event */
        $event = config('rk-laravel.subscription.event_ended', SubscriptionEnded::class);
        if ($event) event(new $event($subscription));
    }

    /**
     * @param Subscription $subscription
     * @param TransitionEvent $event
     * @return void
     */
    public static function afterENDED(Subscription $subscription, TransitionEvent $event)
    {
        /** @var SubscriptionEnded $event */
        $event = config('rk-laravel.subscription.event_ended', SubscriptionEnded::class);
        if ($event) event(new $event($subscription));
    }

    /**
     * @param Subscription $subscription
     * @param TransitionEvent $event
     * @return void
     */
    public static function afterPAID(Subscription $subscription, TransitionEvent $event)
    {
        /** @var SubscriptionEnded $event */
        $event = config('rk-laravel.subscription.event_paid', SubscriptionPaid::class);
        if ($event) event(new $event($subscription));
    }

    public function TRIALING()
    {

    }

    public function ACTIVE_MANUAL()
    {
        $this->renewed_at = now();
    }

    public function UNPAID()
    {

    }

    public function PAID()
    {
        $this->renewed_at = now();
    }

    public function CANCELED()
    {

        if ($this->gateway === Subscription::GATEWAY_PAGARME) {
            PagarMeSubscription::cancel($this);
        }

        if (!$this->current_period_end) $this->current_period_end = now();
        $this->expired_at = now();
    }

    public function ENDED()
    {
        $this->expired_at = now();
        $this->current_period_end = now();
    }

}
