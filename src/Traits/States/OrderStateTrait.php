<?php

namespace Rockapps\RkLaravel\Traits\States;

use Rockapps\RkLaravel\Events\OrderApproved;
use Rockapps\RkLaravel\Events\OrderCanceled;
use Rockapps\RkLaravel\Events\OrderDelivered;
use Rockapps\RkLaravel\Events\OrderPending;
use Rockapps\RkLaravel\Events\OrderRejected;
use Rockapps\RkLaravel\Interfaces\PurchaseableInterface;
use Rockapps\RkLaravel\Models\Order;
use SM\Event\TransitionEvent;

trait OrderStateTrait
{
    public static $state_machine = [
        'class' => Order::class,
        'graph' => 'order',
        'property_path' => 'status',
        'states' => [
            PurchaseableInterface::STATUS_DRAFT,
            Order::STATUS_PENDENTE,
            Order::STATUS_APROVADO,
            Order::STATUS_ENTREGUE,
            Order::STATUS_CANCELADO,
            PurchaseableInterface::STATUS_BLOCKED_PURCHASE,
            PurchaseableInterface::STATUS_CONFIRMED_PURCHASE,
            PurchaseableInterface::STATUS_PENDING_PURCHASE,
        ],

        // list of all possible transitions
        'transitions' => [

            PurchaseableInterface::STATUS_PENDING_PURCHASE => [
                'from' => [PurchaseableInterface::STATUS_DRAFT],
                'to' => PurchaseableInterface::STATUS_PENDING_PURCHASE,
            ],
            PurchaseableInterface::STATUS_CONFIRMED_PURCHASE => [
                'from' => [PurchaseableInterface::STATUS_PENDING_PURCHASE, PurchaseableInterface::STATUS_BLOCKED_PURCHASE, Order::STATUS_DRAFT],
                'to' => PurchaseableInterface::STATUS_CONFIRMED_PURCHASE,
            ],
            PurchaseableInterface::STATUS_BLOCKED_PURCHASE => [
                'from' => [PurchaseableInterface::STATUS_PENDING_PURCHASE, PurchaseableInterface::STATUS_CONFIRMED_PURCHASE],
                'to' => PurchaseableInterface::STATUS_BLOCKED_PURCHASE,
            ],

            PurchaseableInterface::STATUS_DRAFT => [
                'from' => [Order::STATUS_PENDENTE],
                'to' => Order::STATUS_DRAFT,
            ],
            Order::STATUS_PENDENTE => [
                'from' => [Order::STATUS_DRAFT,
//                    self::STATUS_BLOCKED_PURCHASE
                ],
                'to' => Order::STATUS_PENDENTE,
            ],
            Order::STATUS_APROVADO => [
                'from' => [Order::STATUS_PENDENTE],
                'to' => Order::STATUS_APROVADO,
            ],

//            self::STATUS_PAGO => [
//                'from' => [self::STATUS_APROVADO],
//                'to' => self::STATUS_PAGO,
//            ],
            Order::STATUS_ENTREGUE => [
                'from' => [
//                    self::STATUS_CONFIRMED_PURCHASE,
                    Order::STATUS_APROVADO
                ],
                'to' => Order::STATUS_ENTREGUE,
            ],
            Order::STATUS_CANCELADO => [
                'from' => [Order::STATUS_DRAFT, Order::STATUS_PENDENTE, Order::STATUS_APROVADO],
                'to' => Order::STATUS_CANCELADO,
            ],
        ],
        'callbacks' => [
            'after' => [
//                'after_ready' => [
//                    'on' => 'READY',
//                    'do' => '\App\Models\Event@storeHistory',
//                ],
                'history' => [
                    'do' => self::class . '@storeHistory',
                ],
            ],
            'before' => [
                'before_history' => [
                    'do' => '\Rockapps\RkLaravel\Helpers\StateHistoryManager@actionTransition',
                ],
            ],
        ],
    ];

    public static function storeHistory(TransitionEvent $transitionEvent)
    {
        \Rockapps\RkLaravel\Helpers\StateHistoryManager::storeHistory($transitionEvent);
        $sm = $transitionEvent->getStateMachine();
        /** @var Order $order */
        $order = $sm->getObject();

        switch ($transitionEvent->getTransition()) {

            case Order::STATUS_DRAFT:
                $original_status = $order->getOriginal('status');
                if ($original_status === Order::STATUS_PENDENTE) {
                    event(new OrderRejected($order));
                }
                break;
            case Order::STATUS_PENDENTE:
                event(new OrderPending($order));
                break;
            case Order::STATUS_APROVADO:
                event(new OrderApproved($order));
                break;

            // INCLUIR O PROXIMO STATE PENDING PAYMENT, DEPOIS INICIAR O PROCESSO DE PAGAMENTO OU RETORNAR PARA O USUARIO - SE FALHA
//                case Order::STATUS_PAGO:
//                    event(new OrderPaid($order));
//                    break;
            case Order::STATUS_CANCELADO:
                event(new OrderCanceled($order));
                break;
            case Order::STATUS_ENTREGUE:
                event(new OrderDelivered($order));
                break;
//                case Order::STATUS_EXPIRED:
//                    event(new OrderExpired($order));
//                    break;
            default:
                break;

        }

    }

//    protected function getGraph();

    public function isConfirmed()
    {
        return $this->status === self::STATUS_CONFIRMED_PURCHASE;
    }

    public function DRAFT()
    {

    }

    public function CONFIRMED_PURCHASE()
    {

    }

    public function PENDING_PURCHASE()
    {

    }

    public function BLOCKED_PURCHASE()
    {

    }

}
