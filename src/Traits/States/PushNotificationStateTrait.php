<?php


namespace Rockapps\RkLaravel\Traits\States;

use Rockapps\RkLaravel\Events\PushNotificationSending;
use Rockapps\RkLaravel\Models\PushNotification;

trait PushNotificationStateTrait
{
    public static $state_machine = [
        // class of your domain object
        'class' => PushNotification::class,

        // name of the graph (default is "default")
        'graph' => 'push_notification',

        // property of your object holding the actual state (default is "state")
        'property_path' => 'status',

        // list of all possible states
        'states' => [
            PushNotification::STATUS_FINISHED,
            PushNotification::STATUS_SCHEDULED,
            PushNotification::STATUS_SENDING,
        ],

        // list of all possible transitions
        'transitions' => [
            PushNotification::STATUS_SENDING => [
                'from' => [PushNotification::STATUS_SCHEDULED],
                'to' => PushNotification::STATUS_SENDING,
            ],
            PushNotification::STATUS_FINISHED => [
                'from' => [PushNotification::STATUS_SCHEDULED, PushNotification::STATUS_SENDING],
                'to' => PushNotification::STATUS_FINISHED,
            ],
        ],
        'callbacks' => [
            'after' => [
                'history' => [
                    'do' => '\Rockapps\RkLaravel\Helpers\StateHistoryManager@storeHistory',
                ],
            ],

            'before' => [
                'before_history' => [
                    'do' => '\Rockapps\RkLaravel\Helpers\StateHistoryManager@actionTransition',
                ],
            ],
        ],
    ];

    public function getNextStates()
    {
        $transitions = collect(self::$state_machine['transitions']);

        $next_states = [];
        foreach ($transitions as $transition => $values) {

            if (in_array($this->status, $values['from'])) {
                $label = null;
                switch ($values['to']) {
//                    case PushNotification::STATUS_FINISHED:
//                    case PushNotification::STATUS_SCHEDULED:
                    case PushNotification::STATUS_SENDING:
                        $label = 'Enviar push notification';
                        break;
                }
                if ($label) {
                    $next_states[] = [
                        'state' => $values['to'],
                        'label' => $label,
                    ];
                }
            }
        }

        return $next_states;
    }

    public function SENDING()
    {
        $this->started_at = now();
        event(new PushNotificationSending($this));
    }

    public function FINISHED()
    {
        $this->finished_at = now();
    }

}
