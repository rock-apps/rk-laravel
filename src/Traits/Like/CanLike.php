<?php

namespace Rockapps\RkLaravel\Traits\Like;

use Rockapps\RkLaravel\Helpers\Rating as LaravelRating;
use Rockapps\RkLaravel\Models\Rating;

trait CanLike
{
    public function likes()
    {
        return $this->morphMany(Rating::class, 'model');
    }

    public function like($model)
    {
        return (new \Rockapps\RkLaravel\Helpers\Rating)->rate($this, $model, 1);
    }

    public function dislike($model)
    {
        return (new \Rockapps\RkLaravel\Helpers\Rating)->rate($this, $model, 0);
    }

    public function isLiked($model)
    {
        return (new \Rockapps\RkLaravel\Helpers\Rating)->isRated($this, $model);
    }

    public function liked()
    {
        $collection = collect();

        $liked = $this->likes()->where('value', 1)->get();

        return (new \Rockapps\RkLaravel\Helpers\Rating)->resolveRatedItems($liked);
    }

    public function disliked()
    {
        $disliked = $this->likes()->where('value', 0)->get();

        return (new \Rockapps\RkLaravel\Helpers\Rating)->resolveRatedItems($disliked);
    }

    public function likedDisliked()
    {
        return (new \Rockapps\RkLaravel\Helpers\Rating)->resolveRatedItems($this->likes);
    }
}
