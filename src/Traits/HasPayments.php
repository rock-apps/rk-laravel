<?php

namespace Rockapps\RkLaravel\Traits;

use Illuminate\Database\Eloquent\Collection;
use Rockapps\RkLaravel\Models\Payment;

/**
 * Trait HasPayments
 * @package Rockapps\RkLaravel\Traits
 *
 * @property Collection|Payment[] $payment
 */
trait HasPayments
{
    public function payments()
    {
        return $this->morphMany(Payment::class, 'purchaseable');
    }
}
