<?php

namespace Rockapps\RkLaravel\Traits\Vote;

use Rockapps\RkLaravel\Helpers\Rating as LaravelRating;
use Rockapps\RkLaravel\Models\Rating;

trait CanVote
{
    public function votes()
    {
        return $this->morphMany(Rating::class, 'model');
    }

    public function upVote($model)
    {
        return (new \Rockapps\RkLaravel\Helpers\Rating)->rate($this, $model, 1);
    }

    public function downVote($model)
    {
        return (new \Rockapps\RkLaravel\Helpers\Rating)->rate($this, $model, 0);
    }

    public function isVoted($model)
    {
        return (new \Rockapps\RkLaravel\Helpers\Rating)->isRated($this, $model);
    }

    public function upVoted()
    {
        $upVoted = $this->votes()->where('value', 1)->get();

        return (new \Rockapps\RkLaravel\Helpers\Rating)->resolveRatedItems($upVoted);
    }

    public function downVoted()
    {
        $downVoted = $this->votes()->where('value', 0)->get();

        return (new \Rockapps\RkLaravel\Helpers\Rating)->resolveRatedItems($downVoted);
    }

    public function voted()
    {
        return (new \Rockapps\RkLaravel\Helpers\Rating)->resolveRatedItems($this->votes);
    }
}
