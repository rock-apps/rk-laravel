<?php

namespace Rockapps\RkLaravel\Traits;

use Illuminate\Database\Eloquent\Collection;
use Rockapps\RkLaravel\Interfaces\CommentableInterface;
use Rockapps\RkLaravel\Models\Comment;
use Rockapps\RkLaravel\Models\CreditCard;
use Rockapps\RkLaravel\Models\Payment;

/**
 * Trait Payer
 * @package Rockapps\RkLaravel\Traits
 *
 * @property Collection|CreditCard[] $creditCards
 * @property Collection|Payment[] $payments
 */
trait Commenter
{
    public function comment(CommentableInterface $commentable, $comment)
    {
        $comment = new Comment([
            'commentable_type' => \get_class($commentable),
            'commentable_id' => $commentable->id,
            'commenter_type' => \get_class($this),
            'commenter_id' => $this->id,
            'comment' => $comment
        ]);
        $comment->save();
        return $comment;
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commenter');
    }

}
