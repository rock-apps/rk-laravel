<?php

namespace Rockapps\RkLaravel\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Rockapps\RkLaravel\Models\Address;

/**
 * Trait HasAddress
 * @package Rockapps\RkLaravel\Traits
 *
 * @property Address|MorphOne $address
 */
trait HasAddress
{
    public function address()
    {
        return $this->morphOne(Address::class, 'related');
    }
}
