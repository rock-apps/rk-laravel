<?php

namespace Rockapps\RkLaravel\Traits;

use Illuminate\Database\Eloquent\Collection;
use Rockapps\RkLaravel\Models\Address;

/**
 * Trait HasAddresses
 * @package Rockapps\RkLaravel\Traits
 *
 * @property Collection|Address[] $address
 */
trait HasAddresses
{
    public function addresses()
    {
        return $this->morphMany(Address::class, 'related');
    }
}
