<?php

namespace Rockapps\RkLaravel\Traits\Api;

use Rockapps\RkLaravel\Api\Rate\RateSaveRequest;

trait RateableController
{
    public function rate(RateSaveRequest $request)
    {
        $user_class = config('config.user_model');
        $user = $user_class::firstOrFail(\Auth::getUser()->id);

        $model_type = $request->get('model_type');
        $model_id = $request->get('model_id');
        $value = $request->get('value');
        $description = $request->get('description');
        $model = $model_type::filter()->firstOrFail($model_id);

        return (new \Rockapps\RkLaravel\Helpers\Rating)->rate($user, $model, $value, $description);
    }

}
