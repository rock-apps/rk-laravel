<?php

namespace Rockapps\RkLaravel\Traits;


trait Auditable
{
    use \OwenIt\Auditing\Auditable;

    public static function isAuditingEnabled(): bool
    {
        if (php_sapi_name() === 'cli' || php_sapi_name() === 'phpdbg' || getenv('APP_ENV') === 'testing') {
            // Usado somente nos testes audit
            if (config('audit.force_in_test')) {
                return true;
            }
            return false;
        }

        if (\App::runningInConsole()) {
            return \Config::get('audit.console', false);
        }

        return \Config::get('audit.enabled', true);
    }
}
