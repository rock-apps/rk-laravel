<?php /** @noinspection PhpUndefinedFieldInspection */


namespace Rockapps\RkLaravel\Traits\Attributes;


use Illuminate\Support\Str;

trait BankTransferAttributesTrait
{

    public function getValueAsInt()
    {
        return number_format($this->value, 2, '', '');
    }

    public function setStatusAttribute($status)
    {
        $this->attributes['status'] = Str::upper($status);
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = round($value, 2);
    }

    public function getFeeAsInt()
    {
        return number_format($this->fee, 2, '', '');
    }

    public function setFeeAttribute($fee)
    {
        $this->attributes['fee'] = round($fee, 2);
    }

}
