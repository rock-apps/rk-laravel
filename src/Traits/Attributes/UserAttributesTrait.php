<?php /** @noinspection PhpUndefinedFieldInspection */


namespace Rockapps\RkLaravel\Traits\Attributes;


use Carbon\Carbon;
use Illuminate\Support\Str;

trait UserAttributesTrait
{
    public function setBirthDateAttribute($value)
    {
        $this->attributes['birth_date'] = Carbon::parse($value);
    }

    public function setDocumentAttribute($value)
    {
        $this->attributes['document'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function setMobileAttribute($value)
    {
        $this->attributes['mobile'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function setTelephoneAttribute($value)
    {
        $this->attributes['telephone'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function setGenderAttribute($value)
    {
        $this->attributes['gender'] = Str::upper($value);
    }

    public function setPhotoAttribute($value)
    {
//        if($this->attributes['photo']) {
//            Image::removeFromStorage($this->attributes['photo']);
//        }
        $this->attributes['photo'] = $value;
    }

    public function getFirstName()
    {
        if ($this->name)
            return explode(' ', $this->name)[0];
        return '';
    }
}
