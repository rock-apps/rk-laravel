<?php /** @noinspection PhpUndefinedFieldInspection */


namespace Rockapps\RkLaravel\Traits\Attributes;


use Carbon\Carbon;
use Illuminate\Support\Str;

trait PaymentAttributesTrait
{

    public function getValueAsInt()
    {
        return number_format($this->value, 2, '', '');
    }

    public function setStatusAttribute($status)
    {
        $this->attributes['status'] = Str::upper($status);
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = round($value, 2);
    }

}
