<?php


namespace Rockapps\RkLaravel\Traits\Attributes;

use Rockapps\RkLaravel\Interfaces\PurchaseableInterface;

trait PurchaseableAttributesTrait
{
    public function owner()
    {
        return $this->morphTo('owner');
    }
}
