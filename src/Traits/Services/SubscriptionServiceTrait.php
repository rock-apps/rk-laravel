<?php


namespace Rockapps\RkLaravel\Traits\Services;

use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Interfaces\PayerInterface;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\Plan;
use Rockapps\RkLaravel\Models\Subscription;

trait SubscriptionServiceTrait
{
    /**
     * @param Plan $plan
     * @param PayerInterface $payerBalanceChecker
     * @param PayerInterface $payer
     * @param $subscriber
     * @return Subscription
     * @throws \Illuminate\Container\EntryNotFoundException
     * @throws \SM\SMException
     */
    public static function createBalance(Plan $plan, $payerBalanceChecker, $payer, $subscriber)
    {
        \DB::beginTransaction();

        if ($plan->value > $payerBalanceChecker->getBalance()) {
            throw new ResourceException('Saldo suficiente para assinar este plano');
        }
        /** @var Subscription $subscription */
        $class = config('rk-laravel.subscription.model', Subscription::class);
        $subscription = new $class();
        $subscription->plan_id = $plan->id;
        $subscription->payment_method = Subscription::PAYMENT_METHOD_BALANCE;
        $subscription->mode = Subscription::MODE_SINGLE;
        $subscription->gateway = Subscription::GATEWAY_BALANCE;
        $subscription->payer_type = get_class($payer);
        $subscription->payer_id = $payer->id;
        $subscription->subscriber_type = get_class($subscriber);
        $subscription->subscriber_id = $subscriber->id;
        $subscription->current_period_start = now();
        $subscription->current_period_end = now()->addDays($plan->days);
        $subscription->save();
        $subscription->apply(Subscription::STATUS_PAID);
        $subscription->save();

        $payment = new Payment();
        $payment->gateway = Payment::GATEWAY_BALANCE;
        $payment->value = $plan->value;
        $payment->status = Payment::STATUS_PAID;
        $payment->async = false;
        $payment->payer_id = $payer->id;
        $payment->payer_type = get_class($payer);
        $payment->subscription_id = $subscription->id;
        $payment->direction = Payment::DIRECTION_INCOME;
        $payment->purchaseable_id = $subscription->id;
        $payment->purchaseable_type = Subscription::class;
        $payment->save();

        \DB::commit();
        $subscription->refresh();

        return $subscription;
    }

}
