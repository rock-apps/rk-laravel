<?php

namespace Rockapps\RkLaravel\Traits;

use Rockapps\RkLaravel\Interfaces\FavoriterInterface;
use Rockapps\RkLaravel\Models\Favorite;

/**
 * Trait Favoritable
 * @package Rockapps\RkLaravel\Traits
 *
 */
trait Favoritable
{
    public function isFavoritedBy(FavoriterInterface $favoriter = null)
    {
        if(!$favoriter) return false;
        return (bool) Favorite::query()
            ->where('favoritable_type', \get_class($this))
            ->where('favoritable_id', $this->id)
            ->where('favoriter_type', \get_class($favoriter))
            ->where('favoriter_id', $favoriter->id)->get()->count();
    }

    public function favorites()
    {
        return $this->morphMany(Favorite::class, 'favoritable');
    }
}
