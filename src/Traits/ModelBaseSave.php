<?php

namespace Rockapps\RkLaravel\Traits;

use Rockapps\RkLaravel\Exceptions\StoreResourceException;
use Rockapps\RkLaravel\Helpers\Sentry;
use Sentry\Severity;
use Watson\Validating\ValidatingTrait;

trait ModelBaseSave
{
    use ValidatingTrait;
    use ModelTransform;

    public static function getRulesRequest()
    {
        $self = new self();
        return \property_exists($self, 'rules') ? $self->rules : [];
    }

    public function save(array $options = [])
    {
        $validated = $this->validate();
        $errors = $this->getErrors();
        $parent_save = false;
        if ($validated) {
            $parent_save = parent::save($options);
        }

        if (!$validated || $errors->isNotEmpty() || $this->isInvalid() || !$parent_save) {

            if (\DB::transactionLevel()) {
                \DB::rollBack();
            }
            if (config('rk-laravel.sentry.log_validation_messages')) {
                Sentry::capture($this->validationMessages['error.save'], $this->getErrors(), Severity::DEBUG());
            }
            if (\App::environment('testing')) {
                dump($this->validationMessages['error.save'], $this->getErrors(), $options, $this->toArray());
            }
            if (\App::runningInConsole()) {
                dump($this->validationMessages['error.save'], $this->getErrors(), $options, $this->toArray());
            }
            throw new StoreResourceException($this->validationMessages['error.save'], $this->getErrors());
        }
        return true;
    }

    /**
     * Validate the model instance
     *
     * @param array $rules Validation rules
     * @return bool
     */
    public function validate(array $rules = array())
    {
        // check for overrides, then remove any empty rules
        $rules = (empty($rules)) ? $this->rules : $rules;

        foreach ($rules as $field => $rls) {
            if ($rls == '') {
                unset($rules[$field]);
            }
        }

        if (empty($rules)) {
            return true;
        } else {

            // perform validation
            $validator = \Validator::make($this->getAttributes(), $rules, $this->validationMessages);

            if ($validator->passes()) {
                // if the model is valid, unset old errors
//                if ($this->validationErrors === null || $this->validationErrors->count() > 0) {
//                    $this->validationErrors = new MessageBag;
//                }
                return true;
            } else {
                // otherwise set the new ones
                $this->validationErrors = $validator->messages();
                return false;
            }
        }

    }

}
