<?php

namespace Rockapps\RkLaravel\Traits;

use Illuminate\Database\Eloquent\Collection;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\CreditCard;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\Subscription;

/**
 * Trait Subscriber
 * @package Rockapps\RkLaravel\Traits
 *
 * @property Collection|CreditCard[] $creditCards
 * @property Collection|Payment[] $payments
 */
trait Subscriber
{
    public function subscriptions()
    {
        return $this->morphMany(Subscription::class,'subscriber');
    }

}
