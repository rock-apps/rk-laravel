<?php

namespace Rockapps\RkLaravel\Traits;

use Rockapps\RkLaravel\Events\VoucherRedeemed;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\Voucher;

trait CanRedeemVouchers
{
    /**
     * @param Voucher $voucher
     * @return mixed
     */
    public function redeemVoucher(Voucher $voucher)
    {
        return $this->redeemCode($voucher->code);
    }

    /**
     * @param string $code
     * @return mixed
     */
    public function redeemCode(string $code)
    {
        $voucher = Voucher::whereCode($code)->first();

        if ($voucher->availableQty() <= 0) {
            throw new ResourceException('Este código já atingiu o número máximo de utilizações.');
        }

        if ($voucher->users()->wherePivot('user_id', $this->id)->exists()) {
            throw new ResourceException('Este código já foi utilizado.');
        }
        if ($voucher->isExpired()) {
            throw new ResourceException('Este código já expirou.');
        }

        $this->vouchers()->attach($voucher, [
            'redeemed_at' => now()
        ]);
        $event = config('rk-laravel.voucher.event_redeemed', VoucherRedeemed::class);

        event(new $event($this, $voucher));

        return $voucher;
    }

    /**
     * @return mixed
     */
    public function vouchers()
    {
        return $this->belongsToMany(Voucher::class)->withPivot('redeemed_at');
    }
}
