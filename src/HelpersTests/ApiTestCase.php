<?php

namespace Rockapps\RkLaravel\HelpersTests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\User;
use Throwable;

abstract class ApiTestCase extends BaseTestCase
{
    use WithFaker;

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        throw new ResourceException('Sobrescrever esta função');

        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * @throws Throwable|\Throwable
     */
    public function tearDown(): void
    {
        $this->beforeApplicationDestroyed(function () {
            \DB::disconnect();
        });
        parent::tearDown();
    }

    public function jsonUser($method, $uri, array $data = [], $user = null)
    {
        $headers = $this->createAuthHeader($user);
        return $this->json($method, $uri, $data, $headers);
    }

    /**
     * Generate authentication headers
     *
     * @param User $user
     * @return array
     */
    public function createAuthHeader($user = null)
    {
        $headers = ['Accept' => 'application/json'];

        if (!is_null($user)) {
            if (is_array($user)) $user = User::findOrFail($user['id']);
            if (is_int($user)) $user = User::findOrFail($user);
            $token = \JWTAuth::fromUser($user);
            \JWTAuth::setToken($token);
            $headers['Authorization'] = 'Bearer ' . $token;
        }

        return $headers;
    }

    /**
     * Return request headers needed to interact with the API.
     *
     * @param null $user
     * @return array array of headers.
     */
    protected function headers($user = null)
    {
        $headers = ['Accept' => 'application/json'];

        if (!is_null($user)) {
            $token = \JWTAuth::fromUser($user);
            \JWTAuth::setToken($token);
            $headers['Authorization'] = 'Bearer ' . $token;
        }

        return $headers;
    }
}
