<?php

namespace Rockapps\RkLaravel\HelpersTests;

use Exception;
use Illuminate\Foundation\Testing\TestResponse;
use Rockapps\RkLaravel\Models\ModelBase;
use Rockapps\RkLaravel\Models\User;

trait ApiCrudTestTrait
{

    /**
     * The model to use when creating dummy data
     *
     * @var ModelBase
     */
    public $model;

    /**
     * The endpoint to query in the API
     * e.g = /api/v1/<endpoint>
     *
     * @var string
     */
    public $endpoint;

    /**
     * Extra data to pass to POST endpoint
     * aka the (store() method)
     *
     * Must be array (ends up merged with another)
     *
     * @var array
     */
    public $store;


    /**
     * Any additional "states" to add to factory
     *
     * @var string
     */
    public $states = null;

    /**
     * Any additional "data" to add to factory
     *
     * @var array
     */
    public $data = [];

    /**
     * GET /endpoint/
     * Should return 200 with data array
     *
     * @param User|null $user
     * @param int $status_code
     * @param string $querystring
     * @param null $expected_count
     * @param bool $force_dump
     * @return TestResponse
     */
    public function callIndex($user = null, $status_code = 200, $querystring = null, $expected_count = null, $force_dump = false)
    {
        if ($querystring) $querystring = '?' . http_build_query($querystring);
        $uri = "v1/{$this->endpoint}{$querystring}";
        $response = $this->jsonUser('GET', $uri, [], $user);
        $json = $response->decodeResponseJson();
        $this->analyzeJson($response, $force_dump);

        $response->assertStatus($status_code);

        if ($expected_count) {
            $this->assertEquals($expected_count, count($json['data']));
        }

        return $response;
    }

    public function analyzeJson(TestResponse $response, $dump_error = true)
    {
//        $json = $response->decodeResponseJson();
        if ($dump_error) {
            $response->dump();
//            exit;
        }
    }

    /**
     *
     * @param User|null $user
     * @param int $status_code
     * @param string $uri
     * @param null $querystring
     * @param null $expected_count
     * @param bool $force_dump
     * @return TestResponse
     */
    public function callIndexByUri($user = null, $status_code = 200, $uri = '', $querystring = null, $expected_count = null, $force_dump = false)
    {

        if ($querystring) $querystring = '?' . http_build_query($querystring);
        //parent::setUp();

        /** @var \Illuminate\Foundation\Testing\TestResponse $response */
        $response = $this->jsonUser('GET', $uri . $querystring, [], $user);

        $json = $response->decodeResponseJson();
        $this->analyzeJson($response, $force_dump);

        $response->assertStatus($status_code);

        if ($expected_count) {
            $this->assertEquals($expected_count, count($json['data']));
        }

        return $response;
    }

    /**
     * GET /endpoint/<id>
     * Should return 201 with data array
     *
     * @param User|null $user
     * @param int $status_code
     * @param bool $force_dump
     * @return TestResponse
     * @throws Exception
     */
    public function callShow($user = null, $status_code = 200, $force_dump = false)
    {
        //parent::setUp();
        // Create a test shop with filled out fields
        $model = $this->createModel();
        // Check the API for the new entry
        $uri = "v1/{$this->endpoint}/{$model->id}";
        $response = $this->jsonUser('GET', $uri, [], $user);
        // Delete the test shop
        $model->delete();
        $json = $response->decodeResponseJson();
        $this->analyzeJson($response, $force_dump);
        $response->assertStatus($status_code);
//        $response->assertJson([
//            'data' => true
//        ]);
        return $response;
    }

    /**
     * Uses the model factory to generate a fake entry
     *
     * @return ModelBase
     */
    public function createModel()
    {
        //parent::setUp();
        if ($this->states) {
            return factory($this->model)->states($this->states)->create($this->data);
        }

        return factory($this->model)->create($this->data);
    }

    /**
     * POST /endpoint/
     *
     * @param User|null $user
     * @param int $status_code
     * @param array $data
     * @param bool $dont_destroy
     * @return TestResponse
     */
    public function callStore($user = null, $status_code = 201, $data = [], $dont_destroy = false)
    {
        //parent::setUp();
        $model = $this->createModel();
        $model = $model->toArray();
        /**
         * Pass in any extra data
         */
        if ($data) {
            $model = array_merge($model, $data);
        } elseif ($this->store) {
            $model = array_merge($model, $this->store);
        }
        $response = $this->jsonUser('POST', "v1/{$this->endpoint}/", $model, $user);
        if (!$dont_destroy) {
            ($this->model)::destroy($model['id']);
        }

        $json = $response->decodeResponseJson();
        $this->analyzeJson($response, $status_code === 201);

        $response->assertStatus($status_code);
        return $response;
    }

    /**
     * POST /endpoint/
     *
     * @param User|null $user
     * @param int $status_code
     * @param string $uri
     * @param array $data
     * @param bool $force_dump
     * @return TestResponse
     */
    public function callStoreByUri($user = null, $status_code = 201, $uri = '', $data = [], $force_dump = false)
    {
        $response = $this->jsonUser('POST', $uri, $data, $user);

        $this->analyzeJson($response, $force_dump);

        $response->assertStatus($status_code);
        return $response;
    }

    /**
     * PUT /endpoint/
     *
     * @param User|null $user
     * @param int $status_code
     * @param string $uri
     * @param array $data
     * @param bool $force_dump
     * @return TestResponse
     */
    public function callUpdateByUri($user = null, $status_code = 200, $uri = '', $data = [], $force_dump = false)
    {
        //parent::setUp();
        $response = $this->jsonUser('PUT', $uri, $data, $user);

        $json = $response->decodeResponseJson();
        $this->analyzeJson($response, $force_dump);

        $response->assertStatus($status_code);
        return $response;
    }

    /**
     * PUT /endpoint/
     *
     * @param User|null $user
     * @param int $status_code
     * @param array $data
     * @param bool $dont_destroy
     * @param bool $force_dump
     * @return TestResponse
     */
    public function callUpdate($user = null, $status_code = 200, $data = [], $dont_destroy = false, $force_dump = false)
    {
        //parent::setUp();
        $model = $this->createModel();
        $model = $model->toArray();
        /**
         * Pass in any extra data
         */
        if ($data) {
            $model = array_merge($model, $data);
        } elseif ($this->store) {
            $model = array_merge($model, $this->store);
        }
        $response = $this->jsonUser('PUT', "v1/{$this->endpoint}/{$model['id']}", $model, $user);
        if (!$dont_destroy) {
            ($this->model)::destroy($model['id']);
        }
        $json = $response->decodeResponseJson();
        $this->analyzeJson($response, $force_dump);
        $response->assertStatus($status_code);

        if ($status_code === 200) {
            $response->assertJson([
                'data' => true
            ]);
        }
        return $response;
    }

    /**
     * DELETE /endpoint/<id>
     * Tests the destroy() method that deletes the shop
     *
     * @param User|null $user
     * @param int $status_code
     * @param bool $force_dump
     * @return TestResponse
     */
    public function callDestroy($user = null, $status_code = 200, $force_dump = false)
    {
        //parent::setUp();
        $model = $this->createModel();
        $response = $this->jsonUser('DELETE', "v1/{$this->endpoint}/{$model->id}", [], $user);
        $json = $response->decodeResponseJson();
        $this->analyzeJson($response, $force_dump);
        $response->assertStatus($status_code);
        return $response;
    }


    /**
     *
     * @param User|null $user
     * @param int $status_code
     * @param string $uri
     * @param bool $force_dump
     * @return TestResponse
     */
    public function callDestroyByUri($user = null, $status_code = 200, $uri = '', $force_dump = false)
    {
        $response = $this->jsonUser('DELETE', $uri, [], $user);
        $json = $response->decodeResponseJson();
        $this->analyzeJson($response, $force_dump);

        $response->assertStatus($status_code);

        return $response;
    }
}
