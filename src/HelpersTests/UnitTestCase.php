<?php

namespace Rockapps\RkLaravel\HelpersTests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Throwable;

abstract class UnitTestCase extends BaseTestCase
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        throw new ResourceException('Sobrescrever esta função');

        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * @throws Throwable
     */
    public function tearDown(): void
    {
        $this->beforeApplicationDestroyed(function () {
            \DB::disconnect();
        });
        parent::tearDown();
    }
}
