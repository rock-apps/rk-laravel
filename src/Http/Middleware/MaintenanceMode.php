<?php

namespace Rockapps\RkLaravel\Http\Middleware;

use Closure;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Services\MaintenanceModeCheckerService;

class MaintenanceMode
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var MaintenanceModeCheckerService $service */
        $service = config('rk-laravel.maintenance.service', MaintenanceModeCheckerService::class);

        if ($service::check()) {
            $msg = config('rk-laravel.maintenance.message', 'Desculpe, estamos em manutenção.');
            throw new ResourceException($msg);
        }

        return $next($request);
    }
}
