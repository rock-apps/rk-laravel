<?php

namespace Rockapps\RkLaravel\Http\Middleware;

use Closure;
use Rockapps\RkLaravel\Exceptions\AccessDeniedException;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserSignUpToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::getUser();
        if(!$user) {
            throw new AccessDeniedHttpException();
        }
        if ($user->signup_token) {
            throw new ResourceException(__('Você ainda não validou seu código de cadastro'));
        }

        return $next($request);
    }
}
