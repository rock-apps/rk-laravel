<?php

namespace Rockapps\RkLaravel\Http\Middleware;

use Closure;
use Rockapps\RkLaravel\Helpers\Sentry;
use Rockapps\RkLaravel\Services\SentryCaptureRequestService;

class SentryCaptureRequest
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        /** @var SentryCaptureRequestService $service */
        $service = config('rk-laravel.sentry.capture_request', SentryCaptureRequestService::class);

        if ($service::handle()) {
            Sentry::addContent(['request' => request()->all()]);
            Sentry::addContent(['headers' => request()->header()]);
            Sentry::capture('SentryCaptureRequest');
        }

        return $next($request);
    }
}
