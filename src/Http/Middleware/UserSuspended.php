<?php

namespace Rockapps\RkLaravel\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserSuspended
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::getUser();
        if ($user && $user->isSuspended()) {
            throw new AccessDeniedHttpException(__('auth.suspended'));
        }

        return $next($request);
    }
}
