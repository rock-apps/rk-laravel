<?php

namespace Rockapps\RkLaravel\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Rockapps\RkLaravel\Services\LogRequestService;

class LogRequestAfter
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        /** @var JsonResponse $response */
        $response = $next($request);

        /** @var LogRequestService $service */
        $service = config('rk-laravel.log_request.service', LogRequestService::class);
        if (!config('rk-laravel.log_request.enabled', false) || !$service::allow()) {
            return $response;
        }

        /** @var \Rockapps\RkLaravel\Models\LogRequest $log */
        $log = app('log-request');

        $log->fill([
            'response_code' => $response->getStatusCode(),
            'response_body' => method_exists($response, 'getData') ? $response->getData() : $response->content(),
            'duration' => microtime(true) - $log->start_microtime,
        ]);

        unset($log->start_microtime);

        $log->save();


        return $response;
    }
}
