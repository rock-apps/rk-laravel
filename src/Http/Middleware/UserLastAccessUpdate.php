<?php

namespace Rockapps\RkLaravel\Http\Middleware;

use Closure;

class UserLastAccessUpdate
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::getUser();
        if($user) {
            \DB::table('users')
                ->where('id', $user->id)
                ->update(['last_access_at' => now()->format('Y-m-d H:i:s')]);
        }

        return $next($request);
    }
}
