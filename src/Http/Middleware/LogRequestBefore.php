<?php

namespace Rockapps\RkLaravel\Http\Middleware;

use Closure;
use Rockapps\RkLaravel\Services\LogRequestService;

class LogRequestBefore
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var LogRequestService $service */
        $service = config('rk-laravel.log_request.service', LogRequestService::class);
        if (!config('rk-laravel.log_request.enabled', false) || !$service::allow()) {
            return $next($request);
        }

        /** @var \Rockapps\RkLaravel\Models\LogRequest $log */
        $log = app('log-request');

        $log->fill([
            'port' => $request->getPort(),
            'user_id' => \Auth::getUser() ? \Auth::getUser()->id : null,
            'level' => $service::level(),
            'url' => $request->url(),
            'method' => $request->getMethod(),
            'headers' => $request->headers->all(),
            'querystring' => $request->getQueryString(),
            'body' => $request->all(),
            'extra',
            'curl',
        ]);

        $log->save();
        $log->start_microtime = microtime(true); // Não salva na base, somente no objeto para cálculo

        return $next($request);
    }
}
