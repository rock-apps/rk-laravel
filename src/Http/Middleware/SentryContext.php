<?php

namespace Rockapps\RkLaravel\Http\Middleware;

use Closure;
use Rockapps\RkLaravel\Services\SentrySendUserToken;
use Sentry\Laravel\SentryHandler;
use Sentry\State\Scope;

class SentryContext
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (app()->bound('sentry')) {
            /** @var \Sentry\State\Hub $sentry */
            $hub = app('sentry');
            $handler = new SentryHandler($hub);
            $handler->setRelease(env('APP_VERSION', 'APP_VERSION_UNSET'));
            $handler->setEnvironment(env('APP_ENV'));

            // Add app version
            $hub->configureScope(function (Scope $scope) {
                if (request()->header('App-Version')) {
                    $scope->setExtras(['Mobile-App-Version' => request()->header('App-Version')]);
                }
                if (request()->header('App-Build-Number')) {
                    $scope->setExtras(['Mobile-App-Build-Number' => request()->header('App-Build-Number')]);
                }

                // Add user context
                if (\Auth::getUser()) {
                    $user = \Auth::getUser();

                    /** @var SentrySendUserToken $sentry_token_user */
                    $sentry_token_user = config('rk-laravel.sentry.send_user_token', SentrySendUserToken::class);
                    $scope->setUser([
                        'id' => $user->id,
                        'email' => $user->email,
                        'name' => $user->name,
                        'token' => $sentry_token_user::run($user) ? 'Bearer ' . \JWTAuth::getToken() : 'Ativar em SentrySendUserToken',
                    ]);
                }
            });


        }

        return $next($request);
    }
}
