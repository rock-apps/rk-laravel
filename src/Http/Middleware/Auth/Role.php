<?php

/*
 * This file is part of jwt-auth.
 *
 * (c) Sean Tymon <tymon148@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Rockapps\RkLaravel\Http\Middleware\Auth;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class Role
{
    const DELIMITER = '|';

    protected $auth;

    /**
     * Creates a new instance of the middleware.
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param  $roles
     * @return mixed
     * @throws AccessDeniedHttpException
     */
    public function handle($request, Closure $next, $roles)
    {
        if (!is_array($roles)) {
            $roles = explode(self::DELIMITER, $roles);
        }

        if ($this->auth->guest() || !$request->user()->hasRole($roles)) {
            $roles = \Rockapps\RkLaravel\Models\Role::whereIn('name', $roles)->get();
            $roles_array = [];
            foreach ($roles as $role) {
                $roles_array[] = $role->display_name;
            }
            $msg = sprintf('Você não possui perfil para acessar essas informações.');
            throw new ResourceException($msg, $roles_array);
        }

        return $next($request);
    }
}
