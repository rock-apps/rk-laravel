<?php

namespace Rockapps\RkLaravel\Http\Controllers\Auth;

use Rockapps\RkLaravel\Http\Controllers\BaseController;
use Rockapps\RkLaravel\Models\User;

class ActivateEmailController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the profile for the given user.
     *
     * @param $token
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($token)
    {

        /** @var User $user */
        $user = User::where('verification_token', $token)
            ->firstOrFail();

        $user->verified = 1;
        $user->save();
        return view('user.activateEmail', ['user' => $user]);
    }
}
