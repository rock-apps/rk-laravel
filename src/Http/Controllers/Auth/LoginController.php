<?php

namespace Rockapps\RkLaravel\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Rockapps\RkLaravel\Http\Controllers\BaseController;
use Rockapps\RkLaravel\Models\User;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class LoginController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Attempt to log the user into the application.
     *
     * @param  Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        $token = \Auth::guard('web')->attempt($credentials, $request->filled('remember'));

        if (!$token) {
            return false;
        }
        /** @var User $user */
        $user = User::where('email', $credentials['email'])->first();

        if ($user->isSuspended()) {
            throw new AccessDeniedHttpException(__('auth.suspended'));
        }

        \Auth::guard('web')->setUser($user);

        return true;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
