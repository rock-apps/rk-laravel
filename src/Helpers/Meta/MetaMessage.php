<?php

namespace Rockapps\RkLaravel\Helpers\Meta;

class MetaMessage
{
    public $title;
    public $class;
    public $body;
    public $type;

    public static function addFlash($title, $body ='', $class = 'info'): MetaMessage
    {
        return self::addMetaMessage($title, $body, $class, 'flash');
    }

    public static function addMetaMessage($title, $body ='', $class = 'info', $type = 'flash'): MetaMessage
    {
        $message = new MetaMessage();
        $message->title = $title;
        $message->body = $body;
        $message->class = $class;
        $message->type = $type;
        app('meta-bag')->addMessage($message);
        return $message;
    }

    public static function addPopup($title, $body ='', $class = 'info'): MetaMessage
    {
        return self::addMetaMessage($title, $body, $class, 'popup');
    }

    public static function addToast($title, $body ='', $class = 'info'): MetaMessage
    {
        return self::addMetaMessage($title, $body, $class, 'toast');
    }

    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'body' => $this->body,
            'class' => $this->class,
            'type' => $this->type,
        ];
    }
}
