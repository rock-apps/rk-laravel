<?php

namespace Rockapps\RkLaravel\Helpers\Meta;

class MetaBag
{
    /** @var \Illuminate\Support\Collection $messages */
    public $messages;

    public function __construct()
    {
        $this->messages = collect();
    }

    public function addMessage(MetaMessage $meta_message): MetaBag
    {
        $this->messages->add($meta_message);
        return $this;
    }

    public function toArray(): array
    {
        return $this->messages->map(function(MetaMessage $mm){
            return $mm->toArray();
        })->toArray();
    }

    public function clear()
    {
        $this->messages = collect();
    }
}
