<?php

namespace Rockapps\RkLaravel\Helpers;

use SM\Event\TransitionEvent;

class StateHistoryManager
{
    /**
     * @param \SM\Event\TransitionEvent $event
     */
    public static function storeHistory(TransitionEvent $event)
    {
        $sm = $event->getStateMachine();
        $model = $sm->getObject();

        $model->addHistoryLine([
            'transition' => $event->getTransition(),
            'from' => $event->getState(),
            'to' => $sm->getState(),
        ]);
    }
    public static function actionTransition(TransitionEvent $event)
    {
        $sm = $event->getStateMachine();
        $model = $sm->getObject();
        $transition = $event->getTransition();
        $model->$transition();
    }
}
