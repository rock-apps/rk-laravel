<?php

namespace Rockapps\RkLaravel\Helpers;

use Coderello\SocialGrant\Resolvers\SocialUserResolverInterface;
use Exception;
use Illuminate\Contracts\Auth\Authenticatable;
use Laravel\Socialite\Facades\Socialite;
use Rockapps\RkLaravel\Exceptions\IntegrationException;

class SocialUserResolver implements SocialUserResolverInterface
{

    /**
     * Resolve user by provider credentials.
     *
     * @param string $provider
     * @param string $accessToken
     *
     * @return Authenticatable|null
     */
    public function resolveUserByProviderCredentials(string $provider, string $accessToken): ?Authenticatable
    {
        $providerUser = null;

        try {
            $providerUser = Socialite::driver($provider)->userFromToken($accessToken);
        } catch (Exception $exception) {
            throw new IntegrationException('Não foi possível autenticar com o provedor ' . $provider, null, null, $exception);
        }

        if ($providerUser) {
            return (new SocialAccountService())->findOrCreate($providerUser, $provider);
        }

        return null;
    }
}
