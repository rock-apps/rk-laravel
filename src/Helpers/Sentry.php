<?php

namespace Rockapps\RkLaravel\Helpers;

use Sentry\Severity;
use Sentry\State\Hub;
use Sentry\State\Scope;

class Sentry
{
    public static function capture($message, $data = [], $severity = null)
    {
        /** @var Hub $hub */
        $hub = app('sentry');

        self::addContent($data);

        if (!$severity) {
            $severity = Severity::DEBUG();
        }

        $envs = config('rk-laravel.sentry.log_environments', ['production', 'homolog']);
        if (in_array(env('APP_ENV'), $envs)) {
            return $hub->captureMessage($message, $severity);
        }

        return null;
    }

    public static function addContent($data)
    {
        /** @var Hub $hub */
        $hub = app('sentry');

        $hub->configureScope(function (Scope $scope) use ($data) {

            if (is_array($data)) {
                foreach ($data as $k => $v) {
                    $scope->setExtra($k, $v);
                }
            } elseif ($data) {
                $scope->setExtra('extra_content', $data);
            }
        });

    }
}
