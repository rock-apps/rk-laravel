<?php

namespace Rockapps\RkLaravel\Helpers;

use Spatie\MediaLibrary\Models\Media;

class MediaPathGenerator implements \Spatie\MediaLibrary\PathGenerator\PathGenerator
{
    public function getPathForConversions(Media $media): string
    {
        return $this->getPath($media) . 'conversions/';
    }

    public function getPath(\Spatie\MediaLibrary\Models\Media $media): string
    {
        return ClassManipulation::extractClassSlugName($media->model) . '/' . $media->id . '/';
    }

    public function getPathForResponsiveImages(Media $media): string
    {
        return $this->getPath($media) . 'responsive/';
    }
}
