<?php

namespace Rockapps\RkLaravel\Helpers\Iap;

use Rockapps\RkLaravel\Exceptions\IntegrationException;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\Sentry;
use Rockapps\RkLaravel\Models\Payment;


class GooglePayment
{
    public static function sync(Payment $payment)
    {
        $validator = self::initApi();

//        try {
//            $response = $validator->setReceiptData($payment->iap_receipt_id)->validate();
//            if ($response->isValid()) {
//
//                foreach ($response->getPurchases() as $purchase) {
//
//                    /** @var Payment $payment_child */
//                    $payment_child = Payment::where('apple_transaction_id', $purchase->getTransactionId())->first();
//                    if ($payment_child) {
//                        $payment_child->current_period_start = $purchase->getPurchaseDate()->subHours(3)->format('Y-m-d H:i:s');
//                        $payment_child->current_period_end = $purchase->getExpiresDate()->subHours(3)->format('Y-m-d H:i:s');
//                        $payment_child->save();
//
//                        if (now() > $purchase->getExpiresDate() && $payment_child->status !== Payment::STATUS_ENDED) {
//                            $payment_child->apply(Payment::STATUS_ENDED);
//                            $payment_child->save();
//                        }
//                    }
//
//                }
//
//            } else {
//                throw new IntegrationException("O código do recibo IAP iOS não é válido. Código " . $response->getResultCode() . ".", $payment->toArray());
//            }
//        } catch (\Exception $e) {
//            throw new IntegrationException('Erro ao confirmar dados na Apple. ' . $e->getMessage(), $payment->toArray(), null, $e);
//        }

        return $payment;
    }

    /**
     * @return \ReceiptValidator\GooglePlay\Validator
     * @throws \Google\Exception
     */
    public static function initApi()
    {
        $pathToServiceAccountJsonFile = env('GOOGLE_IAP_APPLICATION_CREDENTIALS');
        if (!$pathToServiceAccountJsonFile) {
            $pathToServiceAccountJsonFile = env('GOOGLE_APPLICATION_CREDENTIALS');
        }
        if (!$pathToServiceAccountJsonFile) {
            throw new ResourceException('GOOGLE_IAP_APPLICATION_CREDENTIALS ou GOOGLE_APPLICATION_CREDENTIALS não definido');
        }
        if (!env('GOOGLE_IAP_PACKAGE_NAME')) {
            throw new ResourceException('GOOGLE_IAP_APPLICATION_CREDENTIALS ou GOOGLE_APPLICATION_CREDENTIALS não definido');
        }

        $googleClient = new \Google_Client();
        $googleClient->setScopes([\Google_Service_AndroidPublisher::ANDROIDPUBLISHER]);
        $googleClient->setApplicationName('Your_Purchase_Validator_Name');
        $googleClient->setAuthConfig($pathToServiceAccountJsonFile);

        $googleAndroidPublisher = new \Google_Service_AndroidPublisher($googleClient);
        return new \ReceiptValidator\GooglePlay\Validator($googleAndroidPublisher);

    }

    /**
     * @param Payment $payment
     * @return Payment
     * @throws \Illuminate\Container\EntryNotFoundException
     * @throws \SM\SMException|\Google\Exception
     * @noinspection PhpUndefinedClassInspection
     */
    public static function create(Payment $payment)
    {
        if (!$payment->iap_receipt_id) {
            throw new IntegrationException('A propriedade iap_receipt_id é obrigatória');
        }

        if (Payment::whereIapReceiptId($payment->iap_receipt_id)->first()) {
            throw new ResourceException('Este pagamento já foi validado');
        }

        if (!$payment->product) throw new ResourceException('É necessário vincular o produto em pagamento por IAP');
        if (!$payment->product->active) throw new ResourceException('Produto inativo');
        if (!$payment->product->google_product_id) throw new ResourceException('Produto não cadastrado para Android');
        if (!$payment->product->active_android) throw new ResourceException('Produto inativo para Android');

        \DB::beginTransaction();

        $validator = self::initApi();

        try {
            $response = $validator->setPackageName(env('GOOGLE_IAP_PACKAGE_NAME'))
                ->setProductId($payment->iap_product_id)
                ->setPurchaseToken($payment->iap_receipt_id)
                ->validatePurchase();
        } catch (\Exception $e) {
            $json_error = json_decode($e->getMessage(), true);
            $msg = 'Erro ao confirmar dados no Google.';
            if ($json_error && array_key_exists('error', $json_error)) {
                $msg .= ' ' . $json_error['error']['message'];
            }
            Sentry::capture($msg, ['payment' => $payment->toArray(), 'error' => $json_error]);
            throw new \Rockapps\RkLaravel\Exceptions\ResourceException($msg);
        }
        /** @var Google_Service_AndroidPublisher_ProductPurchase $raw */
        $raw = $response->getRawResponse();

        $payment->google_order_id = $raw->getOrderId();
        $payment->iap_product_id = $raw->getProductId();
//        $payment->value = $raw->getPriceAmountMicros();
        $payment->save();

        $payment->apply(Payment::STATUS_PAID);
        $payment->paid_at = now();
        $payment->save();

        \DB::commit();

        return $payment;
    }
}
