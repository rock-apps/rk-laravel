<?php

namespace Rockapps\RkLaravel\Helpers\Iap;

use Carbon\Carbon;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Interfaces\PayerInterface;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\Plan;
use Rockapps\RkLaravel\Models\Subscription;


class GoogleSubscription
{
    /**
     * @param Subscription $subscription
     * @return Subscription
     * @throws \Google\Exception
     */
    public static function sync(Subscription $subscription)
    {
        $validator = self::initApi();

        try {

            $response = $validator
                ->setProductId($subscription->plan->google_product_id)
                ->setPurchaseToken($subscription->iap_receipt_id)
                ->validateSubscription();

        } catch (\Exception $e) {
            throw new \Rockapps\RkLaravel\Exceptions\ResourceException('Erro ao confirmar dados no Google. ' . $e->getMessage());
        }

        $subscription->current_period_start = Carbon::createFromTimestampMs($response->getStartTimeMillis())->format('Y-m-d H:i:s');
        $subscription->current_period_end = Carbon::createFromTimestampMs($response->getExpiryTimeMillis())->format('Y-m-d H:i:s');
        $subscription->save();

        if ($subscription->current_period_end->isPast()) {
            $subscription->apply(Subscription::STATUS_ENDED);
            $subscription->save();
        }

        \DB::commit();

        return $subscription;
    }

    /**
     * @return \ReceiptValidator\GooglePlay\Validator
     * @throws \Google\Exception
     */
    public static function initApi()
    {
        $pathToServiceAccountJsonFile = env('GOOGLE_IAP_APPLICATION_CREDENTIALS');
        if (!$pathToServiceAccountJsonFile) {
            $pathToServiceAccountJsonFile = env('GOOGLE_APPLICATION_CREDENTIALS');
        }
        if (!$pathToServiceAccountJsonFile) {
            throw new ResourceException('GOOGLE_IAP_APPLICATION_CREDENTIALS ou GOOGLE_APPLICATION_CREDENTIALS não definido');
        }
        if (!env('GOOGLE_IAP_PACKAGE_NAME')) {
            throw new ResourceException('GOOGLE_IAP_APPLICATION_CREDENTIALS ou GOOGLE_APPLICATION_CREDENTIALS não definido');
        }

        $googleClient = new \Google_Client();
        $googleClient->setScopes([\Google_Service_AndroidPublisher::ANDROIDPUBLISHER]);
        $googleClient->setApplicationName('Your_Purchase_Validator_Name'); # Todo: Verificar se isso ta certo em vários arquivos
        $googleClient->setAuthConfig($pathToServiceAccountJsonFile);

        $googleAndroidPublisher = new \Google_Service_AndroidPublisher($googleClient);
        return (new \ReceiptValidator\GooglePlay\Validator($googleAndroidPublisher))
            ->setPackageName(env('GOOGLE_IAP_PACKAGE_NAME'));

    }

    /**
     * @param $iap_receipt_id_b64_data
     * @param PayerInterface $payer
     * @param $subscriber
     * @param Plan $plan
     * @return Subscription
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Container\EntryNotFoundException
     * @throws \SM\SMException
     * @noinspection PhpUndefinedClassInspection
     */
    public static function create($iap_receipt_id_b64_data, $payer, $subscriber, $plan)
    {
        \DB::beginTransaction();

        $validator = self::initApi();

        if (Subscription::where('iap_receipt_id', $iap_receipt_id_b64_data)->count()) {
            throw new ResourceException('Essa assinatura já foi verificada anteriormente.');
        }

        try {
            $response = $validator
                ->setProductId($plan->google_product_id)
                ->setPurchaseToken($iap_receipt_id_b64_data)
                ->validateSubscription();

        } catch (\Exception $e) {
            throw new \Rockapps\RkLaravel\Exceptions\ResourceException('Erro ao confirmar dados no Google. ' . $e->getMessage());
        }

//        dump($response->getRawResponse());
//        dump($response);

        $transaction_id = $response->getRawResponse()->getOrderId();

        if (Subscription::where('android_transaction_id', $transaction_id)->count()) {
            throw new ResourceException('Essa assinatura já foi criada anteriormente.');
        }

        /** @var Subscription $subscription */
        $class = config('rk-laravel.subscription.model', Subscription::class);
        $subscription = new $class();
        $subscription->plan_id = $plan->id;
        $subscription->payment_method = Subscription::PAYMENT_METHOD_IAP;
        $subscription->iap_receipt_id = $iap_receipt_id_b64_data;
        $subscription->android_transaction_id = $transaction_id;
        $subscription->mode = $response->getAutoRenewing() ? Subscription::MODE_RECURRENT : Subscription::MODE_SINGLE;
        $subscription->gateway = Subscription::GATEWAY_GOOGLE;
        $subscription->payer_type = get_class($payer);
        $subscription->payer_id = $payer->id;
        $subscription->subscriber_type = get_class($subscriber);
        $subscription->subscriber_id = $subscriber->id;
        $subscription->current_period_start = Carbon::createFromTimestampMs($response->getStartTimeMillis())->format('Y-m-d H:i:s');
        $subscription->current_period_end = Carbon::createFromTimestampMs($response->getExpiryTimeMillis())->format('Y-m-d H:i:s');
        $subscription->save();
        $subscription->apply(Subscription::STATUS_PAID);
        $subscription->save();

        \DB::commit();

        return $subscription;
    }
}
