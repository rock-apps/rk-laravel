<?php

namespace Rockapps\RkLaravel\Helpers\Iap;

use Rockapps\RkLaravel\Exceptions\IntegrationException;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Interfaces\PayerInterface;
use Rockapps\RkLaravel\Models\Plan;
use Rockapps\RkLaravel\Models\Subscription;

class IosSubscription
{
    public static function sync(Subscription $subscription)
    {
        $validator = self::initApi();

        try {
            $response = $validator->setReceiptData($subscription->iap_receipt_id)->validate();
            if ($response->isValid()) {

                foreach ($response->getPurchases() as $purchase) {

                    /** @var Subscription $subscription_child */
                    $subscription_child = Subscription::where('apple_transaction_id', $purchase->getTransactionId())->first();
                    if ($subscription_child) {
                        $subscription_child->current_period_start = $purchase->getPurchaseDate()->subHours(3)->format('Y-m-d H:i:s');
                        $subscription_child->current_period_end = $purchase->getExpiresDate()->subHours(3)->format('Y-m-d H:i:s');
                        $subscription_child->save();

                        if (now() > $purchase->getExpiresDate() && $subscription_child->status !== Subscription::STATUS_ENDED) {
                            $subscription_child->apply(Subscription::STATUS_ENDED);
                            $subscription_child->save();
                        }
                    }

                }

            } else {
                throw new IntegrationException("O código do recibo IAP iOS não é válido. Código " . $response->getResultCode() . ".", $subscription->toArray());
            }
        } catch (\Exception $e) {
            throw new IntegrationException('Erro ao confirmar dados na Apple. ' . $e->getMessage(), $subscription->toArray(), null, $e);
        }

        return $subscription;
    }

    /**
     * @return \ReceiptValidator\iTunes\Validator
     */
    public static function initApi()
    {
        $sharedSecret = env('APPLE_IAP_SECRET');
        if (!$sharedSecret) {
            throw new ResourceException('APPLE_IAP_SECRET não definido');
        }

        $sandbox = env('APPLE_IAP_SANDBOX', true);

        if ($sandbox) {
            $validator = new \ReceiptValidator\iTunes\Validator(\ReceiptValidator\iTunes\Validator::ENDPOINT_SANDBOX);
        } else {
            $validator = new \ReceiptValidator\iTunes\Validator(\ReceiptValidator\iTunes\Validator::ENDPOINT_PRODUCTION);
        }
        $validator->setSharedSecret($sharedSecret);

        return $validator;
    }

    /**
     * @param $iap_receipt_id_b64_data
     * @param PayerInterface $payer
     * @param $subscriber
     * @param $apple_transaction_id
     * @return bool|Subscription
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Container\EntryNotFoundException
     * @throws \SM\SMException
     * @noinspection PhpUndefinedClassInspection
     */
    public static function create($iap_receipt_id_b64_data, $payer, $subscriber, $apple_transaction_id)
    {
        \DB::beginTransaction();

        $validator = self::initApi();

        try {
            $response = $validator->setReceiptData($iap_receipt_id_b64_data)->validate();
        } catch (\Exception $e) {
            throw new \Rockapps\RkLaravel\Exceptions\ResourceException('Erro ao confirmar dados na Apple. ' . $e->getMessage());
        }

        if (!$response->isValid()) {
            throw new \Rockapps\RkLaravel\Exceptions\ResourceException("O código do recibo IAP iOS não é válido. Código " . $response->getResultCode() . ".");
        }

        $subscription = false;
        foreach ($response->getPurchases() as $purchase) {

            /** @var Plan $plan */
            $plan = Plan::filter()->whereAppleProductId($purchase->getProductId())->first();
            $subscription = Subscription::where('apple_transaction_id', $purchase->getTransactionId())->first();
            if ($plan && !$subscription && $purchase->getExpiresDate()) {
                /** @var Subscription $subscription */
                $class = config('rk-laravel.subscription.model', Subscription::class);
                $subscription = new $class();
                $subscription->plan_id = $plan->id;
                $subscription->payment_method = Subscription::PAYMENT_METHOD_IAP;
                $subscription->iap_receipt_id = $iap_receipt_id_b64_data;
                $subscription->apple_transaction_id = $purchase->getTransactionId();
                $subscription->mode = Subscription::MODE_RECURRENT;
                $subscription->gateway = Subscription::GATEWAY_APPLE;
                $subscription->payer_type = get_class($payer);
                $subscription->payer_id = $payer->id;
                $subscription->subscriber_type = get_class($subscriber);
                $subscription->subscriber_id = $subscriber->id;
                $subscription->current_period_start = $purchase->getPurchaseDate()->subHours(3)->format('Y-m-d H:i:s');
                $subscription->current_period_end = $purchase->getExpiresDate()->subHours(3)->format('Y-m-d H:i:s');
                $subscription->save();
                $subscription->apply(Subscription::STATUS_PAID);
                $subscription->save();
            }

        }

        \DB::commit();

        /**
         * Algumas assinaturas não vem no iap_receipt id
         * então é melhor não realizar essa busca final
         *
         */
        $new_subscription = Subscription::whereAppleTransactionId($apple_transaction_id)->first();
        if ($new_subscription) {
            return $new_subscription;
//            throw new ResourceException('Ocorreu um erro ao processar sua assinatura com a Apple');
        }else if($subscription){
            return $subscription;
        }

        return true;
    }
}
