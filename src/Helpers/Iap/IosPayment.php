<?php

namespace Rockapps\RkLaravel\Helpers\Iap;

use Rockapps\RkLaravel\Exceptions\IntegrationException;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\Product;

class IosPayment
{
    public static function sync(Payment $payment)
    {
        $validator = self::initApi();

        try {
            $response = $validator->setReceiptData($payment->iap_receipt_id)->validate();
            if ($response->isValid()) {

                foreach ($response->getPurchases() as $purchase) {

                    /** @var Payment $payment_child */
                    $payment_child = Payment::where('apple_transaction_id', $purchase->getTransactionId())->first();
                    if ($payment_child) {
                        $payment_child->current_period_start = $purchase->getPurchaseDate()->subHours(3)->format('Y-m-d H:i:s');
                        $payment_child->current_period_end = $purchase->getExpiresDate()->subHours(3)->format('Y-m-d H:i:s');
                        $payment_child->save();

                        if (now() > $purchase->getExpiresDate() && $payment_child->status !== Payment::STATUS_ENDED) {
                            $payment_child->apply(Payment::STATUS_ENDED);
                            $payment_child->save();
                        }
                    }

                }

            } else {
                throw new IntegrationException("O código do recibo IAP iOS não é válido. Código " . $response->getResultCode() . ".", $payment->toArray());
            }
        } catch (\Exception $e) {
            throw new IntegrationException('Erro ao confirmar dados na Apple. ' . $e->getMessage(), $payment->toArray(), null, $e);
        }

        return $payment;
    }

    /**
     * @return \ReceiptValidator\iTunes\Validator
     */
    public static function initApi()
    {
        $sharedSecret = env('APPLE_IAP_SECRET');
        if (!$sharedSecret) {
            throw new ResourceException('APPLE_IAP_SECRET não definido');
        }

        $sandbox = env('APPLE_IAP_SANDBOX', true);

        if ($sandbox) {
            $validator = new \ReceiptValidator\iTunes\Validator(\ReceiptValidator\iTunes\Validator::ENDPOINT_SANDBOX);
        } else {
            $validator = new \ReceiptValidator\iTunes\Validator(\ReceiptValidator\iTunes\Validator::ENDPOINT_PRODUCTION);
        }
        $validator->setSharedSecret($sharedSecret);

        return $validator;
    }

    /**
     * @param Payment $payment
     * @return Payment
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Container\EntryNotFoundException
     * @throws \SM\SMException
     * @noinspection PhpUndefinedClassInspection
     */
    public static function create(Payment $payment)
    {
        if (!$payment->iap_receipt_id) {
            throw new IntegrationException('A propriedade iap_receipt_id é obrigatória');
        }
        if (!$payment->apple_transaction_id) {
            throw new IntegrationException('A propriedade apple_transaction_id é obrigatória');
        }

        if (Payment::whereAppleTransactionId($payment->apple_transaction_id)->first()) {
            throw new ResourceException('Este pagamento já foi validado');
        }

        if (!$payment->product) throw new ResourceException('É necessário vincular o produto em pagamento por IAP');
        if (!$payment->product->active) throw new ResourceException('Produto inativo');
        if (!$payment->product->apple_product_id) throw new ResourceException('Produto não cadastrado para iOS');
        if (!$payment->product->active_ios) throw new ResourceException('Produto inativo para iOS');

        \DB::beginTransaction();

        $validator = self::initApi();

        try {
            $response = $validator->setReceiptData($payment->iap_receipt_id)->validate();
        } catch (\Exception $e) {
            throw new \Rockapps\RkLaravel\Exceptions\ResourceException('Erro ao confirmar dados na Apple. ' . $e->getMessage());
        }

        if (!$response->isValid()) {
            throw new \Rockapps\RkLaravel\Exceptions\ResourceException("O código do recibo IAP iOS não é válido. Código " . $response->getResultCode() . ".");
        }

        foreach ($response->getPurchases() as $purchase) {
            /** @var Payment $payment_new */
            /** @var Product $apple_prod */
            $payment_new = Payment::where('apple_transaction_id', $purchase->getTransactionId())->first();
            if (!$payment_new) {
                $apple_prod = Product::where('apple_product_id', $purchase->getProductId())->withTrashed()->first();
                $payment_new = new Payment();
                $payment_new->gateway = Payment::GATEWAY_METHOD_IAP_APPLE;
                $payment_new->iap_receipt_id = $payment->iap_receipt_id;
                $payment_new->iap_product_id = $purchase->getProductId();
                $payment_new->apple_transaction_id = $purchase->getTransactionId();
                $payment_new->payer_type = $payment->payer_type;
                $payment_new->payer_id = $payment->payer_id;
                $payment_new->purchaseable_type = $payment->purchaseable_type;
                $payment_new->purchaseable_id = $payment->purchaseable_id;
                $payment_new->value = $payment->value;
                $payment_new->product_id = $apple_prod ? $apple_prod->id : null;
                $payment_new->change_purchaseable_state = true;
                $payment_new->created_at = (string)$purchase->getPurchaseDate()->subHours(3)->format('Y-m-d H:i:s'); // Atualiza datas
                $payment_new->save();
                $payment_new->refresh();
                # TODO: Verificar se o pagamento está com status PAGO!
                $payment_new->apply(Payment::STATUS_PAID);
                $payment_new->save();
                $payment_new->paid_at = (string)$purchase->getPurchaseDate()->subHours(3)->format('Y-m-d H:i:s'); // Atualiza datas
                $payment_new->save();
            }

        }

        $payment = Payment::whereAppleTransactionId($payment->apple_transaction_id)->first();

        if (!$payment) {
            throw new ResourceException('Ocorreu um erro ao processar seu pagamento com a Apple');
        }

        \DB::commit();

        return $payment;
    }
}
