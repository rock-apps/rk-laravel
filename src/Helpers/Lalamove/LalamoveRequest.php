<?php /** @noinspection ALL */

namespace Rockapps\RkLaravel\Helpers\Lalamove;

use Psr\Http\Message\ResponseInterface;
use Rockapps\RkLaravel\Exceptions\IntegrationException;

class LalamoveRequest
{
    const HOST_SANDBOX = 'https://rest.sandbox.lalamove.com';
    const HOST_PROD = 'https://rest.lalamove.com';
    public $method = "GET";
    public $body = [];
    public $host = '';
    public $path = '';
    public $header = [];
    public $key = '';
    public $secret = '';
    public $region = LalamoveService::MKT_BR_RIO;
    public $ch = null;

    public function __construct($region = LalamoveService::MKT_BR_RIO)
    {
        $this->region = $region;

        $env_sandbox = filter_var(env('LALAMOVE_SANDBOX'), FILTER_VALIDATE_BOOLEAN);
        $sandbox = config('rk-laravel.shipping.lalamove.sandbox', $env_sandbox);

        if ($sandbox) {
            $this->host = self::HOST_SANDBOX;
            $key = config('rk-laravel.shipping.lalamove.api_key_dev', env('LALAMOVE_API_KEY_DEV'));
            if (!$key) throw new IntegrationException('LALAMOVE_API_KEY_DEV não configurada');
            $this->key = $key;

            $secret = config('rk-laravel.shipping.lalamove.api_secret_dev', env('LALAMOVE_API_SECRET_DEV'));
            if (!$secret) throw new IntegrationException('LALAMOVE_API_SECRET_DEV não configurada');
            $this->secret = $secret;

        } else {
            $this->host = self::HOST_PROD;
            $key = config('rk-laravel.shipping.lalamove.api_key_prod', env('LALAMOVE_API_KEY_PROD'));
            if (!$key) throw new IntegrationException('LALAMOVE_API_KEY_PROD não configurada');
            $this->key = $key;

            $secret = config('rk-laravel.shipping.lalamove.api_secret_prod', env('LALAMOVE_API_SECRET_PROD'));
            if (!$secret) throw new IntegrationException('LALAMOVE_API_SECRET_PROD não configurada');
            $this->secret = $secret;
        }

    }

    /**
     * Send out the request via guzzleHttp
     */
    public function send()
    {

        $client = new \GuzzleHttp\Client();

        $content = [
            'headers' => $this->buildHeader(),
            'http_errors' => false
        ];
        if ($this->method != "GET") {
            $content['json'] = $this->body;
        }

        $response = $client->request($this->method, $this->host . $this->path, $content);

        if ($response->getStatusCode() >= 400 || $response->getStatusCode() >= 500) {
            self::processError($response);
        }

        try {
            return json_decode($response->getBody()->getContents());
        } catch (\Exception $e) {
            throw new IntegrationException('Erro ao decodificar a resposta. '.$e->getMessage());
        }
    }

    /**
     * Build and return the header require for calling lalamove API
     */
    public function buildHeader()
    {
        $time = time() * 1000;
        return [
            "X-Request-ID" => uniqid(),
            "Content-type" => "application/json; charset=utf-8",
            "Authorization" => "hmac " . $this->key . ":" . $time . ":" . $this->getSignature($time),
            "Accept" => "application/json",
            "X-LLM-Country" => $this->region
        ];
    }

    /**
     * Create the signature for the
     * @param $time string time to create the signature (should use current time, same as the Authorization timestamp)
     *
     */
    public function getSignature($time)
    {
        $_encryptBody = '';
        if ($this->method == "GET") {
            $_encryptBody = $time . "\r\n" . $this->method . "\r\n" . $this->path . "\r\n\r\n";
        } else {
            $_encryptBody = $time . "\r\n" . $this->method . "\r\n" . $this->path . "\r\n\r\n" . json_encode((object)$this->body);
        }
        return hash_hmac("sha256", $_encryptBody, $this->secret);
    }

    public static function processError(ResponseInterface $response)
    {
        $content = [];
        try {
            $content = json_decode($response->getBody()->getContents());
            if (property_exists($content, 'detail')) {
                $msg = $content->detail;
            } else {
                $msg = $content->message;
            }
        } catch (\Exception $e) {
            $msg = 'Erro ao obter resposta do Lalamove. ' . $e->getMessage();
        }
//        dump($response);
//        Sentry::capture('Response', $response);
        throw new IntegrationException('Erro de integração Lalamove. ' . $msg, $content);
    }
}
