<?php /** @noinspection ALL */

namespace Rockapps\RkLaravel\Helpers\Lalamove;

class LalamoveService
{
    const MKT_BR_SAO = 'BR_SAO';
    const MKT_BR_RIO = 'BR_RIO';
    const MKT_BR_BHZ = 'BR_BHZ';
    const MKT_BR_POA = 'BR_POA';
    const SERVICE_TYPE_LALAGO = 'LALAGO';
    const SERVICE_TYPE_LALAPRO = 'LALAPRO';
    const SERVICE_TYPE_MPV = 'MPV';
    const SERVICE_TYPE_UV_FIORINO = 'UV_FIORINO';
    const SERVICE_TYPE_TRUCK330 = 'TRUCK330';

    const STATUC_ASSIGNING_DRIVER = 'ASSIGNING_DRIVER';
    const STATUC_ON_GOING = 'ON_GOING';
    const STATUC_PICKED_UP = 'PICKED_UP';
    const STATUC_COMPLETED = 'COMPLETED';
    const STATUC_CANCELED = 'CANCELED';
    const STATUC_REJECTED = 'REJECTED';
    const STATUC_EXPIRED = 'EXPIRED';

    /**
     * Make a http Request to get a quotation from lalamove API via guzzlehttp/guzzle
     *
     * @param $body {Object}, the body of the json
     *   2xx - http request is successful
     *   4xx - unsuccessful request, see body for error message and documentation for matching
     *   5xx - server error, please contact lalamove
     */
    public static function quotation($body, $region = null)
    {
        $request = new LalamoveRequest();
        $request->method = "POST";
        $request->path = "/v2/quotations";
        $request->body = $body;
        if ($region) $request->region = $region;

        return $request->send();
    }

    /**
     * Make a http request to place an order at lalamove API via guzzlehttp/guzzle
     *
     * @param $body {Object}, the body of the json
     *   2xx - http request is successful
     *   4xx - unsuccessful request, see body for error message and documentation for matching
     *   5xx - server error, please contact lalamove
     */
    public static function postOrder($body, $region = null)
    {
        $request = new LalamoveRequest();
        $request->method = "POST";
        $request->path = "/v2/orders";
        $request->body = $body;
        if ($region) $request->region = $region;
        return $request->send();
    }

    /**
     * Make a http request to get the status of order
     *
     * @param $orderId (String), the customerOrderId of lalamove
     *   2xx - http request is successful
     *   4xx - unsuccessful request, see body for error message and documentation for matching
     *   5xx - server error, please contact lalamove
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getOrderStatus($orderId)
    {
        $request = new LalamoveRequest();
        $request->method = "GET";
        $request->path = "/v2/orders/" . $orderId;
        return $request->send();
    }

    /**
     * Make a http request to get the driver Info
     *
     * @param $orderId (String), the customerOrderId of lalamove
     *   2xx - http request is successful
     *   4xx - unsuccessful request, see body for error message and documentation for matching
     *   5xx - server error, please contact lalamove
     */
    public static function getDriverInfo($orderId, $driverId)
    {
        $request = new LalamoveRequest();
        $request->method = "GET";
        $request->path = "/v2/orders/" . $orderId . "/drivers/" . $driverId;
        return $request->send();
    }

    /**
     * Make a http request to get the driver Location
     *
     * @param $orderId (String), the customerOrderId of lalamove
     * @param $driverId (String), the id of the driver at lalamove
     *   2xx - http request is successful
     *   4xx - unsuccessful request, see body for error message and documentation for matching
     *   5xx - server error, please contact lalamove
     */
    public static function getDriverLocation($orderId, $driverId)
    {
        $request = new LalamoveRequest();
        $request->method = "GET";
        $request->path = "/v2/orders/" . $orderId . "/drivers/" . $driverId . "/location";
        return $request->send();
    }

    /**
     * Cancel the http request to get the driver location
     *
     * @param $orderId string the customerOrderId of lalamove
     *   2xx - http request is successful
     *   4xx - unsuccessful request, see body for error message and documentation for matching
     *   5xx - server error, please contact lalamove
     */
    public static function cancelOrder($orderId)
    {
        $request = new LalamoveRequest();
        $request->method = "PUT";
        $request->path = "/v2/orders/" . $orderId . "/cancel";
        return $request->send();
    }
}
