<?php /** @noinspection PhpUnused */

/**
 * Created by PhpStorm.
 * User: erick
 * Date: 08/04/2019
 * Time: 21:26
 */

namespace Rockapps\RkLaravel\Helpers;

use Carbon\CarbonInterval;
use Exception;
use Rockapps\RkLaravel\Exceptions\IntegrationException;
use Rockapps\RkLaravel\Exceptions\ResourceException;

class GoogleMapsDistanceMatrix
{

    /**
     * https://developers.google.com/maps/documentation/javascript/directions
     *
     * OK indicates the response contains a valid DirectionsResult.
     * NOT_FOUND indicates at least one of the locations specified in the request's origin, destination, or waypoints could not be geocoded.
     * ZERO_RESULTS indicates no route could be found between the origin and destination.
     * MAX_WAYPOINTS_EXCEEDED indicates that too many DirectionsWaypoint fields were provided in the DirectionsRequest. See the section below on limits for way points.
     * MAX_ROUTE_LENGTH_EXCEEDED indicates the requested route is too long and cannot be processed. This error occurs when more complex directions are returned. Try reducing the number of waypoints, turns, or instructions.
     * INVALID_REQUEST indicates that the provided DirectionsRequest was invalid. The most common causes of this error code are requests that are missing either an origin or destination, or a transit request that includes waypoints.
     * OVER_QUERY_LIMIT indicates the webpage has sent too many requests within the allowed time period.
     * REQUEST_DENIED indicates the webpage is not allowed to use the directions service.
     * UNKNOWN_ERROR indicates a directions request could not be processed due to a server error. The request may succeed if you try again.
     */
    const ERROR_NOT_FOUND = 'Não foi possível encontrar o endereço solicitado.';
    const ERROR_ZERO_RESULTS = 'Não foi possível encontrar uma rota para os endereços solicitados.';
    const ERROR_MAX_WAYPOINTS_EXCEEDED = 'A rota ultrapassou a quantidade de checkpoints permitidos.';
    const ERROR_MAX_ROUTE_LENGTH_EXCEEDED = 'A rota ultrapassou o tamanho permitido.';
    const ERROR_MAX_DIMENSIONS_EXCEEDED = 'Dimensões máximas atingidas.';
    const ERROR_INVALID_REQUEST = 'Requisição inválida. Verifique os parâmetros utilizados';
    const ERROR_OVER_QUERY_LIMIT = 'O limite de solicitações de rota foi atingido.';
    const ERROR_REQUEST_DENIED = 'A requisição não foi autorizada pelo Google Maps.';
    const ERROR_UNKNOWN_ERROR = 'Ocorreu um erro de comunicação. Tente novamente.';
    const ERROR_INVALID_KEY = 'Chave de Integração do Google Maps Inválida.';
    const ERROR_EMPTY_ORIGIN = 'Favor insira ao menos uma origem.';
    const ERROR_EMPTY_DESTINATION = 'Favor insira ao menos um destino para traçar a rota.';
    const ERROR_GOOGLE_COMM_ERROR = 'Ocorreu um erro de comunicação com o Google Maps.';
    const ERROR_INVALID_LAT = 'Latitude inválida.';
    const ERROR_INVALID_LONG = 'Longitude inválida.';
    const ERROR_INVALID_ADDRESS = 'Endereço inválido.';
    const MODE_BICYCLING = 'bicycling';
    const MODE_DRIVING = 'driving';
    const MODE_WALKING = 'walking';
    const MODE_TRANSIT = 'transit';
    public $origins = [];
    public $destinations = [];
    public $result = null;
    public $key = null;

    // https://developers.google.com/maps/documentation/distance-matrix/intro
    public $uri = null;
    public $options = [];
    public $mode = self::MODE_DRIVING;
    public $language = 'pt-BR';

    public function __construct()
    {
        $key = env('GOOGLE_MAPS_KEY');
        if (!$key) {
            throw new IntegrationException(self::ERROR_INVALID_KEY);
        }
        $this->key = $key;
        return $this;
    }

    public function addOriginByLatLong($lat, $long)
    {
        if (!$lat) {
            throw new ResourceException(self::ERROR_INVALID_LAT);
        }
        if (!$long) {
            throw new ResourceException(self::ERROR_INVALID_LONG);
        }
        $this->origins[] = trim($lat) . ',' . trim($long);
        return $this;
    }

    public function addOriginByAddress($address, $number, $city, $state, $country)
    {
        if (!$address || !$number || !$city || !$state || !$country) {
            throw new ResourceException(self::ERROR_INVALID_ADDRESS);
        }
        $this->origins[] = urlencode(implode(',', [
            $address, $number, $city, $state, $country
        ]));
    }

    public function addDestinationByLatLong($lat, $long)
    {
        if (!$lat) {
            throw new ResourceException(self::ERROR_INVALID_LAT);
        }
        if (!$long) {
            throw new ResourceException(self::ERROR_INVALID_LONG);
        }
        $this->destinations[] = trim($lat) . ',' . trim($long);
        return $this;
    }

    public function addDestinationByAddress($address, $number, $city, $state, $country)
    {
        if (!$address || !$number || !$city || !$state || !$country) {
            throw new ResourceException(self::ERROR_INVALID_ADDRESS);
        }
        $this->destinations[] = urlencode(implode(',', [
            $address, $number, $city, $state, $country
        ]));
    }

    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    public function setLanguage($language)
    {
        $this->language = $language;
    }

    public function calculate()
    {
        $mode = $this->mode;
        $language = $this->language;

        if (count($this->origins) == 0) {
            throw new ResourceException(self::ERROR_EMPTY_ORIGIN);
        }
        if (count($this->destinations) == 0) {
            throw new ResourceException(self::ERROR_EMPTY_DESTINATION);
        }

        $origins = implode('|', $this->origins);
        $destinations = implode('|', $this->destinations);

        $this->uri = 'https://maps.googleapis.com/maps/api/distancematrix/json?key=' . $this->key . '&origins=' . $origins . '&destinations=' . $destinations . '&mode=' . $mode . '&language=' . $language;

        try {
            $return = file_get_contents($this->uri);
        } catch (Exception $exception) {
            $data_sentry = array_merge($this->toArray(), [
                'exception' => $exception->getMessage(),
                'exception_code' => $exception->getCode(),
            ]);
//            Sentry::capture(self::ERROR_GOOGLE_COMM_ERROR, $data_sentry);
            throw new ResourceException(self::ERROR_GOOGLE_COMM_ERROR, $data_sentry, $exception->getPrevious());
        }
        $this->result = json_decode($return);
        if (\property_exists($this->result,'error_message')) {
            throw new ResourceException(self::ERROR_GOOGLE_COMM_ERROR . ' ' . $this->result->error_message, $return);
        }

        if(config('rk-laravel.geo.trigger_sentry_debug')) {
            Sentry::capture('Chamada no Google Matrix API', array_merge($this->toArray(), [
                'result' => $this->result,
            ]));
        }

        foreach ($this->result->rows as $row) {
            foreach ($row->elements as $element) {
                if ($element->status !== 'OK') {
                    if ($element->status === 'NOT_FOUND') {
                        $this->throwException(self::ERROR_NOT_FOUND);
                    } else if ($element->status === 'ZERO_RESULTS') {
                        $this->throwException(self::ERROR_ZERO_RESULTS);
                    } else if ($element->status === 'MAX_ROUTE_LENGTH_EXCEEDED') {
                        $this->throwException(self::ERROR_MAX_ROUTE_LENGTH_EXCEEDED);
                    } else if ($element->status === 'MAX_DIMENSIONS_EXCEEDED') {
                        $this->throwException(self::ERROR_MAX_DIMENSIONS_EXCEEDED);
                    } else {
                        $this->throwException(self::ERROR_UNKNOWN_ERROR);
                    }
                }
            }
        }
        return $this;

    }

    public function toArray()
    {
        return [
            'origins' => $this->origins,
            'destinations' => $this->destinations,
            'result' => $this->result,
            'key' => $this->key,
            'uri' => $this->uri,
            'options' => $this->options,
            'mode' => $this->mode,
            'language' => $this->language,
        ];
    }

    private function throwException($message)
    {
        Sentry::capture($message, $this->toArray());
        throw new ResourceException($message);
    }

    /**
     * Calcula a distância total entre os pontos e retorna em metros
     *
     * @param bool $total
     * @return array|float|int
     */
    public function getDistance($total = false)
    {
        $distances = [];
        if (property_exists($this->result, 'rows')) {
            /** @noinspection PhpUndefinedFieldInspection */
            foreach ($this->result->rows as $row) {
                foreach ($row->elements as $element) {
                    $distances[] = $element->distance->value;
                }
            }
        }
        return $total ? array_sum($distances) : $distances;
    }

    /**
     * Calcula a duração total de tempo entre os pontos e retorna um array com o resultado
     *
     * @param bool $total
     * @return array
     */
    public function getDuration($total = false)
    {
        $duration = 0;
        $durations = [];
        if (property_exists($this->result, 'rows')) {
            /** @noinspection PhpUndefinedFieldInspection */
            foreach ($this->result->rows as $row) {
                foreach ($row->elements as $element) {
                    $duration += $element->duration->value;
                    $durations[] = [
                        'hours' => CarbonInterval::seconds($element->duration->value)->cascade()->hours,
                        'minutes' => CarbonInterval::seconds($element->duration->value)->cascade()->minutes
                    ];
                }
            }
        }

        if ($total) {
            $minutes = CarbonInterval::seconds($duration)->cascade()->minutes;
            $hours = CarbonInterval::seconds($duration)->cascade()->hours;
            return [
                'hours' => $hours,
                'minutes' => $minutes
            ];
        }

        return $durations;

    }

    /**
     * Calcula a duração total de tempo entre os pontos e retorna o total em minutos
     *
     * @return int
     */
    public function getDurationInMinutes()
    {
        $duration = 0;
        if (property_exists($this->result, 'rows')) {
            /** @noinspection PhpUndefinedFieldInspection */
            foreach ($this->result->rows as $row) {
                foreach ($row->elements as $element) {
                    $duration += $element->duration->value;
                }
            }
        }

        $minutes = CarbonInterval::seconds($duration)->cascade()->minutes;
        $hours = CarbonInterval::seconds($duration)->cascade()->hours;

        return $hours * 60 + $minutes;

    }

}
