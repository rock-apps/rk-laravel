<?php

namespace Rockapps\RkLaravel\Helpers;

use Exception;
use Rockapps\RkLaravel\Exceptions\IntegrationException;
use Rockapps\RkLaravel\Exceptions\ResourceException;

class GoogleMapsGeoDecode
{
    /**
     * https://developers.google.com/maps/documentation/javascript/directions
     *
     * OK indicates the response contains a valid DirectionsResult.
     * NOT_FOUND indicates at least one of the locations specified in the request's origin, destination, or waypoints could not be geocoded.
     * ZERO_RESULTS indicates no route could be found between the origin and destination.
     * MAX_WAYPOINTS_EXCEEDED indicates that too many DirectionsWaypoint fields were provided in the DirectionsRequest. See the section below on limits for way points.
     * MAX_ROUTE_LENGTH_EXCEEDED indicates the requested route is too long and cannot be processed. This error occurs when more complex directions are returned. Try reducing the number of waypoints, turns, or instructions.
     * INVALID_REQUEST indicates that the provided DirectionsRequest was invalid. The most common causes of this error code are requests that are missing either an origin or destination, or a transit request that includes waypoints.
     * OVER_QUERY_LIMIT indicates the webpage has sent too many requests within the allowed time period.
     * REQUEST_DENIED indicates the webpage is not allowed to use the directions service.
     * UNKNOWN_ERROR indicates a directions request could not be processed due to a server error. The request may succeed if you try again.
     */
    const ERROR_NOT_FOUND = 'Não foi possível encontrar o endereço solicitado.';
    const ERROR_ZERO_RESULTS = 'Não foi possível encontrar uma rota para os endereços solicitados.';
    const ERROR_MAX_WAYPOINTS_EXCEEDED = 'A rota ultrapassou a quantidade de checkpoints permitidos.';
    const ERROR_MAX_ROUTE_LENGTH_EXCEEDED = 'A rota ultrapassou o tamanho permitido.';
    const ERROR_INVALID_REQUEST = 'Requisição inválida. Verifique os parâmetros utilizados';
    const ERROR_OVER_QUERY_LIMIT = 'O limite de solicitações de rota foi atingido.';
    const ERROR_REQUEST_DENIED = 'A requisição não foi autorizada pelo Google Maps.';
    const ERROR_UNKNOWN_ERROR = 'Ocorreu um erro de comunicação. Tente novamente.';
    const ERROR_INVALID_KEY = 'Chave de Integração do Google Maps Inválida.';
    const ERROR_EMPTY_ORIGIN = 'Favor insira ao menos uma origem.';
    const ERROR_EMPTY_DESTINATION = 'Favor insira ao menos um destino para traçar a rota.';
    const ERROR_GOOGLE_COMM_ERROR = 'Ocorreu um erro de comunicação com o Google Maps.';
    const ERROR_INVALID_LAT = 'Latitude inválida.';
    const ERROR_INVALID_LONG = 'Longitude inválida.';
    const ERROR_INVALID_ADDRESS = 'Endereço inválido.';
    const MODE_BICYCLING = 'bicycling';
    const MODE_DRIVING = 'driving';
    const MODE_WALKING = 'walking';

    // https://developers.google.com/maps/documentation/distance-matrix/intro
    const MODE_TRANSIT = 'transit';
    public $result = null;
    public $key = null;
    public $uri = null;

    public function __construct()
    {
        $key = env('GOOGLE_MAPS_KEY');
        if (!$key) {
            throw new IntegrationException(self::ERROR_INVALID_KEY);
        }
        $this->key = $key;
        return $this;
    }

    public function getLat()
    {
        try {
            return $this->result->results[0]->geometry->location->lat;
        } catch (Exception $exception) {
            throw new IntegrationException('Não foi possível obter a localização de latitude');
        }
    }

    public function getLong()
    {
        try {
            return $this->result->results[0]->geometry->location->lng;
        } catch (Exception $exception) {
            throw new IntegrationException('Não foi possível obter a localização de longitude');
        }
    }

    public function getZipcode()
    {
        $zip = $this->findComponent('postal_code', 'long_name');
        if ($zip) {
            return str_pad($zip, '8', 0, STR_PAD_RIGHT);
        }
        return null;
    }

    private function findComponent($key, $name = 'long_name')
    {
        try {
            $components = $this->result->results[0]->address_components;
            foreach ($components as $i => $item) {
                foreach ($item->types as $k => $props) {
                    if ($props === $key) {
                        return $components[$i]->$name;
                    }
                }
            }
        } catch (\Exception $e) {
            throw new IntegrationException('Ocorreu um erro ao decodificar o endereço.');
        }
        return null;
    }

    public function getCountry($name = 'long_name')
    {
        return $this->findComponent('country', $name);
    }

    public function getState($name = 'long_name')
    {
        return $this->findComponent('administrative_area_level_1', $name);
    }

    public function getNumber()
    {
        return $this->findComponent('street_number');
    }

    public function getCity($name = 'long_name')
    {
        return $this->findComponent('administrative_area_level_2', $name);
    }

    public function getStreet($name = 'long_name')
    {
        return $this->findComponent('route', $name);
    }

    public function getDistrict($name = 'long_name')
    {
        return $this->findComponent('sublocality_level_1', $name);
    }

    /**
     * @param string $address Pode ser endereço ou ponto de interesse
     * @param null $number
     * @param null $city
     * @param null $state
     * @param null $country
     * @return $this
     */
    public function geoDecode($address, $number = null, $city = null, $state = null, $country = null)
    {
//        if (!($address && $number && $city && $state && $country)) {
//            throw new ResourceException(self::ERROR_INVALID_ADDRESS);
//        }

        $address = urlencode(implode(',', [$address, $number, $city, $state, $country]));

        $this->uri = 'https://maps.googleapis.com/maps/api/geocode/json?key=' . $this->key . '&address=' . $address . '&language=pt-BR';

        try {
            $return = file_get_contents($this->uri);
        } catch (Exception $exception) {
            $data_sentry = array_merge($this->toArray(), [
                'exception' => $exception->getMessage(),
                'exception_code' => $exception->getCode(),
            ]);
//            Sentry::capture(self::ERROR_GOOGLE_COMM_ERROR, $data_sentry);
            throw new ResourceException(self::ERROR_GOOGLE_COMM_ERROR, $data_sentry, $exception->getPrevious());
        }
        $json = json_decode($return);
        if (property_exists($json, 'error_message') && $json->error_message) {
            $msg = self::ERROR_GOOGLE_COMM_ERROR . ' (' . $json->status . ')';
            Sentry::capture($msg, $json);
            throw new ResourceException($msg);
        }

        $this->result = $json;

        Sentry::capture('Chamada no Google Maps API', array_merge($this->toArray(), [
            'result' => $this->result,
        ]));

        if ($this->result->status === 'ZERO_RESULTS') {
            Sentry::capture('Erro na Chamada do Google Maps', $this->toArray());
            throw new ResourceException('Verifique o endereço digitado.');
        }

        return $this;
    }

    public function toArray()
    {
        return [
            'result' => $this->result,
            'key' => $this->key,
            'uri' => $this->uri,
        ];
    }
}
