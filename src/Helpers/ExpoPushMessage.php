<?php

namespace Rockapps\RkLaravel\Helpers;

use Exception;
use ExponentPhpSDK\Expo;
use Illuminate\Database\Eloquent\Model;
use Rockapps\RkLaravel\Models\User;

class ExpoPushMessage
{
    private $title;
    private $body;
    private $channel;
    private $data;
    private $android_channel;
    private $class;

    public function __construct()
    {
        $this->android_channel = 'Default';
    }

    public static function subscribe($channel, $token)
    {
        $expo = self::init();
        return $expo->subscribe($channel, $token);
    }

    public static function init()
    {
        return Expo::normalSetup();
    }

    public static function unsubscribe($channel, $token)
    {
        $expo = self::init();
        return $expo->unsubscribe($channel, $token);
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    public function setClass($class)
    {
        $this->class = explode('\\', $class)[2];
        return $this;
    }

    public function setChannel($channel)
    {
        $this->channel = $channel;
        return $this;
    }

    public function setAndroidChannel($android_channel)
    {
        $this->android_channel = $android_channel;
        return $this;
    }

    public function setData(array $data, $data_message, $data_type = null)
    {
        $data['msg'] = $data_message;
        $data['type'] = $data_type;
        $this->data = $data;
        return $this;
    }

    public function sendToChannel($channel)
    {
        $notification = self::buildNotification();

        $expo = self::init();
        $push = false;
        try {
            $push = $expo->notify($channel, $notification);
        } catch (Exception $exception) {
            Sentry::capture('Erro no envio do Push', [
                'exception' => [
                    'message' => $exception->getMessage(),
                    'exception_messagefile' => $exception->getFile(),
                    'line' => $exception->getLine(),
                ],
                'notification' => $notification,
                'channel' => $channel
            ]);
        }
        return $push;
    }

    private function buildNotification()
    {
        $return = [
            'title' => $this->title,
            'sound' => 'default',
            'body' => $this->body,
            'android' => [
                'priority' => 'Urgent',
                'vibrate' => [0, 250, 250, 250]
            ],
        ];
        if ($this->channel) {
            $return['android']['channelId'] = $this->android_channel;
        }
        if ($this->data || $this->class) {
            $data = $this->data;
            if ($this->class) {
                $data['class'] = $this->class;
            }
            $return['data'] = json_encode($data);
        }

        return $return;
    }

    /**
     * @param User|Model $user
     * @return array|bool
     */
    public function sendToUser($user)
    {
        $pushes = [];

        foreach ($user->devices as $device) {
            $pushes[] = $this->sendToDevice($device->registration_id);
        }

        return count($pushes) ? $pushes : false;
    }

    public function sendToDevice($device)
    {
        $expo = self::init();
        $push = false;

        $notification = self::buildNotification();

        try {
            $expo->subscribe($device, $device);
            $push = $expo->notify([$device], $notification);
        } catch (Exception $exception) {
            $data = [
                'exception' => [
                    'message' => $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                ],
                'notification' => $notification,
                'channel' => $device
            ];

            if (config('rk-laravel.push_notification.send_to_sentry_on_error')) {
                Sentry::capture('Erro no envio do Push', $data);
            }
            $push = $data;
        }
        return $push;
    }


}
