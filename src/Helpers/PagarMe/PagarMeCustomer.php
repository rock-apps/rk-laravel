<?php

namespace Rockapps\RkLaravel\Helpers\PagarMe;


use Carbon\Carbon;
use Dingo\Api\Exception\ResourceException;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Interfaces\PayerInterface;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\User;

class PagarMeCustomer
{
    /**
     * @param PayerInterface|User $payer
     * @return bool|PayerInterface
     */
    public static function save($payer)
    {
        if (Constants::isMarketplaceMultiple()) {
            return $payer;
        }

        if ($payer->pgm_customer_id) return $payer;


        $data = self::buildCreateData($payer);
        try {
            $pgm_customer = PagarMe::initApi()->customers()->create($data);
            $payer->pgm_customer_id = $pgm_customer->id;
            $payer->save();
            return $payer;

        } catch (\Exception $e) {
            PagarMe::processException($e, $data);
        }

        return false;
    }

    /**
     * @param PayerInterface $payer
     * @return array
     */
    public static function buildCreateData($payer)
    {
        $error = [];
        if (!$payer->document) $error[] = 'documento (CPF ou CNPJ)';
        if (!$payer->birth_date) $error[] = 'data de nascimento';
        if (count($error)) {
            $plural = count($error) > 1 ? '(s)' : '';
            $msg = "Favor preencher o$plural campo$plural " . \implode(" e ", $error);
            throw new ResourceException($msg);
        }

        $phone_numbers = [];
        if ($payer->mobile) {
            $phone_numbers[] = '+55' . $payer->mobile;
        }
        if ($payer->telephone) {
            $phone_numbers[] = '+55' . $payer->telephone;
        }
        if (!count($phone_numbers)) {
            throw new \Rockapps\RkLaravel\Exceptions\ResourceException('É necessário ao menos um telefone de contato');
        }

        return [
            'external_id' => $payer->id . '',
            'name' => $payer->name,
            'type' => strlen($payer->document) === 14 ? 'corporation' : 'individual',
            'country' => 'br',
            'email' => $payer->email,
            'documents' => [
                [
                    'type' => strlen($payer->document) === 14 ? 'cnpj' : 'cpf',
                    'number' => (string)$payer->document
                ]
            ],
            'phone_numbers' => $phone_numbers,
            'birthday' => Carbon::parse($payer->birth_date)->format('Y-m-d')
        ];
    }

    /**
     * @param PayerInterface $payer
     * @param Address $address
     * @return array
     */
    public static function buildCreateDataToSubscription($payer, $address)
    {
        $data = self::buildCreateData($payer);
        unset($data['documents']);
        $data['document_number'] = (string)$payer->document;
        $data['address'] = PagarMeAddress::processAddress($address);
        $phone = $payer->mobile ?: $payer->telephone;
        $data['phone'] = ['ddd' => substr($phone, 0, 2), 'number' => substr($phone, 2, 9)];
        return $data;

    }


}
