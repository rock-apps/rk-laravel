<?php

namespace Rockapps\RkLaravel\Helpers\PagarMe;


use PagarMe\Exceptions\PagarMeException;
use Rockapps\RkLaravel\Exceptions\IntegrationException;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Interfaces\BankRecipientInterface;
use Rockapps\RkLaravel\Models\BankAccount;
use Rockapps\RkLaravel\Models\BankRecipient;
use Rockapps\RkLaravel\Models\ModelBase;
use Rockapps\RkLaravel\Traits\HasBankRecipient;

class PagarMeRecipient
{

    /**
     * @param BankAccount $bankAccount
     * @param BankRecipientInterface|HasBankRecipient|ModelBase $recipientObj
     * @param string $transfer_interval
     * @param bool $transfer_enabled
     * @param int $transfer_day
     * @param array $data
     * @return bool|BankRecipient
     */
    public static function save(BankAccount $bankAccount, BankRecipientInterface $recipientObj, $transfer_enabled, $transfer_interval = null, $transfer_day = null, $data = [])
    {
        PagarMe::checkMarketplaceMode();

        if (!$bankAccount || !$bankAccount->id) throw new ResourceException('Preencha a conta bancária');
        if (!$recipientObj) throw new ResourceException('Preencha o recebedor');
        if ($transfer_enabled === null) throw new ResourceException('Preencha a ativação de transferência');
//        if (!is_int($transfer_day)) throw new ResourceException('Preencha a data de transferência');
//        if (!is_int($transfer_interval)) throw new ResourceException('Preencha o intervalo de transferência');

        $webhook_url = config('rk-laravel.payment.pagarme.webhook', filter_var(env('PAGARME_WEBHOOK',false)));

        if (!$webhook_url)
            throw new IntegrationException('É necessário definir um webhook para recebedor.');

        if ($recipientObj->bankRecipient) {
            $bankRecipient = $recipientObj->bankRecipient;
        } else {
            $bankRecipient = new BankRecipient();
        }

        $data['transfer_enabled'] = (bool)($transfer_enabled ? $transfer_enabled : $bankRecipient->transfer_enabled); //true,
        $data['transfer_day'] = (int)($transfer_day ? $transfer_day : $bankRecipient->transfer_day); //'5',
        $data['transfer_interval'] = $transfer_interval ? $transfer_interval : $bankRecipient->transfer_interval; // daily, weekly, monthly

        $data['bank_account_id'] = $bankAccount->pgm_bank_id;
        $data['postback_url'] = $webhook_url;
        $data['metadata'] = [
            'bank_account_id' => $bankAccount->id,
            'related_type' => $bankRecipient->related_type,
            'related_id' => $bankRecipient->related_id,
            'bank_recipient_id' => $bankRecipient->id,
        ];

        try {

            if ($bankRecipient->pgm_recipient_id) {
                $data['id'] = $bankRecipient->pgm_recipient_id;
                $pgm_receivable = PagarMe::initApi()->recipients()->update($data);
            } else {
                $pgm_receivable = PagarMe::initApi()->recipients()->create($data);
            }

            $bankRecipient->related_type = get_class($recipientObj);
            $bankRecipient->related_id = $recipientObj->id;
            $bankRecipient->pgm_recipient_id = $pgm_receivable->id;
            $bankRecipient->bank_account_id = $bankAccount->id;
            $bankRecipient->transfer_interval = $pgm_receivable->transfer_interval;
            $bankRecipient->transfer_day = $pgm_receivable->transfer_day;
            $bankRecipient->transfer_enabled = $pgm_receivable->transfer_enabled;
            $bankRecipient->anticipatable_volume_percentage = $pgm_receivable->anticipatable_volume_percentage;
            $bankRecipient->automatic_anticipation_enabled = $pgm_receivable->automatic_anticipation_enabled;
            $bankRecipient->automatic_anticipation_type = $pgm_receivable->automatic_anticipation_type;
            $bankRecipient->automatic_anticipation_days = $pgm_receivable->automatic_anticipation_days;
            $bankRecipient->automatic_anticipation_1025_delay = $pgm_receivable->automatic_anticipation_1025_delay;

            $bankRecipient->save();

            return $bankRecipient;

        } catch (PagarMeException $e) {
            PagarMe::processException($e, $data);
        }
        return false;
    }

}
