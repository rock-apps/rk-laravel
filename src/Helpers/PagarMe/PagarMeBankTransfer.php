<?php

namespace Rockapps\RkLaravel\Helpers\PagarMe;


use Carbon\Carbon;
use Dingo\Api\Exception\ResourceException;
use PagarMe\Exceptions\PagarMeException;
use Rockapps\RkLaravel\Models\BankTransfer;

class PagarMeBankTransfer
{

    /**
     * @param BankTransfer $bankTransfer
     * @return bool|BankTransfer
     */
    public static function create(BankTransfer $bankTransfer)
    {
        PagarMe::checkMarketplaceMode();

        if (!$bankTransfer) throw new ResourceException('Preencha a conta bancária');

        $value = $bankTransfer->value - $bankTransfer->fee;

        $data = [
            // É necessário que o marketplace possua saldo para recebero o saldo só é disponibilizado na conta dele através de split
            'recipient_id' => $bankTransfer->transfered->bankRecipient->pgm_recipient_id,
//            'bank_account_id' => $bankTransfer->bankAccount->pgm_bank_id,
            'amount' => number_format($value, 2, '', ''),
            'metadata' => [
                'bank_transfer_id' => $bankTransfer->id,
                'bank_account_id' => $bankTransfer->bank_account_id,
                'transfered_id' => $bankTransfer->transfered_id,
                'transfered_type' => $bankTransfer->transfered_type,
                'bank_recipient_id' => $bankTransfer->transfered->bankRecipient->id,
            ]
        ];

        try {
            $pgm_transfer = PagarMe::initApi()->transfers()->create($data);
            return self::saveData($pgm_transfer, $bankTransfer);

        } catch (PagarMeException $e) {
            PagarMe::processException($e, $data);
        }
        return false;
    }

    /**
     * @param $pgm_transfer
     * @param BankTransfer $bankTransfer
     * @return BankTransfer
     */
    public static function saveData($pgm_transfer, BankTransfer $bankTransfer)
    {
        $bankTransfer->status = $pgm_transfer->status;
        $bankTransfer->pgm_transfer_id = $pgm_transfer->id;
        $bankTransfer->pgm_transaction_id = $pgm_transfer->transaction_id;
        $bankTransfer->bank_account_id = $bankTransfer->id;
        $bankTransfer->value = $pgm_transfer->amount / 100;
        $bankTransfer->fee = $pgm_transfer->fee / 100;
        $bankTransfer->type = $pgm_transfer->type;
        $bankTransfer->funding_estimated_at = Carbon::parse($pgm_transfer->funding_estimated_date)->format('Y-m-d H:i:s');

        if ($pgm_transfer->funding_date) {
            $bankTransfer->funding_at = Carbon::parse($pgm_transfer->funding_date)->format('Y-m-d H:i:s');
        }

//        $new_status = Str::upper($pgm_transfer->status);
//        if ($bankTransfer->status != $new_status) {
//            if ($bankTransfer->canApply($new_status)) {
//                $bankTransfer->apply($new_status);
//                $bankTransfer->save();
//            } else {
//                Sentry::capture("Tentativa Inválida de Trocar de State Atual($bankTransfer->status) Futuro($new_status)", $bankTransfer->getAttributes());
//            }
//        }

        $bankTransfer->save();
        return $bankTransfer;
    }

    public static function sync(BankTransfer $bankTransfer)
    {
        PagarMe::checkMarketplaceMode();

        $pgm_bank_transfer = PagarMe::initApi()->subscriptions()->get(['id' => $bankTransfer->pgm_transfer_id]);

        self::saveData($pgm_bank_transfer, $bankTransfer);

        return $bankTransfer;
    }
}
