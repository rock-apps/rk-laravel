<?php

namespace Rockapps\RkLaravel\Helpers\PagarMe;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Str;
use PagarMe\Exceptions\PagarMeException;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Exceptions\IntegrationException;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\Sentry;
use Rockapps\RkLaravel\Interfaces\PayerInterface;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Traits\Payer;

class PagarMeTransaction
{
    public $data = [];
    public $gateway = null;
    /** @var Payment */
    public $payment;
    /** @var Payer|PayerInterface */
    public $payer;

    /**
     * PagarMeTransaction constructor.
     * @param Payment $payment
     * @param null $split_rules
     * @throws Exception
     */
    public function __construct(Payment $payment, $split_rules = null)
    {
        /** @var PayerInterface|User $payer */
        $this->payment = $payment;
        $this->payer = $payment->payer;

        $this->data = [
            'metadata' => [
                'payment_id' => $payment->id,
                'purchaseable_id' => $payment->purchaseable_id,
                'purchaseable_type' => $payment->purchaseable_type,
                'payer_id' => $payment->payer_id,
                'payer_type' => $payment->payer_type
            ],
        ];
        $this->data['split_rules'] = $this->buildSplitRules($split_rules);
        $this->data['amount'] = $this->buildAmount();
        $this->data['billing'] = $this->buildBilling();
        $this->data['payment_method'] = $this->buildGateway();
        $this->data['customer'] = $this->buildCustomer();
        $this->data['soft_descriptor'] = $this->buildSoftDescriptor();
        $this->data['async'] = $this->buildAsync(); # Atenção: este método também escreve o webhook (postback_url)

    }

    /**
     * @param null $split_rules
     * @return array|null
     * @throws Exception
     */
    private function buildSplitRules($split_rules = null)
    {
        $payment = $this->payment;
        /**
         * Caso o split seja enviado previamente, provavelmente pela função charge()
         */
        if ($split_rules) return $split_rules;

        if ($this->payment->direction === Payment::DIRECTION_OUTCOME) {

            $value = (float)$payment->bankTransfers()->sum('value');
            if ($payment->value !== $value) {
                throw new ResourceException('O valor do pagamento é diferente da soma do valor das transferências');
            }
            if (!$payment->bankTransfers()->count()) {
                throw new ResourceException('Não existem transferências vinculadas a esse pagamento');
            }
            $rules = [];
            $rules[] = [
                'liable' => true,
                'charge_processing_fee' => true,
                'amount' => number_format(Parameter::getByKey(Constants::PGM_BOLETO_FEE)->value_float, 2, '', ''),
                'charge_remainder_fee' => true,
                'recipient_id' => Parameter::getByKey(Constants::PGM_DEFAULT_RECIPIENT_ID)->value,
            ];

            foreach ($payment->bankTransfers as $bankTransfer) {
                if (!$bankTransfer->transfered->bankRecipient) {
                    throw new ResourceException("O destinatário ({$bankTransfer->transfered->name}) não é um recebedor");
                }
                $rules[] = [
                    'liable' => false,
                    'charge_processing_fee' => false,
                    'amount' => number_format($bankTransfer->value, 2, '', ''),
                    'charge_remainder_fee' => true,
                    'recipient_id' => $bankTransfer->transfered->bankRecipient->pgm_recipient_id,
                ];
            }
            return $rules;

        } elseif ($this->payment->direction === Payment::DIRECTION_INCOME) {

            // Utilizar o build rules enviado no argumento da função
            //throw new ResourceException('Implementar na função charge() do payment.');

        }
        return null;
    }

    /**
     *
     */
    private function buildAmount()
    {
        $payment = $this->payment;

        if ($this->payment->direction === Payment::DIRECTION_OUTCOME) {
            $value = $payment->value + Parameter::getByKey(Constants::PGM_BOLETO_FEE)->value_float;
        } else {
            $value = $payment->value;
        }
        return number_format($value, 2, '', '');
    }

    private function buildBilling()
    {
        return [
            'name' => $this->payer->name,
            'address' => PagarMeAddress::processAddress($this->payment->address)
        ];
    }

    private function buildGateway()
    {
        switch ($this->payment->gateway) {
            case  Payment::GATEWAY_PAGARME_CC:
                return 'credit_card';
            case Payment::GATEWAY_PAGARME_BOLETO:
                return 'boleto';
            case Payment::GATEWAY_PAGARME_PIX:
                return 'pix';
        }
        throw new ResourceException('Pagar.Me Gateway inválido');
    }

    private function buildCustomer()
    {
        if (config('rk-laravel.marketplace.mode')) {
            return PagarMeCustomer::buildCreateData($this->payer);
        } else {
            return ['id' => PagarMeCustomer::save($this->payer)->pgm_customer_id];
        }
    }

    private function buildSoftDescriptor()
    {
        $sd = config('rk-laravel.payment.pagarme.soft_descriptor', env('PAGARME_SOFT_DESCRIPTOR', false));
        if (!$sd) {
            $sd = env('PAGARME_SOFT_DESCRIPTOR', Parameter::getByKey(Constants::PGM_SOFT_DESCRIPTOR)->value);
        }
        return $sd;
    }

    private function buildAsync()
    {
        if ($this->payment->async && in_array($this->payment->gateway, [Payment::GATEWAY_PAGARME_PIX, Payment::GATEWAY_PAGARME_BOLETO])) {
            $this->setWebhook();
        }

        return $this->payment->async;
    }

    private function setWebhook()
    {
        if (!env('PAGARME_WEBHOOK', false)) {
            throw new IntegrationException('É necessário definir um webhook para transações assíncronas ou boleto.', []);
        }
        $this->data['postback_url'] = env('PAGARME_WEBHOOK');
    }

    /**
     * @param Payment $payment
     * @return \ArrayObject|bool
     */
    public static function refundTransaction(Payment $payment)
    {
        $transaction = PagarMe::initApi()->transactions()->get([
            'id' => $payment->pgm_transaction_id,
        ]);
        if ($transaction->status === 'refunded') {
            return $transaction;
        }

        $data = [
            'id' => $payment->pgm_transaction_id,
        ];

        try {
            return PagarMe::initApi()->transactions()->refund($data);

        } catch (PagarMeException $e) {
            PagarMe::processException($e, $data);
        }
        return false;
    }

    public static function sync(Payment $payment)
    {

        $pgm_transaction = PagarMe::initApi()->transactions()->get(['id' => $payment->pgm_transaction_id]);
        $status = Str::upper($pgm_transaction->status);

        if ($pgm_transaction->boleto_barcode) {
            $payment->boleto_barcode = $pgm_transaction->boleto_barcode;
            $payment->boleto_url = $pgm_transaction->boleto_url;
            $payment->boleto_expiration_date = Carbon::parse($pgm_transaction->boleto_expiration_date)->format('Y-m-d H:i:s');
            $payment->save();
        }

        if ($pgm_transaction->authorized_amount) {
            $payment->value = $pgm_transaction->authorized_amount / 100;
            $payment->save();
        }

        if ($payment->status != $status) {
            if ($payment->canApply($status)) {
                $payment->apply($status);
                $payment->save();
            } else {
                Sentry::capture("Tentativa Inválida de Trocar de State Atual($payment->status) Futuro($status)", $payment->getAttributes());
            }
        }

        return $payment;
    }

    /**
     * Simula a troca de status. Só funciona no ambiente de homologação
     *
     * @param Payment $payment
     * @param string $status
     * @return Payment
     */
    public static function simulateStatus($payment, $status = 'paid')
    {
        \Rockapps\RkLaravel\Helpers\PagarMe\PagarMe::initApi()->transactions()->simulateStatus([
            'id' => $payment->pgm_transaction_id,
            'status' => $status
        ]);
        $payment->syncWithGateway();
        return $payment;
    }

    public function chargeBoleto($boleto_expiration_date = null, $boleto_instructions = null)
    {
        $payment = $this->payment;

        $this->checkPermission(Payment::GATEWAY_PAGARME_BOLETO);

        if ($boleto_expiration_date) $this->data['boleto_expiration_date'] = Carbon::parse($boleto_expiration_date)->format('Y-m-d');
        if ($boleto_instructions) $this->data['boleto_instructions'] = $boleto_instructions;

        $this->setWebhook();

        try {
            $pme_transaction = PagarMe::initApi()->transactions()->create($this->data);

            // O PGM precisa para enviar o código e url do obleto;
            sleep(2);
            $pme_transaction = PagarMe::initApi()->transactions()->get(['id' => $pme_transaction->id]);

            $payment->async = true;
            $payment->pgm_transaction_id = $pme_transaction->id;
            $payment->status = $pme_transaction->status;
            $payment->boleto_barcode = $pme_transaction->boleto_barcode;
            $payment->boleto_url = $pme_transaction->boleto_url;
            $payment->boleto_expiration_date = Carbon::parse($pme_transaction->boleto_expiration_date)->format('Y-m-d H:i:s');
            $payment->save();

        } catch (PagarMeException $e) {
            PagarMe::processException($e, $this->data);
        }
        return $payment;
    }

    /**
     * @param $instanced_gateway
     */
    private function checkPermission($instanced_gateway)
    {
        if ($this->payment->gateway !== $instanced_gateway) {

            throw new ResourceException('Não é possível realizar a cobrança neste gateway. Foi instanciado um gateway diferente.');
        }

        switch ($this->gateway) {
            case Payment::GATEWAY_PAGARME_CC:
                $prop = 'can_pay_with_cc';
                $msg = 'O cliente não pode realizar pagamentos por cartão de crédito.';
                break;
            case Payment::GATEWAY_PAGARME_PIX:
                $prop = 'can_pay_with_pix';
                $msg = 'O cliente não pode realizar pagamentos por PIX.';
                break;
            case Payment::GATEWAY_PAGARME_BOLETO:
                $prop = 'can_pay_with_boleto';
                $msg = 'O cliente não pode realizar pagamentos por boleto.';
                break;
            default:
                $prop = false;
                $msg = '';
                break;
        }
        if ($prop && !$this->payer->$prop)
            throw new ResourceException($msg);

    }

    public function chargeCC($data = [])
    {
        $payment = $this->payment;

        $this->checkPermission(Payment::GATEWAY_PAGARME_CC);

        /**
         * Se o cartão for passado como extra, ele é enviado ao invés do cartão do payment
         */
        if ($data) {
            $this->data = array_merge($this->data, PagarMeCreditCard::buildCreateData(
                $data['card_holder_name'],
                $data['card_number'],
                $data['card_expiration_date'],
                $data['card_cvv']
            ));
            $payment->credit_card_id = null;
            $payment->save();

        } else {
            if ($payment->creditCard && $payment->creditCard->pgm_card_id) {
                $this->data['card_id'] = $payment->creditCard->pgm_card_id;
            } else {
                throw new ResourceException('Cartão não encontrado.');
            }
        }

        $this->data['async'] = $this->buildAsync();

        try {
            $pgm_transaction = PagarMe::initApi()->transactions()->create($this->data);
            $payment = $this->payment;
            $payment->pgm_transaction_id = $pgm_transaction->id;
            $payment->apply(Str::upper($pgm_transaction->status));
            $payment->save();

            if (config('rk-laravel.payment.commit_on_failed_payment', true)) {
                \DB::commit();
            }

            if (!$payment->async) {
                if (!$payment->isPaid()) {
                    throw new ResourceException('O pagamento não foi aceito.');
                }
            }
        } catch (PagarMeException $e) {
            PagarMe::processException($e, $this->data);
        }
        return $payment;
    }

    public function chargePix($pix_expiration_date = null)
    {
        $payment = $this->payment;

        $this->checkPermission(Payment::GATEWAY_PAGARME_PIX);

        if ($pix_expiration_date) $this->data['pix_expiration_date'] = Carbon::parse($pix_expiration_date)->format('Y-m-d');

        try {
            $pgm_transaction = PagarMe::initApi()->transactions()->create($this->data);
            $payment = $this->payment;
            $payment->async = true;
            $payment->pgm_transaction_id = $pgm_transaction->id;
            $payment->pix_qr_code = $pgm_transaction->pix_qr_code;
            $payment->apply(Str::upper($pgm_transaction->status));
            $payment->save();

            if (config('rk-laravel.payment.commit_on_failed_payment', true)) {
                \DB::commit();
            }

            if (!$payment->async) {
                if (!$payment->isPaid()) {
                    throw new ResourceException('O pagamento não foi aceito.');
                }
            }
        } catch (PagarMeException $e) {
            PagarMe::processException($e, $this->data);
        }
        return $payment;
    }

    public function addItem(string $id, $title, $quantity = 1, $unit_price = null, $tangible = false)
    {
        $this->data['items'][] = [
            'id' => $id,
            'title' => $title,
            'unit_price' => $unit_price ? $unit_price : $this->buildAmount(),
            'quantity' => $quantity,
            'tangible' => $tangible
        ];
    }

}
