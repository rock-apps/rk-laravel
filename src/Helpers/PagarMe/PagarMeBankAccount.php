<?php

namespace Rockapps\RkLaravel\Helpers\PagarMe;


use Dingo\Api\Exception\ResourceException;
use Illuminate\Support\Str;
use PagarMe\Exceptions\PagarMeException;
use Rockapps\RkLaravel\Interfaces\HasBankAccountInterface;
use Rockapps\RkLaravel\Models\BankAccount;
use Rockapps\RkLaravel\Models\User;

class PagarMeBankAccount
{

    /**
     * @param HasBankAccountInterface|User $has_bankable
     * @param $bank_code
     * @param $agencia
     * @param $agencia_dv
     * @param $conta
     * @param $conta_dv
     * @param $document_number
     * @param $legal_name
     * @param $type
     * @return BankAccount
     */
    public static function create($has_bankable, $bank_code, $agencia, $agencia_dv, $conta, $conta_dv, $document_number, $legal_name, $type)
    {
        PagarMe::checkMarketplaceMode();

        if (!$bank_code) throw new ResourceException('Preencha o código do banco');
        if (!$agencia) throw new ResourceException('Preencha a agência');
        if (!$conta) throw new ResourceException('Preencha a conta');
        if (!$document_number) throw new ResourceException('Preencha o número do documento CPF/CNPJ');
        if (!$legal_name) throw new ResourceException('Preencha o nome ou razão social');
        if (!$type) throw new ResourceException('Preencha o tipo de conta bancária');
        if ($type === BankAccount::TYPE_PIX) {
            throw new ResourceException('Não é permitido criar conta bancária PIX no gateway PagarMe');
        }

        $data = [
            'bank_code' => $bank_code,
            'agencia' => $agencia,
            'conta' => $conta,
            'document_number' => $document_number,
            'legal_name' => Str::limit($legal_name, 30, ''),
            'type' => $type,
        ];

        !is_null($agencia_dv) ? $data['agencia_dv'] = (int)$agencia_dv : null;
        !is_null($conta_dv) ? $data['conta_dv'] = (int)$conta_dv : null;

        $bank = false;
        try {
            $pgm_bank = PagarMe::initApi()->bankAccounts()->create($data);

            $bank = new BankAccount([
                'pgm_bank_id' => $pgm_bank->id,
                'bank_code' => $pgm_bank->bank_code,
                'agencia' => $pgm_bank->agencia,
                'agencia_dv' => $pgm_bank->agencia_dv,
                'conta' => $pgm_bank->conta,
                'conta_dv' => $pgm_bank->conta_dv,
                'document_number' => $pgm_bank->document_number,
                'legal_name' => $pgm_bank->legal_name,
                'type' => $pgm_bank->type,
                'owner_id' => $has_bankable->id,
                'owner_type' => get_class($has_bankable),
            ]);
            $bank->save();
            return $bank;

        } catch (PagarMeException $e) {
            PagarMe::processException($e, $data);
        }
        return $bank;
    }

}
