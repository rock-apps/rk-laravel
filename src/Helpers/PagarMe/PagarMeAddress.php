<?php

namespace Rockapps\RkLaravel\Helpers\PagarMe;


use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Interfaces\PayerInterface;
use Rockapps\RkLaravel\Models\Address;

class PagarMeAddress
{

    /**
     * @param PayerInterface|Address $address
     * @return array
     */
    public static function processAddress(Address $address = null)
    {
        if(!$address) throw new ResourceException('É necessário cadastrar um endereço para realizar o pagamento');

        if (!$address->street) throw new ResourceException('O endereço é obrigatório');
        if (!$address->number) throw new ResourceException('O número do endereço é obrigatório');
        if (!$address->state) throw new ResourceException('O estado do endereço é obrigatório');
        if (!$address->city) throw new ResourceException('A cidade do endereço é obrigatória');
        if (!$address->district) throw new ResourceException('O bairro do endereço é obrigatório');
        if (!$address->zipcode) throw new ResourceException('O CEP do endereço é obrigatório');

        $data = [
            'country' => 'br',
            'street' => $address->street,
            'street_number' => $address->number,
            'state' => $address->state,
            'city' => $address->city,
            'neighborhood' => $address->district,
            'zipcode' => $address->zipcode,
        ];

        $address->complement ? $data['complementary'] = $address->complement : null;

        return $data;
    }
}
