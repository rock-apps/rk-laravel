<?php

namespace Rockapps\RkLaravel\Helpers\PagarMe;


use PagarMe\Exceptions\PagarMeException;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Interfaces\PayerInterface;
use Rockapps\RkLaravel\Models\CreditCard;
use Rockapps\RkLaravel\Models\User;

class PagarMeCreditCard
{
    /**
     * @param $holder_name string Yoda
     * @param $number string 4242424242424242
     * @param $expiration_date string 1225
     * @param $cvv string 123
     * @param PayerInterface|User $payer
     * @return CreditCard
     */
    public static function create($holder_name, $number, $expiration_date, $cvv, $payer)
    {
        PagarMe::checkMarketplaceMode();

        $card = null;
        if (!$payer->pgm_customer_id) {
            throw new ResourceException('Seu perfil não está concluído. Verifique o preenchimento do documento, data de nascimento e telefone.');
        }
        $data = self::buildCreateData($holder_name, $number, $expiration_date, $cvv);
        $data['customer_id'] = $payer->pgm_customer_id;

        try {
            $pgm_card = PagarMe::initApi()->cards()->create($data);
            $card = new CreditCard([
                'pgm_card_id' => $pgm_card->id,
                'brand' => $pgm_card->brand,
                'last_digits' => $pgm_card->last_digits,
                'valid' => $pgm_card->valid,
                'holder_name' => $pgm_card->holder_name,
                'expiration_date' => $pgm_card->expiration_date,
                'default' => 1,
                'owner_id' => $payer->id,
                'owner_type' => get_class($payer),
            ]);
            $card->save();

        } catch (PagarMeException $e) {
            PagarMe::processException($e, $data);
        }

        return $card;
    }

    /**
     * @param $holder_name
     * @param $number
     * @param $expiration_date
     * @param $cvv
     * @return array
     */
    public static function buildCreateData($holder_name, $number, $expiration_date, $cvv)
    {
        if (!$holder_name) throw new ResourceException('Nome inválido do portador do cartão');
        if (!$number) throw new ResourceException('Número do cartão inválido');
        if (!$expiration_date) throw new ResourceException('Data de vencimento inválida');
        if (!$cvv) throw new ResourceException('CVV inválido');
        return [
            'card_holder_name' => trim($holder_name),
            'card_number' => filter_var($number, FILTER_SANITIZE_NUMBER_INT),
            'card_expiration_date' => filter_var($expiration_date, FILTER_SANITIZE_NUMBER_INT),
            'card_cvv' => $cvv
        ];

    }

}
