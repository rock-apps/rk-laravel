<?php

namespace Rockapps\RkLaravel\Helpers\PagarMe;

use PagarMe\Exceptions\PagarMeException;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\Plan;

class PagarMePlan
{
    /**
     * @param Plan $plan
     * @return bool|Plan
     */
    public static function save(Plan $plan)
    {
        PagarMe::checkMarketplaceMode();

        try {
            if ($plan->pgm_plan_id) {
                $data = [
                    'id' => $plan->pgm_plan_id,
                    'name' => $plan->name,
                    'trial_days' => $plan->trial_days,
                    'invoice_reminder' => $plan->invoice_reminder,
                ];
                PagarMe::initApi()->plans()->update($data);
            } else {
                $data = $plan->getAttributes();
                $data['payment_method'] = \explode(',', $plan->payment_method);
                $data['amount'] = number_format($plan->value, 2, '', '');
                $pgm_bank = PagarMe::initApi()->plans()->create($data);
                $plan->pgm_plan_id = $pgm_bank->id;
                $plan->save();
            }
            return $plan;

        } catch (PagarMeException $e) {
            PagarMe::processException($e, $plan->getAttributes());
        }
        return false;
    }

}
