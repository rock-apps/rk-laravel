<?php

namespace Rockapps\RkLaravel\Helpers\PagarMe;

use Carbon\Carbon;
use Illuminate\Support\Str;
use PagarMe\Exceptions\PagarMeException;
use Rockapps\RkLaravel\Exceptions\IntegrationException;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\Sentry;
use Rockapps\RkLaravel\Interfaces\PayerInterface;
use Rockapps\RkLaravel\Models\Subscription;
use Rockapps\RkLaravel\Traits\States\SubscriptionStateTrait;

class PagarMeSubscription
{
    /**
     * @param Subscription $subscription
     * @return bool|Subscription
     * @throws \Illuminate\Container\EntryNotFoundException
     * @throws \SM\SMException
     */
    public static function save(Subscription $subscription)
    {
        PagarMe::checkMarketplaceMode();

        if ($subscription->pgm_subscription_id) {
            throw new ResourceException('Esta assinatura já foi criada no PagarMe');
        }
        if ($subscription->gateway !== Subscription::GATEWAY_PAGARME) {
            throw new ResourceException('O gateway seleciona não é o Pagar.Me');
        }
        if (!\in_array($subscription->payment_method, [Subscription::PAYMENT_METHOD_CREDIT_CARD, Subscription::PAYMENT_METHOD_BOLETO])) {
            throw new ResourceException('Método de pagamento inválido para o gateway Pagar.Me');
        }
        $data = [];
        try {
            $data = self::buildCreateData($subscription);
            $pgm_subscription = PagarMe::initApi()->subscriptions()->create($data);
            return self::saveData($pgm_subscription, $subscription);

        } catch (PagarMeException $e) {
            PagarMe::processException($e, $data);
        }
        return false;
    }

    private static function buildCreateData(Subscription $subscription)
    {
        if (!env('PAGARME_WEBHOOK', false)) {
            throw new IntegrationException('É necessário definir um webhook para transações assíncronas ou boleto.', []);
        }
        /** @var PayerInterface $payer */
        $payer = $subscription->payer;

        /*
         * A API do PagarMe não aceitou a documentação cadastrada no cliente como na transaction.
         * Precisou ser enviada novamente na requisição como um novo customer.
         */
        $data = [
            'plan_id' => $subscription->plan->pgm_plan_id,
            'payment_method' => Str::lower($subscription->payment_method),
            'postback_url' => env('PAGARME_WEBHOOK'),
            'customer_id' => $payer->pgm_customer_id,
            'customer' => PagarMeCustomer::buildCreateDataToSubscription($payer, $subscription->address),
            'metadata' => [
                'plan_id' => $subscription->plan_id,
                'subscription_id' => $subscription->id,
                'payer_id' => $subscription->payer_id,
                'payer_type' => $subscription->payer_type,
                'subscriber_id' => $subscription->subscriber_id,
                'subscriber_type' => $subscription->subscriber_type,
            ]
        ];

        if ($subscription->payment_method === Subscription::PAYMENT_METHOD_CREDIT_CARD) {
            $data['card_id'] = $subscription->creditCard->pgm_card_id;
        }

        return $data;
    }

    /**
     * @param $pgm_subscription
     * @param Subscription $subscription
     * @return Subscription
     * @throws \Illuminate\Container\EntryNotFoundException
     * @throws \SM\SMException
     */
    public static function saveData($pgm_subscription, Subscription $subscription)
    {
        $subscription->pgm_subscription_id = $pgm_subscription->id;
        $subscription->current_period_start = Carbon::parse($pgm_subscription->current_period_start)->subHours(3);
        $subscription->current_period_end = Carbon::parse($pgm_subscription->current_period_end)->subHours(3);
        $new_status = Str::upper($pgm_subscription->status);

        if ($subscription->status != $new_status) {
            if ($subscription->canApply($new_status)) {
                $subscription->apply($new_status);
                $subscription->save();
            } else {
                Sentry::capture("Tentativa Inválida de Trocar de State Atual($subscription->status) Futuro($new_status)", $subscription->getAttributes());
            }
        }

//        if ($subscription->status !== $new_status) {
//            $subscription->apply($new_status);
//        }
        $subscription->save();
        return $subscription;
    }

    /**
     * @param Subscription|SubscriptionStateTrait $subscription
     * @return bool|Subscription
     * @throws \Illuminate\Container\EntryNotFoundException
     * @throws \SM\SMException
     */
    public static function cancel(Subscription $subscription)
    {
        PagarMe::checkMarketplaceMode();

        if ($subscription->gateway !== Subscription::GATEWAY_PAGARME) return $subscription;

        try {
            $pgm_subscription = PagarMe::initApi()->subscriptions()->get(['id' => $subscription->pgm_subscription_id]);
            if ($pgm_subscription->status !== 'canceled') {
                $pgm_subscription = PagarMe::initApi()->subscriptions()->cancel(['id' => $subscription->pgm_subscription_id]);
                return self::saveData($pgm_subscription, $subscription);
            }
            return $subscription;

        } catch (PagarMeException $e) {
            PagarMe::processException($e, $subscription->getAttributes());
        }
        return false;
    }

    public static function sync(Subscription $subscription)
    {
        PagarMe::checkMarketplaceMode();

        $pgm_subscription = PagarMe::initApi()->subscriptions()->get(['id' => $subscription->pgm_subscription_id]);

        self::saveData($pgm_subscription, $subscription);

        return $subscription;
    }
}
