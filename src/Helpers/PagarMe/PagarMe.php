<?php

namespace Rockapps\RkLaravel\Helpers\PagarMe;

use PagarMe\Client;
use PagarMe\Exceptions\PagarMeException;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Exceptions\IntegrationException;
use Rockapps\RkLaravel\Helpers\Sentry;
use Rockapps\RkLaravel\Models\BankTransfer;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\Subscription;
use Rockapps\RkLaravel\Services\PaymentGatewayService;

class PagarMe
{
    /**
     * @param $request
     * @param null $test
     */
    public static function webhook($request, $test = null)
    {
        if ($test) {
            $object = $request['object'];
            $id = $request['id'];
        } else {
            $postbackPayload = file_get_contents('php://input');
            $signature = $_SERVER['HTTP_X_HUB_SIGNATURE'];
            $postbackIsValid = self::initApi()->postbacks()->validate($postbackPayload, $signature);
            if (!$postbackIsValid) {
                $msg = 'Erro de comunicação com o sistema de pagamento. O postback não é válido.';
                throw new IntegrationException($msg);
            }
            $object = $_POST['object']; // transaction || subscription
            $id = $_POST['id'];
        }

        if (config('rk-laravel.sentry.log_pagarme_webhook')) {
            Sentry::capture("Webhook [$object]", $request);
        }

        /** @var PaymentGatewayService $service */
        $service = config('rk-laravel.payment.service', PaymentGatewayService::class);


        if ($object === 'transaction') {

            /** @var Payment $payment */
            $payment = Payment::wherePgmTransactionId($id)->get()->first();
            if ($payment) {

                if ($payment->purchaseable && $payment->purchaseable->company) {
                    $service::setCredentials($payment->purchaseable->company, $payment->gateway);
                }

                $payment->syncWithGateway();
            }

        } else if ($object === 'subscription') {

            /** @var Subscription $subscription */
            $subscription = Subscription::wherePgmSubscriptionId($id)->get()->first();
            if ($subscription) {
                // Não existe vínculo com a company
//                if ($subscription->purchaseable && $payment->purchaseable->company) {
//                    $service::setCredentials($payment->purchaseable->company, $payment->gateway);
//                }
                $subscription->syncWithGateway();
            }

        } else if ($object === 'transfer') {

            /** @var BankTransfer $bankTransfer */
            $bankTransfer = BankTransfer::wherePgmTransferId($id)->get()->first();
            if ($bankTransfer) {
                // Não existe vínculo com a company
//                if ($subscription->purchaseable && $payment->purchaseable->company) {
//                    $service::setCredentials($payment->purchaseable->company, $payment->gateway);
//                }
                $bankTransfer->syncWithGateway();
            }

        }
    }

    /**
     * @return Client
     */
    public static function initApi()
    {
        $env_sandbox = filter_var(env('PAGARME_SANDBOX'), FILTER_VALIDATE_BOOLEAN);
        $sandbox = config('rk-laravel.payment.pagarme.sandbox', $env_sandbox);

        if ($sandbox) {
            $api_key = config('rk-laravel.payment.pagarme.api_key_dev', env('PAGARME_API_KEY_DEV'));
            if (!$api_key) throw new IntegrationException('PAGARME_API_KEY_DEV não configurada');
        } else {
            $api_key = config('rk-laravel.payment.pagarme.api_key_prod', env('PAGARME_API_KEY_PROD'));
            if (!$api_key) throw new IntegrationException('PAGARME_API_KEY_PROD não configurada');
        }

        return new Client($api_key);
    }

    /**
     * @param PagarMeException|\Exception $e
     * @param $data
     */
    public static function processException($e, $data)
    {
        $errors['error'] = [
            'type' => $e->getType(),
            'parameter_name' => $e->getParameterName(),
            'message' => $e->getMessage(),
            'data' => $data
        ];
        $gateway_parameter = $e->getParameterName();
        $gateway_message = explode('MESSAGE: ', $e->getMessage());
        $msg = 'Erro de comunicação com o sistema de pagamento.';
        //[CUSTOMER[PHONE][NUMBER]]NÚMERO DE TELEFONE INVÁLIDO
        if ($gateway_parameter === 'document_number') {
            $msg .= ' Verifique o seu documento. ';
        } elseif ($gateway_parameter === 'legal_name') {
            if ($gateway_message[1] === 'Value too long') {
                $msg .= ' Reduza o tamanho do nome. ';
            } else {
                $msg .= ' Erro no nome. ';
            }
        } elseif ($gateway_parameter === 'bank_code') {
            if ($gateway_message[1] === 'Value too short') {
                $msg .= ' Código do banco muito curto. ';
            } else {
                $msg .= ' Erro no código bancário. ';
            }
        } elseif ($gateway_parameter === 'conta_dv') {
            if ($gateway_message[1] === 'Value is required') {
                $msg .= ' O dígito da conta é obrigatório. ';
            } else {
                $msg .= ' Erro no dígito da conta. ';
            }
        } elseif ($gateway_parameter === 'agencia_dv') {
            if ($gateway_message[1] === 'Value is required') {
                $msg .= ' O dígito da agência é obrigatório. ';
            } else {
                $msg .= ' Erro no dígito da agência. ';
            }
        } elseif ($gateway_parameter === 'bank_account_id') {
            if ($gateway_message[1] === 'The new bank account should have the same document number as the previous') {
                $msg .= ' A conta bancária precisa ter o mesmo documento (CPF ou CNPJ) do cadastro. ';
            } else {
                $msg .= $gateway_message[1];
            }
        } elseif ($gateway_parameter === 'value') {
            if ($gateway_message[1] === 'Value is required') {
                $msg .= ' O valor é obrigatório.';
            } else {
                $msg .= $gateway_message[1];
            }
        } elseif ($gateway_parameter === 'customer[phone][number]') {
            if ($gateway_message[1] === 'número de telefone inválido') {
                $msg .= ' Número de telefone inválido. Verifique se o número está correto.';
            } else {
                $msg .= $gateway_message[1];
            }
        } else {
            $msg .= " [{$gateway_parameter}] {$gateway_message[1]}";
        }

        throw new IntegrationException($msg, $errors, null, $e);
    }

    public static function checkMarketplaceMode()
    {
        if(Constants::isMarketplaceMultiple()) {
            throw new \Rockapps\RkLaravel\Exceptions\ResourceException('Não é permitido criar contas no modo Marketplace Mútiplo');
        }
    }

}
