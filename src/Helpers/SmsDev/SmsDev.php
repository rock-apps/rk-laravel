<?php

namespace Rockapps\RkLaravel\Helpers\SmsDev;

use Rockapps\RkLaravel\Exceptions\IntegrationException;
use Zenvia\Model\Sms;
use Zenvia\Model\SmsFacade;

class SmsDev
{
    /**
     * @param $to
     * @param $msg
     * @return bool
     */
    public static function sendSms($to, $msg)
    {
        $api = self::initApi();

        $to = preg_replace('/[^0-9]/', '', $to);
        $send = $api->send($to, $msg);
        if ($send) {
            return true;
        }

        throw new IntegrationException('Erro ao enviar o SMS.', $api->getResult());
    }

    /**
     * @return \enricodias\SmsDev
     */
    public static function initApi()
    {
        $api_key = env('SMS_DEV_API_KEY');

        if (!$api_key) throw new IntegrationException('O env SMS_DEV_API_KEY não está configurado');
        return new \enricodias\SmsDev($api_key);
    }


}
