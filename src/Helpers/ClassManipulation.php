<?php

namespace Rockapps\RkLaravel\Helpers;

use Illuminate\Support\Str;

class ClassManipulation
{
    public static function extractClassSlugName($class)
    {
        if(!$class) return '';
        $class = get_class($class);
        $model_elements = explode('\\', $class);
        $model = $model_elements[count($model_elements) - 1];
        return Str::slug(Str::lower($model));
    }
}
