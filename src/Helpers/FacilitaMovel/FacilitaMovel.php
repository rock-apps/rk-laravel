<?php

namespace Rockapps\RkLaravel\Helpers\FacilitaMovel;

use Rockapps\RkLaravel\Exceptions\IntegrationException;

class FacilitaMovel
{
    /**
     * @param $to
     * @param $msg
     * @return bool
     */
    public static function sendSms($to, $msg)
    {
        self::initApi();

        $user = env('FACILITAMOVEL_USER');
        $pw = env('FACILITAMOVEL_PASSWORD');

        $to = preg_replace('/[^0-9]/', '', $to);
        $msgEncoded = urlencode($msg);
        $urlChamada = "https://www.facilitamovel.com.br/api/simpleSend.ft?user=" . $user . "&password=" . $pw . "&destinatario=" . $to . "&msg=" . $msgEncoded;
        $return = explode(";", file_get_contents($urlChamada));

        if (in_array($return[0], [5, 6])) {
            return true;
        }

        if (in_array($return[0], [3])) {
            throw new IntegrationException('Número Inválido.', $return);
        }

        if (in_array($return[0], [1, 2, 4])) {
            throw new IntegrationException('Erro interno ao enviar o SMS.', $return);
        }
        throw new IntegrationException('Erro interno ao enviar o SMS.', $return);
    }

    /**
     * @return bool
     */
    public static function initApi()
    {
        $api_user = env('FACILITAMOVEL_USER');
        $api_pw = env('FACILITAMOVEL_PASSWORD');

        if (!$api_user) throw new IntegrationException('O env FACILITAMOVEL_USER não está configurado');
        if (!$api_pw) throw new IntegrationException('O env FACILITAMOVEL_PASSWORD não está configurado');
        return true;
    }
}
