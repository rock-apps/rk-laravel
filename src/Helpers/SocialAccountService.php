<?php

namespace Rockapps\RkLaravel\Helpers;

use Laravel\Socialite\Two\User as ProviderUser;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Models\UserSocialAccount;

class SocialAccountService
{
    /**
     * Find or create user instance by provider user instance and provider name.
     *
     * @param ProviderUser $providerUser
     * @param string $provider
     *
     * @return User
     */
    public function findOrCreate(ProviderUser $providerUser, string $provider): User
    {
        /**
         * @var UserSocialAccount $linkedSocialAccount
         * @var User $user
         */
        $linkedSocialAccount = UserSocialAccount::where('provider_name', $provider)
            ->where('provider_id', $providerUser->getId())
            ->first();

        if ($linkedSocialAccount) {
            return $linkedSocialAccount->user;
        } else {
            $user = null;

            if ($email = $providerUser->getEmail()) {
                $user = User::where('email', $email)->first();
            }

            if (!$user) {
                $user = User::create([
                    'password' => microtime(),
                    'name' => $providerUser->getName() ? $providerUser->getName() : explode('@', $providerUser->getEmail())[0],
                    'email' => $providerUser->getEmail(),
                    'photo' => $providerUser->getAvatar(),
                    'verified' => 1
                ]);
            } else {
                $user->verified = 1;
                $user->save();
            }

            $user->linkedSocialAccounts()->create([
                'provider_id' => $providerUser->getId(),
                'provider_name' => $provider,
            ]);

            return $user;
        }
    }
}
