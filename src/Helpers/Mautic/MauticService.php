<?php

namespace Rockapps\RkLaravel\Helpers\Mautic;

use Mautic\Auth\ApiAuth;
use Mautic\MauticApi;
use Rockapps\RkLaravel\Helpers\Sentry;
use Rockapps\RkLaravel\Models\User;

class MauticService
{
    /**
     * @var mixed
     */
    public $apiUrl;
    /**
     * @var \Mautic\Auth\AuthInterface
     */
    public $auth;

    public function __construct()
    {
        $settings = [
            'userName' => env('MAUTIC_API_USERNAME'),
            'password' => env('MAUTIC_API_PASSWORD'),
        ];

        $this->apiUrl = env('MAUTIC_API_URL');

        $initAuth = new ApiAuth();
        $this->auth = $initAuth->newAuth($settings, 'BasicAuth');
    }

    /**
     * @param User $user
     * @return bool|mixed
     * @throws \Mautic\Exception\ContextNotFoundException
     */
    public function createContact(User $user)
    {
        $api = new MauticApi();
        $contactApi = $api->newApi('contacts', $this->auth, $this->apiUrl);

        // Create the contact
        $response = $contactApi->create([
            'firstname' => $user->splitName()[0],
            'lastname' => $user->splitName()[1],
            'email' => $user->email,
        ]);

        $r = $this->processResponse($response);
        if ($r) {
            return $r['contact'];
        }
        return false;
    }
    /**
     * @param User $user
     * @return bool|mixed
     * @throws \Mautic\Exception\ContextNotFoundException
     */
    public function createContactEmail($email)
    {
        $api = new MauticApi();
        $contactApi = $api->newApi('contacts', $this->auth, $this->apiUrl);

        // Create the contact
        $response = $contactApi->create([
            'email' => $email,
        ]);

        $r = $this->processResponse($response);
        if ($r) {
            return $r['contact'];
        }
        return false;
    }
    /**
     * @param User $user
     * @return bool|mixed
     * @throws \Mautic\Exception\ContextNotFoundException
     */
    public function createContactCustom($fields)
    {
        $api = new MauticApi();
        $contactApi = $api->newApi('contacts', $this->auth, $this->apiUrl);

        // Create the contact
        $response = $contactApi->create($fields);

        $r = $this->processResponse($response);
        if ($r) {
            return $r['contact'];
        }
        return false;
    }

    public function processResponse($response)
    {
        if (is_array($response) && array_key_exists('errors', $response)) {
            dump($response['errors']);
            Sentry::capture('Erro de integração Mautic', $response['errors']);
            return false;
        }

        return $response;
    }

    /**
     * @param $m_contact_id
     * @param $m_campaign_id
     * @return bool
     * @throws \Mautic\Exception\ContextNotFoundException
     */
    public function addContactToCampaign($m_contact_id, $m_campaign_id)
    {
        $api = new MauticApi();
        $campaignApi = $api->newApi("campaigns", $this->auth, $this->apiUrl);
        $response = $campaignApi->addContact($m_campaign_id, $m_contact_id);

        $r = $this->processResponse($response);
        if (isset($r['success'])) {
            return true;
        }
        return false;
    }

}
