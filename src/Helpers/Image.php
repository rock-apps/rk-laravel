<?php

namespace Rockapps\RkLaravel\Helpers;

use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Image
{
    /**
     * @param $data64
     * @param HasMedia|HasMediaTrait $object
     * @param string $collection
     * @param null $extension
     * @param null $name
     * @return bool|\Spatie\MediaLibrary\Models\Media
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\InvalidBase64Data
     */
    public static function storeMediable($data64, HasMedia $object, $collection = 'default', $extension = null, $name = null, $width = null, $height = null, $sharpen = null)
    {
        if (!$data64) return false;
        if (!$extension) $extension = self::extractExtension($data64);

//        $object_name = ClassManipulation::extractClassSlugName($object);
        $imageName = date('Ymdhis') . '-' . (string)Str::orderedUuid() . $extension;

        $media = $object
            ->addMediaFromBase64($data64)
            ->usingFileName($imageName)
            ->setName($name ?: $imageName);

        if ($width) $media->width($width);
        if ($height) $media->height($height);
        if ($sharpen) $media->sharpen($sharpen);

        return $media->toMediaCollection($collection);
    }

    /**
     * @param $data64
     * @return bool|string
     */
    public static function extractExtension($data64)
    {
        if (strpos($data64, ';base64')) {
            $extension = explode('/', $data64);
            $extension = explode(';', $extension[1]);
            return '.' . $extension[0];
        }
        Sentry::capture('Não foi possível extrair a extensão do arquivo.', ['data' => $data64]);
        return null;
    }

}
