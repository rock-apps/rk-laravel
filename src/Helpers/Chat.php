<?php

namespace Rockapps\RkLaravel\Helpers;

use Musonza\Chat\Models\Conversation;

class Chat
{

    public static function createConversation($channel = null, $title = null, $data = null, $participants = [])
    {
        $conversation = \Chat::createConversation($participants);


        $data['title'] = $title;
        $data['channel'] = $channel;
        $conversation->update(['data' => $data]);

        return $conversation;
    }

    public static function updateConversation($chat_id, $data)
    {
        /** @var Conversation $conversation */
        $conversation = \Chat::conversations()->getById($chat_id);
        $conversation->update(['data' => $data]);
    }

    public static function sendMessage($message, $user_id, $chat_id)
    {
        return \Chat::message($message)
            ->from($user_id)
            ->to(\Chat::conversations()->getById($chat_id))
            ->send();
    }

    public static function addParticipants($chat_id, $participants)
    {
        $chat = \Chat::conversations()->getById($chat_id);
        $chat->addParticipants($participants);
    }

    public static function hasParticipant($chat_id, $user_id)
    {
        foreach (self::getParticipants($chat_id) as $participant) {
            if($participant->id === $user_id) return true;
        }
        return false;
    }

    public static function getParticipants($chat_id)
    {
        $chat = \Chat::conversations()->getById($chat_id);
        return $chat->users;
    }
}
