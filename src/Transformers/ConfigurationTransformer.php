<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\Configuration;

class ConfigurationTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Configuration $configuration
     * @return array
     */
    public function transform(Configuration $configuration)
    {
        return [
            'id' => (int)$configuration->id,
            'configs' => (array)$configuration->configs,
            'created_at' => (string)$configuration->created_at,
            'updated_at' => (string)$configuration->updated_at,
        ];
    }
}
