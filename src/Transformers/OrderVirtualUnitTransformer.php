<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\OrderVirtualUnit;

class OrderVirtualUnitTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param OrderVirtualUnit $order
     * @return array
     */
    public function transform(OrderVirtualUnit $order)
    {
        $user_single_transformer = config('rk-laravel.user.transformer_sigle', UserSingleTransformer::class);
        $product_single_transformer = config('rk-laravel.product.transformer_single', ProductSingleTransformer::class);
        $subscription_transformer = config('rk-laravel.subscription.transformer_single', SubscriptionSingleTransformer::class);
        $voucher_transformer = config('rk-laravel.voucher.transformer', VoucherTransformer::class);
        return [
            'id' => (int)$order->id,
            'user_id' => (int)$order->user_id,
            'total_value' => (float)$order->total_value,
            'virtual_units' => (int)$order->virtual_units,
            'status' => (string)$order->status,
            'mode' => (string)$order->mode,
            'user' => $order->user ? $order->user->transform($user_single_transformer) : [],
            'created_at' => (string)$order->created_at,
            'company_id' => $order->company_id,
            'product_id' => $order->product_id,
            'product' => $order->product ? $order->product->transform($product_single_transformer) : null,
            'subscription_id' => $order->subscription_id,
            'subscription' => $order->subscription ? $order->subscription->transform($subscription_transformer) : null,
            '_next_states' => $order->getNextStates(),
            'voucher_id' => $order->voucher_id,
            'voucher' => $order->voucher ? $order->voucher->transform($voucher_transformer) : null,
        ];
    }
}
