<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Product;
use Rockapps\RkLaravel\Models\Variant;

class ProductTransformer extends TransformerBase
{
    /**
     * A Fractal transformer.
     *
     * @param Product $product
     * @return array
     */
    public function transform(Product $product)
    {
        return [
            'id' => (int)$product->id,
            'name' => (string)($product->name ? $product->name : $product->category->name),
            'description' => (string)$product->description,
            'sku' => (string)$product->sku,
            'active' => (bool)$product->active,
            'unit_value' => (float)$product->unit_value,
            'company_id' => (integer)$product->company_id,
            'categories' => $product->categories->map(function (Category $cat) {
                return $cat->transform(CategoryTransformer::class);
            }),
            'variants' => $product->variants->map(function (Variant $v) {
                return $v->transform(VariantSingleTransformer::class);
            }),
            'medias' => $this->transformMedias($product),
            'created_at' => (string)$product->created_at,
            'updated_at' => (string)$product->updated_at,
            'deleted_at' => isset($product->deleted_at) ? (string)$product->deleted_at : null,

            'type' => (string)$product->type,
            'instance' => (string)$product->instance,
            'variation_property' => (string)$product->variation_property,
            'variation_value' => (string)$product->variation_value,
            'image' => $product->image,

            'weight' => (float)$product->weight,
            'dimension_height' => (float)$product->dimension_height,
            'dimension_width' => (float)$product->dimension_width,
            'dimension_depth' => (float)$product->dimension_depth,

            'virtual_units_release' => (float)$product->virtual_units_release,
            'apple_product_id' => (string)$product->apple_product_id,
            'google_product_id' => (string)$product->google_product_id,
            'active_ios' => (bool)$product->active_ios,
            'active_android' => (bool)$product->active_android,

            'main_product_id' => $product->main_product_id,
        ];
    }
}
