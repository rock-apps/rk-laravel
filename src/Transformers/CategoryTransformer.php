<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\Category;

class CategoryTransformer extends TransformerBase
{
    /**
     * A Fractal transformer.
     *
     * @param Category $category
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'id' => (int)$category->id,
            'name' => (string)$category->name,
            'slug' => (string)$category->slug,
            'description' => (string)$category->description,
            'headline' => (string)$category->headline,
            'color_front' => (string)$category->color_front,
            'color_back' => (string)$category->color_back,
            'icon' => (string)$category->icon,
            'icon_family' => (string)$category->icon_family,
            'medias' => $this->transformMedias($category),
            'active' => (bool)$category->active,
            'sequence' => (int)$category->sequence,
            'model_type' => (string)$category->model_type,
            'created_at' => (string)$category->created_at,
            'updated_at' => (string)$category->updated_at,
            'deleted_at' => isset($category->deleted_at) ? (string)$category->deleted_at : null,
        ];
    }
}
