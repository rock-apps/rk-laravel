<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\Parameter;

class ParameterTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Parameter $parameter
     * @return array
     */
    public function transform(Parameter $parameter)
    {
        $return = [
            'id' => (int)$parameter->id,
            'key' => (string)$parameter->key,
            'type' => (string)$parameter->type,
            'name' => (string)$parameter->name,
            'readonly' => (string)$parameter->readonly,
            'group' => (string)$parameter->group,
            'value' => (string)$parameter->value,
            'value_float' => (float)$parameter->value_float,
            'value_bool' => (bool)$parameter->value_bool,
            'value_currency' => (float)$parameter->value_currency,
            'value_int' => (int)$parameter->value_int,
            'description' => (string)$parameter->description,
        ];

        return $return;
    }
}
