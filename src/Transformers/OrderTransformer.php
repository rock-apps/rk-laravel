<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\Item;
use Rockapps\RkLaravel\Models\Order;
use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Transformers\CreditCardTransformer;
use Rockapps\RkLaravel\Transformers\PaymentTransformer;

class OrderTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Order $order
     * @return array
     */
    public function transform(Order $order)
    {
        return [
            'id' => (int)$order->id,
            'is_paid' => (boolean)$order->is_paid,
            'payment_gateway' => (string)$order->payment_gateway,
            'status' => (string)$order->status,
            'description' => (string)$order->description,
            'conversation_id' => (int)$order->conversation_id,
            'user_id' => (int)$order->user_id,
            'user' => $order->user ? $order->user->transform(UserSingleTransformer::class) : null,
            'address_id' => (int)$order->address_id,
            'address_street' => (string)$order->address_street,
            'address_number' => (string)$order->address_number,
            'address_complement' => (string)$order->address_complement,
            'address_district' => (string)$order->address_district,
            'address_zipcode' => (string)$order->address_zipcode,
            'address_city' => (string)$order->address_city,
            'address_state' => (string)$order->address_state,
            'reject_reason' => (string)$order->reject_reason,
            'voucher_code' => (string)$order->voucher_code,
            'voucher_id' => (int)$order->voucher_id,
            'voucher_discount_value' => (float)$order->voucher_discount_value,
            'total_value' => (float)$order->total_value,
            'company_id' => (int)$order->company_id,
            'company' => $order->company ? $order->company->transform(CompanySingleTransformer::class) : null,
            'credit_card_id' => (int)$order->credit_card_id,
            'credit_card' => $order->credit_card ? $order->credit_card->transform(CreditCardTransformer::class) : null,
            'validated_at' => (string)$order->approved_at,
            'deliver_value' => (float)$order->deliver_value,
            'items' => $order->items->map(function (Item $item) {
                return $item->transform(OrderItemTransformer::class);
            }),
            'payment' => $order->payments->map(function (Payment $payment) {
                return $payment->transform(PaymentTransformer::class);
            }),
            'rating' => (float)$order->rating,
            'rating_description' => (string)$order->rating_description,
            'deliver_scheduled_at' => (string)$order->deliver_scheduled_at,
            'delivered_at' => (string)$order->delivered_at,
            'rejected_at' => (string)$order->rejected_at,
            'created_at' => (string)$order->created_at,
            'updated_at' => (string)$order->updated_at,
            'deleted_at' => isset($order->deleted_at) ? (string)$order->deleted_at : null,
        ];
    }
}
