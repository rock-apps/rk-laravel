<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;

class UserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        $return = [
            'id' => (int)$user->id,
            'value' => (int)$user->id,
            'fid' => 'USR-' . $user->id,
            'name' => (string)$user->name,
            'email' => (string)$user->email,
            'birth_date' => (string)$user->birth_date,
            'is_support' => (bool)$user->is_support,
            'description' => (string)$user->description,
            'photo' => (string)$user->photo,
            'document' => (string)$user->document,
            'gender' => (string)$user->gender,
            'admin_comments' => (string)$user->admin_comments,
            'invitation_code' => (string)$user->invitation_code,
            'invited_by' => (int)$user->invited_by,
            'user_lng' => (float)$user->user_lng,
            'user_lat' => (float)$user->user_lat,
            'mobile' => (string)$user->mobile,
            'telephone' => (string)$user->telephone,
            'verified' => (int)$user->verified,
            'suspended' => (int)$user->suspended,
            'online_status' => (string)($user->show_online_status ? $user->online_status : User::ONLINE_STATUS_DISABLED),
            'show_online_status' => null,
            'can_pay_with_boleto' => (int)$user->can_pay_with_boleto,
            'can_pay_with_bank' => (int)$user->can_pay_with_bank,
            'can_pay_with_cash' => (int)$user->can_pay_with_cash,
            'can_pay_with_cc' => (int)$user->can_pay_with_cc,
            'can_pay_with_pix' => (int)$user->can_pay_with_pix,
            'education' => (string)$user->education,
            'handcap' => (string)$user->handcap,
            'created_at' => (string)$user->created_at,
            'updated_at' => (string)$user->updated_at,
            'deleted_at' => isset($user->deleted_at) ? (string)$user->deleted_at : null,
        ];

        foreach (Role::all() as $role) {
            /** @var $role Role */
            $return['roles'][$role->name] = $user->hasRole($role->name);
        }

        $logged_user = \Auth::getUser();
        if ($logged_user && ($logged_user->id === $user->id || $logged_user->hasRole(Constants::ROLE_ADMIN))) {
            $return['show_online_status'] = $user->show_online_status;
            $return['virtual_unit_balance'] = (int)$user->virtual_unit_balance;
            $return['last_access_at'] = (string)$user->last_access_at;
        }

        return $return;
    }
}
