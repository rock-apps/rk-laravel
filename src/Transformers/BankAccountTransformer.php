<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\BankAccount;

class BankAccountTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param BankAccount $bankAccount
     * @return array
     */
    public function transform(BankAccount $bankAccount)
    {
        $return =  [
            'id' => (int)$bankAccount->id,
            'bank' => BankAccount::bankFind($bankAccount->bank_code),
            'bank_code' => (int)$bankAccount->bank_code,
            'agencia' => (int)$bankAccount->agencia,
            'agencia_dv' => (string)$bankAccount->agencia_dv,
            'conta' => (int)$bankAccount->conta,
            'conta_dv' => $bankAccount->conta_dv,
            'document_number' => (string)$bankAccount->document_number,
            'pix_key' => (string)$bankAccount->pix_key,
            'gateway' => (string)$bankAccount->gateway,
            'legal_name' => (string)$bankAccount->legal_name,
            'type' => (string)$bankAccount->type,
            'charge_transfer_fees' => (bool)$bankAccount->charge_transfer_fees,
            'owner_type' => (string)$bankAccount->owner_type,
            'owner_id' => (integer)$bankAccount->owner_id,
            'created_at' => (string)$bankAccount->created_at,
            'updated_at' => (string)$bankAccount->updated_at,
            'deleted_at' => isset($bankAccount->deleted_at) ? (string)$bankAccount->deleted_at : null,
        ];

        $user = \Auth::getUser();
        if($user && $user->hasRole('admin')) {

            $return['pgm_recipient_id'] = $bankAccount->pgm_recipient_id;
            $return['transfer_interval'] = $bankAccount->transfer_interval;
            $return['transfer_day'] = $bankAccount->transfer_day;
            $return['transfer_enabled'] = $bankAccount->transfer_enabled;
            $return['anticipatable_volume_percentage'] = $bankAccount->anticipatable_volume_percentage;
            $return['automatic_anticipation_enabled'] = $bankAccount->automatic_anticipation_enabled;
            $return['automatic_anticipation_type'] = $bankAccount->automatic_anticipation_type;
            $return['automatic_anticipation_days'] = $bankAccount->automatic_anticipation_days;
            $return['automatic_anticipation_1025_delay'] = $bankAccount->automatic_anticipation_1025_delay;
        }

        return $return;
    }
}
