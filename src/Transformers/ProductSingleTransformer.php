<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\Product;
use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Transformers\CategoryTransformer;
use Rockapps\RkLaravel\Transformers\TransformerBase;

class ProductSingleTransformer extends TransformerBase
{
    /**
     * A Fractal transformer.
     *
     * @param Product $product
     * @return array
     */
    public function transform(Product $product)
    {
        return [
            'id' => (int)$product->id,
            'name' => (string)$product->name,
            'sku' => (string)$product->sku,
            'description' => (string)$product->description,
            'active' => (bool)$product->active,
            'unit_value' => (float)$product->unit_value,
            'categories' => $product->categories->map(function (Category $cat) {
                return $cat->transform(CategoryTransformer::class);
            }),
            'medias' => $this->transformMedias($product),
        ];
    }
}
