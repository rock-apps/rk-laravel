<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\Permission;
use League\Fractal\TransformerAbstract;

class PermissionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Permission $permission
     * @return array
     */
    public function transform(Permission $permission)
    {
        $return = [
            'id' => (int)$permission->id,
            'name' => (string)$permission->name,
            'system' => (bool)$permission->system,
            'display_name' => (string)$permission->display_name,
            'description' => (string)$permission->description,
        ];

        return $return;
    }
}
