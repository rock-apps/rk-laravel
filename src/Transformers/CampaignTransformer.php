<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\Campaign;
use Rockapps\RkLaravel\Models\CampaignElement;

class CampaignTransformer extends TransformerBase
{
    /**
     * A Fractal transformer.
     *
     * @param Campaign $campaign
     * @return array
     */
    public function transform(Campaign $campaign)
    {
        return [
            'id' => (int)$campaign->id,
            'name' => (string)$campaign->name,
            'url' => (string)$campaign->url,
            'active' => (bool)$campaign->active,
            'company_id' => (int)$campaign->company_id,
            'actual_views' => (int)$campaign->actual_views,
            'actual_clicks' => (int)$campaign->actual_clicks,
            'max_views' => (int)$campaign->max_views,
            'max_clicks' => (int)$campaign->max_clicks,
            'priority' => (int)$campaign->priority,
            'start_at' => (string)$campaign->start_at,
            'end_at' => (string)$campaign->end_at,
            'elements' => $campaign->elements->map(function (CampaignElement $element) {
                return $element->transform(CampaignElementTransformer::class);
            }),
            'created_at' => (string)$campaign->created_at,
            'updated_at' => (string)$campaign->updated_at,
        ];
    }
}

