<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\Banner;

class BannerTransformer extends TransformerBase
{
    /**
     * A Fractal transformer.
     *
     * @param Banner $banner
     * @return array
     */
    public function transform(Banner $banner)
    {
        return [
            'id' => (int)$banner->id,
            'title' => (string)$banner->title,
            'object_id' => (int)$banner->object_id,
            'object_type' => (string)$banner->object_type,
            'active' => (boolean)$banner->active,
            'link_url' => (string)$banner->link_url,
            'image' => (string)$banner->image,
            'position' => (string)$banner->position,
            'medias' => $this->transformMedias($banner),
            'created_at' => (string)$banner->created_at,
            'updated_at' => (string)$banner->updated_at,
            'deleted_at' => isset($banner->deleted_at) ? (string)$banner->deleted_at : null,
        ];
    }
}
