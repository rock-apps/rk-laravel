<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\CampaignElement;

class CampaignElementTransformer extends TransformerBase
{
    /**
     * A Fractal transformer.
     *
     * @param CampaignElement $campaign_element
     * @return array
     */
    public function transform(CampaignElement $campaign_element)
    {

        return [
            'id' => (int)$campaign_element->id,
            'active' => (bool)$campaign_element->active,
            'campaign_id' => (int)$campaign_element->campaign_id,
            'element_id' => (int)$campaign_element->element_id,
            'element_type' => (string)$campaign_element->element_type,
            'element' => $campaign_element->element ? $campaign_element->element->transform() : [],
            'priority' => (int)$campaign_element->priority,
            'created_at' => (string)$campaign_element->created_at,
            'updated_at' => (string)$campaign_element->created_at,
        ];
    }
}

