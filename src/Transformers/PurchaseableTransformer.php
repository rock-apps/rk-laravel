<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Interfaces\PurchaseableInterface;

class PurchaseableTransformer extends TransformerAbstract
{
    public function transform(PurchaseableInterface $purchaseable)
    {
        return [
            'id' => (int)$purchaseable->id,
        ];
    }
}
