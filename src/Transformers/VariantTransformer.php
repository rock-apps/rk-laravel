<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Variant;

class VariantTransformer extends TransformerBase
{
    /**
     * A Fractal transformer.
     *
     * @param Variant $variant
     * @return array
     */
    public function transform(Variant $variant)
    {
        return [
            'id' => (int)$variant->id,
            'name' => (string)$variant->name,
            'type' => (string)$variant->type,
            'value' => $variant->getCastedValue(),
            'value_cast' => (string)strtoupper($variant->value_cast),
            'description' => (string)$variant->description,
            'icon' => (string)$variant->icon,
            'color' => (string)$variant->color,
            'image' => (string)$variant->image,
            'url' => (string)$variant->url,
            'icon_family' => (string)$variant->icon_family,
            'style' => (string)$variant->style,
            'medias' => $this->transformMedias($variant)
//            'categories' => $variant->categorias()->get()->map(function (Category $category) {
//                return $category->transform();
//            }),
        ];
    }
}
