<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\Subscription;

class SubscriptionSingleTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Subscription $subscription
     * @return array
     */
    public function transform(Subscription $subscription)
    {
        $user = \Auth::getUser();
        $return = [
            'id' => (int)$subscription->id,
            'status' => (string)$subscription->status,
            'gateway' => (string)$subscription->gateway,
            'payment_method' => (string)$subscription->payment_method,
            'credit_card_id' => (int)$subscription->credit_card_id,
            'mode' => (string)$subscription->mode,

            'plan_id' => (int)$subscription->plan_id,
            'plan' => $subscription->plan ? $subscription->plan->transform(PlanSingleTransformer::class) : null,
            'pgm_subscription_id' => (string)$subscription->pgm_subscription_id,
            'android_transaction_id' => (string)$subscription->android_transaction_id,
            'apple_transaction_id' => (string)$subscription->apple_transaction_id,
            'subscriber_type' => (string)$subscription->subscriber_type,
            'subscriber_id' => (int)$subscription->subscriber_id,
            'subscriber' => $subscription->subscriber ? [
                'name' => (string)$subscription->subscriber->name ? $subscription->subscriber->name : null,
                'id' => (int)$subscription->subscriber->id,
            ] : [],
            'payer_id' => (int)$subscription->payer_id,
            'payer_type' => (string)$subscription->payer_type,
            'payer' => $subscription->payer ? [
                'name' => (string)$subscription->payer->name ? $subscription->payer->name : null,
                'id' => (int)$subscription->payer->id,
            ] : [],
            'subscribed_at' => (string)$subscription->subscribed_at,
            'expired_at' => (string)$subscription->expired_at,
            'renewed_at' => (string)$subscription->renewed_at,
            'product' => $subscription->product ? $subscription->product->transform(ProductSingleTransformer::class) : null,
            'product_id' => $subscription->product_id,
            'current_period_start' => (string)$subscription->current_period_start,
            'current_period_end' => (string)$subscription->current_period_end,

            'is_active' => (bool)$subscription->isActive(),

            'created_at' => (string)$subscription->created_at,
            'updated_at' => (string)$subscription->updated_at,
            'deleted_at' => isset($subscription->deleted_at) ? (string)$subscription->deleted_at : null,
        ];

        if ($user && $user->hasRole('admin')) {
            $return['comments_operator'] = (string)$subscription->comments_operator;
        }


        return $return;
    }
}
