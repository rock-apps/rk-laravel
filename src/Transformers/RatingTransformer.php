<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\Rating;
use Rockapps\RkLaravel\Models\Role;
use League\Fractal\TransformerAbstract;

class RatingTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Rating $rate
     * @return array
     */
    public function transform(Rating $rate)
    {
        return [
            'id' => (int)$rate->id,
            'rating' => (string)$rate->value,
            'model' => $rate->model ? $rate->model->transform(config('rk-laravel.rating.user_transformer', UserSingleTransformer::class)) : null,
            'description' => (string)$rate->description,
            'created_at' => (string)$rate->created_at,
        ];
    }
}
