<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use OwenIt\Auditing\Models\Audit;

class AuditTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Audit $audit
     * @return array
     */
    public function transform($audit)
    {
        return [
            'id' => (int)$audit->id,
            'event' => $audit->event,
            'url' => $audit->url,
            'ip_address' => $audit->ip_address,
            'user_agent' => $audit->user_agent,
            'tags' => $audit->tags,
            'modified' => $audit->getModified(),
            'user_id' => (int)$audit->user_id,
            'user_type' => (string)$audit->user_type,
            'created_at' => (string)$audit->created_at,
            'updated_at' => (string)$audit->updated_at,
        ];
    }
}
