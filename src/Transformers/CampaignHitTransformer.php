<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\CampaignHit;
use Rockapps\RkLaravel\Transformers\TransformerBase;

class CampaignHitTransformer extends TransformerBase
{
    /**
     * A Fractal transformer.
     *
     * @param CampaignHit $campaign_hit
     * @return array
     */
    public function transform(CampaignHit $campaign_hit)
    {
        $return = [
            'id' => (int)$campaign_hit->id,
            'ip' => (string)$campaign_hit->ip,
            'agent' => (string)$campaign_hit->agent,
            'created_at' => (string)$campaign_hit->created_at,
            'updated_at' => (string)$campaign_hit->created_at,
        ];

        return $return;
    }
}

