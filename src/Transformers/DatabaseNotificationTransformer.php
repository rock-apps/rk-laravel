<?php

namespace Rockapps\RkLaravel\Transformers;

use Illuminate\Notifications\DatabaseNotification;
use League\Fractal\TransformerAbstract;

class DatabaseNotificationTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param DatabaseNotification $notification
     * @return array
     */
    public function transform(DatabaseNotification $notification)
    {
        return [
            'id' => (string)$notification->id,
            'type' => $notification->type,
            'data' => $notification->data,
            'read_at' => (string)$notification->read_at,
            'created_at' => (string)$notification->created_at,
        ];
    }
}
