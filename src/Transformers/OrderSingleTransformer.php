<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\Order;
use League\Fractal\TransformerAbstract;

class OrderSingleTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Order $transaction
     * @return array
     */
    public function transform(Order $transaction)
    {
        $return = [
            'id' => (int)$transaction->id,
            'status' => (string)$transaction->status,
        ];

        return $return;
    }
}
