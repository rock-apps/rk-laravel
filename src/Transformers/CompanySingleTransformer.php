<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Transformers\CategoryTransformer;
use Rockapps\RkLaravel\Transformers\TransformerBase;

class CompanySingleTransformer extends TransformerBase
{
    /**
     * A Fractal transformer.
     *
     * @param company $company
     * @return array
     */
    public function transform(Company $company)
    {
        return [
            'id' => (int)$company->id,
            'name' => (string)$company->name,
            'headline' => (string)$company->headline,
            'rating' => (float)$company->calculateRating(),
            'min_value_to_free_deliver' => (float)$company->min_value_to_free_deliver,
            'deliver_estimate_time' => (int)$company->deliver_estimate_time,
            'deliver_max_range' => (int)$company->deliver_max_range,
            'deliver_value' => (float)$company->deliver_value,
            'medias' => $this->transformMedias($company),
            'user_distance' => $company->getAttribute('user_distance'), // Enviado pelo transforme index
            'categories' => $company->categories->map(function (Category $cat) {
                return $cat->transform(CategoryTransformer::class);
            }),
        ];
    }
}
