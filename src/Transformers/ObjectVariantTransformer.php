<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\ObjectVariant;

class ObjectVariantTransformer extends TransformerBase
{
    /**
     * A Fractal transformer.
     *
     * @param ObjectVariant $objectVariant
     * @return array
     */
    public function transform(ObjectVariant $objectVariant)
    {
        return [
            'id' => (int)$objectVariant->id,
            'object_id' => (int)$objectVariant->object_id,
            'object_type' => (string)$objectVariant->object_type,
            'variant_id' => (int)$objectVariant->variant_id,
        ];
    }
}
