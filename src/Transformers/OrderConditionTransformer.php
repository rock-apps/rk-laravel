<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\Item;
use League\Fractal\TransformerAbstract;

class OrderConditionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Item $item
     * @return array
     */
    public function transform(Item $item)
    {
        return [
            'id' => (int)$item->id,
            'comments' => (string)$item->comments,
            'product_id' => (int)$item->product_id,
            'product' => $item->product ? $item->product->transform(ProductSingleTransformer::class) : null,
            'order_id' => (int)$item->order_id,
            'quantity' => (int)$item->quantity,
            'unit_value' => (float)$item->unit_value,
            'total_value' => (float)$item->total_value,
        ];
    }
}
