<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Musonza\Chat\Models\Message;

class ChatMessageTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Message $message
     * @return array
     */
    public function transform(Message $message)
    {

        return [
            'id' => (int)$message->id,
            'message' => $message->body,
            'user' => $message->sender ? $message->sender->transform(UserSingleTransformer::class) : null,
            'created_at' => (string)$message->created_at,
        ];
    }
}
