<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\Address;

class AddressTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Address $address
     * @return array
     */
    public function transform(Address $address)
    {
        return [
            'id' => (int)$address->id,
            'default' => (bool)$address->default,
            'name' => (string)$address->name,
            'street' => (string)$address->street,
            'number' => (string)$address->number,
            'complement' => (string)$address->complement,
            'district' => (string)$address->district,
            'zipcode' => (string)$address->zipcode,
            'city' => (string)$address->city,
            'state' => (string)$address->state,
            'country' => (string)$address->country,
            'lat' => (string)$address->lat,
            'lng' => (string)$address->lng,
//            'related' => $address->related ? $address->related->transform() : null, // Não enviar pois pode dar erro
            'related_id' => (int)$address->related_id,
            'related_type' => (string)$address->related_type,
            'created_at' => (string)$address->created_at,
            'updated_at' => (string)$address->updated_at,
            'deleted_at' => isset($address->deleted_at) ? (string)$address->deleted_at : null,
        ];
    }
}
