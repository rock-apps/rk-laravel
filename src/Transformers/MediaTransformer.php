<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;

class MediaTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param \Spatie\MediaLibrary\Models\Media $media
     * @return array
     */
    public function transform(\Spatie\MediaLibrary\Models\Media $media)
    {
        $urls = [
            'default' => $media->getUrl()
        ];
        foreach ($media['custom_properties'] as $custom_properties) {
            foreach ($custom_properties as $key => $value) {
                if ($value) $urls[$key] = $media->getUrl($key);
            }
        }
        return [
            'id' => (int)$media->id,
            'collection_name' => (string)$media->collection_name,
            'order_column' => (string)$media->order_column,
            'url' => $urls,
            'size' => (integer)$media->size,
            'mime_type' => (string)$media->mime_type,
            "file_name" => (string)$media->file_name,
        ];
    }
}
