<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\Media;
use Rockapps\RkLaravel\Traits\HasCategories;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class TransformerBase extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param HasMediaTrait|HasMedia $object
     * @return array
     */
    public function transformMedias($object)
    {
        $medias = [];
        foreach ($object->media as $media) {
            $transformer = MediaTransformer::class;
            $medias[] = fractal($media, new $transformer())->toArray()['data'];
        }
        return $medias;
    }
    /**
     * A Fractal transformer.
     *
     * @param HasCategories $object
     * @return array
     */
    public function transformCategories($object)
    {
        return $object->categories->map(function (Category $cat) {
            return $cat->transform();
        });
    }


}
