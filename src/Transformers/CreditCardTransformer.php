<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\CreditCard;

class CreditCardTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param CreditCard $creditCard
     * @return array
     */
    public function transform(CreditCard $creditCard)
    {
        return [
            'id' => (int)$creditCard->id,
            'last_digits' => (string)$creditCard->last_digits,
            'brand' => (string)$creditCard->brand,
            'holder_name' => (string)$creditCard->holder_name,
            'expiration_date' => (string)$creditCard->expiration_date,
            'valid' => (bool)$creditCard->valid,
            'default' => (bool)$creditCard->default,
            'created_at' => (string)$creditCard->created_at,
            'updated_at' => (string)$creditCard->updated_at,
            'deleted_at' => isset($creditCard->deleted_at) ? (string)$creditCard->deleted_at : null,
        ];
    }
}
