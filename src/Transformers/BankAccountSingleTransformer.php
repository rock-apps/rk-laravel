<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\BankAccount;

class BankAccountSingleTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param BankAccount $bankAccount
     * @return array
     */
    public function transform(BankAccount $bankAccount)
    {
        $return = [
            'id' => (int)$bankAccount->id,
            'bank' => BankAccount::bankFind($bankAccount->bank_code),
            'bank_code' => (int)$bankAccount->bank_code,
            'agencia' => (int)$bankAccount->agencia,
            'agencia_dv' => (string)$bankAccount->agencia_dv,
            'conta' => (int)$bankAccount->conta,
            'conta_dv' => $bankAccount->conta_dv,
            'document_number' => (string)$bankAccount->document_number,
            'pix_key' => (string)$bankAccount->pix_key,
            'gateway' => (string)$bankAccount->gateway,
            'legal_name' => (string)$bankAccount->legal_name,
            'type' => (string)$bankAccount->type,
        ];

        $user = \Auth::getUser();
        if ($user && $user->hasRole('admin')) {
            $return['pgm_recipient_id'] = $bankAccount->pgm_recipient_id;
        }

        return $return;
    }
}
