<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\PaymentMethod;

class PaymentMethodTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param PaymentMethod $payment_method
     * @return array
     */
    public function transform(PaymentMethod $payment_method)
    {
        $return = [
            'id' => (int)$payment_method->id,
            'gateway' => (string)$payment_method->gateway,
            'method' => (string)$payment_method->method,
            'title' => (string)$payment_method->title,
            'description' => (string)$payment_method->description,
            'image' => (string)$payment_method->image,
            'bank_account_id' => (integer)$payment_method->bank_account_id,
            'active' => (bool)$payment_method->active,
            'company_id' => (integer)$payment_method->company_id,
            'created_at' => (string)$payment_method->created_at,
            'updated_at' => (string)$payment_method->updated_at,
            'deleted_at' => isset($payment_method->deleted_at) ? (string)$payment_method->deleted_at : null,
            'bank_account' => $payment_method->bankAccount ? $payment_method->bankAccount->transform(BankAccountSingleTransformer::class) : null,
            'company' => $payment_method->company ? $payment_method->company->transform(CompanySingleTransformer::class) : null,
        ];
        $user = \Auth::getUser();

        if ($user && ($user->hasRole('admin') || $user->company_id === $payment_method->company_id)) {
            $return['gateway_key'] = (string)$payment_method->gateway_key;
            $return['gateway_secret'] = (string)$payment_method->gateway_secret;
            $return['gateway_sandbox'] = (bool)$payment_method->gateway_sandbox;
            $return['gateway_webhook'] = (bool)$payment_method->gateway_webhook;
            $return['gateway_soft_descriptor'] = (bool)$payment_method->gateway_soft_descriptor;
        }

        return $return;
    }
}
