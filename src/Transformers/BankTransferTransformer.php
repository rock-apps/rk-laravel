<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\BankTransfer;

class BankTransferTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param BankTransfer $bankTransfer
     * @return array
     */
    public function transform(BankTransfer $bankTransfer)
    {
        $return = [
            'id' => (int)$bankTransfer->id,
            'pgm_transfer_id' => (string)$bankTransfer->pgm_transfer_id,
            'pgm_recipient_id' => (string)$bankTransfer->pgm_recipient_id,
            'pgm_transaction_id' => (string)$bankTransfer->pgm_transaction_id,
            'gateway' => (string)$bankTransfer->gateway,
            'comments_operator' => null,
            'bank_account_id' => $bankTransfer->bank_account_id,
            'status' => (string)$bankTransfer->status,
            'payment_id' => $bankTransfer->payment_id,
            'value' => (float)$bankTransfer->value,
            'virtual_units' => (int)$bankTransfer->virtual_units,
            'virtual_units_tax' => (float)$bankTransfer->virtual_units_tax,
            'fee' => (float)$bankTransfer->fee,
            'type' => (string)$bankTransfer->type,
            '_next_states' => $bankTransfer->getNextStates(),
            'transfered_type' => (string)$bankTransfer->transfered_type,
            'transfered_id' => (integer)$bankTransfer->transfered_id,
            'created_at' => (string)$bankTransfer->created_at,
            'updated_at' => (string)$bankTransfer->updated_at,
            'deleted_at' => isset($bankTransfer->deleted_at) ? (string)$bankTransfer->deleted_at : null,
        ];
        $user = \Auth::getUser();
        if ($user && $user->hasRole('admin')) {
            $return['comments_operator'] = (string)$bankTransfer->comments_operator;
            $return['bank_account'] = $bankTransfer->bankAccount ? $bankTransfer->bankAccount->transform(BankAccountSingleTransformer::class) : null;
        }

        return $return;
    }
}
