<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\Comment;

class CommentTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Comment $comment
     * @return array
     */
    public function transform(Comment $comment)
    {
        $fractal = fractal($comment->commenter, new UserSingleTransformer);
        return [
            'id' => (int)$comment->id,
            'commentable_type' => (string)$comment->commentable_type,
            'commentable_id' => (int)$comment->commentable_id,
            'commenter_type' => (string)$comment->commenter_type,
            'commenter_id' => (int)$comment->commenter_id,
            'comment' => (string)$comment->getAttribute('comment'),
            'user_id' => $comment->commenter ? $comment->commenter->id : null,
            'user' => $fractal->toArray()['data'],
            'created_at' => (string)$comment->created_at,
        ];
    }
}
