<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Models\Product;

class CompanyTransformer extends TransformerBase
{
    /**
     * A Fractal transformer.
     *
     * @param company $company
     * @return array
     */
    public function transform(Company $company)
    {
        $user_transform = config('rk-laravel.user.transformer_sigle', UserSingleTransformer::class);
        return [
            'id' => (int)$company->id,
            'name' => (string)$company->name,
            'headline' => (string)$company->headline,
            'suspended' => (bool)$company->suspended,
            'document' => (string)$company->document,
            'cnpj' => (string)$company->cnpj,
            'email' => (string)$company->email,
            'active' => (bool)$company->active,
            'rating' => (float)$company->calculateRating(),
            'rating_count' => (int)$company->countRating(),
            'is_favorite' => (bool)$company->isFavoritedBy(\Auth::getUser()),
            'min_value_to_free_deliver' => (float)$company->min_value_to_free_deliver,
            'deliver_value' => (float)$company->deliver_value,
            'deliver_estimate_time' => (int)$company->deliver_estimate_time,
            'deliver_max_range' => (int)$company->deliver_max_range,

            'responsible_id' => (int)$company->responsible_id,
            'responsible' => $company->responsible ? $company->responsible->transform($user_transform) : [],

            'categories' => $company->categories->map(function (Category $cat) {
                return $cat->transform(CategoryTransformer::class);
            }),
            'products' => $company->products()->filter()->get()->map(function (Product $product) {
                return $product->transform(ProductTransformer::class);
            }),
            'medias' => $this->transformMedias($company),

            'mobile' => (string)$company->mobile,
            'telephone' => (string)$company->telephone,

            'address_id' => $company->address_id,
            'address' => $company->address ? $company->address->transform(AddressTransformer::class) : null,
            'long' => (string)$company->long,
            'lat' => (string)$company->lat,
            'user_distance' => $company->getAttribute('user_distance'), // Enviado pelo controller index

            'created_at' => (string)$company->created_at,
            'updated_at' => (string)$company->updated_at,
            'deleted_at' => isset($company->deleted_at) ? (string)$company->deleted_at : null,
        ];
    }
}
