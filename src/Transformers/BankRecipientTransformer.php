<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\BankRecipient;

class BankRecipientTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param BankRecipient $bankRecipient
     * @return array
     */
    public function transform(BankRecipient $bankRecipient)
    {
        return [
            'id' => (int)$bankRecipient->id,
            'pgm_recipient_id' => $bankRecipient->pgm_recipient_id,
            'transfer_interval' => $bankRecipient->transfer_interval,
            'transfer_day' => $bankRecipient->transfer_day,
            'transfer_enabled' => $bankRecipient->transfer_enabled,
            'anticipatable_volume_percentage' => $bankRecipient->anticipatable_volume_percentage,
            'automatic_anticipation_enabled' => $bankRecipient->automatic_anticipation_enabled,
            'automatic_anticipation_type' => $bankRecipient->automatic_anticipation_type,
            'automatic_anticipation_days' => $bankRecipient->automatic_anticipation_days,
            'automatic_anticipation_1025_delay' => $bankRecipient->automatic_anticipation_1025_delay,
            'created_at' => (string)$bankRecipient->created_at,
            'updated_at' => (string)$bankRecipient->updated_at,
            'deleted_at' => isset($bankRecipient->deleted_at) ? (string)$bankRecipient->deleted_at : null,
        ];
    }
}
