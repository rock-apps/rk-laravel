<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\Post;
use Rockapps\RkLaravel\Models\PostBlock;

class PostTransformer extends TransformerAbstract
{
    /**
     * @param Post $post
     * @return array
     */
    public function transform(Post $post)
    {
        return [
            'id' => (int)$post->id,
            'title' => (string)$post->title,
            'heading' => (string)$post->heading,
            'slug' => (string)$post->slug,
            'status' => (string)$post->status,
            'visibility' => (string)$post->visibility,
            'blocks' => $post->blocks->map(function (PostBlock $block) {
                return $block->transform(PostBlockTransformer::class);
            }),
            'content' => (string)$post->content,
            'type' => (string)$post->type,
            'owner_id' => (string)$post->owner_id,
            'owner_type' => (string)$post->owner_type,
            'owner' => $post->owner ? $post->owner->transform(config('rk-laravel.user.transformer', UserSingleTransformer::class)) : null,
            'posted_at' => (string)$post->posted_at,
            'approved_at' => (string)$post->approved_at,
            'created_at' => (string)$post->created_at,
            'updated_at' => (string)$post->updated_at,
            'deleted_at' => isset($post->deleted_at) ? (string)$post->deleted_at : null,
        ];
    }
}
