<?php

namespace Rockapps\RkLaravel\Transformers;

use Auth;
use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\Payment;

class PaymentTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Payment $payment
     * @return array
     */
    public function transform(Payment $payment)
    {
        $return = [
            'id' => (int)$payment->id,
            'status' => (string)$payment->status,
            'credit_card_id' => (string)$payment->credit_card_id,
            'payer_id' => (int)$payment->payer_id,
            'payer_type' => (string)$payment->payer_type,
            'pgm_transaction_id' => (string)$payment->pgm_transaction_id,
            'iap_receipt_id' => (string)$payment->iap_receipt_id,
            'iap_product_id' => (string)$payment->iap_product_id,
            'apple_transaction_id' => (string)$payment->apple_transaction_id,
            'google_order_id' => (string)$payment->google_order_id,
            'boleto_barcode' => (string)$payment->boleto_barcode,
            'boleto_expiration_date' => (string)$payment->boleto_expiration_date,
            'boleto_instructions' => (string)$payment->boleto_instructions,
            'pix_expiration_date' => (string)$payment->pix_expiration_date,
            'pix_qr_code' => (string)$payment->pix_qr_code,
            'boleto_url' => (string)$payment->boleto_url,
            'purchaseable_id' => (int)$payment->purchaseable_id,
            'purchaseable_type' => (string)$payment->purchaseable_type,
            'gateway' => (string)$payment->gateway,
            'direction' => (string)$payment->direction,
            'value' => (float)$payment->value,
            'payer' => null,
            '_next_states' => $payment->getNextStates(),
            'credit_card' => $payment->creditCard ? $payment->creditCard->transform(CreditCardTransformer::class) : null,
            'product' => $payment->product ? $payment->product->transform(ProductSingleTransformer::class) : null,
            'company' => $payment->company ? $payment->company->transform(CompanySingleTransformer::class) : null,
            'company_id' => $payment->company_id,
            'product_id' => $payment->product_id,
            'paid_at' => (string)$payment->paid_at,
            'created_at' => (string)$payment->created_at,
            'updated_at' => (string)$payment->updated_at,
            'deleted_at' => isset($payment->deleted_at) ? (string)$payment->deleted_at : null,
        ];

        $user = Auth::getUser();
        if ($user && $user->hasRole('admin')) {
            $return['comments_operator'] = $payment->comments_operator;
        }

        if ($payment->payer) {
            $return['payer'] = [
                'name' => $payment->payer->name,
                'id' => $payment->payer->id,
            ];
        }

        return $return;
    }
}
