<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\CustomAttribute;

class CustomAttributesTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param CustomAttribute $custom_attribute
     * @return array
     */
    public function transform(CustomAttribute $custom_attribute)
    {
        $return = [
            'id' => (int)$custom_attribute->id,
            'object_type' => (string)$custom_attribute->object_type,
            'object_id' => (int)$custom_attribute->object_id,
            'field' => (string)$custom_attribute->field,
            'cast' => (string)$custom_attribute->cast,
            'value' => (string)$custom_attribute->value,
            'created_at' => (string)$custom_attribute->created_at,
            'updated_at' => (string)$custom_attribute->updated_at,
            'deleted_at' => (string)$custom_attribute->deleted_at,
        ];

        return $return;
    }
}
