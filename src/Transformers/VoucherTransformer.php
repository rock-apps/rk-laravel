<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\Voucher;

class VoucherTransformer extends TransformerBase
{
    /**
     * A Fractal transformer.
     *
     * @param Voucher $voucher
     * @return array
     */
    public function transform(Voucher $voucher)
    {
        $return = [
            'id' => (int)$voucher->id,
            'code' => (string)$voucher->code,
            'model_type' => (string)$voucher->model_type,
            'model_id' => (int)$voucher->model_id,
            'data' => (string)$voucher->data,
            'expires_at' => (string)$voucher->expires_at,
            'created_at' => (string)$voucher->created_at,
            'updated_at' => (string)$voucher->updated_at,
            'deleted_at' => isset($voucher->deleted_at) ? (string)$voucher->deleted_at : null,
        ];

        $user = \Auth::getUser();
        if ($user && $user->hasRole('admin')) {
            $return['available_qty'] = $voucher->availableQty();
            $return['max_redeem_qty'] = $voucher->max_redeem_qty;
            $return['company_id'] = $voucher->company_id;
            $return['company'] = $voucher->company ? $voucher->company->transform(CompanySingleTransformer::class) : null;
            $return['campaign'] = $voucher->campaign;
        }

        return $return;
    }
}
