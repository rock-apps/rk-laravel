<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\PushNotification;

class PushNotificationTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param PushNotification $pushNotification
     * @return array
     */
    public function transform(PushNotification $pushNotification)
    {
        $success = $pushNotification->pushMessagesTotalSuccess();
        $total = $pushNotification->pushMessagesTotalCount();
        $perc = $total ? ($success / $total) : 0;

        return [
            'id' => (int)$pushNotification->id,
            'status' => (string)$pushNotification->status,
            'name' => (string)$pushNotification->name,
            'type' => (string)$pushNotification->type,
            'destination' => (string)$pushNotification->destination,
            'scheduled_at' => (string)$pushNotification->scheduled_at,
            'title' => (string)$pushNotification->title,
            'body' => (string)$pushNotification->body,
            'object_id' => (int)$pushNotification->object_id,
            'object_type' => (string)$pushNotification->object_type,
            '_next_states' => $pushNotification->getNextStates(),
            '_total_count' => $pushNotification->pushMessagesTotalCount(),
            '_total_error' => $pushNotification->pushMessagesTotalError(),
            '_total_success' => $success,
            '_total_waiting' => $pushNotification->pushMessagesTotalWaiting(),
            '_total_perc' => $perc,

            'created_at' => (string)$pushNotification->created_at,
            'updated_at' => (string)$pushNotification->updated_at,
            'deleted_at' => isset($pushNotification->deleted_at) ? (string)$pushNotification->deleted_at : null,
        ];
    }
}
