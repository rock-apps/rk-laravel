<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\Plan;
use Rockapps\RkLaravel\Models\Variant;

class PlanTransformer extends TransformerBase
{
    /**
     * A Fractal transformer.
     *
     * @param Plan $plan
     * @return array
     */
    public function transform(Plan $plan)
    {
        $return = [
            'id' => (int)$plan->id,
            'active' => (bool)$plan->active,
            'gateway' => (string)$plan->gateway,
            'apple_product_id' => (string)$plan->apple_product_id,
            'google_product_id' => (string)$plan->google_product_id,
            'name' => (string)$plan->name,
            'code' => (string)$plan->code,
            'headline' => (string)$plan->headline,
            'days' => (int)$plan->days,
            'trial_days' => (int)$plan->trial_days,
            'payment_method' => (string)$plan->payment_method,
            'color' => (string)$plan->color,
            'value' => (float)$plan->value,
            'charges' => (int)$plan->charges,
            'installments' => (int)$plan->installments,
            'invoice_reminder' => (int)$plan->invoice_reminder,
            'pgm_plan_id' => (int)$plan->pgm_plan_id,
            'active_android' => (bool)$plan->active_android,
            'active_ios' => (bool)$plan->active_ios,
            'description' => (string)$plan->description,
            'product' => $plan->product ? $plan->product->transform(ProductSingleTransformer::class) : null,
            'product_id' => $plan->product_id,
            'variants' => $plan->variants->map(function (Variant $v) {
                return $v->transform(VariantSingleTransformer::class);
            }),

            'created_at' => (string)$plan->created_at,
            'updated_at' => (string)$plan->updated_at,
            'deleted_at' => isset($plan->deleted_at) ? (string)$plan->deleted_at : null,
        ];

        $user = \Auth::getUser();
        if($user && $user->hasRole('admin')) {
            $return['comments_operator'] = $plan->comments_operator;
        }

        return $return;
    }
}
