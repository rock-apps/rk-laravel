<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\PostBlock;

class PostBlockTransformer extends TransformerAbstract
{
    /**
     * @param PostBlock $block
     * @return array
     */
    public function transform(PostBlock $block)
    {
        return [
            'id' => (int)$block->id,
            'content' => (string)$block->content,
            'type' => (string)$block->type,
            'sequence' => (integer)$block->sequence,
            'post_id' => (integer)$block->post_id,
            'text_size' => (integer)$block->text_size,
            'bold' => (boolean)$block->bold,
            'italic' => (boolean)$block->italic,
            'color' => (string)$block->color,
            'style' => $block->style,
            'created_at' => (string)$block->created_at,
            'updated_at' => (string)$block->updated_at,
        ];
    }
}
