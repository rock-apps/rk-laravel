<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\ShippingMethod;

class ShippingMethodTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param ShippingMethod $shipping_method
     * @return array
     */
    public function transform(ShippingMethod $shipping_method)
    {
        $return = [
            'id' => (int)$shipping_method->id,
            'carrier' => (string)$shipping_method->carrier,
            'address_id' => (int)$shipping_method->address_id,
            'active' => (bool)$shipping_method->active,
            'image' => (string)$shipping_method->image,
            'title' => (string)$shipping_method->title,
            'description' => (string)$shipping_method->description,
            'fixed_value' => (float)$shipping_method->fixed_value,
            'company_id' => (integer)$shipping_method->company_id,
            'created_at' => (string)$shipping_method->created_at,
            'updated_at' => (string)$shipping_method->updated_at,
            'deleted_at' => isset($shipping_method->deleted_at) ? (string)$shipping_method->deleted_at : null,
            'address' => $shipping_method->address ? $shipping_method->address->transform(AddressTransformer::class) : null,
            'company' => $shipping_method->company ? $shipping_method->company->transform(CompanySingleTransformer::class) : null,
        ];
        $user = \Auth::getUser();

        if ($user && ($user->hasRole('admin') || $user->company_id === $shipping_method->company_id)) {
            $return['carrier_key'] = (string)$shipping_method->carrier_key;
            $return['carrier_secret'] = (string)$shipping_method->carrier_secret;
            $return['carrier_sandbox'] = (bool)$shipping_method->carrier_sandbox;
        }

        return $return;
    }
}
