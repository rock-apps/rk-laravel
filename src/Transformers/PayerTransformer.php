<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Interfaces\PayerInterface;

class PayerTransformer extends TransformerAbstract
{
    public function transform(PayerInterface $payer)
    {
        return [
            'id' => (int)$payer->id,
        ];
    }
}
