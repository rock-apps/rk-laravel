<?php

namespace Rockapps\RkLaravel\Transformers;

use Rockapps\RkLaravel\Models\Permission;
use Rockapps\RkLaravel\Models\Role;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Role $role
     * @return array
     */
    public function transform(Role $role)
    {
        return [
            'id' => (int)$role->id,
            'name' => (string)$role->name,
            'system' => (bool)$role->system,
            'display_name' => (string)$role->display_name,
            'description' => (string)$role->description,
            'permissions' => $role->perms->each(function(Permission $permission){
               return $permission->transform(PermissionTransformer::class);
            }),
            'created_at' => (string)$role->created_at,
            'updated_at' => (string)$role->updated_at,

        ];
    }
}
