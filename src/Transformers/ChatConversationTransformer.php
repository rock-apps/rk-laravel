<?php

namespace Rockapps\RkLaravel\Transformers;

use League\Fractal\TransformerAbstract;
use Rockapps\RkLaravel\Models\Conversation;
use Rockapps\RkLaravel\Models\User;

class ChatConversationTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Conversation $conversation
     * @return array
     */
    public function transform(Conversation $conversation)
    {
        $user = \Auth::getUser();
        $t = config('rk-laravel.chat.chat_message_transformer', ChatMessageTransformer::class);
        return [
            'id' => $conversation['id'],
            'data' => $conversation['data'],
            'unread' => $user ? $conversation->unReadNotificationsCount($user) : 0,
            'last_message' => $conversation->last_message ? $conversation->last_message->transform($t) : null,
            'users' => $conversation->users->map(function (User $u) {
                return $u->transform(UserSingleTransformer::class);
            }),
        ];
    }
}
