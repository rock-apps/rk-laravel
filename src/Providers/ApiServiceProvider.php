<?php

namespace Rockapps\RkLaravel\Providers;

use Rockapps\RkLaravel\Exceptions\ApiHandler;
use Dingo\Api\Provider\DingoServiceProvider;

class ApiServiceProvider extends DingoServiceProvider
{

    protected function registerExceptionHandler()
    {
        $this->app->singleton('api.exception', function ($app) {
            return new ApiHandler($app['Rockapps\RkLaravel\Exceptions\Handler'], $this->config('errorFormat'), $this->config('debug'));
//            return new ApiHandler($app['Illuminate\Contracts\Debug\ExceptionHandler'], $this->config('errorFormat'), $this->config('debug'));
        });
    }
}
