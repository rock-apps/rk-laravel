<?php

namespace Rockapps\RkLaravel\Providers;

use Illuminate\Support\ServiceProvider;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        Broadcast::routes();
        \Broadcast::routes(['middleware' => ['rk.auth']]);
        /** @noinspection PhpIncludeInspection */
        require base_path('routes/channels.php');

    }
}
