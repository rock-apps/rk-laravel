<?php

namespace Rockapps\RkLaravel;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Rockapps\RkLaravel\Skeleton\SkeletonClass
 * @codeCoverageIgnore
 */
class RkLaravelFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'rk-laravel';
    }
}
