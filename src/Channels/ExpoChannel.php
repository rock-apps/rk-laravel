<?php

namespace Rockapps\RkLaravel\Channels;

use Illuminate\Notifications\Notification;

class ExpoChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toExpo($notifiable);

        // Send notification to the $notifiable instance...
    }
}
