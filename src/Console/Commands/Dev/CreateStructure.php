<?php

namespace Rockapps\RkLaravel\Console\Commands\Dev;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Rockapps\RkLaravel\Models\User;

class CreateStructure extends Command
{
    /**
     * @var mixed
     */
    public $singular;
    /**
     * @var mixed
     */
    public $singular_case;
    /**
     * @var mixed
     */
    public $plural;
    /**
     * @var mixed
     */
    public $plural_case;
    /**
     * @var bool
     */
    public $create_migration;
    /**
     * @var bool
     */
    public $create_model;
    /**
     * @var bool
     */
    public $create_controller;
    /**
     * @var bool
     */
    public $create_model_factory;
    /**
     * @var bool
     */
    public $create_model_filter;
    /**
     * @var bool
     */
    public $create_model_transformer;

    /**
     * @var bool
     */
    public $create_tests_unit;
    /**
     * @var bool
     */
    public $create_tests_api;


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dev:structure';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Dev Struture';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        /** @var User $user */
        $this->singular = $this->ask('Digite o nome no singular. Ex.: post, user_post','user_post');
        $this->plural = $this->ask('Digite o nome no plural. Ex.: posts, user_posts','user_posts');
        $this->singular_case = $this->ask('Digite o nome no singular com case. Ex.: Post, UserPost','UserPost');
        $this->plural_case = $this->ask('Digite o nome no plural com case. Ex.: Post, UserPosts','UserPosts');

        $this->line('Defina os arquivos que serão gerados');
        $this->create_migration = $this->askWithCompletion('Migration', ['S', 'N'], 'S') === 'S';
        $this->create_model = $this->askWithCompletion('Model', ['S', 'N'], 'S') === 'S';
        $this->create_model_filter = $this->askWithCompletion('Model Filter', ['S', 'N'], 'S') === 'S';
        $this->create_model_transformer = $this->askWithCompletion('Model Transformer', ['S', 'N'], 'S') === 'S';
        $this->create_model_factory = $this->askWithCompletion('Model Factory', ['S', 'N'], 'S') === 'S';
        $this->create_controller = $this->askWithCompletion('Controller', ['S', 'N'], 'S') === 'S';
        $this->create_tests_unit = $this->askWithCompletion('Tests Unit', ['S', 'N'], 'S') === 'S';
        $this->create_tests_api = $this->askWithCompletion('Tests API', ['S', 'N'], 'S') === 'S';

        if ($this->create_migration) $this->createMigration();
        if ($this->create_model) $this->createModel();
        if ($this->create_model_filter) $this->createModelFilter();
        if ($this->create_model_transformer) $this->createModelTransformer();
        if ($this->create_model_factory) $this->createModelFactory();
        if ($this->create_controller) $this->createController();
        if ($this->create_tests_unit) $this->createTestsUnit();
        if ($this->create_tests_api) $this->createTestsApi();

        return 0;
    }

    public function createMigration()
    {
        $this->line('Criando migration ' . "create_{$this->singular}_table");
        $cmd = "make:migration";
        $this->line('===========================================================');
        $this->line('=                                                         =');
        $this->line('= Caso o comando falhe, execute o composer dumto-autoload =');
        $this->line('=                                                         =');
        $this->line('===========================================================');
        \Artisan::call("make:migration create_{$this->singular}_table --table={$this->singular}");
    }

    public function createModel()
    {
        $this->line("Criando model {$this->singular_case}");

        \File::makeDirectory(app_path("Models"),493,1,1);
        $file_path = app_path("Models/{$this->singular_case}.php");
        $content =
            '<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use Rockapps\RkLaravel\Models\ModelBase;
use App\ModelFilters\\' . $this->singular_case . 'Filter;
use App\Transformers\\' . $this->singular_case . 'Transformer;


class ' . $this->singular_case . ' extends ModelBase implements \OwenIt\Auditing\Contracts\Auditable
{
    use SoftDeletes, Auditable, Filterable;

    const CONSTA = \'CONSTA\';

    public $transformer = ' . $this->singular_case . 'Transformer::class;

    protected $table = \'' . $this->singular . '\';

    protected $casts = [
        \'object_id\' => \'int\',
        \'object_id\' => \'decimal:2\',
        \'object_id\' => \'boolean\',
    ];

    protected $dates = [];

    protected $fillable = [
        \'object_type\',
    ];

    protected $rules = [
        \'title\' => \'nullable|string\',
    ];

    protected $attributes = [
        \'active\' => true,
    ];

    public function object()
    {
        return $this->morphTo(\'object\');
    }

    public function modelFilter()
    {
        return $this->provideFilter(' . $this->singular_case . 'Filter::class);
    }
}
';

        return file_put_contents($file_path, $content);

    }

    public function createModelFilter()
    {
        $this->line("Criando model filter {$this->singular_case}");

        \File::makeDirectory(app_path("ModelFilters"),493,1,1);
        $file_path = app_path("ModelFilters/{$this->singular_case}Filter.php");
        $content =
            '<?php

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class ' . $this->singular_case . 'Filter extends ModelFilter
{
    protected $blacklist = [];

    public function setup()
    {
        return $this;
    }

}

';

        return file_put_contents($file_path, $content);

    }

    public function createModelTransformer()
    {
        $this->line("Criando model transformer {$this->singular_case}");

        \File::makeDirectory(app_path("Transformers"),493,1,1);
        $file_path = app_path("Transformers/{$this->singular_case}Transformer.php");
        $content =
            '<?php

namespace App\Transformers;

use App\Models\\' . $this->singular_case . ';
use Rockapps\RkLaravel\Transformers\TransformerBase;

class ' . $this->singular_case . 'Transformer extends TransformerBase
{
    /**
     * A Fractal transformer.
     *
     * @param ' . $this->singular_case . ' $' . $this->singular . '
     * @return array
     */
    public function transform(' . $this->singular_case . ' $' . $this->singular . ')
    {
        $return = [
            \'id\' => (int)$' . $this->singular . '->id,
            \'prop\' => (string)$' . $this->singular . '->prop,
            \'prop\' => (float)$' . $this->singular . '->prop,
            \'prop\' => (boolean)$' . $this->singular . '->prop,
            \'created_at\' => (string)$' . $this->singular . '->created_at,
            \'updated_at\' => (string)$' . $this->singular . '->updated_at,
        ];

        return $return;
    }
}

';

        return file_put_contents($file_path, $content);

    }

    public function createModelFactory()
    {
        $this->line("Criando model factory {$this->singular_case}");

        $file_path = database_path("factories/{$this->singular_case}Factory.php");
        $content =
            '<?php

/** @var Factory $factory */

use App\Models\\' . $this->singular_case . ';
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(' . $this->singular_case . '::class, function (Faker $faker) {
    return [
        \'consta\' => ' . $this->singular_case . '::CONSTA,
        \'rand\' => $faker->randomElement([0,1]),
        \'bool\' => true,
        \'date\' => now()->hour(8)->minutes(0)->second(0),
    ];
});

$factory->afterCreatingState(' . $this->singular_case . '::class, \'state-a\', function (' . $this->singular_case . ' $' . $this->singular . ') {

});

$factory->state(' . $this->singular_case . '::class, ' . $this->singular_case . '::CONSTA, function () {
    return [\'mode\' => \'ABCDEF\'];
});


';

        return file_put_contents($file_path, $content);

    }

    public function createController()
    {
        $this->line("Criando controller {$this->singular_case}");

        \File::makeDirectory(app_path("Api/{$this->singular_case}"),493,1,1);
        $file_path = app_path("Api/{$this->singular_case}/{$this->singular_case}Controller.php");

        $content = '<?php

namespace App\Api\\' . $this->singular_case . ';

use App\Models\\'.$this->singular_case.';
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Rockapps\RkLaravel\Api\ControllerBase;

/**
 * @group ' . $this->singular_case . '
 *
 * ' . $this->singular_case . ' API Requests
 */
class ' . $this->singular_case . 'Controller extends ControllerBase
{
    /**
     * Get All ' . $this->singular_case . '
     *
     * @param ' . $this->singular_case . 'IndexRequest $request
     * @return JsonResponse
     * @throws ValidationException
     * @transformerCollection \App\Transformers\\' . $this->singular_case . 'Transformer
     */
    public function index(' . $this->singular_case . 'IndexRequest $request)
    {
        $' . $this->plural . ' = ' . $this->singular_case . '::filter($request->all())->get();
        return $this->showAll($' . $this->plural . ');
    }

    /**
     * Create new '.$this->singular_case.'
     *
     * Create a new '.$this->singular_case.' description
     *
     * @param '.$this->singular_case.'SaveRequest $request
     * @return JsonResponse
     * @throws Exception
     * @transformer \App\Transformers\\'.$this->singular_case.'Transformer
     */
    public function store('.$this->singular_case.'SaveRequest $request)
    {
        DB::beginTransaction();
        $user = \Auth::getUser();

        $'.$this->singular.' = new '.$this->singular_case.'($request->except(\'medias_upload\'));
        $'.$this->singular.'->save();
        $this->addMedias($'.$this->singular.', $request->get(\'medias_upload\'));
        DB::commit();

        return $this->showOne($'.$this->singular.', 201);
    }

    /**
     * Get specific '.$this->singular_case.'
     *
     * @transformer \App\Transformers\\'.$this->singular_case.'Transformer
     * @queryParam '.$this->singular_case.' required The id of the '.$this->singular_case.'.
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        /** @var'.$this->singular_case.'$'.$this->singular.' */
        $'.$this->singular.' = '.$this->singular_case.'::filter()->findOrFail($id);
        return $this->showOne($'.$this->singular.', 200);
    }

    /**
     * Update '.$this->singular_case.'
     *
     * @transformer \App\Transformers\\'.$this->singular_case.'Transformer
     * @queryParam '.$this->singular_case.' required The id of the '.$this->singular_case.'.
     * @param '.$this->singular_case.'SaveRequest $request
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update('.$this->singular_case.'SaveRequest $request, $id)
    {
        DB::beginTransaction();
        /** @var '.$this->singular_case.' $'.$this->singular.' */
        $'.$this->singular.' = '.$this->singular_case.'::filter()->findOrFail($id);
        $'.$this->singular.'->fill($request->except(\'medias_upload\'));
        $'.$this->singular.'->save();
        $this->addMedias($'.$this->singular.', $request->get(\'medias_upload\'));
        DB::commit();

        return $this->showOne($'.$this->singular.', 200);
    }

    /**
     * Delete '.$this->singular_case.'
     *
     * @param int $id
     * @queryParam '.$this->singular_case.' required The id of the '.$this->singular_case.'.
     * @return \Illuminate\Http\JsonResponse
     * @urlParam '.$this->singular_case.' required '.$this->singular_case.' ID
     * @throws Exception
     */
    public function destroy($id)
    {
        /** @var'.$this->singular_case.'$'.$this->singular.' */
        $'.$this->singular.' = '.$this->singular_case.'::filter()->findOrFail($id);
        $'.$this->singular.'->delete();

        return $this->successDelete();
    }

}
';

        $this->createControllerIndexRequest();
        $this->createControllerSaveRequest();

        return file_put_contents($file_path, $content);

    }

    public function createControllerIndexRequest()
    {
        $this->line("Criando controller index request {$this->singular_case}");

        \File::makeDirectory(app_path("Api/{$this->singular_case}"),493,1,1);
        $file_path = app_path("Api/{$this->singular_case}/{$this->singular_case}IndexRequest.php");

        $content = '<?php

namespace App\Api\\'.$this->singular_case.';

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam name string required
 */
class '.$this->singular_case.'IndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            \'name\' => \'nullable|string\',
        ];
    }
}
';

        return file_put_contents($file_path, $content);

    }

    public function createControllerSaveRequest()
    {
        $this->line("Criando controller save request {$this->singular_case}");

        \File::makeDirectory(app_path("Api/{$this->singular_case}"),493,1,1);
        $file_path = app_path("Api/{$this->singular_case}/{$this->singular_case}SaveRequest.php");

        $content = '<?php

namespace App\Api\\'.$this->singular_case.';
<?php

namespace App\Api\\'.$this->singular_case.';

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam name string required
 */
class '.$this->singular_case.'SaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            \'name\' => \'required|string\',
        ];
    }
}

';

        return file_put_contents($file_path, $content);

    }

    public function createTestsUnit()
    {
        $this->line("Criando tests Unit {$this->singular_case}");

        \File::makeDirectory(base_path("tests/Unit"),493,1,1);
        $file_path = base_path("tests/Unit/{$this->singular_case}UnitTest.php");

        $content = '<?php

namespace Tests\Unit;

use App\Models\\'.$this->singular_case.';
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class '.$this->singular_case.'UnitTest extends TestCase
{
    use RefreshDatabase;

    public function test'.$this->singular_case.'()
    {
        $sample = '.$this->singular_case.'::factory([
            \'registration_id\' => $registration->id,
       ]);

        $this->assertInstanceOf( '.$this->singular_case.'::class, $'.$this->singular.');
        $this->assertEquals(Related::class, $'.$this->singular.'->related);
    }

    protected function setUp(): void
    {
        parent::setUp();
        (new \RoleTableSeeder())->run();
        (new \ParameterSeeder())->run();
    }
}

';

        return file_put_contents($file_path, $content);

    }

    public function createTestsApi()
    {
        $this->line("Criando tests API {$this->singular_case}");

        \File::makeDirectory(base_path("tests/Api"),493,1,1);
        $file_path = base_path("tests/Api/{$this->singular_case}ApiTest.php");

        $url = Str::replaceFirst('_','-',$this->plural);

        $content = '<?php

namespace Tests\Api;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\HelpersTests\ApiCrudTestTrait;
use Rockapps\RkLaravel\HelpersTests\MigrationTestTrait;
use Rockapps\RkLaravel\Models\\'.$this->singular_case.';
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Models\Role;
use Tests\ApiTestCase;

class '.$this->singular_case.'ControllerTest extends ApiTestCase
{
    use RefreshDatabase;
    use ApiCrudTestTrait;
    use MigrationTestTrait;

    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $admin;
    /* @var \Illuminate\Database\Eloquent\Model|User */
    public $user;
    /* @var \Illuminate\Database\Eloquent\Model|Category */
    public $category;

    public function setUp(): void
    {
        parent::setUp();
        $this->setupMigration("/../..");

        (new \RkRoleTableSeeder())->run();

        $this->admin = factory(User::class)->create();
        $this->admin->attachRole(Role::findOrFailByName(Constants::ROLE_ADMIN));

        $this->user = factory(User::class)->create();
        $this->user->attachRole(Role::findOrFailByName(Constants::ROLE_USER));

        $this->category = factory(Category::class)->create();
    }

    public function testAdminCreate'.$this->singular_case.'()
    {
        $data = [
            \'title\' => \''.$this->singular_case.' teste\',
            \'object_type\' => get_class($this->category),
            \'object_id\' => $this->category->id,
            \'active\' => false,
            \'image_upload\' => Constants::LOGO_RK_B64
        ];

        $'.$this->singular.' = $this->callStoreByUri($this->admin, 201, "/v1/'.$url.'", $data, false)
            ->decodeResponseJson()[\'data\'];

        $this->assertDatabaseHas(\'media\', [
            \'model_type\' => '.$this->singular_case.'::class,
            \'model_id\' => '.$this->singular.'[\'id\']
        ]);

        $this->assertDatabaseHas(\''.$this->singular.'s\', [
            \'id\' => '.$this->singular.'[\'id\'],
            \'object_type\' => get_class($this->category),
            \'object_id\' => $this->category->id,
            \'active\' => false,
        ]);

    }

    public function testAdminUpdate'.$this->singular_case.'()
    {
        /** @var '.$this->singular_case.' '.$this->singular.' */
        $'.$this->singular.' = '.$this->singular_case.'::factory();

        $this->callUpdateByUri($this->admin, 200, \'/v1/'.$url.'/\' . $'.$this->singular.'->id, [
            \'title\' => \'Novo nome\',
            \'active\' => true,
            \'image_upload\' => Constants::LOGO_RK_B64
        ], true)->assertJsonFragment([
            \'id\' => $'.$this->singular.'->id,
            \'title\' => \'Novo nome\',
            \'active\' => true
        ]);

        $this->assertDatabaseHas(\''.$url.'\', [
            \'id\' => $'.$this->singular.'->id,
            \'title\' => \'Novo nome\',
            \'active\' => true
        ]);
    }

    public function testAdminDestroy'.$this->singular_case.'()
    {
        /** @var '.$this->singular_case.' '.$this->singular.' */
        $'.$this->singular.' = '.$this->singular_case.'::factory();

        $this->callDestroyByUri($this->admin, 200, \'/v1/'.$url.'/\' . $'.$this->singular.'->id)
            ->assertJsonFragment([\'message\' => \'ok\']);

        $'.$this->singular.'->refresh();
        $this->assertNotNull($'.$this->singular.'->deleted_at);
    }

    public function testAdminIndex'.$this->singular_case.'s()
    {
        '.$this->singular_case.'::factory([\'active\' => true, \'title\' => \'Teste\'], 2);
        '.$this->singular_case.'::factory([\'active\' => false, \'object_type\' => \'MODEL\', \'object_id\' => 1234], 2);

        $this->callIndexByUri($this->admin, 200, \'/v1/'.$url.'\', null, 4);
        $this->callIndexByUri($this->admin, 200, \'/v1/'.$url.'\', [\'active\' => true], 2);
        $this->callIndexByUri($this->admin, 200, \'/v1/'.$url.'\', [\'title\' => \'Teste\'], 2);
        $this->callIndexByUri($this->admin, 200, \'/v1/'.$url.'\', [\'object_type\' => \'MODEL\'], 2);
        $this->callIndexByUri($this->admin, 200, \'/v1/'.$url.'\', [\'object_id\' => 1234], 2);
        $this->callIndexByUri($this->admin, 200, \'/v1/'.$url.'\', [\'title\' => \'Teste\', \'active\' => false], 0);

        $this->callIndexByUri($this->admin, 200, \'v1/'.$url.'\', [\'page\' => 1, \'per_page\' => 2], 2)
            ->assertJsonStructure([\'meta\' => [\'pagination\' => [\'total\', \'count\', \'per_page\', \'current_page\', \'total_pages\']]]);
    }

    public function testAdminShow'.$this->singular_case.'s()
    {
        $'.$this->singular.' = '.$this->singular_case.'::factory();

        $this->callIndexByUri($this->admin, 200, \'/v1/'.$url.'/\' . $'.$this->singular.'->id)            ->assertJsonStructure([\'data\' => [
                \'id\',// => (int)'.$this->singular.'->id,
                \'title\',// => (string)'.$this->singular.'->title,
            ]]);
    }

}


';
        return file_put_contents($file_path, $content);

    }


}
