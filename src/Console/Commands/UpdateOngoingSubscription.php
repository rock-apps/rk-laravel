<?php

namespace Rockapps\RkLaravel\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Rockapps\RkLaravel\Helpers\Sentry;
use Rockapps\RkLaravel\Models\Subscription;

class UpdateOngoingSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscription:update-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update subscription status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $this->syncExternalGateway();
        $this->syncManualBalanceGateway();

        return 0;
    }

    private function syncExternalGateway()
    {
        /** @var Subscription[] $subscriptions */
        $subscriptions = Subscription::query()
            ->whereIn('status', [
                Subscription::STATUS_PAID,
                Subscription::STATUS_PENDING_PAYMENT,
                Subscription::STATUS_TRIALING,
            ])
            ->whereIn('gateway', [
                Subscription::GATEWAY_PAGARME,
                Subscription::GATEWAY_APPLE,
                Subscription::GATEWAY_GOOGLE,
            ])
            ->get();

        DB::beginTransaction();

        foreach ($subscriptions as $subscription) {
            try {
                $subscription->syncWithGateway();
                $subscription->save();
                $subscription->checkRenewed();

            } catch (\Exception $e) {
                Sentry::capture('syncExternalGateway: ' . $e->getMessage(), [
                    'subscription' => $subscription->getAttributes(),
                    'errors' => $subscription->getErrors()->getMessages()
                ]);
            }

        }
        DB::commit();
    }

    private function syncManualBalanceGateway()
    {
        /** @var Subscription[] $subscriptions */
        $subscriptions = Subscription::query()
            ->whereIn('status', [
                Subscription::STATUS_PAID,
                Subscription::STATUS_ACTIVE_MANUAL,
            ])
            ->whereIn('gateway', [
                Subscription::GATEWAY_BALANCE,
                Subscription::GATEWAY_MANUAL,
            ])
            ->where('mode', '<>', Subscription::MODE_LIFETIME)
            ->whereDate('current_period_end', '<', now()->format('Y-m-d'))
            ->get();


        DB::beginTransaction();
        foreach ($subscriptions as $subscription) {
            try {
                // Double check se a assinatura está expirada
                if ($subscription->isExpired()) {
                    $subscription->apply(Subscription::STATUS_ENDED);
                    $subscription->save();
                }

            } catch (\Exception $e) {
                Sentry::capture('syncManualBalanceGateway: ' . $e->getMessage(), [
                    'subscription' => $subscription->getAttributes(),
                    'errors' => $subscription->getErrors()->getMessages()
                ]);
            }

        }
        DB::commit();
    }

}
