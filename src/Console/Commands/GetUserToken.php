<?php

namespace Rockapps\RkLaravel\Console\Commands;

use Illuminate\Console\Command;
use Rockapps\RkLaravel\Models\User;

class GetUserToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:get-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get User Token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        /** @var User $user */
        $user_var = $this->ask('Digite o nome do usuário, email ou id. Ou ENTER para listar todos.');
        if ((int)$user_var > 0) {
            $users = User::where('id', $user_var)->get();
        } else {
            $users = User::where('id', $user_var)
                ->orWhere('email', 'like', "%$user_var%")
                ->orWhere('name', 'like', "%$user_var%")
                ->get();
        }

        $this->info('Usuários Encontrados: ' . count($users) . "\n");
        foreach ($users as $user) {

            $this->line('User ID: ' . $user->id);
            $this->line('User Name: ' . $user->name);
            $this->line('User Email: ' . $user->email);
            $this->line('Created_at: ' . $user->created_at);
            $token = auth()->login($user);
            $this->info('User Token: Bearer ' . $token);
        }

        return 0;
    }
}
