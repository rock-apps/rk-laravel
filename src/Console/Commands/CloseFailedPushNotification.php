<?php

namespace Rockapps\RkLaravel\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Rockapps\RkLaravel\Models\PushMessage;
use Rockapps\RkLaravel\Models\PushNotification;

class CloseFailedPushNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push-notification:close-failed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close stuck push notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        DB::beginTransaction();
        $pushMessages = PushMessage::query()
            ->where('status', PushMessage::STATUS_WAITING)
            ->whereNull('sent_at')
            ->get();

        foreach ($pushMessages as $message) {
            /** @var PushMessage $message */
            if ($message->created_at->diffInHours(now()) >= 24) {
                $message->status = PushMessage::STATUS_ERROR;
                $message->save();
            }
        }

        PushNotification::query()
            ->where('status', PushNotification::STATUS_SENDING)
            ->whereNull('deleted_at')
            ->whereDoesntHave('pushMessages', function (Builder $query) {
                return $query->where('status', PushMessage::STATUS_WAITING);
            })
            ->get()
            ->each(function (PushNotification $pushNotification) {
                $pushNotification->apply(PushNotification::STATUS_FINISHED);
                $pushNotification->save();
            });

        DB::commit();

        return 0;
    }
}
