<?php

namespace Rockapps\RkLaravel\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Rockapps\RkLaravel\Models\PushNotification;

class SendScheduledPushNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push-notification:send-scheduled';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send scheduled Push Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        DB::beginTransaction();
        $pns = PushNotification::isPast()
            ->where('status', PushNotification::STATUS_SCHEDULED)
            ->whereNull('deleted_at')
            ->get();

        foreach ($pns as $push_notification) {
            $push_notification->apply(PushNotification::STATUS_SENDING);
            $push_notification->save();
        }
        DB::commit();

        return 0;
    }
}
