<?php

namespace Rockapps\RkLaravel\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Rockapps\RkLaravel\Models\Payment;

class UpdateProcessingPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:update-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update payments with status processing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        //
        /** @var Payment[] $payments */
        $payments = Payment::query()
            ->where('status', Payment::STATUS_PROCESSING)
            ->whereIn('gateway', [Payment::GATEWAY_PAGARME_CC, Payment::GATEWAY_PAGARME_BOLETO])
            ->get();

        DB::beginTransaction();
        foreach ($payments as $payment) {
            $payment->syncWithGateway();
            $payment->save();
        }
        DB::commit();

        return 0;
    }
}
