<?php

namespace Rockapps\RkLaravel\Console\Commands;

use Illuminate\Console\Command;
use Rockapps\RkLaravel\Models\User;

class ResetPassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:reset-password';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset User Password';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        /** @var User $user */
        $user_var = $this->ask('Digite o nome do usuario, email ou id. Ou ENTER para listar todos');
        if ((int)$user_var > 0) {
            $users = User::where('id', $user_var)->get();
        } else {
            $users = User::where('id', $user_var)
                ->orWhere('email', 'like', "%$user_var%")
                ->orWhere('name', 'like', "%$user_var%")
                ->get();
        }

        if($users->count() > 1) {
            $this->error('Encontramos: ' . count($users) . " usuarios. Refine sua busca para trocar a senha.\n");
        }

        foreach ($users as $user) {

            $this->line('User ID: ' . $user->id);
            $this->line('User Name: ' . $user->name);
            $this->line('User Email: ' . $user->email);
            $this->line('Created_at: ' . $user->created_at);

            if($users->count() === 1) {
                $user_pw = $this->ask('Defina a nova senha do usuario');
                $user->password = $user_pw;
                $user->save();
                $token = auth()->login($user);
                $this->info('Senha atualizada!');
                $this->info('User Token: Bearer ' . $token);
            }
        }
        if($users->count() > 1) {
            $this->error('Encontramos: ' . count($users) . " usuarios. Refine sua busca para trocar a senha.\n");
        }

        return 0;
    }
}
