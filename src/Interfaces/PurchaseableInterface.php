<?php

namespace Rockapps\RkLaravel\Interfaces;

interface PurchaseableInterface
{
    const STATUS_DRAFT = 'DRAFT';
    const STATUS_CONFIRMED_PURCHASE = 'CONFIRMED_PURCHASE';
    const STATUS_PENDING_PURCHASE = 'PENDING_PURCHASE';
    const STATUS_BLOCKED_PURCHASE = 'BLOCKED_PURCHASE';

//    protected function getGraph();

    public function isConfirmed();

    public function DRAFT();

    public function CONFIRMED_PURCHASE();

    public function PENDING_PURCHASE();

    public function BLOCKED_PURCHASE();
}

