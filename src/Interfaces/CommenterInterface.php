<?php

namespace Rockapps\RkLaravel\Interfaces;

interface CommenterInterface
{
    public function comment(CommentableInterface $commentable, $comment);
}

