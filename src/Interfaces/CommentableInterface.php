<?php

namespace Rockapps\RkLaravel\Interfaces;

interface CommentableInterface
{
    public function isCommentedBy(CommenterInterface $commenter);
}

