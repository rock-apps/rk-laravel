<?php

namespace Rockapps\RkLaravel\Interfaces;

interface FavoritableInterface
{
    public function isFavoritedBy(FavoriterInterface $favoriter);
}

