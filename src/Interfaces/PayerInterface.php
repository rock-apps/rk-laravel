<?php

namespace Rockapps\RkLaravel\Interfaces;

use Carbon\Carbon;

/**
 * Interface PayerInterface
 * @package Rockapps\RkLaravel\Interfaces
 * @property bool $can_pay_with_balance
 * @property bool $can_pay_with_boleto
 * @property bool $can_pay_with_cc
 * @property bool $can_pay_with_bank
 * @property bool $can_pay_with_cash
 * @property bool $can_pay_with_pix
 * @property string|null $pgm_customer_id
 * @property string $document
 * @property Carbon $birth_date
 * @property string $mobile
 * @property string $telephone
 */
interface PayerInterface
{
    public function getBalance();
}

