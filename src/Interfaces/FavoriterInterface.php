<?php

namespace Rockapps\RkLaravel\Interfaces;

interface FavoriterInterface
{
    public function hasFavorited(FavoritableInterface $favoritable);

    public function favorite(FavoritableInterface $favoritable, bool $value = true);
}

