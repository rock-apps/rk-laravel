<?php

namespace Rockapps\RkLaravel;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Rockapps\RkLaravel\Helpers\Meta\MetaBag;
use Rockapps\RkLaravel\Models\LogRequest;

class RkLaravelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'rk-laravel');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'rk-laravel');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');
//         $this->loadFaRoutesFrom(__DIR__.'/routes.php');
        $this->registerEloquentFactoriesFrom(__DIR__ . '/../database/factories');
        $this->registerEloquentFactoriesFrom(__DIR__ . '/../tests/Factories');


        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/rk-laravel.php' => config_path('rk-laravel.php'),
            ], 'config');

            $this->publishMigration();

            // Publishing the views.
            /*$this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/vendor/rk-laravel'),
            ], 'views');*/

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/rk-laravel'),
            ], 'assets');*/

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/rk-laravel'),
            ], 'lang');*/

            // Registering package commands.
            // $this->commands([]);
        }
    }

    /**
     * Register factories.
     *
     * @param string $path
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function registerEloquentFactoriesFrom($path)
    {
        $this->app->make(Factory::class)->load($path);
    }

    private function publishMigration()
    {
        $files = [
            'create_users_table',
            'alter_table_users_add_geo',
            'alter_table_users_add_pagarme',
            'entrust_setup_tables',
            'create_state_history_table',
            'create_user_devices_table',
            'create_audits_table',
            'create_parameters_table',
            'create_credit_cards_table',
            'create_plans_table',
            'create_categories_table',
            'create_addresses_table',
            'create_subscriptions_table',
            'create_payments_table',
            'create_bank_accounts_table',
            'create_bank_transfers_table',
            'create_chat_tables',
            'create_favorites_table',
            'create_comments_table',
            'create_configurations_table',
            'create_ratings_table',
            'add_reputation_field_on_user_table',
            'create_gamify_tables',
            'create_tag_tables',
            'create_media_table',
            'create_notifications_table',
            'create_password_resets_table',
            'create_push_notifications_table',
            'create_push_messages_table',
            'create_failed_jobs_table',
            'create_telescope_entries_table',
            'create_bank_recipients_table',
            'create_vouchers_table',
            'create_user_social_accounts_table',
            'create_posts_table',
            'create_companies_table',
            'create_products_table',
            'create_orders_table',
            'alter_table_users_add_handcap_education',
            'alter_table_categories_add_parent_category',
            'create_distance_matrices_table',
            'add_is_support_to_users_table',
            'add_pix_code_on_bank_accounts_table',
            'add_protected_on_acl_tables',
            'add_signup_token_to_users_table',
            'add_can_pay_with_pix_on_users_table',
            'add_pix_on_payments_table',
            'create_banners_table',
            'add_invited_by_to_users_table',
            'create_orders_virtual_unit_table',
            'add_produto_to_payment',
            'add_virtual_unit_to_bank_transfer',
            'create_tables_variants',
            'add_mobile_fields_to_plans',
            'add_username_is_active_to_users',
            'add_renewed_at_to_subscription',
            'add_product_to_plan_subscription',
            'add_virtual_unit_balance_to_users',
            'add_last_access_at_to_users',
            'add_android_transaction_to_subscription',
            'add_vouchers_redeemed_model',
            'create_payment_methods_table',
            'create_shipping_methods_table',
            'create_custom_attributes_table',
            'add_company_to_payment',
            'create_campaign_table',
            'add_company_to_acl',
            'create_employees_table',
            'create_log_requests_table',
        ];

        $publish = [];
        $date = '2000_01_01_0000';
        foreach ($files as $k => $file) {
            $publish[__DIR__ . "/../database/migrations/$file.php.stub"] = database_path('migrations/' . $date . ($k < 10 ? "0$k" : $k) . '_' . $file . '.php');
        }

        $this->publishes($publish, 'rk-migration');
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../config/filesystems.php', 'filesystems');
        $this->mergeConfigFrom(__DIR__ . '/../config/rk-laravel.php', 'rk-laravel');
        $this->mergeConfigFrom(__DIR__ . '/../config/state-machine.php', 'config');
        $this->mergeConfigFrom(__DIR__ . '/../config/gamify.php', 'gamify');
        $this->mergeConfigFrom(__DIR__ . '/../config/entrust.php', 'entrust');
        $this->mergeConfigFrom(__DIR__ . '/../config/medialibrary.php', 'medialibrary');

        // Register the main class to use with the facade
        $this->app->singleton('rk-laravel', function () {
            return new RkLaravel;
        });

        $this->app->singleton('meta-bag', function () {
            return new MetaBag();
        });
        $this->app->singleton('log-request', function () {
            return new LogRequest();
        });
    }
}
