<?php

namespace Rockapps\RkLaravel\Metrics;

use Rockapps\RkLaravel\Exceptions\ResourceException;

class MetricsBase
{

    const RANGE_7 = '7';
    const RANGE_15 = '15';
    const RANGE_30 = '30';
    const RANGE_60 = '60';
    const RANGE_180 = '180';
    const RANGE_P1M = 'P1M'; // Past 1 Month
    const RANGE_P3M = 'P3M'; // Past 3 Months
    const RANGE_P6M = 'P6M'; // Past 6 Months
    const RANGE_P12M = 'P12M'; // Past 12 Months
    const RANGE_365 = '365';
    const RANGE_TODAY = 'TODAY';
    const RANGE_MTD = 'MTD';
    const RANGE_YTD = 'YTD';

    public function setRange($range)
    {
        switch ($range) {

            case self::RANGE_7:

                return [
                    'key' => self::RANGE_7,
                    'actual_date_start' => now()->subDays(7)->format('Y-m-d') . ' 00:00:00',
                    'actual_date_end' => now()->format('Y-m-d') . ' 23:59:59',
                    'compare_date_start' => now()->subDays(15)->format('Y-m-d') . ' 00:00:00',
                    'compare_date_end' => now()->subDays(8)->format('Y-m-d') . ' 23:59:59',
                ];
                break;
            case self::RANGE_15:
                return [
                    'key' => self::RANGE_15,
                    'actual_date_start' => now()->subDays(15)->format('Y-m-d') . ' 00:00:00',
                    'actual_date_end' => now()->format('Y-m-d') . ' 23:59:59',
                    'compare_date_start' => now()->subDays(31)->format('Y-m-d') . ' 00:00:00',
                    'compare_date_end' => now()->subDays(16)->format('Y-m-d') . ' 23:59:59',
                ];
                break;
            case self::RANGE_30:
                return [
                    'key' => self::RANGE_30,
                    'actual_date_start' => now()->subDays(30)->format('Y-m-d') . ' 00:00:00',
                    'actual_date_end' => now()->format('Y-m-d') . ' 23:59:59',
                    'compare_date_start' => now()->subDays(61)->format('Y-m-d') . ' 00:00:00',
                    'compare_date_end' => now()->subDays(31)->format('Y-m-d') . ' 23:59:59',
                ];
                break;
            case self::RANGE_60:
                return [
                    'key' => self::RANGE_60,
                    'actual_date_start' => now()->subDays(60)->format('Y-m-d') . ' 00:00:00',
                    'actual_date_end' => now()->format('Y-m-d') . ' 23:59:59',
                    'compare_date_start' => now()->subDays(121)->format('Y-m-d') . ' 00:00:00',
                    'compare_date_end' => now()->subDays(61)->format('Y-m-d') . ' 23:59:59',
                ];
                break;
            case self::RANGE_180:
                return [
                    'key' => self::RANGE_180,
                    'actual_date_start' => now()->subDays(180)->format('Y-m-d') . ' 00:00:00',
                    'actual_date_end' => now()->format('Y-m-d') . ' 23:59:59',
                    'compare_date_start' => now()->subDays(180*2+1)->format('Y-m-d') . ' 00:00:00',
                    'compare_date_end' => now()->subDays(180+1)->format('Y-m-d') . ' 23:59:59',
                ];
                break;
            case self::RANGE_365:
                return [
                    'key' => self::RANGE_365,
                    'actual_date_start' => now()->subDays(365)->format('Y-m-d') . ' 00:00:00',
                    'actual_date_end' => now()->format('Y-m-d') . ' 23:59:59',
                    'compare_date_start' => now()->subDays(365*2+1)->format('Y-m-d') . ' 00:00:00',
                    'compare_date_end' => now()->subDays(365+1)->format('Y-m-d') . ' 23:59:59',
                ];
                break;
            case self::RANGE_P1M:
                return [
                    'key' => self::RANGE_P1M,
                    'actual_date_start' => now()->firstOfMonth()->format('Y-m-d') . ' 00:00:00',
                    'actual_date_end' => now()->endOfMonth()->format('Y-m-d') . ' 23:59:59',
                    'compare_date_start' => now()->subMonths(1)->firstOfMonth()->format('Y-m-d') . ' 00:00:00',
                    'compare_date_end' => now()->subMonths(1)->endOfMonth()->format('Y-m-d') . ' 23:59:59',
                ];
                break;
            case self::RANGE_P3M:
                return [
                    'key' => self::RANGE_P3M,
                    'actual_date_start' => now()->subMonths(3)->firstOfMonth()->format('Y-m-d') . ' 00:00:00',
                    'actual_date_end' => now()->endOfMonth()->format('Y-m-d') . ' 23:59:59',
                    'compare_date_start' => now()->subMonths(7)->firstOfMonth()->format('Y-m-d') . ' 00:00:00',
                    'compare_date_end' => now()->subMonths(4)->endOfMonth()->format('Y-m-d') . ' 23:59:59',
                ];
                break;
            case self::RANGE_P6M:
                return [
                    'key' => self::RANGE_P6M,
                    'actual_date_start' => now()->subMonths(6)->firstOfMonth()->format('Y-m-d') . ' 00:00:00',
                    'actual_date_end' => now()->endOfMonth()->format('Y-m-d') . ' 23:59:59',
                    'compare_date_start' => now()->subMonths(13)->firstOfMonth()->format('Y-m-d') . ' 00:00:00',
                    'compare_date_end' => now()->subMonths(7)->endOfMonth()->format('Y-m-d') . ' 23:59:59',
                ];
                break;
            case self::RANGE_P12M:
                return [
                    'key' => self::RANGE_P12M,
                    'actual_date_start' => now()->subMonths(12)->firstOfMonth()->format('Y-m-d') . ' 00:00:00',
                    'actual_date_end' => now()->endOfMonth()->format('Y-m-d') . ' 23:59:59',
                    'compare_date_start' => now()->subMonths(25)->firstOfMonth()->format('Y-m-d') . ' 00:00:00',
                    'compare_date_end' => now()->subMonths(13)->endOfMonth()->format('Y-m-d') . ' 23:59:59',
                ];
                break;
            case self::RANGE_TODAY:
                return [
                    'key' => self::RANGE_TODAY,
                    'actual_date_start' => now()->format('Y-m-d') . ' 00:00:00',
                    'actual_date_end' => now()->format('Y-m-d') . ' 23:59:59',
                    'compare_date_start' => now()->subDays(1)->format('Y-m-d') . ' 00:00:00',
                    'compare_date_end' => now()->subDays(1)->format('Y-m-d') . ' 23:59:59',
                ];
                break;
            case self::RANGE_MTD:
                return [
                    'key' => self::RANGE_MTD,
                    'actual_date_start' => now()->firstOfMonth()->format('Y-m-d') . ' 00:00:00',
                    'actual_date_end' => now()->format('Y-m-d') . ' 23:59:59',
                    'compare_date_start' => now()->subMonths(1)->firstOfMonth()->format('Y-m-d') . ' 00:00:00',
                    'compare_date_end' => now()->subMonths(1)->format('Y-m-d') . ' 23:59:59',
                ];
                break;
            case self::RANGE_YTD:
                return [
                    'key' => self::RANGE_YTD,
                    'actual_date_start' => now()->firstOfYear()->format('Y-m-d') . ' 00:00:00',
                    'actual_date_end' => now()->format('Y-m-d') . ' 23:59:59',
                    'compare_date_start' => now()->subYears(1)->firstOfYear()->format('Y-m-d') . ' 00:00:00',
                    'compare_date_end' => now()->subYears(1)->format('Y-m-d') . ' 23:59:59',
                ];
                break;
            default:
                break;
        }
        throw new ResourceException('Range Inválido '.$range);
    }
}
