<?php

namespace Rockapps\RkLaravel\Metrics;

use Illuminate\Database\Eloquent\Builder;

class MetricsValue extends MetricsBase
{
    private $range;
    private $range_column;
    /**
     * @var Builder
     */
    private $query;
    /**
     * @var array
     */
    private $extra_data;

    public function __construct(Builder $query, $range, $range_column, $extra_data = [])
    {
        $this->range = $this->setRange($range);
        $this->range_column = $range_column;
        $this->query = $query;
        $this->extra_data = $extra_data;
    }

    public function count()
    {
        $actual = $this->calculate('count', '*', $this->range['actual_date_start'], $this->range['actual_date_end']);
        $compare = $this->calculate('count', '*', $this->range['compare_date_start'], $this->range['compare_date_end']);
        return $this->transformResult($actual, $compare, 'count');
    }

    private function calculate($function, $column, $range_start, $range_end)
    {
        return (clone $this->query)
            ->whereDate($this->range_column, '>=', $range_start)
            ->whereDate($this->range_column, '<=', $range_end)
            ->$function($column);
    }

    private function transformResult($actual, $compare, $action)
    {
        return [
            'type' => 'value',
            'range' => $this->range,
            'actual' => (float)$actual,
            'compare' => (float)$compare,
            'ratio' => (float)(!$compare ? 0 : round(($actual / $compare - 1) * 100, 2)),
            'extra_data' => $this->extra_data,
            'action' => $action,
        ];
    }

    public function sum($column)
    {
        $actual = $this->calculate('sum', $column, $this->range['actual_date_start'], $this->range['actual_date_end']);
        $compare = $this->calculate('sum', $column, $this->range['compare_date_start'], $this->range['compare_date_end']);
        return $this->transformResult($actual, $compare, 'sum');
    }

    public function average($column)
    {
        $actual = $this->calculate('average', $column, $this->range['actual_date_start'], $this->range['actual_date_end']);
        $compare = $this->calculate('average', $column, $this->range['compare_date_start'], $this->range['compare_date_end']);
        return $this->transformResult($actual, $compare, 'average');
    }

    public function max($column)
    {
        $actual = $this->calculate('max', $column, $this->range['actual_date_start'], $this->range['actual_date_end']);
        $compare = $this->calculate('max', $column, $this->range['compare_date_start'], $this->range['compare_date_end']);
        return $this->transformResult($actual, $compare, 'max');
    }

    public function min($column)
    {
        $actual = $this->calculate('min', $column, $this->range['actual_date_start'], $this->range['actual_date_end']);
        $compare = $this->calculate('min', $column, $this->range['compare_date_start'], $this->range['compare_date_end']);
        return $this->transformResult($actual, $compare, 'min');
    }
}
