<?php

namespace Rockapps\RkLaravel\Metrics;

use Illuminate\Database\Eloquent\Builder;
use Rockapps\RkLaravel\Exceptions\ResourceException;

class MetricsTrend extends MetricsBase
{
    const GROUP_BY_DAYS = 'DAYS';
    const GROUP_BY_MONTHS = 'MONTHS';
    const GROUP_BY_YEARS = 'YEARS';

    private $range;
    private $group_by;
    private $range_column;
    /**
     * @var Builder
     */
    private $query_actual;
    /**
     * @var array
     */
    private $extra_data;

    public function __construct(Builder $query, $range, $range_column, $group_by, $extra_data = [])
    {
        $this->range = $this->setRange($range);
        $this->range_column = $range_column;
        $this->query = $query;
        $this->group_by = $group_by;
        $this->extra_data = $extra_data;
    }

    public function count()
    {
        $actual = $this->calculate('count', '*', $this->range['actual_date_start'], $this->range['actual_date_end']);
        $compare = $this->calculate('count', '*', $this->range['compare_date_start'], $this->range['compare_date_end']);
        return $this->transformResult($actual, $compare, 'count');
    }

    private function calculate($function, $column, $range_start, $range_end)
    {
        switch ($this->group_by) {
            case self::GROUP_BY_DAYS:
                $format = 'Y-m-d';
                $group_by = '1 day';
                break;
            case self::GROUP_BY_MONTHS:
                $format = 'Y-m';
                $group_by = '1 month';
                break;
            case self::GROUP_BY_YEARS:
                $format = 'Y';
                $group_by = '1 year';
                break;
        }

        $period = \Carbon\CarbonPeriod::create($range_start, $group_by, $range_end);
        $values = [];
        foreach ($period as $dt) {
            $values[] = [
                'date' => $dt->format($format),
                'value' => (clone $this->query)
                    ->when($this->group_by === self::GROUP_BY_DAYS, function (Builder $query) use ($dt) {
                        $query->whereDate($this->range_column, '=', $dt->format("Y-m-d"));
                    })
                    ->when($this->group_by === self::GROUP_BY_MONTHS, function (Builder $query) use ($dt) {
                        $query->whereMonth($this->range_column, '=', $dt->format("m"))
                            ->whereYear($this->range_column, '=', $dt->format("Y"));
                    })
                    ->when($this->group_by === self::GROUP_BY_YEARS, function (Builder $query) use ($dt) {
                        $query->whereYear($this->range_column, '=', $dt->format("Y"));
                    })
                    ->$function($column)
            ];
        }
        return ($values);
    }

    private function transformResult($actual, $compare, $action)
    {
        return [
            'type' => 'trend',
            'range' => $this->range,
            'actual' => $actual,
            'compare' => $compare,
            'group_by' => $this->group_by,
            'action' => $action,
            'extra_data' => $this->extra_data,
        ];
    }

    public function sum($column)
    {
        $actual = $this->calculate('sum', $column, $this->range['actual_date_start'], $this->range['actual_date_end']);
        $compare = $this->calculate('sum', $column, $this->range['compare_date_start'], $this->range['compare_date_end']);
        return $this->transformResult($actual, $compare, 'sum');
    }

    public function average($column)
    {
        $actual = $this->calculate('average', $column, $this->range['actual_date_start'], $this->range['actual_date_end']);
        $compare = $this->calculate('average', $column, $this->range['compare_date_start'], $this->range['compare_date_end']);
        return $this->transformResult($actual, $compare, 'average');
    }

    public function max($column)
    {
        $actual = $this->calculate('max', $column, $this->range['actual_date_start'], $this->range['actual_date_end']);
        $compare = $this->calculate('max', $column, $this->range['compare_date_start'], $this->range['compare_date_end']);
        return $this->transformResult($actual, $compare, 'max');
    }

    public function min($column)
    {
        $actual = $this->calculate('min', $column, $this->range['actual_date_start'], $this->range['actual_date_end']);
        $compare = $this->calculate('min', $column, $this->range['compare_date_start'], $this->range['compare_date_end']);
        return $this->transformResult($actual, $compare, 'min');
    }
}
