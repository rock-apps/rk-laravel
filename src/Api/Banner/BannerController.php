<?php

namespace Rockapps\RkLaravel\Api\Banner;


use DB;
use Illuminate\Http\Request;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\Banner;
use Rockapps\RkLaravel\Transformers\BannerTransformer;

/**
 * @group Banner
 *
 */
class BannerController extends ControllerBase
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $class = config('rk-laravel.banner.model', Banner::class);
        $banners = $class::filter($request->all())->get();

        return $this->showAll($banners, 200, config('rk-laravel.banner.transformer', BannerTransformer::class));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BannerSaveRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\InvalidBase64Data
     */
    public function store(BannerSaveRequest $request)
    {
        DB::beginTransaction();

        $class = config('rk-laravel.banner.model', Banner::class);
        $banner = new $class();
        $banner->fill($request->except(['medias_upload', 'image_upload']));
        $banner->save();

        if ($request->has('image_upload'))
            $banner->image = $this->addMedia($banner, $request->get('image_upload'));

        $banner->save();

        DB::commit();
        return $this->showOne($banner, 201, config('rk-laravel.banner.transformer', BannerTransformer::class));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function show($id)
    {
        /** @var Banner $banner */
        $class = config('rk-laravel.banner.model', Banner::class);
        $banner = $class::filter()->findOrFail($id);
        return $this->showOne($banner, 200, config('rk-laravel.banner.transformer', BannerTransformer::class));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\InvalidBase64Data
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $class = config('rk-laravel.banner.model', Banner::class);
        $banner = $class::filter()->findOrFail($id);

        $banner->fill($request->except(['medias_upload', 'image_upload']));

        if ($request->has('image_upload'))
            $banner->image = $this->addMedia($banner, $request->get('image_upload'));

        $banner->save();
        $this->addMedias($banner, $request->get('medias_upload'));

        DB::commit();
        return $this->showOne($banner, 200, config('rk-laravel.banner.transformer', BannerTransformer::class));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $class = config('rk-laravel.banner.model', Banner::class);
        $banner = $class::filter()->findOrFail($id);
        $banner->media()->delete();
        $banner->refresh();
        $banner->delete();

        return $this->successDelete();
    }
}
