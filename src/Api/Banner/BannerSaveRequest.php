<?php

namespace Rockapps\RkLaravel\Api\Banner;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 *
 * @bodyParam title string required Título do banner
 * @bodyParam active boolean required Título do banner
 * @bodyParam object_id integer
 * @bodyParam object_type string
 * @bodyParam position string
 * @bodyParam image_upload file Arquivo de imagem do banner
 * @bodyParam medias_upload file Arquivos de imagem do banner

 */
class BannerSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|string',
            'active' => 'required|boolean',
            'object_id' => 'required|numeric',
            'object_type' => 'required|string',
            'position' => 'nullable|string',
            'image_upload' => 'nullable',
            'medias_upload' => 'nullable'
        ];
    }
}
