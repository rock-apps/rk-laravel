<?php

namespace Rockapps\RkLaravel\Api\Payment;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam status required string
 * @bodyParam value required numeric
 * @bodyParam comments_operator nullable string
 */
class PaymentSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'status' => 'required|string',
            'value' => 'required|numeric|gt:0',
            'comments_operator' => 'nullable|string',
        ];
    }
}
