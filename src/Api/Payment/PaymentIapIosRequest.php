<?php

namespace Rockapps\RkLaravel\Api\Payment;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam iap_receipt_id required string,
 */
class PaymentIapIosRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'iap_receipt_id' => 'required|string',
            'apple_transaction_id' => 'required|string',
        ];
    }
}
