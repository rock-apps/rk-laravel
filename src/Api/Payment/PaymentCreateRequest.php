<?php

namespace Rockapps\RkLaravel\Api\Payment;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam payer_id required numeric
 * @bodyParam payer_type required string
 * @bodyParam purchaseable_id nullable numeric
 * @bodyParam purchaseable_type nullable string
 * @bodyParam value required numeric gt:0
 * @bodyParam gateway required string in:CASHPAGARME_CCPAGARME_BOLETOFATURABANK_TRANSFER
 * @bodyParam comments_operator nullable string
 * @bodyParam credit_card_id nullable numeric
 * @bodyParam address_id required numeric
 * @bodyParam card_holder_name nullable
 * @bodyParam card_expiration_date nullable
 * @bodyParam card_number nullable
 * @bodyParam card_cvv nullable
 */
class PaymentCreateRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'payer_id' => 'required|numeric',
            'payer_type' => 'required|string',
            'purchaseable_id' => 'nullable|numeric',
            'purchaseable_type' => 'nullable|string',
            'value' => 'required|numeric|gt:0',
            'gateway' => 'required|string',
            'comments_operator' => 'nullable|string',
            'credit_card_id' => 'nullable|numeric',
            'address_id' => 'required|numeric',
            'card_holder_name' => 'nullable',
            'card_expiration_date' => 'nullable|max:4|string',
            'card_number' => 'nullable',
            'card_cvv' => 'nullable',
        ];
    }
}
