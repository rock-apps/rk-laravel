<?php

namespace Rockapps\RkLaravel\Api\Payment;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam bank_transfers array required
 * @bodyParam id int required Payment ID
 */
class PaymentProvisionUpdateRequest extends RequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:payments,id',
            'bank_transfers' => 'required|array',
        ];
    }
}
