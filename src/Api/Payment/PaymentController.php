<?php

namespace Rockapps\RkLaravel\Api\Payment;

use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\Iap\IosPayment;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMe;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeTransaction;
use Rockapps\RkLaravel\Models\BankTransfer;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Services\MarketplaceSplitService;
use Rockapps\RkLaravel\Services\PaymentGatewayService;
use Rockapps\RkLaravel\Transformers\PaymentTransformer;

/**
 * @resource Payment
 * @group Payments
 *
 * Payment API Requests
 */
class PaymentController extends ControllerBase
{
    /**
     * Get All Payments
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\PaymentTransformer
     * @param PaymentIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(PaymentIndexRequest $request)
    {
        $payments = Payment::filter($request->all())->get();

        return $this->showAll($payments, 200, PaymentTransformer::class);
    }

    /**
     * Get specific Payment
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PaymentTransformer
     * @queryParam id required The id of the payment.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $payment = Payment::filter()->whereId($id)->firstOrFail();

        return $this->showOne($payment, 200, PaymentTransformer::class);
    }

    /**
     * Update Payment Status
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PaymentTransformer
     * @queryParam id required The id of the payment.
     * @param int $id
     * @param $status
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function changeStatus($id, $status)
    {
        DB::beginTransaction();
        $payment = Payment::filter()->whereId($id)->firstOrFail();
        $status = Str::upper($status);

        $payment->apply($status);
        $payment->save();

        DB::commit();

        return $this->showOne($payment, 200, PaymentTransformer::class);
    }

    /**
     * Create new Payment
     *
     * @transformer \Rockapps\RkLaravel\Transformers\PaymentTransformer
     * @authenticated
     * @param PaymentCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception|\GuzzleHttp\Exception\GuzzleException
     */
    public function store(PaymentCreateRequest $request)
    {
        DB::beginTransaction();
        $payment = new Payment($request->all());
        $payment->save();

        if (config('rk-laravel.marketplace.mode') === Constants::MARKETPLACE_MODE_MULTIPLE) {
            if ($payment->purchaseable && property_exists($payment->purchaseable, 'company') && $payment->purchaseable->company) {

                /** @var PaymentGatewayService $service */
                $service = config('rk-laravel.payment.service', PaymentGatewayService::class);
                $service->setCredentials($payment->purchaseable->company, $payment->gateway);

                $payment->company_id = $payment->purchaseable->company_id;
                $payment->save();
            }
        }

        /** @var MarketplaceSplitService $split_service */
        $split_service = config('rk-laravel.marketplace.split_service', MarketplaceSplitService::class);
        $split = $split_service::calculate($payment, []); // TODO: Definir o split data

        $payment->charge($request->get('item_title', 'Pagamento Individual'), $split, $request->all());

        DB::commit();

        return $this->showOne($payment, 201, PaymentTransformer::class);
    }

    /**
     * Update Payment
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PaymentTransformer
     * @param PaymentSaveRequest $request
     * @queryParam id required The id of the payment.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function update(PaymentSaveRequest $request, $id)
    {
        DB::beginTransaction();
        $payment = Payment::filter()->whereId($id)->firstOrFail();
        if ($payment->purchaseable && $payment->purchaseable->company) {
            $service = config('rk-laravel.payment.service', PaymentGatewayService::class);
            $service->setCredentials($payment->purchaseable->company, $payment->gateway);
        }

        $payment->fill($request->except(['payer_type', 'payer_id', 'purchaseable_type', 'purchaseable_id']));
        $payment->save();
        DB::commit();
        return $this->showOne($payment, 200, PaymentTransformer::class);
    }

    /**
     * Receive webhook from Pagar.Me gateways
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @response OK
     * @bodyParam notification string Gateway notification token
     */
    public function webhooksPagarMe(Request $request)
    {
        DB::beginTransaction();
        // O setCredentials está dentro do hook, pois o objeto é iniciado lá
        PagarMe::webhook($request);
        DB::commit();
        return $this->successResponse('OK', 200);
    }

    /**
     * Simulate PagarMe Status
     *
     * Essa request só pode ser utilizada em ambiente de homologação. Não funcionará em produção.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @response OK
     * @queryParam id integer Payment ID
     * @queryParam status string Status do pagamento. Utilizar o queryparam ?status=paid. Se não for enviado, será utilizado o 'paid'
     */
    public function simulatePagarmeStatus(Request $request, $id)
    {
        DB::beginTransaction();

        $payment = Payment::findOrFail($id);

        /** @var PaymentGatewayService $service */
        $service = config('rk-laravel.payment.service', PaymentGatewayService::class);

        if ($payment->purchaseable && $payment->purchaseable->company) {
            $service::setCredentials($payment->purchaseable->company, $payment->gateway);
        }

        $payment = PagarMeTransaction::simulateStatus($payment, $request->get('status','paid'));

        DB::commit();
        return $this->showOne($payment, 200);
    }

    /**
     * Payment Purchase IAP iOS
     *
     * @transformer \Rockapps\RkLaravel\Transformers\SubscriptionTransformer
     * @queryParam Subscription required The id of the Subscription.
     * @param PaymentIapIosRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Container\EntryNotFoundException
     * @throws \SM\SMException
     */
    public function paymentIapIos(PaymentIapIosRequest $request)
    {
        $user = \Auth::getUser();

        $payment = new Payment();
        $payment->payer_type = get_class($user);
        $payment->payer_id = $user->id;
        $payment->purchaseable_id = $request->get('purchaseable_id');
        $payment->purchaseable_type = $request->get('purchaseable_type');
        $payment->iap_receipt_id = $request->get('iap_receipt_id');
        $payment->apple_transaction_id = $request->get('apple_transaction_id');

        $payment = IosPayment::create($payment);

        $payment->refresh();
        return $this->showOne($payment, 201, PaymentTransformer::class);
    }

    /**
     * Create new Payment Provision
     *
     * @transformer \Rockapps\RkLaravel\Transformers\PaymentTransformer
     * @authenticated
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception|\GuzzleHttp\Exception\GuzzleException
     */
    public function storeProvisionPayment()
    {
        /** @var User $user */
//        $user = User::findOrFail(1);
//        dd($user);

        $user_class = config('rk-laravel.user.model', User::class);
        $user = $user_class::findOrFail(Parameter::getByKey(Constants::DEFAULT_MARKETPLACE_USER_ID)->value_int);
        DB::beginTransaction();
        /*
         * Boletão de provisionamento
         */
        if (!$user->address) throw new ResourceException('O usuário padrão não possui um endereço vinculado');

        $payment = new Payment([
            'value' => 1,
            'gateway' => Payment::GATEWAY_PAGARME_BOLETO,
            'direction' => Payment::DIRECTION_OUTCOME,
            'payer_id' => $user->id,
            'payer_type' => $user_class,
            'address_id' => $user->address->id,
            'async' => false,
            'boleto_instructions' => 'Provisionamento de Pagamento',
        ]);
        $payment->save();

        $bankTransfers = BankTransfer::whereStatus(BankTransfer::STATUS_REQUESTED)->get();

        foreach ($bankTransfers as $bankTransfer) {
            $bankTransfer->payment_id = $payment->id;
            $bankTransfer->save();
        }

        $payment->refresh();

        /*
         * Pagamento do Boleto de Provisionamento com SPLIT de pagamento para vários recebedores
         */
        $value = BankTransfer::whereStatus(BankTransfer::STATUS_REQUESTED)->sum('value');
        if ($value) {
            $payment->value = BankTransfer::whereStatus(BankTransfer::STATUS_REQUESTED)->sum('value');
            $payment->save();
            $payment->charge('Boleto Provisionamento');
            DB::commit();
        } else {
            DB::rollBack();
        }

        return $this->showOne($payment, 201, PaymentTransformer::class);
    }

    /**
     * Update Payment Provision
     *
     * @transformer \Rockapps\RkLaravel\Transformers\PaymentTransformer
     * @authenticated
     * @param PaymentProvisionUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception|\GuzzleHttp\Exception\GuzzleException
     */
    public function updateProvisionPayment(PaymentProvisionUpdateRequest $request)
    {
        /*
         * Boletão de provisionamento
         */
        $payment = Payment::findOrFail($request->get('id'));
        $payment->bankTransfers()->update(['payment_id' => null]);

        /*
         * Pagamento do Boleto de Provisionamento com SPLIT de pagamento para vários recebedores
         */
        foreach ($request->get('bank_transfers') as $bank_transfer) {
            $bank_transfer->payment_id = $payment->id;
            $bank_transfer->save();
        }
        $payment->refresh();
        $payment->value = $payment->bankTransfers()->sum('value');
        $payment->save();
        $payment->charge('Boleto Provisionamento');

        return $this->showOne($payment, 201, PaymentTransformer::class);
    }
}
