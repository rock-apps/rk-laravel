<?php
namespace Rockapps\RkLaravel\Api\Payment;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 *
 * @bodyParam status string
 * @bodyParam start_at date
 * @bodyParam end_at date
 * @bodyParam payer_id integer
 * @bodyParam payer_type string
 * @bodyParam purchaseable_id integer
 * @bodyParam purchaseable_type string
 */
class PaymentIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'status' => 'sometimes',
            'start_at' => 'sometimes',
            'end_at' => 'sometimes',
            'payer_id' => 'sometimes',
            'payer_type' => 'sometimes',
            'purchaseable_id' => 'sometimes',
            'purchaseable_type' => 'sometimes',
        ];
    }
}
