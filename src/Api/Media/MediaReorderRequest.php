<?php

namespace Rockapps\RkLaravel\Api\Media;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @queryParam medias array required
 */
class MediaReorderRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'medias' => 'required|array',
        ];
    }
}
