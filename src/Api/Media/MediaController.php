<?php

namespace Rockapps\RkLaravel\Api\Media;

use Exception;
use Illuminate\Http\JsonResponse;
use Rockapps\RkLaravel\Api\ControllerBase;
use Spatie\MediaLibrary\Models\Media;

/**
 * @resource Media
 * @group Medias
 *
 * Plan API Requests
 */
class MediaController extends ControllerBase
{

    /**
     * Reorder Media
     *
     * @response { "message" : "ok" }
     * @authenticated
     * @param MediaReorderRequest $request
     * @return JsonResponse
     */
    public function reorder(MediaReorderRequest $request)
    {
        $medias = collect($request->get('medias'));
        Media::setNewOrder($medias->pluck('id')->toArray());

        return $this->successResponseOk();
    }

    /**
     * Delete Media
     *
     * @authenticated
     * @queryParam id required The id of the Media.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @response { "message" : "ok" }
     */
    public function destroy($id)
    {
        /** @var Media $media */
        $media = Media::query()->findOrFail($id);
        $media->delete();

        return $this->successDelete();
    }


}
