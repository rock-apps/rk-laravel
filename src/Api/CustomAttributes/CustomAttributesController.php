<?php

namespace Rockapps\RkLaravel\Api\CustomAttributes;


use DB;
use Illuminate\Http\Request;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\CustomAttribute;
use Rockapps\RkLaravel\Transformers\CustomAttributesTransformer;

/**
 * @group CustomAttribute
 *
 */
class CustomAttributesController extends ControllerBase
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $class = config('rk-laravel.custom_attributes.model', CustomAttribute::class);
        $custom_attributes = $class::filter($request->all())->get();

        return $this->showAll($custom_attributes, 200, config('rk-laravel.custom_attributes.transformer', CustomAttributesTransformer::class));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CustomAttributesSaveRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(CustomAttributesSaveRequest $request)
    {
        DB::beginTransaction();

        $class = config('rk-laravel.custom_attributes.model', CustomAttribute::class);
        $custom_attribute = new $class();
        $custom_attribute->fill($request->all());
        $custom_attribute->save();

        DB::commit();
        return $this->showOne($custom_attribute, 201, config('rk-laravel.custom_attributes.transformer', CustomAttributesTransformer::class));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function show($id)
    {
        /** @var CustomAttribute $custom_attribute */
        $class = config('rk-laravel.custom_attributes.model', CustomAttribute::class);
        $custom_attribute = $class::filter()->findOrFail($id);
        return $this->showOne($custom_attribute, 200, config('rk-laravel.custom_attributes.transformer', CustomAttributesTransformer::class));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $class = config('rk-laravel.custom_attributes.model', CustomAttribute::class);
        $custom_attribute = $class::filter()->findOrFail($id);

        $custom_attribute->fill($request->all());
        $custom_attribute->save();

        DB::commit();
        return $this->showOne($custom_attribute, 200, config('rk-laravel.custom_attributes.transformer', CustomAttributesTransformer::class));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $class = config('rk-laravel.custom_attributes.model', CustomAttribute::class);
        $custom_attribute = $class::filter()->findOrFail($id);
        $custom_attribute->delete();

        return $this->successDelete();
    }
}
