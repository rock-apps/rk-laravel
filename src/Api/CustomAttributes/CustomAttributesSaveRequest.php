<?php

namespace Rockapps\RkLaravel\Api\CustomAttributes;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam object_type required string
 * @bodyParam object_id required int
 * @bodyParam field required string
 * @bodyParam cast required string
 * @bodyParam value required string
 */
class CustomAttributesSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'object_type' => 'required|string',
            'object_id' => 'required|int',
            'field' => 'required|string',
            'cast' => 'required|string',
            'value' => 'required|string',
        ];
    }
}
