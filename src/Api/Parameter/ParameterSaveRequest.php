<?php

namespace Rockapps\RkLaravel\Api\Parameter;

use Rockapps\RkLaravel\Api\RequestBase;
use Rockapps\RkLaravel\Models\Parameter;

/**
 * @bodyParam name string
 */
class ParameterSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Parameter::getRulesRequest();
    }
}
