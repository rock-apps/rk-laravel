<?php

namespace Rockapps\RkLaravel\Api\Parameter;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 *
 * @queryParam name string Description or key
 */
class ParameterIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'sometimes',
        ];
    }
}
