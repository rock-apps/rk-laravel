<?php

namespace Rockapps\RkLaravel\Api\Parameter;

use Exception;
use Illuminate\Http\JsonResponse;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\Parameter;
use Rockapps\RkLaravel\Transformers\ParameterTransformer;

/**
 * @group Parameter
 *
 * System API Requests
 */
class ParameterController extends ControllerBase
{
    /**
     * Get All Parameters
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\ParameterTransformer
     * @param ParameterIndexRequest $request
     * @return JsonResponse
     * @authenticated
     * @throws Exception
     */
    public function index(ParameterIndexRequest $request)
    {
        if (count($request->all())) {
            $parameters = Parameter::filter($request->all())->get();
        } else {
            $parameters = cache()->remember('parameters.all', config('rk-laravel.cache.parameters.ttl', 60), function () use ($request) {
                return Parameter::filter($request->all())->get();
            });
        }

        return $this->showAll($parameters,200, ParameterTransformer::class);
    }

    /**
     * Update Parameter
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\ParameterTransformer
     * @param ParameterSaveRequest $request
     * @param int $id
     * @return JsonResponse
     * @queryParam id required The id of the parameter.
     */
    public function update(ParameterSaveRequest $request, $id)
    {
        /** @var Parameter $parameter */
        $parameter = Parameter::findOrFail($id);

        $parameter->fill($request->all());
        $parameter->save();

        return $this->showOne($parameter, 200, ParameterTransformer::class);
    }

    /**
     * Get specific Parameter
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\ParameterTransformer
     * @queryParam id required The id of the payment.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $parameter = Parameter::filter()->whereId($id)->firstOrFail();

        return $this->showOne($parameter, 200, ParameterTransformer::class);
    }

}
