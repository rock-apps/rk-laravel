<?php

namespace Rockapps\RkLaravel\Api\Post;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam name string required
 * @bodyParam description string
 * @bodyParam headline string
 * @bodyParam content string
 * @bodyParam slug string
 * @bodyParam color_front string
 * @bodyParam color_back string
 * @bodyParam icon string
 * @bodyParam active boolean required
 * @bodyParam index string
 * @bodyParam model_type string required Verificar as opções disponíveis conforme o projeto
 */
class PostSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|string',
            'heading' => 'nullable|string',
            'slug' => 'nullable|string',
            'content' => 'nullable|string',
            'status' => 'required|string|in:DRAFT,PENDING,APPROVED',
            'type' => 'nullable|string',
            'owner_id' => 'nullable|numeric',
            'owner_type' => 'nullable|string',
            'visibility' => 'nullable|string',
            'approved_at' => 'nullable|date',
            'posted_at' => 'nullable|date',
        ];
    }
}
