<?php

namespace Rockapps\RkLaravel\Api\Post;

use Exception;
use Illuminate\Http\JsonResponse;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\Post;
use Rockapps\RkLaravel\Transformers\PostTransformer;

/**
 * @resource Post
 * @group Posts
 *
 * Post API Requests
 */
class PostController extends ControllerBase
{
    /**
     * Get All Posts
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\PostTransformer
     * @param PostIndexRequest $request
     * @return JsonResponse
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(PostIndexRequest $request)
    {
        $posts = Post::filter($request->all())->get();

        return $this->showAll($posts, 200, PostTransformer::class);
    }

    /**
     * Create new Post
     *
     * @transformer \Rockapps\RkLaravel\Transformers\PostTransformer
     * @authenticated
     * @param PostSaveRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function store(PostSaveRequest $request)
    {
        $post = new Post($request->all());

        if (!$request->get('owner_type')) $post->owner_type = config('rk-laravel.user.model');
        if (!$request->get('owner_id')) $post->owner_id = \Auth::getUser()->id;

        $post->save();

        return $this->showOne($post, 201, PostTransformer::class);
    }

    /**
     * Get specific Post
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PostTransformer
     * @queryParam id required The id of the post.
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        /** @var Post $post */
        $post = Post::filter()->findOrFail($id);

        return $this->showOne($post, 200, PostTransformer::class);
    }

    /**
     * Update Post
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PostTransformer
     * @param PostSaveRequest $request
     * @queryParam id required The id of the post.
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update(PostSaveRequest $request, $id)
    {
        $post = Post::filter()->findOrFail($id);
        $post->fill($request->all());
        $post->save();
        return $this->showOne($post, 200, PostTransformer::class);
    }

    /**
     * Delete Post
     *
     * @authenticated
     * @queryParam id required The id of the Post.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @response { "message" : "ok" }
     */
    public function destroy($id)
    {
        /** @var Post $post */
        $post = Post::filter()->findOrFail($id);

        $post->delete();

        return $this->successDelete();
    }
}
