<?php

namespace Rockapps\RkLaravel\Api\Post;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam text_size
 * @bodyParam bold
 * @bodyParam italic
 * @bodyParam color
 * @bodyParam style
 * @bodyParam content
 * @bodyParam type
 * @bodyParam sequence
 * @bodyParam post_id
 */
class PostBlockSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'text_size' => 'nullable|numeric',
            'bold' => 'nullable|boolean',
            'italic' => 'nullable|boolean',
            'color' => 'nullable|string',
            'style' => 'nullable|array',
            'content' => 'nullable|string',
            'type' => 'required|string|in:TEXT,IMAGE,LINK,YOUTUBE,UL,OL',
            'sequence' => 'required|numeric',
            'post_id' => 'required|numeric|exists:posts,id',
        ];
    }
}
