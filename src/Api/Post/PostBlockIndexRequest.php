<?php

namespace Rockapps\RkLaravel\Api\Post;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @queryParam post_id
 */
class PostBlockIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'post_id' => 'nullable|numeric',
        ];
    }
}
