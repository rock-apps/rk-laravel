<?php

namespace Rockapps\RkLaravel\Api\Post;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @queryParam title
 * @queryParam heading
 * @queryParam status
 * @queryParam slug
 * @queryParam owner_id
 * @queryParam owner_type
 * @queryParam visibility
 */
class PostIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'nullable|string',
            'heading' => 'nullable|string',
            'status' => 'nullable|string|in:DRAFT,PENDING,APPROVED',
            'owner_id' => 'nullable|numeric',
            'owner_type' => 'nullable|string',
            'visibility' => 'nullable|string',
        ];
    }
}
