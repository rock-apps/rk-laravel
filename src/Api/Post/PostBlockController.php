<?php

namespace Rockapps\RkLaravel\Api\Post;

use Exception;
use Illuminate\Http\JsonResponse;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\PostBlock;
use Rockapps\RkLaravel\Transformers\PostBlockTransformer;

/**
 * @resource PostBlockBlock
 * @group PostBlocks
 *
 * PostBlock API Requests
 */
class PostBlockController extends ControllerBase
{
    /**
     * Get All PostBlock
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\PostBlockTransformer
     * @param PostBlockIndexRequest $request
     * @return JsonResponse
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(PostBlockIndexRequest $request)
    {
        $post_blocks = PostBlock::filter($request->all())->get();

        return $this->showAll($post_blocks, 200, PostBlockTransformer::class);
    }

    /**
     * Create new PostBlock
     *
     * @transformer \Rockapps\RkLaravel\Transformers\PostBlockTransformer
     * @authenticated
     * @param PostBlockSaveRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function store(PostBlockSaveRequest $request)
    {
        $post = new PostBlock($request->all());
        $post->save();

        return $this->showOne($post, 201, PostBlockTransformer::class);
    }

    /**
     * Get specific PostBlock
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PostBlockTransformer
     * @queryParam id required The id of the post.
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        /** @var PostBlock $post */
        $post = PostBlock::filter()->findOrFail($id);

        return $this->showOne($post, 200, PostBlockTransformer::class);
    }

    /**
     * Update PostBlock
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PostBlockTransformer
     * @param PostBlockSaveRequest $request
     * @queryParam id required The id of the post.
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update(PostBlockSaveRequest $request, $id)
    {
        $post = PostBlock::filter()->findOrFail($id);
        $post->fill($request->all());
        $post->save();
        return $this->showOne($post, 200, PostBlockTransformer::class);
    }

    /**
     * Delete PostBlock
     *
     * @authenticated
     * @queryParam id required The id of the PostBlock.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @response { "message" : "ok" }
     */
    public function destroy($id)
    {
        /** @var PostBlock $post */
        $post = PostBlock::filter()->findOrFail($id);

        $post->delete();

        return $this->successDelete();
    }
}
