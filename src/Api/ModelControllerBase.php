<?php

namespace Rockapps\RkLaravel\Api;


class ModelControllerBase extends ControllerBase
{
    /**
     * @var null
     */
    private $transformer;
    /**
     * @var null
     */
    private $object;

    /**
     * ModelControllerBase constructor.
     * @param $object
     * @param null $transformer
     */
    public function __construct($object, $transformer = null)
    {
        $this->object = $object;
        $this->transformer = $transformer;
        parent::__construct();
    }

    /**
     * @transformerCollection \Rockapps\RkLaravel\Transformers\ModelTransformer
     * @param \Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(\Request $request)
    {
        $users = ($this->object)::filter($request->all())->get();

        return $this->showAll($users, 200, $this->transformer);
    }

    /**
     * @transformer \Rockapps\RkLaravel\Transformers\ModelTransformer
     * @param \Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(\Request $request)
    {
        $object = new ($this->object)($request->all());
        $object->save();

        return $this->showOne($object, 201, $this->transformer);
    }

    /**
     * @transformer \Rockapps\RkLaravel\Transformers\ModelTransformer
     * @queryParam id required The id of the user.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $object = ($this->object)::findOrFail($id);

        return $this->showOne($object, 200, $this->transformer);
    }

    /**
     * @transformer \Rockapps\RkLaravel\Transformers\ModelTransformer
     * @param \Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @queryParam id required The id of the object.
     */
    public function update(\Request $request, $id)
    {
        $user = ($this->object)::findOrFail($id);
        $user->fill($request->all());
        $user->save();

        return $this->showOne($user, 200, $this->transformer);
    }

    /**
     * @transformer \Rockapps\RkLaravel\Transformers\ModelTransformer
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @queryParam id required The id of the object.
     */
    public function destroy($id)
    {
        $user = ($this->object)::findOrFail($id);
        $user->delete();

        return $this->successDelete();
    }

    /**
     * @transformer \Rockapps\RkLaravel\Transformers\ModelTransformer
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @queryParam id required The id of the object.
     */
    public function restore($id)
    {
        $user = ($this->object)::withTrashed()->findOrFail($id);
        $user->restore();

        return $this->showOne($user, 200, $this->transformer);
    }


}
