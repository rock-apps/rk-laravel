<?php

namespace Rockapps\RkLaravel\Api;

use Dingo\Api\Exception\ValidationHttpException;
use Dingo\Api\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Rockapps\RkLaravel\Exceptions\RequestValidationException;

class RequestBase extends FormRequest
{
    /**
     * Handle a failed validation attempt.
     *
     * @param Validator $validator
     * @return void
     */
    protected function failedValidation(Validator $validator)
    {
        if ($this->container['request'] instanceof \Request) {
            throw new ValidationHttpException($validator->errors());
        }
        throw new RequestValidationException('Dados Inválidos', $validator->errors());
    }
}
