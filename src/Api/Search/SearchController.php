<?php

namespace Rockapps\RkLaravel\Api\Search;

use Dingo\Api\Http\Request;
use Illuminate\Http\JsonResponse;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Services\SearchService;

/**
 * @group Search
 *
 * Search API Requests
 */
class SearchController extends ControllerBase
{
    /**
     * Search Data
     *
     * @queryParam $term string
     * @param Request $request
     * @return JsonResponse
     */
    public function search(Request $request)
    {
        /** @var SearchService $service */
        $service = config('rk-laravel.search.service');
        if(!$service) throw new ResourceException('rk-laravel.search.service não definido');

        return $this->successResponse($service::run($request));
    }

}
