<?php

namespace Rockapps\RkLaravel\Api\Plan;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam value required numeric,
 * @bodyParam name required
 * @bodyParam gateway required string,
 * @bodyParam days required numeric,
 * @bodyParam trial_days required numeric,
 * @bodyParam payment_method nullable string,
 * @bodyParam color nullable string,
 * @bodyParam charges nullable numeric,
 * @bodyParam installments nullable numeric,
 * @bodyParam active required boolean,
 * @bodyParam invoice_reminder required numeric,
 * @bodyParam comments_operator nullable string,
 */
class PlanSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'code' => 'required|string',
            'value' => 'required|numeric',
            'gateway' => 'required|string',
            'days' => 'required|numeric',
            'trial_days' => 'required|numeric',
            'payment_method' => 'nullable|string',
            'color' => 'nullable|string',
            'charges' => 'nullable|numeric',
            'installments' => 'nullable|numeric',
            'active' => 'nullable|boolean',
            'invoice_reminder' => 'required|numeric',
            'comments_operator' => 'nullable|string',
        ];
    }
}
