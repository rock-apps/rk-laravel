<?php

namespace Rockapps\RkLaravel\Api\Plan;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @queryParam is_active integer
 * @queryParam apple bool
 * @queryParam google bool
 */
class PlanIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'is_active' => 'nullable|bool',
            'apple' => 'nullable|bool',
            'google' => 'nullable|bool',
        ];
    }
}
