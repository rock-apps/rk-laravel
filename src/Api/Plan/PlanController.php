<?php

namespace Rockapps\RkLaravel\Api\Plan;

use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMePlan;
use Rockapps\RkLaravel\Models\Plan;
use Rockapps\RkLaravel\Transformers\PlanTransformer;

/**
 * @resource Plan
 * @group Plans
 *
 * Plan API Requests
 */
class PlanController extends ControllerBase
{
    /**
     * Get All Plans
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\PlanTransformer
     * @param PlanIndexRequest $request
     * @return JsonResponse
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(PlanIndexRequest $request)
    {
        $plans = Plan::filter($request->all())->get();

        return $this->showAll($plans, 200, PlanTransformer::class);
    }

    /**
     * Create new Plan
     *
     * @transformer \Rockapps\RkLaravel\Transformers\PlanTransformer
     * @authenticated
     * @param PlanSaveRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function store(PlanSaveRequest $request)
    {
        DB::beginTransaction();
        $plan = new Plan();
        $plan->fill($request->except(['medias_upload', 'image_upload', 'variants']));
        $plan->save();
        $this->syncVariants($plan->variants(), $request->get('variants'), Plan::class);
        $this->addMedias($plan, $request->get('medias_upload'));
        $plan->save();
        DB::commit();

        return $this->showOne($plan, 201, PlanTransformer::class);
    }

    /**
     * Get specific Plan
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PlanTransformer
     * @queryParam id required The id of the plan.
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        /** @var Plan $plan */
        $plan = Plan::filter()->findOrFail($id);

        return $this->showOne($plan, 200, PlanTransformer::class);
    }

    /**
     * Update Plan
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PlanTransformer
     * @param PlanSaveRequest $request
     * @queryParam id required The id of the plan.
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update(PlanSaveRequest $request, $id)
    {
        $plan = Plan::filter()->findOrFail($id);
        $plan->fill($request->except(['medias_upload', 'image_upload', 'variants']));
        $plan->save();
        $this->syncVariants($plan->variants(), $request->get('variants'), Plan::class);
        $this->addMedias($plan, $request->get('medias_upload'));
        $plan->save();
        return $this->showOne($plan, 200, PlanTransformer::class);
    }

}
