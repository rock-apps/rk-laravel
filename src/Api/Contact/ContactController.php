<?php

namespace Rockapps\RkLaravel\Api\Contact;

use Exception;
use Illuminate\Support\Str;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Models\Parameter;

/**
 * @group Cotnact
 *
 * Contact API Requests
 */
class ContactController extends ControllerBase
{
    /**
     * Send New COntact Message by Mail
     *
     * @param ContactSendRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @transformerCollection \Rockapps\RkLaravel\Transformers\CommentTransformer
     */
    public function sendMail(ContactSendRequest $request)
    {
        $emails = Parameter::getByKey(Constants::CONTACT_MAIL_TO)->value;
        $subject = Parameter::getByKey(Constants::CONTACT_MAIL_SUBJECT)->value;

        $user = \Auth::getUser();
        $type = $request->get('type');// => 'required|string',
        $comments = $request->get('comments');// => 'required|string',
        $data = $request->get('data');// => 'nullable|array',
        $data_line = "\n";
        foreach ($data as $k => $item) {
            $data_line .= "<li>$k: $item</li>";
        }
        $data_line .= "\n";

        $html = "Usuário: $user->name<br>
            Usuário ID: $user->id<br>
            Usuário E-mail: $user->email<br>
            Assunto: $type<br>
            Comentários: $comments<br>
            Dados Extras: <br><ul>$data_line</ul>
            ";

        foreach (explode(' ', $emails) as $email) {

            \Mail::send([], [], function ($message) use ($type, $email, $html, $subject) {
                $message->to($email);
                $message->subject('[' . Str::upper($type) . '] ' . $subject);
                $message->setBody($html, 'text/html');
            });

        }
        return $this->successResponse(['data' => 'ok']);

    }

}
