<?php

namespace Rockapps\RkLaravel\Api\Contact;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam type string required
 * @bodyParam comments string required
 *
 */
class ContactSendRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type' => 'required|string',
            'comments' => 'required|string',
            'data' => 'nullable|array',
        ];
    }
}
