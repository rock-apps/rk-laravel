<?php

namespace Rockapps\RkLaravel\Api\Chat;

use Auth;
use Dingo\Api\Http\Request;
use Illuminate\Http\JsonResponse;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\Conversation;
use Rockapps\RkLaravel\Models\ConversationMessage;
use Rockapps\RkLaravel\Transformers\ChatConversationTransformer;

/**
 * @group Chat
 *
 * Chat API Requests
 */
class ChatController extends ControllerBase
{
    /**
     * Send new Chat Message
     *
     * @bodyParam message string required
     * @bodyParam chat_id int required
     * @bodyParam type string
     * @transformer \Rockapps\RkLaravel\Transformers\ChatMessageTransformer
     * @param ChatPostRequest $request
     * @param $chat_id
     * @return JsonResponse
     */
    public function storeMessage(ChatPostRequest $request, $chat_id)
    {
        $user = Auth::getUser();

        $conversation = Conversation::filter()->findOrFail($chat_id);

        if (!$conversation->hasParticipant($user->id)) {
            throw new ResourceException('Usuário não percente a esse chat');
        }

        $m = $conversation->sendMessage($request->get('message'), $user->id);

        return $this->showOne($m, 201, config('rk-laravel.chat.chat_message_transformer'));
    }

    /**
     * Get Chat Messages from Conversation
     *
     *
     * @transformer \Rockapps\RkLaravel\Transformers\ChatMessageTransformer
     * @param Request $request
     * @param $chat_id
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function showMessages(Request $request, $chat_id)
    {
        $user = Auth::getUser();

        $conversation = Conversation::filter()->findOrFail($chat_id);

        if (!$conversation->hasParticipant($user->id)) {
            throw new ResourceException('Usuário não percente a esse chat');
        }

        $conversation->readAll($user);

        $messages = ConversationMessage::filter($request->all())
            ->where('conversation_id', $conversation->id)
            ->get();

        return $this->showAll($messages, 200, config('rk-laravel.chat.chat_message_transformer'));
    }

    /**
     * Get Chat Messages from Conversation
     *
     *
     * @transformer \Rockapps\RkLaravel\Transformers\ChatMessageTransformer
     * @param $chat_id
     * @return JsonResponse
     */
    public function markAsRead($chat_id)
    {
        $user = Auth::getUser();

        $conversation = Conversation::filter()->findOrFail($chat_id);

        if (!$conversation->hasParticipant($user->id)) {
            throw new ResourceException('Usuário não percente a esse chat');
        }

        $conversation->readAll($user);

        return $this->showOne($conversation, 200, config('rk-laravel.chat.chat_conversation_transformer'));

    }


    /**
     * Get Chat Messages from Conversation
     *
     *
     * @transformer \Rockapps\RkLaravel\Transformers\ChatMessageTransformer
     * @param $chat_id
     * @return JsonResponse
     */
    public function show($chat_id)
    {
        $user = Auth::getUser();

        $conversation = Conversation::filter()->findOrFail($chat_id);

        if (!$conversation->hasParticipant($user->id)) {
            throw new ResourceException('Usuário não percente a esse chat');
        }

        return $this->showOne($conversation, 200, config('rk-laravel.chat.chat_conversation_transformer'));
    }

    /**
     * Get All Chat Rooms
     *
     * @transformer \Rockapps\RkLaravel\Transformers\ChatMessageTransformer
     * @param Request $request
     * @return JsonResponse|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $user = Auth::getUser();

        $conversations = Conversation::whereUserIsParticipant($user);

        return $this->showAll($conversations, 200, config('rk-laravel.chat.chat_conversation_transformer', ChatConversationTransformer::class));
    }

}
