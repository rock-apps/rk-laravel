<?php

namespace Rockapps\RkLaravel\Api\Chat;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam message string required
 * @bodyParam chat_id integer required
 * @bodyParam type string
 */
class ChatPostRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }
   public function rules()
    {
        return [
            'message' => 'required',
            'chat_id' => 'required|integer',
            'type' => 'sometimes'
        ];
    }
}
