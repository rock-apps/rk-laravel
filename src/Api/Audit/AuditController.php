<?php

namespace Rockapps\RkLaravel\Api\Audit;

use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Models\Audit;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\ModelBase;
use Rockapps\RkLaravel\Transformers\AuditTransformer;

/**
 * @resource Audit
 * @group Audits
 *
 * Audit API Requests
 */
class AuditController extends ControllerBase
{
    /**
     * Get All Audits
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\AuditTransformer
     * @param AuditIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @authenticated
     */
    public function index(AuditIndexRequest $request)
    {
        /** @var Auditable|\Rockapps\RkLaravel\Traits\Auditable|ModelBase $model */
        $model = $request->get('auditable_type');
        $auditable = $model::where('id', $request->get('auditable_id'))->first();

        return $this->showAll($auditable->audits()->get(), 200, AuditTransformer::class);
    }

    /**
     * Show Audit
     *
     * @transformer \Rockapps\RkLaravel\Transformers\AuditTransformer
     * @authenticated
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $audit = Audit::where('id', $id)->firstOrFail();

        return $this->showOne($audit, 200, AuditTransformer::class);
    }


}
