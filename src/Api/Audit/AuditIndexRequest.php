<?php

namespace Rockapps\RkLaravel\Api\Audit;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @queryParam auditable_id string
 * @queryParam auditable_type string
 */
class AuditIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'auditable_id' => 'sometimes',
            'auditable_type' => 'sometimes',
            'per_page' => 'nullable|integer|min:2',
            'page' => 'nullable|integer'
        ];
    }
}
