<?php

namespace Rockapps\RkLaravel\Api\CreditCard;

use Rockapps\RkLaravel\Api\RequestBase;


/**
 * @bodyParam holder_name string required Name of card holder
 * @bodyParam expiration_date string required Expiration date. 0822 = 08/2022
 * @bodyParam number string required CreditCard Number
 * @bodyParam cvv string required CVV
 * @bodyParam user_id int optional User ID. If user is admin, the field can be set. Otherwise, is automatically filled.
 * @bodyParam default bool optional
 *
 */
class CreditCardSaveRequest extends RequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'holder_name' => 'required',
            'expiration_date' => 'required|max:4|string',
            'number' => 'required',
            'cvv' => 'required',
            'default' => 'nullable|bool',
            'owner_id' => 'sometimes',
            'owner_type' => 'sometimes',
        ];
    }
}
