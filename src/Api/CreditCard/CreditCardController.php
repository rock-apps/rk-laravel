<?php

namespace Rockapps\RkLaravel\Api\CreditCard;

use Auth;
use DB;
use Exception;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeCreditCard;
use Rockapps\RkLaravel\Models\CreditCard;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Transformers\CreditCardTransformer;

/**
 * @resource CreditCard
 * @group CreditCards
 *
 * CreditCard API Requests
 */
class CreditCardController extends ControllerBase
{
    /**
     * Get All CreditCards
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\CreditCardTransformer
     * @param CreditCardIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @authenticated
     */
    public function index(CreditCardIndexRequest $request)
    {
        $creditCards = CreditCard::filter($request->all())->get();

        return $this->showAll($creditCards, 200, CreditCardTransformer::class);
    }

    /**
     * Create new CreditCard
     *
     * Create a new creditCard description
     *
     * @transformer \Rockapps\RkLaravel\Transformers\CreditCardTransformer
     * @authenticated
     * @param CreditCardSaveRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(CreditCardSaveRequest $request)
    {
        DB::beginTransaction();
        $data = $request->all(['holder_name', 'number', 'expiration_date', 'cvv', 'default']);
        $user = Auth::getUser();
        if ($user->hasRole('admin')) {
            $user_class = config('rk-laravel.user.model',User::class);
            $user = $user_class::whereId($request->get('owner_id'))->firstOrFail();
        }else{
            $user_class = config('rk-laravel.user.model',User::class);
            $user = $user_class::whereId($user->id)->firstOrFail();
        }
        $card = PagarMeCreditCard::create($data['holder_name'], $data['number'], $data['expiration_date'], $data['cvv'], $user);

        if ($request->get('default')) {
            $card->setDefault();
        }
        DB::commit();

        return $this->showOne($card, 201, CreditCardTransformer::class);
    }

    /**
     * Delete CreditCard
     *
     * @authenticated
     * @queryParam id required The id of the creditCard.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @response { "message" : "ok" }
     */
    public function destroy($id)
    {
        /** @var CreditCard $creditCard */
        $creditCard = CreditCard::filter()->where('id', $id)->firstOrFail();

        $creditCard->delete();

        return $this->successDelete();
    }

    /**
     * Set default credit card
     *
     * Cliente define o cartão de crédito parão para transações
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\CreditCardTransformer
     * @queryParam id required The id of the order.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function setDefault($id)
    {
        DB::beginTransaction();
        /** @var CreditCard $card */
        $creditCard = CreditCard::filter()->where('id', $id)->firstOrFail();

        $creditCard->setDefault();

        DB::commit();
        return $this->showOne($creditCard, 200, CreditCardTransformer::class);
    }

}
