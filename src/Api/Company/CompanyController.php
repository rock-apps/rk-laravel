<?php

namespace Rockapps\RkLaravel\Api\Company;


use DB;
use Illuminate\Http\Request;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Helpers\Image;
use Rockapps\RkLaravel\Models\Company;
use Rockapps\RkLaravel\Transformers\CompanyTransformer;

/**
 * @group Company
 *
 */
class CompanyController extends ControllerBase
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $class = config('rk-laravel.company.model', Company::class);
        $companys = $class::filter($request->all())->get();

        return $this->showAll($companys, 200, config('rk-laravel.company.transformer', CompanyTransformer::class));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CompanySaveRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\InvalidBase64Data
     */
    public function store(CompanySaveRequest $request)
    {
        DB::beginTransaction();

        /** @var Company $company */
        $class = config('rk-laravel.company.model', Company::class);
        $company = new $class();
        $company->fill($request->except(['medias_upload', 'logo_upload']));
        $company->save();

        if ($request->has('logo_upload'))
            $company->logo = $this->addMedia($company, $request->get('logo_upload'));
        $company->save();

        DB::commit();
        return $this->showOne($company, 201, config('rk-laravel.company.transformer', CompanyTransformer::class));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function show($id)
    {
        /** @var Company $company */
        $class = config('rk-laravel.company.model', Company::class);
        $company = $class::whereId($id)->firstOrFail();
        return $this->showOne($company, 200, config('rk-laravel.company.transformer', CompanyTransformer::class));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\InvalidBase64Data
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $class = config('rk-laravel.company.model', Company::class);
        $company = $class::findOrFail($id);

        $company->fill($request->except(['medias_upload', 'logo_upload']));

        if ($request->has('logo_upload'))
            $company->logo = $this->addMedia($company, $request->get('logo_upload'));
        $company->save();
        $this->addMedias($company, $request->get('medias_upload'));

        DB::commit();
        return $this->showOne($company, 200, config('rk-laravel.company.transformer', CompanyTransformer::class));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $class = config('rk-laravel.company.model', Company::class);
        $company = $class::filter()->whereId($id)->firstOrFail();
        $company->media()->delete();
        $company->refresh();
        $company->delete();

        return $this->successDelete();
    }
}
