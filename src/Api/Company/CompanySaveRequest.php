<?php

namespace Rockapps\RkLaravel\Api\Company;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 *
 * @bodyParam name
 * @bodyParam headline
 * @bodyParam about
 * @bodyParam active
 * @bodyParam mobile
 * @bodyParam telephone
 * @bodyParam responsible_id
 * @bodyParam deliver_max_range
 * @bodyParam deliver_estimate_time
 * @bodyParam address_id
 */
class CompanySaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'nullable|string',
            'headline' => 'nullable',
            'about' => 'nullable',
            'active' => 'required|boolean',
            'mobile' => 'nullable',
            'telephone' => 'nullable',
            'responsible_id' => 'nullable|exists:users,id',
            'deliver_max_range' => 'nullable|numeric|min:1',
            'deliver_estimate_time' => 'nullable|numeric',
            'address_id' => 'nullable|numeric|exists:addresses,id',
        ];
    }
}
