<?php

namespace Rockapps\RkLaravel\Api\ShippingMethod;

use DB;
use Exception;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\ShippingMethod;
use Rockapps\RkLaravel\Transformers\ShippingMethodTransformer;

/**
 * @resource ShippingMethod
 * @group ShippingMethods
 *
 * ShippingMethod API Requests
 */
class ShippingMethodController extends ControllerBase
{
    /**
     * Get All ShippingMethods
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\ShippingMethodTransformer
     * @param ShippingMethodIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(ShippingMethodIndexRequest $request)
    {
        $shipping_methods = ShippingMethod::filter($request->all())->get();

        return $this->showAll($shipping_methods, 200, ShippingMethodTransformer::class);
    }

    /**
     * Get specific ShippingMethod
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\ShippingMethodTransformer
     * @queryParam id required The id of the shipping.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $shipping_method = ShippingMethod::filter()->whereId($id)->firstOrFail();

        return $this->showOne($shipping_method, 200, ShippingMethodTransformer::class);
    }

    /**
     * Create new ShippingMethod
     *
     * @transformer \Rockapps\RkLaravel\Transformers\PlanTransformer
     * @authenticated
     * @param ShippingMethodSaveRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(ShippingMethodSaveRequest $request)
    {
        $user = \Auth::getUser();
        DB::beginTransaction();
        $shipping_method = new ShippingMethod($request->except(['image_upload']));

        if ($user->hasRole('admin')) {
            $shipping_method->company_id = $request->get('company_id');
        } else {
            $shipping_method->company_id = $user->company_id;
        }

        $shipping_method->save();

        if ($request->has('image_upload'))
            $shipping_method->image = $this->addMedia($shipping_method, $request->get('image_upload'));

        $shipping_method->save();

        DB::commit();

        return $this->showOne($shipping_method, 201, ShippingMethodTransformer::class);
    }

    /**
     * Update ShippingMethod
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\ShippingMethodTransformer
     * @param ShippingMethodSaveRequest $request
     * @queryParam id required The id of the shipping.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function update(ShippingMethodSaveRequest $request, $id)
    {
        DB::beginTransaction();
        $shipping_method = ShippingMethod::filter()->whereId($id)->firstOrFail();

        $shipping_method->fill($request->except(['image_upload']));
        $shipping_method->save();

        if ($request->has('image_upload'))
            $shipping_method->image = $this->addMedia($shipping_method, $request->get('image_upload'));

        $shipping_method->save();

        DB::commit();
        return $this->showOne($shipping_method, 200, ShippingMethodTransformer::class);
    }


    /**
     * Delete ShippingMethod
     *
     * @authenticated
     * @queryParam id required The id of the ShippingMethod.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @response { "message" : "ok" }
     */
    public function destroy($id)
    {
        /** @var ShippingMethod $shipping_method */
        $shipping_method = ShippingMethod::filter()->where('id', $id)->firstOrFail();

        $shipping_method->delete();

        return $this->successDelete();
    }
}
