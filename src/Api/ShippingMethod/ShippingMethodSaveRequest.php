<?php

namespace Rockapps\RkLaravel\Api\ShippingMethod;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam gateway required

 * @bodyParam carrier required
 * @bodyParam carrier_key string
 * @bodyParam carrier_secret string
 * @bodyParam carrier_sandbox bool
 * @bodyParam company_id required int
 * @bodyParam address_id int
 * @bodyParam active required bool
 * @bodyParam image_upload string base64
 * @bodyParam title required string
 * @bodyParam description string
 * @bodyParam fixed_value nullable float
 */
class ShippingMethodSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'carrier' => 'required',
            'carrier_key' => 'nullable',
            'carrier_secret' => 'nullable',
            'carrier_sandbox' => 'nullable|bool',
            'company_id' => 'nullable|exists:companies,id',
            'address_id' => 'nullable|exists:addresses,id',
            'active' => 'required|bool',
            'image' => 'nullable',
            'title' => 'required',
            'description' => 'nullable',
            'fixed_value' => 'nullable|numeric',
        ];
    }
}
