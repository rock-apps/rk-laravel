<?php
namespace Rockapps\RkLaravel\Api\ShippingMethod;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam company integer
 * @bodyParam title string
 * @bodyParam active bool
 * @bodyParam carrier string
 */
class ShippingMethodIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'company' => 'sometimes',
            'title' => 'sometimes',
            'active' => 'sometimes',
            'carrier' => 'sometimes',
        ];
    }
}
