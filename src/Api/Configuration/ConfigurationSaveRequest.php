<?php

namespace Rockapps\RkLaravel\Api\Configuration;

use Rockapps\RkLaravel\Api\RequestBase;


/**
 * @bodyParam data array required
 *
 */
class ConfigurationSaveRequest extends RequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'configs' => 'required|array',
        ];
    }
}
