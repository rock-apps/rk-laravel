<?php

namespace Rockapps\RkLaravel\Api\Configuration;

use Exception;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\Configuration;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Transformers\ConfigurationTransformer;

/**
 * @resource Configuration
 * @group Configurations
 *
 * Configuration API Requests
 */
class ConfigurationController extends ControllerBase
{
    /**
     * Get User Configuration
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\ConfigurationTransformer
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @authenticated
     */
    public function index()
    {
        $configuration = Configuration::filter()->get()->first();
        if (!$configuration) {
            $configuration = new Configuration();
            $configuration->configurable_id = \Auth::getUser()->id;
            $configuration->configurable_type = config('rk-laravel.user.model', User::class);
            $configuration->save();
        }

        return $this->showOne($configuration, 200, ConfigurationTransformer::class);
    }

    /**
     * Create new Configuration
     *
     * Create a new Configuration description
     *
     * @transformer \Rockapps\RkLaravel\Transformers\ConfigurationTransformer
     * @authenticated
     * @param ConfigurationSaveRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(ConfigurationSaveRequest $request)
    {
        $configuration = Configuration::filter()->get()->first();
        if (!$configuration) {
            $configuration = new Configuration();
            $configuration->configurable_id = \Auth::getUser()->id;
            $configuration->configurable_type = config('rk-laravel.user.model', User::class);
            $configuration->save();
        }
        $configuration->configs = array_merge($configuration->configs, $request->get('configs'));
        $configuration->save();

        return $this->showOne($configuration, 201, ConfigurationTransformer::class);
    }

    /**
     * Update Configuration
     *
     * @transformer \Rockapps\RkLaravel\Transformers\ConfigurationTransformer
     * @authenticated
     * @param ConfigurationSaveRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ConfigurationSaveRequest $request)
    {
        $configuration = Configuration::filter()->get()->first();
        if (!$configuration) {
            $configuration = new Configuration();
            $configuration->configurable_id = \Auth::getUser()->id;
            $configuration->configurable_type = config('rk-laravel.user.model', User::class);
            $configuration->save();
        }
        $configuration->configs = array_merge($configuration->configs, $request->get('configs'));
        $configuration->save();

        return $this->showOne($configuration, 200, ConfigurationTransformer::class);
    }


}
