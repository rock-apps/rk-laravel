<?php

namespace Rockapps\RkLaravel\Api\Variant;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 *
 * @bodyParam type string
 * @bodyParam value string
 *
 */
class VariantIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'nullable|string',
            'type' => 'nullable|string',
            'value' => 'nullable|string',
        ];
    }
}
