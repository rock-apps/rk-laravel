<?php

namespace Rockapps\RkLaravel\Api\Variant;

use Illuminate\Database\Eloquent\Builder;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\Variant;
use Rockapps\RkLaravel\Transformers\VariantTransformer;

/**
 * @group Variant
 *
 */
class VariantController extends ControllerBase
{
    /**
     * Display a listing of the variant.
     *
     * @param VariantIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(VariantIndexRequest $request)
    {
        /** @var Builder $variant_query */
        $variant = Variant::filter($request->all())->get();
        return $this->showAll($variant, 200,null);
    }

    /**
     * Store a newly created variant in storage.
     *
     * @param VariantSaveRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Exception
     * @throws \Exception
     */
    public function store(VariantSaveRequest $request)
    {
        \DB::beginTransaction();
        $variant = Variant::create($request->except(['image_upload']));
        if ($request->has('image_upload'))
            $variant->image = $this->addMedia($variant, $request->get('image_upload'));
        $variant->save();
        \DB::commit();
        return $this->showOne($variant, 201, VariantTransformer::class);
    }

    /**
     * Display the specified variant.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function show($id)
    {
        /** @var Variant $variant */
        $variant = Variant::filter()->whereId($id)->firstOrFail();
        return $this->showOne($variant, 200, VariantTransformer::class);
    }

    /**
     * Update the specified variant.
     *
     * @param VariantSaveRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Exception
     * @throws \Exception
     */
    public function update(VariantSaveRequest $request, $id)
    {
        \DB::beginTransaction();
        $variant = Variant::filter()->findOrFail($id);
        $variant->fill($request->except(['image_upload']));
        $variant->save();
        if ($request->has('image_upload'))
            $variant->image = $this->addMedia($variant, $request->get('image_upload'));
        $variant->save();
        \DB::commit();
        return $this->showOne($variant, 200, VariantTransformer::class);
    }

    /**
     * Remove the specified variant.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Exception
     * @throws \Exception
     */
    public function destroy($id)
    {
        Variant::filter()->findOrFail($id)->delete();
        return $this->successDelete();
    }
}
