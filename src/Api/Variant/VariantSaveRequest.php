<?php

namespace Rockapps\RkLaravel\Api\Variant;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 *
 * @bodyParam name string required The name of variant.
 * @bodyParam type string required The type of variant.
 * @bodyParam value string required The value of the variant.
 *
 */
class VariantSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'type' => 'required|string',
            'value' => 'required|string',
        ];
    }
}
