<?php

namespace Rockapps\RkLaravel\Api\Comment;

use Exception;
use Illuminate\Support\Collection;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\ModelFilters\CommentFilter;
use Rockapps\RkLaravel\Models\Comment;
use Rockapps\RkLaravel\Notifications\CommentDeleted;
use Rockapps\RkLaravel\Transformers\CommentTransformer;

/**
 * @group Comment
 *
 * Comment API Requests
 */
class CommentController extends ControllerBase
{
    /**
     * Get All Comments
     *
     * @return \Illuminate\Http\JsonResponse
     * @transformerCollection \Rockapps\RkLaravel\Transformers\CommentTransformer
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index()
    {
        /** @var Collection $Comments */
        $Comments = Comment::filter(request()->all(), CommentFilter::class)->get();

        return $this->showAll($Comments, 200, CommentTransformer::class);
    }


    /**
     * Get specific Comment
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @transformer \Rockapps\RkLaravel\Transformers\CommentTransformer
     */
    public function show($id)
    {
        /** @var Comment $Comment */
        $Comment = Comment::whereId($id)->filter([], CommentFilter::class)->firstOrFail();

        return $this->showOne($Comment, 200);
    }

    /**
     * Update Comment
     * @param CommentSaveRequest $request
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @transformer \Rockapps\RkLaravel\Transformers\CommentTransformer
     */
    public function update(CommentSaveRequest $request, $id)
    {
        /** @var Comment $Comment */
        $Comment = Comment::whereId($id)->filter([], CommentFilter::class)->firstOrFail();
        $Comment->fill($request->all());
        $Comment->save();

        return $this->showOne($Comment, 200);
    }

    /**
     * Delete Comment
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function destroy($id)
    {
        /** @var Comment $Comment */
        $Comment = Comment::whereId($id)->filter([], CommentFilter::class)->firstOrFail();
        $Comment->delete();

        if (method_exists($Comment->commenter, 'notify')) {
            $Comment->commenter->notify(new CommentDeleted($Comment));
        }

        return $this->successDelete();
    }

    /**
     * Create new Comment
     *
     * @transformer \Rockapps\RkLaravel\Transformers\CommentTransformer
     * @authenticated
     * @param CommentSaveRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CommentSaveRequest $request)
    {
        $comment = new Comment($request->all());
        $comment->commenter_id = \Auth::getUser()->id;
        $comment->commenter_type = get_class(\Auth::getUser());
        $comment->save();

        return $this->showOne($comment, 201, CommentTransformer::class);
    }

    /**
     * Restore Deleted Comment
     *
     * @transformer \Rockapps\RkLaravel\Transformers\CommentTransformer
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @queryParam id required The id of the object.
     */
    public function restore($id)
    {
        /** @var Comment $comments */
        $comments = Comment::whereId($id)->filter([], CommentFilter::class)->firstOrFail();
        $comments->restore();

        return $this->showOne($comments, 200, CommentTransformer::class);
    }
}
