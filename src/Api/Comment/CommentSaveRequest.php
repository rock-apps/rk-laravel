<?php

namespace Rockapps\RkLaravel\Api\Comment;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam comment string required
 * @bodyParam commentable_id integer required
 * @bodyParam commentable_type string required
 * @bodyParam commenter_id integer
 * @bodyParam commenter_type string
 *
 */
class CommentSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'comment' => 'required|string',
            'commentable_id' => 'required|numeric',
            'commentable_type' => 'required|string',
            'commenter_id' => 'nullable|numeric',
            'commenter_type' => 'nullable|string',
        ];
    }
}
