<?php

namespace Rockapps\RkLaravel\Api\Campaign;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam active bool required
 * @bodyParam campaign_id required
 * @bodyParam element_id required numeric
 * @bodyParam element_type required string
 * @bodyParam priority nullable numeric
 */
class CampaignElementSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'active' => 'bool|required',
            'campaign_id' => 'required|exists:campaigns,id',
            'element_id' => 'required|numeric',
            'element_type' => 'required|string',
            'priority' => 'nullable|numeric',
        ];
    }
}

