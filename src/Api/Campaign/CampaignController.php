<?php

namespace Rockapps\RkLaravel\Api\Campaign;

use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\Campaign;
use Rockapps\RkLaravel\Models\CampaignElement;
use Rockapps\RkLaravel\Models\CampaignHit;

/**
 * @group Campaign
 *
 * Campaign API Requests
 */
class CampaignController extends ControllerBase
{

    /**
     * Campaign View
     *
     * @return JsonResponse
     * @throws Exception
     * @transformerCollection \Rockapps\Transformers\CampaignTransformer
     */
    public function actionView()
    {
        $campaign = Campaign::getRandomActive();
        if (!$campaign) throw new ResourceException('Nenhuma campanha ativada');

        /** @var CampaignElement $campaign_element */
        $campaign_element = $campaign->elements()->where('active', true)->inRandomOrder()->first();
        if (!$campaign_element) throw new ResourceException('Nenhum elemento disponível na campanha');

        $element = $campaign_element->element;
        if (!$element) throw new ResourceException('O elemento não está disponível');

        DB::beginTransaction();

        $user = \Auth::getUser();
        $hit = new CampaignHit();
        $hit->user_id = $user ? $user->id : null;
        $hit->campaign_id = $campaign->id;
        $hit->method = CampaignHit::METHOD_VIEW;
        $hit->campaign_element_id = $campaign_element->id;
        $hit->element_id = $campaign_element->element_id;
        $hit->element_type = $campaign_element->element_type;
        $hit->ip = request()->ip();
        $hit->agent = request()->userAgent();
        $hit->save();

        DB::commit();

        $url_click = \App::make('url')->to("/v1/campaigns/action/click/{$campaign->id}/{$campaign_element->id}");
//        $url_media = '';

//        if ($element instanceof Banner) {
//            $url_media = $element->link_url;
//        }

        return $this->showData(collect([
            'url_click' => $url_click,
//            'url_media' => $url_media,
        ]));
    }

    /**
     * Campaign Click
     *
     * @queryParam campaign_id
     * @queryParam campaign_element_id
     * @return \Illuminate\Http\RedirectResponse
     * @transformerCollection \Rockapps\Transformers\CampaignTransformer
     * @throws Exception
     */
    public function actionClick($campaign_id, $campaign_element_id)
    {
        DB::beginTransaction();

        /** @var Campaign $campaign */
        # TODO: Verificar se a campanha e elemento podem ser usados. Utilizar um service?
        $campaign = Campaign::findOrFail($campaign_id);

        /** @var CampaignElement $campaign_element */
        $campaign_element = CampaignElement::filter()->where('campaign_id', $campaign->id)->findOrFail($campaign_element_id);

        $element = $campaign_element->element;
        if (!$element) throw new ResourceException('O elemento não está disponível');

        $user = \Auth::getUser();
        $hit = new CampaignHit();
        $hit->user_id = $user ? $user->id : null;
        $hit->campaign_id = $campaign->id;
        $hit->method = CampaignHit::METHOD_VIEW;
        $hit->campaign_element_id = $campaign_element->id;
        $hit->element_id = $campaign_element->element_id;
        $hit->element_type = $campaign_element->element_type;
        $hit->ip = request()->ip();
        $hit->agent = request()->userAgent();
        $hit->save();

        DB::commit();
//        return redirect()->away('https://www.google.com');
        return \Redirect::away($campaign->url);
//        return \Redirect::to($campaign->url);
    }

    /**
     * Get All Campaign
     *
     * @param CampaignIndexRequest $request
     * @return JsonResponse
     * @throws ValidationException
     * @transformerCollection \Rockapps\Transformers\CampaignTransformer
     */
    public function index(CampaignIndexRequest $request)
    {
        $campaigns = Campaign::filter($request->all())->get();
        return $this->showAll($campaigns);
    }

    /**
     * Create new Campaign
     *
     * Create a new Campaign description
     *
     * @param CampaignSaveRequest $request
     * @return JsonResponse
     * @throws Exception
     * @transformer \Rockapps\Transformers\CampaignTransformer
     */
    public function store(CampaignSaveRequest $request)
    {
        DB::beginTransaction();
        $user = \Auth::getUser();

        $campaign = new Campaign($request->all());
        if ($user->company_id) $campaign->company_id = $user->company_id;
        $campaign->save();

        $elements = collect($request->get('elements'))->map(function($item) use($campaign){
            $item['campaign_id'] = $campaign->id;
            return $item;
        });
        $campaign->syncRelationHasMany($elements->toArray(), 'elements', true);

        DB::commit();

        return $this->showOne($campaign, 201);
    }

    /**
     * Get specific Campaign
     *
     * @transformer \Rockapps\Transformers\CampaignTransformer
     * @queryParam Campaign required The id of the Campaign.
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        /** @varCampaign$campaign */
        $campaign = Campaign::filter()->findOrFail($id);
        return $this->showOne($campaign, 200);
    }

    /**
     * Update Campaign
     *
     * @transformer \Rockapps\Transformers\CampaignTransformer
     * @queryParam Campaign required The id of the Campaign.
     * @param CampaignSaveRequest $request
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update(CampaignSaveRequest $request, $id)
    {
        DB::beginTransaction();
        /** @var Campaign $campaign */
        $campaign = Campaign::filter()->findOrFail($id);
        $campaign->fill($request->except('medias_upload'));
        $campaign->save();

        $elements = collect($request->get('elements'))->map(function($item) use($campaign){
            $item['campaign_id'] = $campaign->id;
            return $item;
        });
        $campaign->syncRelationHasMany($elements, 'elements', true);

        $this->addMedias($campaign, $request->get('medias_upload'));
        DB::commit();

        return $this->showOne($campaign, 200);
    }

    /**
     * Delete Campaign
     *
     * @param int $id
     * @queryParam Campaign required The id of the Campaign.
     * @return \Illuminate\Http\JsonResponse
     * @urlParam Campaign required CampaignID
     * @throws Exception
     */
    public function destroy($id)
    {
        /** @varCampaign$campaign */
        $campaign = Campaign::filter()->findOrFail($id);
        $campaign->delete();

        return $this->successDelete();
    }

}
