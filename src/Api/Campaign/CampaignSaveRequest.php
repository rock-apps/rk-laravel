<?php

namespace Rockapps\RkLaravel\Api\Campaign;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam name string required
 * @bodyParam active bool required
 * @bodyParam company_id nullable exists:companies,id
 * @bodyParam actual_views nullable numeric
 * @bodyParam actual_clicks nullable numeric
 * @bodyParam max_views nullable numeric
 * @bodyParam max_clicks nullable numeric
 * @bodyParam priority nullable numeric
 * @bodyParam start_at nullable date
 * @bodyParam end_at nullable date
 */
class CampaignSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'string|required',
            'active' => 'bool|required',
            'actual_views' => 'nullable|numeric',
            'actual_clicks' => 'nullable|numeric',
            'max_views' => 'nullable|numeric',
            'max_clicks' => 'nullable|numeric',
            'priority' => 'nullable|numeric',
            'start_at' => 'nullable|date',
            'end_at' => 'nullable|date',
        ];
    }
}

