<?php

namespace Rockapps\RkLaravel\Api\Campaign;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam name string required
 */
class CampaignIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'nullable|string',
        ];
    }
}
