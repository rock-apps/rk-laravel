<?php

namespace Rockapps\RkLaravel\Api\Campaign;

use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\CampaignElement;

/**
 * @group CampaignElement
 *
 * CampaignElement API Requests
 */
class CampaignElementController extends ControllerBase
{

    /**
     * Create new CampaignElement
     *
     * Create a new CampaignElement description
     *
     * @param CampaignElementSaveRequest $request
     * @return JsonResponse
     * @throws Exception
     * @transformer \Rockapps\Transformers\CampaignElementTransformer
     */
    public function store(CampaignElementSaveRequest $request)
    {
        DB::beginTransaction();

        $campaign_element = new CampaignElement($request->except('medias_upload'));
        $campaign_element->save();
        $this->addMedias($campaign_element, $request->get('medias_upload'));
        DB::commit();

        return $this->showOne($campaign_element, 201);
    }

    /**
     * Get specific CampaignElement
     *
     * @transformer \Rockapps\Transformers\CampaignElementTransformer
     * @queryParam CampaignElement required The id of the CampaignElement.
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        /** @varCampaignElement$campaign_element */
        $campaign_element = CampaignElement::filter()->findOrFail($id);
        return $this->showOne($campaign_element, 200);
    }

    /**
     * Update CampaignElement
     *
     * @transformer \Rockapps\Transformers\CampaignElementTransformer
     * @queryParamCampaignElementrequired The id of the CampaignElement.
     * @param CampaignElementSaveRequest $request
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update(CampaignElementSaveRequest $request, $id)
    {
        DB::beginTransaction();
        /** @varCampaignElement$campaign_element */
        $campaign_element = CampaignElement::filter()->findOrFail($id);
        $campaign_element->fill($request->except('medias_upload'));
        $campaign_element->save();
        $this->addMedias($campaign_element, $request->get('medias_upload'));
        DB::commit();

        return $this->showOne($campaign_element, 200);
    }

    /**
     * Delete CampaignElement
     *
     * @param int $id
     * @queryParamCampaignElementrequired The id of the CampaignElement.
     * @return \Illuminate\Http\JsonResponse
     * @urlParamCampaignElementrequiredCampaignElementID
     * @throws Exception
     */
    public function destroy($id)
    {
        /** @varCampaignElement$campaign_element */
        $campaign_element = CampaignElement::filter()->findOrFail($id);
        $campaign_element->delete();

        return $this->successDelete();
    }

}
