<?php

namespace Rockapps\RkLaravel\Api\Address;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @queryParam owner_id string Owner ID
 * @queryParam owner_type string Owner Type
 */
class AddressIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'owner_id' => 'sometimes',
            'owner_type' => 'sometimes',
        ];
    }
}
