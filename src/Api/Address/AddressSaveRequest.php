<?php

namespace Rockapps\RkLaravel\Api\Address;

use Rockapps\RkLaravel\Api\RequestBase;


/**
 * @bodyParam name string
 * @bodyParam street string required
 * @bodyParam number string required
 * @bodyParam complement string
 * @bodyParam district string required
 * @bodyParam zipcode string required
 * @bodyParam city string required
 * @bodyParam state string required
 * @bodyParam country string required
 * @bodyParam lat string
 * @bodyParam lng string
 * @bodyParam default bool
 * @bodyParam related_id bool Somente admins podem definir
 * @bodyParam related_type bool Somente admins podem definir
 *
 */
class AddressSaveRequest extends RequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes',
            'street' => 'required',
            'number' => 'required',
            'complement' => 'sometimes',
            'district' => 'required',
            'zipcode' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'default' => 'nullable|bool',
            'lat' => 'sometimes',
            'lng' => 'sometimes',
            'related_id' => 'sometimes',
            'related_type' => 'sometimes',
        ];
    }
}
