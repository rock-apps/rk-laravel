<?php

namespace Rockapps\RkLaravel\Api\Address;

use Auth;
use Exception;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\Address;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Transformers\AddressTransformer;

/**
 * @resource Address
 * @group Addresss
 *
 * Address API Requests
 */
class AddressController extends ControllerBase
{
    /**
     * Get All Addresss
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\AddressTransformer
     * @param AddressIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @authenticated
     */
    public function index(AddressIndexRequest $request)
    {
        $Addresss = Address::filter($request->all())->get();

        return $this->showAll($Addresss, 200, AddressTransformer::class);
    }

    /**
     * Create new Address
     *
     * Create a new Address description
     *
     * @transformer \Rockapps\RkLaravel\Transformers\AddressTransformer
     * @authenticated
     * @param AddressSaveRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(AddressSaveRequest $request)
    {
        $address = new Address($request->all());

        $user = Auth::getUser();
        if ($user) {
            if ($user->hasRole('admin')) {
                $address->related_id = $request->get('related_id');
                $address->related_type = $request->get('related_type');
            } else {
                $address->related_id = $user->id;
                $address->related_type = config('rk-laravel.user.model', User::class);
            }
        }
        $address->save();

        if ($request->get('default')) {
            $address->setDefault();
        }

        return $this->showOne($address, 201, AddressTransformer::class);
    }

    /**
     * Update new Address
     *
     * @transformer \Rockapps\RkLaravel\Transformers\AddressTransformer
     * @authenticated
     * @param AddressSaveRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AddressSaveRequest $request, $id)
    {
        /** @var Address $address */
        $address = Address::filter()->where('id', $id)->firstOrFail();
        $address->fill($request->all());
        $address->save();

        if ($request->get('default')) {
            $address->setDefault();
        }

        return $this->showOne($address, 200, AddressTransformer::class);
    }

    /**
     * Show Address
     *
     * @transformer \Rockapps\RkLaravel\Transformers\AddressTransformer
     * @authenticated
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $address = Address::whereId($id)->filter()->firstOrFail();

        return $this->showOne($address, 200, AddressTransformer::class);
    }

    /**
     * Delete Address
     *
     * @authenticated
     * @queryParam id required The id of the Address.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @response { "message" : "ok" }
     */
    public function destroy($id)
    {
        /** @var Address $Address */
        $Address = Address::filter()->where('id', $id)->firstOrFail();

        $Address->default = false;
        $Address->save();
        $Address->delete();

        return $this->successDelete();
    }

    /**
     * Set default address
     *
     * Cliente define o endereço parão para transações
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\AddressTransformer
     * @queryParam id required The id of the address.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function setDefault($id)
    {
        /** @var Address $address */
        $address = Address::filter()->where('id', $id)->firstOrFail();
        $address->setDefault();

        return $this->showOne($address, 200, AddressTransformer::class);
    }

}
