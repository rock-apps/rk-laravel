<?php

namespace Rockapps\RkLaravel\Api\Category;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @queryParam name
 * @queryParam description
 * @queryParam headline
 * @queryParam slug
 * @queryParam active Opções: `true` `false`
 * @queryParam model_type Verificar as opções conforme o projeto.
 */
class CategoryIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'is_active' => 'sometimes|bool',
        ];
    }
}
