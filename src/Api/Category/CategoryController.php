<?php

namespace Rockapps\RkLaravel\Api\Category;

use Exception;
use Illuminate\Http\JsonResponse;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\Category;
use Rockapps\RkLaravel\Transformers\CategoryTransformer;

/**
 * @resource Category
 * @group Categories
 *
 * Category API Requests
 */
class CategoryController extends ControllerBase
{
    /**
     * Get All Categories
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\CategoryTransformer
     * @param CategoryIndexRequest $request
     * @return JsonResponse
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(CategoryIndexRequest $request)
    {
        $categories = Category::filter($request->all())->get();

        return $this->showAll($categories, 200, CategoryTransformer::class);
    }

    /**
     * Get All Categories Types
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\CategoryTransformer
     * @return JsonResponse
     * @authenticated
     */
    public function indexTypes()
    {
        $data = collect();

        Category::select('model_type')->distinct()->get()->each(function (Category $category) use($data) {
            $data[] = $category->model_type;
        });


        return $this->showData($data, 200);
    }

    /**
     * Create new Category
     *
     * @transformer \Rockapps\RkLaravel\Transformers\CategoryTransformer
     * @authenticated
     * @param CategorySaveRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function store(CategorySaveRequest $request)
    {
        $category = new Category($request->all());
        $category->save();
        $this->addMedias($category, $request->get('medias_upload'));

        return $this->showOne($category, 201, CategoryTransformer::class);
    }

    /**
     * Get specific Category
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\CategoryTransformer
     * @queryParam id required The id of the plan.
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        /** @var Category $category */
        $category = Category::filter()->findOrFail($id);

        return $this->showOne($category, 200, CategoryTransformer::class);
    }

    /**
     * Update Category
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\CategoryTransformer
     * @param CategorySaveRequest $request
     * @queryParam id required The id of the plan.
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update(CategorySaveRequest $request, $id)
    {
        $category = Category::filter()->findOrFail($id);
        $category->fill($request->all());
        $category->save();
        $this->addMedias($category, $request->get('medias_upload'));
        return $this->showOne($category, 200, CategoryTransformer::class);
    }

    /**
     * Delete Category
     *
     * @authenticated
     * @queryParam id required The id of the Category.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @response { "message" : "ok" }
     */
    public function destroy($id)
    {
        /** @var Category $category */
        $category = Category::filter()->findOrFail($id);

        $category->delete();

        return $this->successDelete();
    }
}
