<?php

namespace Rockapps\RkLaravel\Api\Category;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam name string required
 * @bodyParam description string
 * @bodyParam headline string
 * @bodyParam slug string
 * @bodyParam color_front string
 * @bodyParam color_back string
 * @bodyParam icon string
 * @bodyParam active boolean required
 * @bodyParam index string
 * @bodyParam model_type string required Verificar as opções disponíveis conforme o projeto
 */
class CategorySaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'slug' => 'nullable|string',
            'description' => 'nullable|string',
            'headline' => 'nullable|string',
            'color_front' => 'nullable|string',
            'color_back' => 'nullable|string',
            'icon' => 'nullable|string',
            'active' => 'required|boolean',
            'index' => 'nullable|numeric',
            'model_type' => 'required|string',
        ];
    }
}
