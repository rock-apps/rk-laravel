<?php

namespace Rockapps\RkLaravel\Api\User;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam id integer sometimes User ID - Somente para Admin.
 */
class UserCreateCustomerPagarMeRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'sometimes',
        ];
    }
}
