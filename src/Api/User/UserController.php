<?php

namespace Rockapps\RkLaravel\Api\User;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Helpers\Image;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeCustomer;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Transformers\UserTransformer;

/**
 * @resource User
 * @group Users
 *
 * User API Requests
 */
class UserController extends ControllerBase
{
    /**
     * Get All Users
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\UserTransformer
     * @param UserIndexRequest $request
     * @return JsonResponse
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(UserIndexRequest $request)
    {
        $users = User::filter($request->all())->get();

        $transformer = config('rk-laravel.user.transformer') ?: UserTransformer::class;
        return $this->showAll($users, 200, $transformer);
    }

    /**
     * Create new User
     *
     * Create a new User description
     *
     * @transformer \Rockapps\RkLaravel\Transformers\UserTransformer
     * @authenticated
     * @param UserSaveRequest $request
     * @return JsonResponse
     * @authenticated
     */
    public function store(UserSaveRequest $request)
    {
        $user = new User($request->all());
        $user->save();

        foreach ($request->get('roles',[]) as $role => $has_role) {
            if (!$has_role) continue;
            $role = Role::findOrFailByName($role);
            $user->attachRole($role);
        }
        $user->refresh();

        $transformer = config('rk-laravel.user.transformer') ?: UserTransformer::class;
        return $this->showOne($user, 201, $transformer);
    }

    /**
     * Get specific User
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\UserTransformer
     * @queryParam id required The id of the user.
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        /** @var User $user */
        $user = User::findOrFail($id);

        $transformer = config('rk-laravel.user.transformer') ?: UserTransformer::class;
        return $this->showOne($user, 200, $transformer);
    }

    /**
     * Update User
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\UserTransformer
     * @param UserSaveRequest $request
     * @param int $id
     * @return JsonResponse
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\InvalidBase64Data
     * @queryParam id required The id of the user.
     */
    public function update(UserSaveRequest $request, $id)
    {
        /** @var User $user */
        $user = User::findOrFail($id);
        $user->fill($request->all());
        $path = Image::storeMediable($request->get('photo_upload'), $user);
        if ($path) $user->photo = $path->getUrl();

        $user->save();

        foreach ($request->get('roles',[]) as $role => $add_role) {
            $role = Role::findOrFailByName($role);
            $user->detachRole($role);
            if ($add_role) {
                $user->attachRole($role);
            }
        }

        $transformer = config('rk-laravel.user.transformer') ?: UserTransformer::class;
        return $this->showOne($user, 200, $transformer);
    }

    /**
     * Create Customer in Pagar.Me
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\UserTransformer
     * @param UserCreateCustomerPagarMeRequest $request
     * @return JsonResponse
     */
    public function createCustomerPagarme(UserCreateCustomerPagarMeRequest $request)
    {
        /** @var User $user */
        $user = \Auth::getUser();
        if ($user->hasRole('admin')) {
            $user = User::findOrFail($request->get('id'));
        }

        $user = PagarMeCustomer::save($user);

        $transformer = config('rk-laravel.user.transformer') ?: UserTransformer::class;
        return $this->showOne($user, 200, $transformer);
    }

    /**
     * Add Role to User
     *
     * @authenticated
     * @queryParam user_id required The id of the user.
     * @queryParam role_id required The id of the role.
     * @transformer \Rockapps\RkLaravel\Transformers\UserTransformer
     * @param $user_id
     * @param $role_id
     * @return JsonResponse
     * @return Response
     * @throws Exception
     */
    public function roleAdd($user_id, $role_id)
    {
        /** @var User $user */
        $user = User::findOrFail($user_id);
        /** @var Role $role */
        $role = Role::find($role_id);
        if (!$role) {
            $role = Role::findOrFailByName($role_id);
        }
        if (!$user->hasRole($role->name)) {
            $user->attachRole($role->id);
        }

        # O pacote Entrust cria cache em cima das roles. É necessário limpar.
        \Cache::tags(config('entrust.role_user_table'))->flush();

        $transformer = config('rk-laravel.user.transformer') ?: UserTransformer::class;
        return $this->showOne($user, 201, $transformer);
    }

    /**
     * Remove Role from User
     *
     * @authenticated
     * @queryParam user_id required The id of the user.
     * @queryParam role_id required The id of the role.
     * @transformer \Rockapps\RkLaravel\Transformers\UserTransformer
     * @param $user_id
     * @param $role_id
     * @return JsonResponse
     * @return Response
     * @throws Exception
     */
    public function roleDestroy($user_id, $role_id)
    {
        /** @var User $user */
        $user = User::findOrFail($user_id);
        /** @var Role $role */
        $role = Role::find($role_id);
        if (!$role) {
            $role = Role::findOrFailByName($role_id);
        }

        $user->detachRole($role);

        $user->save();
        # O pacote Entrust cria cache em cima das roles. É necessário limpar.
        \Cache::tags(config('entrust.role_user_table'))->flush();

        $user->refresh();

        $transformer = config('rk-laravel.user.transformer') ?: UserTransformer::class;
        return $this->showOne($user, 200, $transformer);
    }

}
