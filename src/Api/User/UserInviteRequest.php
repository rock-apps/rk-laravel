<?php

namespace Rockapps\RkLaravel\Api\User;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam name string required Nome do convidado
 * @bodyParam email email required Email do convidado
 */
class UserInviteRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
        ];
    }
}
