<?php

namespace Rockapps\RkLaravel\Api\User;

use Auth;
use Dingo\Api\Http\Request;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\Image;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Models\UserDevice;
use Rockapps\RkLaravel\Notifications\UserEmailActivation;
use Rockapps\RkLaravel\Notifications\UserInvite;
use Rockapps\RkLaravel\Notifications\UserSignUpToken;
use Rockapps\RkLaravel\Services\UserSelfDestroyService;
use Rockapps\RkLaravel\Transformers\UserTransformer;
use Throwable;

/**
 * @resource User
 * @group UserLogged
 *
 * User API Requests
 */
class UserLoggedController extends ControllerBase
{
    /**
     * Validate user's SignUp token
     *
     * @param ValidateSignUpTokenRequest $request
     * @return JsonResponse
     */
    public function validateSignUpToken(ValidateSignUpTokenRequest $request)
    {
        $user = Auth::getUser();

        if ($user->signup_token !== (string)$request->get('signup_token')) {
            throw new ResourceException(__('Código de verificação incorreto'));
        }

        $user->signup_token = null;
        $user->verified = true;
        $user->save();

        $transformer = config('rk-laravel.user.transformer', UserTransformer::class);

        return $this->showOne($user, 200, $transformer);

    }

    /**
     * Add user device
     *
     * Add user device to receive notifications
     *
     * @transformer \Rockapps\RkLaravel\Transformers\UserTransformer
     * @queryParam registration_id required Registration id of the user device.
     * @param $registration_id
     * @return JsonResponse
     * @throws Throwable
     * @authenticated
     */
    public function addDevice($registration_id)
    {
        /** @var User $user */
        $user = Auth::getUser();
        /** @var UserDevice $device */
        $device = UserDevice::whereRegistrationId($registration_id)->withTrashed()->get()->first();

        if ($device) {
            $device->user_id = $user->id;
            $device->save();
            $device->restore();
        } else {
            UserDevice::create([
                'user_id' => $user->id,
                'registration_id' => $registration_id
            ]);
        }

        $transformer = config('rk-laravel.user.transformer', UserTransformer::class);

        return $this->showOne($user, 200, $transformer);
    }

    /**
     * Remove user device
     *
     * @transformer \Rockapps\RkLaravel\Transformers\UserTransformer
     * @queryParam registration_id required Registration id of the user device.
     * @param $registration_id
     * @return JsonResponse
     * @throws Throwable
     * @throws Exception
     * @authenticated
     */
    public function removeDevice($registration_id)
    {
        /** @var UserDevice $device */
        $device = UserDevice::whereUserId(Auth::getUser()->id)
            ->whereRegistrationId($registration_id)
            ->first();

        if ($device) $device->delete();

        return $this->successDelete();
    }

    /**
     * Update Me
     *
     * @transformer \Rockapps\RkLaravel\Transformers\UserTransformer
     * @param UserSaveRequest $request
     * @return JsonResponse
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\InvalidBase64Data
     * @authenticated
     */
    public function updateMe(UserSaveRequest $request)
    {
        /** @var User $user */
        $user = Auth::getUser();

        $send_activation_email = false;
        if ($user->email !== $request->get('email')) {
            $user->verified = 0;
            $send_activation_email = true;
        }

        $user->fill($request->except(['suspended', 'name', 'birth_date', 'document']));
        if (!$user->birth_date && $request->get('birth_date')) $user->birth_date = $request->get('birth_date');
        if (!$user->document && $request->get('document')) $user->document = $request->get('document');

        if ($request->get('photo_upload')) {
            $path = Image::storeMediable($request->get('photo_upload'), $user);
            if ($path) $user->photo = $path->getUrl();
        }

        $user->save();

        $notification = config('rk-laravel.sign_up.user_activation_notification');
        if ($notification && $send_activation_email) {
            $user->notify(new $notification($user));
        }

        $transformer = config('rk-laravel.user.transformer', UserTransformer::class);

        return $this->showOne($user, 200, $transformer);
    }

    /**
     * Send Activation Email
     *
     * @transformer \Rockapps\RkLaravel\Transformers\UserTransformer
     * @param Request $request
     * @return JsonResponse
     * @authenticated
     */
    public function sendActivationEmail(Request $request)
    {
        /** @var User $user */
        $user = Auth::getUser();
        $user->verification_token = $user->generateVerificationToken();
        $user->save();
        $notification = config('rk-laravel.sign_up.user_activation_notification', UserEmailActivation::class);
        if ($notification) {
            $user->notify(new $notification($user));
        }

        $transformer = config('rk-laravel.user.transformer', UserTransformer::class);

        return $this->showOne($user, 200, $transformer);
    }

    /**
     * Send SignUp Token Email
     *
     * @transformer \Rockapps\RkLaravel\Transformers\UserTransformer
     * @param Request $request
     * @return JsonResponse
     * @authenticated
     */
    public function sendSignUpTokenEmail(Request $request)
    {
        /** @var User $user */
        $user = Auth::getUser();
        if($user->verified) {
            throw new ResourceException('Seu cadastro já está validado!');
        }

        if (!$user->signup_token) {
            $user->signup_token = $user->generateSignUpToken();
            $user->save();
        }
        $notification = config('rk-laravel.sign_up.user_signup_token_notification', UserSignUpToken::class);
        if ($notification) {
            $user->notify(new $notification($user));
        }

        $transformer = config('rk-laravel.user.transformer', UserTransformer::class);

        return $this->showOne($user, 200, $transformer);
    }

    /**
     * Invite
     *
     * @transformer \Rockapps\RkLaravel\Transformers\UserTransformer
     * @param UserInviteRequest $request
     * @return JsonResponse
     * @authenticated
     */
    public function invite(UserInviteRequest $request)
    {
        /** @var User $user */
        $user = Auth::getUser();
        $user->notify(new UserInvite($request->get('email'), $request->get('name')));

        return $this->successResponse(['OK'], 200);
    }

    /**
     * Logged User Info
     *
     * @transformer \Rockapps\RkLaravel\Transformers\UserTransformer
     * @return JsonResponse
     * @authenticated
     */
    public function me()
    {
        $user = Auth::getUser();

        $transformer = config('rk-laravel.user.transformer', UserTransformer::class);

        return $this->showOne($user, 200, $transformer);
    }

    /**
     * Destroy User Logged
     *
     * @response { "status" : "ok" }
     * @return JsonResponse
     * @authenticated
     */
    public function destroyMe()
    {
        $user = Auth::getUser();

        $service = config('rk-laravel.user.user_self_destroy_service', UserSelfDestroyService::class);
        $service::run($user);

        return $this->successResponseOk();
    }

}
