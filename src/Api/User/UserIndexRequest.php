<?php

namespace Rockapps\RkLaravel\Api\User;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 *
 * @queryParam email string
 * @queryParam document string
 * @queryParam telephone string
 * @queryParam name string
 * @queryParam roles string Comma separated values with roles (eg: admin,assessor)
 */
class UserIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'roles' => 'sometimes',
            'email' => 'sometimes',
            'document' => 'sometimes',
            'telephone' => 'sometimes',
            'name' => 'sometimes',
        ];
    }
}
