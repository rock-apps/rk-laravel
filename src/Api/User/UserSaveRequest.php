<?php

namespace Rockapps\RkLaravel\Api\User;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam name string required User name
 * @bodyParam email string required User email
 * @bodyParam password string required User password
 * @bodyParam birth_date date
 * @bodyParam document string User CPF
 * @bodyParam photo_upload string
 * @bodyParam address_street string
 * @bodyParam address_number string
 * @bodyParam address_district string
 * @bodyParam address_zipcode string
 * @bodyParam address_city string
 * @bodyParam address_state string
 * @bodyParam mobile string
 * @bodyParam telephone string
 * @bodyParam suspended int
 */
class UserSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'sometimes',
            'email' => 'sometimes|email',
            'password' => 'sometimes',
            'birth_date' => 'nullable|date',
            'document' => 'nullable',
            'photo_upload' => 'nullable',
            'address_street' => 'nullable',
            'address_number' => 'nullable',
            'address_complement' => 'nullable',
            'address_district' => 'nullable',
            'address_zipcode' => 'nullable',
            'address_city' => 'nullable',
            'address_state' => 'nullable',
            'mobile' => 'nullable',
            'telephone' => 'nullable',
            'suspended' => 'nullable',
            'roles' => 'nullable|array',
        ];
    }
}
