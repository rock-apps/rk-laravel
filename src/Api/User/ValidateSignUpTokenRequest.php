<?php

namespace Rockapps\RkLaravel\Api\User;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam signup_token required string
 */
class ValidateSignUpTokenRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'signup_token' => 'required',
        ];
    }
}
