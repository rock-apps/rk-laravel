<?php

namespace Rockapps\RkLaravel\Api\BankAccount;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @queryParam owner_id string Owner ID
 * @queryParam owner_type string Owner Type
 */
class BankAccountIndexRequest extends RequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'owner_id' => 'sometimes',
            'owner_type' => 'sometimes',
        ];
    }
}
