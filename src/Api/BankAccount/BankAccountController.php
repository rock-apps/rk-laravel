<?php

namespace Rockapps\RkLaravel\Api\BankAccount;

use Auth;
use DB;
use Dingo\Api\Http\Request;
use Exception;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeBankAccount;
use Rockapps\RkLaravel\Models\BankAccount;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Services\CheckObjectPermissionService;
use Rockapps\RkLaravel\Transformers\BankAccountTransformer;

/**
 * @resource BankAccount
 * @group BankAccounts
 *
 * BankAccount API Requests
 */
class BankAccountController extends ControllerBase
{
    /**
     * Get All BankAccounts
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\BankAccountTransformer
     * @param BankAccountIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @authenticated
     */
    public function index(BankAccountIndexRequest $request)
    {
        $BankAccounts = BankAccount::filter($request->all())->get();

        return $this->showAll($BankAccounts, 200, BankAccountTransformer::class);
    }

    /**
     * Create new BankAccount
     *
     * Create a new BankAccount
     *
     * @transformer \Rockapps\RkLaravel\Transformers\BankAccountTransformer
     * @authenticated
     * @param BankAccountSaveRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(BankAccountSaveRequest $request)
    {
        DB::beginTransaction();

        $user = Auth::getUser();
        $user_class = config('rk-laravel.user.model', User::class);
        $user = $user_class::whereId($user->id)->firstOrFail();
        if ($user->hasRole('admin')) {
            $user = $user_class::whereId($request->get('owner_id'))->firstOrFail();
        }

        $gateway = $request->get('gateway', config('rk-laravel.bank_transfer.bank_account_default_gateway'));

//        /** @var CheckObjectPermissionService $permission */
//        $owner_type = $request->get('owner_type', $user_class);
//        $owner_id = $request->get('owner_id', $user->id);
//        $permission = config('rk-laravel.permission.object.service', CheckObjectPermissionService::class);
//        $object = $owner_type::findOrFail($owner_id);
//        $permission::run($user, $object, CheckObjectPermissionService::ACTION_WRITE);

        if ($gateway === BankAccount::GATEWAY_PGM) {
            $data = $request->all(['bank_code', 'agencia', 'agencia_dv', 'conta', 'conta_dv', 'document_number', 'legal_name', 'type', 'gateway']);
            $account = PagarMeBankAccount::create($user,
                $data['bank_code'],
                $data['agencia'],
                $data['agencia_dv'],
                $data['conta'],
                $data['conta_dv'],
                $data['document_number'],
                $data['legal_name'],
                $data['type']
            );
            $recipient = $account->syncWithBankRecipient();
            $account = $recipient->bankAccount;
        } else if ($gateway === BankAccount::GATEWAY_MANUAL) {
            $data = $request->all(['bank_code', 'agencia', 'agencia_dv', 'conta', 'conta_dv', 'document_number', 'legal_name', 'type', 'gateway', 'pix_key']);
            $account = new BankAccount();
            $account->fill($data);
            $account->owner_type = $user_class;
            $account->owner_id = $user->id;
            $account->save();
        } else {
            throw new ResourceException('Gateway inválido');
        }

        DB::commit();

        return $this->showOne($account, 201, BankAccountTransformer::class);
    }

    /**
     * Delete BankAccount
     *
     * @authenticated
     * @queryParam id required The id of the BankAccount.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @response { "message" : "ok" }
     */
    public function destroy($id)
    {
        /** @var BankAccount $BankAccount */
        $BankAccount = BankAccount::filter()->where('id', $id)->firstOrFail();

        $BankAccount->delete();

        return $this->successDelete();
    }

    /**
     * Get All Bank List
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @queryParam label string
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function indexBankList(Request $request)
    {
        $list = BankAccount::bankList();

        if ($request->has('label')) {
            $list = $list->filter(function ($item) use ($request) {
                return false !== stristr($item['label'], $request->get('label'));
            });
        }

        return $this->showData($list, 200);
    }
}
