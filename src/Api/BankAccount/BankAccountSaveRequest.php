<?php

namespace Rockapps\RkLaravel\Api\BankAccount;

use Rockapps\RkLaravel\Api\RequestBase;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\BankAccount;


/**
 * @bodyParam bank_code string required Código bancário ex 341
 * @bodyParam agencia string required Número da agência
 * @bodyParam agencia_dv string Dígito verificador da agência
 * @bodyParam conta string required Número da conta
 * @bodyParam conta_dv string Dígito verificador da conta
 * @bodyParam document_number integer required Número do documento CPF ou CNPJ
 * @bodyParam legal_name string required Nome da Pessoa Fìsica ou razão social do PJ
 * @bodyParam type string required Tipo de Conta
 * @bodyParam pix_key string Código PIX
 * @bodyParam owner_id integer
 * @bodyParam owner_type string
 *
 */
class BankAccountSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'type.required' => 'O campo Tipo de Conta é obrigatório.',
        ];
    }

    public function rules()
    {
        if (in_array($this->get('type'), [
            BankAccount::TYPE_CONTA_CORRENTE,
            BankAccount::TYPE_CONTA_POUPANCA,
            BankAccount::TYPE_CONTA_CORRENTE_CONJUNTA,
            BankAccount::TYPE_CONTA_POUPANCA_CONJUNTA
        ])) {
            return [
                'type' => 'required',
                'bank_code' => 'required',
                'agencia' => 'required',
                'conta' => 'required',
                'document_number' => 'required',
                'legal_name' => 'required|string|max:30'
            ];
        } else if ($this->get('type') === BankAccount::TYPE_PIX) {
            return [
                'type' => 'required',
                'pix_key' => 'required'
            ];
        } else {
            throw new ResourceException('Tipo de conta inválido' . $this->type);
        }

    }
}
