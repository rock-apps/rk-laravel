<?php

namespace Rockapps\RkLaravel\Api\Subscription;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 *
 * @bodyParam plan_id integer required ID do Plano
 */
class SubscriptionSubscribeInternalRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'plan_id' => 'required',
        ];
    }
}
