<?php

namespace Rockapps\RkLaravel\Api\Subscription;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam status required,
 * @bodyParam comments_operator nullable string,
 */
class SubscriptionUpdateRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'status' => 'nullable',
            'comments_operator' => 'nullable|string',
        ];
    }
}
