<?php

namespace Rockapps\RkLaravel\Api\Subscription;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam status required,
 * @bodyParam payment_method required
 * @bodyParam gateway required
 * @bodyParam mode required
 * @bodyParam plan_id required
 * @bodyParam subscriber_type required
 * @bodyParam subscriber_id required
 * @bodyParam subscribed_at
 * @bodyParam current_period_start required
 * @bodyParam current_period_end
 * @bodyParam comments_operator nullable string,
 */
class SubscriptionSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'status' => 'required',
            'payment_method' => 'required',
            'mode' => 'required',
            'gateway' => 'required',
            'comments_operator' => 'nullable|string',
            'plan_id' => 'required|numeric',
            'subscriber_type' => 'required|string',
            'subscriber_id' => 'required|numeric',
            'subscribed_at' => 'nullable|date',
            'current_period_start' => 'required|date',
            'current_period_end' => 'nullable|date',
        ];
    }
}
