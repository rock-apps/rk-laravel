<?php

namespace Rockapps\RkLaravel\Api\Subscription;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam iap_receipt_id required string,
 * @bodyParam apple_transaction_id required string,
 */
class SubscriptionIapGoogleRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'iap_receipt_id' => 'required|string',
            'plan_id' => 'required|exists:plans,id',
        ];
    }
}
