<?php

namespace Rockapps\RkLaravel\Api\Subscription;

use Dingo\Api\Http\Request;
use Exception;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Helpers\Iap\GoogleSubscription;
use Rockapps\RkLaravel\Helpers\Iap\IosSubscription;
use Rockapps\RkLaravel\Models\CreditCard;
use Rockapps\RkLaravel\Models\Plan;
use Rockapps\RkLaravel\Models\Subscription;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Transformers\SubscriptionTransformer;

/**
 * @group Subscription
 *
 * Subscription API Requests
 */
class SubscriptionController extends ControllerBase
{
    /**
     * Get All Subscriptions
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @transformerCollection \Rockapps\RkLaravel\Transformers\SubscriptionTransformer
     */
    public function index(Request $request)
    {
        $class = config('rk-laravel.subscription.model', Subscription::class);
        $subscriptions = $class::filter($request->all())->get();

        return $this->showAll($subscriptions, 200, config('rk-laravel.subscription.transformer', SubscriptionTransformer::class));
    }


    /**
     * Get specific Subscription
     *
     * @transformer \Rockapps\RkLaravel\Transformers\SubscriptionTransformer
     * @queryParam Subscription required The id of the Subscription.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var Subscription $subscription */
        $class = config('rk-laravel.subscription.model', Subscription::class);
        $subscription = $class::filter()->findOrFail($id);
        return $this->showOne($subscription, 200, config('rk-laravel.subscription.transformer', SubscriptionTransformer::class));

    }

    /**
     * Cancel specific Subscription
     *
     * @transformer \Rockapps\RkLaravel\Transformers\SubscriptionTransformer
     * @queryParam Subscription required The id of the Subscription.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Container\EntryNotFoundException
     * @throws \SM\SMException
     */
    public function cancel($id)
    {
        /** @var Subscription $subscription */
        $class = config('rk-laravel.subscription.model', Subscription::class);
        $subscription = $class::filter()->findOrFail($id);

        $subscription->apply(Subscription::STATUS_CANCELED);
        $subscription->save();
        return $this->showOne($subscription, 200, config('rk-laravel.subscription.transformer', SubscriptionTransformer::class));
    }

    /**
     * Create Subscription
     *
     * @param SubscriptionSaveRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @transformer \Rockapps\RkLaravel\Transformers\SubscriptionTransformer
     * @throws Exception
     */
    public function store(SubscriptionSaveRequest $request)
    {
        \DB::beginTransaction();

        /** @var Subscription $subscription */
        $class = config('rk-laravel.subscription.model', Subscription::class);
        $subscription = new $class();
        $subscription->fill($request->all());
        $subscription->save();

        \DB::commit();
        $subscription->refresh();

        return $this->showOne($subscription, 200, config('rk-laravel.subscription.transformer', SubscriptionTransformer::class));
    }

    /**
     * Update Subscription
     *
     * @param SubscriptionUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Container\EntryNotFoundException
     * @throws \SM\SMException
     * @transformer \Rockapps\RkLaravel\Transformers\SubscriptionTransformer
     */
    public function update(SubscriptionUpdateRequest $request, $id)
    {
        /** @var Subscription $subscription */
        $class = config('rk-laravel.subscription.model', Subscription::class);
        $subscription = $class::filter()->findOrFail($id);

        $subscription->fill($request->all(['comments_operator']));
        $subscription->save();

        if ($subscription->status !== $request->get('status')) {
            $subscription->apply($request->get('status'));
            $subscription->save();
        }

        $subscription->refresh();
        \DB::commit();

        return $this->showOne($subscription, 200, config('rk-laravel.subscription.transformer', SubscriptionTransformer::class));
    }

    /**
     * Subscription Purchase
     *
     * @transformer \Rockapps\RkLaravel\Transformers\SubscriptionTransformer
     * @queryParam Subscription required The id of the Subscription.
     * @param SubscriptionSubscribeRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function subscribe(SubscriptionSubscribeRequest $request)
    {
        $user = \Auth::getUser();
        \DB::beginTransaction();

        /** @var Plan $plan */
        $plan = Plan::filter()->whereId($request->get('plan_id'))->firstOrFail();
        /** @var CreditCard $card */
        $card = CreditCard::filter()->whereId($request->get('credit_card_id'))->firstOrFail();

        /** @var Subscription $subscription */
        $class = config('rk-laravel.subscription.model', Subscription::class);
        $subscription = new $class();
        $subscription->plan_id = $plan->id;
        $subscription->payment_method = Subscription::PAYMENT_METHOD_CREDIT_CARD;
        $subscription->mode = Subscription::MODE_RECURRENT;
        $subscription->gateway = Subscription::GATEWAY_PAGARME;
        $subscription->payer_type = config('rk-laravel.user.model', User::class);
        $subscription->payer_id = $user->id;
        $subscription->credit_card_id = $card->id;
        $subscription->subscriber_type = config('rk-laravel.user.model', User::class);
        $subscription->subscriber_id = $user->id;
        $subscription->address_id = $user->address ? $user->address->id : null;
        $subscription->save();

        \DB::commit();
        $subscription->refresh();
        return $this->showOne($subscription, 201, config('rk-laravel.subscription.transformer', SubscriptionTransformer::class));
    }


    /**
     * Subscription Purchase IAP iOS
     *
     * @transformer \Rockapps\RkLaravel\Transformers\SubscriptionTransformer
     * @queryParam Subscription required The id of the Subscription.
     * @param SubscriptionIapIosRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception|\GuzzleHttp\Exception\GuzzleException
     * @noinspection PhpUndefinedClassInspection
     */
    public function subscribeIapIos(SubscriptionIapIosRequest $request)
    {
        $user = \Auth::getUser();

        $subscription = IosSubscription::create($request->get('iap_receipt_id'), $user, $user, $request->get('apple_transaction_id'));

        $subscription->refresh();
        return $this->showOne($subscription, 201, config('rk-laravel.subscription.transformer', SubscriptionTransformer::class));
    }

    /**
     * Subscription Purchase IAP Google
     *
     * @transformer \Rockapps\RkLaravel\Transformers\SubscriptionTransformer
     * @queryParam Subscription required The id of the Subscription.
     * @param SubscriptionIapGoogleRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Container\EntryNotFoundException
     * @throws \SM\SMException
     * @noinspection PhpUndefinedClassInspection
     */
    public function subscribeIapGoogle(SubscriptionIapGoogleRequest $request)
    {
        $user = \Auth::getUser();

        $plan = Plan::findOrFail($request->get('plan_id'));

        $subscription = GoogleSubscription::create($request->get('iap_receipt_id'), $user, $user, $plan);

        $subscription->refresh();
        return $this->showOne($subscription, 201, config('rk-laravel.subscription.transformer', SubscriptionTransformer::class));
    }

    /**
     * Subscription Purchase with Balance
     *
     * @transformer \Rockapps\RkLaravel\Transformers\SubscriptionTransformer
     * @queryParam Subscription required The id of the Subscription.
     * @param SubscriptionSubscribeInternalRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public
    function subscribeBalance(SubscriptionSubscribeInternalRequest $request)
    {
        $user = \Auth::getUser();
        \DB::beginTransaction();

        /** @var Plan $plan */
        $plan = Plan::filter()->whereId($request->get('plan_id'))->firstOrFail();

        $subscription = Subscription::createBalance($plan, $user, $user, $user);

        \DB::commit();

        return $this->showOne($subscription, 201, config('rk-laravel.subscription.transformer', SubscriptionTransformer::class));
    }
}
