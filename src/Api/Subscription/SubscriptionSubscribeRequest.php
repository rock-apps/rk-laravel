<?php

namespace Rockapps\RkLaravel\Api\Subscription;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 *
 * @bodyParam plan_id integer required ID do Plano
 * @bodyParam credit_card_id string required ID do cartão de crédito
 */
class SubscriptionSubscribeRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'plan_id' => 'required',
            'credit_card_id' => 'required',
        ];
    }
}
