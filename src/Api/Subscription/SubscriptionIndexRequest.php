<?php

namespace Rockapps\RkLaravel\Api\Subscription;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam is_active integer
 * @bodyParam status string
 * @bodyParam gateway string
 * @bodyParam payment_method string
 * @bodyParam mode string
 * @bodyParam current_period_start date
 * @bodyParam current_period_end date
 * @bodyParam subscriber_id integer
 * @bodyParam subscriber_type string
 * @bodyParam plan_id integer
 */
class SubscriptionIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'is_active' => 'sometimes',
            'status' => 'sometimes',
            'gateway' => 'sometimes',
            'payment_method' => 'sometimes',
            'mode' => 'sometimes',
            'current_period_start' => 'sometimes',
            'current_period_end' => 'sometimes',
            'subscriber_id' => 'sometimes',
            'subscriber_type' => 'sometimes',
            'plan_id' => 'sometimes',
        ];
    }
}
