<?php

namespace Rockapps\RkLaravel\Api\Order;

use Auth;
use DB;
use Dingo\Api\Http\Request;
use Illuminate\Http\JsonResponse;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Models\OrderVirtualUnit;
use Rockapps\RkLaravel\Models\Payment;
use Rockapps\RkLaravel\Models\Product;
use Rockapps\RkLaravel\Transformers\OrderVirtualUnitTransformer;

/**
 * @group Orders Virtual Unit
 *
 * Order API Requests
 */
class OrderVirtualUnitController extends ControllerBase
{
    /**
     * Get All Orders Virtual Unit
     *
     * @param Request $request
     * @return JsonResponse
     * @transformerCollection \Rockapps\Transformers\OrderTransformer
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $class = config('rk-laravel.order_virtual_unit.model', OrderVirtualUnit::class);
        $orders = $class::filter($request->all())->get();

        return $this->showAll($orders, 200, config('rk-laravel.order_virtual_unit.transformer', OrderVirtualUnitTransformer::class));
    }

    /**
     * Create new Order Virtual Unit
     *
     * Create a new Order Virtual Unit description
     *
     * @param OrderVirtualUnitSaveRequest $request
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @transformer \Rockapps\Transformers\orderTransformer
     */
    public function store(OrderVirtualUnitSaveRequest $request)
    {
        DB::beginTransaction();
        $user = Auth::getUser();

        $class = config('rk-laravel.order_virtual_unit.model', OrderVirtualUnit::class);
        $order = new $class();

        $product = Product::findOrFail($request->get('product_id'));

        $iap_product_id = null;
        if($request->get('payment_gateway') === Payment::GATEWAY_METHOD_IAP_GOOGLE){
            $iap_product_id = $product->google_product_id;
        }elseif($request->get('payment_gateway') === Payment::GATEWAY_METHOD_IAP_GOOGLE){
            $iap_product_id = $product->apple_product_id;
        }

        if ($user->hasRole(Constants::ROLE_ADMIN)) {
            $order->fill($request->all());
            $order->save();
        } else if ($user->hasRole(Constants::ROLE_USER)) {
            $order->mode = OrderVirtualUnit::MODE_PURCHASE;
            $order->user_id = $user->id;
            $order->status = OrderVirtualUnit::STATUS_PENDING_PURCHASE;
            $order->payment_gateway = $request->get('payment_gateway');
            $order->product_id = $product->id;
            $order->total_value = $product->unit_value;
            $order->quantity = $request->get('quantity');
            $order->save();

            $payment = new Payment();
            $payment->payer_id = $user->id;
            $payment->payer_type = get_class($user);
            $payment->purchaseable_id = $order->id;
            $payment->purchaseable_type = OrderVirtualUnit::class;
            $payment->value = $order->total_value;
            $payment->gateway = $request->get('payment_gateway');
            $payment->change_purchaseable_state = true;
            $payment->iap_receipt_id = $request->get('iap_receipt_id');
            $payment->iap_product_id = $iap_product_id;
            $payment->product_id = $product->id;
            $payment->apple_transaction_id = $request->get('apple_transaction_id');
            $payment->google_order_id = $request->get('google_order_id');

            $payment->charge("PEDIDO-{$order->id}");
            $order->refresh();
        } else {
            throw new ResourceException('Você não possui permissão para criar pedidos');
        }

        DB::commit();

        return $this->showOne($order, 201, config('rk-laravel.order_virtual_unit.transformer', OrderVirtualUnitTransformer::class));
    }

    /**
     * Get specific Order Virtual Unit
     *
     * @transformer \Rockapps\Transformers\OrderTransformer
     * @queryParam Order required The id of the Order.
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        /** @var OrderVirtualUnit $order */
        $order = OrderVirtualUnit::filter()->findOrFail($id);

        return $this->showOne($order, 200, config('rk-laravel.order_virtual_unit.transformer', OrderVirtualUnitTransformer::class));
    }
}
