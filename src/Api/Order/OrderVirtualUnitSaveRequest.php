<?php

namespace Rockapps\RkLaravel\Api\Order;

use Rockapps\RkLaravel\Api\RequestBase;
use Rockapps\RkLaravel\Models\Payment;

/**
 * @bodyParam payment_gateway string required
 * @bodyParam credit_card_id int optional
 * @bodyParam product_id int required
 * @bodyParam user_id int optional
 * @bodyParam iap_receipt_id string
 * @bodyParam apple_transaction_id string
 */
class OrderVirtualUnitSaveRequest extends RequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'apple_transaction_id.required'=> 'Variável apple_transaction_id é obrigatória',
            'iap_receipt_id.required'=> 'Variável iap_receipt_id é obrigatória',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'payment_gateway' => 'required|string',
            'product_id' => 'required|exists:products,id',
            "user_id" => 'nullable|exists:users,id',
            'quantity' => 'required|numeric',
        ];

        if($this->get('payment_gateway') === Payment::GATEWAY_METHOD_IAP_APPLE) {
            $rules['iap_receipt_id'] = 'required|string';
            $rules['apple_transaction_id'] = 'required|string';
        }

        if($this->get('payment_gateway') === Payment::GATEWAY_METHOD_IAP_GOOGLE) {
            $rules['iap_receipt_id'] = 'required|string';
        }

        return $rules;
    }
}
