<?php

namespace Rockapps\RkLaravel\Api\PushNotification;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam name required string,
 * @bodyParam channel required string,
 * @bodyParam type required string,
 * @bodyParam destination required string,
 * @bodyParam scheduled_at required date,
 * @bodyParam title required string,
 * @bodyParam body nullable string,
 * @bodyParam object_type nullable string,
 * @bodyParam objecT_id nullable int,
 */
class PushNotificationSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'channel' => 'nullable|string',
            'type' => 'required|string',
            'destination' => 'required|string',
            'scheduled_at' => 'required|date',
            'title' => 'required|string',
            'body' => 'nullable|string',
            'object_type' => 'nullable|string',
            'objecT_id' => 'nullable|int',
        ];
    }
}
