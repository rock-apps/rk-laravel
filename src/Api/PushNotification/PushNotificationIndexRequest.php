<?php

namespace Rockapps\RkLaravel\Api\PushNotification;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @queryParam object_type
 * @queryParam object_id
 * @queryParam destination
 * @queryParam type
 * @queryParam status
 * @queryParam scheduled_at_start
 * @queryParam scheduled_at_end
 */
class PushNotificationIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'object_type' => 'sometimes|string',
            'object_id' => 'sometimes|integer',
            'destination' => 'sometimes|string',
            'type' => 'sometimes|string',
            'status' => 'sometimes|string',
            'scheduled_at_start' => 'sometimes|date',
            'scheduled_at_end' => 'sometimes|date',
        ];
    }
}
