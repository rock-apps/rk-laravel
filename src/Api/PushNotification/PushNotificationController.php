<?php

namespace Rockapps\RkLaravel\Api\PushNotification;

use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\PushNotification;
use Rockapps\RkLaravel\Services\PushNotificationService;
use Rockapps\RkLaravel\Transformers\PushNotificationTransformer;

/**
 * @resource PushNotification
 * @group PushNotifications
 *
 * PushNotification API Requests
 */
class PushNotificationController extends ControllerBase
{
    /**
     * Get All PushNotifications
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\PushNotificationTransformer
     * @return JsonResponse
     * @authenticated
     */
    public function destinations()
    {
        /** @var PushNotificationService $service */

        $service = \config('rk-laravel.push_notification.service', PushNotificationService::class);

        return $this->showData($service::destinations(), 200);
    }

    /**
     * Get All PushNotifications
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\PushNotificationTransformer
     * @param PushNotificationIndexRequest $request
     * @return JsonResponse
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(PushNotificationIndexRequest $request)
    {
        $pushNotifications = PushNotification::filter($request->all())->get();

        return $this->showAll($pushNotifications, 200, PushNotificationTransformer::class);
    }

    /**
     * Create new PushNotification
     *
     * @transformer \Rockapps\RkLaravel\Transformers\PushNotificationTransformer
     * @authenticated
     * @param PushNotificationSaveRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function store(PushNotificationSaveRequest $request)
    {
        DB::beginTransaction();
        $pushNotification = new PushNotification($request->all());
        $pushNotification->save();
        DB::commit();

        return $this->showOne($pushNotification, 201, PushNotificationTransformer::class);
    }

    /**
     * Get specific PushNotification
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PushNotificationTransformer
     * @queryParam id required The id of the pushNotification.
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        /** @var PushNotification $pushNotification */
        $pushNotification = PushNotification::filter()->findOrFail($id);

        return $this->showOne($pushNotification, 200, PushNotificationTransformer::class);
    }

    /**
     * Update PushNotification
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PushNotificationTransformer
     * @param PushNotificationSaveRequest $request
     * @queryParam id required The id of the pushNotification.
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update(PushNotificationSaveRequest $request, $id)
    {
        $pushNotification = PushNotification::filter()->findOrFail($id);
        $pushNotification->fill($request->all());
        $pushNotification->save();
        return $this->showOne($pushNotification, 200, PushNotificationTransformer::class);
    }

    /**
     * Update PushNotification Status
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PushNotificationTransformer
     * @queryParam id required The id of the pushNotification.
     * @param int $id
     * @param $status
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function changeStatus($id, $status)
    {
        DB::beginTransaction();
        $pushNotification = PushNotification::filter()->whereId($id)->firstOrFail();
        $status = Str::upper($status);

        $pushNotification->apply($status);
        $pushNotification->save();

        DB::commit();

        return $this->showOne($pushNotification, 200, PushNotificationTransformer::class);
    }

    /**
     * Delete Push Notification
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function destroy($id)
    {
        /** @var PushNotification $recipe */
        $recipe = PushNotification::where('id', '=', $id)->firstOrFail();
        $recipe->delete();

        return $this->successDelete();
    }

}
