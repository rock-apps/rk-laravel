<?php

namespace Rockapps\RkLaravel\Api\BankTransfer;

use Auth;
use DB;
use Exception;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Helpers\PagarMe\PagarMeRecipient;
use Rockapps\RkLaravel\Models\BankAccount;
use Rockapps\RkLaravel\Models\BankTransfer;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Services\BankTransferService;
use Rockapps\RkLaravel\Transformers\BankTransferTransformer;

/**
 * @resource BankTransfer
 * @group BankTransfers
 *
 * BankTransfer API Requests
 */
class BankTransferController extends ControllerBase
{
    /**
     * Get All BankTransfers
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\BankTransferTransformer
     * @param BankTransferIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @authenticated
     */
    public function index(BankTransferIndexRequest $request)
    {
        $BankTransfers = BankTransfer::filter($request->all())->get();

        $transformer = config('rk-laravel.bank_transfer.transformer', BankTransferTransformer::class);
        return $this->showAll($BankTransfers, 200, $transformer);
    }

    /**
     * Show BankTransfers
     *
     * @transformer \Rockapps\RkLaravel\Transformers\BankTransferTransformer
     * @authenticated
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $BankTransfer = BankTransfer::whereId($id)->filter()->firstOrFail();

        $transformer = config('rk-laravel.bank_transfer.transformer', BankTransferTransformer::class);
        return $this->showOne($BankTransfer, 200, $transformer);
    }

    /**
     * Create new BankTransfer
     *
     * Create a new BankTransfer description
     *
     * @transformer \Rockapps\RkLaravel\Transformers\BankTransferTransformer
     * @authenticated
     * @param BankTransferSaveRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(BankTransferSaveRequest $request)
    {
        DB::beginTransaction();

        $user = Auth::getUser();

        $user_class = config('rk-laravel.user.model', User::class);
        $user = $user_class::whereId($user->id)->firstOrFail();

        $bankTransfer = new BankTransfer();

        if ($user->hasRole('admin')) {
            $bankTransfer->fill($request->all());
        } else {

            if ($request->has('bank_account_id')) {
                $bankAccountObject = BankAccount::filter()->findOrFail($request->get('bank_account_id'));
            } else {
                $bankAccountObject = BankAccount::whereOwnerType(get_class($user))
                    ->whereOwnerId($user->id)
                    ->where('default', 1)
                    ->firstOrFail();
            }

            if (in_array($request->get('gateway'), [BankTransfer::GATEWAY_PAGARME_TRANSFER, BankTransfer::GATEWAY_PAGARME_PAYMENT_SPLIT])) {
                $bankAccountObject = PagarMeRecipient::save($bankAccountObject, $user, true);
            }

            $bankTransfer->transfered_type = get_class($user);
            $bankTransfer->transfered_id = $user->id;
            $bankTransfer->status = BankTransfer::STATUS_REQUESTED;
            $bankTransfer->bank_account_id = $bankAccountObject->id;
            $bankTransfer->gateway = $request->get('gateway');
            $bankTransfer->fee = 0;
            $bankTransfer->type = BankTransfer::TYPE_NEW;
            $bankTransfer->value = $request->get('value');

            /** @var BankTransferService $service */
            $service = config('rk-laravel.bank_transfer.service', BankTransferService::class);
            if ($service) {
                $bankTransfer = $service::validate($bankTransfer);
            }
        }

        $bankTransfer->save();

        DB::commit();

        $transformer = config('rk-laravel.bank_transfer.transformer', BankTransferTransformer::class);
        return $this->showOne($bankTransfer, 201, $transformer);
    }

    /**
     * Update BankTransfer
     *
     * @transformer \Rockapps\RkLaravel\Transformers\BankTransferTransformer
     * @authenticated
     * @param BankTransferSaveRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function update(BankTransferSaveRequest $request, $id)
    {
        DB::beginTransaction();

        $bankTransfer = BankTransfer::filter()->where('id', $id)->firstOrFail();
        $bankTransfer->fill($request->all());
        $bankTransfer->save();

        DB::commit();

        $transformer = config('rk-laravel.bank_transfer.transformer', BankTransferTransformer::class);
        return $this->showOne($bankTransfer, 200, $transformer);
    }

    /**
     * Delete BankTransfer
     *
     * @authenticated
     * @queryParam id required The id of the BankTransfer.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @response { "message" : "ok" }
     */
    public function destroy($id)
    {
        /** @var BankTransfer $BankTransfer */
        $BankTransfer = BankTransfer::filter()->where('id', $id)->firstOrFail();

        $BankTransfer->delete();

        return $this->successDelete();
    }

}
