<?php

namespace Rockapps\RkLaravel\Api\BankTransfer;

use Illuminate\Validation\Rule;
use Rockapps\RkLaravel\Api\RequestBase;
use Rockapps\RkLaravel\Models\BankTransfer;


/**
 * @bodyParam gateway string required
 * @bodyParam bank_account_id string
 * @bodyParam status string required
 * @bodyParam value float required
 * @bodyParam fee float required
 * @bodyParam transfered_id integer
 * @bodyParam transfered_type string
 *
 */
class BankTransferSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'gateway' => ['required', Rule::in(BankTransfer::GATEWAYS)],
            'bank_account_id' => 'nullable|exists:bank_accounts,id',
//            'bank_account_id' => 'nullable',
            'transfered_id' => 'nullable',
            'transfered_type' => 'nullable',
            'status' => 'nullable',
            'value' => 'required|numeric|min:0',
            'payment_id' => 'nullable|numeric|exists:payments',

//            'type' => 'required',
//            'fee' => 'required|numeric|min:0',
        ];
    }
}
