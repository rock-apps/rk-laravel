<?php

namespace Rockapps\RkLaravel\Api\BankTransfer;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @queryParam owner_id string Owner ID
 * @queryParam owner_type string Owner Type
 */
class BankTransferIndexRequest extends RequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gateway' => 'nullable',
            'bank_account_id' => 'nullable',
            'transfered_id' => 'nullable',
            'transfered_type' => 'nullable',
            'type' => 'nullable',
            'status' => 'nullable',
//            'fee' => 'required|numeric|min:0',
//            'value' => 'required|numeric|min:0',
            'payment_id' => 'nullable',
        ];
    }
}
