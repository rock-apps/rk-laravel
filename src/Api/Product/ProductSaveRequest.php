<?php

namespace Rockapps\RkLaravel\Api\Product;

use Illuminate\Validation\Rule;
use Rockapps\RkLaravel\Api\RequestBase;
use Rockapps\RkLaravel\Models\Product;

/**
 *
 * @bodyParam title string required Título do product
 * @bodyParam active boolean required Título do product
 * @bodyParam object_id integer
 * @bodyParam object_type string
 * @bodyParam position string
 * @bodyParam image_upload file Arquivo de imagem do product
 * @bodyParam medias_upload file Arquivos de imagem do product
 */
class ProductSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'type' => ['required', 'string', Rule::in(Product::TYPES)],
            'instance' => 'nullable|string',
            'variation_property' => 'nullable',
            'variation_value' => 'nullable',
            'description' => 'nullable',
            'active' => 'required|boolean',
            'weight' => 'nullable',
            'dimension_height' => 'nullable',
            'dimension_width' => 'nullable',
            'dimension_depth' => 'nullable',
            'unit_value' => 'required|numeric',
            'virtual_units_release' => 'nullable',
            'apple_product_id' => 'nullable',
            'google_product_id' => 'nullable',
            'active_ios' => 'nullable',
            'active_android' => 'nullable',
            'company_id' => 'nullable',
            'main_product_id' => 'nullable',
            'image_upload' => 'nullable',
            'medias_upload' => 'nullable'
        ];
    }
}
