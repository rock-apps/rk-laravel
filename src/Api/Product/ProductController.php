<?php

namespace Rockapps\RkLaravel\Api\Product;


use DB;
use Illuminate\Http\Request;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\Product;
use Rockapps\RkLaravel\Transformers\ProductTransformer;

/**
 * @group Product
 *
 */
class ProductController extends ControllerBase
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $class = config('rk-laravel.product.model', Product::class);
        $products = $class::filter($request->all())->get();

        return $this->showAll($products, 200, config('rk-laravel.product.transformer', ProductTransformer::class));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductSaveRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\InvalidBase64Data
     */
    public function store(ProductSaveRequest $request)
    {
        DB::beginTransaction();

        $class = config('rk-laravel.product.model', Product::class);
        $product = new $class();
        $product->fill($request->except(['medias_upload', 'image_upload', 'variants']));
        $product->save();
        $this->syncVariants($product->variants(), $request->get('variants'), $class);
        $this->addMedias($product, $request->get('medias_upload'));
        if ($request->has('image_upload'))
            $product->image = $this->addMedia($product, $request->get('image_upload'));
        $product->save();

        DB::commit();
        return $this->showOne($product, 201, config('rk-laravel.product.transformer', ProductTransformer::class));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function show($id)
    {
        /** @var Product $product */
        $class = config('rk-laravel.product.model', Product::class);
        $product = $class::filter()->findOrFail($id);
        return $this->showOne($product, 200, config('rk-laravel.product.transformer', ProductTransformer::class));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\InvalidBase64Data
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        /** @var Product $product */
        $class = config('rk-laravel.product.model', Product::class);
        $product = $class::filter()->findOrFail($id);

        $product->fill($request->except(['medias_upload', 'image_upload', 'variants']));

        if ($request->has('image_upload'))
            $product->image = $this->addMedia($product, $request->get('image_upload'));
        $product->save();
        $this->addMedias($product, $request->get('medias_upload'));
        if ($request->has('image_upload'))
            $product->image = $this->addMedia($product, $request->get('image_upload'));
        $product->save();
        $this->syncVariants($product->variants(), $request->get('variants'), $class);
        $product->save();

        DB::commit();
        return $this->showOne($product, 200, config('rk-laravel.product.transformer', ProductTransformer::class));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $class = config('rk-laravel.product.model', Product::class);
        $product = $class::filter()->findOrFail($id);
        $product->active = false;
        $product->save();
        $product->delete();

        return $this->successDelete();
    }
}
