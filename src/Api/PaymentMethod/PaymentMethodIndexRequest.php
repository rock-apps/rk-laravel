<?php
namespace Rockapps\RkLaravel\Api\PaymentMethod;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam company integer
 * @bodyParam title string
 * @bodyParam active bool
 * @bodyParam gateway string
 * @bodyParam method string
 */
class PaymentMethodIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'company' => 'sometimes',
            'title' => 'sometimes',
            'active' => 'sometimes',
            'gateway' => 'sometimes',
            'method' => 'sometimes',
        ];
    }
}
