<?php

namespace Rockapps\RkLaravel\Api\PaymentMethod;

use DB;
use Exception;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\PaymentMethod;
use Rockapps\RkLaravel\Transformers\PaymentMethodTransformer;

/**
 * @resource PaymentMethod
 * @group PaymentMethods
 *
 * PaymentMethod API Requests
 */
class PaymentMethodController extends ControllerBase
{
    /**
     * Get All PaymentMethods
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\PaymentMethodTransformer
     * @param PaymentMethodIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(PaymentMethodIndexRequest $request)
    {
        $payment_methods = PaymentMethod::filter($request->all())->get();

        return $this->showAll($payment_methods, 200, PaymentMethodTransformer::class);
    }

    /**
     * Get specific PaymentMethod
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PaymentMethodTransformer
     * @queryParam id required The id of the payment.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $payment_method = PaymentMethod::filter()->whereId($id)->firstOrFail();

        return $this->showOne($payment_method, 200, PaymentMethodTransformer::class);
    }

    /**
     * Create new PaymentMethod
     *
     * @transformer \Rockapps\RkLaravel\Transformers\PlanTransformer
     * @authenticated
     * @param PaymentMethodSaveRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(PaymentMethodSaveRequest $request)
    {
        $user = \Auth::getUser();
        DB::beginTransaction();
        $payment_method = new PaymentMethod($request->except(['image_upload']));

        if ($user->hasRole('admin')) {
            $payment_method->company_id = $request->get('company_id');
        } else {
            $payment_method->company_id = $user->company_id;
        }

        $payment_method->save();

        if ($request->has('image_upload'))
            $payment_method->image = $this->addMedia($payment_method, $request->get('image_upload'));

        $payment_method->save();

        DB::commit();

        return $this->showOne($payment_method, 201, PaymentMethodTransformer::class);
    }

    /**
     * Update PaymentMethod
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PaymentMethodTransformer
     * @param PaymentMethodSaveRequest $request
     * @queryParam id required The id of the payment.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function update(PaymentMethodSaveRequest $request, $id)
    {
        DB::beginTransaction();
        $payment_method = PaymentMethod::filter()->whereId($id)->firstOrFail();

        $payment_method->fill($request->except(['image_upload']));
        $payment_method->save();

        if ($request->has('image_upload'))
            $payment_method->image = $this->addMedia($payment_method, $request->get('image_upload'));

        $payment_method->save();

        DB::commit();
        return $this->showOne($payment_method, 200, PaymentMethodTransformer::class);
    }


    /**
     * Delete PaymentMethod
     *
     * @authenticated
     * @queryParam id required The id of the PaymentMethod.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @response { "message" : "ok" }
     */
    public function destroy($id)
    {
        /** @var PaymentMethod $payment_method */
        $payment_method = PaymentMethod::filter()->where('id', $id)->firstOrFail();

        $payment_method->delete();

        return $this->successDelete();
    }
}
