<?php

namespace Rockapps\RkLaravel\Api\PaymentMethod;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam gateway required
 * @bodyParam method required
 * @bodyParam gateway_key string
 * @bodyParam gateway_secret string
 * @bodyParam gateway_sandbox required bool
 * @bodyParam bank_account_id int
 * @bodyParam active required bool
 * @bodyParam image_upload string base64
 * @bodyParam title required
 * @bodyParam description string
 * @bodyParam gateway_soft_descriptor string
 * @bodyParam company_id int
 */
class PaymentMethodSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'gateway' => 'required',
            'method' => 'required',
            'gateway_key' => 'nullable',
            'gateway_secret' => 'nullable',
            'gateway_sandbox' => 'nullable|bool',
            'bank_account_id' => 'nullable|exists:bank_accounts,id',
            'active' => 'required|bool',
            'image' => 'nullable',
            'title' => 'required',
            'description' => 'nullable',
            'gateway_soft_descriptor' => 'nullable',
            'company_id' => 'nullable',
        ];
    }
}
