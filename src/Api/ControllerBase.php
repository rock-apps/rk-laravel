<?php

namespace Rockapps\RkLaravel\Api;

use Dingo\Api\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Collection;
use Illuminate\Validation\Validator;
use JWTAuth;
use Rockapps\RkLaravel\Exceptions\RequestValidationException;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\Image;
use Rockapps\RkLaravel\Helpers\Meta\MetaMessage;
use Rockapps\RkLaravel\Models\ModelBase;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Services\AuditService;
use Rockapps\RkLaravel\Services\CheckObjectPermissionService;
use Rockapps\RkLaravel\Transformers\AuditTransformer;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class ControllerBase extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /** @var User $currentUser */
    protected $currentUser;

    public function __construct()
    {
        try {
            if (!$this->currentUser = JWTAuth::parseToken()->authenticate()) {
                return $this->errorResponse('user_not_found', 404);
            }

        } catch (TokenExpiredException $e) {
            return $this->errorResponse('Token Expirado', 403);
        } catch (TokenInvalidException $e) {
            return $this->errorResponse('Token Inválido', 403);
        } catch (JWTException $e) {
            return $this->errorResponse('Token Ausente', 403);
        }
        return true;
    }

    protected function errorResponse($message, $code)
    {
        return response()->json([
            'error' => $message,
            'code' => $code
        ], $code);
    }

    /**
     * @param Validator $validator
     * @param string $message
     * @param int $code
     * @return bool
     */
    public function checkValidator($validator, $message = 'Requisiçao Inválida', $code = 500)
    {
        if ($validator->errors()->count()) {
            throw new RequestValidationException($message, $validator->errors());
        }
        return true;
    }

    /**
     * @param ModelBase|HasMediaTrait|HasMedia $object
     * @param $media_uploads
     * @return array
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\InvalidBase64Data
     */
    public function addMedias(ModelBase $object, $media_uploads = [])
    {
        $media_obj = [];
        if (!$object->id) throw new ResourceException('O objeto precisa ser criado antes de anexar uma mídia');

        if (!$media_uploads || !is_array($media_uploads)) return $media_obj;

        foreach ($media_uploads as $media_upload) {
            $obj = Image::storeMediable($media_upload, $object);
            if ($obj) $media_obj[] = $obj;
        }

        return $media_obj;
    }

    /**
     * @param BelongsToMany $object_relation
     * @param array $items
     * @param string $object_type
     * @return array
     */
    public function syncVariants(BelongsToMany $object_relation, $items, $object_type)
    {
        $items = collect($items);

        if (is_numeric($items->first())) {

            \DB::table('object_variants')
                ->where('object_type', '=', $object_type)
                ->where('object_id', '=', $object_relation->getParent()->id)
                ->delete();
            foreach ($items as $item) {
                $object_relation->attach($item, ['object_type' => $object_type]);
            }
            return $object_relation->sync($items->toArray(), true);
        }
        if (is_array($items->first())) {
            return $object_relation->sync($items->pluck('id')->toArray());
        }
        return [];
    }

    /**
     * @param ModelBase|HasMediaTrait|HasMedia $object
     * @param $media_upload
     * @return string
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\InvalidBase64Data
     */
    public function addMedia(ModelBase $object, $media_upload)
    {
        if ($media_upload) {
            if (!$object->id) throw new ResourceException('O objeto precisa ser criado antes de anexar uma mídia');
            $path = Image::storeMediable($media_upload, $object);
            if ($path) return $path->getUrl();
        }
        return null;
    }


    protected function successDelete($data = '', $code = 200)
    {
        if (!$data) {
            $data = ['message' => 'ok'];
        }
        return $this->successResponse($data, $code);
    }

    protected function successResponse($data, $code = 200)
    {
        return response()->json($data, $code);
    }

    protected function successResponseOk()
    {
        return response()->json(['status' => 'ok'], 200);
    }

    protected function showOne(Model $model, $code = 200, $transformer = null, $cache_tags = null, $cache_key = null)
    {
        if ($cache_key) {
            $cached = \Cache::tags($cache_tags)->get($cache_key);
            if ($cached) {
                return $this->successResponse($cached, $code);
            }
        }

        $model = $this->transformData($model, $transformer);

        if ($cache_key) {
            \Cache::tags($cache_tags)->put($cache_key, $model, config('rk-laravel.cache.parameters.ttl', 60));
        }

        return $this->successResponse($model, $code);
    }

    protected function transformData($data, $transformer = null)
    {
        if (!$transformer) {

            if ($data instanceof Collection) {
                if (!$data->count()) return ['data' => []];
                $elements = explode('\\', get_class($data->first()));
            } else if ($data instanceof LengthAwarePaginator) {
                /** @var @data LengthAwarePaginator */
                if (!$data->count()) return ['data' => []];
                $elements = explode('\\', get_class($data->first()));
            } else {
                $elements = explode('\\', get_class($data));
            }

            if ($elements[0] === 'Rockapps') {
                $transformer = '\\Rockapps\\RkLaravel\\Transformers\\' . $elements[3] . 'Transformer';
            } else if ($elements[1] === 'Models') {
                $transformer = '\\App\\Transformers\\' . $elements[2] . 'Transformer';
            } else {
                if (!is_object($data->first())) {
                    throw new ResourceException('Página inválida');
                }
                $elements = explode('\\', get_class($data->first()));
                $transformer = '\\App\\Transformers\\' . $elements[2] . 'Transformer';
            }
        }
        $transformation = fractal($data, new $transformer);
        return $this->processMeta($transformation->toArray());
    }

    private function processMeta($data)
    {
        if (!array_key_exists('meta', $data)) $data['meta'] = [];
        if (!array_key_exists('messages', $data['meta'])) $data['meta']['messages'] = [];

        try {
            $data['meta']['messages'] = app('meta-bag')->toArray();
            app('meta-bag')->clear();
        } catch (\Exception $e) {
            dump($e);
        }

        return $data;
    }

    private function message($message, $code)
    {
        return response()->json(['message' => $message], $code);
    }

    private function trackAudit(Request $request)
    {
        $only_values = $request->get('only_values');
        $field = $request->get('field');
        $object = $request->get('auditable_type')::findOrFail($request->get('auditable_id'));

        $this->checkObjectPermission($object);
        $data = AuditService::filterByAttribute($object, $field, $only_values);

        if ($only_values) {
            return $this->showData(collect($data));
        }

        return $this->showAll($data, 200, AuditTransformer::class);
    }

    /**
     * @param ModelBase $object
     * @param string $action
     * @param bool $throw_exception
     * @return mixed
     */
    private function checkObjectPermission($object, $action = CheckObjectPermissionService::ACTION_READ, $throw_exception = true)
    {
        /** @var CheckObjectPermissionService $service */
        $service = config('rk-laravel.permission.object.service', CheckObjectPermissionService::class);

        // Dispara exception on failure
        return $service::run(\Auth::getUser(), $object, $action, $throw_exception);
    }

    protected function showData(Collection $data, $code = 200)
    {
        $data = ['data' => $data];
        $data = $this->processMeta($data);
        return $this->successResponse($data, $code);
    }

    /**
     * @param Collection $collection
     * @param int $code
     * @param null $transformer
     * @param array $cache_tags
     * @param null $cache_key
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function showAll(Collection $collection, $code = 200, $transformer = null, $cache_tags = null, $cache_key = null)
    {
        if ($cache_key) {
            $cached = \Cache::tags($cache_tags)->get($cache_key);
            if ($cached) {
                return $this->successResponse($cached, $code);
            }
        }

        if ($collection->isEmpty()) {
            return $this->successResponse(['data' => $collection], $code);
        }
        $collection = $this->paginate($collection);

        $collection = $this->transformData($collection, $transformer);

        if ($cache_key) {
            \Cache::tags($cache_tags)->put($cache_key, $collection, config('rk-laravel.cache.parameters.ttl', 60));
        }

        return $this->successResponse($collection, $code);
    }

    /**
     * @param Collection $collection
     * @return LengthAwarePaginator|Collection
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function paginate(Collection $collection)
    {
        $rules = [
            'per_page' => 'integer|min:2'
        ];

        \Validator::validate(request()->all(), $rules);
        $page = LengthAwarePaginator::resolveCurrentPage();

        $per_page = null;
        if (request()->has('per_page')) {
            $per_page = (int)request()->per_page;
        }
        if (!$per_page) {
            return $collection;
        }

        $results = $collection->slice(($page - 1) * $per_page, $per_page)->values();

        $paginated = new LengthAwarePaginator($results, $collection->count(), $per_page, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);

        $paginated->appends(request()->all());

        return $paginated;
    }
}
