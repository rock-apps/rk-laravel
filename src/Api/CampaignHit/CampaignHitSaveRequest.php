<?php

namespace App\Api\CampaignHit;
<?php

namespace App\Api\CampaignHit;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam name string required
 */
class CampaignHitSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
        ];
    }
}

