<?php

namespace App\Api\CampaignHit;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam name string required
 */
class CampaignHitIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'nullable|string',
        ];
    }
}
