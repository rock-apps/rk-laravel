<?php

namespace App\Api\CampaignHit;

use App\Models\CampaignHit;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Rockapps\RkLaravel\Api\ControllerBase;

/**
 * @group CampaignHit
 *
 * CampaignHit API Requests
 */
class CampaignHitController extends ControllerBase
{
    /**
     * Get All CampaignHit
     *
     * @param CampaignHitIndexRequest $request
     * @return JsonResponse
     * @throws ValidationException
     * @transformerCollection \Rockapps\Transformers\CampaignHitTransformer
     */
    public function index(CampaignHitIndexRequest $request)
    {
        $campaign_hits = CampaignHit::filter($request->all())->get();
        return $this->showAll($campaign_hits);
    }

    /**
     * Create new CampaignHit
     *
     * Create a new CampaignHit description
     *
     * @param CampaignHitSaveRequest $request
     * @return JsonResponse
     * @throws Exception
     * @transformer \Rockapps\Transformers\CampaignHitTransformer
     */
    public function store(CampaignHitSaveRequest $request)
    {
        DB::beginTransaction();
        $user = \Auth::getUser();

        $campaign_hit = new CampaignHit($request->except('medias_upload'));
        $campaign_hit->save();
        $this->addMedias($campaign_hit, $request->get('medias_upload'));
        DB::commit();

        return $this->showOne($campaign_hit, 201);
    }

    /**
     * Get specific CampaignHit
     *
     * @transformer \Rockapps\Transformers\CampaignHitTransformer
     * @queryParam CampaignHit required The id of the CampaignHit.
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        /** @varCampaignHit$campaign_hit */
        $campaign_hit = CampaignHit::filter()->findOrFail($id);
        return $this->showOne($campaign_hit, 200);
    }

    /**
     * Update CampaignHit
     *
     * @transformer \Rockapps\Transformers\CampaignHitTransformer
     * @queryParamCampaignHitrequired The id of the CampaignHit.
     * @param CampaignHitSaveRequest $request
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update(CampaignHitSaveRequest $request, $id)
    {
        DB::beginTransaction();
        /** @varCampaignHit$campaign_hit */
        $campaign_hit = CampaignHit::filter()->findOrFail($id);
        $campaign_hit->fill($request->except('medias_upload'));
        $campaign_hit->save();
        $this->addMedias($campaign_hit, $request->get('medias_upload'));
        DB::commit();

        return $this->showOne($campaign_hit, 200);
    }

    /**
     * Delete CampaignHit
     *
     * @param int $id
     * @queryParamCampaignHitrequired The id of the CampaignHit.
     * @return \Illuminate\Http\JsonResponse
     * @urlParamCampaignHitrequiredCampaignHitID
     * @throws Exception
     */
    public function destroy($id)
    {
        /** @varCampaignHit$campaign_hit */
        $campaign_hit = CampaignHit::filter()->findOrFail($id);
        $campaign_hit->delete();

        return $this->successDelete();
    }

}
