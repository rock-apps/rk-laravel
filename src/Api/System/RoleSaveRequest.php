<?php

namespace Rockapps\RkLaravel\Api\System;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam name string required
 * @bodyParam description string
 * @bodyParam display_name string
 * @bodyParam system  bool
 */
class RoleSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'display_name' => 'nullable|string',
            'description' => 'nullable|string',
            'system' => 'nullable|boolean',
        ];
    }
}
