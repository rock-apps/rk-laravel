<?php

namespace Rockapps\RkLaravel\Api\System;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @queryParam name
 * @queryParam description
 * @queryParam system
 */
class PermissionIndexRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'system' => 'sometimes|bool',
            'name' => 'sometimes|bool',
            'description' => 'sometimes|bool',
        ];
    }
}
