<?php

namespace Rockapps\RkLaravel\Api\System;

use Exception;
use Illuminate\Http\JsonResponse;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\Permission;
use Rockapps\RkLaravel\Transformers\PermissionTransformer;

/**
 * @resource Permissions
 * @group Permissions
 *
 * Permission API Requests
 */
class PermissionController extends ControllerBase
{
    /**
     * Get All Permissions
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\PermissionTransformer
     * @param PermissionIndexRequest $request
     * @return JsonResponse
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(PermissionIndexRequest $request)
    {
        $class = config('entrust.permission', Permission::class);
        $permissions = $class::filter($request->all())->get();

        return $this->showAll($permissions, 200, PermissionTransformer::class);
    }

    /**
     * Create new Permission
     *
     * @transformer \Rockapps\RkLaravel\Transformers\PermissionTransformer
     * @authenticated
     * @param PermissionSaveRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function store(PermissionSaveRequest $request)
    {
        $class = config('entrust.permission', Permission::class);
        $plan = new $class($request->all());
        $plan->save();

        return $this->showOne($plan, 201, PermissionTransformer::class);
    }

    /**
     * Get specific Permission
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PermissionTransformer
     * @queryParam id required The id of the plan.
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $class = config('entrust.permission', Permission::class);
        $permission = $class::filter()->findOrFail($id);

        return $this->showOne($permission, 200, PermissionTransformer::class);
    }

    /**
     * Update Permission
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\PermissionTransformer
     * @param PermissionSaveRequest $request
     * @queryParam id required The id of the plan.
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update(PermissionSaveRequest $request, $id)
    {
        $class = config('entrust.permission', Permission::class);
        $permission = $class::filter()->findOrFail($id);
        $permission->fill($request->all());
        $permission->save();
        return $this->showOne($permission, 200, PermissionTransformer::class);
    }

    /**
     * Delete Permission
     *
     * @authenticated
     * @queryParam id required The id of the Permission.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @response { "message" : "ok" }
     */
    public function destroy($id)
    {
        $class = config('entrust.permission', Permission::class);

        /** @var Permission $permission */
        $permission =$class::whereId($id)->firstOrFail();

        $permission->delete();

        return $this->successDelete();
    }
}
