<?php

namespace Rockapps\RkLaravel\Api\System;

use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Constants\Constants;
use Rockapps\RkLaravel\Models\Parameter;

/**
 * @resource Políticas e Termos
 * @group Políticas e Termos
 *
 * Políticas e Termos API Requests
 */
class TermsController extends ControllerBase
{
    /**
     * Termos de Uso
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function tos()
    {
        $param = Parameter::whereKey(Constants::URL_TOS_TERMS)->first();
        $url = 'https://api.services.rockapps.com.br/termos-de-uso.pdf';
        if ($param) {
            $url = $param->value;
        }
        return \Redirect::to($url);
    }

    /**
     * Política de Privacidade
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function privacy()
    {
        $param = Parameter::whereKey(Constants::URL_PRIVACY_TERMS)->first();
        $url = 'https://api.services.rockapps.com.br/politica-de-privacidade.pdf';
        if ($param) {
            $url = $param->value;
        }
        return \Redirect::to($url);
    }

}
