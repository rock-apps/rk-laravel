<?php

namespace Rockapps\RkLaravel\Api\System;

use Exception;
use Illuminate\Http\JsonResponse;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\Permission;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Transformers\RoleTransformer;

/**
 * @resource Roles
 * @group Roles
 *
 * Role API Requests
 */
class RoleController extends ControllerBase
{
    /**
     * Get All Roles
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\RoleTransformer
     * @param RoleIndexRequest $request
     * @return JsonResponse
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(RoleIndexRequest $request)
    {
        $class = config('entrust.role', Role::class);
        $roles = $class::filter($request->all())->get();

        return $this->showAll($roles, 200, RoleTransformer::class);
    }

    /**
     * Create new Role
     *
     * @transformer \Rockapps\RkLaravel\Transformers\RoleTransformer
     * @authenticated
     * @param RoleSaveRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function store(RoleSaveRequest $request)
    {
        $class = config('entrust.role', Role::class);
        $plan = new $class($request->all());
        $plan->save();

        return $this->showOne($plan, 201, RoleTransformer::class);
    }

    /**
     * Attach Permission to Role
     *
     * @transformer \Rockapps\RkLaravel\Transformers\RoleTransformer
     * @authenticated
     * @param $role_id
     * @param $permission_id
     * @return JsonResponse
     */
    public function attachPermission($role_id, $permission_id)
    {
        /** @var Role $role */
        /** @var Permission $permisson */
        $class = config('entrust.role', Role::class);
        $role = $class::filter()->findOrFail($role_id);
        $class = config('entrust.permission', Permission::class);
        $permission = $class::filter()->findOrFail($permission_id);

        if (!$role->hasPermission($permission->name)) {
            $role->attachPermission($permission);
            \Cache::tags(config('entrust.role_user_table'))->flush();
        }

        return $this->showOne($role, 201, RoleTransformer::class);
    }


    /**
     * Dettach Permission to Role
     *
     * @transformer \Rockapps\RkLaravel\Transformers\RoleTransformer
     * @authenticated
     * @param $role_id
     * @param $permission_id
     * @return JsonResponse
     */
    public function dettachPermission($role_id, $permission_id)
    {
        /** @var Role $role */
        /** @var Permission $permisson */
        $class = config('entrust.role', Role::class);
        $role = $class::filter()->findOrFail($role_id);
        $class = config('entrust.permission', Permission::class);
        $permission = $class::filter()->findOrFail($permission_id);

        if ($role->hasPermission($permission->name)) {
            $role->detachPermission($permission->id);
            \Cache::tags(config('entrust.role_user_table'))->flush();
        }

        return $this->showOne($role, 200, RoleTransformer::class);
    }

    /**
     * Get specific Role
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\RoleTransformer
     * @queryParam id required The id of the plan.
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $class = config('entrust.role', Role::class);
        $role = $class::filter()->findOrFail($id);

        return $this->showOne($role, 200, RoleTransformer::class);
    }

    /**
     * Update Role
     *
     * @authenticated
     * @transformer \Rockapps\RkLaravel\Transformers\RoleTransformer
     * @param RoleSaveRequest $request
     * @queryParam id required The id of the plan.
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update(RoleSaveRequest $request, $id)
    {
        $class = config('entrust.role', Role::class);
        $role = $class::filter()->findOrFail($id);
        $role->fill($request->all());
        $role->save();
        return $this->showOne($role, 200, RoleTransformer::class);
    }

    /**
     * Delete Role
     *
     * @authenticated
     * @queryParam id required The id of the Role.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     * @response { "message" : "ok" }
     */
    public function destroy($id)
    {
        $class = config('entrust.role', Role::class);

        /** @var Role $role */
        $role = $class::whereId($id)->firstOrFail();

        $role->delete();

        return $this->successDelete();
    }
}
