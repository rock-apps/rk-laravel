<?php

namespace Rockapps\RkLaravel\Api\Auth;

use Rockapps\RkLaravel\Api\RequestBase;
use Config;
use Dingo\Api\Http\FormRequest;

/**
 * @bodyParam name string required User name
 * @bodyParam email string required User e-mail
 * @bodyParam password string required User password
 * @bodyParam invitation_code string
 */
class SignUpRequest extends RequestBase
{
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'invitation_code' => 'nullable',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
