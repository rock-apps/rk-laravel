<?php

namespace Rockapps\RkLaravel\Api\Auth;

use Rockapps\RkLaravel\Api\RequestBase;
use Config;
use Dingo\Api\Http\FormRequest;

/**
 * @bodyParam email string required User e-mail
 * @bodyParam password string required User password
 */
class LoginRequest extends RequestBase
{
    public function rules()
    {
        return [
            'email' => 'required',
            'password' => 'required'
        ];
    }

    public function authorize()
    {
        return true;
    }
}
