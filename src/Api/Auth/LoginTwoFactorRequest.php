<?php

namespace Rockapps\RkLaravel\Api\Auth;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam mobile required string
 * @bodyParam two_factor_token required string
 */
class LoginTwoFactorRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'mobile' => 'required',
            'two_factor_token' => 'required',
        ];
    }
}
