<?php

namespace Rockapps\RkLaravel\Api\Auth;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam email required string
 * @bodyParam signup_token required string
 */
class LoginWithEmailTokenRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required',
            'signup_token' => 'required',
        ];
    }
}
