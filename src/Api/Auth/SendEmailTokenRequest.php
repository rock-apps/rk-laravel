<?php

namespace Rockapps\RkLaravel\Api\Auth;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam email required string
 */
class SendEmailTokenRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required',
        ];
    }
}
