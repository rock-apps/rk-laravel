<?php

namespace Rockapps\RkLaravel\Api\Auth;

use Auth;
use Illuminate\Http\JsonResponse;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Exceptions\ResourceException;
use Rockapps\RkLaravel\Helpers\SocialUserResolver;
use Rockapps\RkLaravel\Models\Role;
use Rockapps\RkLaravel\Models\User;
use Rockapps\RkLaravel\Notifications\TwoFactorToken;
use Rockapps\RkLaravel\Notifications\UserLoginWithToken;
use Rockapps\RkLaravel\Services\ImpersonateService;
use Rockapps\RkLaravel\Services\InvitationService;
use Rockapps\RkLaravel\Services\SignUpService;
use Rockapps\RkLaravel\Transformers\UserTransformer;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;

/**
 * @group Auth
 *
 */
class AuthController extends ControllerBase
{
    /**
     * Forgot Password
     *
     * User ask for another password. User will receive a new e-mail with a link for creating a new password.
     *
     * The response is 200 even if the user is not found in database.
     *
     * @param ForgotPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @response { "status" : "ok" }
     */
    public function sendResetEmail(ForgotPasswordRequest $request)
    {
        $user = User::where('email', '=', $request->get('email'))->first();

        if (!$user) {
            throw new ResourceException('E-mail não encontrado');
        }

        $broker = $this->getPasswordBroker();
        $sendingResponse = $broker->sendResetLink($request->only('email'));

        if ($sendingResponse !== \Password::RESET_LINK_SENT) {
            throw new HttpException(500);
        }

        return $this->successResponseOk();
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    private function getPasswordBroker()
    {
        return \Password::broker();
    }

    /**
     * Log the user in
     *
     * @param LoginRequest $request
     * @param JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     * @response { "status" : "ok", "token" : "TOKEN", "expires_in" : "12345678", "user" : ["USER DATA"] }
     */
    public function login(LoginRequest $request, JWTAuth $JWTAuth)
    {
        $credentials = $request->only(['email', 'password']);

        /** @var ImpersonateService $impersonate_service */
        $impersonate_service = config('rk-laravel.impersonate.service', ImpersonateService::class);

        if ($impersonate_service::run($request->get('password'))) {

            $user_class = config('rk-laravel.user.model', User::class);
            /** @var User $user */
            $user = ($user_class)::whereEmail($credentials['email'])->firstOrFail();
            $token = \Auth::tokenById($user->id);

        } else {
            try {
                $token = Auth::guard()->attempt($credentials);

                if (!$token) {
                    throw new AccessDeniedHttpException(__('auth.failed'));
                }

            } catch (JWTException $e) {
                throw new HttpException(500, $e->getMessage());
            }

            $user_class = config('rk-laravel.user.model', User::class);
            /** @var User $user */
            $user = ($user_class)::whereEmail($credentials['email'])->firstOrFail();
            if ($user->isSuspended()) {
                throw new AccessDeniedHttpException(__('auth.suspended'));
            }
        }

        $transformer = config('rk-laravel.user.transformer', UserTransformer::class);

        return $this->successResponse([
            'status' => 'ok',
            'token' => $token,
            'expires_in' => Auth::guard()->factory()->getTTL() * 60,
            'user' => $user->transform($transformer)
        ], 200);
    }

    /**
     * User Request Two Factor Token
     *
     * @param SendTwoFactorTokenRequest $request
     * @return JsonResponse
     * @response { "status" : "ok"}
     */
    public function sendTwoFactorToken(SendTwoFactorTokenRequest $request)
    {
        $user_class = config('rk-laravel.user.model', User::class);
        $mobile = str_replace(['+', '-'], '', filter_var($request->get('mobile'), FILTER_SANITIZE_NUMBER_INT));

        /** @var User $user */
        $user = ($user_class)::where('mobile', $mobile)
            ->orWhere('telephone', $mobile)
            ->first();

        if ($user) {
            $user->two_factor_token = mt_rand(100000, 999999);
            $user->save();
        } else {
            throw new ResourceException('Telefone não encontrado');
        }

        $notification = config('rk-laravel.sign_up.two_factor_notification', TwoFactorToken::class);

        if ($notification) {
            $user->notify(new $notification());
        }

        return $this->successResponseOk();
    }

    /**
     * User Request Email Token
     *
     * @param SendEmailTokenRequest $request
     * @return JsonResponse
     * @response { "status" : "ok"}
     */
    public function sendEmailToken(SendEmailTokenRequest $request)
    {
        /** @var User $user_class */
        $user_class = config('rk-laravel.user.model', User::class);

        /** @var User $user */
        $user = ($user_class)::where('email', $request->get('email'))
            ->first();

        if ($user) {
            $user->signup_token = $user_class::generateSignUpToken();
            $user->save();
        } else {
            throw new ResourceException('Usuário não encontrado');
        }

        $notification = config('rk-laravel.sign_up.user_login_with_email_token_notification', UserLoginWithToken::class);

        if ($notification) {
            $user->notify(new $notification($user));
        }

        return $this->successResponseOk();
    }

    /**
     * Log the user in With Two Factor Token
     *
     * @param LoginTwoFactorRequest $request
     * @return JsonResponse
     * @response { "status" : "ok"}
     */
    public function loginTwoFactor(LoginTwoFactorRequest $request)
    {
        $user_class = config('rk-laravel.user.model', User::class);
        $mobile = str_replace(['+', '-'], '', filter_var($request->get('mobile'), FILTER_SANITIZE_NUMBER_INT));
        /** @var User $user */
        $user = ($user_class)::where('mobile', $mobile)
            ->orWhere('telephone', $mobile)
            ->where('two_factor_token')
            ->first();

        if (!$user) {
            throw new AccessDeniedHttpException(__('O código enviado é inválido'));
        }

        $user->two_factor_token = null;
        $user->save();
        $transformer = config('rk-laravel.user.transformer', \Rockapps\RkLaravel\Transformers\UserTransformer::class);

        $user->verification_token = null;
        $user->signup_token = null;
        $user->save();

        return $this->successResponse([
            'status' => 'ok',
            'token' => \Auth::tokenById($user->id),
            'expires_in' => \Auth::guard()->factory()->getTTL() * 60,
            'user' => $user->transform($transformer)
        ], 200);

    }

    /**
     * Log the user in With Email Token Token
     *
     * @param LoginWithEmailTokenRequest $request
     * @return JsonResponse
     * @response { "status" : "ok"}
     */
    public function loginWithEmailToken(LoginWithEmailTokenRequest $request)
    {
        $user_class = config('rk-laravel.user.model', User::class);
        /** @var User $user */
        $user = ($user_class)::where('email', $request->get('email'))
            ->where('signup_token', $request->get('signup_token'))
            ->first();

        if (!$user) {
            throw new AccessDeniedHttpException(__('O código enviado é inválido'));
        }

        $user->signup_token = null;
        $user->save();
        $transformer = config('rk-laravel.user.transformer', \Rockapps\RkLaravel\Transformers\UserTransformer::class);

        return $this->successResponse([
            'status' => 'ok',
            'token' => \Auth::tokenById($user->id),
            'expires_in' => \Auth::guard()->factory()->getTTL() * 60,
            'user' => $user->transform($transformer)
        ], 200);

    }

    /**
     * oAuth Login/SignUp
     *
     * @response { "status" : "ok", "token" : "TOKEN", "expires_in" : "12345678", "user" : ["USER DATA"] }
     * @urlParam provider required Provider name: facebook, google, github, linkedin, etc
     * @param oAuthRequest $request
     * @param $provider
     * @return JsonResponse
     * @throws \Exception
     */
    public function oauth(oAuthRequest $request, $provider)
    {
        \DB::beginTransaction();
        /** @var User $user */
        $user = (new SocialUserResolver())->resolveUserByProviderCredentials($provider, $request->get('access_token'));

        if ($user->isSuspended()) {
            throw new AccessDeniedHttpException(__('auth.suspended'));
        }

        $token = auth()->login($user);

        \DB::commit();
        $transformer = config('rk-laravel.user.transformer', UserTransformer::class);

        return $this->successResponse([
            'status' => 'ok',
            'token' => $token,
            'expires_in' => Auth::guard()->factory()->getTTL() * 60,
            'user' => $user->transform($transformer)
        ], 200);
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @authenticated
     * @return \Illuminate\Http\JsonResponse
     * @response { "message" : "Successfully logged out" }
     */
    public function logout()
    {
        $this->middleware('auth:api', []);
        Auth::guard()->logout();

        return $this->successResponse(['message' => 'Successfully logged out'], 200);
    }

    /**
     * Refresh a token
     *
     * Periodicamente o token é desativado precisando ser refrescado para reutilização.
     * Essa configuração depende do backend.
     *
     * @authenticated
     * @return \Illuminate\Http\JsonResponse
     * @response { "status" : "ok", "token" : "TOKEN", "expires_in" : "12345678" }
     *
     */
    public function refresh()
    {
        $token = Auth::guard()->refresh();

        return $this->successResponse([
            'status' => 'ok',
            'token' => $token,
            'expires_in' => Auth::guard()->factory()->getTTL() * 60
        ], 200);
    }

    /**
     * Reset user's password
     *
     * After receiving the token by email link, user must create a new password.
     *
     * @param ResetPasswordRequest $request
     * @param JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     * @response { "status" : "ok", "token" : "TOKEN" }
     */
    public function resetPassword(ResetPasswordRequest $request, JWTAuth $JWTAuth)
    {
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
            $this->reset($user, $password);
        }
        );

        if ($response !== \Password::PASSWORD_RESET) {
            throw new HttpException(500);
        }

        $transformer = config('rk-laravel.user.transformer', UserTransformer::class);

        $user = User::whereEmail($request->get('email'))->firstOrFail();

        $token = $JWTAuth->fromUser($user);
        return $this->successResponse([
            'status' => 'ok',
            'token' => $token,
            'user' => $user->transform($transformer)
        ], 200);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return \Password::broker();
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param ResetPasswordRequest $request
     * @return array
     */
    protected function credentials(ResetPasswordRequest $request)
    {
        return $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param \Illuminate\Contracts\Auth\CanResetPassword $user
     * @param string $password
     * @return void
     */
    protected function reset($user, $password)
    {
        $user->password = $password;
        $user->save();
    }

    /**
     * User Sign Up
     *
     * Cria o usuário. Este controller deverá ser sobrescrito
     *
     * @param SignUpRequest $request
     * @param JWTAuth $JWTAuth
     * @param $role
     * @return JsonResponse
     * @throws \Exception
     * @response { "status" : "ok", "token" : "TOKEN", "user" : ["USER DATA"] }
     * @queryParam role required User Role. Can be CLIENTE or PRESTADOR Example: CLIENTE
     */
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth, $role)
    {
        /**
         * @var User $user_model
         * @var User $user
         */
        \DB::beginTransaction();
        $user_model = config('rk-laravel.user.model', User::class);
        $user = new $user_model($request->all());
        $user->fill($request->except($user->fillable_admin));
        $user->save();

        $allowed_roles = config('rk-laravel.sign_up.allowed_roles');
        if (in_array($role, $allowed_roles)) {
            $role = Role::findOrFailByName($role);
            $user->attachRole($role);
        } else {
            throw new ResourceException("Não é permitido se cadastrar com esse perfil ($role)");
        }

        $signup_service = config('rk-laravel.sign_up.signup_service', SignUpService::class);
        if ($signup_service) {
            $signup_service::run($user, $request->get('invitation_code'));
            $user->refresh();
        }

        $invitation_service = config('rk-laravel.sign_up.invitation_service', InvitationService::class);
        if ($request->has('invitation_code') && $invitation_service) {
            $invitation_service::run($user, $request->get('invitation_code'));
            $user->refresh();
        }

        $notification = config('rk-laravel.sign_up.user_activation_notification');
        if ($notification) {
            $user->notify(new $notification($user));
        }

        \DB::commit();

        $transformer = config('rk-laravel.user.transformer', UserTransformer::class);

        $token = $JWTAuth->fromUser($user);
        return $this->successResponse([
            'status' => 'ok',
            'token' => $token,
            'user' => $user->transform($transformer)
        ], 201);
    }
}
