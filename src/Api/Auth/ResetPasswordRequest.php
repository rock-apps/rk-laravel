<?php

namespace Rockapps\RkLaravel\Api\Auth;

use Rockapps\RkLaravel\Api\RequestBase;
use Config;
use Dingo\Api\Http\FormRequest;

/**
 * @bodyParam token string required Token received by e-mail
 * @bodyParam email string required User e-mail
 * @bodyParam password string required New Password
 */
class ResetPasswordRequest extends RequestBase
{
    public function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required',
            'password' => 'required|confirmed|min:8'
        ];
    }

    public function authorize()
    {
        return true;
    }
}
