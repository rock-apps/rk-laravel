<?php

namespace Rockapps\RkLaravel\Api\Auth;

use Rockapps\RkLaravel\Api\RequestBase;
use Config;
use Dingo\Api\Http\FormRequest;

/**
 * @bodyParam access_token string required
 */
class oAuthRequest extends RequestBase
{
    public function rules()
    {
        return [
            'access_token' => 'required|string'
        ];
    }

    public function authorize()
    {
        return true;
    }
}
