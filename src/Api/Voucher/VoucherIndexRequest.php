<?php

namespace Rockapps\RkLaravel\Api\Voucher;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @queryParam code string
 * @queryParam model_type string
 * @queryParam model_id integer
 * @queryParam data string
 * @queryParam expires_at integer
 */
class VoucherIndexRequest extends RequestBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'nullable|string',
            'model_type' => 'nullable|string',
            'model_id' => 'nullable|integer',
            'data' => 'nullable|string',
            'expires_at' => 'nullable|date',
        ];
    }
}
