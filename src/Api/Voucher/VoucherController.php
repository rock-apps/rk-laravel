<?php

namespace Rockapps\RkLaravel\Api\Voucher;

use Exception;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Models\Voucher;
use Rockapps\RkLaravel\Transformers\VoucherTransformer;

/**
 * @group Voucher
 *
 * Voucher API Requests
 */
class VoucherController extends ControllerBase
{
    /**
     * Get All Vouchers
     *
     * @param VoucherIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @transformerCollection \Rockapps\Transformers\VoucherTransformer
     */
    public function index(VoucherIndexRequest $request)
    {
        $vouchers = Voucher::filter($request->all())->get();

        return $this->showAll($vouchers, 200, VoucherTransformer::class);
    }

    /**
     * Create new Voucher
     *
     * @param VoucherSaveRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @transformer \Rockapps\Transformers\VoucherTransformer
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(VoucherSaveRequest $request)
    {
        $replicate = $request->get('replicate', 1);
        $vouchers = collect();
        for ($x = 1; $x <= $replicate; $x++) {
            $voucher = new Voucher($request->except(['medias_upload', 'replicate']));
            $voucher->save();
            $vouchers->add($voucher);
        }

        if ($vouchers->count() > 1) {
            return $this->showAll($vouchers, 201, VoucherTransformer::class);
        } else {
            return $this->showOne($voucher, 201, VoucherTransformer::class);
        }
    }

    /**
     * Get specific Voucher
     *
     * @transformer \Rockapps\Transformers\VoucherTransformer
     * @param int $id
     * @urlParam voucher required Voucher ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $voucher = Voucher::filter()->findOrFail($id);

        return $this->showOne($voucher, 200, VoucherTransformer::class);
    }

    /**
     * Update Voucher
     *
     * @transformer \Rockapps\Transformers\VoucherTransformer
     * @param VoucherSaveRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @urlParam voucher required Voucher ID
     */
    public function update(VoucherSaveRequest $request, $id)
    {
        /** @var Voucher $voucher */
        $voucher = Voucher::filter()->findOrFail($id);
        $voucher->fill($request->except(['medias_upload']));
        $voucher->save();
//        $this->addMedias($voucher, $request->get('medias_upload'));

        return $this->showOne($voucher, 200, VoucherTransformer::class);
    }

    /**
     * Use specific Voucher
     *composer require pragmarx/tracker
     * @transformer \Rockapps\Transformers\VoucherTransformer
     * @param string $code
     * @urlParam code required Voucher Code
     * @return \Illuminate\Http\JsonResponse
     */
    public function redeemed($code)
    {
        $voucher = Voucher::filter()->where('code', $code)->firstOrFail();
        $user = \Auth::getUser();
        $user->redeemCode($code);

        return $this->showOne($voucher, 200, VoucherTransformer::class);
    }

    /**
     * Delete Voucher
     *
     * @param int $id
     * @queryParam Voucher required The id of the Voucher.
     * @return \Illuminate\Http\JsonResponse
     * @urlParam voucher required Voucher ID
     * @throws Exception
     */
    public function destroy($id)
    {
        /** @var Voucher $voucher */
        $voucher = Voucher::filter()->findOrFail($id);
        $voucher->delete();

        return $this->successDelete();
    }

}
