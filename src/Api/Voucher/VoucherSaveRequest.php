<?php

namespace Rockapps\RkLaravel\Api\Voucher;

/**
 * @bodyParam code string
 * @bodyParam model_type string required
 * @bodyParam model_id integer required
 * @bodyParam data string
 * @bodyParam expires_at date
 * @bodyParam max_redeem_qty required
 * @bodyParam company_id
 * @bodyParam campaign
 * @bodyParam replicate Definir a quantidade de vouchers que serão criados
 *
 */

class VoucherSaveRequest extends \Rockapps\RkLaravel\Api\RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'code' => 'nullable|string',
            'model_type' => 'required|string',
            'model_id' => 'required|integer',
            'data' => 'nullable|string',
            'expires_at' => 'nullable|date',
            'max_redeem_qty' => 'required|numeric|min:1',
            'company_id' => 'nullable|exists:companies,id',
            'campaign' => 'nullable',
        ];
    }
}
