<?php

namespace Rockapps\RkLaravel\Api\Notification;

use Illuminate\Notifications\DatabaseNotification;
use Rockapps\RkLaravel\Api\ControllerBase;
use Rockapps\RkLaravel\Transformers\DatabaseNotificationTransformer;

/**
 * @resource Notification
 * @group Notifications
 *
 * Notification API Requests
 *
 */
class NotificationController extends ControllerBase
{

    /**
     * Get All Notifications
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\DatabaseNotificationTransformer
     * @return \Illuminate\Http\JsonResponse
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index()
    {
        $user = \Auth::getUser();

        $notifications = $user->notifications()->get();

        return $this->showAll($notifications, 200, DatabaseNotificationTransformer::class);
    }

    /**
     * Get All Unread Notifications
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\DatabaseNotificationTransformer
     * @return \Illuminate\Http\JsonResponse
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     */
    public function indexUnread()
    {
        $user = \Auth::getUser();

        $notifications = $user->unreadNotifications()->get();

        return $this->showAll($notifications, 200, DatabaseNotificationTransformer::class);
    }

    /**
     * Mark Notification as Read
     *
     * @transformer \Rockapps\RkLaravel\Transformers\DatabaseNotificationTransformer
     * @param $id
     * @queryParam id required string
     * @return \Illuminate\Http\JsonResponse
     * @authenticated
     */
    public function markAsRead($id)
    {
        $user = \Auth::getUser();
        /** @var DatabaseNotification $notification */
        $notification = $user->notifications()->where('id', $id)->firstOrFail();
        $notification->markAsRead();

        return $this->showOne($notification, 200, DatabaseNotificationTransformer::class);
    }

    /**
     * Mark All Notifications as Read
     *
     * @transformerCollection \Rockapps\RkLaravel\Transformers\DatabaseNotificationTransformer
     * @queryParam id required string
     * @return \Illuminate\Http\JsonResponse
     * @authenticated
     * @throws \Illuminate\Validation\ValidationException
     */
    public function markAllAsRead()
    {
        $user = \Auth::getUser();
        $unread = $user->unreadNotifications();
        if($unread->count()){
            $unread->update(['read_at' => now()]);
        }

        return $this->showAll(collect(), 200, DatabaseNotificationTransformer::class);
    }

}
