<?php

namespace Rockapps\RkLaravel\Api\Rate;

use Rockapps\RkLaravel\Api\RequestBase;

/**
 * @bodyParam model_type required string,
 * @bodyParam model_id required integer,
 * @bodyParam value required integer min:1 max>5,
 * @bodyParam description nullable string,
 */
class RateSaveRequest extends RequestBase
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'model_type' => 'required|string',
            'model_id' => 'nullable|numeric',
            'value' => 'required|int|min:1|max:5',
            'description' => 'nullable|string',
        ];
    }
}
